#!/bin/bash -e

scriptDir=$(pushd "$(dirname "$0")" > /dev/null; pwd; popd > /dev/null;)

. ${scriptDir}/x-asset2.config.sh

sourceDir="${scriptDir}/${assetSourceDir}"
targetDir="${scriptDir}/${assetTargetDir}"

# ===========================================================================
echo "player"

# ---------------------------------------------------------------------------
# dead		88x94	80x86	// 2nd: extent && move-up
itemState=dead && echo "- ${itemState}" && itemDir="${sourceDir}/player/${itemState}"

convert \
	${itemDir}/dino-${itemState}-outline.png \
	-background transparent \
	-resize 88x94 \
	${itemDir}/.dino-${itemState}-outline.png

convert \
	${itemDir}/dino-${itemState}.png \
	-background transparent \
	-gravity south \
	-extent 88x94-0-4 \
	${itemDir}/.dino-${itemState}.png

cp -f ${itemDir}/.dino-${itemState}.png         ${targetDir}/player/${itemState}-1.png
cp -f ${itemDir}/.dino-${itemState}-outline.png ${targetDir}/player/${itemState}-2.png

# ---------------------------------------------------------------------------
# duck		118x60	118x60	???
itemState=duck && echo "- ${itemState}" && itemDir="${sourceDir}/player/${itemState}"

for i in 1 2
do
	convert \
		${itemDir}/dino-${itemState}-${i}.png \
		-background transparent \
		-resize 118x \
		${itemDir}/.dino-${itemState}-${i}.png

	cp -f \
		${itemDir}/.dino-${itemState}-${i}.png \
		${targetDir}/player/${itemState}-${i}.png
done

# ---------------------------------------------------------------------------
# idle		88x94	88x94
itemState=idle && echo "- ${itemState}" && itemDir="${sourceDir}/player/${itemState}"

for i in 1 2
do
	convert \
		${itemDir}/dino-${itemState}-${i}.png \
		-background transparent \
		-resize 88x94 \
		${itemDir}/.dino-${itemState}-${i}.png

	cp -f \
		${itemDir}/.dino-${itemState}-${i}.png \
		${targetDir}/player/${itemState}-${i}.png
done

# ---------------------------------------------------------------------------
# run		88x94	88x94
itemState=run && echo "- ${itemState}" && itemDir="${sourceDir}/player/${itemState}"

for i in 1 2
do
	convert \
		${itemDir}/dino-${itemState}-${i}.png \
		-background transparent \
		-resize 88x94 \
		${itemDir}/.dino-${itemState}-${i}.png

	cp -f \
		${itemDir}/.dino-${itemState}-${i}.png \
		${targetDir}/player/${itemState}-${i}.png
done

# ---------------------------------------------------------------------------
# start		88x90			<<expand-up
itemState=start && echo "- ${itemState}" && itemDir="${sourceDir}/player/${itemState}"

convert \
	${itemDir}/dino-${itemState}.png \
	-background transparent \
	-gravity south \
	-extent 88x94 \
	${itemDir}/.dino-${itemState}.png

# ===========================================================================
echo "obstacles"

# bird		92x68	92x60	// both: extent due wings {down, up}
itemState=bird && echo "- ${itemState}" && itemDir="${sourceDir}/obstacles/${itemState}"

convert \
	${itemDir}/${itemState}-1.png \
	-background transparent \
	-gravity center \
	-extent 88x94-0-7 \
	${itemDir}/.${itemState}-1.png

convert \
	${itemDir}/${itemState}-2.png \
	-background transparent \
	-gravity north \
	-extent 88x94-0-8 \
	${itemDir}/.${itemState}-2.png


cp -f ${itemDir}/.${itemState}-1.png ${targetDir}/obstacles/${itemState}-1.png
cp -f ${itemDir}/.${itemState}-2.png ${targetDir}/obstacles/${itemState}-2.png

# ---------------------------------------------------------------------------
itemState=cactus && itemState2=small && echo "- ${itemState}" && itemDir="${sourceDir}/obstacles/${itemState}/${itemState2}"
# small		34x70	68x70	102x70	// 34w each!

convert \
	${itemDir}/${itemState}-${itemState2}-1.png \
	-background transparent \
	-gravity south \
	-extent 88x94-0-0 \
	${itemDir}/.${itemState}-${itemState2}-1.png

convert \
	${itemDir}/${itemState}-${itemState2}-2.png \
	-background transparent \
	-gravity south \
	-extent 88x94-0-0 \
	${itemDir}/.${itemState}-${itemState2}-2.png

convert \
	${itemDir}/${itemState}-${itemState2}-3.png \
	-background transparent \
	-gravity south \
	-extent 88x94-0-0 \
	${itemDir}/.${itemState}-${itemState2}-3.png


cp -f ${itemDir}/.${itemState}-${itemState2}-1.png ${targetDir}/obstacles/${itemState}-${itemState2}-1.png
cp -f ${itemDir}/.${itemState}-${itemState2}-2.png ${targetDir}/obstacles/${itemState}-${itemState2}-2.png

# big		50x100	100x100	150x100	// 50w each!

# for itemSize in big small
# do
# 	for i in 1 2 3
# 	do
# 		convert \
# 			${itemDir}/${itemSize}/${itemState}-${itemSize}-${i}.png \
# 			-background transparent \
# 			${itemDir}/${itemSize}/.${itemState}-${itemSize}-${i}.png
# 	done
# done

# ===========================================================================
# ${assetSourceDir}/horizon

# cloud		92x27
itemState=cloud && echo "- ${itemState}" && itemDir="${sourceDir}/horizon/${itemState}"

convert \
	${itemDir}/${itemState}.png \
	-background transparent \
	-grayscale average \
	${itemDir}/.${itemState}.png

cp -f ${itemDir}/.${itemState}.png ${targetDir}/horizon/${itemState}.png

# ground	2400x24
itemState=ground && echo "- ${itemState}" && itemDir="${sourceDir}/horizon/${itemState}"

rm -f ${itemDir}/.${itemState}*.png
convert \
	${itemDir}/${itemState}.png \
	-background transparent \
	${itemDir}/.${itemState}.png

convert \
	${itemDir}/.${itemState}.png \
	-background transparent \
	+repage \
	-crop 88x24 \
	${itemDir}/.${itemState}-%02d.png

# 23+24 => 23
convert \
	${itemDir}/.${itemState}.png \
	-crop 88x24+2097+0 \
	${itemDir}/.${itemState}-23.png

mv -f ${itemDir}/.${itemState}-00.png ${itemDir}/.${itemState}-24.png
rm -f ${itemDir}/.${itemState}-27.png

ls -1 ${itemDir}/.${itemState}-*.png \
	| while read fn;
	do
			i=$(echo "${fn}"|rev|cut -d'.' -f2|cut -d'-' -f1|rev)
			cp -f ${fn} ${targetDir}/horizon/${itemState}-${i}.png
	done

# moon		80x80	40x80	40x80	40x80	40x80	40x80	40x80
itemState=moon && echo "- ${itemState}" && itemDir="${sourceDir}/horizon/${itemState}"

convert \
	${itemDir}/${itemState}-1.png \
	-background transparent \
	${itemDir}/.${itemState}-1.png

for i in 2 3 4
do
	convert \
		${itemDir}/${itemState}-${i}.png \
		-background transparent \
		-gravity east \
		-extent 80x80-40-0 \
		${itemDir}/.${itemState}-${i}.png
done

for i in 5 6 7
do
	convert \
		${itemDir}/${itemState}-${i}.png \
		-background transparent \
		-gravity west \
		-extent 80x80-40-0 \
		${itemDir}/.${itemState}-${i}.png
done

ls -1 ${itemDir}/.${itemState}-*.png \
	| while read fn;
	do
			i=$(echo "${fn}"|rev|cut -d'.' -f2|cut -d'-' -f1|rev)
			cp -f ${fn} ${targetDir}/horizon/${itemState}-${i}.png
	done

# star		18x18	18x18	18x18
itemState=star && echo "- ${itemState}" && itemDir="${sourceDir}/horizon/${itemState}"

for i in 1 2 3
do
	convert \
		${itemDir}/${itemState}-${i}.png \
		-background transparent \
		${itemDir}/.${itemState}-${i}.png

	cp -f ${itemDir}/.${itemState}-${i}.png ${targetDir}/horizon/${itemState}-${i}.png

done
