package com.gde.common.resources;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Test;

import com.badlogic.gdx.utils.JsonReader;

import junit.framework.TestCase;


public class CommonResourcesTest
extends TestCase
{
	private CommonResources resources;
	private final JsonReader json = new JsonReader();

	@Override
	protected void setUp() throws Exception
	{
		resources = new CommonResources();
	}

	@Test
	public void testAllResourcesAndReadThat()
	{
		testClass(resources.getClass());
	}

	private void testClass(Class<?>[] clazzes)
	{
		for (Class<?> clazz : clazzes)
		{
			testClass(clazz);
		}
	}

	private void testClass(Class<?> clazz)
	{
		testClass(clazz.getDeclaredClasses());
		testFields(clazz);
	}

	private void testFields(Class<?> clazz)
	{
		for (Field field : clazz.getFields())
		{
			if (Modifier.isPublic(field.getModifiers()))
			{
				if (field.getType().isAssignableFrom(String.class))
				{
					try
					{
						testResource((String)field.get(resources), clazz.getName() + "::" + field.getName());
					}
					catch (IllegalArgumentException | IllegalAccessException e)
					{
						fail(clazz.getName() + "\nreason: " + e);
					}
				}
			}
		}
	}

	private void testResource(String resource, String tag)
	{
		if (resource.contains("%"))
		{
			// TODO: testAnimation();
		}
		else
		if (resource.toLowerCase().endsWith(".json"))
		{
			try
			{
				assertTrue(
					resource + "@" + tag,
					!json.parse(TestResourcesUtils.readFileAsString(resource)).isNull()
				);
			}
			catch(Exception e)
			{
				fail(resource + "@" + tag + "\nreason: " + e);
			}
		}
		else
		{
			testResourceAsBytes(resource, tag);
		}
	}

	private void testResourceAsBytes(String resource, String tag)
	{
		byte[] buffer = new byte[2048];
		try(
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
		)
		{
			int character;
			int index = 0;
			while ((character = is.read()) != -1)
			{
				buffer[index++] = (byte) character;
				if (index >= buffer.length)
				{
					break;
				}
			}
			assertTrue(resource + "@" + tag, (index > 0));
		}
		catch (Exception e)
		{
			fail(resource + "@" + tag + "\nreason: " + e);
		}
	}
}
