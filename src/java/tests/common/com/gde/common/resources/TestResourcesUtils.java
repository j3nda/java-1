package com.gde.common.resources;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStream;

public class TestResourcesUtils
{
	public static String readFileAsString(String resource)
	{
		StringBuffer sb = new StringBuffer();
		try(
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
		)
		{
			int character;
			while ((character = is.read()) != -1)
			{
				sb.append((char) character);
			}
			return sb.toString();
		}
		catch (Exception e)
		{
			fail("unable to read: \"" + resource + "\". reason: " + e);
			return "";
		}
	}
}
