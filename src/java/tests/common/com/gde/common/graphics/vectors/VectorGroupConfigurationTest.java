package com.gde.common.graphics.vectors;

import org.junit.Test;

import com.badlogic.gdx.utils.Json;
import com.gde.common.resources.TestResourcesUtils;

import junit.framework.TestCase;

public class VectorGroupConfigurationTest
extends TestCase
{
	private final Json json = new Json();
	public static final String resource_treeUp1 = "com/gde/common/graphics/vectors/treeUp1-noPoints.json";
	public static final String resource_treeUp2 = "com/gde/common/graphics/vectors/treeUp2-pointsAsFlower.json";

	@Test
	public void testTreeUp_jsonToClass()
	{
		VectorGroupConfiguration config = json.fromJson(
			VectorGroupConfiguration.class,
			TestResourcesUtils.readFileAsString(resource_treeUp1)
		);
		assertTrue(config instanceof VectorGroupConfiguration);

		assertEquals(1.0f, config.unitScale);
		assertEquals(0.0f, config.origin.x);
		assertEquals(1.0f, config.origin.y);
		assertEquals(12, config.groups.size());
	}
}
