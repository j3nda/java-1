package com.gde.common.graphics.vectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.platform.commons.util.ReflectionUtils;

import com.badlogic.gdx.utils.Json;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorGroup;
import com.gde.common.resources.TestResourcesUtils;

import junit.framework.TestCase;
import space.earlygrey.shapedrawer.ShapeDrawer;

public class VectorGroupDrawableTest
extends TestCase
{
	private final Json json = new Json();
	private static final String resource_treeUp1 = VectorGroupConfigurationTest.resource_treeUp1;
	private ShapeDrawer drawer;

//	@Override
//	protected void setUp() throws Exception
//	{
//		super.setUp();
////		drawer = new ShapeDrawer(new SpriteBatch());
//	}

	@SuppressWarnings("unchecked")
	@Test
	public void testTreeUp_drawOrder() throws Exception
	{
		VectorGroupConfiguration config = json.fromJson(
			VectorGroupConfiguration.class,
			TestResourcesUtils.readFileAsString(resource_treeUp1)
		);
		VectorGroupDrawable drawable = new VectorGroupDrawable(drawer, config);

		List<Integer> drawOrder = (List<Integer>) ReflectionUtils
			.tryToReadFieldValue(VectorGroupDrawable.class, "drawOrder", drawable)
			.get()
		;
		assertEquals("[0, 1, 3, 4, 2, 5, 6, 7, 8, 10, 9, 11]", drawOrder.toString());

		List<VectorGroup> groups = (List<VectorGroup>) ReflectionUtils
			.tryToReadFieldValue(VectorGroupConfiguration.class, "groups", config)
			.get()
		;
		List<String> drawOrderGroupNameExpected = Arrays.asList(
			"[0]left-1",
				"[1]left-2.1",
					"[1]left-3.1",
					"[1]left-3.2",
				"[1]left-2.2",
					"[1]left-3.3",
					"[1]left-3.4",
			"[0]right-1",
				"[1]right-2.1",
					"[2]right-3.1",
				"[1]right-2.2",
					"[2]right-3.2"
		);
		List<String> drawOrderGroupNameActual = new ArrayList<>();
		for(int index : drawOrder)
		{
			drawOrderGroupNameActual.add(groups.get(index).name);
		}
		assertEquals(
			drawOrderGroupNameExpected.toString(),
			drawOrderGroupNameActual.toString()
		);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testTreeUp_normalize() throws Exception
	{
		VectorGroupConfiguration config = json.fromJson(
			VectorGroupConfiguration.class,
			TestResourcesUtils.readFileAsString(resource_treeUp1)
		);
		VectorGroupDrawable drawable = new VectorGroupDrawable(drawer, config);
		List<Integer> drawOrder = (List<Integer>) ReflectionUtils
			.tryToReadFieldValue(VectorGroupDrawable.class, "drawOrder", drawable)
			.get()
		;
		List<VectorGroup> groups = (List<VectorGroup>) ReflectionUtils
			.tryToReadFieldValue(VectorGroupConfiguration.class, "groups", config)
			.get()
		;

		List<String> drawOrderGroupParentNameExpected = Arrays.asList(
			"",
			"[0]left-1",
				"[1]left-2.1",
				"[1]left-2.1",
			"[0]left-1",
				"[1]left-2.2",
				"[1]left-2.2",
			"",
			"[0]right-1",
				"[1]right-2.1",
			"[0]right-1",
				"[1]right-2.2"
		);
		List<String> drawOrderGroupParentNameActual = new ArrayList<>();
		for(int index : drawOrder)
		{
			drawOrderGroupParentNameActual.add(groups.get(index).parentName);
		}
		assertEquals(
			drawOrderGroupParentNameExpected.toString(),
			drawOrderGroupParentNameActual.toString()
		);
	}
}
