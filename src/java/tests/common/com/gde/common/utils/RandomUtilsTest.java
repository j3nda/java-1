package com.gde.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import junit.framework.TestCase;


public class RandomUtilsTest
extends TestCase
{
	@Test
	public void testNextInt_paramInt()
	{
		assertEquals(-1, RandomUtils.nextInt(-1, -1));
		assertEquals(+0, RandomUtils.nextInt(+0, +0));
		assertEquals(+1, RandomUtils.nextInt(+1, +1));

		List<Integer> numbers = new ArrayList<>();

		assertEquals(1,  MathUtils.min(fillRandom(100, 1, 10, numbers)));
		assertEquals(10, MathUtils.max(fillRandom(100, 1, 10, numbers)));

		assertEquals(-5, MathUtils.min(fillRandom(100, -5, +5, numbers)));
		assertEquals(+5, MathUtils.max(fillRandom(100, -5, +5, numbers)));

		assertEquals(-10, MathUtils.min(fillRandom(100, -10, -1, numbers)));
		assertEquals(-1,  MathUtils.max(fillRandom(100, -10, -1, numbers)));
	}

	private List<Integer> fillRandom(int count, int min, int max, List<Integer> numbers)
	{
		numbers.clear();
		for (int i = 0; i < count; i++)
		{
			numbers.add(RandomUtils.nextInt(min, max));
		}
		System.out.println(numbers);
		return numbers;
	}
}
