package com.gde.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.gde.common.utils.RandomUtils;

/**
 * unit-test for {@link RandomUtils}. its quite interresting, because it tests random-numbers ;-)
 */
public class RandomUtilsTest
{
	private static final boolean logDebug = false;
	private static final boolean logDebugVerbose = false;
	private static final boolean logDebugLimit = false;

	@Test
	public void nextInt()
	{
		// total: 1
		nextIntMinMax(0, 0, 1);
		nextIntMinMax(1, 1, 1);
		nextIntMinMax(-1, -1, 1);

		// total: 2
		nextIntMinMax(11, 12, 2);
		nextIntMinMax(-12, -11, 2);

		// total: 3
		nextIntMinMax(0, 2, 3);
		nextIntMinMax(-2, 0, 3);

		// total: 5
		nextIntMinMax(1, 5, 5);
		nextIntMinMax(-5, -1, 5);

		// total: 9999
		nextIntMinMax(1, 9999, 9999);
	}

	@Test
	public void nextFloat()
	{
		// total: 1
		nextFloatMinMax(0, 0);
		nextFloatMinMax(1, 1);
		nextFloatMinMax(-1, -1);

		// interval: small ~ {1,2}
		nextFloatMinMax(0, 1);
		nextFloatMinMax(-1, 0);
		nextFloatMinMax(1, 3);
		nextFloatMinMax(-3, -1);

		// interval: medium ~ {5,10}
		nextFloatMinMax(5, 10);
		nextFloatMinMax(1, 10);

		// interval: big ~ {9999}
		nextFloatMinMax(1, 9999);
	}

	private void nextIntMinMax(int min, int max, int total)
	{
		Set<Integer> exp = fill(min, max);
		Set<Integer> cur = tryRandom(exp);

		assertEquals(total, exp.size());
		assertEquals(exp, cur);
	}

	private void nextFloatMinMax(float min, float max)
	{
		float range = max - min;
		int limit = (int)range;
		if (range == 0)
		{
			limit = 1;
		}
		else
		if (range <= 1)
		{
			limit = 10;
		}
		else
		if (range <= 10)
		{
			limit = (int) (100 * range);
		}
		nextFloatMinMax(min, max, limit);
	}

	private void nextFloatMinMax(float min, float max, int limit)
	{
		Set<Float> exp = fill(min, max);
		Set<Float> cur = tryRandom(exp, limit);

		assertRandom(exp, cur);
	}

	private void assertRandom(Set<Float> exp, Set<Float> cur)
	{
		float min = Collections.min(exp);
		float max = Collections.max(exp);
		for(float num : cur)
		{
			assertTrue((num >= min), "num(" + num + ") >= min(" + min + ")");
			assertTrue((num <= max), "num(" + num + ") <= max(" + max + ")");
		}
	}

	private <T extends Number & Comparable<T>> Set<T> tryRandom(Set<T> exp)
	{
		return tryRandom(
			exp,
			tryRandomLimit(exp)
		);
	}

	@SuppressWarnings("unchecked")
	private <T extends Number & Comparable<T>> Set<T> tryRandom(Set<T> exp, int limit)
	{
		Set<T> cur = new HashSet<>();
		T min = Collections.min(exp);
		T max = Collections.max(exp);
		int counter = 0;
		Class<?> expTypeClazz = exp.iterator().next().getClass();

		while(
			   (expTypeClazz.isAssignableFrom(Integer.class) && cur.size() < exp.size() && counter < limit)
			|| (expTypeClazz.isAssignableFrom(Float.class) && counter < limit)
		)
		{
			counter++;
			T random = null;
			if (expTypeClazz.isAssignableFrom(Integer.class))
			{
				cur.add(random = (T) Integer.valueOf(
					RandomUtils.nextInt((int)min, (int)max)
				));
			}
			else
			if (expTypeClazz.isAssignableFrom(Float.class))
			{
				cur.add(random = (T) Float.valueOf(
					RandomUtils.nextFloat((float)min, (float)max)
				));
			}
			logDebug(counter, limit, random, exp, cur);
		}
		if (logDebug)
		{
			System.out.println(new Throwable().getStackTrace()[1].getMethodName() + ": cnt=" + counter + " / " + limit);
			System.out.println();
		}
		return cur;
	}

	/**
	 * return limit how many times we will try to generate randomNumber
	 * (~based on 'coupon collector's problem')
	 */
	private int tryRandomLimit(Set<?> entries)
	{
		// i asked chatGPT (--https://chat.openai.com) about this problem and he/she/it recommend:
		// 1) 'Pigeonhole Principle' -- https://en.wikipedia.org/wiki/Pigeonhole_principle
		//    and later...
		//    (~because i wasnt satisfied)
		// 2) 'Coupon collector's problem' -- https://en.wikipedia.org/wiki/Coupon_collector%27s_problem

		int min = 1;
		int max = entries.size();

		// so: coupon collector's problem its...
		int n = max - min + 1; // range
		int p = 3; // multiply by 2 for a higher probability
		double maxIterations = n * Math.log(n) * p;

		// just some corrections...
		int retIterations = (maxIterations > Integer.MAX_VALUE
			? Integer.MAX_VALUE
			: (int)Math.ceil(maxIterations)
		);

		if (logDebugLimit)
		{
			Throwable t = new Throwable();
			if (!logDebug)
			{
				System.out.println(t.getStackTrace()[2].getMethodName());
			}
			System.out.println("\t" + t.getStackTrace()[0].getMethodName()
				+ ": n=" + n
				+ ", maxIterations=" + (n * Math.log(n) * p)
				+ ", Integer(maxIterations > MAX_VALUE)=" + (maxIterations > Integer.MAX_VALUE)
				+ ", Math.max=" + Math.max(n * 2, maxIterations)
			);
		}

		// well, well, well naacelniku :) chatGPT is just a language model...
		// chat: 'You're right, my previous formula does not work well for very small ranges. In such cases, we can simply set maxIterations to a value greater than or equal to the number of distinct values in the range.'
		// so i made a improvement...
		return Math.max(
			(n < 20
				? n * 4
				: n * 2
			),
			retIterations
		);
	}

	private Set<Integer> fill(int min, int max)
	{
		Set<Integer> fill = new HashSet<>();
		for (int i = min; i <= max; i++)
		{
			fill.add(i);
		}
		if (logDebug)
		{
			System.out.println(log(fill, 2));
		}
		return fill;
	}

	private Set<Float> fill(float min, float max)
	{
		Set<Float> fill = new HashSet<>(Arrays.asList(min, max));
		if (logDebug)
		{
			System.out.println(log(fill, 2));
		}
		return fill;
	}

	private <T extends Number & Comparable<T>> String log(Set<T> entries, int index)
	{
		List<T> sorted = new ArrayList<>(entries);
		Collections.sort(sorted);
	    return (index < 0
		    	? ""
	    		: new Throwable().getStackTrace()[index].getMethodName() + ": "
	    	)
	    	+ "set=" + sorted
    	;
	}

	private <T extends Number & Comparable<T>> void logDebug(int counter, int limit, T random, Set<T> exp, Set<T> cur)
	{
		if (!logDebugVerbose)
		{
			return;
		}
		System.out.println("\tcnt:(" + counter + " < " + limit + "); random=" + random);
		System.out.println("\tcur(" + cur.size() + "):" + log(cur, -1));
		System.out.println("\texp(" + exp.size() + "):" + log(exp, -1));
		System.out.println();
	}
}
