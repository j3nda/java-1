package com.gde.luzanky.dguy.hra.collisions;

public interface IHasCollisionBody
{
	CollisionBody body();
}
