package com.gde.luzanky.dguy.hra.player.gun;

import com.badlogic.gdx.graphics.Color;
import com.gde.luzanky.dguy.hra.bonus.BaseAttr;

public class GunAttr
extends BaseAttr<GunAttr>
{
	public int damage;
	public float range;
	public int size;
	public Color color;

	public GunAttr(int damage, float range, int size, Color color)
	{
		this.damage = damage;
		this.range = range;
		this.size = size;
		this.color = color;
	}

	public void update(GunBonus bonus)
	{
		damage = bonus.damage;
		range = bonus.range;
		size = bonus.size;
		color = bonus.color;
	}

	public void update(ProjectileColorBonus bonus)
	{
		color = bonus.color;
	}
}
