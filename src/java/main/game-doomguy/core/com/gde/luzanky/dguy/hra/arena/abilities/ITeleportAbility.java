package com.gde.luzanky.dguy.hra.arena.abilities;

import com.gde.luzanky.dguy.hra.player.Player;

public interface ITeleportAbility
{
	void teleport(Player player);
}
