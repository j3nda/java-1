package com.gde.luzanky.dguy.hra;

public interface IHasId
{
	int id();
}
