package com.gde.luzanky.dguy.hra.player;

import com.gde.luzanky.dguy.hra.bonus.BonusAttr;

public class PlayerSpeedBonus
extends BonusAttr
{
	/** speed multiplier */
	public float speed;

	public PlayerSpeedBonus(float speed)
	{
		super(ApplyType.Stacked, Player.class);
		this.speed = speed;
	}
}
