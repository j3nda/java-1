package com.gde.luzanky.dguy.hra.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.luzanky.dguy.debug.DebugDrawCollision;
import com.gde.luzanky.dguy.hra.IHasId;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.collisions.CollisionBody;
import com.gde.luzanky.dguy.hra.collisions.IHasCollisionBody;

public class BonusItem
extends Image
implements IHasCollisionBody, IHasId
{
	private final int id;
	private final BonusAttr attr;
	private static int idCounter = 0;
	private CollisionBody collisionBody;
	private static final boolean debug = ObrazovkaHry.debug;
	private DebugDrawCollision debugDraw;

	public BonusItem(BonusAttr attr)
	{
		super(
			new Texture("tint/square16x16.png")
		);
		this.id = idCounter++;
		this.attr = attr;
		this.collisionBody = new CollisionBody(4);
		setSize(32, 32);
		setColor(Color.GOLD);
	}

	public BonusAttr attr()
	{
		return attr;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		// TODO: game/collision: bonus x player
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		if (debug)
		{
			if (debugDraw == null)
			{
				debugDraw = new DebugDrawCollision(collisionBody);
			}
			debugDraw.draw(batch, parentAlpha);
		}
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		if (collisionBody != null)
		{
			updateCollisionBody();
		}
	}

	@Override
	protected void sizeChanged()
	{
		super.sizeChanged();
		setOrigin(
			getWidth() / 2f,
			getHeight() / 2f
		);
		if (collisionBody != null)
		{
			updateCollisionBody();
		}
	}

	private void updateCollisionBody()
	{
		collisionBody.setPosition(getX(), getY());
		collisionBody.setOrigin(getOriginX(),getOriginY());
		collisionBody.setRotation(getRotation());

		collisionBody.updatePoint(0, 0, 0);//left-bottom
		collisionBody.updatePoint(1, getWidth(), 0);//right-bottom
		collisionBody.updatePoint(2, 0, getHeight());//left-top
		collisionBody.updatePoint(3, getWidth(), getHeight());//right-top
	}

	@Override
	public CollisionBody body()
	{
		return collisionBody;
	}

	@Override
	public int id()
	{
		return id;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		BonusItem other = (BonusItem) obj;
		if (id != other.id)
		{
			return false;
		}
		return true;
	}
}
