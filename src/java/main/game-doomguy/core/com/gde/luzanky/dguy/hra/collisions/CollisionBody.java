package com.gde.luzanky.dguy.hra.collisions;

import com.badlogic.gdx.math.Polygon;

public class CollisionBody
extends Polygon // TODO: libgdx.polygon has non-optimal way of getTransformedVertices(); esp. sin/cos if rotation == 0!
{
	public CollisionBody(int maxPoints)
	{
		super(new float[2 * maxPoints]);
	}

	/**
	 * update collision point
	 * @param index of [x,y] point
	 * @param x
	 * @param y
	 */
	public void updatePoint(int index, float x, float y)
	{
		float[] v = getVertices();

		v[index * 2 + 0] = x;
		v[index * 2 + 1] = y;

		setVertices(v);
	}
}
