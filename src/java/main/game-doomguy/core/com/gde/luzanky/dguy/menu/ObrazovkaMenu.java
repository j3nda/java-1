package com.gde.luzanky.dguy.menu;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.luzanky.dguy.DGuyGame;
import com.gde.luzanky.dguy.SdilenaObrazovka;
import com.gde.luzanky.dguy.TypObrazovky;
import com.gde.luzanky.dguy.hra.GameConfiguration;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.arena.BounceArena;
import com.gde.luzanky.dguy.hra.arena.TeleportArena;

public class ObrazovkaMenu
extends SdilenaObrazovka
{
	private Label start;
	private static final List<ArenaItem> arenaItems;
	private static final List<ControlsItem> controlsItems;
	private Label arena;
	private Label arenaName;
	private int arenaIndex = 0;
	private Label controls;
	private Label controlsName;
	private int controlsIndex = 0;
	static
	{
		arenaItems = new ArrayList<>();
		arenaItems.add(new ArenaItem(BounceArena.class));
		arenaItems.add(new ArenaItem(TeleportArena.class));

		controlsItems = new ArrayList<>();
	}

	public ObrazovkaMenu(DGuyGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MENU;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}

	@Override
	public void show()
	{
		super.show();

		controlsItems.clear();
		controlsItems.add(new ControlsItem(ControlsEnum.WSAD));
		controlsItems.add(new ControlsItem(ControlsEnum.Joystick));
		// TODO: per platform

//		5
//		D!GUY
		Table wrapper = new Table();

		wrapper
			.add(controls = resources.createLabel("controls"))
		;
		wrapper
			.add(arena = resources.createLabel("arena"))
		;
		wrapper
			.add(start = resources.createLabel("start"))
		;
		wrapper
			.add(resources.createLabel("player"))
		;
		wrapper.row();

		wrapper.add(controlsName = resources.createLabel(controlsItems.get(controlsIndex).name));
		wrapper.add(arenaName = resources.createLabel(arenaItems.get(arenaIndex).name));
		wrapper.add(new Actor());
		wrapper.add(new Actor());
		wrapper.row();

		wrapper.setPosition(0, 0);
		wrapper.setSize(getWidth(), getHeight());
		wrapper.debug();

		start.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				parentGame.getScreenResources().setGameConfiguration(
					new GameConfiguration(
						arenaItems.get(arenaIndex).clazz,
						70,
						ControlsEnum.values()[controlsIndex]
					)
				);
				parentGame.setScreen(
					new ObrazovkaHry(
						parentGame,
						getScreenType()
					)
				);
//				parentGame.setScreen(
//					new DoomMelt(
//						parentGame,
//						getScreenType()
//					)
//				);
			}
		});
		ClickListener arenaClick = new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				arenaIndex++;
				if (arenaIndex >= arenaItems.size())
				{
					arenaIndex = 0;
				}
				arenaName.setText(arenaItems.get(arenaIndex).name);
			}
		};
		arena.addListener(arenaClick);
		arenaName.addListener(arenaClick);
		ClickListener controlsClick = new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				controlsIndex++;
				if (controlsIndex >= controlsItems.size())
				{
					controlsIndex = 0;
				}
				controlsName.setText(controlsItems.get(controlsIndex).name);
			}
		};
		controls.addListener(controlsClick);
		controlsName.addListener(controlsClick);

		getStage().addActor(wrapper);
	}

	public enum ControlsEnum { WSAD, Joystick }
	private static class ControlsItem
	{
		public final ControlsEnum type;
		public final String name;

		ControlsItem(ControlsEnum type)
		{
			this.type = type;
			this.name = type.name();
		}
	}

	private static class ArenaItem
	{
		public final Class<?> clazz;
		public final String name;

		ArenaItem(Class<?> clazz)
		{
			this.clazz = clazz;
			this.name = clazz.getSimpleName();
		}
	}

}
