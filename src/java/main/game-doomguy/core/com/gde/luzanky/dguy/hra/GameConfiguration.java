package com.gde.luzanky.dguy.hra;

import com.gde.luzanky.dguy.menu.ObrazovkaMenu.ControlsEnum;

public class GameConfiguration
{
	public final Class<?> arenaClazz;
	public final int arenaTimeLeft;
	public final ControlsEnum controls;

	public GameConfiguration(Class<?> arenaClazz, int arenaTimeLeft, ControlsEnum controls)
	{
		this.arenaClazz = arenaClazz;
		this.arenaTimeLeft = arenaTimeLeft;
		this.controls = controls;
	}
}
