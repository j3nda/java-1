package com.gde.luzanky.dguy.hra.bonus;

import java.util.HashSet;
import java.util.Set;

import com.gde.luzanky.dguy.hra.bonus.BonusAttr.UseType;

public abstract class BonusAttr
{
	public enum ApplyType {
		Replaced,
		Stacked
	}
	public enum UseState {
		None,
		ActiveAndUsing,
		ActiveNotUsingCooldownInProgress,
		WaitingForActivation,
	}
	protected enum UseType {
		ByPick,
		ByUse, // TODO: game/bonus: ByUse -> WaitingForActivation -> lifecycle
	}
	/** maximum amount of using it */
	protected int useLimit = 1;
	/** type how {@link BonusAttr} are applied */
	protected UseType useType = UseType.ByPick;
	/** state, how {@link BonusAttr} its used. it has impact on act(); */
	protected UseState useState = UseState.None;
	/** cooldown time for {@link UseType.Periodical} */
	protected Float useCooldownTime = null;
	/** set of 'objects', which are able to use this {@link BonusAttr} */
	private final Set<Class<?>> whoCanUse = new HashSet<>();
	/** how its applied (~eg: stacked, replaced) */
	final ApplyType applyType;

	protected BonusAttr(ApplyType applyType, Class<?>... whoCanUse)
	{
		this.applyType = applyType;
		for(Class<?> entry : whoCanUse)
		{
			this.whoCanUse.add(entry);
		}
	}

	/** return true, if 'Object' can apply this bonus. otherwise returns false. */
	public boolean canApply(Object who)
	{
		if (whoCanUse.isEmpty())
		{
			return true; // everybody
		}
		for(Class<?> entry : whoCanUse)
		{
			if (entry.isAssignableFrom(who.getClass()))
			{
				return true; // only neo! can follow rabbit hole.
			}
		}
		return false;
	}

	public void apply()
	{
		// TODO: game/bonus: useLimit--
	}

	public boolean isPeriodical()
	{
		return (useCooldownTime != null);
	}

	@Override
	public String toString()
	{
		return ""
			+ "useType=" + useType
			+ ", useLimit=" + useLimit
			+ ", useCooldownTime=" + useCooldownTime
		;
	}
}
