package com.gde.luzanky.dguy.hra.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.luzanky.dguy.debug.DebugDraw;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.collisions.CollisionBody;
import com.gde.luzanky.dguy.hra.player.Player;
import com.gde.luzanky.dguy.hra.player.gun.Gun;

public class Projectile // TODO: XHONZA/optimalizace: Poolable<Projectile>
extends Image
implements IProjectile
{
	private final Gun gun;
	private final CollisionBody collisionBody;
	private final int parentId;
	private final int id;
	private final float range;
	private final int damage;
	private static int idCounter = 0;
	private static final boolean debug = ObrazovkaHry.debug;

	public Projectile(Player player)
	{
		super(
			new Texture("tint/square16x16.png")
		);
		this.id = idCounter++;
		this.gun = player.gun(); // TODO: hra/logika: hrac muze zmenit zbran! je potreba to freeznout!
		this.range = gun.range();
		this.damage = gun.damage();
		this.parentId = player.id();
		this.collisionBody = new CollisionBody(4);

		int size = gun.projectileSize();
		setSize(size, size);
		setOrigin(size / 2f, size /2f);
		setColor(player.getColor());

		float rot = gun.getRotation();
		float p1x = MathUtils.cosDeg(rot) * gun.getWidth();
		float p1y = MathUtils.sinDeg(rot) * gun.getWidth();

		setPosition(
			player.getX() + player.getOriginX() + p1x - (getWidth() / 2f),
			player.getY() + player.getOriginY() + p1y - (getHeight()/ 2f)
		);
		setRotation(rot);
		updateCollisionBody();

		fire(p1x, p1y, rot, 1f / player.speed());
	}

	private void fire(float x, float y, float rot, float speed)
	{
		float p2x = MathUtils.cosDeg(rot) * gun.range();
		float p2y = MathUtils.sinDeg(rot) * gun.range();

		// TODO: ugly! refactor!
		final Projectile THIS = this;
		addAction(
			Actions.sequence(
				Actions.moveBy(
					p2x,
					p2y,
					speed
				),
				Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						Gdx.app.postRunnable(new Runnable()
						{
							@Override
							public void run()
							{
								THIS.remove();
							}
						});
					}
				})
			)
		);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		drawDebug(batch, parentAlpha);
	}

	private void drawDebug(Batch batch, float parentAlpha)
	{
		if (!debug)
		{
			return;
		}
		boolean isBatch = batch.isDrawing();
		if (!isBatch)
		{
			batch.begin();
		}
		drawDebugCollision(batch, parentAlpha);
		if (!isBatch)
		{
			batch.end();
		}
	}

	private void drawDebugCollision(Batch batch, float parentAlpha)
	{
		int radius = 10;
		int lineWidth = 2;
		float x = collisionBody.getX();
		float y = collisionBody.getY();
		float cx = x + collisionBody.getOriginX();
		float cy = y + collisionBody.getOriginY();

//		// center: circle
//		DebugDraw.drawer(batch).circle(cx, cy, radius, lineWidth);
//
//		// center: cross
//		DebugDraw.drawer(batch).line( // horizontal
//			cx - (2 * radius),
//			cy,
//			cx + (2 * radius),
//			cy,
//			lineWidth
//		);
//		DebugDraw.drawer(batch).line( // vertical
//			cx,
//			cy - (2 * radius),
//			cx,
//			cy + (2 * radius),
//			lineWidth
//		);

		// body's vertices
		float[] vert = collisionBody.getTransformedVertices();
		for(int i = 0; i < vert.length; i += 2)
		{
			float vx = vert[i];
			float vy = vert[i + 1];
//System.out.println("========");
//System.out.println(x + "," + y);
//System.out.println(vx + "," + vy);
//System.out.println(getX() + "," + getY());
//
//			DebugDraw.drawer(batch).circle(
//				x + vx,
//				y + vy,
//				5,
//				lineWidth
//			);
			DebugDraw.drawer(batch).filledRectangle(
				vx,
				vy,
				4,
				4
			);
		}

//		Rectangle r = collisionBody.getBoundingRectangle();
//		DebugDraw.drawer(batch).circle(r.x, r.y, radius, lineWidth);
//		DebugDraw.drawer(batch).circle(r.x + r.width, r.y, radius, lineWidth);
//		DebugDraw.drawer(batch).circle(r.x + r.width, r.y + r.height, radius, lineWidth);
//		DebugDraw.drawer(batch).circle(r.x, r.y + r.height, radius, lineWidth);


//		collisionBody.setPosition(getX(), getY());
//		collisionBody.setOrigin(getOriginX(),getOriginY());
//		collisionBody.setRotation(getRotation());
//
//		collisionBody.updatePoint(0, 0, 0);//left-bottom
//		collisionBody.updatePoint(1, getWidth(), 0);//right-bottom
//		collisionBody.updatePoint(2, 0, getHeight());//left-top
//		collisionBody.updatePoint(3, getWidth(), getHeight());//right-top
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		if (collisionBody != null)
		{
			collisionBody.setPosition(getX(), getY());
		}
	}

	@Override
	protected void sizeChanged()
	{
		super.sizeChanged();
		if (collisionBody != null)
		{
			updateCollisionBody();
		}
	}

	@Override
	protected void rotationChanged()
	{
		super.rotationChanged();
		if (collisionBody == null)
		{
			collisionBody.setRotation(getRotation());
		}
	}

	private void updateCollisionBody()
	{
		collisionBody.setPosition(getX(), getY());
		collisionBody.setOrigin(getOriginX(),getOriginY());
		collisionBody.setRotation(getRotation());

		collisionBody.updatePoint(0, 0, 0);//left-bottom
		collisionBody.updatePoint(1, getWidth(), 0);//right-bottom
		collisionBody.updatePoint(2, 0, getHeight());//left-top
		collisionBody.updatePoint(3, getWidth(), getHeight());//right-top
	}

	public int parentId()
	{
		return parentId;
	}

	@Override
	public int id()
	{
		return id;
	}

	@Override
	public CollisionBody body()
	{
		return collisionBody;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Projectile other = (Projectile) obj;
		if (id != other.id)
		{
			return false;
		}
		return true;
	}

	public float range()
	{
		return range;
	}

	public int damage()
	{
		return damage;
	}
}
