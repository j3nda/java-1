package com.gde.luzanky.dguy.hra.arena;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.luzanky.dguy.hra.GameConfiguration;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.arena.abilities.IBounceAbility;
import com.gde.luzanky.dguy.hra.player.Player;

public class BounceArena
extends BaseArena
implements IBounceAbility
{
	public BounceArena(Viewport viewport, Batch batch, ObrazovkaHry game, GameConfiguration gameConfig)
	{
		super(viewport, batch, game, gameConfig);
	}

	@Override
	protected void processCollisions()
	{
		super.processCollisions();
// TODO: vsechno co se hybe -> chces kontrolovat.... abych se zbavil toho processBehavior() coz je blbe rozhodnuti!
	}

	@Override
	public void processBehavior(Player player)
	{
		bounce(player);
	}

	@Override
	public void bounce(Player player)
	{
		if (player.getX() < 0)
		{
			player.direction().x *= -1;
			player.setX(0);
		}
		if (player.getX() + player.getWidth() > getWidth())
		{
			player.direction().x *= -1;
			player.setX(getWidth() - player.getWidth());
		}
		if (player.getY() < 0)
		{
			player.direction().y *= -1;
			player.setY(0);
		}
		if (player.getY() + player.getHeight() > getHeight())
		{
			player.direction().y *= -1;
			player.setY(getHeight() - player.getHeight());
		}
		// TODO: gfx/bounce action
		// TODO: sfx/bounce sound
	}
}
