package com.gde.luzanky.dguy.hra.player.gun;

public class GunBonusJson
{
	public int damage;
	public float range;
	public int size;
	public GunBonusJson bonus;
}
