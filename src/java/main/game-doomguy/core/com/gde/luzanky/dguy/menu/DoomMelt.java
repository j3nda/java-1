package com.gde.luzanky.dguy.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.gde.luzanky.dguy.DGuyGame;
import com.gde.luzanky.dguy.SdilenaObrazovka;
import com.gde.luzanky.dguy.TypObrazovky;


public class DoomMelt extends SdilenaObrazovka
{
	private Texture textureA;
	private Texture textureB;
	private Pixmap pixmapA;
	private Pixmap pixmapB;
    private float transitionProgress = 0f;
    private float transitionSpeed = 0.05f; // Adjust the speed of transition
	private FrameBuffer frameBuffer;


	public DoomMelt(DGuyGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MELT_TEST;
	}

	@Override
	public void show()
	{
		super.show();

		textureA = new Texture(Gdx.files.internal("a.jpg"));
		textureB = new Texture(Gdx.files.internal("b.jpg"));

		// Convert textures to pixmaps for direct pixel manipulation
		pixmapA = new Pixmap(Gdx.files.internal("a.jpg"));
		pixmapB = new Pixmap(Gdx.files.internal("b.jpg"));

		frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
	}

	@Override
	protected void renderScreen(float delta)
	{
		Batch batch = getBatch();

        // Update transition progress
        transitionProgress += transitionSpeed * Gdx.graphics.getDeltaTime();

        // Clamp the transition progress to [0, 1]
        transitionProgress = Math.min(1f, transitionProgress);

        // Clear the screen
//        Gdx.gl.glClearColor(0, 0, 0, 1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        // Begin frame buffer
        frameBuffer.begin();

        // Begin batch and draw to the frame buffer
        batch.setProjectionMatrix(batch.getProjectionMatrix());
        batch.setTransformMatrix(batch.getTransformMatrix());
        batch.begin();

        // Compute the melting effect
        for (int x = 0; x < Gdx.graphics.getWidth(); x++) {
            for (int y = 0; y < Gdx.graphics.getHeight(); y++) {
                int colorA = pixmapA.getPixel(x, y);
                int colorB = pixmapB.getPixel(x, y);

                int mixedColor = mixColors(colorA, colorB, transitionProgress);
                pixmapA.drawPixel(x, y, mixedColor);
            }
        }

        // Convert the modified pixmap to a texture
        Texture modifiedTexture = new Texture(pixmapA);

        // Draw the modified texture to the frame buffer
        batch.draw(modifiedTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();


        // End frame buffer
        frameBuffer.end();

        // Draw the frame buffer to the screen
        batch.begin();
        batch.draw(frameBuffer.getColorBufferTexture(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();
	}

	private int mixColors(int colorA, int colorB, float alpha)
	{
		// Extract RGB components
		int aR = (colorA >> 24) & 0xff;
		int aG = (colorA >> 16) & 0xff;
		int aB = (colorA >> 8) & 0xff;

		int bR = (colorB >> 24) & 0xff;
		int bG = (colorB >> 16) & 0xff;
		int bB = (colorB >> 8) & 0xff;

		// Interpolate components
		int r = (int) (aR * (1 - alpha) + bR * alpha);
		int g = (int) (aG * (1 - alpha) + bG * alpha);
		int b = (int) (aB * (1 - alpha) + bB * alpha);

		return (r << 24) | (g << 16) | (b << 8) | 0xff;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		textureA.dispose();
		textureB.dispose();
		pixmapA.dispose();
		pixmapB.dispose();
		frameBuffer.dispose();
	}
}
// git-gula / commitId: 6dbce9d5 / screenTransitionManager
// -- https://github.com/crykn/libgdx-screenmanager?tab=readme-ov-file >> /c/xhonza/src/java-libgdx-screen-manager

// doom-melt
// -- https://twitter.com/davidwalshblog/status/769245845245964288
// -- https://davidwalsh.name/canvas-effect
// -- https://gist.github.com/neuro-sys/0a60949101b5781b8d1420e691a120db
// -- https://doom.fandom.com/wiki/Screen_melt
