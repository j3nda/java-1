package com.gde.luzanky.dguy.debug;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.gde.luzanky.dguy.hra.collisions.CollisionBody;

public class DebugDrawCollision
{
	private final CollisionBody collisionBody;

	public DebugDrawCollision(CollisionBody collisionBody)
	{
		this.collisionBody = collisionBody;
	}

	public void draw(Batch batch, float parentAlpha)
	{
		boolean isBatch = batch.isDrawing();
		if (!isBatch)
		{
			batch.begin();
		}
		drawCollision(batch, parentAlpha);
		if (!isBatch)
		{
			batch.end();
		}
	}

	private void drawCollision(Batch batch, float parentAlpha)
	{
		int radius = 10;
		int lineWidth = 2;
		float x = collisionBody.getX();
		float y = collisionBody.getY();
		float cx = x + collisionBody.getOriginX();
		float cy = y + collisionBody.getOriginY();

		// center: circle
		DebugDraw.drawer(batch).circle(cx, cy, radius, lineWidth);

		// center: cross
		DebugDraw.drawer(batch).line( // horizontal
			cx - (2 * radius),
			cy,
			cx + (2 * radius),
			cy,
			lineWidth
		);
		DebugDraw.drawer(batch).line( // vertical
			cx,
			cy - (2 * radius),
			cx,
			cy + (2 * radius),
			lineWidth
		);

		// body's vertices
		float[] vert = collisionBody.getTransformedVertices();
		for(int i = 0; i < vert.length; i += 2)
		{
			float vx = vert[i];
			float vy = vert[i + 1];
//System.out.println("========");
//System.out.println(x + "," + y);
//System.out.println(vx + "," + vy);
//System.out.println(getX() + "," + getY());
//
//			DebugDraw.drawer(batch).circle(
//				x + vx,
//				y + vy,
//				5,
//				lineWidth
//			);
			DebugDraw.drawer(batch).filledRectangle(
				vx,
				vy,
				4,
				4
			);
		}

		Rectangle r = collisionBody.getBoundingRectangle();
		DebugDraw.drawer(batch).circle(r.x, r.y, radius, lineWidth);
		DebugDraw.drawer(batch).circle(r.x + r.width, r.y, radius, lineWidth);
		DebugDraw.drawer(batch).circle(r.x + r.width, r.y + r.height, radius, lineWidth);
		DebugDraw.drawer(batch).circle(r.x, r.y + r.height, radius, lineWidth);


//		collisionBody.setPosition(getX(), getY());
//		collisionBody.setOrigin(getOriginX(),getOriginY());
//		collisionBody.setRotation(getRotation());
//
//		collisionBody.updatePoint(0, 0, 0);//left-bottom
//		collisionBody.updatePoint(1, getWidth(), 0);//right-bottom
//		collisionBody.updatePoint(2, 0, getHeight());//left-top
//		collisionBody.updatePoint(3, getWidth(), getHeight());//right-top
	}
}
