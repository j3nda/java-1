package com.gde.luzanky.dguy.debug;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import space.earlygrey.shapedrawer.ShapeDrawer;

// TODO: XHONZA/merge it together with: package com.gde.common.graphics.display.DebugDraw!
public class DebugDraw
{
	private static final Texture debugTexture;
	private static final TextureRegion debugRegion;
	private static Map<Batch, ShapeDrawer> debugDrawers = new HashMap<>();
	static
	{
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.drawPixel(0, 0);
		debugTexture = new Texture(pixmap);
		pixmap.dispose();
		debugRegion = new TextureRegion(debugTexture, 0, 0, 1, 1);
	}

	public static TextureRegion texture()
	{
		return debugRegion;
	}

	public static ShapeDrawer drawer(Batch batch)
	{
		ShapeDrawer drawer = debugDrawers.get(batch);
		if (drawer == null)
		{
			debugDrawers.put(
				batch,
				drawer = new ShapeDrawer(batch, texture())
			);
		}
		return drawer;
	}

	public static void dispose()
	{
		if (debugTexture != null)
		{
			debugTexture.dispose();
		}
		debugDrawers.clear();
	}
}
