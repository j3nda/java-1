package com.gde.luzanky.dguy.hra.player.gun;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.JsonReader;
import com.gde.luzanky.dguy.hra.bonus.BonusAttr;

public class GunBonus
extends BonusAttr
{
	public int damage;
	public float range;
	public int size;
	public Color color;
	private static final JsonReader json = new JsonReader();

	public GunBonus(GunBonusJson json)
	{
		this(json.damage, json.range, json.size, Color.WHITE);
	}

	public GunBonus(String jsonString)
	{
		this(
			json.parse(jsonString).getInt("damage", 111),
			json.parse(jsonString).getFloat("range", 111),
			json.parse(jsonString).getInt("size", 111),
			Color.WHITE
		);
	}

	public GunBonus(int damage, float range, int size, Color color)
	{
		super(ApplyType.Replaced);
		this.damage = damage;
		this.range = range;
		this.size = size;
		this.color = color;
	}
}
