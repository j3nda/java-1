package com.gde.luzanky.dguy.hra.arena;

import com.gde.luzanky.dguy.hra.player.Player;

public interface IArena
{
	void processBehavior(Player player);
//	void processCollisions(Player player);
}
