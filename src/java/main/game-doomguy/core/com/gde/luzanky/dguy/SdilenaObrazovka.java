package com.gde.luzanky.dguy;

import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky} a {@link IDGuyScreenResources})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, DGuyScreenResources, DGuyGame>
{
	public SdilenaObrazovka(DGuyGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}
}
