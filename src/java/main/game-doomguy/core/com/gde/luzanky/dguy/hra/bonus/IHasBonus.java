package com.gde.luzanky.dguy.hra.bonus;

public interface IHasBonus
{
	boolean applyBonus(BonusAttr attr);
}
