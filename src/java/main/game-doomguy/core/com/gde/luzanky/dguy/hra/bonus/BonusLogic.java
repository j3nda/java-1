package com.gde.luzanky.dguy.hra.bonus;

import java.util.ArrayList;
import java.util.List;

import com.gde.luzanky.dguy.hra.bonus.BonusAttr.UseState;

public abstract class BonusLogic
implements IHasBonus
{
	private final List<BonusAttr> active = new ArrayList<>();
//	private final List<BonusAttr> notActive = new ArrayList<>(); // TODO: game/bonus: BonusAttr with cooldown

	protected List<BonusAttr> active()
	{
		return active;
	}

	@Override
	public boolean applyBonus(BonusAttr attr)
	{
		List<BonusAttr> bonusDomain = null;
		switch(attr.useType)
		{
			case ByPick:
			{
				attr.useState = UseState.ActiveAndUsing;
				bonusDomain = active;
				break;
			}
//			case ByUse: // TODO: game/bonus: ByUse
//			{
//				attr.useState = UseState.WaitingForActivation;
//				bonusDomain = notActive;
//				break;
//			}
			default:
				throw new RuntimeException("Invalid useType! {" + attr + "}");
		}
		switch(attr.applyType)
		{
			case Stacked:
			{
				// TODO: game/bonus: BonusAttr(stacked && useLimitMax)?how?
				applyBonusStack(attr, bonusDomain);
				break;
			}
			case Replaced:
			{
				applyBonusReplace(attr, bonusDomain);
				break;
			}
			default:
				throw new RuntimeException("Invalid applyType! {" + attr + "}");
		}
		bonusChanged(active); // TODO: game/bonus: notActive?
		return true;
	}

	private void applyBonusReplace(BonusAttr attr, List<BonusAttr> domain)
	{
		if (domain.isEmpty())
		{
			domain.add(attr);
		}
		else
		{
			List<Integer> toRemove = new ArrayList<>(domain.size());
			for(int i = 0; i < domain.size(); i++)
			{
				BonusAttr entry = domain.get(i);
				if (entry.getClass().equals(attr.getClass()))
				{
					toRemove.add(i);
				}
			}
			for(int toRemoveIndex : toRemove)
			{
				domain.remove(toRemoveIndex);
			}
			domain.add(attr);
		}
	}

	private void applyBonusStack(BonusAttr attr, List<BonusAttr> domain)
	{
		domain.add(attr);
	}

	// TODO: game/bonus: act(delta) ~ cooldown
	protected abstract void bonusChanged(List<BonusAttr> bonuses);
}
