package com.gde.luzanky.dguy.hra.player.gun;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.luzanky.dguy.hra.bonus.BonusAttr;
import com.gde.luzanky.dguy.hra.bonus.BonusLogic;
import com.gde.luzanky.dguy.hra.bonus.IHasBonus;

public class Gun
extends Image
implements IHasBonus
{
	private final BonusLogic bonusLogic;
	private final GunAttr attr;

	public Gun()
	{
		super(new Texture("tint/square16x16.png"));
		attr = new GunAttr(100, 100f, 10, Color.RED);
		bonusLogic = new BonusLogic()
		{
			@Override
			protected void bonusChanged(List<BonusAttr> bonuses)
			{
				for(BonusAttr bonus : bonuses)
				{
					if (bonus instanceof ProjectileColorBonus)
					{
						// TODO: game/bonus: projectile's color conflict vs gun(below)!
						attr.update((ProjectileColorBonus)bonus);
					}
					if (bonus instanceof GunBonus)
					{
						attr.update((GunBonus) bonus);
					}
				}
			}
		};
		setColor(attr.color);
	}

	public float range()
	{
		// TODO: hra/logika: range musi byt vetsi nez size P1! jinakt to nevypada dobre
		return attr.range;
	}

	public int damage()
	{
		// TODO: hra/logika: ruzne veliky damage v zavislosti na zbranich a abilitach hrace
		return attr.damage;
	}

	public int projectileSize()
	{
		// TODO: hra/logika: ruzne velikosti projektilu v zavislosti na zbrani
		return attr.size;
	}

	@Override
	public boolean applyBonus(BonusAttr attr)
	{
		if (!attr.canApply(this))
		{
			return false;
		}
		return bonusLogic.applyBonus(attr);
	}
}
