package com.gde.luzanky.dguy.hra.player;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.gde.luzanky.dguy.debug.DebugDrawCollision;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.arena.BaseArena;
import com.gde.luzanky.dguy.hra.arena.IArena;
import com.gde.luzanky.dguy.hra.bonus.BonusAttr;
import com.gde.luzanky.dguy.hra.bonus.BonusLogic;
import com.gde.luzanky.dguy.hra.collisions.CollisionBody;
import com.gde.luzanky.dguy.hra.player.gun.Gun;
import com.gde.luzanky.dguy.hra.projectiles.Projectile;

public abstract class Player
extends Image
implements IPlayer
{
	private float moveSpeed = 1f;
	protected final Vector2 direction;
	private Gun gun;
	private CollisionBody collisionBody;
	private final String name;
	private final int id;
	private static int idCounter = 0;
	private static final boolean debug = ObrazovkaHry.debug;
	private DebugDrawCollision debugDraw;
	private int hitpoints = 1000;
	private Label hitpointsLabel;
	private BaseArena arena;
	private final BonusLogic bonusLogic;
	private final PlayerAttr attr;

	public Player(String name)
	{
		super(
			new Texture("tint/square16x16.png")
		);
		this.id = idCounter++;
		this.direction = new Vector2();
		this.name = name;
		this.gun = new Gun();
		this.collisionBody = new CollisionBody(4);
		this.hitpointsLabel = new Label(
			"" + hitpoints,
			new Skin(Gdx.files.internal("skins/c64/uiskin.json"))
		);
		attr = new PlayerAttr();
		bonusLogic = new BonusLogic()
		{
			@Override
			protected void bonusChanged(List<BonusAttr> bonuses)
			{
				float moveSpeed = 1f;
				for(BonusAttr bonus : bonuses)
				{
					if (bonus instanceof PlayerSpeedBonus)
					{
						moveSpeed *= ((PlayerSpeedBonus)bonus).speed;
					}
				}
				attr.moveSpeedMultiplier = moveSpeed;
			}
		};
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		gun.act(delta);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		if (debug)
		{
			if (debugDraw == null)
			{
				debugDraw = new DebugDrawCollision(collisionBody);
			}
			debugDraw.draw(batch, parentAlpha);
		}
		gun.draw(batch, parentAlpha);
		hitpointsLabel.draw(batch, parentAlpha);
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		if (collisionBody != null)
		{
			updateCollisionBody();
		}
		if (gun == null)
		{
			return;
		}
		gun.setPosition(
			getX() + getOriginX(),
			getY() + getOriginY() - gun.getOriginY()
		);
		hitpointsLabel.setPosition(
			getX() + getOriginX(),
			getY() + getHeight() + 10
		);
		processArenaBehavior();
	}

	@Override
	protected void sizeChanged()
	{
		super.sizeChanged();
		setOrigin(
			getWidth() / 2f,
			getHeight() / 2f
		);
		if (collisionBody != null)
		{
			updateCollisionBody();
		}
		if (gun == null)
		{
			return;
		}
		float gunSize = (getWidth() / 2) + 10;
		float gunRotation = gun.getRotation();

		gun.setRotation(0);
		gun.setSize(gunSize, 10);
		gun.setOrigin(0, 5);
		gun.setRotation(gunRotation);

		hitpointsLabel.setPosition(
			getX() + getOriginX(),
			getY() + getHeight() + 10
		);
	}

	private void updateCollisionBody()
	{
		collisionBody.setPosition(getX(), getY());
		collisionBody.setOrigin(getOriginX(),getOriginY());
		collisionBody.setRotation(getRotation());

		collisionBody.updatePoint(0, 0, 0);//left-bottom
		collisionBody.updatePoint(1, getWidth(), 0);//right-bottom
		collisionBody.updatePoint(2, 0, getHeight());//left-top
		collisionBody.updatePoint(3, getWidth(), getHeight());//right-top
	}

	private void processArenaBehavior()
	{
		Stage stage = getStage();
		if (stage instanceof IArena)
		{
			((IArena)stage).processBehavior(this);
		}
	}

	public void fireProjectile()
	{
		Projectile projectile = new Projectile(this);
		getStage().addActor(projectile);

		Sound sfx = Gdx.audio.newSound(Gdx.files.internal("sounds/LittleRobotSoundFactory/hit-00__270327.mp3"));
		sfx.play();
//		sfx.dispose();

		Music mus = Gdx.audio.newMusic(Gdx.files.internal("sounds/LittleRobotSoundFactory/jingle-win-00__270333.mp3"));
		mus.play();
	}

	@Override
	public Gun gun()
	{
		return gun;
	}

	@Override
	public Vector2 direction()
	{
		return direction;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public int id()
	{
		return id;
	}

	@Override
	public CollisionBody body()
	{
		return collisionBody;
	}

	public void setSpeed(float multiplier)
	{
		moveSpeed = multiplier;
	}

	public float speed()
	{
		return moveSpeed * attr.moveSpeedMultiplier;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Player other = (Player) obj;
		if (id != other.id)
		{
			return false;
		}
		return true;
	}

	@Override
	public void hitpointsChanged(int delta)
	{
		hitpoints += delta;
		hitpointsLabel.setText("" + hitpoints);
	}

	@Override
	public int hitpoints()
	{
		return hitpoints;
	}

	@Override
	public BaseArena arena()
	{
		return (BaseArena)super.getStage();
	}

	@Override
	public boolean applyBonus(BonusAttr attr)
	{
		boolean applyGun = gun().applyBonus(attr);
		boolean applyPlayer = (attr.canApply(this) ? bonusLogic.applyBonus(attr) : false);

		return (applyGun || applyPlayer);
	}
}
