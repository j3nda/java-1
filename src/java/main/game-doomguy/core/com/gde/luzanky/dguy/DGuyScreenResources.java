package com.gde.luzanky.dguy;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.ScreenResources;
import com.gde.common.resources.CommonResources;
import com.gde.common.resources.GdeAssetManager;
import com.gde.common.resources.GdeFontsManager;
import com.gde.common.resources.IFontsManager;
import com.gde.luzanky.dguy.hra.GameConfiguration;

public class DGuyScreenResources
extends ScreenResources
{
	private final IFontsManager fontsManager;
	private final IAssetManager assetmanager;
	private GameConfiguration gameConfig;

	public DGuyScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		this.fontsManager = new GdeFontsManager();
		this.assetmanager = new GdeAssetManager();
//		PlayersNameManager getPlayersNameManager();//TODO: XHONZA/refactor: from: ppl/j3/games/lode --> common
	}

	public IFontsManager getFontsManager()
	{
		return fontsManager;
	}

	public IAssetManager getAssetManager()
	{
		return assetmanager;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		fontsManager.dispose();
		assetmanager.dispose();
	}

	public void setGameConfiguration(GameConfiguration gameConfig)
	{
		this.gameConfig = gameConfig;
	}

	public GameConfiguration getGameConfiguration()
	{
		return gameConfig;
	}

	// TODO: XHONZA/resources/refactor: createLabel, etc -> getLabelFactory() {box,dguy}
	public Label createLabel(String name)
	{
		return createLabel(name, 33);
	}

	public Label createLabel(String name, int fontSize)
	{
		return createLabel(name, fontSize, Align.center);
	}

	public Label createLabel(String name, int fontSize, Color color)
	{
		return createLabel(name, fontSize, color, Align.center);
	}

	public Label createLabel(String name, int fontSize, int align)
	{
		return createLabel(name, fontSize, Color.RED, align);
	}

	public Label createLabel(String name, int fontSize, Color color, int align)
	{
		Label label = new Label(
			name,
			fontsManager.getLabelStyle(CommonResources.Font.vt323, fontSize, color)
		);
		label.setAlignment(align);
		return label;
	}
}
