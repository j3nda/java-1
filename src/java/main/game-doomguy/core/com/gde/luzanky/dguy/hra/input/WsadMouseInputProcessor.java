package com.gde.luzanky.dguy.hra.input;

import java.util.Arrays;
import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.gde.common.inputs.KeyboardInputProcessor;

public abstract class WsadMouseInputProcessor
extends KeyboardInputProcessor
{
	private final Vector2 mouseVector = new Vector2();
	private final IWsadPosition player;
	private final IWsadHeight screen;
	private float moveMultiplier = 1f;
	private Vector2 moveMultiplierLimit;

	public WsadMouseInputProcessor(IWsadPosition pos, IWsadHeight screen, Vector2 multiplierLimit)
	{
		super(new HashSet<>(Arrays.asList(
			Input.Keys.W,
			Input.Keys.S,
			Input.Keys.A,
			Input.Keys.D
		)));
		this.player = pos;
		this.screen = screen;
		this.moveMultiplierLimit = multiplierLimit;
	}

	protected abstract void mouseAim(float degrees);
	protected abstract void wsadMoveLeft(float multiplier);
	protected abstract void wsadMoveRight(float multiplier);
	protected abstract void wsadMoveUp(float multiplier);
	protected abstract void wsadMoveDown(float multiplier);
	protected abstract void resetMultiplier(float multiplier);

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		if (player == null || screen == null)
		{
			return super.mouseMoved(screenX, screenY);
		}
		// thx, chat-gpt ;)
		mouseVector.set(
			screenX - (player.getX() + player.getOriginX()),
			screen.getHeight() - screenY - (player.getY() + player.getOriginY())
		);
		mouseAim(mouseVector.angleDeg());
		return true;
	}

	@Override
	protected boolean keyTyped(char character, boolean keyProcess)
	{
		if (player == null || !keyProcess)
		{
			return super.keyTyped(character);
		}
		if (checkForKeyCombinations())
		{
			moveMultiplier += Gdx.graphics.getDeltaTime();
			if (moveMultiplier > moveMultiplierLimit.y)
			{
				moveMultiplier = moveMultiplierLimit.y;
			}
		}
		boolean typed = false;
		if (pressedKeys.contains(Input.Keys.W))
		{
			wsadMoveUp(moveMultiplier);
			mouseMoved(Gdx.input.getX(), Gdx.input.getY());
			typed = true;
		}
		if (pressedKeys.contains(Input.Keys.S))
		{
			wsadMoveDown(moveMultiplier);
			mouseMoved(Gdx.input.getX(), Gdx.input.getY());
			typed = true;
		}
		if (pressedKeys.contains(Input.Keys.A))
		{
			wsadMoveLeft(moveMultiplier);
			mouseMoved(Gdx.input.getX(), Gdx.input.getY());
			typed = true;
		}
		if (pressedKeys.contains(Input.Keys.D))
		{
			wsadMoveRight(moveMultiplier);
			mouseMoved(Gdx.input.getX(), Gdx.input.getY());
			typed = true;
		}
		return typed;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		super.keyUp(keycode);
		if (!checkForKeyCombinations())
		{
			moveMultiplier = moveMultiplierLimit.x;
			resetMultiplier(moveMultiplier);
		}
		return true;
	}

	public void setMultiplierMax(float limit)
	{
		moveMultiplierLimit.y = limit;
	}

	public interface IWsadPosition
	{
		void setX(float x);
		void setY(float y);
		float getX();
		float getY();
		float getOriginX();
		float getOriginY();
	}

	public interface IWsadHeight
	{
		int getHeight();
	}
}
