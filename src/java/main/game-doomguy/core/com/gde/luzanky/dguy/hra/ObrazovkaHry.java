package com.gde.luzanky.dguy.hra;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gde.common.exceptions.NotImplementedException;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.screens.IScreenResources;
import com.gde.common.resources.CommonResources;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.dguy.DGuyGame;
import com.gde.luzanky.dguy.DGuyScreenResources;
import com.gde.luzanky.dguy.SdilenaObrazovka;
import com.gde.luzanky.dguy.TypObrazovky;
import com.gde.luzanky.dguy.debug.DebugDraw;
import com.gde.luzanky.dguy.hra.arena.BaseArena;
import com.gde.luzanky.dguy.hra.arena.BounceArena;
import com.gde.luzanky.dguy.hra.arena.IArenaTimeLeft;
import com.gde.luzanky.dguy.hra.arena.TeleportArena;
import com.gde.luzanky.dguy.hra.bonus.BonusItem;
import com.gde.luzanky.dguy.hra.input.WsadMouseInputProcessor;
import com.gde.luzanky.dguy.hra.input.WsadMouseInputProcessor.IWsadHeight;
import com.gde.luzanky.dguy.hra.npc.HonzaNpc;
import com.gde.luzanky.dguy.hra.npc.Npc;
import com.gde.luzanky.dguy.hra.obstacles.ObstacleItem;
import com.gde.luzanky.dguy.hra.player.DoomGuy;
import com.gde.luzanky.dguy.hra.player.Player;
import com.gde.luzanky.dguy.hra.player.PlayerSpeedBonus;
import com.gde.luzanky.dguy.hra.player.gun.GunBonus;
import com.gde.luzanky.dguy.hra.player.gun.GunBonusJson;
import com.gde.luzanky.dguy.hra.player.gun.ProjectileColorBonus;
import com.gde.luzanky.dguy.menu.ObrazovkaMenu.ControlsEnum;

/** herni obrazovka, zde se odehrava nase hra doom guy! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements IWsadHeight, IArenaTimeLeft
{
	private List<Player> npc;
	private Stage uiStageBg;
	private DoomGuy P1;
	private WsadMouseInputProcessor wsadProcessor;
	private Label timeLeftLabel;
	private Stage uiStageFg;
	private enum JoystickType { Move, Aim }
	public static final boolean debug = DGuyGame.isDebug();

	public ObrazovkaHry(DGuyGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public void show()
	{
		uiStageBg = new Stage();
		uiStageFg = new Stage();
		super.show();

		createUI(resources.getGameConfiguration().controls);
		createGame();
		setInputProcessor(getStage());
	}

	@Override
	protected void createInputProcessors(List<InputProcessor> inputs)
	{
		super.createInputProcessors(inputs);

		if (resources.getGameConfiguration().controls == ControlsEnum.Joystick)
		{
			inputs.add(uiStageBg);
		}
		else
		if (resources.getGameConfiguration().controls == ControlsEnum.WSAD)
		{
			inputs.add(uiStageBg);
			inputs.add(wsadProcessor = new WsadMouseInputProcessor(P1, this, new Vector2(1f, 3f))
			{
				@Override
				protected void mouseAim(float degrees)
				{
					P1.gun().setRotation(degrees);
				}
				@Override
				protected void wsadMoveUp(float multiplier)
				{
					P1.setSpeed(multiplier);
					P1.setY(P1.getY() + (1 * multiplier));
				}
				@Override
				protected void wsadMoveDown(float multiplier)
				{
					P1.setSpeed(multiplier);
					P1.setY(P1.getY() - (1 * multiplier));
				}
				@Override
				protected void wsadMoveLeft(float multiplier)
				{
					P1.setSpeed(multiplier);
					P1.setX(P1.getX() - (1 * multiplier));
				}
				@Override
				protected void wsadMoveRight(float multiplier)
				{
					P1.setSpeed(multiplier);
					P1.setX(P1.getX() + (1 * multiplier));
				}
				@Override
				protected void resetMultiplier(float multiplier)
				{
					P1.setSpeed(multiplier);
				}
				@Override
				public boolean touchDown(int screenX, int screenY, int pointer, int button)
				{
					if (button == Input.Buttons.LEFT)
					{
						P1.fireProjectile();
						return true;
					}
					return super.touchDown(screenX, screenY, pointer, button);
				}
			});
		}
	}

	@Override
	public void updateTime(String time)
	{
		GlyphLayout glyph = timeLeftLabel.getGlyphLayout();
		glyph.setText(
			timeLeftLabel.getStyle().font,
			time
		);
		timeLeftLabel.setText(time);
		timeLeftLabel.setPosition(
			getWidth() / 2f,
			getHeight() - glyph.height,
			Align.center
		);
	}

	private void createUI(ControlsEnum type)
	{
		timeLeftLabel = new Label(
			"0:0",
			resources.getFontsManager().getLabelStyle(
				CommonResources.Font.vt323,
				33,
				Color.WHITE
			)
		);
		switch(type)
		{
			case Joystick:
			{
				createUiJoystick();
				break;
			}
			case WSAD:
			{
				createUiWSAD();
				break;
			}
			default:
				throw new RuntimeException("Invalid controls!");
		}
		uiStageFg.addActor(timeLeftLabel);
	}

	private void createUiJoystick()
	{
		Table wrapper = new Table();

		Table moveWrapper = new Table();
		moveWrapper.add(createFireButton()).growX().height(111).align(Align.bottom);
		moveWrapper.row();
		moveWrapper.add(createJoystick(JoystickType.Move)).align(Align.top);
		moveWrapper.row();
		moveWrapper.debug();

		wrapper.add(moveWrapper).align(Align.bottom);
		wrapper.add(new Actor()).grow();
		wrapper.add(createJoystick(JoystickType.Aim)).align(Align.bottom);

		wrapper.row();
		wrapper.setSize(getWidth(), getHeight());
		wrapper.debug();

		uiStageBg.addActor(wrapper);
	}

	private void createUiWSAD()
	{
		// TODO: WSAD + mouse + vizual, ze jsem to zmacknul

		Table wrapper = new Table();

		Table moveWrapper = new Table();
		moveWrapper.add(createFireButton()).growX().height(111).align(Align.bottom);
		moveWrapper.row();
		moveWrapper.add(createJoystick(JoystickType.Move)).align(Align.top);
		moveWrapper.row();
		moveWrapper.debug();

		wrapper.add(moveWrapper).align(Align.bottom);
		wrapper.add(new Actor()).grow();
		wrapper.add(createJoystick(JoystickType.Aim)).align(Align.bottom);

		wrapper.row();
		wrapper.setSize(getWidth(), getHeight());
		wrapper.debug();

		uiStageBg.addActor(wrapper);
	}

	private Actor createFireButton()
	{
		Skin skin = new Skin(Gdx.files.internal("skins/c64/uiskin.json"));

		TextButton button = new TextButton("fire", skin);
		button.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				P1.fireProjectile();
			}
		});

		return button;
	}

	private Touchpad createJoystick(final JoystickType type)
	{
		Skin skin = new Skin(Gdx.files.internal("skins/c64/uiskin.json"));
		Touchpad touchpad = new Touchpad(10, skin); // TODO: deadzoneRadius
		touchpad.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				joystickChanged(type, (Touchpad)actor);
			}
		});
		touchpad.addListener(new EventListener()
		{
			@Override
			public boolean handle(Event event)
			{
				System.out.println(event);
				return false;
			}
		});
		return touchpad;
	}

	private void joystickChanged(JoystickType type, Touchpad touchpad)
	{
		switch (type)
		{
			case Move:
			{
		        float deltaX = touchpad.getKnobPercentX();
		        float deltaY = touchpad.getKnobPercentY();
		        P1.setPosition(
		        	P1.getX() + deltaX,
		        	P1.getY() + deltaY
	        	);
				break;
			}
			case Aim:
			{
		        float deltaX = touchpad.getKnobPercentX();
		        float deltaY = touchpad.getKnobPercentY();
		        double angle = Math.atan2(deltaY, deltaX);

		        P1.gun().setRotation((float) Math.toDegrees(angle));
				break;
			}
			default:
				break;
		}
	}

	private void createGame()
	{
		P1 = createDoomGuy();
		getStage().addActor(P1);

		npc = new ArrayList<Player>();
		for (int i = 0; i < 2; i++)
		{
			Npc enemy = createNpcEnemy();
			npc.add(enemy);
			getStage().addActor(enemy);
		}

		// random bonuses... for just now debug shlitz!
		getStage().addAction(
			Actions.forever(
				createGameBonuses()
			)
		);

		ObstacleItem prekazka = new ObstacleItem();
		prekazka.setPosition(
			RandomUtils.nextInt(50, 777),
			RandomUtils.nextInt(50, 555)
		);
		getStage().addActor(prekazka);
	}

	private Action createGameBonuses()
	{
		// random bonuses... for just now debug shlitz!
		return Actions.parallel(
			Actions.delay(
				RandomUtils.nextFloat(5f,  10f),
				Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						Color color = Palette.Hardware.Commodore.C64.Default()[RandomUtils.nextInt(1, Palette.Hardware.Commodore.C64.Default().length) - 1];
//						BonusItem bonus = new BonusItem(new GunBonus(9999, 100f, 20, color));
//						BonusItem bonus = new BonusItem(
//							new GunBonus(
//								Gdx.files.internal(
//									"gun" + RandomUtils.nextInt(1, 2) + ".json"
//								).readString()
//							)
//						);
						BonusItem bonus = new BonusItem(
							new GunBonus(
								new Json().fromJson(
									GunBonusJson.class,
									Gdx.files.internal(
										"gun" + RandomUtils.nextInt(1, 2) + ".json"
									)
								)
							)
						);
						bonus.setColor(color);
						bonus.setPosition(
							RandomUtils.nextInt(50, Gdx.graphics.getWidth() - 50),
							RandomUtils.nextInt(50, Gdx.graphics.getHeight() - 50)
						);
						getStage().addActor(bonus);
					}
				})
			),
			Actions.delay(
				RandomUtils.nextFloat(5f,  10f),
				Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						Color color = Palette.Hardware.ZxSpectrum.Default()[RandomUtils.nextInt(1, Palette.Hardware.ZxSpectrum.Default().length) - 1];
						BonusItem bonus = new BonusItem(new PlayerSpeedBonus(RandomUtils.nextFloat(0.5f, 3f)));
						bonus.setColor(color);
						bonus.setPosition(
							RandomUtils.nextInt(50, Gdx.graphics.getWidth() - 50),
							RandomUtils.nextInt(50, Gdx.graphics.getHeight() - 50)
						);
						getStage().addActor(bonus);
					}
				})
			),
			Actions.delay(
				RandomUtils.nextFloat(5f,  10f),
				Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						Color color = Palette.Hardware.ZxSpectrum.Default()[RandomUtils.nextInt(1, Palette.Hardware.ZxSpectrum.Default().length) - 1];
						BonusItem bonus = new BonusItem(new ProjectileColorBonus(color));
						bonus.setColor(color);
						bonus.setPosition(
							RandomUtils.nextInt(50, Gdx.graphics.getWidth() - 50),
							RandomUtils.nextInt(50, Gdx.graphics.getHeight() - 50)
						);
						getStage().addActor(bonus);
					}
				})
			)
		);
	}

	private Npc createNpcEnemy()
	{
		int smerX = RandomUtils.nextInt(0, 10) % 2 == 0 ? +1 : -1;
		int smerY = RandomUtils.nextInt(0, 10) % 2 == 0 ? +1 : -1;
		Npc npc = new HonzaNpc();
//		Npc npc = new RomanNpc();
//		new Vector2(
//			RandomUtils.nextInt(1f, 2f) * smerX,
//			RandomUtils.nextInt(0.01f, 0.02f) * smerY
//		));
		npc.setSize(100, 100);
		npc.setPosition(
			RandomUtils.nextInt(50, Gdx.graphics.getWidth() - 50),
			RandomUtils.nextInt(50, Gdx.graphics.getHeight() - 50)
		);
		npc.setColor(
			RandomUtils.nextFloat(0, 1f),
			RandomUtils.nextFloat(0, 1f),
			RandomUtils.nextFloat(0, 1f),
			1f
		);
		return npc;
	}

	private DoomGuy createDoomGuy()
	{
		int smerX = RandomUtils.nextInt(0, 10) % 2 == 0 ? +1 : -1;
		int smerY = RandomUtils.nextInt(0, 10) % 2 == 0 ? +1 : -1;
		DoomGuy doomGuy = new DoomGuy(
			Vector2.Zero,
			"P1"
		);
//		new Vector2(
//			RandomUtils.nextInt(1f, 2f) * smerX,
//			RandomUtils.nextInt(0.01f, 0.02f) * smerY
//		));
		doomGuy.setSize(100, 100);
		doomGuy.setPosition(
			RandomUtils.nextInt(50, Gdx.graphics.getWidth() - 50),
			RandomUtils.nextInt(50, Gdx.graphics.getHeight() - 50)
		);
		doomGuy.setColor(
			RandomUtils.nextFloat(0, 1f),
			RandomUtils.nextFloat(0, 1f),
			RandomUtils.nextFloat(0, 1f),
			1f
		);

		doomGuy.setRotation(120);

		return doomGuy;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	@Override
	protected void renderScreen(float delta)
	{
		uiStageBg.act(delta);
		uiStageBg.draw();

		if (wsadProcessor != null)
		{
			wsadProcessor.act(delta);
		}
		getStage().act(delta);
		getStage().draw();

		uiStageFg.act(delta);
		uiStageFg.draw();
	}

	/** will create a stage as {@link BaseArena}, which holds the whole game! */
	@Override
	protected Stage createStage(IScreenResources resources)
	{
		DGuyScreenResources res = (DGuyScreenResources) resources;
//		res.getViewport().setWorldSize(getWidth(), getHeight());

// TODO: fromGULA
// ./camera/Cameraman.java:                camera             = new OrthographicCamera(viewportWidth, viewportHeight);
// ./GraphicObject.java:                      getY() - offset > cam.position.y + cam.zoom * cam.viewportHeight / 2 // out of screen top


		Vector2 screenSize = new Vector2(512, 384); // 1024x768 (div 2 => 512x384)
		Vector2 worldSize = new Vector2(1024, 768);

		OrthographicCamera gameCamera = new OrthographicCamera(screenSize.x, screenSize.y);

		ScreenViewport gameViewport = new ScreenViewport(gameCamera);
		gameViewport.setWorldSize(worldSize.x, worldSize.y);

		gameViewport.setScreenSize((int)screenSize.x, (int)screenSize.y);
		gameViewport.setScreenPosition(0, 0);
//		gameViewport.setScreenBounds(getHeight(), getHeight(), getWidth(), getHeight());

		gameViewport.setUnitsPerPixel(1f);


//		Camera camera = createCamera(
//			Gdx.graphics.getWidth(),
//			Gdx.graphics.getHeight()
//		);
//		screenResources = createScreenResources(
//			camera,
//			new ScalingViewport(
//				Scaling.stretch,
//				camera.viewportWidth,
//				camera.viewportHeight,
//				camera
//			),
//			new SpriteBatch()
//		);


//		gameViewport = res.getViewport();

		Class<?> arenaClazz = res.getGameConfiguration().arenaClazz;
		if (BounceArena.class.isAssignableFrom(arenaClazz))
		{
			return new BounceArena(
				gameViewport,
				res.getBatch(),
				this,
				res.getGameConfiguration()
			);
		}
		if (TeleportArena.class.isAssignableFrom(arenaClazz))
		{
			return new TeleportArena(
				gameViewport,
				res.getBatch(),
				this,
				res.getGameConfiguration()
			);
		}
		throw new NotImplementedException("Invalid arena!");
	}

	@Override
	public void dispose()
	{
		super.dispose();
		DebugDraw.dispose();
	}

	public void gameOver(Set<Player> winners)
	{
		System.out.println("GAME-OVER");
		System.out.println(winners);
		Gdx.app.exit();
	}
}

// TODO: XHONZA/notes: camera, controllers, etc.
// -- https://copyprogramming.com/howto/libgdx-move-camera-with-touch
// -- https://www.youtube.com/watch?v=l2-1bk7-FbA
// -- https://www.youtube.com/watch?v=z4Vqkp_ve3I
// -- https://github.com/libgdx/gdx-controllers
// -- https://gamefromscratch.com/libgdx-tutorial-part-14-gamepad-support/
// -- https://gamedev.stackexchange.com/questions/199074/is-there-an-algorithm-for-a-joystick-class-that-happens-to-be-engine-framework-i

// doom melt / screen transition
// -- https://youtu.be/lUsCXSNhHmI?si=63wc9t8dJ-dmIQLu&t=115