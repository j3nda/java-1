package com.gde.luzanky.dguy.hra.arena;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.luzanky.dguy.hra.GameConfiguration;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.arena.abilities.ITeleportAbility;
import com.gde.luzanky.dguy.hra.player.Player;

public class TeleportArena
extends BaseArena
implements ITeleportAbility
{
	public TeleportArena(Viewport viewport, Batch batch, ObrazovkaHry game, GameConfiguration gameConfig)
	{
		super(viewport, batch, game, gameConfig);
	}

	@Override
	public void processBehavior(Player player)
	{
		teleport(player);
	}

	@Override
	public void teleport(Player player)
	{
		if (player.getX() + player.getOriginX() < 0)
		{
			player.setX(getWidth() - player.getOriginX());
		}
		if (player.getX() + player.getOriginX() > getWidth())
		{
			player.setX(-1 * player.getOriginX());
		}
		if (player.getY() + player.getOriginY() < 0)
		{
			player.setY(getHeight() - player.getOriginY());
		}
		if (player.getY() + player.getOriginY() > getHeight())
		{
			player.setY(-1 * player.getOriginY());
		}
		// TODO: player's gun-shots
	}
}
