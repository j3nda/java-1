package com.gde.luzanky.dguy.hra.player;

import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.dguy.hra.IHasId;
import com.gde.luzanky.dguy.hra.arena.BaseArena;
import com.gde.luzanky.dguy.hra.bonus.IHasBonus;
import com.gde.luzanky.dguy.hra.collisions.IHasCollisionBody;
import com.gde.luzanky.dguy.hra.input.WsadMouseInputProcessor.IWsadPosition;
import com.gde.luzanky.dguy.hra.player.gun.Gun;

public interface IPlayer
extends IHasId, IHasCollisionBody, IWsadPosition, IHasBonus
{
	Gun gun();
	Vector2 direction();
	String name();

	int hitpoints();
	void hitpointsChanged(int delta);

	BaseArena arena();
}
