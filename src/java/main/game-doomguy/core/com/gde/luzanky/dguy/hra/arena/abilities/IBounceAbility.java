package com.gde.luzanky.dguy.hra.arena.abilities;

import com.gde.luzanky.dguy.hra.player.Player;

public interface IBounceAbility
{
	void bounce(Player player);
}
