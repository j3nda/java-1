package com.gde.luzanky.dguy.hra.player.gun;

import com.badlogic.gdx.graphics.Color;
import com.gde.luzanky.dguy.hra.bonus.BonusAttr;

public class ProjectileColorBonus
extends BonusAttr
{
	public Color color;

	public ProjectileColorBonus(Color color)
	{
		super(ApplyType.Replaced, Gun.class);
		this.color = color;
	}
}
