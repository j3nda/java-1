package com.gde.luzanky.dguy.hra.npc;

import com.gde.luzanky.dguy.hra.player.Player;

public abstract class Npc
extends Player
{
	public Npc(String name)
	{
		super(name);
	}

	@Override
	public void act(float delta)
	{
		setX(getX() + direction.x);
		setY(getY() + direction.y);

		super.act(delta);
	}
}
