package com.gde.luzanky.dguy.hra.player;

import com.badlogic.gdx.math.Vector2;

public class DoomGuy
extends Player
{
	public DoomGuy(Vector2 direction, String name)
	{
		super(name);
		this.direction.set(direction);
	}
}
