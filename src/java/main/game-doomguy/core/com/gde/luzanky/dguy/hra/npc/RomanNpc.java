package com.gde.luzanky.dguy.hra.npc;

import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.dguy.hra.player.Player;

public class RomanNpc
extends Npc
{
	private float lookUpTarget;
	private float lookUpTargetMax = 3f;

	public RomanNpc()
	{
		super("Roman");
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		lookUpTarget += delta;
		if (lookUpTarget >= lookUpTargetMax)
		{
			lookUpTarget = 0;
			direction().set(getTargetDirection());
		}
	}

	private Vector2 getTargetDirection()
	{
		Vector2 me = new Vector2(
			getX() + getOriginX(),
			getY() + getOriginY()
		);
		Vector2 tmp = new Vector2();
		Vector2 ret = new Vector2();
		float currentDistance = Float.MAX_VALUE;
		for(Player enemy : arena().players())
		{
			if (enemy.id() == id())
			{
				continue;
			}
			tmp.set(
				enemy.getX() + enemy.getOriginX(),
				enemy.getY() + enemy.getOriginY()
			);
			float distance = tmp.dst(me);
			if (distance < currentDistance)
			{
				currentDistance = distance;
				ret.set(tmp);
			}
		}
		return ret.nor();
	}
}
