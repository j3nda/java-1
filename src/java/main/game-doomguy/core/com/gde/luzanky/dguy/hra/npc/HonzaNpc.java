package com.gde.luzanky.dguy.hra.npc;

import com.gde.common.utils.RandomUtils;

public class HonzaNpc
extends Npc
{
	private float currentDirection;
	private float currentDirectionLimit = 2f;
	private float currentGunRotation;
	private float currentGunRotationLimit = 1f;
	private float currentGunFire;
	private float currentGunFireLimit = 0.5f;

	public HonzaNpc()
	{
		super("Honza");
		currentDirection = currentDirectionLimit;
	}

	@Override
	public void act(float delta)
	{
		currentDirection += delta;
		if (currentDirection >= currentDirectionLimit)
		{
			currentDirection = 0;
			direction.set(
				RandomUtils.nextFloat(-1, +1),
				RandomUtils.nextFloat(-1, +1)
			);
		}
		currentGunRotation += delta;
		if (currentGunRotation >= currentGunRotationLimit)
		{
			currentGunRotation = 0;
			gun().setRotation(
				RandomUtils.nextFloat(0, 359)
			);
		}
		currentGunFire += delta;
		if (currentGunFire >= currentGunFireLimit)
		{
			currentGunFire = 0;
			fireProjectile();
		}
		super.act(delta);
	}
}
