package com.gde.luzanky.dguy.hra.projectiles;

import com.gde.luzanky.dguy.hra.IHasId;
import com.gde.luzanky.dguy.hra.collisions.IHasCollisionBody;

public interface IProjectile
extends IHasId, IHasCollisionBody
{

}
