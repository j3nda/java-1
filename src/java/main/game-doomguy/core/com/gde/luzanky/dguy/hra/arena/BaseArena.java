package com.gde.luzanky.dguy.hra.arena;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.luzanky.dguy.hra.GameConfiguration;
import com.gde.luzanky.dguy.hra.ObrazovkaHry;
import com.gde.luzanky.dguy.hra.bonus.BonusItem;
import com.gde.luzanky.dguy.hra.collisions.CollisionChecker;
import com.gde.luzanky.dguy.hra.collisions.CollisionEntry;
import com.gde.luzanky.dguy.hra.obstacles.ObstacleItem;
import com.gde.luzanky.dguy.hra.player.DoomGuy;
import com.gde.luzanky.dguy.hra.player.Player;
import com.gde.luzanky.dguy.hra.projectiles.Projectile;

// TODO: see: [gula]com.sjet.gula.graphics.objects.GameStage.java
// - public Vector2 stageToScreenCoordinates (Vector2 stageCoords);/** Transforms the stage coordinates to screen coordinates. */
public abstract class BaseArena
extends Stage
implements IArena
{
	private CollisionChecker collissionChecker;
	private float collisionTime;
	private float collisionLimit = 0.10f;
	private final Set<Player> players = new HashSet<>();
	private final Set<Player> playersToRemove = new HashSet<>();
	private final Map<Integer, Set<Projectile>> playersProjectiles = new HashMap<>();
	private final Set<Projectile> projectiles = new HashSet<>();
	private final ObrazovkaHry game;
	protected final GameConfiguration gameConfig;
	private float timeLeftTime = 99;
	private int timeLeft;
	private Set<BonusItem> bonuses = new HashSet<>();
	private Set<ObstacleItem> obstacles = new HashSet<>();
	private CollisionProcessor collissionProcessor;

	BaseArena(Viewport viewport, Batch batch, ObrazovkaHry game, GameConfiguration gameConfig)
	{
		super(viewport, batch);
		collissionChecker = new CollisionChecker(this);
		collissionProcessor = createCollisionProcessor();
		this.game = game;
		this.gameConfig = gameConfig;
		this.timeLeft = gameConfig.arenaTimeLeft;
	}

	protected CollisionProcessor createCollisionProcessor()
	{
		return new CollisionProcessor();
	}

	@Override
	public void addActor(Actor actor)
	{
		super.addActor(actor);
		if (actor instanceof Player)
		{
			Player player = (Player) actor;
			players.add(player);
			playersProjectiles.put(player.id(), new HashSet<>());
		}
		if (actor instanceof Projectile)
		{
			Projectile projectile = (Projectile) actor;
			projectiles.add(projectile);
			playersProjectiles.get(projectile.parentId()).add(projectile);
		}
		if (actor instanceof BonusItem)
		{
			BonusItem bonus = (BonusItem) actor;
			bonuses.add(bonus);
		}
		if (actor instanceof ObstacleItem)
		{
			ObstacleItem obstacle = (ObstacleItem) actor;
			obstacles.add(obstacle);
		}
	}

	@Override
	public void unfocus(Actor actor)
	{
		super.unfocus(actor);
		if (actor instanceof Player)
		{
			Player player = (Player) actor;
			players.remove(player);
			playersProjectiles.remove(player.id());
		}
		if (actor instanceof Projectile)
		{
			Projectile projectile = (Projectile) actor;
			projectiles.remove(projectile);
			if (playersProjectiles.containsKey(projectile.parentId()))
			{
				playersProjectiles.get(projectile.parentId()).remove(projectile);
			}
		}
		if (actor instanceof BonusItem)
		{
			BonusItem bonus = (BonusItem) actor;
			bonuses.remove(bonus);
		}
		if (actor instanceof ObstacleItem)
		{
			ObstacleItem obstacle = (ObstacleItem) actor;
			obstacles.remove(obstacle);
		}
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		collisionTime += delta;
		if (collisionTime >= collisionLimit)
		{
			collisionTime = 0f;
			processCollisions();
			processWinConditions();
		}
		timeLeftTime += delta;
		if (timeLeftTime >= 1f)
		{
			timeLeftTime = 0;
			timeLeft--;
			game.updateTime(timeLeft + "s");
		}
	}

	private void processWinConditions()
	{
		playersToRemove.clear();
		for(Player player : players)
		{
			if (player.hitpoints() <= 0)
			{
				playersToRemove.add(player);
			}
		}
		for(Player player : playersToRemove)
		{
			// remove player from stage inc. from Set<Player>
			player.remove();

			// if playerToRemove is me ~ DoomGuy!
			if (player instanceof DoomGuy)
			{
				game.gameOver(players);
				return;
			}
		}
		if (players.size() == 1)
		{
			game.gameOver(players);
		}
		if (timeLeft <= 0)
		{
			players.clear();
			game.gameOver(players);
		}
	}

	protected void processCollisions()
	{
		for(CollisionEntry entry : collissionChecker.process())
		{

			if (entry.source() instanceof Player && entry.target() instanceof Player) // Player x Player
			{
				collissionProcessor.player_x_player(
					(Player) entry.source(),
					(Player) entry.target()
				);
			}
			if (entry.source() instanceof Projectile && entry.target() instanceof Projectile) // Projectile x Projectile
			{
				collissionProcessor.projectile_x_projectile(
					(Projectile) entry.source(),
					(Projectile) entry.target()
				);
			}
			if (entry.source() instanceof Projectile && entry.target() instanceof Player) // Projectile x Player
			{
				collissionProcessor.projectile_x_player(
					(Projectile) entry.source(),
					(Player) entry.target()
				);
			}
			if (entry.source() instanceof Player && entry.target() instanceof BonusItem) // Player x Bonus
			{
				collissionProcessor.player_x_bonus(
					(Player) entry.source(),
					(BonusItem) entry.target()
				);
			}
			if (entry.source() instanceof ObstacleItem && entry.target() instanceof Player) // Obstacle x Player
			{
				collissionProcessor.obstacle_x_player(
					(ObstacleItem) entry.source(),
					(Player) entry.target()
				);
			}
			if (entry.source() instanceof ObstacleItem && entry.target() instanceof Projectile) // Obstacle x Projectile
			{
				collissionProcessor.obstacle_x_projectile(
					(ObstacleItem) entry.source(),
					(Projectile) entry.target()
				);
			}
			if (entry.source() instanceof ObstacleItem && entry.target() instanceof BonusItem) // Obstacle x Bonus
			{
				collissionProcessor.obstacle_x_bonus(
					(ObstacleItem) entry.source(),
					(BonusItem) entry.target()
				);
			}
		}
	}

	public Set<Player> players()
	{
		return players;
	}

	public Set<Projectile> projectiles()
	{
		return projectiles;
	}

	public Set<Projectile> projectiles(int playerId)
	{
		return playersProjectiles.get(playerId);
	}

	public Set<BonusItem> bonuses()
	{
		return bonuses;
	}

	public Set<ObstacleItem> obstacles()
	{
		return obstacles;
	}

	protected class CollisionProcessor
	{
		public void player_x_player(Player p1, Player npc)
		{
			System.out.println("collision[P-P]: "
				+ p1.name() + "[" + p1.id() + "]"
				+ " x "
				+ npc.name() + "[" + npc.id() + "]"
			);
		}

		public void projectile_x_projectile(Projectile p1, Projectile p2)
		{
			System.out.println("collision[o-o]: "
				+ "projectile[" + p1.id() + "@" + p1.parentId() + "]"
				+ " x "
				+  "projectile[" + p2.id() + "@" + p2.parentId() + "]"
			);
		}

		public void projectile_x_player(Projectile projectile, Player player)
		{
			System.out.println("collision[o-P]: "
				+ "projectile[" + projectile.id() + "@" + projectile.parentId() + "]"
				+ " x "
				+ player.name()+ "[" + player.id() + "]"
			);
			player.hitpointsChanged(-1 * projectile.damage());
			projectile.remove();
		}

		public void player_x_bonus(Player player, BonusItem bonus)
		{
			System.out.println("collision[P-B]: "
				+ player.name() + "[" + player.id() + "]"
				+ " x "
				+ /*npc.name() +*/ "[" + bonus.id() + "]"
			);
			if (player.applyBonus(bonus.attr()))
			{
				bonus.remove();
			}
		}

		public void obstacle_x_player(ObstacleItem obstacle, Player player)
		{
			System.out.println("collision[x-P]: "
				+ "obstacle[" + obstacle.id() + "]"
				+ " x "
				+ player.name() + "[" + player.id() + "]"
			);
		}

		public void obstacle_x_projectile(ObstacleItem obstacle, Projectile projectile)
		{
			System.out.println("collision[x-o]: "
				+ "obstacle[" + obstacle.id() + "]"
				+ " x "
				+  "projectile[" + projectile.id() + "@" + projectile.parentId() + "]"
			);
		}

		public void obstacle_x_bonus(ObstacleItem obstacle, BonusItem bonus)
		{
			System.out.println("collision[x-B]: "
				+ "obstacle[" + obstacle.id() + "]"
				+ " x "
				+  "bonus[" + bonus.id() + "]"
			);
		}
	}
}
