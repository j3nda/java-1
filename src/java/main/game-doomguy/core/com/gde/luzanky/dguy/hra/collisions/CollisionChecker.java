package com.gde.luzanky.dguy.hra.collisions;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.math.Intersector;
import com.gde.luzanky.dguy.hra.arena.BaseArena;
import com.gde.luzanky.dguy.hra.bonus.BonusItem;
import com.gde.luzanky.dguy.hra.obstacles.ObstacleItem;
import com.gde.luzanky.dguy.hra.player.Player;
import com.gde.luzanky.dguy.hra.projectiles.Projectile;

public class CollisionChecker
{
	private final Set<CollisionEntry> collisions;
	private final BaseArena arena;

	public CollisionChecker(BaseArena arena)
	{
		this.arena = arena;
		this.collisions = new HashSet<>();
	}

	public Set<CollisionEntry> process()
	{
		collisions.clear();

		processPlayers(arena.players());
		processObstacles(arena.obstacles());

		return collisions;
	}

	private void processPlayers(Set<Player> players)
	{
		CollisionEntry col = null;
		for(Player p1 : players)
		{
			for(Player npc : players)
			{
				if (p1.id() == npc.id())
				{
					continue;
				}
				col = processCollision(p1, npc);
				if (col != null)
				{
					collisions.add(col);
				}

				// P1's projectiles vs NPC
				for(Projectile p : arena.projectiles(p1.id()))
				{
					// TODO: hra/logika: ??muzu zasahnout vlastnim projektilem sam sebe??
					if (p.parentId() == npc.id())
					{
						continue;
					}
					col = processCollision(p, npc);
					if (col != null)
					{
						collisions.add(col);
					}
				}
			}
			for(BonusItem bonus : arena.bonuses())
			{
				if (!bonus.isVisible())
				{
					return;
				}
				col = processCollision(p1, bonus);
				if (col != null)
				{
					collisions.add(col);
				}
			}
		}
	}

	private void processProjectiles(Set<Projectile> projectiles)
	{
		for(Projectile p1 : projectiles)
		{
			for(Projectile p2 : projectiles)
			{
				// TODO: hra/logika: ??muzu zasahnout vlastnim projektilem sam sebe??
				if (p1.parentId() == p2.parentId())
				{
					continue;
				}
				CollisionEntry col = processCollision(p1, p2);
				if (col != null)
				{
					collisions.add(col);
				}
			}
		}
	}

	private void processObstacles(Set<ObstacleItem> obstacles)
	{
		for(ObstacleItem obstacle : obstacles)
		{
			for(Player player : arena.players())
			{
				CollisionEntry entry = processCollision(obstacle, player);
				if (entry != null)
				{
					collisions.add(entry);
				}
			}
			for(Projectile projectile : arena.projectiles())
			{
				CollisionEntry entry = processCollision(obstacle, projectile);
				if (entry != null)
				{
					collisions.add(entry);
				}
			}
			for(BonusItem bonus : arena.bonuses())
			{
				CollisionEntry entry = processCollision(obstacle, bonus);
				if (entry != null)
				{
					collisions.add(entry);
				}
			}
		}
	}
// TODO: hra/logika: kolize: P1 x arena(zed)
// TODO: hra/logika: kolize: proj x arena(zed)

	private CollisionEntry processCollision(IHasCollisionBody o1, IHasCollisionBody o2)
	{
		if (Intersector.overlapConvexPolygons(o1.body(), o2.body()))
		{
			return new CollisionEntry(o1, o2);
		}
		return null;
	}
}
