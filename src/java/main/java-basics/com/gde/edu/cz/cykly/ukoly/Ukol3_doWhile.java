package com.gde.edu.cz.cykly.ukoly;

public class Ukol3_doWhile
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1) vypis 5x svoje jmeno

		// 2) vypis 6x nahodne cislo (od nuly do sta)
		// NAPOVEDA: nahodne cislo ziskas zavolanim: CyklusDoWhile.getRandomNumber(<min>, <max>);

		// 3) BONUS: vypisuj nahodna cisla dokud nepadne nula")
		// 3.1) libovolna implementace
		// 3.2) nekonecna smycka + prikaz: break (~liche cisla nevypisuj, prikaz: continue)
		// 3.3) vhodna podminka (~na konci) pro opusteni cyklu
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("POZNAMKA: k reseni pouzij cyklus \"do-while\"");
		System.out.println("1) vypis 5x svoje jmeno");
		System.out.println("2) vypis 6x nahodne cislo (od nuly do sta)");
		System.out.println("   NAPOVEDA: nahodne cislo ziskas zavolanim: Prehled.getRandomNumber(<min>, <max>);");
		System.out.println("3) BONUS: vypisuj nahodna cisla dokud nepadne nula");
		System.out.println("-----------------------------");
	}
}
