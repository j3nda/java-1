package com.gde.edu.cz.promenne.cisla;

public class Prehled
{

	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

//		+-------- toto je datovy typ! @see: datovetypy.*
//		|
//		|         +-------------- toto je libovolne jmeno promenne [a-Z0-9_]
//		|         |
//		|         |       +------ toto je prirazovaci znamenko
//		|         |       |
//		|         |       | +---- toto je hodnota (cele cislo) ulozena do promenne
//		V     VVVVVVVVV   V V
		int promennaCislo = 123;

		// hodnotu promenne mohu vypsat, napr:
		System.out.println(promennaCislo);

		// anebo ji porovnat, @see: datovetypy.cisla.Prehled
		if (promennaCislo > 0)
		{
			// a vypsat na std-out (~standartni vystup)
			System.out.println("Cislo: " + promennaCislo + " je kladne cislo!");
		}

		// muzu provadet aritmeticke operace, napr:
		promennaCislo = promennaCislo - 123; // odcitani
		promennaCislo = promennaCislo + 123; // scitani
		promennaCislo = promennaCislo * 2;   // nasobeni
		promennaCislo = promennaCislo / 2;   // deleni

		// promennou mohu porovnavat pomoci ==
		if (promennaCislo == 123)
		{
			// a vypsat ji na std-out
			System.out.println("Promenna \"promennaCislo\" je prave cislo: " + promennaCislo);
		}
	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("promenna:");
		System.out.println("- slouzi k uchovani hodnoty, napr: cisla, retezce (anebo jine datove struktury)");
		System.out.println(
			"- vytvorim ji tak, ze si zvolim nejaky nazev, napr: \"honza\"\n"
			+ "\ta do nej budu chtit ulozit svou vysku, napr: 182\n"
			+ "\ta zapisu to takto:\n\n"
			+ "\tint honza = 182;"
		);
		System.out.println();
		System.out.println(
			"- v pripade realneho cisla, napr: \"cisloPi\"\n\n"
			+ "\tfloat cisloPi = 3.1415926535f;\n\n"
			+ "\tkde \"f\" na konci znaci \"float\" ~ desetinne cislo."
		);
		System.out.println();
		System.out.println("- obdobnym zpusobem mohu ukladat cela cisla, realna cisla (~popr. cele struktury)");
		System.out.println("-----------------------------");
	}
}
