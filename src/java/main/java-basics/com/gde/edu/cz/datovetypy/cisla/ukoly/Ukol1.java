package com.gde.edu.cz.datovetypy.cisla.ukoly;

public class Ukol1
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1.1) soucet, napr: -5 + 8

		// 1.2) rozdil, napr: 51 - 21

		// 1.3) nasobeni, napr: 14 * 21

		// 1.4) deleni, napr: 1452 / 333

		// 1.5) deleni se zbytkem, napr: 1452 modulo 333

		// BONUS)
		// 2.1) vysledek vypis na 2x desetinna mista

		// 2.2) vysledek vypis zaokrouhleny na 0x desetinnych mist
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) proved vypocet a vysledek vypis");
		System.out.println("1.1) soucet, napr: -5 + 8");
		System.out.println("1.2) rozdil, napr: 51 - 21");
		System.out.println("1.3) nasobeni, napr: 14 * 21");
		System.out.println("1.4) deleni, napr: 1452 / 333");
		System.out.println("1.5) deleni se zbytkem, napr: 1452 % 333");
		System.out.println();
		System.out.println("2) BONUS:");
		System.out.println("2.1) vysledek vypis na 2x desetinna mista");
		System.out.println("2.2) vysledek vypis zaokrouhleny na 0x desetinnych mist");
		System.out.println("-----------------------------");
	}
}
