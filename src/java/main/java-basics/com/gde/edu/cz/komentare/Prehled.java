package com.gde.edu.cz.komentare;


public class Prehled
{
	public static void main(String[] args)
	{
		// 2x (lomitka) na zacatku radku uvozuji: jednoradkovy komentar.
		// a muzu ho pouzit i na dalsim radku...

		int promenna = 123; // anebo rict, ze: "do 'promenna' ukladam cele cislo: 123"

		/*
		 * toto je vice-radkovy komentar,
		 * kde se muzu poradne rozepsat
		 *
		 * a komentovat pres vice radku...
		 */

		/**
		 * toto je tzv. "JavaDoc", kterym DOKUMENTUJI zdrojovy kod.
		 * tj. chci rict budoucimu programatorovi, co dana:
		 * - promenna/metoda/objekt
		 * provadi.
		 * Takze:
		 * "jmenoPohadky" mi popisuje detskou pohadku. a vychozi jmeno je "Alenka v risi divu"
		 */
		String jmenoPohadky = "Alenka v risi divu";
		System.out.println(jmenoPohadky);

		/** i takto: 1x radkem mohu pomoci JavaDoc dokumentovat kod */
		String pohadkaBajaja = jmenoPohadky;
		System.out.println(pohadkaBajaja);


		// napriklad zakomentovani volani metody (viz radek nize)
		// (schalne, zkus "napoveda();" odkomentovat a spustit a pozoruj, co se stane)
//		napoveda();
	}

	/** obsahuje napovedu, jak psat komentare */
	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("// jednoradkovy komentar");
		System.out.println();
		System.out.println("/* vice\n * radkovy\n * komentar\n */");
		System.out.println();
		System.out.println("/**\n * javaDoc\n * dokumentovani funkcnosti kodu\n */");
		System.out.println("-----------------------------");
	}
}
