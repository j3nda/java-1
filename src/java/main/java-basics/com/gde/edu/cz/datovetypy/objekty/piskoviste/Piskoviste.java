package com.gde.edu.cz.datovetypy.objekty.piskoviste;

import java.text.MessageFormat;

/**
 * toto je objekt "Piskoviste", tj. "herni-zona" pro vsechny, kteri se radi vrtaji v pisku.
 * kazde piskoviste ma:
 * - "VLASTNOSTI"
 *   - svou velikost (sirka, delka, hloubka)
 *   - umisteni (~napr: "na chalupe u lesa")
 *   - zda je povolen vstup se zviraty?
 *   - zda ma lavicky k sezeni kolem
 *
 * - "CHOVANI", napr: co se stane:
 *   - kdyz vstoupim se zviretem a vstup se zviretem je zakazan
 *   - kdyz si chci sednout a nejsou tam lavicky
 *
 * POZNAMKA: pokud hledas vysvetleni objektu jako celku, podivej se do {@link oop}
 */
public class Piskoviste
extends Object // kazdy novy objekt dedi vlastnosti z "Object" a je zbytecne to psat!
{
	// ======================================================================
	// vlastnosti
	public int sirka;
	public int delka;
	public int hloubka;
	public String umisteni = "na chalupe u lesa";
	public boolean povolenVstupSeZviraty;
	public boolean maLavicky;


	/** bez parametricky konstruktor */
	public Piskoviste()
	{
		this(20, 20, 20);
		povolenVstupSeZviraty = true;
	}

	/** konstruktor s parametry (~pro uvedeni velikosti a umisteni) */
	public Piskoviste(int sirka, int delka, int hloubka)
	{
		this.sirka = sirka;
		this.delka = delka;
		this.hloubka = hloubka;
	}

	/** konstruktor s parametry (~pro uvedeni velikosti a umisteni) */
	public Piskoviste(int sirka, int delka, int hloubka, String umisteni)
	{
		this.sirka = sirka;
		this.delka = delka;
		this.hloubka = hloubka;
		this.umisteni = umisteni;
	}

	// ======================================================================
	// chovani
	/** vyvola akci "vstup do piskoviste" a zachova se podle toho, zda mas zvire anebo ne */
	public void vstupDoPiskoviste(boolean masZvire)
	{
		if (!povolenVstupSeZviraty && masZvire)
		{
			throw new PiskovisteException("VSTUP ZAKAZAN!");
		}
		System.out.println(
			MessageFormat.format(
				"uspesne jsi vstoupil/a do piskoviste({0}) {1} zviretem.",
				umisteni,
				(masZvire ? "se" : "bez")
			)
		);
	}

	/** vyvola akci "sedni si na lavicku" a zachova se podle toho, zda jsou tam lavicky... */
	public void sedniSiNaLavicku()
	{
		if (!maLavicky)
		{
			throw new PiskovisteException("HA! SPADLS A SEDIS NA ZEMI!");
		}
		System.out.println("uspesne sis sedl/a na lavicku.");
	}
}
