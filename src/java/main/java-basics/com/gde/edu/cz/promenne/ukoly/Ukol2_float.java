package com.gde.edu.cz.promenne.ukoly;

public class Ukol2_float
{

	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor 2x promenne datoveho typu \"float\"");
		System.out.println("2) prirad do nich libovolne cela cisla.");
		System.out.println("3) proved");
		System.out.println("3.1) soucet, tj. secti obe promenne");
		System.out.println("3.2) rozdil, tj. odecti promenne od sebe");
		System.out.println("3.3) nasobeni, tj. vynasob promenne mezi sebou");
		System.out.println("3.4) deleni");
		System.out.println();
		System.out.println("Bonus:");
		System.out.println("- vypis veskere aritmeticke operace na obrazovku (std-out)");
		System.out.println("-----------------------------");
	}
}
