package com.gde.edu.cz.datovetypy.objekty;

import com.gde.edu.cz.datovetypy.objekty.krabice.IKrabice;
import com.gde.edu.cz.datovetypy.objekty.krabice.Krabice;

public class Prehled_Krabice
{
	public static void main(String[] args)
	{
		napoveda();

		ukazkaKrabice1_vytvoreniInstanceObjektu();

		ukazkaKrabice2_volaniMetodNadObjekty();
	}

	public static void ukazkaKrabice1_vytvoreniInstanceObjektu()
	{
		// vytvoreni promenne: datoveho-typu 'Krabice' vc. prirazeni(=) a vytvoreni 'new' nove-instance objektu typu 'Krabice'
		// 1/ 'new Krabice(...) --> vytvori novy objekt v pameti --> a ulozim ho do promenne 'malaLehkaKrabice'
		// 2/ volam tzv 'parametricky-konstruktor' --> (1.25f, 10, 20, 30) --> kde (hmotnost, delka, sirka, vyska)
		Krabice malaLehkaKrabice = new Krabice(1.25f, 10, 20, 30);

		// obdobne volani: s tim rozdilem, ze mam jine parametry v konstruktoru,
		// tj. 'malaTezkaKrabice' bude mit jine vlastnosti, nez 'malaLehkaKrabice'
		Krabice malaTezkaKrabice = new Krabice(999, 10, 20, 30);

		Krabice velkaLehkaKrabice = new Krabice(1.38f, 1000, 2000, 3000);

		// obdobne zde: ovsem s rozdilem, ze datovy-typ je 'IKrabice',
		// tj. komunikacni rozhranni! nikoliv predpis tridy 'Krabice'!
		// a opet: jine parametry krabice...
		IKrabice velkaTezkaKrabice = new Krabice(9998, 1000, 2000, 3000);
	}

	public static void ukazkaKrabice2_volaniMetodNadObjekty()
	{
		// vytvorim si promenne, obdobne jako v ukazkaKrabice1
		Krabice malaLekhaKrabice = new Krabice(1.25f, 10, 20, 30);
		Krabice malaTezkaKrabice = new Krabice(999, 10, 20, 30);
		Krabice velkaLehkaKrabice = new Krabice(1.38f, 1000, 2000, 3000);
		IKrabice velkaTezkaKrabice = new Krabice(9998, 1000, 2000, 3000);

		// a budu volat jejich parametry a metody
		System.out.println("malaLehkaKrabice: jeji hmotnost je: " + malaLekhaKrabice.hmotnost + "kg");

		// vim, ze to je 'Krabice', takze ji mohu posunout...
		malaTezkaKrabice.posun();

		// vim, ze to je 'Krabice', takze ji mohu prevratit...
		velkaLehkaKrabice.prevrat();
		malaTezkaKrabice.prevrat();

		// diky 'komunikacnimu rozhranni' vim, ze ji mohu otevrit
		velkaTezkaKrabice.otevri();
	}


	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("- v jave jsou objekty zakladnim stavebnim kamenem, maji vlastnosti (~atributy) a chovani.");
		System.out.println();
		System.out.println("- na zaklade teto kombinace je mozne stavet dalsi(~slozitejsi) objekty,");
		System.out.println("  ktere utvareji cely program/hru anebo jeho pod-cast (~herni mechaniku, grafiku, pohyb, aj)");
		System.out.println();
		System.out.println("- podrobneji se budeme objektum venovatv OOP (~objektove orientovane programovani) casti");
		System.out.println();
		System.out.println("- zatim je dobre vedet:");
		System.out.println("\t- ze existuji,");
		System.out.println("\t- jak se vytvareji (~tj konstruktor)");
		System.out.println("\t- jak mohu volat jejich metody \"chovani\"");
		System.out.println("-----------------------------");
	}
}
