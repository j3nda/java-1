package com.gde.edu.cz.cykly;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CyklusWhile
{
	public static void main(String[] args)
	{
		napoveda();

		ukazkaCykluWhile_poleCelychCisel("pole cisel---: ");
		ukazkaCykluWhile_kolekceCelychCisel("kolekce cisel: ");
		ukazkaCykluDoWhile_pocitaniZivotu("pocitani zivotu: ");
	}

	private static void ukazkaCykluWhile_poleCelychCisel(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		int[] poleCisel = new int[5];

		// cyklus while(condition) ~ je zalozen na podmince.
		// kdyz je podminka splnena, vykonava se. kdyz splnena neni - vykonavat se prestane.
		int index = 0;
		while(index < 5)
		{
			// do poleCisel[index] ~ index ~ je pouzit jako "iterator", tj. pocitadloCyklu
			// a do promenne priradime cele nahodne cislo.
			poleCisel[index] = getRandomNumber(0, 99);

			// a zvysime iterator
			index = index + 1;
		}

		// vypis pole
		index = 0; // iterator musime resetovat! (jinak by podminka nebyla splnena)
		while(index < 5)
		{
			System.out.print(
				poleCisel[index] // vypis cisla, ulozeneho v poli na pozici [index]

				// tzv. ternarni operator (~obdoba prikazu "if") vyhodnoceneho za behu
				+ (index < poleCisel.length - 1 ? ", " : ".")
			);
			index = index + 1;
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	/**
	 * tento zapis je vhodny pro iteraci nad kolekci, ktera ma iterator.
	 * v tomto zapise vidime, ze muzeme pouzit "iterator", ktery drzi aktualni pocitadlo iteraci.
	 */
	private static void ukazkaCykluWhile_kolekceCelychCisel(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		List<Integer> kolekceCisel = new ArrayList<>();

		// kolekci naplnime na 1. pohled krkolomne...
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));

		// ziskame iterator
		Iterator<Integer> iterator = kolekceCisel.iterator();

		// a budeme iterovat, dokud mame dalsi prvek
		while(iterator.hasNext())
		{
			System.out.print(
				// iterator zaroven drzi aktualni hodnotu
				// a posune "pointer" (~ukazatel) na nasledujici prvek.
				iterator.next()

				// i zde muzeme elegantne vyuzit ternarni operator...
				+ (iterator.hasNext() ? ", " : ".")
			);
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	private static void ukazkaCykluDoWhile_pocitaniZivotu(String title)
	{
		System.out.println(title);
		int zivoty = 10;
		while(zivoty > 0)
		{
		    System.out.println("- zivoty: " + zivoty);
		    zivoty = zivoty - 1;
		}
		System.out.println("GAME OVER");
		System.out.println("-");
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("while ~ tento cyklus pouzijeme, pokud nezname presny pocet opakovani.");
		System.out.println("-----------------------------");
	}
}
