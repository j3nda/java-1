package com.gde.edu.cz.datovetypy.objekty.piskoviste;

/**
 * Exception, nebo-li vyjimka tvori zaklad OOP (~objektove orientovane programovani),
 *   budeme se mu venovat ve zvlastni casti {@link oop}
 *
 * strucne: vyjimka je neco, co rika:
 * - ze neni vse vporadku
 * - a nese nezbytne nutne informace k zjisteni (~debuggovani), proc tomu tak je.
 *
 * na zaklade tzv "odchytavani" vyjimek je mozne stavet logiku programu/hry,
 *   aniz bychom psali "if" (~prikazy pro rizeni toku)
 */
public class PiskovisteException
extends RuntimeException
{
	// kazda vyjimka obsahuje unikatni identifikator,
	//   aby ji mohlo cokoliv jednoznacne identifikovat v celem programu
	//   (~napr, aby mohla byt posilana po siti)
	private static final long serialVersionUID = 8820282688947068239L;

	public PiskovisteException(String message)
	{
		// zavolam konstruktor predka, tj. "RuntimeException" a predam mu vstupni "message"
		super(message);

		// potom vypisu na std-out chybovou zpravu
		// (normalne bych to neudelal, zde pouze pro potreby vyuky a pochopeni objektu)
		System.out.println(getClass().getSimpleName() + ": " + message);
	}
}
