package com.gde.edu.cz.datovetypy.retezce.ukoly;

public class Ukol1
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: vypis svoje jmeno
		// napoveda: System.out.println(); // vypise retezec a odradkuje
		// napoveda: System.out.print();   // vypise retezec a NEodradkuje!

		// TODO: vypis kolik je hodin (~ve formatu: HH:MM)
		// (kde HH je hodina 00-23 a MM je minuta 00-59)
		// napoveda: pouzij scitani retezcu a cisla zapis jako cisla

		// TODO: vypis svoje jmeno a kolik je hodin pomoci: MessageFormat.format(...)
		// napoveda: "Jmenuji se: [dopln] a je [dopln] hodin a [dopln] minut."

		// TODO: vypis "ludolfovo cislo" PI na 4 desetinna mista
		// napoveda: k vypisu pouzi "printf"
		// napoveda: cislo PI naleznes v Math.PI
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vypis svoje jmeno.");
		System.out.println("2) vypis kolik je hodin (~ve formatu: HH:MM).");
		System.out.println("3) vypis svoje jmeno a kolik je hodin pomoci: MessageFormat.format(...).");
		System.out.println("4) vypis \"ludolfovo cislo\" PI na 4 desetinna mista.");
		System.out.println("-----------------------------");
	}
}
