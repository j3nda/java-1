package com.gde.edu.cz.datovetypy.cisla.ukoly;

public class Ukol3
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1) jaky bude vysledek souctu?
		int ahoj = 'A' + 'H' + 'O' + 'J';

		// 2) jaky bude vysledek souctu?
		byte svete = (byte)('S' + 'V' + 'E' + 'T' + 'E');
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) jaky bude vysledek souctu: int ahoj = 'A' + 'H' + 'O' + 'J';");
		System.out.println("   NAPOVEDA: ke zjisteni pouzij ASCII tabulku");
		System.out.println("1.1) BONUS: proc mohu scitat pismena?");
		System.out.println();
		System.out.println("2) jaky bude vysledek souctu: byte svete = (byte)('S' + 'V' + 'E' + 'T' + 'E');");
		System.out.println("   NAPOVEDA: ke zjisteni pouzij ASCII tabulku");
		System.out.println("2.1) jaky ma datovy typ \"byte\" rozsah?");
		System.out.println("2.2) BONUS: jaky bude vysledek souctu \"v prave zavorce (S+V+E+T+E)\"?");
		System.out.println("2.3) BONUS: co se stane, kdyz napisu: \"(byte)\"(pred_nejaky_vyraz) ?");
		System.out.println("-----------------------------");
	}
}
