package com.gde.edu.cz.datovetypy.retezce.ukoly;

public class Ukol2
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		String text = "The Quick Brown Fox Jumps Over The Lazy Dog.";

		// 1) preved vsechna pismena na mala, tj. [a-z]
		// 2) preved vsechna pismena na VELKA, tj. [A-Z]
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) preved vsechna pismena na mala, tj. [a-z]");
		System.out.println("2) preved vsechna pismena na VELKA, tj. [A-Z]");
		System.out.println("-----------------------------");
	}
}
