package com.gde.edu.cz.rizenitoku.ukoly;

import com.gde.edu.cz.rizenitoku.Prehled;

public class Ukol2_if
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		int sekund = 86399;

		// 1) napis program, ktery prevede pocet sekund na: hodina:minuta:sekunda");
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	private static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) napis program, ktery prevede pocet sekund na: hodina:minuta:sekunda");
		System.out.println("-----------------------------");
	}
}
