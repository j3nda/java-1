package com.gde.edu.cz.pole;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("pole (~anglicky: array) sdruzuje konecny pocet prvku (cisel, retezcu, objektu...) stejneho datoveho typu!");
		System.out.println();
		System.out.println("- velikost pole musi byt znama predem (~tj. delka pole)");
		System.out.println("- datovy typ pole musi byt znam predem (~tj. vsechny prvky jsou stejneho datoveho typu)");
		System.out.println("- pole nelze adresovat mimo jeho velikost! (~tj. delku pole)");
		System.out.println();
		System.out.println("- pole muzeme mit:");
		System.out.println("  1x (jedno-rozmerne, napr: int[] = {1,2,3,4,5};");
		System.out.println("  2x (dvou-rozmerne, napr: int[][] = { {1,2}, {3,4} };");
		System.out.println("  az");
		System.out.println("  n-rozmerne ~ v takovych situacich jsou vhodnejsi jine datove struktury,");
		System.out.println("    (napr: kolekce (~mapy, listy, stromy) anebo jejich kombinace)");
		System.out.println("-----------------------------");
	}
}
