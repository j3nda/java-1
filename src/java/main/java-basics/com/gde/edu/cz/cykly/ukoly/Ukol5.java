package com.gde.edu.cz.cykly.ukoly;

public class Ukol5
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1) pomoci vhodnych cyklu vypis:
		// (viz varianty: 1.1 az 1.6 nize ~ razeno: od nejjednodussi po slozitejsi)
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("POZNAMKA: vyber vhodny cyklus k reseni ulohy");
		System.out.println("1) pomoci vhodnych cyklu vypis:");
		System.out.println("\t1.1)       \t1.2)      \t1.3)");
		System.out.println("\t********** \t*         \t    *");
		System.out.println("\t********** \t**        \t   **");
		System.out.println("\t********** \t***       \t  ***");
		System.out.println("\t********** \t****      \t ****");
		System.out.println("\t********** \t*****     \t*****");
		System.out.println();
		System.out.println("\t1.4)       \t1.5)      \t1.6)");
		System.out.println("\t    *      \t    1     \t    1");
		System.out.println("\t   ***     \t   222    \t   212");
		System.out.println("\t  *****    \t  33333   \t  32123");
		System.out.println("\t *******   \t 4444444  \t 4321234");
		System.out.println("\t*********  \t555555555 \t543212345");
		System.out.println("-----------------------------");
	}
}
