package com.gde.edu.cz.datovetypy.retezce.ukoly;

public class Ukol4
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		String slovo1 = "Pondeli";
		String slovo2= "Zanzibar";
		String slovo3= "GamingIsNotAcrime!";

		// 1) vypis prvni znak z kazdeho slova
		// 2) vypis posledni znak z kazdeho slova

		// BONUS:
		// 3) vypis prostredni znak z kazdeho slova
		// 4) vypsana pismena preved na VELKA
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vypis prvni znak z kazdeho slova");
		System.out.println("2) vypis posledni znak z kazdeho slova");
		System.out.println();
		System.out.println("BONUS:");
		System.out.println("3) vypis prostredni znak z kazdeho slova");
		System.out.println("4) vypsana pismena preved na VELKA");
		System.out.println("-----------------------------");
	}
}
