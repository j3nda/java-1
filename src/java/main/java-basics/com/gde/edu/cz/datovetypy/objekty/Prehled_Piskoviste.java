package com.gde.edu.cz.datovetypy.objekty;

import java.text.MessageFormat;

import com.gde.edu.cz.datovetypy.objekty.piskoviste.Piskoviste;
import com.gde.edu.cz.datovetypy.objekty.piskoviste.PiskovisteException;

public class Prehled_Piskoviste
{
	public static void main(String[] args)
	{
		napoveda();

		// na chalupe u lesa: NEMA LAVICKY!
		ukazkaPiskoviste1();

		// doma v byte: NEMA ZVIRATA ANI LAVICKY!
		ukazkaPiskoviste2();

		// venku: MA LAVICKY, ale NEMA ZVIRATA!
		// (zde si vsimni nesmyslu - nemuzu vstoupit se zviretem a presto si sednu)
		ukazkaPiskoviste3();
	}

	/**
	 * - vytvoreni piskoviste ~ bez udani parametru
	 * - pokus se vejit do piskoviste,
	 * - a pokus se sednout si na lavicku
	 */
	public static void ukazkaPiskoviste1()
	{
		// vytvoreni objektu (typu: Piskoviste) s vychozim, tj. bez-parametrickym konstruktorem!
		Piskoviste naChalupe_veVychoziVelikosti = new Piskoviste();

		// vypisu informace o piskovisti
		vypisInformaceO(naChalupe_veVychoziVelikosti);


		// zkusim vstoupim do piskoviste ~ se zviretem!
		try
		{
			naChalupe_veVychoziVelikosti.vstupDoPiskoviste(true);
		}
		catch(PiskovisteException e) { }


		// zkusim si sednout na lavicku
		try
		{
			naChalupe_veVychoziVelikosti.sedniSiNaLavicku();
		}
		catch(PiskovisteException e) { }
	}

	/**
	 * - vytvoreni piskoviste ~ s udanim parametru jako velikosti piskoviste
	 * - pokus se vejit do piskoviste,
	 * - a pokus se sednout si na lavicku
	 */
	public static void ukazkaPiskoviste2()
	{
		// vytvoreni objektu (typu: Piskoviste) s parametrickym konstruktorem (~tj. predam mu velikost piskoviste)
		Piskoviste vByte_velimiMale = new Piskoviste(1, 2, 3, "doma v byte");

		// vypisu informace o piskovisti
		vypisInformaceO(vByte_velimiMale);


		// zkusim vstoupim do piskoviste ~ se zviretem!
		try
		{
			vByte_velimiMale.vstupDoPiskoviste(true);
		}
		catch(PiskovisteException e) { }


		// zkusim si sednout na lavicku
		try
		{
			vByte_velimiMale.sedniSiNaLavicku();
		}
		catch(PiskovisteException e) { }
	}


	/**
	 * - vytvoreni piskoviste ~ s udanim parametru jako velikosti piskoviste a zmenou jeho vlastnosti
	 * - pokus se vejit do piskoviste,
	 * - a pokus se sednout si na lavicku
	 */
	public static void ukazkaPiskoviste3()
	{
		// vytvoreni objektu (typu: Piskoviste) s parametrickym konstruktorem (~tj. predam mu velikost piskoviste)
		Piskoviste venku_sLavickamaBezZvirat = new Piskoviste(3, 2, 1);

		// uprava vlastnosti piskoviste
		venku_sLavickamaBezZvirat.umisteni = "venku s lavickama, bez zvirat";
		venku_sLavickamaBezZvirat.maLavicky = true;
		venku_sLavickamaBezZvirat.povolenVstupSeZviraty = false;

		// vypisu informace o piskovisti
		vypisInformaceO(venku_sLavickamaBezZvirat);


		// zkusim vstoupim do piskoviste ~ se zviretem!
		try
		{
			venku_sLavickamaBezZvirat.vstupDoPiskoviste(true);
		}
		catch(PiskovisteException e) { }


		// zkusim si sednout na lavicku
		try
		{
			venku_sLavickamaBezZvirat.sedniSiNaLavicku();
		}
		catch(PiskovisteException e) { }
	}

	/**
	 * vypise informace o piskovisti.
	 * pokud se nejedna o piskoviste, nebude se nic vypisova.
	 */
	public static void vypisInformaceO(Object object)
	{
		if (object instanceof Piskoviste)
		{
			// pretypovani (Object) -> (Piskoviste), abych mohl pristoupi k jeho vlastnostem a chovani
			// (toto muzu provest s jistotou, protoze tomu predchazi podminka "if")
			Piskoviste piskoviste = (Piskoviste) object;

			// vypsani, co je Piskoviste za typ objektu a jeho umisteni
			System.out.println("---");
			System.out.println(
				MessageFormat.format(
					"Typ: \"{0}\", umisteni: \"{1}\"",
					piskoviste.getClass().getName(),
					piskoviste.umisteni
				)
			);

			// vypsani velikosti piskoviste
			System.out.println(
				MessageFormat.format(
					"- velikost(sirka x delka x hloubka): {0}x{1}x{2}",
					piskoviste.sirka,
					piskoviste.delka,
					piskoviste.hloubka
				)
			);

			// vypsani, zda je povolen vstup se zviraty
			System.out.println(
				MessageFormat.format(
					"- vstup se zviraty povolen? {0}",
					(piskoviste.povolenVstupSeZviraty ? "ano" : "ne")
				)
			);

			// vypsani, zda ma lavicky k sezeni
			System.out.println(
				MessageFormat.format(
					"- ma lavicky? {0}",
					(piskoviste.maLavicky ? "ano" : "ne")
				)
			);
		}
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("- v jave jsou objekty zakladnim stavebnim kamenem, maji vlastnosti (~atributy) a chovani.");
		System.out.println();
		System.out.println("- na zaklade teto kombinace je mozne stavet dalsi(~slozitejsi) objekty,");
		System.out.println("  ktere utvareji cely program/hru anebo jeho pod-cast (~herni mechaniku, grafiku, pohyb, aj)");
		System.out.println();
		System.out.println("- podrobneji se budeme objektum venovat v OOP (~objektove orientovane programovani) casti");
		System.out.println();
		System.out.println("- zatim je dobre vedet:");
		System.out.println("\t- ze existuji,");
		System.out.println("\t- jak se vytvareji (~tj konstruktor)");
		System.out.println("\t- jak mohu volat jejich metody \"chovani\"");
		System.out.println("-----------------------------");
	}
}
