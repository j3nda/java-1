package com.gde.edu.cz.datovetypy.retezce;

import java.text.MessageFormat;

public class Prehled
{
	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

		String promennaJakoString = "Hello World!"; // ma 12x znaku
			// String ~ sled/posloupnost znaku
			// 1.pozice=H; 2.pozice=e; 3.pozice=l; atd... 12.pozice=!
			// String ~ pole znaku

		char[] promennaJakoPoleZnaku = {
			// prave ty "[]" rikaji, ze to je pole!
			// datovy typ "char" rika, ze to je znak
			// pozor: znaky musim uvodit ' (apostrofem)
			'H', 'e', 'l', 'l', 'o',
			' ',
			'W', 'o', 'r', 'l', 'd',
			'!',
		};

		String dalsiPromennaJakoStringVytvorenaZPoleZnaku1 = new String(promennaJakoPoleZnaku);
		String dalsiPromennaJakoStringVytvorenaZPoleZnaku2 = new String("Hello World!");
		String dalsiPromennaJakoStringVytvorenaZPoleZnaku3 = "Hello World!";

		// prikaz "System.out.println" vypise retezec na obrazovku (~standardni vystup ~ stdout)
		System.out.println("vypis promennych:");
		System.out.println(promennaJakoString);
		System.out.println(promennaJakoPoleZnaku);
		System.out.println(dalsiPromennaJakoStringVytvorenaZPoleZnaku1);
		System.out.println(dalsiPromennaJakoStringVytvorenaZPoleZnaku2);
		System.out.println(dalsiPromennaJakoStringVytvorenaZPoleZnaku3);

		System.out.println("Cizinec rika: \"" + promennaJakoString + "\".");
		System.out.println("Cizinec rika: \"Length: " + promennaJakoString.length() + "x znaku!\"");
		System.out.println("String"
			+ " muze byt spojovan pomoci:"
			+ " \"+\" (znaku: plus)."
		);

		// vsimni si, jak je zapsan tento retezec a jak je vypsan na obrazovku
		System.out.print("anebo".concat("dalsim textem")
			.concat(" muze byt spojovan")
			.concat(" pomoci:")
			.concat(" \".concat()\" metody!")
		);

		// vsimni si, jak je zapsan tento retezec a jak je vypsan na obrazovku
		// formatovani: %[číslo parametru][příznaky][šířka][.přesnost][velikost]typ
		//   %s ~ string
		//   %d ~ int
		//   %f ~ float
		System.out.printf(
			"%s\n\tcele cislo: %d\n\tdesetinne cislo: %.2f\n",

			"Muzeme vyuzit \"printf\" funkcionality formatovani retezcu:", // 1. parametr
			123, // 2.parametr
			321.123f // 3. parametr
		);

		// vsimni si, jak je zapsan tento retezec a jak je vypsan na obrazovku
		// formatovani: {cislo-parametru}
		System.out.println(
			MessageFormat.format(
				"Stejne tak muzeme pouzit \"MessageFormat.format\": {0} dalsi argument bude: {1} a {2}",

				MessageFormat.class.getSimpleName(),
				"neco dalsiho",
				1234
			)
		);
		System.out.println(
			MessageFormat.format(
				"Retezec \"{0}\" muzeme porovnavat s: \"{1}\" == {2}.",

				promennaJakoString,
				"Ahoj Svete!",
				"Ahoj Svete!".equals(promennaJakoString)
			)
		);


		// cislo zapsane jako retezec!
		// (POZOR: jenom clovek vidi cislo! pocitac vidi retezec a *nejaky* sled znaku)
		String retezecJakoCislo = "1234";
		System.out.println(retezecJakoCislo);

		// 1
		retezecJakoCislo = retezecJakoCislo + "1"; // "1234" + "1" = "12341"
		System.out.println(retezecJakoCislo);

		// 2
		retezecJakoCislo = (Integer.valueOf(retezecJakoCislo) + 1) + ""; // 1234 + 1 = 1235
		System.out.println(retezecJakoCislo);



		// std.vstup ~ stdin ~ System.in
		// std.vystup ~ stdout ~ System.out
		// std.chyba ~ stderr ~ System.err

		int cislo123 = 123;
		int cislo0 = 1;

		System.err.println("nejaka chybova hlaska! DELENI NULOU");
		System.out.println(cislo123 / cislo0);

	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println(
			"Strings (~retezece),"
			+ " jsou siroce pouzivany v Jave,"
			+ " a je to posloupnost znaku."
			+ " V Java jsou \"strings\" (~retezce) objekty."
		);
		System.out.println(
			"Objekt ma metody, ktere muzeme pouzit/zavolat,"
			+ " napr: String.concat()"
			+ " nebo \"myString\".length(); == " + "myString".length()
		);
		System.out.println("-----------------------------");
	}
}
