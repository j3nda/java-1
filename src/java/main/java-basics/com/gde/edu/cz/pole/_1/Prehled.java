package com.gde.edu.cz.pole._1;

import java.util.Arrays;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();

		// deklarace (jedno-rozmerneho) pole o velikosti 5x prvku,
		// kde vsechny prvky pole budou cela cisla, datoveho typu: int
		int[] mojePrvniPole = new int[5];

		// ukazka naplneni pole cisly
		mojePrvniPole[0] = 1159; // prvni
		mojePrvniPole[1] = 0;
		mojePrvniPole[2] = -12;
		mojePrvniPole[3] = 11150;
		mojePrvniPole[4] = -111; // posledni

		// kdyz se snazim vypsat pole na std-out,
		// zjistim, ze nema implementovanou metodu: "toString()",
		// takze se vypise pointer na 1. prvek v poli.
		System.out.println(mojePrvniPole);

		// neco jako: [I@768debd
		//
		// namisto toho, kdyz pouziju 'helper' (~pomocnicek) pro zretezeni prvku v poli do retecze,
		System.out.println(Arrays.toString(mojePrvniPole));

		// bude to vypadat takto:
		// [1159, 0, -12, 11150, -111]


		// do pole na pozici: 2 mohu znovu priradit cele cislo,
		// takze tim prepisu ulozenou predchozi hodnotu!
		mojePrvniPole[2] = 710;

		// kdyz zapisu mimo pridelenou pamet, program spadne!
		// schvalne: odkomentuj nasl. radek a vyzkousej a zamysli se proc to tak je.
//		mojePrvniPole[5] = 66;


		// znovu vypisu pole, pomuzu si "helperem"
		System.out.println(Arrays.toString(mojePrvniPole));

		// a pomoci cyklu 'for' iteruju nad vsemi prvky v poli a postupne je vypisuju
		for(int i = 0; i < mojePrvniPole.length; i++)
		{
			System.out.println("index: " + i + ", hodnota: " + mojePrvniPole[i]);
		}


		// pole muzu mit i jako 'retezce', napr. dny v tydnu
		String[] den_v_tydnu = new String[7]; // velikost pole: 7


		den_v_tydnu[0] = "pondeli"; // indexuju od: 0
		den_v_tydnu[6] = "nedele";  // posledni prvek: velikost-pole (minus) 1 = 6

		for(int i = 1; i <= 5; i++)
		{
			den_v_tydnu[i] = den_v_tydnu[0] + " (neco mezi) " + den_v_tydnu[6];
		}

		System.out.println(Arrays.toString(den_v_tydnu));
		for(int i = 0; i < den_v_tydnu.length; i++)
		{
			System.out.println("index: " + i + ", hodnota: " + den_v_tydnu[i]);
		}

	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("pole (~anglicky: array) sdruzuje konecny pocet prvku (cisel, retezcu, objektu...) stejneho datoveho typu!");
		System.out.println();
		System.out.println("- velikost pole musi byt znama predem (~tj. delka pole)");
		System.out.println("- datovy typ pole musi byt znam predem (~tj. vsechny prvky jsou stejneho datoveho typu)");
		System.out.println("- pole nelze adresovat mimo jeho velikost! (~tj. delku pole)");
		System.out.println();
		System.out.println("- jedno-rozmerne pole se deklaruje \"[]\" (2x hranatyma zavorkama!)");
		System.out.println("-----------------------------");
	}
}
