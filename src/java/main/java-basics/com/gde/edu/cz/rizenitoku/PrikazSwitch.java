package com.gde.edu.cz.rizenitoku;

public class PrikazSwitch
{
	public static void main(String[] args)
	{
		napoveda();

		// pro rizeni toku programu mohu pouzit i tzv: switch.
		// (to je trochu jiny zapis podminek "if")

		// vytvorime promennou "hodKostkou",
		// do ktere priradime nahodnou hodnotu <1..6>
		int hodKostkou = getRandomNumber(1, 6);

		// pro jistotu si hodnotu vypisu
		System.out.println("hodKostkou: " + hodKostkou);

		// zde je podminka tvorena kombinaci: obsahu promenne "hodKostkou" + moznosti "case N", tj.
		// (hodKostkou == 3 && case 3) == true => potom se vykona kod v danem bloku {...}
		//
		// kazda "case" moznost by mela koncit prikazem: "break", ktery ukonci provadeni dalsich moznosti.
		// (v pripade, ze prikaz "break" bude chybet, AUTOMATICKY se zacne vykonavat nasledujici {...} blok moznosti case)
		switch(hodKostkou)
		{
			case 6: System.out.println("HURAAAA! hodil jsem 6 ku!"); break;
			// ekvivalentni zapis pomoci: "if (hodKostkou == 6) { ... }"

			case 5:
			case 4:
			case 41:
			case 411:
			case 4111:
			// ekvivalentni zapis pomoci: "if (hodKostkou == 5 || hodKostkou == 4 || hodKostkou == 41 || hodKostkou == 411 || hodKostkou == 4111) { ... }"
			{
				System.out.println("oh! hodil jsem 5 ku anebo 4ku...");
				break;
			}
			case 3:
			{
				System.out.println("woooow! 3 ka....");
				break;
			}
			default:
			// ekvivalentni zapis pomoci: "if (hodKostkou == 1 || hodKostkou == 2) { ... }"
			{
				System.out.println("hmmmm! to vypada na jednicku anebo dvojku...");
			}
		}
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	private static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("switch(podminka) case moznost1: {...}; case moznost2: {...}; default: {...};");
		System.out.println("-----------------------------");
	}
}
