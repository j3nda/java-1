package com.gde.edu.cz.pole._1.ukoly;

public class Ukol3
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: deklarace pole (dle zadani)
		// napoveda: ???[] mojePole = new ???[5];

		// TODO: naplneni pole (dle zadani)
		// napoveda: ???[0] = ???;

		// TODO: vypsani pole (pro jistotu)

		// TODO: setrideni (vzestupne ~ dle zadani)

		// TODO: vypsani pole (dle zadani)

		// TODO: Bonus (dle zadani)
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor pole celych cisel o velikosti 20");
		System.out.println("2) toto pole napln nahodnymi cisly: <0..40>.");
		System.out.println("3) takto naplnene pole serad vzestupne, tj. 0 < 22");
		System.out.println("   (~1. bude nejnizsi cislo, nasledovane cislem o neco vyssim a nakonec bude nejvyssi cislo)");
		System.out.println("4) serazene pole vypis na std-out.");
		System.out.println();
		System.out.println("Bonus:");
		System.out.println("A) naplnene pole seraz sestupne, tj. 22 > 0");
		System.out.println("  (~1. bude nevyssi cislo, nasledovany cislem o neco nizsim a nakonec bude nejnizsi cislo)");
		System.out.println("B) serazene pole nahodne zamichej");
		System.out.println("-----------------------------");
	}
}
