package com.gde.edu.cz.pole._2.ukoly;

public class Ukol2
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: deklarace pole (dle zadani)
		// napoveda: char[][] mojePole = new char[???][???];

		// TODO: naplneni pole (dle zadani)
		// napoveda: naplnPoleZnakem(mojePole, ???);

		// TODO: vypsani pole (dle zadani)
		// napoveda: vypisPole(mojePole);

		// TODO: zavolani: vodorovnaLinka(); svislaLinka(); (dle zadani)
		// napoveda: vodorovnaLinka(???);
		// napoveda: svislaLinka(???);

		// TODO: vypsani pole (dle zadani)

		// TODO: Bonus (dle zadani)
	}

	private static void vodorovnaLinka(char[][] display)
	{
		for(int i = 0; i < 80; i++)
		{
			display[i][5] = '*';
		}
	}

	private static void naplnPoleZnakem(
		char[][] display,
		Object znak // TODO: uprav datovy typ! (~dat.typ: "Object" nebude fungovat!)
	)
	{
		// TODO: uprav! cislo "0" nula zde nebude fungovat spravne.
		int maxI = 0;
		int maxJ = 0;
		for(int i = 0; i < maxI; i++)
		{
			for(int j = 0; j < maxJ; j++)
			{
				// TODO: odkomentuj nasl. radek a podle datoveho typu vyse -> promenna "znak" uprav.
//				display[i][j] = znak;
			}
		}
	}

	private static void vypisPole(char[][] display)
	{
		// TODO: uprav! cislo "0" nula zde nebude fungovat spravne.
		int maxI = 0;
		int maxJ = 0;
		for(int i = 0; i < maxI; i++)
		{
			for(int j = 0; j < maxJ; j++)
			{
				System.out.print(display[i][j]);
			}
			System.out.println();
		}
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor 2x rozmerne pole znaku (~char) o velikosti 80x25 znaku.");
		System.out.println("2) toto pole napln znakem: ' ' (mezera, hex(20), dec(32)).");
		System.out.println("3) takto naplnene pole vypis na std-out.");
		System.out.println("4) pomoci pripravene metody: vodorovnaLinka(); se inspiruj a vytvor/dopln metody: \"svislaLinka();\"");
		System.out.println("5) zavolej: vodorovnaLinka() i svislaLinka();");
		System.out.println("6) a vypis pole na std-out.");
		System.out.println();
		System.out.println("Bonus:");
		System.out.println("B1) uprav metody: vodorovnaLinka() a svislaLinka() tak, abys byl schopen vykreslit ctverec!");
		System.out.println("B2) vytvor novou metodu: kresliCtverec(int x, int y, int velikost);");
		System.out.println("    (ktera vykresli ctverec na pozadovanem miste [x,y] o dane velikosti)");
		System.out.println("-----------------------------");
	}
}
