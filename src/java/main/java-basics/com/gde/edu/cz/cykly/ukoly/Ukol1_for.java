package com.gde.edu.cz.cykly.ukoly;

public class Ukol1_for
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1) vypis 5x svoje jmeno

		// 2) vypis 6x nahodne cislo (od nuly do sta)
		// NAPOVEDA: nahodne cislo ziskas zavolanim: CyklusFor.getRandomNumber(<min>, <max>);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("POZNAMKA: k reseni pouzij cyklus \"for\"");
		System.out.println("1) vypis 5x svoje jmeno");
		System.out.println("2) vypis 6x nahodne cislo (od nuly do sta)");
		System.out.println("   NAPOVEDA: nahodne cislo ziskas zavolanim: Prehled.getRandomNumber(<min>, <max>);");
		System.out.println("-----------------------------");
	}
}
