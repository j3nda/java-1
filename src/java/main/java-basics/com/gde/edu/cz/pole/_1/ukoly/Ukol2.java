package com.gde.edu.cz.pole._1.ukoly;

import com.gde.edu.cz.pole._1.Prehled;

public class Ukol2
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: deklarace pole (dle zadani)
		// napoveda: ???[] mojePole = new ???[5];

		// TODO: naplneni pole (dle zadani)
		// napoveda: ???[0] = 1159;

		// TODO: vypsani pole (dle zadani)

		// TODO: Bonus (dle zadani)
	}

	/**
	 * metoda vraci pole celych cisel o velikosti udane parametrem
	 * @param velikostPole
	 */
	private static int[] vytvorPoleCelychCisel(int velikostPole)
	{
		int[] cisla = new int[velikostPole];

		// TODO: napln pole nahodnymi cisly

		return cisla;
	}

	/** vrati nahodne cele cislo (~z blize nespecifikovaneho rozsahu) */
	static int vratNahodneCislo()
	{
		return vratNahodneCislo(0, 123);
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor pole nahodnych cisel o velikosti 22");
		System.out.println("2) v poli nalezni:");
		System.out.println("2.1) maximalni hodnotu");
		System.out.println("2.2) minimalni hodnotu");
		System.out.println("3) vyspis pole, maximalni i minimalni hodnotu a celkovy soucet");
		System.out.println();
		System.out.println("4) Bonus:");
		System.out.println("4.1) secti vsechna cisla (~suma) a vypis soucet");
		System.out.println("4.2) spocti prumernou hodnotu a vypis ji");
		System.out.println("-----------------------------");
	}
}
