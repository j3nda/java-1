package com.gde.edu.cz.rizenitoku.ukoly;

import com.gde.edu.cz.rizenitoku.Prehled;

public class Ukol3_ifAndOr
{
	public static void main(String[] args)
	{
		napoveda();

		// do promennych priradim nahodne hodnoty, podle toho, jak se promenne jmenuji.
		boolean venkuJeTma = (vratNahodneCislo(0, 1) == 1 ? true : false);
		boolean venkuPrsi = (vratNahodneCislo(0, 1) == 1 ? true : false);
		int denniHodina = vratNahodneCislo(0, 23);

		// TODO: viz napoveda
		// 1) podle "denniHodina" vypis fazi dne (dopoledne, odpoledne, vecer, noc), kazda faze ma cca 6h

		// 2) zobraz varovani, pokud je venku tma a denni hodina neni vecer ani noc
		// BONUS: oprav "venkuJeTma", tak aby odpovidala pravdive hodnote z "denniHodina".

		// 3) pokud venku neprsi a je svetlo, vypis "Dnes je krasny den!"
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	private static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) podle \"denniHodina\" vypis fazi dne (dopoledne, odpoledne, vecer, noc)");
		System.out.println("   NAPOVEDA: kazda faze trva cca 6h");
		System.out.println();
		System.out.println("2) zobraz varovani, pokud je venku tma a denni hodina neni vecer ani noc.");
		System.out.println("   NAPOVEDA: pouzij logicke operatory:");
		System.out.println("     \"&&\" AND (~a zaroven)");
		System.out.println("     \"||\" OR  (~nebo)");
		System.out.println("     \"!\"  NOT (~negace)");
		System.out.println("   BONUS: oprav \"venkuJeTma\", tak aby odpovidala pravdive hodnote z \"denniHodina\".");
		System.out.println();
		System.out.println("3) pokud venku neprsi a je svetlo, vypis \"Dnes je krasny den!\"");
		System.out.println("-----------------------------");
	}
}
