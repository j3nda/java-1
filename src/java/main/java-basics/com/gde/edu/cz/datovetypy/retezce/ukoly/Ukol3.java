package com.gde.edu.cz.datovetypy.retezce.ukoly;

public class Ukol3
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		String tutorial = "Tutorial";
		String java2020 = "Java 2020";

		// 1) vloz slovo "Tutorial" mezi dve slova v retezci oddelena mezerou, napr: "Java 2020"
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vloz slovo \"Tutorial\" mezi dve slova v retezci oddelena mezerou,");
		System.out.println("   napr: \"Java 2020\" a vysledek vypis");
		System.out.println();
		System.out.println("vysledek bude, napr: \"Java Tutorial 2020\"");
		System.out.println("-----------------------------");
	}
}
