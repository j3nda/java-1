package com.gde.edu.cz.datovetypy.konstanty;

public class Prehled
{
	// definice konstant
	// (viditelnost: 'package-private', vsimni si: ze na zacatku neni nic uvedeno)
	static final int POCET_KLAVES_NA_KLAVESNICI = 101;
	static final float CISLO_PI = 3.14159265f;

	// definice konstant (vc. viditelnosti)
	// (viditelnost: {private, protected, public}; vsimni si, ze je na zacatku)
	private static final String NAZEV_OBCE  = "Hajany";
	protected static final String NAZEV_MESTA = "Brno";
	public static final String NAZEV_STATU = "Ceska Republika";

	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

		pouzitiKonstant(); // ukazka pouziti konstant
	}

	private static void pouzitiKonstant()
	{
		System.out.println("POCET_KLAVES_NA_KLAVESNICI = " + POCET_KLAVES_NA_KLAVESNICI);
		System.out.println("CISLO_PI = " + Prehled.CISLO_PI);
		System.out.println();
		System.out.println("NAZEV_OBCE = " + NAZEV_OBCE);
		System.out.println("NAZEV_MESTA = " + NAZEV_MESTA);
		System.out.println("NAZEV_STATU = " + NAZEV_STATU);
	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("konstanta:");
		System.out.println("- je identifikator (~podobne jako promenna), ale nelze ji behem provadeni programu menit");
		System.out.println("- je definovana 1x a muze byt pouzita v programu mnohokrat");
		System.out.println("- jeji pouziti usnadnuje orientaci a udrzbu v kodu, protoze se na jeji hodnotu odkazuju smysluplnym jmenem");
		System.out.println();
		System.out.println(
			"- vytvorim ji tak, ze pouziju klicova slova:\n"
			+ "\t- static (~rika: ze hodnota bude ulozena pouze 1x a na 1x miste)\n"
			+ "\t- final  (~rika: ze hodnota bude konecna, tj. nelze ji zmenit)"
		);
		System.out.println();
		System.out.println(
			"- priklad:\n"
			+ "\tstatic final int POCET_KLAVES_NA_KLAVESNICI = 1010;\n"
			+ "\tstatic final float CISLO_PI = 3.14159265f;\n"
		);
		System.out.println();
		System.out.println(
			"- priklad (vc. viditelnosti):\n"
			+ "\tprivate   static final String NAZEV_OBCE  = \"Hajany\";\n"
			+ "\tprotected static final String NAZEV_MESTA = \"Brno\";\n"
			+ "\tpublic    static final String NAZEV_STATU = \"Ceska Republika\";"
		);
		System.out.println("-----------------------------");
	}
}
