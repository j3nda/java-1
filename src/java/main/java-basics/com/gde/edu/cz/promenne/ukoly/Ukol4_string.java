package com.gde.edu.cz.promenne.ukoly;

public class Ukol4_string
{

	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor 2x promenne datoveho typu \"String\"");
		System.out.println("2) prirad do nich libovolny retezec (~napr: jmeno a prijmeni).");
		System.out.println("3) proved");
		System.out.println("3.1) spojeni retezcu (~concat), tj. spoj oba retezce do 3. promenne");
		System.out.println("3.2) vypis delku 3. promenne ~ spojenych retezcu");
		System.out.println("3.3) vypis prvni znak 3. promenne ~ spojeneho retezce");
		System.out.println("3.4) vypis posledni znak 3. promenne ~ spojeneho retezce");
		System.out.println("-----------------------------");
	}
}
