package com.gde.edu.cz.datovetypy.cisla.ukoly;

public class Ukol2
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1.1)
		byte todo1_1 = 0b101 + 0b11;

		// 1.2)
		byte todo1_2 = 12 + 46;

		// 1.3)
		byte todo1_3 = 0x13 + 0x10;

		// 4
		byte todo_4 = (byte) 333;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) v jake ciselne soustave je dany zapis?");
		System.out.println("1.1) 0b101 + 0b11");
		System.out.println("1.2) 12 + 46");
		System.out.println("1.3) 0x13 + 0x10");
        System.out.println();
		System.out.println("2) jaky bude vysledek v desitkove(~dec) soustave?");
		System.out.println("2.1) pro soucet z (1.1)");
		System.out.println("2.2) pro soucet z (1.2)");
		System.out.println("2.3) pro soucet z (1.3)");
        System.out.println();
		System.out.println("3) kdyz porovnam cisla \"0x20\" a \"32\", jaky bude vysledek?");
		System.out.println("4) co se stane, kdyz priradim cislo 333 (~dec/desitkove soustave) do datoveho typu byte?");
		System.out.println("-----------------------------");
	}
}
