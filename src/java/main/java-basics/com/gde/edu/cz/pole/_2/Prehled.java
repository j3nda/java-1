package com.gde.edu.cz.pole._2;

import java.util.Arrays;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();

		// deklarace (dvou-rozmerneho) pole o velikosti 3x3 prvku,
		// kde vsechny prvky pole budou desetinna cisla, datoveho typu: float
		float[][] dvourozmernePole = new float[3][3];

		// ukazka naplneni pole cisly
		dvourozmernePole[0][0] = 0.0f;
		dvourozmernePole[0][1] = 0.1f;
		dvourozmernePole[0][2] = 0.2f;

		dvourozmernePole[1][0] = 1.0f;
		dvourozmernePole[1][1] = 1.1f;
		dvourozmernePole[1][2] = 1.2f;

		dvourozmernePole[2][0] = 2.0f;
		dvourozmernePole[2][1] = 2.1f;
		dvourozmernePole[2][2] = 2.2f;

		// kdyz se snazime vypsat pole na std-out,
		// zjistime ze nema implementovanou metodu: "toString()",
		// takze se nam vypise pointer na 1. prvek v poli.
		System.out.println(dvourozmernePole);

		// pomuzeme si "helperem", ktery je soucasti "java.util.Arrays",
		// kde zavolame metodu "toString()" a predame ji jako parametr nase pole[].
		// metoda projde postupne vsechny prvky pole a bude se je snazit vypsat... :/
		// (bohuzel metoda neni pripravena na 2x a vice rozmerna pole!)
		System.out.println(Arrays.toString(dvourozmernePole));

		// abych prvky vypsal spravne, pouziju cyklus: for (~protoze znam presny pocet prvku pole)
		for(int radek = 0; radek < 3; radek++)
		{
			for(int sloupec = 0; sloupec < 3; sloupec++)
			{
				System.out.printf("%.1f ", dvourozmernePole[radek][sloupec]);
			}
			System.out.println();
		}
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("pole (~anglicky: array) sdruzuje konecny pocet prvku (cisel, retezcu, objektu...) stejneho datoveho typu!");
		System.out.println();
		System.out.println("- velikost pole musi byt znama predem (~tj. delka pole)");
		System.out.println("- datovy typ pole musi byt znam predem (~tj. vsechny prvky jsou stejneho datoveho typu)");
		System.out.println("- pole nelze adresovat mimo jeho velikost! (~tj. delku pole)");
		System.out.println();
		System.out.println("- dvou-rozmerne pole se deklaruje \"[][]\" (2x2x hranatyma zavorkama!)");
		System.out.println(   "(1. cislo udava pocet radku; 2. cislo udava pocet sloupcu)");
		System.out.println(   "(toto rozlozeni muze byt zamenitelne, podle potreb programu a vule programatora)");
		System.out.println("-----------------------------");
	}
}
