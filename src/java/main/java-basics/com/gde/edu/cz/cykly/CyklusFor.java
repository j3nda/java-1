package com.gde.edu.cz.cykly;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CyklusFor
{
	public static void main(String[] args)
	{
		napoveda();

		ukazkaCykluFor1_klasickyAnsiC_zapis("ansi-c zapis--------: ");
		ukazkaCykluFor2_typickyJava_zapis_pole("java zapis----------: ");
		ukazkaCykluFor2_typickyJava_zapis_kolekce1("java zapis(kolekce1): ");
		ukazkaCykluFor2_typickyJava_zapis_kolekce2("java zapis(kolekce2): ");
		ukazkaCykluDoWhile_pocitaniZivotu("pocitani zivotu-----: ");
	}

	private static void ukazkaCykluFor1_klasickyAnsiC_zapis(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		int[] poleCisel = new int[5];

		// naplneni pole - pomoci cyklu
		for(int i = 0  ; i < 5; i = i + 1)
		{
			// promenne: pole ~ \"poleCisel\", na pozici indexu ~ i ~ priradime nahodne cele cislo.
			// index: "i" predstavuje pocitadlo iteraci cykly.
			// (pocet opakovani je dany v zapise for(...) nahore: a bude se opakovat od 0 do 4, tj. 5x!)
			poleCisel[i] = getRandomNumber(0, 99);
		}

		// vypis pole
		// nad polem muzu volat ruzne vlasnosti, jako:
		// - 'poleCisel.length' = vraci celkovou velikost pole
		// - 'i++' je ekvivalent k 'i = i + 1', tzv. inkrement zprava
		for(int i = 0; i < poleCisel.length; i++)
		{
			System.out.print(
				poleCisel[i] // vypis cisla, ulozeneho v poli na pozici [i]

				// tzv. ternarni operator (~obdoba prikazu "if") vyhodnoceneho za behu
				// (podminka ? true : false)
				+ (i < poleCisel.length - 1 ? ", " : ".")
			);
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	/**
	 * tento zapis je vhodny pro iteraci nad kolekci, ktera ma iterator.
	 * jak vidime z vystupu: pri jednotlive iteraci muzeme z pole pouze cist, nikoliv do nej zapsat!
	 * proc? protoze pri prekladu se z: "int[] poleCisel" udela read-only kolekce, ktera nese iterator.
	 */
	private static void ukazkaCykluFor2_typickyJava_zapis_pole(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		int[] poleCisel = new int[5];

		// naplneni pole - pomoci cyklu
		// nad polem muzeme volat ruzne vlasnosti, jako:
		// - poleCisel.length = vraci celkovou velikost pole
		// ! vsimnete si "WARNINGu" ide/eclipse u "cislo_z_aktualni_iterace"
		for(int cislo_z_aktualni_iterace : poleCisel)
		{
			// promenna: cislo_z_aktualni_iterace obsahuje hodnotu z promenne pole ~ \"poleCisel\", pro aktualni iteraci.
			// pocet opakovani se zde automaticky zjisti z: poleCisel.length!
			cislo_z_aktualni_iterace = getRandomNumber(0, 99);

			// vsimni si, ze: cislo se sice priradi do promenne "cislo_z_aktualni_iterace",
			//   ale ve vypisu nize zobrazuje "0"!
			// je to tim, ze cyklus for() se prochazi nad read-only kolekci, tj.
			//   - vygeneruje se nahodne cislo
			//   - a ulozi do promenne "cislo_z_aktualni_iterace"
			//   ! ale uz se neulozi do poleCisel[index]!
			//     tj. jakmile opusti {} zavorky, informace o nahodnem cisle se ztrati
			//     a vypise se "0" nula (~nebot nove pole celych cisel se vzdy inicializuje s "0" nulou)
		}

		// vypis pole
		for(int cislo_z_aktualni_iterace : poleCisel)
		{
			System.out.print(cislo_z_aktualni_iterace + ", ");
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	/**
	 * tento zapis je vhodny pro iteraci nad kolekci, ktera ma iterator.
	 * v tomto zapise vidime, ze muzeme pouzit "iterator", ktery drzi aktualni pocitadlo iteraci.
	 */
	private static void ukazkaCykluFor2_typickyJava_zapis_kolekce1(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		List<Integer> kolekceCisel = new ArrayList<>();

		// naplneni pole - pomoci cyklu
		// nad polem muzeme volat ruzne vlasnosti, jako:
		// - poleCisel.length = vraci celkovou velikost pole
		for(int i = 0; i < 5; i++)
		{
			kolekceCisel.add(getRandomNumber(0, 99));
		}

		// vypis pole
		for(int cislo_z_aktualni_iterace : kolekceCisel)
		{
			System.out.print(
				cislo_z_aktualni_iterace
				+ (kolekceCisel.iterator().hasNext() ? ", " : ".")
			);
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	private static void ukazkaCykluFor2_typickyJava_zapis_kolekce2(String title)
	{
		System.out.print(title);

		// deklarace 1x rozmerneho pole celych cisel
		List<Integer> kolekceCisel = new ArrayList<>();

		// kolekci naplnime na 1. pohled krkolomne...
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));
		kolekceCisel.add(getRandomNumber(0, 99));

		// vypis pole za pouziti iteratoru
		for(Iterator<Integer> iterator = kolekceCisel.iterator(); iterator.hasNext();)
		{
			System.out.print(
				iterator.next()
				+ (iterator.hasNext() ? ", " : ".")
			);
		}
		System.out.println(); // po dokonceni vypisu cisel v radku, odradkujeme \n
	}

	private static void ukazkaCykluDoWhile_pocitaniZivotu(String title)
	{
		System.out.println(title);
		for(int zivoty = 10; zivoty >= 0; zivoty = zivoty - 1)
		{
		    System.out.println("- zivoty: " + zivoty);
		    if (zivoty == 0)
		    {
		    	System.out.println("GAME OVER");
		    }
		}
		System.out.println("-");
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("for ~ tento cyklus pouzijeme, pokud zname presny pocet opakovani.");
		System.out.println("-----------------------------");
	}
}
