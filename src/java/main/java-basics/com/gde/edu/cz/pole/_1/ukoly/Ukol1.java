package com.gde.edu.cz.pole._1.ukoly;

public class Ukol1
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: deklarace pole (dle zadani)
		// napoveda: ???[] mojePole = new ???[5];

		// TODO: naplneni pole (dle zadani)
		// napoveda: ???[0] = 1159;

		// TODO: vypsani pole (dle zadani)

		// TODO: Bonus (dle zadani)
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor 1x rozmerne pole retezcu o delce 5x slov.");
		System.out.println("2) toto pole napln slovy: krestni-jmena, druhy-stromu anebo nazvy-jidel.");
		System.out.println("3) takto naplnene pole vypis na std-out.");
		System.out.println();
		System.out.println("Bonus:");
		System.out.println("- naplnene pole vypis v obracenem poradi");
		System.out.println("-----------------------------");
	}
}
