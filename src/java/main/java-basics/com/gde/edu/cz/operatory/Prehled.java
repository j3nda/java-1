package com.gde.edu.cz.operatory;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();

		ukazka_inkrement();
		ukazka_bitovyPosun();
		ukazka_bitovaNegace();
	}

	private static void ukazka_inkrement()
	{
		System.out.println("===[ ukazka: inkrement");

		// inkrement
		int x = 5;
		System.out.println("x = " + x + "; // pred inkrementaci");

		x++; // pricte +1 a priradi do 'x', tj. x == 6
		System.out.println("x = " + x + "; // po  inkrementaci");
		System.out.println();

		int a = 10;
		int b = 10;
		// inkrement (zleva)
		System.out.println("a = " + a + "; // pred inkrementaci (zleva)");
		System.out.println("++a;" + "    // vykonavam... hodnota: " + (++a) + " (~behem vykonavani)");
		System.out.println("a = " + a + "; // po   inkrementaci (zleva)");
		System.out.println();

		// inkrement (zprava)
		System.out.println("b = " + b + "; // pred inkrementaci (zprava)");
		System.out.println("b++;" + "    // vykonavam... hodnota: " + (b++) + " (~behem vykonavani)");
		System.out.println("b = " + b + "; // po   inkrementaci (zprava)");
		System.out.println();
	}

	private static void ukazka_bitovyPosun()
	{
		System.out.println("===[ ukazka: bitovy posun");

		int x = 7;
		System.out.println("int x = " + x + ";");
		System.out.println();

		String binaryX = Integer.toString(x, 2);
		System.out.println("dec(x) = " + Zarovnani.pad(x + "", 8, " ", ZarovnaniType.VLEVO) + " // desitkove");
		System.out.println("bin(x) = " + Zarovnani.pad(binaryX, 8, "0", ZarovnaniType.VPRAVO) + " // binarne");
		System.out.println();

		int y = x << 3;
		System.out.println("int y = x << 3;   // vykonavam... bitovy posun o 3 bity vlevo");
		System.out.println();

		String binaryY = Integer.toString(y, 2);
		System.out.println("dec(y) = " + Zarovnani.pad(y + "", 8, " ", ZarovnaniType.VLEVO) + " // desitkove");
		System.out.println("bin(y) = " + Zarovnani.pad(binaryY, 8, "0", ZarovnaniType.VPRAVO) + " // binarne");
		System.out.println();
	}

	private static void ukazka_bitovaNegace()
	{
		System.out.println("===[ ukazka: bitova negace (~bitovy doplnek)");

		int x = 7;
		System.out.println("int x = " + x + ";");
		System.out.println();

		String binaryX = Integer.toString(x, 2);
		System.out.println("dec(x) = " + Zarovnani.pad(x + "", 8, " ", ZarovnaniType.VLEVO) + " // desitkove");
		System.out.println("bin(x) = " + Zarovnani.pad(binaryX, 8, "0", ZarovnaniType.VPRAVO) + " // binarne");
		System.out.println();

		int y = ~x;
		System.out.println("int y = ~x;       // vykonavam... bitova negace");
		System.out.println();

		String binaryY = Integer.toString(y, 2);
//		System.out.println("bin(y) = " + Zarovnani.pad(binaryY, 8, "0", ZarovnaniType.VPRAVO) + " // binarne   (signed)");
		System.out.println("bin(y) = " + Zarovnani.pad(Integer.toString(y & 0xff, 2), 8, "0", ZarovnaniType.VPRAVO) + " // binarne");
		System.out.println("dec(y) = " + Zarovnani.pad(y + "", 8, " ", ZarovnaniType.VLEVO) + " // desitkove");
		System.out.println();
	}

	private static void napoveda(String operator, String nazev, String ukazka)
	{
		System.out.println(""
			+ Zarovnani.pad(operator, Zarovnani.OPERATOR, Zarovnani.ZNAK, ZarovnaniType.NA_STRED)
			+ " | " + Zarovnani.pad(nazev, Zarovnani.NAZEV, Zarovnani.ZNAK, ZarovnaniType.VLEVO)
			+ " | " + Zarovnani.pad(ukazka, Zarovnani.UKAZKA, Zarovnani.ZNAK, ZarovnaniType.NA_STRED)
		);
	}

	private static void napoveda(String titulek)
	{
		System.out.println();
		System.out.println("===[ " + titulek);
		napoveda("operator", "popis", "ukazka");
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");

		napoveda("aritmeticke");
		napoveda("+",  "pricitani", "x + y");
		napoveda("-",  "odecitani", "x - y");
		napoveda("*",  "nasobeni",  "x * y");
		napoveda("/",  "deleni",    "x / y");
		napoveda("%",  "modulo (~zbytek po deleni)", "x % y");
		napoveda("++", "inkrement (~zvysi hodnotu o +1)", "x++ (nebo) ++x");
		napoveda("+-", "dekrement (~snizi hodnotu o -1)", "x-- (nebo) --x");

		napoveda("bitove");
		napoveda("&",  "bitovy AND",    "x & y");
		napoveda("|",  "bitovy OR",     "x | y");
		napoveda("^",  "bitovy XOR",    "x ^ y");
		napoveda("~",  "bitova negace", "~x");

		napoveda("bitoveho posunu");
		napoveda("<<", "bitovy posun (vlevo)  o \"y\" bitu", "x << y");
		napoveda(">>", "bitovy posun (vpravo) o \"y\" bitu", "x >> y");

		napoveda("logicke");
		napoveda("!",  "logicka negace",          "!(x < 5 && x < 10)");
		napoveda("&&", "logicke AND (a-zaroven)", "x < 5 && x < 10");
		napoveda("||", "logicke OR (anebo)",      "x < 5 || x > 10");

		napoveda("porovnavaci");
		napoveda("==", "je   rovno", "x == y");
		napoveda("!=", "neni rovno", "x != y");
		napoveda("<",  "je mensi", "x < y");
		napoveda("<=", "je mensi nebo rovno", "x <= y");
		napoveda(">",  "je vetsi", "x == y");
		napoveda("<=", "je vetsi nebo rovno", "x >= y");

		napoveda("prirazovaci");
		napoveda("+=", "pricte a priradi",   "x += 3 (ekvivalent) x = x + 3");
		napoveda("-=", "odecte a priradi",   "x -= 3 (ekvivalent) x = x - 3");
		napoveda("*=", "vynasobi a priradi", "x *= 3 (ekvivalent) x = x * 3");
		napoveda("/=", "vydeli a priradi",   "x /= 3 (ekvivalent) x = x / 3");
		napoveda("%=", "zbytek po deleni a priradi", "x %= 3 (ekvivalent) x = x % 3");

		napoveda("<<=", "bitove posune o bity (vlevo) a priradi", "x <<= 3 (ekvivalent) x = x << 3");
		napoveda(">>=", "bitove posune o bity (vpravo) a priradi", "x >>= 3 (ekvivalent) x = x >> 3");

		System.out.println("-----------------------------");
	}

	private enum ZarovnaniType { VLEVO, VPRAVO, NA_STRED }
	/**
	 * php str_pad() function implementation in java
	 * @see https://stackoverflow.com/questions/34290981/str-pad-implemented-in-java
	 */
	private static class Zarovnani
	{
		public static final String ZNAK = " ";
		private static final int OPERATOR = 8;
		private static final int NAZEV = 40;
		private static final int UKAZKA = 32;

		static String pad(String input, int length, String padChar, ZarovnaniType padType)
		{
			int resto_pad = length - input.length();
			String padded = "";

			if (resto_pad <= 0)
			{
				return input;
			}

			if (padType == ZarovnaniType.VLEVO)
			{
				padded = input;
				padded += fillString(padChar, resto_pad);
			}
			else
				if (padType == ZarovnaniType.VPRAVO)
				{
					padded = fillString(padChar, resto_pad);
					padded += input;
				}
				else // STR_PAD_BOTH
				{
					int pad_left = (int) Math.ceil(resto_pad / 2);
					int pad_right = resto_pad - pad_left;

					padded = fillString(padChar, pad_left);
					padded += input;
					padded += fillString(padChar, pad_right);
				}
			return padded;
		}

		private static String fillString(String pad, int resto)
		{
			boolean first = true;
			String padded = "";

			if (resto >= pad.length())
			{
				for (int i = resto; i >= 0; i = i - pad.length())
				{
					if (i >= pad.length())
					{
						if (first)
						{
							padded = pad;
						}
						else
						{
							padded += pad;
						}
					}
					else
					{
						if (first)
						{
							padded = pad.substring(0, i);
						}
						else
						{
							padded += pad.substring(0, i);
						}
					}
					first = false;
				}
			}
			else
			{
				padded = pad.substring(0, resto);
			}
			return padded;
		}
	}
}
