package com.gde.edu.cz.rizenitoku.ukoly;

import com.gde.edu.cz.rizenitoku.Prehled;

public class Ukol5_switch
{
	public static void main(String[] args)
	{
		napoveda();

		// do promennych priradim nahodne hodnoty, z rozsahu definovanym parametry
		int mesic = vratNahodneCislo(1, 12);
		String zvire = vratNahodneZvire(new String[]{
			"pes", "kocka", "osel", "krava", "husa", "kun", "slepice", "koza", "kralik", "prase", "kohout", "ovce"
		});

		// TODO: viz napoveda
		// 1) vypis nazev mesice podle cisla

		// 2) zjisti a vypis, jake zvuky zvire vydava");
		//    (uvazuj zvirata: pes, kocka, osel, krava, a vsechny ostatni domaci zvirata)
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	private static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	/**
	 * vybere nahodne zvire (a podle potreb ukolu) zmeni velka/mala pismena.
	 * @param zvirata
	 * @return
	 */
	private static String vratNahodneZvire(String[] zvirata)
	{
		String nahodneZvire = Prehled.getRandomString(zvirata);
		switch(Prehled.getRandomNumber(0, 2))
		{
			case 0: return nahodneZvire.toLowerCase();
			case 1: return nahodneZvire.toUpperCase();
			default:
			{
				int max = Prehled.getRandomNumber(1, nahodneZvire.length());
				for(int i = 0; i < max; i++)
				{
					int letter = Prehled.getRandomNumber(1, (nahodneZvire.length() - 2));
					nahodneZvire = nahodneZvire.substring(0, letter)
						+ nahodneZvire.substring(letter, letter + 1).toUpperCase()
						+ nahodneZvire.substring(letter + 1)
					;
				}
				break;
			}
		}
		return nahodneZvire;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vypis nazev mesice podle cisla");
		System.out.println("   NAPOVEDA: k reseni pouzij prikaz \"switch\"");
		System.out.println();
		System.out.println("2) zjisti a vypis, jake zvuky zvire vydava");
		System.out.println("   (uvazuj zvirata: pes, kocka, osel, krava, a vsechny ostatni domaci zvirata)");
		System.out.println("   NAPOVEDA:");
		System.out.println("   pes   ~ haf, haf");
		System.out.println("   kocka ~ mnau mnau");
		System.out.println("-----------------------------");
	}
}
