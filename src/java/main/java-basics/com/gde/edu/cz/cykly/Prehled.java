package com.gde.edu.cz.cykly;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("cykly slouzi k opakovanemu provadeni prikazu.");
		System.out.println();
		System.out.println("k dispozici mame 3x druhy cyklu:");
		System.out.println("- for(pocet-opakovani) ~ zname presny pocet opakovani.");
		System.out.println("- while(condition) ~ vykonavame dokud podminka \"condition\" je pravda (~true).");
		System.out.println("- do...while(condition) ~ obdoba \"while\" s tim rozdilem, ze podminka je na konci,");
		System.out.println("  tj, 1. iterace cyklu se PROVEDE VZDY! protoze podminka je na konci!");
		System.out.println("-----------------------------");
	}
}
