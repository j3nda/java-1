package com.gde.edu.cz.rizenitoku;

public class Prehled
{
	public static void main(String[] args)
	{
		napoveda();

		// podminka (true) se da prelozit jako: 1 == 1 ~ pravdivostni hodnota: PRAVDA
		// tj. podminka je vyhodnocena kladne, provede se nasl. blok kodu!
		if (true)
		{
			System.out.println("podminkaKteraSeProvedeVzdy: tento text se vypise vzdy!");
		}


		// rychla ukazka prikazu 'switch'
		int bebe = 1639;
		switch(bebe)
		{
			// pripad pro: (bebe == 1639)
			case 1639:
				System.out.println("Switch: Nasel jsem BeBe!");
				break;

			// vsechny neuvedene pripady
			default:
			{
				System.out.println("Switch: Vsechny ostatni pripady neuvedene jako \"case\"...");
				break;
			}
		}
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	/**
	 * vrati nahodny retezec z rozsahu definovanem pomoci pole.
	 *
	 * @param animals
	 * @return
	 */
	public static String getRandomString(String[] animals)
	{
		return animals[getRandomNumber(0, (animals.length - 1))];
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("rizeni toku informaci ~ chcete li: chovani aplikace.");
		System.out.println("(existuji 2x zakladni pristupy)");
		System.out.println();
		System.out.println("1) pomoci prikazu \"if\" (podminka)");
		System.out.println("   pomoci prikazu \"switch\" (hodnota) ...podminky...");
		System.out.println("2) pomoci chovani objektu a odchytavani vyjimek");
		System.out.println("   (~tento pristup si ukazeme v objekty/oop sekci)");
		System.out.println("-----------------------------");
	}
}
