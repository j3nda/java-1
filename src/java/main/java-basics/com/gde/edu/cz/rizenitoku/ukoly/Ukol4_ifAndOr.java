package com.gde.edu.cz.rizenitoku.ukoly;

import com.gde.edu.cz.rizenitoku.Prehled;

public class Ukol4_ifAndOr
{
	public static void main(String[] args)
	{
		napoveda();

		// do promennych priradim nahodne logicke hodnoty: true x false
		boolean a = (vratNahodneCislo(0, 1) == 1 ? true : false);
		boolean b = (vratNahodneCislo(0, 1) == 1 ? true : false);

		// TODO: viz napoveda
		// 1) vypis pravdivostni tabulku pro: 2x logicke hodnoty
		// - "a" a zaroven "b"
		// - "a" anebo "b"
		// - "a" je negace "b"


		// 2) zjisti, zda pozice kurzoru[x,y] je anebo neni v definovanem obdelniku[x,y,sirka,vyska]
		int kx = vratNahodneCislo(0, 320); // kurzor: x-ova souradnice
		int ky = vratNahodneCislo(0, 200); // kurzor: y-ova souradnice
		// obdelnik
		int ox = vratNahodneCislo(0, 220); // obdelnik: x-ova souradnice
		int oy = vratNahodneCislo(0, 100); // obdelnik: y-ova souradnice
		int osirka = vratNahodneCislo(10, 100); // obdelnik: sirka
		int ovyska = vratNahodneCislo(10, 100); // obdelnik: vyska

		// TODO: vypis souradnice kurzoru, napr: "Kurzor: [x,y]"
		// TODO: vypis souradnice obdelniku, napr: "Obdelnik: [x1,y1,x2,y2]"
		// TODO: zjisti, zda obdelnik je ctverec?
		// TODO: zjisti, zda kurzor je uvnitr obdelniku - tuto informaci vypis
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	private static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vypis pravdivostni tabulku pro: 2x logicke hodnoty");
		System.out.println("   NAPOVEDA:");
		System.out.println("   - pravdivostni tabulka zobrazuje vsechny mozne kombinace logickych hodnot");
		System.out.println("   - plus jejich vazby jako: AND a OR a NOT");
		System.out.println();
		System.out.println("   priklad: a = true, b = true; potom: a && b => bude true");
		System.out.println();
		System.out.println("2) zjisti, zda pozice kurzoru[x,y] je anebo neni v definovanem obdelniku[x,y,sirka,vyska]");
		System.out.println("   BONUS: zjisti, zda obdelnik je ctverec?");
		System.out.println("-----------------------------");
	}
}
