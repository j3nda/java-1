package com.gde.edu.cz.rizenitoku.ukoly;

import com.gde.edu.cz.rizenitoku.Prehled;

public class Ukol1_if
{
	public static void main(String[] args)
	{
		napoveda();

		// do promennych "a", "b", "c" priradim nahodne cislo z rozsahu (uvedenem parametry)
		int a = vratNahodneCislo(1, 45);
		int b = vratNahodneCislo(0, 30);
		int c = vratNahodneCislo(-15, 15);

		// TODO: viz napoveda
		// 1) ktere cislo je nejvetsi?

		// 2) ktere cislo je nejmensi?

		// 3) vypis, zda je cislo: sude nebo liche.
		// - cislo v promenne "a" je sude? nebo liche?
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 */
	private static int vratNahodneCislo(int min, int max)
	{
		return Prehled.getRandomNumber(min, max);
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) ktere cislo je nejvetsi? (max)");
		System.out.println("2) ktere cislo je nejmensi? (min)");
		System.out.println();
		System.out.println("3) vypis, zda je cislo: sude nebo liche.");
		System.out.println("   NAPOVEDA: ke zjisteni pouzij operator \"%\" ~ modulo, tj. zbytek po deleni");
		System.out.println("-----------------------------");
	}
}
