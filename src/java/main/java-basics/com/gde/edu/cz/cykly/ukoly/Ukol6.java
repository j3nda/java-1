package com.gde.edu.cz.cykly.ukoly;

public class Ukol6
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1) vypis celou ASCII tabulku 0-255
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vypis celou ASCII tabulku 0-255 ve formatu:");
		System.out.println("   @see: http://www.asciitable.com/");
		System.out.println("\tDec\tHex\tOct\tChar\tBin");
		System.out.println();
		System.out.println("NAPOVEDA:");
		System.out.println("- netisknutelne znaky zobraz jako \"NULL\"");
		System.out.println("- Bin ~ Binary ~ dvojkova soustava");
		System.out.println("- Oct ~ Octal ~ osmickova soustava");
		System.out.println("- Dec ~ Decimal ~ desitkova soustava");
		System.out.println("- Hex ~ Hexadecimal ~ sesnactkova soustava");
		System.out.println("-----------------------------");
	}
}
