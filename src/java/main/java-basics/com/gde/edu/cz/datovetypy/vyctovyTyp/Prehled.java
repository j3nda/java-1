package com.gde.edu.cz.datovetypy.vyctovyTyp;

import java.text.MessageFormat;

public class Prehled
{
	// ukazka: ze vyctovy typ muzu nadefinovat i jako konstantu
	static final int PONDELI = 1;
	static final int UTERY = 2;

	/** ukazka: ze vyctovy typ muzu nadefinovat jako 'vyctovy typ', tzv. enum */
	enum PracovniTyden {
		Pondeli, // vraci hodnotu: 0
		Utery,   // vraci hodnotu: 1
		Streda,  // vraci hodnotu: 2
		Ctvrtek, // vraci hodnotu: 3
		Patek    // vraci hodnotu: 4
	}

	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

		ukazka_denVpracovnimTydnu(PracovniTyden.Patek);

		ukazka_mesicVroce();
	}

	private static void ukazka_denVpracovnimTydnu(PracovniTyden denVpracovnimTydnu)
	{
		// datovy typ "PracovniTyden" predstavuje v jave "objekt",
		//   ktery je datoveho typu "enum", coz je specialni vyctovy datovy typ!
		// muzes si pod nim predstavit "konstantu" (~ciselnou nemennou hodnotu)
		//
		// PracovniTyden denVpracovnimTydnu = PracovniTyden.Patek; // zakomentovano: prijde jako vstupni parametr!

		// automaticky se zavola: PracovniTyden.toString();
		System.out.println(denVpracovnimTydnu);

		// vypis jmena/nazvu, tj. zavolani: PracovniTyden.name();
		System.out.println("den v pracovnim tydnu[nazev]: " + denVpracovnimTydnu.name());

		// vypis cisla "v poradi", tj. zavolani: PracovniTyden.name(); // cisluje se od 0!
		System.out.println("den v pracovnim tydnu[cislo]: " + denVpracovnimTydnu.ordinal());

		// mohu i porovnavat, viz: Lekce7
		if (denVpracovnimTydnu == PracovniTyden.Patek) { System.out.println("JE PATEK"); } // ukazka: if!

		switch (denVpracovnimTydnu) { case Patek: System.out.println("JE PATEK"); break; } // ukazka: switch!
	}

	private static void ukazka_mesicVroce()
	{
		System.out.println(Mesic.ZARI); // automaticky zavola: Mesic.ZARI.toString();

		// vypis vsech mesicu v roce
		for(Mesic mesic : Mesic.values())
		{
			System.out.println(mesic); // automaticky zavola: Mesic.toString();
		}
	}

	/** ukazka: vyctovy typ, se muze mit chovani, je to 'classa' */
	enum Mesic
	{
		LEDEN(1, "Leden"),
		UNOR(2, "Unor"),
		BREZEN(3, "Brezen"),
		DUBEN(4, "Duben"),
		KVETEN(5, "Kveten"),
		CERVEN(6, "Cerven"),
		CERVENEC(7, "Cervenec"),
		SRPEN(8, "Srpen"),
		ZARI(9, "Zari"),
		RIJEN(10, "Rijen"),
		LISTOPAD(11, "Listopad"),
		PROSINEC(12, "Prosinec")
		;
		private final String jmeno;
		private final int cislo;

		private Mesic(int cislo, String jmeno)
		{
			this.cislo = cislo;
			this.jmeno = jmeno;
		}

		@Override
		public String toString()
		{
			return MessageFormat.format(
				"'{'index: {0}, id: {1}, poradi: {2}, jmeno: {3}'}'",
				this.ordinal(),
				this.name(),
				this.cislo,
				this.jmeno
			);
		}
	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("Vyctovy datovy typ");
		System.out.println("- v jave je \"enum\" objekt!");
		System.out.println("- - jako objekt ma vsechny \"objektove\" vlastnosti");
		System.out.println("- obsahuje \"vyctove\" hodnoty: 0, 1, 2, 3, 4, 5, atd");
		System.out.println("\t- https://cs.wikipedia.org/wiki/V%C3%BD%C4%8Dtov%C3%BD_typ#V%C3%BD%C4%8Dtov%C3%BD_typ_v_jazyce_Java");
		System.out.println("\t- https://www.algoritmy.net/article/30320/Enum-19");
		System.out.println("-----------------------------");
	}
}
