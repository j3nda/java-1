package com.gde.edu.cz.promenne.retezce;

public class Prehled
{

	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

//		+-------- toto je datovy typ! @see: datovetypy.*
//		|
//		|             +--------------- toto je libovolne jmeno promenne [a-Z0-9_]
//		|             |
//		|             |        +------ toto je prirazovaci znamenko
//		|             |        |
//		|             |        |    +- toto je hodnota (cele cislo) ulozena do promenne
//		|             |        |    |
//		V        VVVVVVVVVVV   V  VVVVVVVVVVVV
		String promennaRetezec = "Hello World!";

		// poznamka: retezed se uvozuje uvozovkama (")


		// hodnotu promenne mohu vypsat, napr:
		System.out.println(promennaRetezec);

		// anebo ji porovnat, @see: datovetypy.cisla.Prehled
		if (promennaRetezec.contentEquals("Hello World!"))
		{
			// a vypsat na std-out (~standartni vystup)
			System.out.println("Retezec: \"" + promennaRetezec + "\"");
		}

		// muzu provadet "scitaci" operace, kdy "prictu" dalsi retezec!
		promennaRetezec = promennaRetezec + "! je vetsinou text, ktery zobrazi nas 1. program!";

		// a znovu vypisu obsah promenne. tentokrat obsahuje rozsirujici text!
		System.out.println(promennaRetezec);

		// datovy typ "String" je objekt, nad kterym mohu volat jeho metody
		// napr: delku retezce: <promenna>.length()
		System.out.println("Delka retezce \"" + promennaRetezec + "\" je: " + promennaRetezec.length() + "x znaku.");

		// anebo porovnavani: a zobrazit vystup, tj. false
		System.out.println(promennaRetezec.equals("nejaky jiny retezec"));
	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("promenna:");
		System.out.println("- slouzi k uchovani hodnoty, napr: cisla, retezce (anebo jine datove struktury)");
		System.out.println(
			"- vytvorim ji tak, ze si zvolim nejaky nazev, napr: \"honza\"\n"
			+ "\ta do nej budu chtit ulozit sve jmeno, napr: \"Bajaja\"\n"
			+ "\ta zapisu to takto:\n\n"
			+ "\tString honza = \"Bajaja\";"
		);
		System.out.println();
		System.out.println("-----------------------------");
	}
}
