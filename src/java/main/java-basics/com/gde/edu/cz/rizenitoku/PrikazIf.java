package com.gde.edu.cz.rizenitoku;

import java.text.MessageFormat;

public class PrikazIf
{
	public static void main(String[] args)
	{
		napoveda();

		// if (podminka)
		podminkaKteraSeProvedeVzdy();

		// if (podminka) {...} else {...}
		podminkaVcetneAlternativniVetve();

		// if (podminka) {...} else if {...} else if {...} else {...}
		podminkaVcetneViceAlternativnichVetvi();

		// if (podminka && (dalsiPodminka || jesteDalsiPodminka)) {...}
		kombinovaniVicePodminekDoJedne();

		// if (!podminka) { ... }
		podminkaNegace();
	}

	private static void podminkaKteraSeProvedeVzdy()
	{
		// podminka (true) se da prelozit jako: 1 == 1 ~ pravdivostni hodnota: PRAVDA
		// tj. podminka je vyhodnocena kladne, provede se nasl. blok kodu!
		if (true)
		{
			System.out.println("podminkaKteraSeProvedeVzdy: tento text se vypise vzdy!");
		}
	}

	private static void podminkaVcetneAlternativniVetve()
	{
		// vygenerujeme nahodne cislo v rozsahu: <0..50>
		int nahodneCislo = getRandomNumber(0, 50);

		// pouzijeme 'modulo' (~deleni se zbytkem) pro podminku
		// napr: cislo 6 % 2 == 0 (protoze celociselne deleni 6 / 2 == 3 a nema zadny zbytek)
		// napr: cislo 9 % 2 == 1 (protoze celociselne deleni 9 / 2 == 4 a ma zbytek 1 ~ protoze: 9 == 2 * 4 + 1)
		if (nahodneCislo % 2 == 0)
		{
			System.out.println(
				MessageFormat.format(
					"podminkaVcetneAlternativniVetve: nahodneCislo({0}) je {1}.",
					nahodneCislo,
					"sude"
				)
			);
		}
		else
		{
			System.out.println(
				MessageFormat.format(
					"podminkaVcetneAlternativniVetve: nahodneCislo({0}) je {1}.",
					nahodneCislo,
					"liche"
				)
			);
		}
	}

	private static void podminkaVcetneViceAlternativnichVetvi()
	{
		// vytvorime promennou "hodKostkou",
		// do ktere priradime nahodnou hodnotu <1..6>
		int hodKostkou = getRandomNumber(1, 6);

		// kdyz hazime kostkou a hodime 6ku, postavime novou figurku (jako v clovece-nezlob-se)
		boolean postavNovouFigurku = false;

		if (true == false)
		{
			System.out.println("podminkaVcetneViceAlternativnichVetvi: tento text se NIKDY nezobrazi!");
		}
		else
		if (hodKostkou % 6 == 0) // je to sestka?
		{
			System.out.println("podminkaVcetneViceAlternativnichVetvi: tento text se zobrazi, kdyz na kostce hodime 6 ku!");
			postavNovouFigurku = true;
		}
		else
		if (hodKostkou % 2 != 0) // je ruzno?, v matematice("<>"), v programovani("!=")
		{
			System.out.println(
				MessageFormat.format(
					"podminkaVcetneViceAlternativnichVetvi: tento text se zobrazi, kdyz na kostce hodime {0} liche cislo!",
					hodKostkou
				)
			);
		}
		else
		{
			System.out.println(
				MessageFormat.format(
					"podminkaVcetneViceAlternativnichVetvi: hodKostkou: {0}",
					hodKostkou
				)
			);
		}

		if (postavNovouFigurku)
		{
			System.out.println("podminkaVcetneViceAlternativnichVetvi: postavNovouFigurku: " + postavNovouFigurku);
		}
	}

	private static void kombinovaniVicePodminekDoJedne()
	{
		boolean svitiSlunicko = false;
		boolean jeZima = true;

		if ((1 == 1 && svitiSlunicko) || jeZima) // 1 == 1 A-ZAROVEN svitiSlunicko NEBO jeZima
		{
			if (svitiSlunicko)
			{
				System.out.println("sviti slunicko");
			}

			if (jeZima)
			{
				System.out.println("je zima");
			}
		}
		else
		{
			System.out.println("nesviti slunicko a neni zima.");
		}
	}

	private static void podminkaNegace()
	{
		boolean logickaPromennaJaNevim = true;

		// "!" vykricnik neguje logickou hodnotu
		logickaPromennaJaNevim = !logickaPromennaJaNevim; // !true >> false

		if ( ! logickaPromennaJaNevim || !(1 == 1) == false)
		{
			System.out.println("PRAVDA");
		}

		System.out.println("...");
	}

	/**
	 * vrati nahodne cele cislo v rozsahu definovanem pomoci {min, max}.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	private static int getRandomNumber(int min, int max)
	{
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("if (podminka) {...} else if {...} else if {...} else {...}");
		System.out.println();
		System.out.println("1) pokud \"podminka\" je pravda, vykona se blok kodu");
		System.out.println("2) \"podminka\" muze obsahovat logicke operatory");
		System.out.println();
		System.out.println("logicke operatory");
		System.out.println("  && ~ AND ~ a zaroven");
		System.out.println("  || ~ OR  ~ nebo");
		System.out.println("  !  ~ NOT ~ negace");
		System.out.println("-----------------------------");
	}
}
