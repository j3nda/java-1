package com.gde.edu.cz.datovetypy.objekty.krabice;

import java.util.ArrayList;
import java.util.List;

public class Krabice // predpis pro tridu 'krabice'
implements IKrabice  // implemenuje rozhranni 'IKrabice', ktere umi otevrit kazdou krabici
{
	/** data: tj. prostor pro ukladani veci do krabice */
	private List<String> data = new ArrayList<String>() {{
		add("stetec");
		add("lepidlo");
		add("globus");
	}};
	// atributy krabice (~tj. aby kazda krabice mohla byt jina)
	public final float hmotnost;
	private final float[] velikost = new float[3];

	/** parametricky konstruktor: ktery vytvari instanci objektu typu 'krabice' */
	public Krabice(float hmotnost, float delka, float sirka, float vyska)
	{
		this.hmotnost = hmotnost;
		velikost[0] = delka;
		velikost[1] = sirka;
		velikost[2] = vyska;
	}

	public void posun()
	{
		/** kod, ktery posune krabici */
	}

	public void prevrat()
	{
		System.out.print(this.getClass().getName() + ": ");
		if (hmotnost <= 50f)
		{
			System.out.println("prevracim...");
		}
		else
		{
			System.out.println("NELZE prevratit! (hmotnost je prilis velika!)");
		}
	}

	@Override
	public void otevri()
	{
		/** kod, ktery otevre krabici */
	}
}
