package com.gde.edu.cz.datovetypy.objekty;

public class Prehled
{
	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

		// datovy typ "Object" predstavuje v jave zakladni prvek,
		// od ktereho JSOU VZDY odvozeny vsechny ostatni objekty!
		//
		// objekt obsahuje sadu chovani (~metod), ktere jsou velmi uzitecne!
		Object mujObjektA = new Object();

		// jako:
		//
		// mujObjektA.toString() ~ prevede vnitrni obsah objektu do retezce
		//
		// mujObjektA.equals(Object ciziObjekt) ~ porovna 2x objekty a rekne, zda jsou stejne

		// POZNAMKA:
		// - v jave JE VSECHNO (mimo primitivni datove typy) OBJEKT!!!
		// - nad kterym muzu volat jeho metody (~tj. vyvolavat chovani objektu)

		// PRIKLAD:
		// datove typy:
		// - String, Integer, Float jsou objekty! (~s chytrejsim chovanim)
		// - char[], int, float ~ jsou primitivni datove typy!
		//
		// napr: zjisteni maximalniho CELEHO cisla, tj. int x Integer
		// - v pripade int (~bychom museli provest: naplneni 0 a udelat bitovou negaci)
		// - v pripade Integer (~to lze jednoduse: Integer.MAX_VALUE)

		System.out.println("Hello World!".length() + " ~ pocet znaku retezce(String)");
		System.out.println(Integer.MAX_VALUE + " ~ maximalni mozne cislo datoveho typu int");
		System.out.println(Float.MAX_VALUE + " ~ maximalni mozne cislo datoveho typu float");

		System.out.println(
			mujObjektA
			+ " ~ nema implementovanu metodu: toString(),"
			+ " takze zobrazi adresu v pameti, kde je objekt alokovan!"
		);

	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("Objekt");
		System.out.println("- v jave je vsechno objekt!");
		System.out.println("- objekt je neco \"jako krabicka, ktera obsahuje vlastni chovani\"");
		System.out.println("- docela vystizne zde:");
		System.out.println("\t- http://programujte.com/clanek/2007082501-java-tutorial-objekty-a-tridy-7-dil/");
		System.out.println("\t- https://www.algoritmy.net/article/21514/Objekty-5");
		System.out.println("\t- https://is.muni.cz/el/1433/podzim2007/PB162/um/02/printable.html");
		System.out.println("\t- http://www.cs.vsb.cz/benes/vyuka/upr/texty/java/ch01s02.html");
		System.out.println("-----------------------------");
	}
}
