package com.gde.edu.cz.datovetypy.cisla.ukoly;

public class Ukol4
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: viz napoveda
		// 1.1) cislo 25 je vetsi nez 39
		// 1.2) cislo 25 neni rovno 39
		// 1.3) cislo 25 je delitelne 5 bez zbytku
		// 1.4) cislo 39 je delitelne 5 bez zbytku
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) porovnej dve cisla, napr: 25 a 39 a vypis, zda je to pravda");
		System.out.println("1.1) cislo 25 je vetsi nez 39");
		System.out.println("1.2) cislo 25 neni rovno 39");
		System.out.println("1.3) cislo 25 je delitelne 5 bez zbytku");
		System.out.println("1.4) cislo 39 je delitelne 5 bez zbytku");
		System.out.println("-----------------------------");
	}
}
