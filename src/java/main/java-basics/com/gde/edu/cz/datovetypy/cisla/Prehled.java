package com.gde.edu.cz.datovetypy.cisla;

public class Prehled
{
	public static void main(String[] args)
	{
		// toto je volani metody, ktera se zobrazuje v panelu napravo v "Outline".
		// (klikni na "napoveda" v panelu napravo v "Outline" a pozoruj co se stane...)
		napoveda();

		celaCisla(); // ukazka celych cisel: byte, int, Integer, long, Long

		desetinnaCisla(); // ukazka desetinnych cisel: float, long, ...

		logickaHodnota_pravda_vs_nepravda(); // ukazka: pravda x nepravda

		datovyTyp_znak(); // ukazka: datovy typ: znak (~char)

		datovyTyp_autoBoxingUnboxing(); // ukazka: auto-boxing a un-boxing
	}

	/** ukazka celych cisel, napr: byte, int, Integer, long, Long */
	private static void celaCisla()
	{
		// 1x byte == 8x bitu (~1x bit = 0 nebo 1)
		//                             0b ~ dvojkova soustava
		//                               00101010
		//                      index:   76543210
		// - postupuju zprava doleva
		// - (0 * 2^0) + (1 * 2^1) + (2 * 2^3) + (1 * 2^5) = 0 + 2 + 8 + 32 = 42
		byte cisloVeDvojkoveSoustave = 0b00101010; // v binarni/dvojkove soustave
		System.out.println(cisloVeDvojkoveSoustave);

		String cisloVeDvojkoveSoustaveJakoRetezec = "1000001";
		int celeCislo1 = Integer.parseInt(cisloVeDvojkoveSoustaveJakoRetezec);
		int celeCislo2 = 'A';  // uvozeni (apostrof) -> znak(~char) se prevede automaticky na cislo(~byte)
		int celeCislo3 = 65;   // dec(65)
		int celeCislo4 = 0x41; // hex(0x41) == dec(65)
		int celeCislo5 = 0101; // oct(101) == dec(65)
		int celeCislo6 = cisloVeDvojkoveSoustave;
		Integer celeCislo7 = 65;
		long celeCislo8_zapsanoHezkySesnactkove = 0xFF_EC_DE_5E;
		long celeCislo9_zapsanoHezkySesnactkoveJakoSlova = 0xCAFE_BABE;
		long celeCislo10 = Long.MAX_VALUE;

		System.out.println("binarni cislo: " + cisloVeDvojkoveSoustaveJakoRetezec);
		System.out.println("cele cislo #1: " + celeCislo1
			+ " << BUG! (java si mysli, ze: \"cisloVeDvojkoveSoustaveJakoRetezec\" je ve skutecnosti integer! oprav to!)"
		);
		System.out.println("cele cislo #2: " + celeCislo2);
		System.out.println("cele cislo #3: " + celeCislo3);
		System.out.println("cele cislo #4: " + celeCislo4);
		System.out.println("cele cislo #5: " + celeCislo5);
		System.out.println("cele cislo #6: " + celeCislo6);
		System.out.println("cele cislo #7: " + celeCislo7);
		System.out.println("cele cislo #8: " + celeCislo8_zapsanoHezkySesnactkove);
		System.out.println("cele cislo #9: " + celeCislo9_zapsanoHezkySesnactkoveJakoSlova);
		System.out.println("cele cislo #10: " + celeCislo10);
		System.out.println();

		byte cisloJakoBajt1 = 'A';
		byte cisloJakoBajt2 = 65;
		byte cisloJakoBajt3 = (byte) 321;
		Byte cisloJakoBajt4 = (byte) 0101;

		System.out.println("cislo jako bajt #1: " + cisloJakoBajt1);
		System.out.println("cislo jako bajt #2: " + cisloJakoBajt2);
		System.out.println("cislo jako bajt #3: " + cisloJakoBajt3
			+ " << BUG? (hint: buffer overflow!)"
		);
		System.out.println("cislo jako bajt #4: " + cisloJakoBajt4);
		System.out.println();
	}

	/** ukazka desetinnych cisel, napr: float, Float, double, Double */
	private static void desetinnaCisla()
	{
		float desetinneCislo1 = 3.14159265359f;
		float desetinneCislo2 = -111.50f;
		float desetinneCislo3 = Float.MAX_VALUE;
		Float desetinneCislo4 = -1 * desetinneCislo3;
		float desetinneCislo5 = 0.15f; // mohu zapsat s (nulou) na zacatku
		float desetinneCislo6 = .15f;  // anebo bez (nuly) na zacatku

		System.out.println("desetinne cislo(float) #1: " + desetinneCislo1);
		System.out.println("desetinne cislo(float) #2: " + String.format("%.3f", desetinneCislo2));
		System.out.printf("desetinne cislo(float) #3: %,f\n", desetinneCislo3);
		System.out.printf("desetinne cislo(float) #3: %.18g\n", desetinneCislo3);
		System.out.printf("desetinne cislo(float) #3: %a\n", desetinneCislo3);
		System.out.printf("desetinne cislo(float) #4: %a\n", desetinneCislo4);
		System.out.printf("desetinne cislo(float) #5: " + desetinneCislo5);
		System.out.printf("desetinne cislo(float) #6: " + desetinneCislo6);
		System.out.println();


		double desetinneCisloSVyssiPresnosti1 = 3.14159265359d;
		double desetinneCisloSVyssiPresnosti2 = Float.MAX_VALUE;
		Double desetinneCisloSVyssiPresnosti3 = Double.MAX_VALUE;

		System.out.printf("desetinne cislo(double) #5: %,f\n", desetinneCisloSVyssiPresnosti1);
		System.out.printf("desetinne cislo(double) #5: %.18g\n", desetinneCisloSVyssiPresnosti2);
		System.out.printf("desetinne cislo(double) #6: %a\n", desetinneCisloSVyssiPresnosti2);
		System.out.printf("desetinne cislo(double) #6: %a\n", desetinneCisloSVyssiPresnosti2);
		System.out.printf("desetinne cislo(double) #7: %,f\n", desetinneCisloSVyssiPresnosti3);
		System.out.printf("desetinne cislo(double) #7: %.18g\n", desetinneCisloSVyssiPresnosti3);
		System.out.printf("desetinne cislo(double) #7: %a\n", desetinneCisloSVyssiPresnosti3);
	}

	private static void logickaHodnota_pravda_vs_nepravda()
	{
		boolean jeToPravda = true; // drzi pravdivostni hodnotu
		System.out.println("je to pravda? " + jeToPravda);
		if (jeToPravda)
		{
			System.out.println("ano je to pravda...");
		}

		jeToPravda = false;
		System.out.println("je to pravda? " + jeToPravda);
	}

	private static void datovyTyp_znak()
	{
		char pondeli1755 = 123;    // dec(123)
		char pondeli1756 = 'A';    // (apostrof)znak(apostrof) -> znak(ascii) -> cislo: 0..dec(65536), hex(FFFF)
		char pondeli1757 = '±';    // (apostrof)znak(apostrof) -> znak(utf-8) -> cislo: 0..dec(65536), hex(FFFF)
		char pondeli1758 = 0101;   // oct(101) == dec(65) == 'A'
		char pondeli1759 = 0xC2B1; // hex(C2 B1) == utf-8("\u00B1") == znak(±)
	}

	/** https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html */
	private static void datovyTyp_autoBoxingUnboxing()
	{
		// @see https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html
		// auto-boxing:
		// - je automaticka konverze datoveho typu mezi: primitivni x objektovy, napr:
		//   konverze: int   -> Integer
		//   konverze: float -> Float
		//   konverze: char  -> Character
		//
		// un-boxing:
		// - je opacny 'auto-boxing', tj. konverze objektoveho datoveho typu -> na -> primitivni, napr:
		//   konverze: Integer   -> int
		//   konverze: Float     -> float
		//   konverze: Character -> char
		//
		// oboje slouzi k psani cistejsiho a prehlednejsiho kodu
		//
		// auto-boxing a un-boxing je ZAPNUTY!
		// (zkuz jej vypnout v:
		//  window -> preferences ->
		//  -> java -> compiler -> error/warnings ->
		//  -> potential programming problems -> boxing and unboxing conversion -> Error (popr: warning anebo ignore)
		// )
		//                                              primitivni -> objektovy
		Integer   sobota1517 = 1517;   // auto-boxing(ON): int     -> Integer
		Float     sobota1518 = 15.18f; // auto-boxing(ON): float   -> Float
		Byte      sobota1519 = 0x65;   // auto-boxing(ON): byte    -> Byte
		Character sobota1520 = 'A';    // auto-boxing(ON): char    -> Character
		Boolean   sobota1521 = true;   // auto-boxing(ON): boolean -> Booelan

		// un-boxing:
		int     nedele1517 = sobota1517;
		float   nedele1518 = sobota1518;
		byte    nedele1519 = sobota1519;
		char    nedele1520 = sobota1520;
		boolean nedele1521 = sobota1521;
	}

	private static void napoveda()
	{
		// a ted klikni zpet na "main" v panelu napravo v "Outline" a pozoruj co se stane...
		System.out.println("---[ napoveda ]--------------");
		System.out.println("boolean~ <0, 1>            //  1-bit ~ true anebo false, ale \"velikost\" neni neco, co je presne definovane.");
		System.out.println("char   ~ <0, 65535>        // 16-bit unicode znak, od: \\u0000 do: \\uFFFF");
		System.out.println("byte   ~ <-128, 127>       //  8-bit signed two's complement integer.");
		System.out.println("short  ~ <-32768, 32767>   // 16-bit signed two's complement integer.");
		System.out.println("int    ~ <-2^31, 2^31 - 1> // 32-bit signed two's complement integer.");
		System.out.println("long   ~ <-2^63, 2^63 - 1> // 64-bit signed two's complement integer.");
		System.out.println("                           // ve verzi >= java8 je pouzit 64-bit long bez znamenka, <0, 2^64 - 1>!");
		System.out.println("float  ~ ±3.40282347E+38   // single-precision 32-bit IEEE 751 floating point.");
		System.out.println("                           // range: approximately ±3.40282347E+38F, (6-7 vyznamnych desetinnych cislic)");
		System.out.println("double ~ ±1.79769313E+308  // single-precision 64-bit IEEE 751 floating point.");
		System.out.println("                           // range: approximately ±1.79769313486231570E+308 (15 vyznamnych desetinnych cislic)");
		System.out.println("-----------------------------");
	}
}
