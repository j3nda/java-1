package com.gde.edu.cz.pole._2.ukoly;

public class Ukol1
{
	public static void main(String[] args)
	{
		napoveda();

		// TODO: deklarace pole (dle zadani)
		// napoveda: char[][] mojePole = new char[???][???];

		// TODO: naplneni pole (dle zadani)

		// TODO: vypsani pole (dle zadani)

		// TODO: Bonus (dle zadani)
	}

	private static void napoveda()
	{
		System.out.println("---[ napoveda ]--------------");
		System.out.println("1) vytvor 2x rozmerne pole znaku (~char) o velikosti 80x25 znaku.");
		System.out.println("2) toto pole napln znakem: '#'.");
		System.out.println("3) takto naplnene pole vypis na std-out.");
		System.out.println();
		System.out.println("Bonus:");
		System.out.println("- pro vypis pole nepouzivej pevne hodnoty (~tzv. hard-coded)");
		System.out.println("  namisto toho zjisti, kolik ma pole radku a sloupcu - a tento udaj pouzij pro vypis!");
		System.out.println("-----------------------------");
	}
}
