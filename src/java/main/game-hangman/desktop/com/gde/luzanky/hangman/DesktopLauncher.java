package com.gde.luzanky.hangman;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.gde.luzanky.hangman.game.HangmanGame;

/**
 * spoustec "obesence" na desktopu.
 * - obsahuje "static void main()" pro vytvoreni a spusteni hry.
 */
public class DesktopLauncher
{
	/**
	 * hlavni staticka metoda "main", kterou je mozne spustit pri startu.
	 * (automaticky se zavola napr: java -jar obesenec.jar)
	 *
	 * @param arg predstavuje pole parametru, ktere muzeme hre predat z prikazove radky
	 *            (napr: java -jar obesenec.jar --fullscreen)
	 */
	public static void main(String[] arg)
	{
		// vytvori nastaveni a ulozi jej do promenne "config"
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		config.setForegroundFPS(60);
		config.setWindowedMode(1024, 768);
		// TODO: zapni 'full-screen' rezim
		// TODO: nastav titulek aplikace na "Obesenec"
		// (popr. muzes vice prozkoumat tridu: "LwjglApplicationConfiguration" a experimentovat)

		// LibGdx pouziva ke spusteni na desktopu projekt Lwjgl (~Java OpenGL)
		// vytvori instanci: LwjglApplication, ktere pomoci parametru preda:
		// - new HangmanGame ~ nove vytvorenou instanci nasi hry obesenec
		// - config ~ nastaveni okna aplikace (velikost, titulek aj)
		// a aplikace se spusti...
		new Lwjgl3Application(new HangmanGame(), config);
	}
}
