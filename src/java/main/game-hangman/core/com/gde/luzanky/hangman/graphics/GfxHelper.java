package com.gde.luzanky.hangman.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.luzanky.hangman.graphics.display.DisplayLetter;
import com.gde.luzanky.hangman.graphics.display.DisplayPixelArray;
import com.gde.luzanky.hangman.graphics.display.DisplayRowLetters;


/** helper pro ovladani efektu (LibGdx.{@link Action}) nad jednotlivymi LibGdx.{@link Actor} */
public class GfxHelper
{
	/** true, pokud jsou graficke efekty povoleny */
	private static boolean enabled = true;
	/** vyctovy typ: smer efektu */
	public enum GfxDirection {
		LeftToRight,
		DownToUp,
		DownToUpLeftToRight,
		UpToDown,
		Reset,
	};


	/** vraci true, pokud jsou efekty povoleny. jinak vraci false. */
	public static boolean isEnabled()
	{
		return enabled;
	}

	/** gfx nad potvrzenim vyberu pismene */
	public static void confirmSelection(DisplayLetter button, DisplayLetter letter)
	{
		if (isEnabled())
		{
			button.addActionFgOnly(
				Actions.sequence(
					Actions.scaleTo(0.2f, 0.2f, 0.25f),
					Actions.scaleTo(1.0f, 1.0f, 0.20f)
				)
			);
			letter.addActionFgOnly(
				Actions.sequence(
					Actions.scaleTo(0.2f, 0.2f, 0.25f),
					Actions.scaleTo(1.0f, 1.0f, 0.20f)
				)
			);
		}
	}

	/** gfx nad zmenou pismene pomoci sipek */
	public static void scroll(DisplayLetter button, DisplayLetter letter)
	{
		if (isEnabled() && button != null)
		{
			button.addActionFgOnly(
				Actions.sequence(
					Actions.scaleTo(0.2f, 0.2f, 0.1f),
					Actions.scaleTo(1.0f, 1.0f, 0.2f)
				)
			);
			letter.addActionFgOnly(
				Actions.sequence(
					Actions.scaleTo(0.1f, 0.1f, 0.1f),
					Actions.scaleTo(1.0f, 1.0f, 0.2f)
				)
			);
		}
	}

	/** gfx nad zmacknutim A-Z pismene */
	public static void keyAZ(DisplayLetter letter)
	{
		if (isEnabled())
		{
			letter.addAction(
				Actions.sequence(
					Actions.scaleTo(0.2f, 0.2f, 0.25f),
					Actions.scaleTo(1.0f, 1.0f, 0.20f)
				)
			);
		}
	}

	/** gfx nad zobrazenim spravneho pismene v uhadnutem slove */
	public static void confirmLetter(DisplayRowLetters display)
	{
		if (isEnabled())
		{
			display.clearGfxActions();
			display.setGfxActionBefore(
				Actions.sequence(
					Actions.scaleTo(0.2f, 0.2f, 0.25f)
				)
			);
			display.setGfxActionAfter(
				Actions.sequence(
					Actions.scaleTo(1.0f, 1.0f, 0.20f)
				)
			);
		}
	}

	/** gfx nad zobrazenim obesence (jednotlive casti) */
	public static void hangmanReveal(DisplayPixelArray display, GfxDirection direction)
	{
		if (!isEnabled())
		{
			return;
		}
		float[] config = new float[] {
			0.2f,	// delay
			0.5f,	// duration
			0.0f,	// row(reverse?)
			0.0f,	// col(reverse?)
		};
		switch(direction)
		{
			case Reset:
			{
				display.clearUpdatedPixels();
				break;
			}
			case DownToUp:
			case LeftToRight:
			case DownToUpLeftToRight:
			{
				config[2] = 0;
				break;
			}
			case UpToDown:
			{
				config[2] = 1;
				break;
			}
			default:
				break;
		}
		float delayetStart = -1f;
		boolean[][] updatedPixels = display.getUpdatedPixels();
		for(int row = 0; row < display.getRows(); row++)
		{
			int rowIndex = (config[2] == 0.0f ? row : display.getRows() - row - 1);
			for(int col = 0; col < display.getCols(); col++)
			{
				int colIndex = (config[3] == 0.0f ? col : display.getCols() - col - 1);
				if (updatedPixels[rowIndex][colIndex])
				{
					if (delayetStart == -1f)
					{
						delayetStart = 0f;
					}
					else
					{
						delayetStart += config[0];
					}
					display.addActionPixelOnly(
						Actions.sequence(
							Actions.scaleTo(0.0f, 0.0f, 0.00f),
							Actions.delay(delayetStart),
							Actions.scaleTo(1.0f, 1.0f, config[1])
						),
						rowIndex,
						colIndex
					);
				}
			}
		}
	}

	/** gfx nad zobrazenim konce hry */
	public static void hangmanGameOver(final DisplayPixelArray display, final Color color)
	{
		if (isEnabled())
		{
			runCodeAfterActions(
				new Runnable()
				{
					@Override
					public void run()
					{
						display.addActionColorOnly(
							Actions.forever(
								Actions.sequence(
									Actions.delay(0.2f),
									Actions.scaleTo(1.0f, 1.0f, 0.25f),
									Actions.delay(0.2f),
									Actions.scaleTo(0.6f, 0.6f, 0.25f)
								)
							),
							color
						);
					}
				},
				display
			);
		}
	}

	/** gfx ze jsem klikl/tapnul na "hrat novou hru?" */
	public static void confirmNewGame(DisplayRowLetters display, boolean resetToOriginal)
	{
		if (isEnabled())
		{
			if (resetToOriginal)
			{
				display.addAction(
					Actions.sequence(
						Actions.scaleTo(1.0f, 1.0f, 0.10f)
					),
					true
				);
			}
			else
			{
				display.addAction(
					Actions.sequence(
						Actions.scaleTo(0.2f, 0.2f, 0.25f),
						Actions.scaleTo(1.0f, 1.0f, 0.20f)
					),
					true
				);
			}
		}
	}

	/** gfx nad "hrat novou hru?" (pri zobrazeni game-over) */
	public static void fadeInNewGame(Actor... actors)
	{
		if (isEnabled())
		{
			for(Actor actor : actors)
			{
				if (actor instanceof DisplayRowLetters)
				{
					((DisplayRowLetters)actor).addAction(
						Actions.sequence(
							Actions.scaleTo(0.0f, 0.0f, 0.0f),
							Actions.scaleTo(1.0f, 1.0f, 1.0f)
						),
						true
					);
				}
				else
				if (actor instanceof DisplayPixelArray)
				{
					((DisplayPixelArray)actor).addAction(
						Actions.sequence(
							Actions.scaleTo(0.0f, 0.0f, 0.0f),
							Actions.scaleTo(1.0f, 1.0f, 1.0f)
						),
						true
					);
				}
			}
		}
	}

	/** gfx nad game-over, tj. scena ze hry jde pryc... */
	public static void fadeInGameOver(Actor... actors)
	{
		if (isEnabled())
		{
			// prida efektove akce
			for(Actor actor : actors)
			{
				Action actions = Actions.sequence(
					Actions.scaleTo(0.0f, 0.0f, 0.8f)
				);
				if (actor instanceof DisplayRowLetters)
				{
					((DisplayRowLetters)actor).addAction(actions, true);
				}
				else
				if (actor instanceof DisplayPixelArray)
				{
					((DisplayPixelArray)actor).addAction(actions, true);
				}
			}
		}
	}

	/** spusti kod, pouze pokud herci nemaji zadne akce */
	public static void runCodeAfterActions(final Runnable runCode, final Actor... actors)
	{
		// a pocka, az vsechny efekty nad zminenymi "herci" dobehnou.
		// potom spusti kousek kodu predany pomoci: "Runnable run"
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					while(true)
					{
						int actions = 0;
						for(Actor actor : actors)
						{
							if (actor.hasActions())
							{
								actions++;
							}
						}
						if (actions == 0)
						{
							Gdx.app.postRunnable(runCode); // kod musi byt spusten v ramci OpenGL kontextu!
							break;
						}
						Thread.sleep(100);
					}
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	/** gfx nad spustenim nove hry */
	public static void startNewGame(Stage game, Stage gameover)
	{
		if (isEnabled())
		{
			// TODO: ukol/tezky: vyres efekt, ze zacne nova hra...
			// OPRAVDU NAROCNE! jako vstup dostanes:
			// - stage hry (~scena hercu, kteri patri pouze do hry)
			// - stage gameover (~scena hercu, kteri patri pouze do 'gameover')
			// a ty muzes udelat napr. animaci, ze stage 'game' odjede doleva
			// a zprava prijede stage 'gameover'
			// (anebo vymyslet svuj vlastni efekt. inspiruj se volanim nize - popr v jinych metodach teto tridy)

//			game.addAction(Actions.sequence(Actions.alpha(1), Actions.fadeIn(1)));
//			gameover.addAction(Actions.sequence(Actions.alpha(1), Actions.fadeOut(1)));
		}
	}
}
