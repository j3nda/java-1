package com.gde.luzanky.hangman.graphics.display;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.scenes.scene2d.actions.ActionsUtils;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;
import com.gde.luzanky.hangman.graphics.display.DisplayPixelArray.DisplayRenderMode;


/** zobrazi 1x slovo (~nekolik pismen) na 1x radku */
public class DisplayRowLetters
extends Actor
{
	/** pole displayu zobrazujicich pismena slova */
	private final DisplayLetter[] displayLetters;
	/** barva popredi */
	private Color fgColor;
	/** barva pozadi */
	private Color bgColor;
	/** akce pro graficky efekt: 'pred' upravou pismene */
	private Action updateLetterGfxBefore;
	/** akce pro graficky efekt: 'po' uprave pismene */
	private Action updateLetterGfxAfter;


	public DisplayRowLetters(BitFontMaker2Configuration fontConfiguration, int wordLength)
	{
		// zavolani konstruktoru s vice parametry
		// (nektere parametry jsou zde 'default', napr: Color.WHITE)
		this(fontConfiguration, wordLength, Color.WHITE, Color.BLACK);
	}

	public DisplayRowLetters(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor,
		Color bgColor
	)
	{
		this.displayLetters = new DisplayLetter[length];
		this.fgColor = fgColor;
		this.bgColor = bgColor;
		clearGfxActions();
		createDisplay(fontConfiguration, length);
		setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
			)
		);
	}

	@Override
	public void addAction(Action action)
	{
		addAction(action, true);
	}


	public void addAction(Action action, boolean delegateInto)
	{
		super.addAction(action);
		if (delegateInto)
		{
			for(DisplayLetter letter : displayLetters)
			{
				letter.addAction(
					ActionsUtils.newInstance(action),
					delegateInto
				);
			}
		}
	}

	@Override
	public boolean hasActions()
	{
		int actions = 0;
		if (super.hasActions())
		{
			actions += super.getActions().size;
		}
		for(DisplayLetter letter : displayLetters)
		{
			if (letter.hasActions())
			{
				actions++;
			}
		}
		return (actions > 0 ? true : false);
	}

	/** nastavi pozici a velikost displaye (a prepocte vsechny dulezite veci) */
	public void setViewport(Rectangle viewport)
	{
		float letterX = 0;
		float letterWidth = viewport.width / displayLetters.length;
		float rowHeight = viewport.height;

		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].setViewport(
				new Rectangle(
					viewport.x + letterX,
					viewport.y,
					letterWidth,
					rowHeight
				)
			);
			letterX += letterWidth;
		}

		setWidth(viewport.width);
		setHeight(viewport.height);
		setPosition(viewport.x, viewport.y);
	}

	private void createDisplay(BitFontMaker2Configuration fontConfiguration, int length)
	{
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i] = new DisplayLetter(fontConfiguration);
			displayLetters[i].setRenderMode(DisplayRenderMode.NoBackground);
		}
	}

	/** zaktualizuje pismeno na dane pozici na pozadovane pismeno (~zadane ascii kodem) */
	public void updateLetter(int index, int asciiCode)
	{
		updateLetter(index, asciiCode, fgColor, bgColor);
	}

	public void updateLetter(int index, int asciiCode, Color fgColor)
	{
		updateLetter(index, asciiCode, fgColor, bgColor);
	}

	public void updateLetter(int index, final int asciiCode, final Color fgColor, final Color bgColor)
	{
		if (index < 0 || index >= displayLetters.length)
		{
			return;
		}
		final DisplayLetter singleLetter = displayLetters[index];
		if (updateLetterGfxBefore == null && updateLetterGfxAfter == null)
		{
			singleLetter.update(fgColor, asciiCode, bgColor);
		}
		else
		{
			SequenceAction actions = Actions.sequence();
			if (updateLetterGfxBefore != null)
			{
				actions.addAction(updateLetterGfxBefore);
			}
			actions.addAction(Actions.run(
				new Runnable()
				{
					@Override
					public void run()
					{
						singleLetter.update(fgColor, asciiCode, bgColor);
					}
				}
			));
			if (updateLetterGfxAfter != null)
			{
				actions.addAction(updateLetterGfxAfter);
			}
			if (actions.getActions().size > 1)
			{
				singleLetter.addAction(actions);
			}
		}
	}

	public void setGfxActionBefore(Action action)
	{
		updateLetterGfxBefore = action;
	}

	public void setGfxActionAfter(Action action)
	{
		updateLetterGfxAfter = action;
	}

	public void clearGfxActions()
	{
		updateLetterGfxBefore = null;
		updateLetterGfxAfter = null;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].act(delta);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].draw(batch, parentAlpha);
		}
	}

	public void dispose()
	{
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].dispose();
		}
	}
}
