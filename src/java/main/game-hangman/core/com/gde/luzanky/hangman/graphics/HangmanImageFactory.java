package com.gde.luzanky.hangman.graphics;

import com.badlogic.gdx.graphics.Color;
import com.gde.luzanky.hangman.graphics.display.DisplayPixelArray;


/** tovarnicka {@link HangmanImageFactory} je zodpovedna za vykresleni jednotlivych obrazku na display */
public class HangmanImageFactory
{
	// musime vedet, jaky mam 'display', abych do nej mohl kreslit,
	private DisplayPixelArray display;


	public HangmanImageFactory(DisplayPixelArray display)
	{
		// vstupni parametr 'display' priradim do vnitrni primenne tridy 'this.display'
		// (v pripade, ze nazev parametru neni unikatni - pouziva se slovicko 'this' pro urceni kontextu)
		this.display = display;
	}

	/** vykresli vykricnik do displaye */
	public void vykricnik(Color color)
	{
//		vykricnik_nadruSePsanimNejvic(color); // tim, ze manualne zmenim barvu kazdeho 'pixelu'
		vykricnik_jsemChytrej(color); // anebo pouziju k vykreslovani cykly 'for'
	}

	/**
	 * vykresli vykricnik, tak, ze upravi barvu kazdeho pixelu absolutne.
	 * VYHODA: je to napsane rychle/bez zbytecneho premysleni.
	 * NEVYHODA: kdyz se zmeni velikost display-e nekdo to bude muset prepsat!
	 */
	private void vykricnik_nadruSePsanimNejvic(Color color)
	{
		display.updateColor(13, 3, color); display.updateColor(13, 4, color);
		display.updateColor(12, 3, color); display.updateColor(12, 4, color);
		display.updateColor(11, 3, color); display.updateColor(11, 4, color);
		display.updateColor(10, 3, color); display.updateColor(10, 4, color);
		display.updateColor( 9, 3, color); display.updateColor( 9, 4, color);
		display.updateColor( 8, 3, color); display.updateColor( 8, 4, color);
		display.updateColor( 7, 3, color); display.updateColor( 7, 4, color);
		display.updateColor( 6, 3, color); display.updateColor( 6, 4, color);

		display.updateColor( 3, 3, color); display.updateColor( 3, 4, color);
		display.updateColor( 2, 3, color); display.updateColor( 2, 4, color);
	}

	/**
	 * vykresli vykricnik, tak, ze upravi barvu kazdeho pixelu relativne
	 * VYHODA: kdyz se zmeni velikost displaye, obrazek vykricniku bude vzdy "stejny"
	 * NEVYHODA: pri psani se musi vice premyslet (i nad jinym, nez 16x16 displayi) a musim zvolit 'origin' bod
	 */
	private void vykricnik_jsemChytrej(Color color)
	{
		for(int row = 0; row < display.getRows(); row++)
		{
			for(int col = 0; col < display.getCols(); col++)
			{
				if (col >= 3 && col <= 4)
				{
					if (
						   row == 2
						|| row == 3
						|| (row >= 6 && row <= 13)
					   )
					{
						display.updateColor(row, col, color);
					}
				}
			}
		}
	}

	public void kopecek(Color color)
	{
		// TODO: ukol: [OK] vykresli kopecek (dole) v 16x16 displayi

		// reseni...
		int maxCols = display.getCols() - 1;
		int row = 0;
		int col = 0;
		display.updateColor(row, col, color);
		display.updateColor(row, maxCols-col, color);

		row++;
		col++;
		display.updateColor(row, col, color);
		display.updateColor(row, maxCols-col, color);

		row++;
		col++;
		display.updateColor(row, col, color);
		display.updateColor(row, col+1, color);
		display.updateColor(row, maxCols-col, color);
		display.updateColor(row, maxCols-col-1, color);

		row++;
		col = col + 2;
		display.updateColor(row, col, color);
		display.updateColor(row, col+1, color);
		display.updateColor(row, maxCols-col, color);
		display.updateColor(row, maxCols-col-1, color);

		row++;
		for(col = 6; col <= 9; col++)
		{
			display.updateColor(row, col, color);
		}

		// TODO: ukol: vybarvi nakresleny kopecek
	}

	public void sibeniceStozarNahoru(Color color)
	{
		// TODO: ukol: [OK] vykresli stozar sibenice (nahoru) v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		for(int row = 0; row < display.getRows(); row++)
		{
			if (row >= 2 && row <= 14)
			{
				display.updateColor(row, 1, color);
			}
		}
	}

	public void sibeniceStozarVodorovne(Color color)
	{
		// TODO: ukol: [OK] vykresli stozar sibenice (doprava) v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		for(int col = 0; col < display.getCols(); col++)
		{
			if (col >= 2 && col <= 14)
			{
				display.updateColor(14, col, color);
			}
		}
	}

	public void sibeniceStozarPodpera(Color color)
	{
		// TODO: ukol: [OK] vykresli podperu sibenice (sikmo) v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		display.updateColor(9, 2, color);
		display.updateColor(10, 2, color);
		display.updateColor(10, 3, color);
		display.updateColor(11, 3, color);
		display.updateColor(11, 4, color);
		display.updateColor(12, 4, color);
		display.updateColor(12, 5, color);
		display.updateColor(13, 5, color);
		display.updateColor(13, 6, color);
	}

	public void panacekHlava(Color color)
	{
		// TODO: ukol: [OK] vykresli hlavu panacka v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		display.updateColor(13, 10, color);
		display.updateColor(13, 11, color);
		display.updateColor(13, 12, color);

		display.updateColor(12, 9, color);
		display.updateColor(12, 13, color);

		display.updateColor(11, 10, color);
		display.updateColor(11, 11, color);
		display.updateColor(11, 12, color);
	}

	public void panacekTelo(Color color)
	{
		// TODO: ukol: [OK] vykresli telo panacka v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		display.updateColor(10, 11, color);
		display.updateColor(9, 11, color);
		display.updateColor(8, 11, color);
	}

	public void panacekRuce(Color color)
	{
		// TODO: ukol: [OK] vykresli ruce panacka v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		display.updateColor(9, 9, color);
		display.updateColor(9, 10, color);

		display.updateColor(9, 12, color);
		display.updateColor(9, 13, color);
	}

	public void panacekNohy(Color color)
	{
		// TODO: ukol: [OK] vykresli nohy panacka v 16x16 displayi
		// (tip: pouzij barvu, ktera prisla parametrem 'color')

		// reseni...
		display.updateColor(7, 10, color);
		display.updateColor(7, 11, color);
		display.updateColor(7, 12, color);

		display.updateColor(6, 9, color);
		display.updateColor(6, 10, color);

		display.updateColor(6, 12, color);
		display.updateColor(6, 13, color);
	}
}
