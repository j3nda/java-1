package com.gde.luzanky.hangman.wordlists;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.Gdx;

/**
 * slovnik:
 * - z textoveho souboru (.txt) nacte po radcich slova do pole.
 * - a umozni nacitane slova filtrovat dle:
 *   - isValid(): rika, zda slovo je pro hru validni
 *   - correctWord(): vraci upravene slovo (napr: zahozeni diakritiky; korekce pomlcek apod)
 */
public class Wordlist
{
	private final String[] words;


	public Wordlist(String filename)
	{
		words = readTextFileAsArray(filename);
	}

	/**
	 * nacte .txt soubor a vrati pole typu: String[].
	 * - behem nacitani provolava metody:
	 *   1) isValid() ~ ktera vraci: true/false - pokud je false, preskakuje slovo.
	 *   2) correctWord() ~ ktera vrati opravene slovo (napr: pomlcky, carky, hacky/carky apod...)
	 * - timto vytvori Collection (~kolekci), ktera je nasledne prevedena na pole: String[]
	 */
	private String[] readTextFileAsArray(String filename)
	{
		List<String> words = new ArrayList<String>();
		try (
				BufferedReader br = new BufferedReader(
					new InputStreamReader(
						Gdx.files.internal(filename).read()
					)
				);
		    )
		{
			String line = "";
			while ((line = br.readLine()) != null)
			{
				if (isValid(line))
				{
					String correctedWord = correctWord(line);
					if (correctedWord == null || correctedWord.isEmpty())
					{
						System.err.println("Slovo(" + line + ") je validni, ale uprava slova selhala!");
						continue;
					}
					words.add(line);
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		String[] wordsArray = new String[words.size()];
		return words.toArray(wordsArray);
	}

	/** vraci upravene slovo (~zde je moznost: zahodit diakritiku; pomlcky, mezery aj) */
	private String correctWord(String word)
	{
		// TODO: ukol: [OK] 1) velke znaky preved na male A -> a ([A-Z] -> [a-z])
		// reseni...
		String newWord = word.toLowerCase();

		// TODO: ukol: [OK] 2) a toto slovo vrat jako navratovou hodnotu
		// reseni...
		return newWord;
	}

	/**
	 * vraci true, pokud je slovo validni (napr: delka musi byt vetsi nez 4 znaky a mensi nez 8 znaku).
	 * jinak vraci false.
	 */
	private boolean isValid(String word)
	{
		// zkontroluj slovo - zda:
		// TODO: ukol: [OK] 1) musi byt delsi nez 4 znaky
		// TODO: ukol: [OK] 2) neobsahuje vsechny znaky stejne
		// TODO: ukol: [OK] 3) obsahuje pouze znaky: a-z, A-Z

		// RESENI: 1) musi byt delsi nez 4 znaky
		if (word.length() > 4)
		{
			// RESENI: 2) neobsahuje vsechny znaky stejne
			int citacStejnychPismen = 1;
			for(int i = 1; i < word.length(); i++)
			{
				if (word.charAt(0) == word.charAt(i))
				{
					citacStejnychPismen++;
				}
			}
			if (citacStejnychPismen == word.length())
			{
				System.out.println("debug: isValid(" + word + "): vsechny znaky jsou stejne! PRESKAKUJI!");
				return false;
			}

			// RESENI: 3) obsahuje pouze znaky: a-z, A-Z
			if (isValidByOrdinalInterval(word.toLowerCase(), 'a', 'z')) // ukazka(3.1): ascii-interval
//			if (isValidRegEx(word.toLowerCase(), "^[a-z \\-]+"))        // ukazka(3.2): regularni-vyraz
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * vraci true, pokud slovo "word" vyhovuje regularnimu vyrazu: regex
	 * UKAZKA: ilustruje, jak pouzit regularni vyraz ke zjisteni, zda String (ne)obsahuje nektere znaky.
	 */
	private boolean isValidRegEx(String word, String regex)
	{
		Pattern regExPattern = Pattern.compile(regex);
		Matcher regExMatcher = regExPattern.matcher(word);
		if (regExMatcher.find())
		{
			return true;
		}
		else
		{
			System.out.println("debug: isValidRegEx(" + regex + " | " + word + "): NEVYHOVUJE!");
			return false;
		}
	}

	/**
	 * vraci true, pokud slovo "word" vyhovuje intervalu "asciiMin" a "asciiMax",
	 *   tj. jednotliva pismena spadaji do tohoto intervalu.
	 * UKAZKA: ilustruje, jak pouzit kody ASCII tabulky k porovnani pismen. @see: https://cs.wikipedia.org/wiki/ASCIIs
	 */
	private boolean isValidByOrdinalInterval(String word, int asciiMin, int asciiMax)
	{
		try
		{
			byte[] wordInBytes = word.getBytes("US-ASCII");
			for(int i = 0; i < wordInBytes.length; i++)
			{
				if (wordInBytes[i] < asciiMin || wordInBytes[i] > asciiMax)
				{
					return false;
				}
			}
			return true;
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/** vrati pocet slov v seznamu (~nebo-li nactenych slov) */
	public int getSize()
	{
		return words.length;
	}

	/** vrati nahodne slovo ze seznamu */
	public String getRandomWord()
	{
		int index = (int)(Math.random() * (getSize() - 1));
		return get(index);
	}

	/** vrati slovo na pozici "index" */
	public String get(int index)
	{
		return words[index];
	}
}
