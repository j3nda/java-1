package com.gde.luzanky.hangman.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;
import com.gde.common.inputs.IKeyboardInputListeners;
import com.gde.common.inputs.KeyboardInputProcessorXXX;
import com.gde.luzanky.hangman.graphics.GameOverImageFactory;
import com.gde.luzanky.hangman.graphics.GfxHelper;
import com.gde.luzanky.hangman.graphics.GfxHelper.GfxDirection;
import com.gde.luzanky.hangman.graphics.HangmanImageFactory;
import com.gde.luzanky.hangman.graphics.display.DisplayLetter;
import com.gde.luzanky.hangman.graphics.display.DisplayPixelArray;
import com.gde.luzanky.hangman.graphics.display.DisplayRowLetters;
import com.gde.luzanky.hangman.wordlists.Wordlist;

/**
 * trida {@link HangmanGame} ovlada celou hru, tj. logika i grafika.
 * - zacina metodou {@link HangmanGame#create()}, kde vytvari vse potrebne pro sve fungovani.
 * - zobrazuje na obrazovku metodou {@link HangmanGame#render()}
 * - a konci metodou {@link HangmanGame#dispose()}, ktera uvolnuje vsechny zdroje z pameti
 */
public class HangmanGame
extends ApplicationAdapter // dedi z: LibGdx.ApplicationAdapter
implements IKeyboardInputListeners // implementuje rozhrani: IKeyboardInputListeners, ktere rika jak ohandlovat vstupy.
{
	/** true pro zobrazeni ladicich informaci. jinak false */
	private static boolean debug = false;
	/** OpenGL vykreslovani */
	private SpriteBatch batch;
	/** kamera, ktera snima scenu ~ tj. obrazovka */
	private OrthographicCamera camera;
	/** scena pro herce ve hre ~ game */
	private Stage stageGame;
	/** scena pro herce pri ukonceni hry ~ game-over */
	private Stage stageGameOver;
	/** font, kterym vykresluju pismena */
	private BitFontMaker2Configuration font;

	/** vyctovy typ pro ovladani konce hry */
	private enum GameOver {
		/** porazeny */
		LOSER,
		/** vitez! */
		WINNER,
	}
	/** vyctovy typ pro jednotlive faze hry */
	private enum GameState {
		/** stav, kdy hraju hru, tj. hadam slovo */
		GAME,
		/** stav, kdy je konec hry, tj. zobrazuji {@link GameOver} */
		GAMEOVER
	}

	/** display pro zobrazeni obesence (vlevo) */
	private DisplayPixelArray obesenecDisplay;
	/** tovarnicka na kresleni obrazku do displaye */
	private HangmanImageFactory obesenecObrazky;

	/** ovladaci prvek: zobraz pismeno */
	private DisplayLetter ovladaniZobrazPismeno;
	/** ovladaci prvek: sipka vlevo - pro posun 'zobrazeneho pismene' */
	private DisplayLetter ovladaniVlevo;
	/** ovladaci prvek: sipka vpravo - pro posun 'zobrazeneho pismene' */
	private DisplayLetter ovladaniVpravo;
	/** ovladaci prvek: tlacitko potvrzeni vyberu 'zobrazeneho pismene' */
	private DisplayLetter ovladaniOK;

	/** seznam slov (~slovnik), ze ktereho vybiram slova */
	private Wordlist slovnik;

	/** display, kde se zobrazuje hadane slovo */
	private DisplayRowLetters hadaneSlovoDisplay;
	/**
	 * pocitacem vybrane "hadaneSlovo", ktere se snazi hrac uhodnout.
	 * (abych vedel, ktere pismeno jsem ve slove uhadnul, pouziju jednoduchou fintu:
	 *  [a-z] mala pismena == budou ve slove znamenat, ze jsem je NEUHODL.
	 *  a
	 *  [A-Z] velka pismena == budou ve slove znamenat, ze jsem je UHODL.
	 * )
	 */
	private String hadaneSlovo;


	/** aktualni pocet chyb */
	private int pocetOmylu;
	/** hra konci, kdyz je dosazen maximalni pocet chyb! */
	private int pocetOmyluMax;
	/**
	 * aktualne zvolene pismeno (ovladani: bud pomoci klavesnice anebo sipkama)
	 * poznamka: ulozena hodnota predstavuje ascii-code a JEDNA SE VZDY O VELKE PISMENO [A-Z].
	 */
	private int zvolenePismeno;
	/** stav hry - abych vedel, v jake fazi se hra nachazi */
	private volatile GameState stavHry;
	/** cudlik pro spusteni nove hry */
	private DisplayRowLetters novaHraDisplay;
	/** display pro zobrazeni konce hry (vpravo) */
	private DisplayPixelArray gameOverDisplay;
	/** tovarnicka na kresleni obrazku do displaye */
	private GameOverImageFactory gameOverObrazky;


	/** metoda se vola, kdyz se spusti hra/aplikace a vytvori okno */
	@Override
	public void create()
	{
		// vytvori:
		// - camera = kameru, ktera snima scenu. velikost: sirka x vyska podle okna aplikace
		// - batch  = davku, ktera pomoci OpenGL vykresluje obrazky (~sprite) na obrazovku
		// - stage* = scenu, do ktere se vkladaji tzv. "herci", kteri reaguji na logiku, grafiku a input-processor
		//            (pro stav: hra-bezi(stageGame), hra-skoncila(stageGameOver))
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch = new SpriteBatch();
		stageGame = new Stage(new ScreenViewport(), batch);
		stageGameOver = new Stage(new ScreenViewport(), batch);

		// vytvori font pro zobrazeni pismen. jako font se pouzije nize zmineny.
		// jedna se o: .ttf prevedene do .json formatu pomoci:
		// -- https://www.pentacom.jp/pentacom/bitfontmaker2/
		font = new BitFontMaker2Configuration(debug, "font-16x16-Born2bSportyV2.json");

		// spusti novou hru
		startNewGame();
	}

	/** spusti novou hru */
	private void startNewGame()
	{
		GfxHelper.startNewGame(stageGame, stageGameOver);

		setGameState(GameState.GAME); // pamatuju si stav hry, tj. zda hra skoncila anebo hadam slovo

		createDisplay();						// vytvori: display (~leva strana "obesenec"), kde zobrazujem obesence
		createInputProcessor(stavHry);			// vytvori: ovladani klavesnice, mysi a tapani na display
		createWordlist("slovnik-cz-mesta.txt");	// vytvori: seznam slov, ze kterych vybiras nahodne slovo

		// vytvori: display: ovladaci tlacitka vcetne pismene, posouvani a potvrzeni (~vpravo dole)
		createControls();

		// vytvori: display, ktery zobrazuje hadane slovo
		hadaneSlovo = getRandomWord(slovnik);
		createWordDisplay(hadaneSlovo);

		pocetOmylu = 0;			// na zacatku hry: logika: je pocet chyb vzdy 0 (nula);
		pocetOmyluMax = 8;		// na zacatku hry: logika: maximum chyb (tip: idealne odvodit od delky slova a poctu neopakujicich pismen)
		zvolenePismeno = 'A';	// na zacatku hry: logika: pred-vyberu pismeno 'A'
		updateLetter(0);		// na zacatku hry: grafika: zaktualizuj vybrane pismeno
		updateHangman();		// na zacatku hry: grafika: zobraz "obesence" podle "pocetOmylu"
	}

	/** vytvari display s obesencem, do ktereho muzes kreslit */
	private void createDisplay()
	{
		obesenecDisplay = new DisplayPixelArray(16, 16);
		obesenecDisplay.setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight()
			)
		);
		stageGame.addActor(obesenecDisplay);

		// vymaze display, tj. levou cast okna, kde je obesenec
		obesenecDisplay.clearDisplay(Color.BLACK);

		// vytvori instanci "tovarnicky", ktera umi kreslit obrazky do displaye (~obesence)
		obesenecObrazky = new HangmanImageFactory(obesenecDisplay);
	}

	/**
	 * vytvori ovladaci tlacitka:
	 * - vlevo
	 * - aktualni zvolene pismeno
	 * - vpravo
	 * - potvrzeni vyberu
	 */
	private void createControls()
	{
		Vector2 velikostDlazdice = new Vector2(
			(Gdx.graphics.getWidth() / 2f) / 3f,
			Gdx.graphics.getHeight() / 4f
		);

		ovladaniZobrazPismeno = new DisplayLetter(font);
		ovladaniZobrazPismeno.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth()  / 2f + velikostDlazdice.x,
				Gdx.graphics.getHeight() / 4f,
				velikostDlazdice.x,
				velikostDlazdice.y
			)
		);
		ovladaniZobrazPismeno.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				updateLetter(+1);
			}
		});
		ovladaniVlevo = new DisplayLetter(font);
		ovladaniVlevo.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth()  / 2f,
				Gdx.graphics.getHeight() / 4f,
				velikostDlazdice.x,
				velikostDlazdice.y
			)
		);
		ovladaniVlevo.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				updateLetter(-1);
			}
		});
		ovladaniVpravo = new DisplayLetter(font);
		ovladaniVpravo.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() - velikostDlazdice.x,
				Gdx.graphics.getHeight() / 4f,
				velikostDlazdice.x,
				velikostDlazdice.y
			)
		);
		ovladaniVpravo.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				updateLetter(+1);
			}
		});
		ovladaniOK = new DisplayLetter(font);
		ovladaniOK.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth()  / 2f + velikostDlazdice.x,
				0,
				velikostDlazdice.x,
				velikostDlazdice.y
			)
		);
		ovladaniOK.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				confirmLetter();
			}
		});

		stageGame.addActor(ovladaniZobrazPismeno);
		stageGame.addActor(ovladaniVlevo);
		stageGame.addActor(ovladaniVpravo);
		stageGame.addActor(ovladaniOK);

		ovladaniZobrazPismeno.clearDisplay(Color.CYAN);
		ovladaniVlevo.update(Color.CYAN, '<');
		ovladaniVpravo.update(Color.CYAN, '>');
		ovladaniOK.update(Color.CYAN, '=');
	}

	/** vytvori display, ktery zobrazuje hadane slovo */
	private void createWordDisplay(String word)
	{
		float lineHeight = Gdx.graphics.getHeight() / 8f;

		hadaneSlovoDisplay = new DisplayRowLetters(font, word.length());
		hadaneSlovoDisplay.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight() - (2 * lineHeight),
				Gdx.graphics.getWidth() / 2f,
				lineHeight
			)
		);

		stageGame.addActor(hadaneSlovoDisplay);
		for(int i = 0; i < hadaneSlovo.length(); i++)
		{
			if (i == 0 || i == hadaneSlovo.length() - 1)
			{
				hadaneSlovoDisplay.updateLetter(i, hadaneSlovo.charAt(i));
				continue;
			}
			hadaneSlovoDisplay.updateLetter(i, '-');
		}
	}

	/** vytvori seznam slov ze slovniku */
	private void createWordlist(String filename)
	{
		slovnik = new Wordlist(filename);
		if (debug)
		{
			System.out.println("debug: -------------------------------------------------------------------");
			System.out.println("debug: slovnik: ma " + slovnik.getSize() + " slov.");
			for(int i = 0; i < slovnik.getSize(); i++)
			{
				String slovo = slovnik.get(i);
				System.out.println("debug: slovnik(" + slovo + ")");
			}
			System.out.println("debug: -------------------------------------------------------------------");
		}
	}

	/**
	 * vytvori a nastavi ovladani vstupu
	 * (psani na klavesnici; klikani a tapani)
	 */
	private void createInputProcessor(GameState state)
	{
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		switch(state)
		{
			case GAME:
			{
				KeyboardInputProcessorXXX keyboard = new KeyboardInputProcessorXXX();
				keyboard.addKeyUpListener(this);

				inputMultiplexer.addProcessor(stageGame);
				inputMultiplexer.addProcessor(keyboard);
				break;
			}
			case GAMEOVER:
			{
				inputMultiplexer.addProcessor(stageGameOver);
				break;
			}
			default:
				throw new RuntimeException("Invalid state!");
		}
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	/** vytvori cudlik: "hrat znovuv?" pri zobrazeni konec-hry (abychom mohli hrat znovu) */
	private void createGameOverControls()
	{
		final String text = "znovu?";
		novaHraDisplay = new DisplayRowLetters(font, text.length());
		novaHraDisplay.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() / 2f,
				0,
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight() / 8f
			)
		);
		char[] textInBytes = text.toCharArray();
		for(int i = 0; i < textInBytes.length; i++)
		{
			novaHraDisplay.updateLetter(i, textInBytes[i]);
		}
		novaHraDisplay.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				if (debug)
				{
					System.out.println("debug: tap: \"" + text + "\"");
				}
				GfxHelper.confirmNewGame(novaHraDisplay, false);
				GfxHelper.runCodeAfterActions(
					new Runnable()
					{
						@Override
						public void run()
						{
							if (debug)
							{
								System.out.println("debug: createGameOverControls: spoustim novou hru...");
							}
							GfxHelper.confirmNewGame(novaHraDisplay, true);
							stageGame.clear(); // vycistim scenu pro hru (abych v ni nemel zadne "herce")
							startNewGame();
						}
					},
					novaHraDisplay
				);
			}
		});

		GfxHelper.hangmanGameOver(obesenecDisplay, Color.YELLOW);
		GfxHelper.fadeInNewGame(novaHraDisplay);

		stageGameOver.addActor(novaHraDisplay);
	}

	/** vytvori display pro zobrazeni konce hry */
	private void createGameOverDisplay()
	{
		float lineHeight = Gdx.graphics.getHeight() / 8f;
		gameOverDisplay = new DisplayPixelArray(16, 16);
		gameOverDisplay.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() / 2f,
				1 * lineHeight,
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight() - (3 * lineHeight)
			)
		);

		// vytvori instanci "tovarnicky", ktera umi kreslit obrazky do displaye (~konec hry)
		gameOverObrazky = new GameOverImageFactory(gameOverDisplay);
	}


	/** metoda se vola pri kazdem vykreslovacim cyklu (~klidne 30x za sekundu!) */
	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0.502f, 1);       // nastaveni barvy v "RGBA" pro
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);   // vyplneni celeho okna nastavenou barvou

		camera.update();                            // aktualizace kamery, ktera snima scenu
        batch.setProjectionMatrix(camera.combined); // nastaveni metriky/pozice kamery pro vykresleni sceny ve spravnych souradnicich do okna

        switch(getGameState())
        {
	        case GAME:
	        {
	    		stageGame.act();  // aktualizace logiky ve scene (hra bezi)
	    		stageGame.draw(); // aktualizace grafiky ve scene (hra bezi)
	    		break;
	        }
	        case GAMEOVER:
	        {
	    		stageGameOver.act();  // aktualizace logiky ve scene (hra skoncila)
	    		stageGameOver.draw(); // aktualizace grafiky ve scene (hra skoncila)
	        	break;
	        }
	        default:
	        	break;
        }
	}

	/**
	 * metoda se vola z tzv. InputProcessoru, tj. kdyz nekdo stiskne libovolnou klavesu.
	 * (to, ze se metoda zavola je reseno listenerem, ktery implementuje {@link IKeyboardInputListeners})
	 */
	@Override
	public boolean handleKeyUp(int keyCode)
	{
		if (keyCode >= Keys.A && keyCode <= Keys.Z)
		{
			handleKeyAZ(
				// keyCode obsahuje ciselnik z LibGdx knihovny: @see: Input.Keys
				// 1) reagujeme na klavesy: A az Z, tj: keyCode == 0 je roven klavese 'a'
				// 2) abych klavesu 'a' prevedl do ascii, musim pricist jeji ascii-code,
				//    ktery je zde vlozen jako datovy typ (char) s tim, ze vyuziju "auto-boxing"!
				keyCode - Keys.A + 'A',
				true
			);
			return true;
		}
		else
		if (keyCode == Keys.LEFT)
		{
			updateLetter(-1);
			return true;
		}
		else
		if (keyCode == Keys.RIGHT || keyCode == Keys.SPACE)
		{
			updateLetter(+1);
			return true;
		}
		else
		if (keyCode == Keys.ENTER)
		{
			confirmLetter();
			return true;
		}
		else
		if (
			   debug
		    && (keyCode == Keys.PLUS || keyCode == Keys.NUM_1)
		   )
		{
			pocetOmylu++;
			updateHangman();
			handleGameOver();
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * metoda se zavola vzdy, kdyz na klavesnici napisu pismeno: "A" az "Z".
	 * (zmacknute pismeno se predava ascii-codem)
	 */
	private void handleKeyAZ(int asciiCode, boolean addGfx)
	{
		if (debug)
		{
			System.out.println("debug: handleInputKeyAZ(" + asciiCode + "): " + (char)asciiCode);
		}
		ovladaniZobrazPismeno.update(
			Color.RED,

			// vysvetleni:
			// - z asciiCode se vytvori pole typu: "byte" o 1x prvku
			// - z byte[] se vytvori retezec
			// - ktery se prevede na velka pismena
			new String(new byte[] {(byte)asciiCode}).toUpperCase(),

			Color.PINK
		);
		if (addGfx)
		{
			GfxHelper.keyAZ(ovladaniZobrazPismeno);
		}

		// TODO: ukol: [OK] aktualizuj pismeno
		// - pismeno je zadano pomoci asciiCode (~POZOR: mysli na logiku i grafiku)
		// - poznamka: aktualizace grafiky se dela (viz vyse)

		// reseni...
		zvolenePismeno = asciiCode;
	}

	/** metoda je volana pokazde, kdyz se meni pismeno pomoci sipek */
	private void updateLetter(int updateStep)
	{
		// TODO: ukol: [OK] aktualizuj pismeno
		// 1) pokud updateStep je > 0 (~napr: +1) posun pismeno v abecede o +1 doprava
		// 2) pokud updateStep je < 0 (~napr: -1) posun pismeno v abecede o -1 vlevo
		// POZNAMKA:
		// - pri aktualizaci musis myslet na logiku i grafiku
		//   (tj. kdyz upravis logiku, grafika to musi reflektovat)

		// reseni...
		zvolenePismeno = zvolenePismeno + updateStep;
		if (zvolenePismeno > 'Z')
		{
			zvolenePismeno = 'A';
		}
		if (zvolenePismeno < 'A')
		{
			zvolenePismeno = 'Z';
		}
		handleKeyAZ(zvolenePismeno, false);
		GfxHelper.scroll(
			(updateStep == 1
				? ovladaniVpravo
				: (updateStep == -1 ? ovladaniVlevo : null)
			),
			ovladaniZobrazPismeno
		);
	}

	/** metoda je volana pokazde, kdyz se potvrdi/vybere pismeno */
	private void confirmLetter()
	{
		GfxHelper.confirmSelection(ovladaniOK, ovladaniZobrazPismeno);

		// TODO: ukol: [OK] 1) navrhni a implementuj potvrzeni zvoleneho pismena
		// 1) pokud je zvolene pismeno SPRAVNE -> zobraz vsechny vyskyty pismena v hadanem slove
		// 2) pokud je zvolene pismeno SPATNE  -> pricti chybny bod (~pozor: musis aktualizovat: logiku i grafiku)

		// reseni...
		boolean error = true;
		String slovo = hadaneSlovo.toUpperCase();
		for(int i = 0; i < slovo.length(); i++)
		{
			if (slovo.charAt(i) == (char)zvolenePismeno)
			{
				// v hadanem slove menim [a-z] -> [A-Z], tj. uhadnute pismeno je vzdy VELKE!
				char[] aktualizujuPismeno = hadaneSlovo.toCharArray();
				aktualizujuPismeno[i] =	new String(	// vytvoreni noveho retezce. konstruktorem predavam pole znaku
						new byte[] {				// vytvoreni pole typu byte
							(byte)zvolenePismeno	// pretypovani: int -> byte
						}
					)
					.toUpperCase()					// preved vsechna pismena na VELKA
					.toCharArray()[0]				// cely retezec preved na pole znaku a [0] znamena, ze vrat 1. prvek z pole!
				;
				hadaneSlovo = String.valueOf(aktualizujuPismeno);
				error = false;

				// grafika
				hadaneSlovoDisplay.clearGfxActions();
				if (i > 0 && i < slovo.length() - 1)
				{
					GfxHelper.confirmLetter(hadaneSlovoDisplay);
				}
				hadaneSlovoDisplay.updateLetter(i, zvolenePismeno);
			}
		}

		// TODO: ukol: [OK] 2) v pripade chyb, zobraz stav 'obeseneho panacka'
		// reseni...
		if (error)
		{
			pocetOmylu++;
			updateHangman();
		}

		handleGameOver(); // zkontroluju zda nastal konec hry
	}

	/**
	 * metoda ovlada logiku hry, tj. rozhoduje zda nastal konec hry
	 * a je volana po {@link HangmanGame#confirmLetter}
	 */
	private void handleGameOver()
	{
		int pocetUhadnutychPismen = 0;
		for(int i = 0; i < hadaneSlovo.length(); i++)
		{
			// vyuziju fintu @see: hadaneSlovo, ze mala pismena jsem neuhadl a velka uhadl.
			if (hadaneSlovo.charAt(i) >= 'A' && hadaneSlovo.charAt(i) <= 'Z')
			{
				pocetUhadnutychPismen++;
			}
		}

		if (debug)
		{
			System.out.println(
				"debug: handleGameOver: pocetUhadnutychPismen: " + pocetUhadnutychPismen
				+ ", delkaSlova: " + hadaneSlovo.length()
			);
		}

		// TODO: ukol: [OK] zjisti, zda je konec hry ~ pokud ano, zapamatuj si 'stav' zda jsi: vyhral x prohral
		boolean isGameOver = false;
		final GameOver gameOverType;

		// reseni...
		if (pocetUhadnutychPismen == hadaneSlovo.length())
		{
			isGameOver = true;
			gameOverType = GameOver.WINNER;
		}
		else
		if (pocetOmylu >= pocetOmyluMax)
		{
			isGameOver = true;
			gameOverType = GameOver.LOSER;
		}
		else
		{
			gameOverType = null;
		}

		// kdyz je konec hry, tak...
		// - pockam, az se provedou vsechny animace
		// - potom spustim zobrazeni "game over" vc. cudliku pro start nove hry
		if (isGameOver)
		{
			createInputProcessor(GameState.GAMEOVER); // vytvorim a nastavim jiny input-processor (at se mi to neplete)

			// seznam "hercu", nad kterymi pridam graficke efekty
			// a pohlidam si, ze vsechny efekty dobehly, abych spustil kod (viz vyse)
			Actor[] actors = {
				ovladaniVlevo, ovladaniZobrazPismeno, ovladaniVpravo, ovladaniOK
			};
			GfxHelper.fadeInGameOver(actors);
			GfxHelper.runCodeAfterActions(
				new Runnable()
				{
					@Override
					public void run() // kod, ktery se vykona, az dobehnout vsechny animace...
					{
						if (debug)
						{
							System.out.println("debug: handleGameOver: zobrazuji GameOver...");
						}
						createGameOverDisplay();  // vytvorim display pro zobrazeni konce hry
						createGameOverControls(); // vytvorim potrebne ovladani
						updateGameOver(gameOverType); // umoznim upravy nad scenou (~abych to mel v 1. metode) a bylo to vice citelne
						setGameState(GameState.GAMEOVER); // zmenim stav hry
					}
				},
				actors
			);
		}
	}

	/** metoda je volana pokazde, kdyz je potreba upravit vykresleni sibenice */
	private void updateHangman()
	{
		if (debug)
		{
			System.out.println("debug: updateHangman: pocetOmlylu: " + pocetOmylu);
		}
		GfxHelper.hangmanReveal(obesenecDisplay, GfxDirection.Reset);

		// TODO: ukol: [OK] aktualizuj obrazek obesence na zaklade promenne "pocetOmylu"
		// poznamka: 1) pocetOmylu == 0; // znamena, ze neni zadna chyba a obrazek by mel byt skryty
		// poznamka: 2) pocetOmylu == pocetOmyluMax; // znamena, ze je dosazeno maximum a hra konci!
		// poznamka:
		// - v nasem pripade ma pocetOmylu 8 stavu:
		//   (nic/zadna-chyba;
		//    chyba/kresleni-sibenice(panacek(hlava, ruce, telo, nohy), kopecek, stozar(nahoru, podpera, vodorovne));
		//    konec-hry)

		// reseni...
		GfxDirection dir = GfxDirection.LeftToRight;
		switch(pocetOmylu)
		{
			case 0: obesenecDisplay.clear(); break;

			case 1: obesenecObrazky.kopecek(Color.GREEN); dir = GfxDirection.DownToUp; break;

			case 2: obesenecObrazky.sibeniceStozarNahoru(Color.BROWN); dir = GfxDirection.DownToUp; break;
			case 3: obesenecObrazky.sibeniceStozarPodpera(Color.BROWN); dir = GfxDirection.DownToUpLeftToRight; break;
			case 4: obesenecObrazky.sibeniceStozarVodorovne(Color.BROWN); dir = GfxDirection.LeftToRight; break;

			case 5: obesenecObrazky.panacekHlava(Color.YELLOW); dir = GfxDirection.UpToDown; break;
			case 6: obesenecObrazky.panacekTelo(Color.YELLOW); dir = GfxDirection.UpToDown; break;
			case 7: obesenecObrazky.panacekRuce(Color.YELLOW); dir = GfxDirection.UpToDown; break;
			case 8: obesenecObrazky.panacekNohy(Color.YELLOW); dir = GfxDirection.UpToDown; break;

			default:
				break;
		}

		// graficky efekt, pokud je display zmenen
		GfxHelper.hangmanReveal(obesenecDisplay, dir);
	}

	/** metoda je volana, kdyz nastane konec hry */
	private void updateGameOver(GameOver gameover)
	{
		if (debug)
		{
			System.out.println("debug: updateGameOver: " + gameover);
		}

		// podle stavu: vykresli obrazek do displaye
		gameOverDisplay.clearDisplay(new Color(0, 0, 0.502f, 1));
		switch(gameover)
		{
			case WINNER:
			{
				gameOverObrazky.palecNahoru(Color.BLACK, Color.WHITE, new Color(0x6699ff));
				break;
			}
			case LOSER:
			{
				gameOverObrazky.palecDolu(Color.BLACK, Color.WHITE, new Color(0x6699ff));
				break;
			}
		}

		stageGameOver.clear(); // vycistim scenu, abych tam nemel "smeti"
		// vlozim do sceny:
		stageGameOver.addActor(obesenecDisplay);	// "obesence" (~vlevo)
		stageGameOver.addActor(hadaneSlovoDisplay);	// hadane slovo (~abych se mohl poucit)
		stageGameOver.addActor(gameOverDisplay);	// game-over obrazek, tj. zda jsem: vitez x porazeny
		stageGameOver.addActor(novaHraDisplay);		// cudlik "nova hra" (abychom mohli hrat znovu)

		GfxHelper.fadeInNewGame(gameOverDisplay);
	}

	/** funkce vraci nahodne slovo ze seznamu slov */
	private String getRandomWord(Wordlist wordList)
	{
		// TODO: ukol: [OK] 1) vyber nahodne slovo ze slovniku "wordList"
		// TODO: ukol: [OK] 2) preved vsechna pismena na mala [a-z]
		// TODO: ukol: [OK] 3) prvni a posledni pismeno preved na VELKE [A-Z]
		// TODO: ukol: [OK] 4) takto upravene slovo vrat pomoci "return"
		// (poznamka: zde pouzijeme fintu @see: hadaneSlovo,
		//  ze vsechna mala pismena jsou neuhodnuta a vsechna velka pismena jsme uhodli)

		// reseni
		String nahodneSlovo = wordList.getRandomWord().toLowerCase();
		String upraveneSlovo = ""
			+ nahodneSlovo.substring(0, 1).toUpperCase()
			+ nahodneSlovo.substring(1, nahodneSlovo.length() - 1)
			+ nahodneSlovo.substring(nahodneSlovo.length() - 1).toUpperCase()
		;

		// pokud mam debug, chci znat: podrobnosti...
		if (debug)
		{
			System.out.println("debug: getRandomWord: " + nahodneSlovo + " >> " + upraveneSlovo);
		}
		return upraveneSlovo;
	}

	/** metoda se vola pri ukonceni aplikace a mela by uvolnovat zdroje z pameti */
	@Override
	public void dispose()
	{
		batch.dispose();
		obesenecDisplay.dispose();
		stageGame.dispose();
	}

	/** metoda se vola pri zmene velikosti okna aplikace */
	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
		stageGame.getViewport().update(width, height, true);
	}

	/**
	 * nastavi stav hry podle vyctoveho typu.
	 * (tato cast kodu se spousti v asynchronnim volani, tj. je potreba ji obalit synchronizaci,
	 *  aby ruzna vlakna nemela ruzne stavy)
	 */
	private synchronized void setGameState(GameState newState)
	{
		stavHry = newState;
	}

	/** vrati stav hry */
	private synchronized GameState getGameState()
	{
		return stavHry;
	}
}
