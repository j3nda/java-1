package com.gde.luzanky.hangman.graphics.display;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.badlogic.scenes.scene2d.actions.ActionsUtils;
import com.gde.common.graphics.display.DebugDraw;
import com.gde.luzanky.hangman.graphics.display.ColorPixel.ColorPixelType;


/**
 * Zobrazovaci display pro presny pocet: radek x sloupec.
 * Kdy velikost jednoho "pixelu" se dopocte na zaklade realne velikosti displaye.
 * Priklad:
 * - okno je velke(1024x768) pixelu a display bude v leve polovine, tj.
 *   velikost-displaye(512x768) pixelu,
 *   pri pozadovanem-poctu(radek=8, sloupec=16),
 *
 *   potom: 1x pixel bude: sirka=64pixel, vyska=48pixel
 */
public class DisplayPixelArray
extends Actor
{
	/** pro rychle debugovani grafiky... */
	private static boolean debug = false;
	/** dvoj-rozmernne pole "barevnych pixelu" [radky][sloupce], @see: {@link ColorPixel} */
	private final ColorPixel[][] colorPixels;
	/** pozadi displaye (~whodne, kdyz se animuji pixely - tak nechci videt skrz, tj. vykreslim background) */
	private final ColorPixel background;
	/** pocet (sloupcu, radku), ulozeno jako Vector2.(x, y) */
	private final Vector2 size;
	/** velikost 1x bodu/pixelu v ramci displaye */
	private Vector2 pixelSize;
	/** typ pixelu, tj. z ceho se generuje textura pro vybarveni 1x pixelu */
	private final ColorPixelType pixelType = ColorPixelType.fromFile;
	/** velikost displaye vcetne jeho pozice */
	private final Rectangle viewport;
	/** dynamicky generovana textura pro 1x barevny bod. performance killer */
	private Pixmap pixmap;
	/** true/false hodnota nad pixely, ktere zmenily barvu a nebylo to pozadi */
	private boolean[][] updatedPixels;
	public enum DisplayRenderMode { Normal, NoBackground }
	/** mod vykreslovani */
	private DisplayRenderMode renderMode;


	public DisplayPixelArray(int maxRows, int maxCols)
	{
		this.colorPixels = new ColorPixel[maxRows][maxCols];
		this.size = new Vector2(maxCols, maxRows);
		this.pixelSize = new Vector2(1, 1);
		this.viewport = new Rectangle(0, 0, 0, 0);
		this.updatedPixels = new boolean[maxRows][maxCols];
		setViewport(new Rectangle(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		this.background = new ColorPixel((int)viewport.width, (int)viewport.height, Color.BLACK);
	}

	@Override
	public void addAction(Action action)
	{
		addAction(action, true);
	}

	public void addAction(Action action, boolean delegateInto)
	{
		super.addAction(action);
		if (delegateInto)
		{
			for(int row = 0; row < getRows(); row++)
			{
				for(int col = 0; col < getCols(); col++)
				{
					ColorPixel pixel = colorPixels[row][col];
					if (pixel != null)
					{
						pixel.addAction(
							ActionsUtils.newInstance(action)
						);
					}
				}
			}
		}
	}

	public void addActionPixelOnly(Action action, int row, int col)
	{
		if (colorPixels[row][col] != null)
		{
			colorPixels[row][col].addAction(action);
		}
	}

	/** prida akci, ktera se vykona pouze na pixel, ktery je jako popredi */
	public void addActionFgOnly(Action action)
	{
		Color bgColor = background.getColor();
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (
					   pixel != null
					&& (
						   pixel.getColor().r != bgColor.r
						|| pixel.getColor().g != bgColor.g
						|| pixel.getColor().b != bgColor.b
					   )
				   )
				{
					pixel.addAction(
						ActionsUtils.newInstance(action)
					);
				}
			}
		}
	}

	/** prida akci, ktera se vykona pouze na pixel, ktery odpovida dane barve */
	public void addActionColorOnly(Action action, Color color)
	{
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (
					   pixel != null
					&& pixel.getColor().r == color.r
					&& pixel.getColor().g == color.g
					&& pixel.getColor().b == color.b
				   )
				{
					pixel.addAction(
						ActionsUtils.newInstance(action)
					);
				}
			}
		}
	}

	@Override
	public void clearActions()
	{
		super.clearActions();
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel != null)
				{
					pixel.clearActions();
				}
			}
		}
	}

	@Override
	public boolean hasActions()
	{
		int actions = 0;
		if (super.hasActions())
		{
			actions += super.getActions().size;
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel != null && pixel.hasActions())
				{
					actions++;
				}
			}
		}
		return (actions > 0 ? true : false);
	}

	/** nastavi pozici a velikost displaye (a prepocte vsechny dulezite veci) */
	public void setViewport(Rectangle viewport)
	{
		if (background != null)
		{	// uplne hnusne osetreny stav: konstruktor vola setViewport() a az potom se vytvari background objekt!
			background.setPosition(viewport.x, viewport.y);
			background.setWidth(viewport.width);
			background.setHeight(viewport.height);
			background.setOrigin(Align.center);
		}

		pixelSize.x = viewport.width  / size.x;
		pixelSize.y = viewport.height / size.y;
		this.viewport.set(viewport);
		pixmap = new Pixmap(
			(int)pixelSize.x,
			(int)pixelSize.y,
			Pixmap.Format.RGBA8888
		);
		recreateColorPixels(pixmap, pixelSize, pixelType, colorPixels);
		setPosition(viewport.x, viewport.y);
		setWidth(viewport.width);
		setHeight(viewport.height);
		setOrigin(Align.center);

		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				pixel.setPosition(
					viewport.x + (col * pixelSize.x),
					viewport.y + (row * pixelSize.y)
				);
			}
		}
		if (debug)
		{
			System.out.println(
				getClass().getSimpleName() + ".setViewport(): "
				+ "size: " + size
				+ ", viewport: " + viewport
				+ ", pixelSize: " + pixelSize
			);
		}
	}

	/** znovu-vytvori {@link ColorPixel} jako pole (sloupcu, radku), vcetne barvy */
	private void recreateColorPixels(
		Pixmap pixmap, Vector2 pixelSize, ColorPixelType textureType, ColorPixel[][] colorPixels
	)
	{
		boolean hasActions = hasActions();
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel == null)
				{	// pokud pixel neexistuje, vytvor ho...
					colorPixels[row][col] = new ColorPixel(
						(int)pixelSize.x + 1, // +1 is correction for crazy gaps between cells!@#$%^&()
						(int)pixelSize.y + 1, // TODO: u can fix it, but i wasted a quite time here...
						Color.BLACK
					);
					if (hasActions)
					{
						for(Action action : getActions())
						{
							colorPixels[row][col].addAction(
								ActionsUtils.newInstance(action)
							);
						}
					}
				}
				else
				if (pixelSize.x != pixel.getWidth() || pixelSize.y != pixel.getHeight())
				{	// pokud se zmenila velikost 1x pixelu, uprav velikost
					colorPixels[row][col].setWidth(pixelSize.x);
					colorPixels[row][col].setHeight(pixelSize.y);
					colorPixels[row][col].setOrigin(Align.center);
				}
			}
		}
	}

	/** vrati pocet radku displaye */
	public int getRows()
	{
		return (int) size.y;
	}

	/** vrati pocet sloupcu displaye */
	public int getCols()
	{
		return (int) size.x;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				colorPixels[row][col].act(delta);
			}
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (renderMode != DisplayRenderMode.NoBackground)
		{
			background.draw(batch, parentAlpha);
		}
		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				pixel.draw(batch, parentAlpha);
				if (debug)
				{
					DebugDraw.begin(batch);
					DebugDraw.point(
						Color.PINK,
						new Vector2(
							viewport.x + (col * pixelSize.x) + (pixelSize.x / 2f) - 2,
							viewport.y + (row * pixelSize.y) + (pixelSize.y / 2f) - 2
						),
						4
					);
					DebugDraw.end(batch);
				}
			}
		}
		if (debug)
		{
			DebugDraw.begin(batch);
			DebugDraw.rectangle(Color.WHITE, viewport, 4);
			DebugDraw.end(batch);
		}
	}

	/** aktualizuje barvu v displayi na urcite pozici */
	public void updateColor(int row, int col, Color color)
	{
		if (row < size.y && col < size.x)
		{
			colorPixels[row][col].updateColor(color);
			updatedPixels[row][col] = true;
		}
	}

	/** aktualizuje alpha (~pruhlednost) v displayi na urcite pozici */
	public void updateAlpha(int row, int col, float alpha)
	{
		if (row < size.y && col < size.x)
		{
			Color color = colorPixels[row][col].getColor();
			color.a = alpha;

			colorPixels[row][col].updateColor(color);
		}
	}

	/** vymaze display danou barvou */
	public void clearDisplay(Color color)
	{
		background.updateColor(color);
		clearUpdatedPixels();
		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				updateColor(row, col, color);
			}
		}
	}

	/** uvolni zdroje z pameti */
	public void dispose()
	{
		if (pixmap != null)
		{
			pixmap.dispose();
			pixmap = null;
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				if (colorPixels[row][col] != null)
				{
					colorPixels[row][col].dispose();
				}
			}
		}
	}

	/** vrati rozdil true/false mezi 2-ma obrazky (~pouziva se v grafickych efektech, abych vedel, co mam animovat) */
	public boolean[][] getUpdatedPixels()
	{
		return updatedPixels;
	}

	public void clearUpdatedPixels()
	{
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				updatedPixels[row][col] = false;
			}
		}
	}

	public void setRenderMode(DisplayRenderMode renderMode)
	{
		this.renderMode = renderMode;
	}

	public void flipX()
	{
		flip(false, true);
	}

	public void flipY()
	{
		flip(true, false);
	}

	private void flip(boolean horizontal, boolean vertical)
	{
		if (!horizontal && !vertical)
		{
			return;
		}
		Color[][] tmp = new Color[(int) size.y][(int) size.x];
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				if (horizontal)
				{
					tmp[(getRows() - 1 - row)][col] = new Color(colorPixels[row][col].getColor());
				}
				if (vertical)
				{
					tmp[row][(getCols() - 1 - col)] = new Color(colorPixels[row][col].getColor());
				}
			}
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				colorPixels[row][col].updateColor(tmp[row][col]);
			}
		}
		tmp = null;
	}
}
