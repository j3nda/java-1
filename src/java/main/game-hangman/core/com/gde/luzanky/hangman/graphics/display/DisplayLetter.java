package com.gde.luzanky.hangman.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

/** zobrazi 1x pismeno za pomoci {@link DisplayPixelArray} */
public class DisplayLetter
extends DisplayPixelArray
{
	// pro zobrazeni potrebuju znat font
	private BitFontMaker2Configuration font;


	public DisplayLetter(BitFontMaker2Configuration font)
	{
		// zavolam predka, tj. konstruktor z: 'DisplayPixelArray'
		super(
			(int)font.draw.height,
			(int)font.draw.width
		);
		// priradim si font, ktery prisel jako parametr
		this.font = font;
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano ascii kodem) */
	public void update(Color fgColor, int asciiCode)
	{
		update(fgColor, asciiCode, Color.BLACK);
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano ascii kodem) vcetne zmeny pozadi */
	public void update(Color fgColor, int asciiCode, Color bgColor)
	{
		clearDisplay(bgColor);
		updateForeground(fgColor, font.getChar(asciiCode));
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano stringem) */
	public void update(Color fgColor, String letter)
	{
		if (letter == null || letter.isEmpty())
		{
			return;
		}
		update(fgColor, letter.charAt(0));
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano stringem) vcetne zmeny pozadi */
	public void update(Color fgColor, String letter, Color bgColor)
	{
		if (letter == null || letter.isEmpty())
		{
			return;
		}
		update(fgColor, letter.charAt(0), bgColor);
	}

	/** provede aktualizaci zobrazovaci mrizky tak, ze vstupem je barva a binary-encoded-char 16x16! */
	private void updateForeground(Color color, int[] charInBinary)
	{
		if (charInBinary == null || charInBinary.length != font.width)
		{
			return;
		}
		for(int fontRow = 0; fontRow < font.width; fontRow++)
		{
			for(int fontCol = 0; fontCol < font.width; fontCol++)
			{
				if (
					   fontRow >= font.draw.y && fontRow <= font.draw.y + font.draw.height
					&& fontCol >= font.draw.x && fontCol <= font.draw.x + font.draw.width
				   )
				{
					int binaryValue = charInBinary[fontRow];
					if (((binaryValue >> fontCol) & 1) == 1)
					{
						updateColor(
							getRows() - 1 - (fontRow - (int)font.draw.y),
							fontCol - (int)font.draw.x,
							color
						);
					}
				}
			}
		}
	}
}
