package com.gde.luzanky.hangman.graphics.display;

import com.badlogic.gdx.graphics.Color;

/**
 * hangman ma vlastni ColorPixel
 * (zpetna kompatibilita: updateColor())
 */
class ColorPixel
extends com.gde.common.graphics.display.pixels.ColorPixel
{
	/** typ, jak bude textura vytvorena */
	enum ColorPixelType { fromPixmap, fromFile }


	ColorPixel(int width, int height, Color color)
	{
		super(width, height, color);
	}

	void updateColor(Color color)
	{
		setColor(color);
	}
}
