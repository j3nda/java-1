package com.gde.luzanky.hangman.graphics;

import com.badlogic.gdx.graphics.Color;
import com.gde.luzanky.hangman.graphics.display.DisplayPixelArray;

/** tovarnicka {@link GameOverImageFactory} zodpovedna za vykresleni jednotlivych obrazku na display (~konec hry) */
public class GameOverImageFactory
{
	// musime vedet, jaky mam 'display', abych do nej mohl kreslit,
	private DisplayPixelArray display;


	public GameOverImageFactory(DisplayPixelArray display)
	{
		// vstupni parametr 'display' priradim do vnitrni primenne tridy 'this.display'
		// (v pripade, ze nazev parametru neni unikatni - pouziva se slovicko 'this' pro urceni kontextu)
		this.display = display;
	}

	/**
	 * vykresli srdicko 13x12 do 16x16 displaye
	 * zdroj: https://art.pixilart.com/602cf5da38e3923.gif
	 */
	public void srdce(Color okraj, Color vypln, Color odlesk)
	{
		// cerny okraj (zespod, odprostred)
		display.updateColor( 2,  8, okraj);

		// leva a prava stoupajici cara (nahoru)
		display.updateColor( 3,  7, okraj); display.updateColor( 3,  9, okraj);
		display.updateColor( 4,  6, okraj); display.updateColor( 4, 10, okraj);
		display.updateColor( 5,  5, okraj); display.updateColor( 5, 11, okraj);
		display.updateColor( 6,  4, okraj); display.updateColor( 6, 12, okraj);
		display.updateColor( 7,  3, okraj); display.updateColor( 7, 13, okraj);

		// leva a prava svisla cara (nahoru)
		display.updateColor( 8,  2, okraj); display.updateColor( 8, 14, okraj);
		display.updateColor( 9,  2, okraj); display.updateColor( 9, 14, okraj);
		display.updateColor(10,  2, okraj); display.updateColor(10, 14, okraj);
		display.updateColor(11,  2, okraj); display.updateColor(11, 14, okraj);
		display.updateColor(11,  8, okraj);

		// levy a pravy (horni) okraj "kopecku"
		display.updateColor(12,  3, okraj); display.updateColor(12, 13, okraj);
		display.updateColor(12,  7, okraj); display.updateColor(12,  9, okraj);

		display.updateColor(13,  4, okraj); display.updateColor(13,  5, okraj); display.updateColor(13,  6, okraj);
		display.updateColor(13, 10, okraj); display.updateColor(13, 11, okraj); display.updateColor(13, 12, okraj);

		// cervena vypln (zespod -> nahoru, odprosted)
		display.updateColor(3, 8, vypln);
		for(int i = 8 - 1; i <= 8 + 1; i++) { display.updateColor(4, i, vypln); }
		for(int i = 8 - 2; i <= 8 + 2; i++) { display.updateColor(5, i, vypln); }
		for(int i = 8 - 3; i <= 8 + 3; i++) { display.updateColor(6, i, vypln); }
		for(int i = 8 - 4; i <= 8 + 4; i++) { display.updateColor(7, i, vypln); }
		for(int i = 8 - 5; i <= 8 + 5; i++)
		{
			for(int j = 8; j <= 11; j++)
			{
				if (i == 8 && j == 11)
				{
					continue;
				}
				display.updateColor(j, i, vypln);
			}
		}
		for(int i = 8 - 4; i <= 8 + 4; i++)
		{
			if (i >= 8 - 1 && i <= 8 + 1)
			{
				continue;
			}
			display.updateColor(12, i, vypln);
		}

		// odlesk
		display.updateColor(10, 8 + 3, odlesk);
		display.updateColor( 9, 8 + 4, odlesk);
	}

	/**
	 * palec nahoru (~like) 14x16 do 16x16 displaye
	 * zdroj: https://cdn.shopify.com/s/files/1/0822/1983/articles/like-button-pixel-art-pixel-art-like-button-thumbs-up-facebook-brik-card-pixel-8bit.png?v=1518155368
	 */
	public void palecNahoru(Color okraj, Color vyplnRuka, Color vyplnRukav)
	{
		palecNahoru_okraj(okraj);
		palecNahoru_vyplnRukav(vyplnRukav);
		palecNahoru_vyplnRuka(vyplnRuka);
		display.flipX();
	}

	private void palecNahoru_okraj(Color color)
	{
		// okraj (zleva -> doprava; zdola -> nahoru)
		// o) spodni linka
		for(int i = 1; i <= 11; i++)
		{
			if (i == 4)
			{
				continue;
			}
			display.updateColor(1, i, color);
			if (i < 4)
			{
				display.updateColor(8, i, color);
			}
		}

		// o) rukav (vlevo)
		for(int i = 2; i <= 7; i++) { display.updateColor( i, 0, color); display.updateColor( i, 4, color); }

		// o) ruka (vpravo)
		display.updateColor( 2, 12, color);
		display.updateColor( 3, 12, color);
		display.updateColor( 4, 13, color);
		display.updateColor( 5, 13, color);
		display.updateColor( 6, 13, color);
		display.updateColor( 7, 14, color);
		display.updateColor( 8, 14, color);
		display.updateColor( 9, 14, color);

		// o) ruka (nahore - ukazovacek)
		display.updateColor(10, 13, color);
		display.updateColor(10, 12, color);
		display.updateColor(10, 11, color);
		display.updateColor(10, 10, color);
		display.updateColor(10,  9, color);

		// o) ruka (nahore - palec)
		display.updateColor(10,  9, color);
		display.updateColor(11,  9, color);
		display.updateColor(12,  9, color);
		display.updateColor(13,  9, color);
		display.updateColor(14,  8, color);
		display.updateColor(14,  7, color);
		display.updateColor(13,  7, color);
		display.updateColor(12,  7, color);
		display.updateColor(11,  6, color);
		display.updateColor(10,  6, color);
		display.updateColor( 9,  5, color);
		display.updateColor( 8,  5, color);
	}

	private void palecNahoru_vyplnRukav(Color color)
	{
		// vypln: rukav
		for(int i = 2; i <= 2 + 5; i++)
		{
			for (int j = 1; j <= 1 + 2; j++)
			{
				display.updateColor(i, j, color);
			}
		}
	}

	private void palecNahoru_vyplnRuka(Color color)
	{
		// vypln: ruka!
		for(int i = 5; i <= 5 + 6; i++)
		{
			display.updateColor(2, i, color);
			display.updateColor(3, i, color);
		}
		for(int i = 5; i <= 5 + 7; i++)
		{
			display.updateColor(4, i, color);
			display.updateColor(5, i, color);
			display.updateColor(6, i, color);
		}
		for(int i = 5; i <= 5 + 8; i++) { display.updateColor(7, i, color); }
		for(int i = 6; i <= 6 + 7; i++)
		{
			display.updateColor(8, i, color);
			display.updateColor(9, i, color);
		}
		for(int i = 7; i <= 7 + 1; i++)
		{
			display.updateColor(10, i, color);
			display.updateColor(11, i, color);
		}
		display.updateColor(12, 8, color);
		display.updateColor(13, 8, color);
	}

	/** vykresli palec dolu (~dislike) */
	public void palecDolu(Color okraj, Color vyplnRuka, Color vyplnRukav)
	{
		palecNahoru(okraj, vyplnRuka, vyplnRukav);
		display.flipY();
	}
}
