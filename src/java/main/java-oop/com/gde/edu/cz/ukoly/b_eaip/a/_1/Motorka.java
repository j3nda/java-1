package com.gde.edu.cz.ukoly.b_eaip.a._1;

class Motorka
implements IVozidlo
{
	private int pocetMist;

	Motorka(int pocetMist)
	{
		this.pocetMist = pocetMist;
	}

	@Override
	public int getPocetMist()
	{
		return pocetMist;
	}

	void pohybDopredu()
	{
		System.out.println("Motorka.pohybDopredu();");
	}

	void pohybDozadu()
	{
		System.out.println("Motorka.pohybDozadu();");
	}

	void zatocVlevo()
	{
		System.out.println("Motorka.zatocVlevo();");
	}

	void zatocVpravo()
	{
		System.out.println("Motorka.zatocVpravo();");
	}

	@Override
	public String toString()
	{
		return "Motorka="
			+ "pocetMist:" + pocetMist
		;
	}
}
