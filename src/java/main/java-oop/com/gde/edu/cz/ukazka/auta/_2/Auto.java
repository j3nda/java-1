package com.gde.edu.cz.ukazka.auta._2;

/**
 * "abstract" VYNUCUJE IMPLEMENTACI,
 * tj. mam zajisteno, ze vsechno, co je uvozeno klicovym slovem (abstract), bude obsahovat implementaci!
 */
abstract class Auto
extends Object
{
	protected String vyrobce;
	protected TypAuta typ;

	protected Auto(String vyrobce, TypAuta typ)
	{
		this.vyrobce = vyrobce;
		this.typ = typ;
	}

	@Override
	public String toString()
	{
		return ""
			+ "vyrobce: \"" + vyrobce + "\""
			+ ", typ: \"" + typ + "\""
		;
	}
}
