package com.gde.edu.cz.ukoly.b_eaip.e._2;

import java.lang.reflect.Modifier;

/**
 * OOP/EAIP: encapsulation (~zapouzdreni)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zapouzdrit data, tak aby:
		//   - byly skryte pro 'okoli',
		//   - a mohl jsem s nimi pracovat uvnitr tridy
		//
		// TODO: 1) v tomto balicku vytvor tridu (~class) s nazvem "OsobniAuto"
		// TODO: 2) trida "OsobniAuto" bude mit:
		// TODO: 2.1) konstruktor: ktery akceptuje parametry (Znacka, pocetDveri, vykonMotoru)
		// TODO: 2.2) gettery:
		// TODO: 2.2.1) Znacka getZnacka();
		// TODO: 2.2.2) int getpocetDveri();
		// TODO: 2.2.3) int getVykonMotoru();
		// TODO: 2.3) trida "OsobniAuto" bude mit viditelnost package-private (~default)
		// TODO: 3) trida "OsobniAuto" bude veskere sve vnitrni atributy:
		// TODO: 3.1) skryvat pred okolim
		// TODO: 3.2) atributy budou finalni
		// TODO: 4) uprav viditelnost ve tride "OsobniAuto" tak, ze:
		// TODO: 4.2) gettery budou public
		// TODO: 5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertNotNull(new OsobniAuto(null, 0, 0));
//		System.out.println("OK: ukol/1");
//
//		OsobniAuto auto1 = new OsobniAuto(Znacka.Skoda, 5, 1600);
//		OsobniAuto auto2 = new OsobniAuto(Znacka.Bmw, 3, 2100);
//		assertEquals(Znacka.Skoda, auto1.getZnacka());
//		assertEquals(Znacka.Bmw, auto2.getZnacka());
//		assertEquals(5, auto1.getPocetDveri());
//		assertEquals(3, auto2.getPocetDveri());
//		assertEquals(1600, auto1.getVykonMotoru());
//		assertEquals(2100, auto2.getVykonMotoru());
//		assertTrue(isPackagePrivate(OsobniAuto.class.getModifiers()));
//		assertEquals(3, OsobniAuto.class.getDeclaredMethods().length);
//		for(Method m : OsobniAuto.class.getDeclaredMethods())
//		{
//			assertTrue(Modifier.isPublic(m.getModifiers()));
//		}
//		System.out.println("OK: ukol/2");
//
//		for(Field f : auto1.getClass().getDeclaredFields())
//		{
//			assertTrue(Modifier.isPrivate(f.getModifiers()));
//			assertTrue(Modifier.isFinal(f.getModifiers()));
//		}
//		System.out.println("OK: ukol/3");
//
//		for(String metoda : new String[] {"Znacka", "PocetDveri", "VykonMotoru"})
//		{
//			Method m = ReflectionUtils.findMethod(auto1.getClass(), "get" + metoda).get();
//			assertNotNull(m);
//			assertTrue(Modifier.isPublic(m.getModifiers()));
//		}
//		System.out.println("OK: ukol/4");
	}

	private static boolean isPackagePrivate(int mod)
	{
		return
			   !Modifier.isPublic(mod)
			&& !Modifier.isProtected(mod)
			&& !Modifier.isPrivate(mod)
		;
	}
}
