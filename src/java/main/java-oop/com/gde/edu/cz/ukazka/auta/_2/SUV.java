package com.gde.edu.cz.ukazka.auta._2;

class SUV
extends Osobni
{
	String model;

	SUV(String vyrobce, String model, String SPZ)
	{
		super(vyrobce, SPZ);
		this.model = model;
		this.typ = TypAuta.SUV;
	}

	@Override
	public String toString()
	{
		return ""
			+ "model: \"" + model + "\""
			+ ", " + super.toString()
		;
	}
}
