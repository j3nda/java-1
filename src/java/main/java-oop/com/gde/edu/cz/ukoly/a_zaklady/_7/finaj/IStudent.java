package com.gde.edu.cz.ukoly.a_zaklady._7.finaj;

public interface IStudent
{
	String getJmeno();
	int getVek();
}
