package com.gde.edu.cz.ukoly.a_zaklady._1;

public class DesktopLauncher
{
	public static void main(String[] args)
	{
		// TODO: 1) vytvor balicek (~package) s nazvem "doom"
		// TODO: 2) v tomto balicku(doom) vytvor tridu (~class) s nazvem "DoomGuy"
		// TODO: 3) v tomto balicku(doom) vytvor rozhranni (~interface) s nazvem "IMaZivoty"
		// TODO: 3.1) rozhranni "IMaZivoty" bude obsahovat:
		//            - void setZivoty(int pocetZivotu);
		//            - int  getZivoty();
		//            - void uberZivot();
		// TODO: 3.2) zarid, aby trida "DoomGuy" implementovala rozhranni "IMaZivoty" a napis implementaci
		// TODO: 3.3) ve tride "DoomGuy" implementuj metodu "toString()", kde vratis pocet zivotu (~instance tridy)
		//
		// TODO: 4) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
//		DoomGuy player1 = new DoomGuy();
//		player1.setZivoty(3);
//		System.out.println(player1);
//
//		while(player1.getZivoty() > 0)
//		{
//			System.out.println("vykonavam: uberZivot();");
//			player1.uberZivot();
//			System.out.println(player1);
//		}
	}
}
