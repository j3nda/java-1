package com.gde.edu.cz.ukoly.a_zaklady._4;

public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor balicek (~package) s nazvem "data"
		// TODO: 1.1) v tomto balicku(data) vytvor tridu (~class) s nazvem "DukeNukem"
		// TODO: 1.2) ve tride "DukeNukem" vytvor konstruktor bez parametru
		// TODO: 1.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		DukeNukem dn1 = new DukeNukem();
//
//		assertEquals(
//			1,
//			ReflectionUtils.findConstructors(dn1.getClass(), c -> c.getParameterCount() == 0).size()
//		);
//		System.out.println("OK: ukol/1");


		// TODO: 2) ve tride "DukeNukem" vytvor nasledujici atributy, vc. viditelnosti
		// TODO: 2.1) atribut "jmeno" jako retezec, ktery bude viditelny pro vsechny
		// TODO: 2.2) atribut "vyska" jako cele cislo, ktery bude viditelny pro vlastni tridu a jeji odvozeniny
		// TODO: 2.3) atribut "vaha" jako desetinne cislo, ktery bude viditelny pro cely balicek
		// TODO: 2.4) atribut "profese" jako retezec, ktery bude viditelny pouze pro danou tridu
		// TODO: 2.5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		DukeNukem dn2 = new DukeNukem();
//
//		assertNotNull(
//			ReflectionUtils.findConstructors(dn2.getClass(), c -> c.getParameterCount() == 0).get(0)
//		);
//		Field field;
//		field = ReflectionSupport.findFields(dn1.getClass(), f -> f.getName().equals("jmeno"), HierarchyTraversalMode.TOP_DOWN).get(0);
//		assertEquals(String.class, field.getType());
//		assertTrue(Modifier.isPublic(field.getModifiers()));
//
//		field = ReflectionSupport.findFields(dn1.getClass(), f -> f.getName().equals("vyska"), HierarchyTraversalMode.TOP_DOWN).get(0);
//		assertEquals(int.class, field.getType());
//		assertTrue(Modifier.isProtected(field.getModifiers()));
//
//		field = ReflectionSupport.findFields(dn1.getClass(), f -> f.getName().equals("vaha"), HierarchyTraversalMode.TOP_DOWN).get(0);
//		assertEquals(float.class, field.getType());
//		assertTrue(
//			   Modifier.isPublic(field.getModifiers()) == false
//			&& Modifier.isProtected(field.getModifiers()) == false
//			&& Modifier.isPrivate(field.getModifiers()) == false
//			&& Modifier.isPublic(field.getModifiers()) == false
//		);
//
//		field = ReflectionSupport.findFields(dn1.getClass(), f -> f.getName().equals("profese"), HierarchyTraversalMode.TOP_DOWN).get(0);
//		assertEquals(String.class, field.getType());
//		assertTrue(Modifier.isPrivate(field.getModifiers()));
//		System.out.println("OK: ukol/2");


		// TODO: 3) ve tride "DukeNukem" vytvor parametricky konstruktor pro "vyska" a "vaha"
		// TODO: 3.1) napis implementaci, ktera tyto parametry ulozi do atributu tridy
		// TODO: 3.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		DukeNukem dn3 = new DukeNukem(123, 3.21f);
//		DukeNukem dn4 = new DukeNukem(456, 7.89f);
//
//		assertNotNull(
//			ReflectionUtils.findConstructors(
//				dn2.getClass(),
//				   c -> c.getParameterCount() == 2
//				&& c.getParameterTypes()[0].equals(int.class)
//				&& c.getParameterTypes()[1].equals(float.class)
//			).get(0)
//		);
//		assertEquals(
//			123,
//			ReflectionUtils.tryToReadFieldValue(DukeNukem.class, "vyska", dn3).get()
//		);
//		assertEquals(
//			3.21f,
//			ReflectionUtils.tryToReadFieldValue(DukeNukem.class, "vaha", dn3).get()
//		);
//		assertEquals(
//			456,
//			ReflectionUtils.tryToReadFieldValue(DukeNukem.class, "vyska", dn4).get()
//		);
//		assertEquals(
//			7.89f,
//			ReflectionUtils.tryToReadFieldValue(DukeNukem.class, "vaha", dn4).get()
//		);
//		System.out.println("OK: ukol/3");


		// TODO: 4) protoze "profese" je soukromy atribut,
		// TODO: 4.1) ve tride "DukeNukem" vytvor verejny getter "getProfese" pro atribut "profese"
		// TODO: 4.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		DukeNukem dn5 = new DukeNukem();
//
//		assertTrue(
//			Modifier.isPublic(
//				ReflectionSupport.findMethod(dn5.getClass(), "getProfese").get().getModifiers()
//			)
//		);
//		System.out.println("OK: ukol/4");
	}
}
