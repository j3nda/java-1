package com.gde.edu.cz.ukazka.tvary._2;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gde.edu.cz.ukazka.tvary._1.primitiva.Bod;

public class TvaryApp
extends ApplicationAdapter
{
	private SpriteBatch batch;
	private Bod bod;

	@Override
	public void create()
	{
		batch = new SpriteBatch(); // vytvori kontejner, ktery vykresluje pomoci OpenGL

		bod = new Bod();
		bod.setX(100);
		bod.setY(200);

		// TODO: tvary#2: ukol.1) pomoci tvary#1 graf. objektu implementuj "smajlik" vc. barvicek (~samozrejme objektove ;)
		// TODO: tvary#2: ukol.2) vytvor 2x smajliky ruznych velikosti
		// TODO: tvary#2: ukol.3.1) zarid, aby smajliky litaly po obrazovce;
		// TODO: tvary#2: ukol.3.2) jakmile narazi do okraje, tak se odrazi
		// TODO: tvary#2: ukol.3.3) jakmile narazi do okraje, tak se na 1s zamraci
		// TODO: tvary#2: ukol.3.b) zarid, aby smajliky meli ruznou rychlost
		// TODO: tvary#2: ukol.4) zarid, aby se smajliky od sebe odrazeli
		// TODO: tvary#2: ukol.5) zarid, abych mohl nastavit pocet 'litajicich smajliku' (~napr: 5, 10, 50)
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		// vykresli elementy @see: IRenderable
		bod.draw(batch);
	}

	@Override
	public void dispose()
	{
		batch.dispose();
	}
}
