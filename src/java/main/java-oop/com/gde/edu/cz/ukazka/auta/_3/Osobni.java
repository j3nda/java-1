package com.gde.edu.cz.ukazka.auta._3;

class Osobni
extends Auto
implements IObsahujeSPZ
{
	private final String SPZ;

	Osobni(String vyrobce, String SPZ)
	{
		super(vyrobce);
		this.SPZ = SPZ;
	}

	@Override
	public String toString()
	{
		return ""
			+ "SPZ: \"" + SPZ + "\""
			+ ", " + super.toString()
		;
	}

	@Override
	public TypAuta getTyp()
	{
		return TypAuta.Osobni;
	}

	@Override
	public String getSPZ()
	{
		return SPZ;
	}
}
