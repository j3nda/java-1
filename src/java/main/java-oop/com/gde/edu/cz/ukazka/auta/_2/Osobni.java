package com.gde.edu.cz.ukazka.auta._2;

class Osobni
extends Auto
{
	public final String SPZ;

	Osobni(String vyrobce, String SPZ)
	{
		super(vyrobce, TypAuta.Osobni);
		this.SPZ = SPZ;
	}

	@Override
	public String toString()
	{
		return ""
			+ "SPZ: \"" + SPZ + "\""
			+ ", " + super.toString()
		;
	}
}
