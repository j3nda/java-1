package com.gde.edu.cz.ukoly.a_zaklady._3.calc;

public interface IChytryKalkulator
extends IKalkulator
{
	int secti(int... cisla);
	int odecti(int... cisla);
	int nasob(int... cisla);
	int vydel(int... cisla);
}
