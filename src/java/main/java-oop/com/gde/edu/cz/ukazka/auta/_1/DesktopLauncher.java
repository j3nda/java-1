package com.gde.edu.cz.ukazka.auta._1;

/**
 * jednoducha ukazka pouziti objektu a jejich instanci
 * - NEOBSAHUJE:
 *   - reseni viditelnosti
 *   - promyslene dedicnosti
 *   => coz muze vyustit v problemy...
 */
public class DesktopLauncher
{
	public static void main(String[] args)
	{
		Auto mojePrvniAuto = new Auto();
		mojePrvniAuto.vyrobce = "Skoda";
		mojePrvniAuto.typ = TypAuta.Osobni;

		Zavodni zavodniAuto = new Zavodni();
		zavodniAuto.vyrobce = "McLaren";

		SUV suvAuto = new SUV("Jeep", "Renegade");

		Osobni skoda = new Osobni("Skoda", "OVJ-87-53");
		Osobni volvo = new Osobni("Volvo", "6T9-1617");

		System.out.println(mojePrvniAuto);
		System.out.println(zavodniAuto);
		System.out.println(suvAuto);
		System.out.println(skoda);
		System.out.println(volvo);
	}
}
