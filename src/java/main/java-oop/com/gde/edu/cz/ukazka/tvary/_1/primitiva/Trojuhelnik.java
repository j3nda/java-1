package com.gde.edu.cz.ukazka.tvary._1.primitiva;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

public class Trojuhelnik
extends Linka
{
	private final Linka aToC;
	private final Linka bToC;

	public Trojuhelnik()
	{
		// aToB ~ linka je resena pomoci 'extends Linka'
		aToC = new Linka();
		bToC = new Linka();

		// TODO: tvary#1: trojuhelnik: vykresli kazdou caru jinou barvou, tj. {A-B, B-C, C-A}
	}

	public void setC(float x, float y)
	{
		aToC.setA(getA().getX(), getA().getY());
		aToC.setB(x, y);

		bToC.setA(getB().getX(), getB().getY());
		bToC.setB(x, y);
	}

	public Bod getC()
	{
		return aToC.getB();
	}

	@Override
	public void setColor(Color color)
	{
		super.setColor(color);
		aToC.setColor(color);
		bToC.setColor(color);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		aToC.draw(batch);
		bToC.draw(batch);
	}
}
