package com.gde.edu.cz.ukazka.auta._1;

class SUV
extends Auto
{
	String model;

	SUV(String vyrobce, String model)
	{
		this.typ = TypAuta.SUV;
		this.vyrobce = vyrobce;
		this.model = model;
	}

	@Override
	public String toString()
	{
		return ""
			+ "model: \"" + model + "\""
			+ ", " + super.toString()
		;
	}
}
