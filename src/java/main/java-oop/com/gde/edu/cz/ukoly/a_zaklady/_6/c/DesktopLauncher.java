package com.gde.edu.cz.ukoly.a_zaklady._6.c;

public class DesktopLauncher
{
	private static final String MESICE_V_ROCE = com.gde.edu.cz.ukoly.a_zaklady._6.a.DesktopLauncher.MESICE_V_ROCE;
	private static final String MESICE_V_ROCE_SK = "SmFudWFyLEZlYnJ1YXIsTWFyZWMsQXByaWwsTWFqLEp1bixKdWwsQXVndXN0LFNlcHRlbWJlcixPa3RvYmVyLE5vdmVtYmVyLERlY2VtYmVy";
	private static final String MESICE_V_ROCE_EN = "SmFudWFyeSxGZWJydWFyeSxNYXJjaCxBcHJpbCxNYXksSnVuZSxKdWx5LEF1Z3VzdCxTZXB0ZW1iZXIsT2N0b2JlcixOb3ZlbWJlcixEZWNlbWJlcg==";
	private static final String DEMO_ENUM_MESIC_1 = "cGFja2FnZSBjb20uZ2RlLmVkdS5jei51a29seS5fNi5iOw0KDQpwdWJsaWMgZW51bSBEZW1vTWVzaWMxeA0KaW1wbGVtZW50cyBJTWVzaWMNCnsNCglMRURFTigiSmFudWFyIiwgIkphbnVhcnkiKSwNCglVTk9SKCJGZWJydWFyIiwgIkZlYnJ1YXJ5IiksDQoJQlJFWkVOKCJNYXJlYyIsICJNYXJjaCIpLA0KCURVQkVOKCJBcHJpbCIsICJBcHJpbCIpLA0KCUtWRVRFTigiTWFqIiwgIk1heSIpLA0KCUNFUlZFTigiSnVuIiwgIkp1bmUiKSwNCglDRVJWRU5FQygiSnVsIiwgIkp1bHkiKSwNCglTUlBFTigiQXVndXN0IiksDQoJWkFSSSgiU2VwdGVtYmVyIiksDQoJUklKRU4oIk9rdG9iZXIiLCAiT2N0b2JlciIpLA0KCUxJU1RPUEFEKCJOb3ZlbWJlciIpLA0KCVBST1NJTkVDKCJEZWNlbWJlciIpLA0KCTsNCglwcml2YXRlIGZpbmFsIFN0cmluZyBzbG92YWs7DQoJcHJpdmF0ZSBmaW5hbCBTdHJpbmcgZW5nbGlzaDsNCg0KCXByaXZhdGUgRGVtb01lc2ljMXgoU3RyaW5nIG5hbWUpDQoJew0KCQl0aGlzKG5hbWUsIG5hbWUpOw0KCX0NCg0KCXByaXZhdGUgRGVtb01lc2ljMXgoU3RyaW5nIHNsb3ZhaywgU3RyaW5nIGVuZ2xpc2gpDQoJew0KCQl0aGlzLnNsb3ZhayA9IHNsb3ZhazsNCgkJdGhpcy5lbmdsaXNoID0gZW5nbGlzaDsNCgl9DQoNCglAT3ZlcnJpZGUNCglwdWJsaWMgU3RyaW5nIHRvRW5nbGlzaCgpDQoJew0KCQlyZXR1cm4gZW5nbGlzaDsNCgl9DQoNCglAT3ZlcnJpZGUNCglwdWJsaWMgU3RyaW5nIHRvU2xvdmFrKCkNCgl7DQoJCXJldHVybiBzbG92YWs7DQoJfQ0KfQ0K";
	private static final String DEMO_ENUM_MESIC_2 = "cGFja2FnZSBjb20uZ2RlLmVkdS5jei51a29seS5fNi5iOw0KDQpwdWJsaWMgZW51bSBEZW1vTWVzaWMyeA0KaW1wbGVtZW50cyBJTWVzaWMNCnsNCglMRURFTiwNCglVTk9SLA0KCUJSRVpFTiwNCglEVUJFTiwNCglLVkVURU4sDQoJQ0VSVkVOLA0KCUNFUlZFTkVDLA0KCVNSUEVOLA0KCVpBUkksDQoJUklKRU4sDQoJTElTVE9QQUQsDQoJUFJPU0lORUMsDQoJOw0KDQoJQE92ZXJyaWRlDQoJcHVibGljIFN0cmluZyB0b0VuZ2xpc2goKQ0KCXsNCgkJc3dpdGNoKHRoaXMpDQoJCXsNCgkJCWNhc2UgTEVERU46ICAgIHJldHVybiAiSmFudWFyeSI7DQoJCQljYXNlIFVOT1I6ICAgICByZXR1cm4gIkZlYnJ1YXJ5IjsNCgkJCWNhc2UgQlJFWkVOOiAgIHJldHVybiAiTWFyY2giOw0KCQkJY2FzZSBEVUJFTjogICAgcmV0dXJuICJBcHJpbCI7DQoJCQljYXNlIEtWRVRFTjogICByZXR1cm4gIk1heSI7DQoJCQljYXNlIENFUlZFTjogICByZXR1cm4gIkp1bmUiOw0KCQkJY2FzZSBDRVJWRU5FQzogcmV0dXJuICJKdWx5IjsNCgkJCWNhc2UgU1JQRU46ICAgIHJldHVybiAiQXVndXN0IjsNCgkJCWNhc2UgWkFSSTogICAgIHJldHVybiAiU2VwdGVtYmVyIjsNCgkJCWNhc2UgUklKRU46ICAgIHJldHVybiAiT2N0b2JlciI7DQoJCQljYXNlIExJU1RPUEFEOiByZXR1cm4gIk5vdmVtYmVyIjsNCgkJCWNhc2UgUFJPU0lORUM6IHJldHVybiAiRGVjZW1iZXIiOw0KCQkJZGVmYXVsdDoNCgkJCQl0aHJvdyBuZXcgUnVudGltZUV4Y2VwdGlvbigiSW52YWxpZCEiKTsNCgkJfQ0KCX0NCg0KCUBPdmVycmlkZQ0KCXB1YmxpYyBTdHJpbmcgdG9TbG92YWsoKQ0KCXsNCgkJc3dpdGNoKHRoaXMpDQoJCXsNCgkJCWNhc2UgTEVERU46ICAgIHJldHVybiAiSmFudWFyIjsNCgkJCWNhc2UgVU5PUjogICAgIHJldHVybiAiRmVicnVhciI7DQoJCQljYXNlIEJSRVpFTjogICByZXR1cm4gIk1hcmVjIjsNCgkJCWNhc2UgRFVCRU46ICAgIHJldHVybiAiQXByaWwiOw0KCQkJY2FzZSBLVkVURU46ICAgcmV0dXJuICJNYWoiOw0KCQkJY2FzZSBDRVJWRU46ICAgcmV0dXJuICJKdW4iOw0KCQkJY2FzZSBDRVJWRU5FQzogcmV0dXJuICJKdWwiOw0KCQkJY2FzZSBTUlBFTjogICAgcmV0dXJuICJBdWd1c3QiOw0KCQkJY2FzZSBaQVJJOiAgICAgcmV0dXJuICJTZXB0ZW1iZXIiOw0KCQkJY2FzZSBSSUpFTjogICAgcmV0dXJuICJPa3RvYmVyIjsNCgkJCWNhc2UgTElTVE9QQUQ6IHJldHVybiAiTm92ZW1iZXIiOw0KCQkJY2FzZSBQUk9TSU5FQzogcmV0dXJuICJEZWNlbWJlciI7DQoJCQlkZWZhdWx0Og0KCQkJCXRocm93IG5ldyBSdW50aW1lRXhjZXB0aW9uKCJJbnZhbGlkISIpOw0KCQl9DQoJfQ0KfQ0K";

	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor enum s nazvem "Mesic" (~jako novy soubor)
		// TODO: 1.1) a jednotlive vyrazy pojmenuj "cesky ~ VELKYMI PISMENY", tak jak jdou po sobe chronologicky
		// TODO: 1.2) enum "Mesic" bude implementovat rozhranni "IMesic" ~ chybejici cast naimplementuj
		// TODO: 1.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertTrue(Mesic.class.isEnum());
//		assertEquals(12, Mesic.values().length);
//		assertInstanceOf(IMesic.class, Mesic.values()[0]);
//		String[] expectedMesice = new String(Base64.getDecoder().decode(MESICE_V_ROCE)).split(",");
//		String[] expectedMesiceSk = new String(Base64.getDecoder().decode(MESICE_V_ROCE_SK)).split(",");
//		String[] expectedMesiceEn = new String(Base64.getDecoder().decode(MESICE_V_ROCE_EN)).split(",");
//		for(int i = 0; i < 12; i++)
//		{
//			assertEquals(expectedMesice[i], Enum.valueOf(Mesic.class, expectedMesice[i]).name());
//			assertEquals(i, Enum.valueOf(Mesic.class, expectedMesice[i]).ordinal());
//			assertEquals(
//				expectedMesiceSk[i].toLowerCase(),
//				Enum.valueOf(Mesic.class, expectedMesice[i]).toSlovak().toLowerCase()
//			);
//			assertEquals(
//				expectedMesiceEn[i].toLowerCase(),
//				Enum.valueOf(Mesic.class, expectedMesice[i]).toEnglish().toLowerCase()
//			);
//		}
//		System.out.println("OK: ukol/1");
	}
}
