package com.gde.edu.cz.ukoly.a_zaklady._6.b;

public class DesktopLauncher
{
	public static final String MESICE_V_ROCE = com.gde.edu.cz.ukoly.a_zaklady._6.a.DesktopLauncher.MESICE_V_ROCE;

	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor enum s nazvem "Mesic" (~jako novy soubor)
		// TODO: 1.1) a jednotlive vyrazy pojmenuj "cesky ~ VELKYMI PISMENY", tak jak jdou po sobe chronologicky
		// TODO: 1.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertTrue(Mesic.class.isEnum());
//		assertEquals(12, Mesic.values().length);
//		String[] expectedMesice = new String(Base64.getDecoder().decode(MESICE_V_ROCE)).split(",");
//		for(int i = 0; i < 12; i++)
//		{
//			assertEquals(expectedMesice[i], Enum.valueOf(Mesic.class, expectedMesice[i]).name());
//			assertEquals(i, Enum.valueOf(Mesic.class, expectedMesice[i]).ordinal());
//		}
//		System.out.println("OK: ukol/1");

		// POZNAMKA: datovy typ 'enum' na objektove vlastnosti, tj. muzes nad nim volat metody()
		// POZNNAMA: metoda: 'name()' ~~~~ vrati nazev
		// POZNNAMA: metoda: 'ordinal()' ~ vrati index (~ordinalni hodnotu), tj. poradi v ramci vyctoveho typu (~enumu)
		//
		// TODO: 2) vypis na obrazovku:
		// TODO: 2.1) vsechny nazvy datoveho typu
		// TODO: 2.2) a jejich 'ordinalni' hodnoty
		//
		// TODO: 2.3) BONUS: idealne to zpracuj cyklem


		// TODO: 3) zamysli se, jak muzes prevest 'String' -> enum a 'int' -> enum
		// TODO: 3.1) NAPOVEDA: trida 'Enum'
		// TODO: 3.2) NAPOVEDA: kazdy datovy typ enum ma metodu 'values()'
		// TODO: 3.3) vypis na obrazovku prevedeny datovy typ
		// TODO: 3.3.1) vypis na obrazovku prevedeny datovy typ: ze String "UNOR" a "SRPEN"
		// TODO: 3.3.2) vypis na obrazovku prevedeny datovy typ: ze int 1 a 7
	}
}
