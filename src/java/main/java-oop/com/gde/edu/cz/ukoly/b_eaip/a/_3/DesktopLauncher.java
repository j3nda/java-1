package com.gde.edu.cz.ukoly.b_eaip.a._3;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * OOP/EAIP: abstraction (~abstrakce)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zobecnit chovani, tak aby:
		//   - byly spolecne pro 'okoli',
		//   - a mohl jsem toto chovani dedit
		//   - a dal upravovat chovani pro potreby odvozenych trid
		//
		assertEquals("Vozidlo", new IVozidlo() { @Override public String getTyp() { return "Vozidlo"; } }.getTyp() );
		System.out.println("OK: ukol/1");


		// TODO: 2) vytvor anonymni tridu (~Auto), ktera bude vracet typ 'Auto'
		// TODO: 2.1) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
		IVozidlo auto = null;
//		assertEquals("Auto", auto.getTyp());
//		System.out.println("OK: ukol/2");

		// TODO: 3) vytvor anonymni tridu (~Motorka), ktera bude vracet typ 'Motorka'
		// TODO: 3.1) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
		IVozidlo motorka = null;
//		assertEquals("Motorka", motorka.getTyp() );
//		System.out.println("OK: ukol/3");
	}

	interface IVozidlo
	{
		/** vrati typ vozidla (~napr: Auto, Motorka, atd) */
		String getTyp();
	}
}
