package com.gde.edu.cz.ukazka.auta._3;

/**
 * jednoducha ukazka pouziti objektu a jejich instanci
 * - NEOBSAHUJE:
 *   - reseni viditelnosti
 *   - promyslene dedicnosti
 *   => coz muze vyustit v problemy...
 */
public class DesktopLauncher
{
	public static void main(String[] args)
	{
		Auto mojePrvniAuto = new Auto("Skoda")
		{
			@Override
			public TypAuta getTyp()
			{
				return TypAuta.Osobni;
			}
		};

		Zavodni zavodniAuto = new Zavodni("McLaren");

		SUV suvAuto = new SUV("Jeep", "Renegade", "BMS-16-29");

		Osobni skoda = new Osobni("Skoda", "OVJ-87-53");
		Osobni volvo = new Osobni("Volvo", "6T9-1617");

		System.out.println(mojePrvniAuto);
		System.out.println(zavodniAuto);
		System.out.println(suvAuto);
		System.out.println(skoda);
		System.out.println(volvo);
	}
}
