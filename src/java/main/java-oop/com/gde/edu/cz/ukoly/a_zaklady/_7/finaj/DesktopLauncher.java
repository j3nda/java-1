package com.gde.edu.cz.ukoly.a_zaklady._7.finaj;

public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor tridu (~class) s nazvem "Student"
		// TODO: 1.1) zarid, aby trida "Student" implementovala rozhranni "IStudent" a napis implementaci
		// TODO: 1.2) zarid, aby promenne pro ulozeni "jmena" a "veku" byly finalni vc. 'private' viditelnosti
		// TODO: 1.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		Student student1 = new Student("honza", 15);
//		Student student2 = new Student("gustav", 18);
//		assertTrue(student1 instanceof IStudent);
//		assertEquals("honza", student1.getJmeno());
//		assertEquals(15, student1.getVek());
//		assertEquals("gustav", student2.getJmeno());
//		assertEquals(18, student2.getVek());
//		for(Field field : Student.class.getDeclaredFields())
//		{
//			assertTrue(Modifier.isPrivate(field.getModifiers()));
//			assertTrue(Modifier.isFinal(field.getModifiers()));
//		}
//		System.out.println("OK: ukol/1");

		// TODO: 2) ve tride "Student" vytvor finalni 'protected' metodu "id()", ktera vrati unikatni identifiaktor studenta
		// TODO: 2.1) pro vypocet id() pozij: "<jmeno>:<vek>"
		// TODO: 2.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		Student student3 = new Student("agraelus", 123);
//		Method metoda = ReflectionUtils.findMethod(Student.class, "id").get();
//		assertTrue(Modifier.isProtected(metoda.getModifiers()));
//		assertTrue(Modifier.isFinal(metoda.getModifiers()));
//		assertEquals("honza:15", student1.id());
//		assertEquals("gustav:18", student2.id());
//		assertEquals("agraelus:123", student3.id());
//		System.out.println("OK: ukol/2");
//
//		// TODO: 3) vytvor tridu (~class) s nazvem "ExtraStudent", ktera bude:
//		// TODO: 3.1) - konecna (~final)
//		// TODO: 3.2) - rozsirovat (~extends) tridu s nazvem "Student" a dopln implementaci
//		// TODO: 3.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
//		//
//		Student student4 = new ExtraStudent("PewDiePie", 111);
//		assertTrue(Modifier.isFinal(ExtraStudent.class.getModifiers()));
//		assertEquals("PewDiePie:111", student4.id());
//		assertEquals("extra:PewDiePie:111", ((ExtraStudent)student4).id(null));
//		System.out.println("OK: ukol/3");

		// TODO: 4) ve tride "ExtraStudent" vytvor:
		// TODO: 4.1) - metodu s nazev "id" (~ktera by mela pretizit chovani finalni metody 'id')
		// TODO: 4.2)   (a to tak, ze vrati slovo "extra" jako prefix puvodniho chovani, tj. "extra:<jmeno>:<vek>")
		// TODO: 4.3) - zkus vymyslet zpusob, jak to udelat (~napr pridanim parametru; dat. typu: Object)
		// TODO: 4.4) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		ExtraStudent student5 = new ExtraStudent("PSY|gangamStyle", 458339457);
//		assertTrue(Modifier.isFinal(ExtraStudent.class.getModifiers()));
//		assertEquals("PSY|gangamStyle:458339457", student5.id());
//		assertEquals("extra:PSY|gangamStyle:458339457", student5.id(null));
//		System.out.println("OK: ukol/4");

		// TODO: 5) vytvor tridu (~class) s nazvem "MegaStudent",
		// TODO: 5.1) ktera bude rozsirovat (~extends) tridu s nazvem "ExtraStudent" a dopln implementaci
		// TODO: 5.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
		// TODO: 5.3) zamysli se, jak by se tento problem dal vyresit...
		// TODO: 5.4) a zduvodni proc?
		//
//		System.out.println("OK: ukol/5 ~ nelze!");
	}
}
