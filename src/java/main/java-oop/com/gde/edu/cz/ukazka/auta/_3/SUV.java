package com.gde.edu.cz.ukazka.auta._3;

class SUV
extends Osobni
implements IObsahujeModel
{
	private final String model;

	SUV(String vyrobce, String model, String SPZ)
	{
		super(vyrobce, SPZ);
		this.model = model;
	}

	@Override
	public TypAuta getTyp()
	{
		return TypAuta.SUV;
	}

	@Override
	public String toString()
	{
		return ""
			+ "model: \"" + model + "\""
			+ ", " + super.toString()
		;
	}

	@Override
	public String getModel()
	{
		return model;
	}
}
