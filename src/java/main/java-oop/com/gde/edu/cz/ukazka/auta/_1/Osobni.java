package com.gde.edu.cz.ukazka.auta._1;

class Osobni
extends Auto
{
	String SPZ;

	Osobni(String vyrobce, String SPZ)
	{
		this.typ = TypAuta.Osobni;
		this.vyrobce = vyrobce;
		this.SPZ = SPZ;
	}

	@Override
	public String toString()
	{
		return ""
			+ "SPZ: \"" + SPZ + "\""
			+ ", " + super.toString()
		;
	}
}
