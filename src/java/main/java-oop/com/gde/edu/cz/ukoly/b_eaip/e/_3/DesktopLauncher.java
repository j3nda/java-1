package com.gde.edu.cz.ukoly.b_eaip.e._3;

import java.lang.reflect.Modifier;

/**
 * OOP/EAIP: encapsulation (~zapouzdreni)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zapouzdrit data, tak aby:
		//   - byly skryte pro 'okoli',
		//   - a mohl jsem s nimi pracovat uvnitr tridy
		//
		// TODO: 1) v tomto balicku vytvor tridu (~class) s nazvem "OsobniAuto"
		// TODO: 2) trida "OsobniAuto" bude mit:
		// TODO: 2.1) konstruktor: ktery akceptuje parametry (Znacka, pocetDveri, vykonMotoru)
		// TODO: 2.2) trida "OsobniAuto" bude mit viditelnost package-private (~default)
		// TODO: 3) trida "OsobniAuto" veskere sve vnitrni atributy:
		// TODO: 3.1) nebude skryvat pred okolim
		// TODO: 3.2) atributy budou finalni
		// TODO: 3.3) atributy se budou jmenovat: znacka, pocetDveri, vykonMotoru
		// TODO: 5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertNotNull(new OsobniAuto(null, 0, 0));
//		System.out.println("OK: ukol/1");
//
//		OsobniAuto auto1 = new OsobniAuto(Znacka.Skoda, 5, 1600);
//		OsobniAuto auto2 = new OsobniAuto(Znacka.Bmw, 3, 2100);
//		assertEquals(Znacka.Skoda, auto1.znacka);
//		assertEquals(Znacka.Bmw, auto2.znacka);
//		assertEquals(5, auto1.pocetDveri);
//		assertEquals(3, auto2.pocetDveri);
//		assertEquals(1600, auto1.vykonMotoru);
//		assertEquals(2100, auto2.vykonMotoru);
//		assertTrue(isPackagePrivate(OsobniAuto.class.getModifiers()));
//		assertEquals(0, OsobniAuto.class.getDeclaredMethods().length);
//		System.out.println("OK: ukol/2");
//
//		assertEquals(3, auto1.getClass().getDeclaredFields().length);
//		for(Field f : auto1.getClass().getDeclaredFields())
//		{
//			assertTrue(Modifier.isPublic(f.getModifiers()) || isPackagePrivate(f.getModifiers()));
//			assertTrue(Modifier.isFinal(f.getModifiers()));
//		}
//		System.out.println("OK: ukol/3");
	}

	private static boolean isPackagePrivate(int mod)
	{
		return
			   !Modifier.isPublic(mod)
			&& !Modifier.isProtected(mod)
			&& !Modifier.isPrivate(mod)
		;
	}
}
