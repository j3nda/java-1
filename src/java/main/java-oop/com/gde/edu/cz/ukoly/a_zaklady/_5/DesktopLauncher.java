package com.gde.edu.cz.ukoly.a_zaklady._5;

public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor balicek (~package) s nazvem "autobox"
		// TODO: 1.1) v tomto balicku(autobox) vytvor tridu (~class) s nazvem "AutoBoxing"
		// TODO: 1.2) trida "AutoBoxing" bude implementovat rozhranni "IAutoBoxing"
		// TODO: 1.3) obsahovat konstruktor s temito parametry
		// TODO: 1.3.1) konstruktor(byte b)
		// TODO: 1.3.2) konstruktor(int i)
		// TODO: 1.3.3) konstruktor(float f)
		// TODO: 1.3.4) konstruktor(boolean b)
		// TODO: 1.3.5) konstruktor(byte b, int i, float f, boolean b)
		// TODO: 1.4) napis implementaci "auto-boxingu", kdy parametry z konstruktoru(1.3) si ulozis
		//            a jejich hodnoty vratis pomoci 'objektovych datovych typu'(1.2)
		// TODO: 1.5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		AutoBoxing ab1 = new AutoBoxing((byte)0, 0, 0f, false);
//
//		assertEquals(Byte.valueOf((byte)0), ab1.getByte());
//		assertEquals(Integer.valueOf(0), ab1.getInteger());
//		assertEquals(Float.valueOf(0), ab1.getFloat());
//		assertEquals(Boolean.valueOf(false), ab1.getBoolean());
//		assertEquals(Byte.valueOf((byte)127), new AutoBoxing((byte)-129).getByte());
//		assertEquals(Integer.valueOf(32767), new AutoBoxing(32767).getInteger());
//		assertEquals(Float.valueOf(325f), new AutoBoxing(325f).getFloat());
//		assertEquals(Boolean.valueOf(false), new AutoBoxing(false).getBoolean());
//		assertEquals(Boolean.valueOf(true), new AutoBoxing(true).getBoolean());
//		assertEquals(null, new AutoBoxing((byte)-129).getInteger());
//		assertEquals(null, new AutoBoxing(-129).getFloat());
//		assertEquals(null, new AutoBoxing(129f).getByte());
//		assertEquals(null, new AutoBoxing(129).getBoolean());
//		System.out.println("OK: ukol/1");

		// TODO: 2) vytvor balicek (~package) s nazvem "autobox" (~pokud existuje, preskoc)
		// TODO: 2.1) v balicku(autobox) vytvor tridu (~class) s nazvem "AutoUnboxing"
		// TODO: 2.2) trida "AutoUnboxing" bude implementovat rozhranni "IAutoUnboxing"
		// TODO: 2.3) obsahovat konstruktor s temito parametry
		// TODO: 2.3.1) konstruktor(byte b)
		// TODO: 2.3.2) konstruktor(int i)
		// TODO: 2.3.3) konstruktor(float f)
		// TODO: 2.3.4) konstruktor(boolean b)
		// TODO: 2.3.5) konstruktor(byte b, int i, float f, boolean b)
		// TODO: 2.4) napis implementaci "auto-unboxingu", kdy parametry z konstruktoru(2.3) si ulozis
		//            a jejich hodnoty vratis pomoci 'objektovych datovych typu'(2.2)
		// TODO: 2.5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		AutoUnboxing ab2 = new AutoUnboxing((byte)0, 0, 0f, false);
//
//		assertEquals((byte)0, ab2.getByte());
//		assertEquals(0, ab2.getInteger());
//		assertEquals(0f, ab2.getFloat());
//		assertEquals(false, ab2.getBoolean());
//		assertEquals((byte)127, new AutoUnboxing((byte)-129).getByte());
//		assertEquals(32767, new AutoUnboxing(32767).getInteger());
//		assertEquals(325f, new AutoUnboxing(325f).getFloat());
//		assertEquals(false, new AutoUnboxing(false).getBoolean());
//		assertEquals(true, new AutoUnboxing(true).getBoolean());
//		assertEquals(0, new AutoUnboxing((byte)-129).getInteger());
//		assertEquals(0, new AutoUnboxing(-129).getFloat());
//		assertEquals(0, new AutoUnboxing(129f).getByte());
//		assertEquals(false, new AutoUnboxing(129).getBoolean());
//		System.out.println("OK: ukol/2");
	}
}
