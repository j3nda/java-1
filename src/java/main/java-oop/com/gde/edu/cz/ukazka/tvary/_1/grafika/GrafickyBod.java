package com.gde.edu.cz.ukazka.tvary._1.grafika;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.common.resources.CommonResources;

public class GrafickyBod
extends Image
{
	public GrafickyBod(int size, Color color)
	{
		super(new Texture(Gdx.files.internal(CommonResources.Tint.square16x16)));
		setColor(color);
		setSize(size, size);
	}
}
