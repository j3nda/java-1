package com.gde.edu.cz.ukoly.a_zaklady._3.calc;

public interface IKalkulator
{
	int secti(int x, int y);
	int odecti(int x, int y);
	int nasob(int x, int y);
	int vydel(int x, int y);
}
