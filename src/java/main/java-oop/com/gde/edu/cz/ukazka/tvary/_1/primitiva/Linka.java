package com.gde.edu.cz.ukazka.tvary._1.primitiva;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gde.edu.cz.ukazka.tvary._1.grafika.GrafickyBod;
import com.gde.edu.cz.ukazka.tvary._1.grafika.IRenderable;

public class Linka
implements IRenderable
{
	private final Bod a;
	private final Bod b;
;
	public Linka()
	{
		super();
		a = new Bod();
		b = new Bod();
	}

	public void setA(float x, float y)
	{
		a.setX(x);
		a.setY(y);
	}

	public Bod getA()
	{
		return a;
	}

	public void setB(float x, float y)
	{
		b.setX(x);
		b.setY(y);
	}

	public Bod getB()
	{
		return b;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		// @see:
		// -- https://ghost-together.medium.com/how-to-code-your-first-algorithm-draw-a-line-ca121f9a1395
		// -- https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#Java
		// -- https://www.javatpoint.com/computer-graphics-bresenhams-line-algorithm

		int x1 = (int) a.getX();
		int y1 = (int) a.getY();
		int x2 = (int) b.getX();
		int y2 = (int) b.getY();

		// delta of exact value and rounded value of the dependent variable
		int d = 0;

		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);

        int dx2 = 2 * dx; // slope scaling factors to
        int dy2 = 2 * dy; // avoid floating point

        int ix = x1 < x2 ? 1 : -1; // increment direction
        int iy = y1 < y2 ? 1 : -1;

		int x = x1;
		int y = y1;

		GrafickyBod grafika = a.getGrafika();
		batch.begin();
		Color prevColor = batch.getColor();
		batch.setColor(grafika.getColor());

		if (dx >= dy)
		{
			while (true)
			{
				grafika.getDrawable().draw(
					batch,
					x,
					y,
					grafika.getWidth(),
					grafika.getHeight()
				);
				if (x == x2)
				{
					break;
				}
				x += ix;
				d += dy2;
				if (d > dx)
				{
					y += iy;
					d -= dx2;
				}
			}
		}
		else
		{
			while (true)
			{
				grafika.getDrawable().draw(
					batch,
					x,
					y,
					grafika.getWidth(),
					grafika.getHeight()
				);
				if (y == y2)
				{
					break;
				}
				y += iy;
				d += dx2;
				if (d > dy)
				{
					x += ix;
					d -= dy2;
				}
			}
		}
		batch.setColor(prevColor);
		batch.end();
	}


	@Override
	public void draw(Batch batch)
	{
		draw(batch, 1f);
	}

	@Override
	public void setColor(Color color)
	{
		a.setColor(color);
		b.setColor(color);
	}

	@Override
	public GrafickyBod getGrafika()
	{
		throw new RuntimeException("Not implemented!");
	}
}
