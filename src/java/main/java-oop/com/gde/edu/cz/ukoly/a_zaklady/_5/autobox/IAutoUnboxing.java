package com.gde.edu.cz.ukoly.a_zaklady._5.autobox;

public interface IAutoUnboxing
{
	byte getByte();
	int getInteger();
	float getFloat();
	boolean getBoolean();
}
