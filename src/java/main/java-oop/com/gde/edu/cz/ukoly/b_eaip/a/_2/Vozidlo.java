package com.gde.edu.cz.ukoly.b_eaip.a._2;

abstract class Vozidlo
{
	/** vrati typ vozidla (~napr: Auto, Motorka, atd) */
	abstract String getTyp();
}
