package com.gde.edu.cz.ukoly.a_zaklady._5.autobox;

public interface IAutoBoxing
{
	Byte getByte();
	Integer getInteger();
	Float getFloat();
	Boolean getBoolean();
}
