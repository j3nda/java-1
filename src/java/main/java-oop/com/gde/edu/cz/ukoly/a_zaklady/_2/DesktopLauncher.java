package com.gde.edu.cz.ukoly.a_zaklady._2;

public class DesktopLauncher
{
	public static void main(String[] args)
	{
		// TODO: 1) vytvor balicek (~package) s nazvem "konstruktor"
		// TODO: 2) v tomto balicku(konstruktor) vytvor tridu (~class) s nazvem "Vychozi"
		// TODO: 2.1) implementuj 'vychozi' konstruktor
		//
		// TODO: 3) v tomto balicku(konstruktor) vytvor tridu (~class) s nazvem "BezParametru"
		// TODO: 3.1) implementuj 'bezparametricky' konstruktor
		//
		// TODO: 4) v tomto balicku(konstruktor) vytvor tridu (~class) s nazvem "Parametricky"
		// TODO: 4.1) implementuj parametricky konstruktor, ma jako parametr "String"
		// TODO: 4.2) implementuj parametricky konstruktor, ma jako parametr "int"
		// TODO: 4.3) implementuj parametricky konstruktor, ma jako parametr "String" a "int"
		//
		// TODO: 4) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
//		Vychozi vychozi = new Vychozi();
//		System.out.println(vychozi);
//
//		BezParametru bezParametru = new BezParametru();
//		System.out.println(bezParametru);
//
//		Parametricky param1 = new Parametricky("Dnes je streda!");
//		Parametricky param2 = new Parametricky(2022);
//		Parametricky param3 = new Parametricky("Dnes je streda!", 2022);
//		System.out.println(param1);
//		System.out.println(param2);
//		System.out.println(param3);
	}
}
