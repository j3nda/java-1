package com.gde.edu.cz.ukoly.b_eaip.a._2;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * OOP/EAIP: abstraction (~abstrakce)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zobecnit chovani, tak aby:
		//   - byly spolecne pro 'okoli',
		//   - a mohl jsem toto chovani dedit
		//   - a dal upravovat chovani pro potreby odvozenych trid
		//
		assertEquals("Vozidlo", new Vozidlo() { @Override String getTyp() { return "Vozidlo"; } }.getTyp() );
		assertEquals("Auto", new Vozidlo() { @Override String getTyp() { return "Auto"; } }.getTyp() );
		assertEquals("Motorka", new Vozidlo() { @Override String getTyp() { return "Motorka"; } }.getTyp() );
		System.out.println("OK: ukol/1");


		// TODO: 2) vytvor tridu 'Auto', ktera dedi tridu 'Vozidlo' a dopis implementaci
		// TODO: 3) vytvor tridu 'Motorka', ktera dedi tridu 'Vozidlo' a dopis implementaci
		// TODO: 4) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertEquals("Vozidlo", new Vozidlo() { @Override String getTyp() { return "Vozidlo"; } }.getTyp() );
//		assertEquals("Auto", new Auto().getTyp());
//		assertEquals("Motorka", new Motorka().getTyp() );
//		System.out.println("OK: ukol/2");
	}
}
