package com.gde.edu.cz.ukazka.auta._3;

interface IAuto
{
	String getVyrobce();
	TypAuta getTyp();
}
