package com.gde.edu.cz.ukoly.a_zaklady._7.statik;

public class DesktopLauncher
{
	// pokud nevis... mrkni na: 'UkazkaJakNaTo'
	public static void main(String[] args) throws Exception
	{
		// TODO: 1) vytvor staticke pocitadlo
		// TODO: 1.1) v tomto balicku(statik) vytvor tridu (~class) s nazvem "Pocitadlo"
		// TODO: 1.2) trida "Pocitadlo" bude mit metody:
		// TODO: 1.2.1) void reset(); ~~~ provede reset pocitadla
		// TODO: 1.2.2) int stav(); ~~~~~ vrati stav pocitadla
		// TODO: 1.2.3) int zapocti(); ~ zapocita +1 pro pocitadlo a vrati vysledek
		// TODO: 1.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		for(String jmeno : new String[] {"reset", "stav", "zapocti"})
//		{
//			Method metoda = ReflectionUtils.findMethod(Pocitadlo.class, jmeno).get();
//			assertNotNull(metoda);
//			assertTrue(Modifier.isStatic(metoda.getModifiers()));
//			switch(jmeno)
//			{
//				case "reset":
//				{
//					assertEquals(void.class, metoda.getReturnType());
//					break;
//				}
//				case "stav":
//				case "zapocti":
//				{
//					assertEquals(int.class, metoda.getReturnType());
//					break;
//				}
//			}
//		}
//		System.out.println("OK: ukol/1 ~ predpis tridy");
//		assertEquals(0, Pocitadlo.stav());
//		assertEquals(1, Pocitadlo.zapocti());
//		assertEquals(1, Pocitadlo.stav());
//		Pocitadlo.reset();
//		assertEquals(0, Pocitadlo.stav());
//		assertEquals(1, Pocitadlo.zapocti());
//		assertEquals(2, Pocitadlo.zapocti());
//		assertEquals(2, Pocitadlo.stav());
//		System.out.println("OK: ukol/1 ~ implementace tridy [static]");
//		Pocitadlo pocitadlo11 = new Pocitadlo();
//		Pocitadlo pocitadlo12 = new Pocitadlo();
//		assertEquals(2, pocitadlo11.stav());
//		assertEquals(2, pocitadlo12.stav());
//		assertEquals(3, pocitadlo11.zapocti());
//		assertEquals(4, pocitadlo12.zapocti());
//		assertEquals(4, pocitadlo11.stav());
//		assertEquals(4, pocitadlo12.stav());
//		pocitadlo11.reset();
//		assertEquals(0, pocitadlo11.stav());
//		assertEquals(0, pocitadlo12.stav());
//		assertEquals(1, pocitadlo11.zapocti());
//		assertEquals(2, pocitadlo12.zapocti());
//		assertEquals(2, Pocitadlo.stav());
//		System.out.println("OK: ukol/1 ~ implementace tridy [dynamic]");

		// TODO: 2) vytvor dynamicke pocitadlo
		// TODO: 2.1) uvnitr tridy "Pocitadlo" vytvor 'vnitrni statickou' tridu s nazvem "DynamickePocitadlo"
		// TODO: 2.2) zarid, aby trida "DynamickePocitadlo" implementovala rozhranni "IPocitadlo" a napis implementaci
		// TODO: 2.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		for(String jmeno : new String[] {"reset", "stav", "zapocti"})
//		{
//			Method metoda = ReflectionUtils.findMethod(DynamickePocitadlo.class, jmeno).get();
//			assertNotNull(metoda);
//			switch(jmeno)
//			{
//				case "reset":
//				{
//					assertEquals(void.class, metoda.getReturnType());
//					break;
//				}
//				case "stav":
//				case "zapocti":
//				{
//					assertEquals(int.class, metoda.getReturnType());
//					break;
//				}
//			}
//		}
//		System.out.println("OK: ukol/2 ~ predpis tridy");
//		IPocitadlo pocitadlo21 = new DynamickePocitadlo();
//		IPocitadlo pocitadlo22 = new DynamickePocitadlo();
//		assertEquals(0, pocitadlo21.stav());
//		assertEquals(0, pocitadlo22.stav());
//		assertEquals(1, pocitadlo21.zapocti());
//		assertEquals(1, pocitadlo22.zapocti());
//		assertEquals(1, pocitadlo21.stav());
//		assertEquals(1, pocitadlo22.stav());
//		pocitadlo21.reset();
//		pocitadlo22.reset();
//		assertEquals(0, pocitadlo21.stav());
//		assertEquals(0, pocitadlo22.stav());
//		assertEquals(2, Pocitadlo.stav());
//		System.out.println("OK: ukol/2 ~ implementace tridy [dynamic]");

		// TODO: 3) ve tride 'Pocitadlo' zajisti, aby:
		// TODO: 3.1) mela 2x dynamicke pocitadla pristupna statickou cestou
		// TODO: 3.2) - promenna pro 1. pocitadlo: 'A'
		// TODO: 3.3) - promenna pro 2. pocitadlo: 'B'
		// TODO: 3.4) k inicializaci pouzij staticky blok kodu
		// TODO: 3.5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		for(String jmeno : new String[] {"A", "B"})
//		{
//			Field field = ReflectionUtils.findFields(Pocitadlo.class, f -> f.getName().equals(jmeno), HierarchyTraversalMode.TOP_DOWN).get(0);
//			assertTrue(Modifier.isStatic(field.getModifiers()));
//			assertTrue(IPocitadlo.class.isAssignableFrom(field.getType()));
//		}
//		System.out.println("OK: ukol/3 ~ pristup ke staticke promenne");
//		assertEquals(0, Pocitadlo.A.stav());
//		assertEquals(0, Pocitadlo.B.stav());
//		assertEquals(1, Pocitadlo.A.zapocti());
//		assertEquals(1, Pocitadlo.B.zapocti());
//		assertEquals(1, Pocitadlo.A.stav());
//		assertEquals(1, Pocitadlo.B.stav());
//		Pocitadlo.A.reset();
//		Pocitadlo.B.reset();
//		assertEquals(0, Pocitadlo.A.stav());
//		assertEquals(0, Pocitadlo.B.stav());
//		assertEquals(2, Pocitadlo.stav());
//		System.out.println("OK: ukol/3 ~ implementace tridy [static -> dynamic]");
	}
}
