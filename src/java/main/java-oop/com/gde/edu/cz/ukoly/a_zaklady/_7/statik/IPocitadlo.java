package com.gde.edu.cz.ukoly.a_zaklady._7.statik;

public interface IPocitadlo
{
	/** vynuluje pocitadlo */
	void reset();
	/** vrati stav pocitadla */
	int stav();
	/** zvysi hodnotu pocitadla, tj. provede +1 */
	int zapocti();
}
