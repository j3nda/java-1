package com.gde.edu.cz.ukoly.a_zaklady._6.c;

interface IMesic
{
	/**
	 * vrati nazev mesice 'anglicky',
	 * @see: https://www.math-only-math.com/images/xmonths-of-the-year.png.pagespeed.ic.b9Gjj7Ai4k.webp
	 */
	String toEnglish();

	/**
	 * vrati nazev mesice 'slovensky' (~bez diakritiky),
	 * @see: https://www.lingohut.com/cs/v512119/lekce-sloven%C5%A1tiny-m%C4%9Bs%C3%ADce-v-roce
	 */
	String toSlovak();
}
