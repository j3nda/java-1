package com.gde.edu.cz.ukazka.tvary._1.grafika;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

public interface IRenderable
{
	GrafickyBod getGrafika();
	void setColor(Color color);
	void draw(Batch batch, float parentAlpha);
	void draw(Batch batch);

	// TODO: tvary#1: IRenderable: implementuj ruznou velikost {bod, linka, trojuhelnik, obdelnik, aj}
}
