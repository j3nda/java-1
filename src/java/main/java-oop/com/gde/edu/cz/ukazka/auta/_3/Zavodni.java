package com.gde.edu.cz.ukazka.auta._3;

class Zavodni
extends Auto
{
	Zavodni(String vyrobce)
	{
		super(vyrobce);
	}

	@Override
	public TypAuta getTyp()
	{
		return TypAuta.Zavodni;
	}
}
