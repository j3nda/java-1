package com.gde.edu.cz.ukoly.a_zaklady._7.statik;

// ukazka: jak na to?
// - klicove slovo 'static' definuje, ze:
//   - existuje pouze 1x v pameti
//   - muzes ji pouzit v:
//     - trida
//     - blok-kodu (~jako soucast tridy)
//     - promenna
//     - metoda
//   - 'promenna' a 'blok-kodu' ~ zalezi na poradi (~shora-dolu)
//   -
public class UkazkaJakNaTo
{
	// staticka promenna 'a'
	static int a = metoda("A", 123);

	// staticky blok
	static
	{
		System.out.println("uvnitr statickeho bloku ~ static { ... }");
	}

	// staticka promenna 'b'
	static int b = metoda("B", 321);

	// staticka methoda
	static int metoda(String id, int cislo)
	{
		System.out.println("uvnitr staticke metody ~~ metoda(String id, int cislo);");
		return cislo;
	}

	// staticka methoda "main" !!!
	public static void main(String[] args)
	{
		System.out.println("---");
		System.out.println("uvnitr staticke metody ~~ \"main(String[] args)\"");
		System.out.println("hodnota promenne: \"a\" = " + a);
		System.out.println("hodnota promenne: \"b\" = " + b);

		System.out.println("---");
		UkazkaJakNaTo dynamickaPromenna = new UkazkaJakNaTo();
		System.out.println("hodnota promenne: \"dynamickaPromenna\" = " + dynamickaPromenna);
		dynamickaPromenna.a++;
		System.out.println("hodnota promenne: \"a\" = " + a + " // ze staticke tridy");
		System.out.println("hodnota promenne: \"a\" = " + dynamickaPromenna.a + " // z dynamicke tridy");
		System.out.println("---");
		System.out.println("volani staticke metody dynamicky: dynamickaPromenna.metoda(\"C\", 1230);");
		int c = dynamickaPromenna.metoda("C", 1230);
		System.out.println("hodnota promenne: \"c\" = " + c + " // jako vysledek volani staticke metoda();");
	}
}
