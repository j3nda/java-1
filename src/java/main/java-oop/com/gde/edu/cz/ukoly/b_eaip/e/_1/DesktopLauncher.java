package com.gde.edu.cz.ukoly.b_eaip.e._1;

import java.lang.reflect.Modifier;

/**
 * OOP/EAIP: encapsulation (~zapouzdreni)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zapouzdrit data, tak aby:
		//   - byly skryte pro 'okoli',
		//   - a mohl jsem s nimi pracovat uvnitr tridy
		//
		// TODO: 1) v tomto balicku vytvor tridu (~class) s nazvem "OsobniAuto"
		// TODO: 2) trida "OsobniAuto" bude mit:
		// TODO: 2.1) settery:
		// TODO: 2.1.1) void setZnacka(Znacka znacka);
		// TODO: 2.1.2) void setpocetDveri(int pocetDveri);
		// TODO: 2.1.3) void setVykonMotoru(int kw);
		// TODO: 2.2) gettery:
		// TODO: 2.2.1) Znacka getZnacka();
		// TODO: 2.2.2) int getpocetDveri();
		// TODO: 2.2.3) int getVykonMotoru();
		// TODO: 2.3) trida "OsobniAuto" bude mit viditelnost package-private (~default)
		// TODO: 3) trida "OsobniAuto" bude veskere sve vnitrni atributy skryvat pred okolim
		// TODO: 4) uprav viditelnost ve tride "OsobniAuto" tak, ze:
		// TODO: 4.1) settery budou package-private (~default)
		// TODO: 4.2) gettery budou public
		// TODO: 5) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		OsobniAuto auto1 = new OsobniAuto();
//		System.out.println("OK: ukol/1");
//
//		assertNull(auto1.getZnacka());
//		assertEquals(0, auto1.getPocetDveri());
//		assertEquals(0, auto1.getVykonMotoru());
//
//		assertTrue(isPackagePrivate(auto1.getClass().getModifiers()));
//		auto1.setZnacka(Znacka.Skoda); assertEquals(Znacka.Skoda, auto1.getZnacka());
//		auto1.setPocetDveri(5); assertEquals(5, auto1.getPocetDveri());
//		auto1.setVykonMotoru(1600); assertEquals(1600, auto1.getVykonMotoru());
//		System.out.println("OK: ukol/2");
//
//		for(Field f : auto1.getClass().getDeclaredFields())
//		{
//			assertTrue(Modifier.isPrivate(f.getModifiers()));
//		}
//		System.out.println("OK: ukol/3");
//
//		for(String metoda : new String[] {"Znacka", "PocetDveri", "VykonMotoru"})
//		{
//			Method m = ReflectionUtils.findMethod(auto1.getClass(), "get" + metoda).get();
//			assertNotNull(m);
//			assertTrue(Modifier.isPublic(m.getModifiers()));
//			switch(metoda)
//			{
//				case "Znacka":
//				{
//					m = ReflectionUtils.findMethod(auto1.getClass(), "set" + metoda, Znacka.class).get();
//					break;
//				}
//				default:
//				{
//					m = ReflectionUtils.findMethod(auto1.getClass(), "set" + metoda, int.class).get();
//					break;
//				}
//			}
//			assertNotNull(m);
//			assertTrue(isPackagePrivate(m.getModifiers()));
//		}
//		System.out.println("OK: ukol/4");
//
//		auto1.setPocetDveri(3); assertEquals(3, auto1.getPocetDveri());
//		auto1.setVykonMotoru(900); assertEquals(900, auto1.getVykonMotoru());
//		OsobniAuto auto2 = new OsobniAuto();
//		auto2.setZnacka(Znacka.Bmw); assertEquals(Znacka.Bmw, auto2.getZnacka());
//		auto2.setPocetDveri(2); assertEquals(2, auto2.getPocetDveri());
//		assertEquals(0, auto2.getVykonMotoru());
//		System.out.println("OK: ukol/5");
	}

	private static boolean isPackagePrivate(int mod)
	{
		return
			   !Modifier.isPublic(mod)
			&& !Modifier.isProtected(mod)
			&& !Modifier.isPrivate(mod)
		;
	}
}
