package com.gde.edu.cz.ukoly.a_zaklady._3;

public class DesktopLauncher
{
	public static void main(String[] args)
	{
		// TODO: 1) v balicku(calc) vytvor tridu (~class) s nazvem "Kalkulator"
		// TODO: 1.1) zarid, aby trida "Kalkulator" implementovala rozhranni "IKalkulator" a napis implementaci
		// TODO: 1.2) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		Kalkulator calc1 = new Kalkulator();
//
//		assertEquals(15, calc1.secti(7, 8));
//		assertEquals(33, calc1.odecti(46, 13));
//		assertEquals(56, calc1.nasob(8, 7));
//		assertEquals(4,  calc1.vydel(32, 8));
//		System.out.println("OK: ukol/1");


		// TODO: 2) v balicku(calc) vytvor tridu (~class) s nazvem "ChytryKalkulator"
		// TODO: 2.1) zarid, aby trida "ChytryKalkulator" implementovala rozhranni "IChytryKalkulator" a napis implementaci
		//
//		ChytryKalkulator calc2 = new ChytryKalkulator();
//
//		assertEquals(15,  calc2.secti(7, 8));
//		assertEquals(35,  calc2.secti(7, 8, 9, 0, 11));
//		assertEquals(33,  calc2.odecti(46, 13));
//		assertEquals(27,  calc2.odecti(46, 13, 1, 0, 5));
//		assertEquals(56,  calc2.nasob(8, 7));
//		assertEquals(0,   calc2.nasob(8, 7, 0));
//		assertEquals(112, calc2.nasob(8, 7, 2, 1));
//		assertEquals(4,  calc2.vydel(32, 8));
//		assertEquals(4,  calc2.vydel(32, 8, 0, 1));
//		assertEquals(2,  calc2.vydel(32, 8, 2, 1));
//		System.out.println("OK: ukol/2");


		// TODO: 3) zarid, aby trida "Kalkulator" mela metodu "protected int vysledek()" ktera bude pracovat s typem enum Operace
		// TODO: 3.1) vhodne zrefaktoruj, abys nemel duplicitni kod!
		// TODO: 3.2) deleni "0" (~nulou) by nemelo skoncit chybou! ...takove deleni preskoc!
		// TODO: 3.3) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		Method metodaVysledek = ReflectionSupport
//			.findMethod(
//				calc2.getClass(),
//				"vysledek",
//				new Class<?>[] { Operace.class, int[].class }
//			)
//			.get()
//		;
//		assertEquals(2, metodaVysledek.getParameterCount());
//		assertTrue(Modifier.isProtected(metodaVysledek.getModifiers()));
//		System.out.println("OK: ukol/3");
	}
}
