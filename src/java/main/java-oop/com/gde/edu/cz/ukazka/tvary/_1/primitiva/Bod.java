package com.gde.edu.cz.ukazka.tvary._1.primitiva;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gde.edu.cz.ukazka.tvary._1.grafika.GrafickyBod;
import com.gde.edu.cz.ukazka.tvary._1.grafika.IRenderable;

public class Bod
implements IRenderable
{
	private float x;
	private float y;
	private final GrafickyBod grafika;

	public Bod()
	{
		grafika = new GrafickyBod(5, Color.WHITE);
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public void setX(float x)
	{
		this.x = x;
		grafika.setX(x);
	}

	public void setY(float y)
	{
		this.y = y;
		grafika.setY(y);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		batch.begin();
		grafika.draw(batch, parentAlpha);
		batch.end();
	}

	@Override
	public void draw(Batch batch)
	{
		draw(batch, 1f);
	}

	@Override
	public void setColor(Color color)
	{
		grafika.setColor(color);
	}

	@Override
	public GrafickyBod getGrafika()
	{
		return grafika;
	}
}
