package com.gde.edu.cz.ukoly.a_zaklady._3.calc;

public enum Operace
{
	Scitani,
	Odcitani,
	Nasobeni,
	Deleni,
}
