package com.gde.edu.cz.ukazka.tvary._1;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gde.edu.cz.ukazka.tvary._1.pokrocile.Kruznice;
import com.gde.edu.cz.ukazka.tvary._1.pokrocile.Obdelnik;
import com.gde.edu.cz.ukazka.tvary._1.primitiva.Bod;
import com.gde.edu.cz.ukazka.tvary._1.primitiva.Linka;
import com.gde.edu.cz.ukazka.tvary._1.primitiva.Trojuhelnik;

public class TvaryApp
extends ApplicationAdapter
{
	private SpriteBatch batch;
	private Bod bod;
	private Linka linka;
	private Trojuhelnik trojuhelnik;
	private Obdelnik obdelnik;
	private Kruznice kruznice;


	@Override
	public void create()
	{
		batch = new SpriteBatch(); // vytvori kontejner, ktery vykresluje pomoci OpenGL

		bod = new Bod();
		bod.setX(100);
		bod.setY(200);

		linka = new Linka();
		linka.setA(50, 50);
		linka.setB(
			Gdx.graphics.getWidth()  - 50,
			Gdx.graphics.getHeight() - 50
		);

		trojuhelnik = new Trojuhelnik();
		trojuhelnik.setA(
			50,
			Gdx.graphics.getHeight() - 50
		);
		trojuhelnik.setB(
			(Gdx.graphics.getWidth() / 2f) - 50,
			Gdx.graphics.getHeight() - 50
		);
		trojuhelnik.setC(
			Gdx.graphics.getWidth() / 4f,
			50
		);

		obdelnik = new Obdelnik();
		obdelnik.setA(
			Gdx.graphics.getWidth() - 50,
			50
		);
		obdelnik.setB(
			Gdx.graphics.getWidth() - 50,
			Gdx.graphics.getHeight() - 50
		);
		obdelnik.setC(
			(Gdx.graphics.getWidth() / 2f)- 50,
			Gdx.graphics.getHeight() - 50
		);
		obdelnik.setD(
			(Gdx.graphics.getWidth() / 2f)- 50,
			50
		);

		kruznice = new Kruznice();
		kruznice.setX(Gdx.graphics.getWidth() / 2f);
		kruznice.setY(Gdx.graphics.getHeight()/ 2f);
		kruznice.setPolomer(
			Math.min(
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight()/ 2f
			)
		);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		// vykresli elementy @see: IRenderable
		bod.draw(batch);
		linka.draw(batch);
		trojuhelnik.draw(batch);
		kruznice.draw(batch);
	}

	@Override
	public void dispose()
	{
		batch.dispose();
	}
}
