package com.gde.edu.cz.ukoly.b_eaip.a._1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * OOP/EAIP: abstraction (~abstrakce)
 * - oop  ~ object oriented programming (~objektove orientovane programovani)
 * - eaip ~ encapsulation / abstraction / inheritance / poly-morphism
 *          (zapouzdreni  / abstrakce   / dedicnost   / vice-tvarost)
 */
public class DesktopLauncher
{
	public static void main(String[] args) throws Exception
	{
		// PROC?
		// - chci zobecnit chovani, tak aby:
		//   - byly spolecne pro 'okoli',
		//   - a mohl jsem toto chovani dedit
		//   - a dal upravovat chovani pro potreby odvozenych trid
		//
		Auto a1 = new Auto(5, 4);
		Motorka m1 = new Motorka(1);
		assertEquals("Auto=pocetMist:5,pocetDveri:4", a1.toString());
		assertEquals("Motorka=pocetMist:1", m1.toString());
		assertEquals(testChovani("Auto"), testChovani(a1));
		assertEquals(testChovani("Motorka"), testChovani(m1));
		System.out.println("OK: ukol/1");


		// TODO: 2) vhodne zrefaktoruj tridy 'Auto' a 'Motorka', tak ze:
		// TODO: 2.1) najdes 'spolecnou funkcionalitu' (~tj. zobecneni) trid: 'Auto', 'Motorka' tak, abys:
		// TODO: 2.1.1) vytvoril 'abstraktni' tridu 'Vozidlo'
		// TODO: 2.1.2) ze ktere trida 'Auto' zdedi vlastnosti
		// TODO: 2.1.3) ze ktere trida 'Motorka' zdedi vlastnosti
		// TODO: 2.*) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertTrue(Modifier.isAbstract(Vozidlo.class.getModifiers()));
//		assertTrue(Vozidlo.class.isAssignableFrom(Auto.class));
//		assertTrue(Vozidlo.class.isAssignableFrom(Motorka.class));
//		assertNotNull(Vozidlo.class.getDeclaredField("pocetMist"));
//		Auto a2 = new Auto(2, 3);
//		Motorka m2 = new Motorka(2);
//		assertEquals("Auto=pocetMist:2,pocetDveri:3", a2.toString());
//		assertEquals("Motorka=pocetMist:2", m2.toString());
//		System.out.println("OK: ukol/2");


		// TODO: 3) trida 'Auto' bude mit
		// TODO: 3.1) getter 'getPocetDveri', ktery vrati cele cislo 'int'
		// TOOD: 3.2) pozor: 'Motorka' tento getter mit nebude
		// TODO: 3.*) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertNotNull(ReflectionUtils.findMethod(Auto.class, "getPocetDveri").get());
//		assertThrows(NoSuchElementException.class, () ->
//		{
//			ReflectionUtils.findMethod(Motorka.class, "getPocetDveri").get();
//		});
//		System.out.println("OK: ukol/3");

		// TODO: 4) vsechny tridy: 'Auto', 'Motorka', 'Vozidlo' budou mit:
		// TODO: 4.1) vsechny data skryte  (~tj. private)
		// TODO: 4.2) vsechny data nemenne (~tj. final)
		// TODO: 4.*) pokud jsi vse udelal spravne, odkomentuj nasledujici kod a spust!
		//
//		assertTrue(Modifier.isFinal(Vozidlo.class.getDeclaredField("pocetMist").getModifiers()));
//		assertTrue(Modifier.isPrivate(Vozidlo.class.getDeclaredField("pocetMist").getModifiers()));
//		assertTrue(Modifier.isFinal(Auto.class.getDeclaredField("pocetDveri").getModifiers()));
//		assertTrue(Modifier.isPrivate(Auto.class.getDeclaredField("pocetDveri").getModifiers()));
//		System.out.println("OK: ukol/4");
	}

	private static String testChovani(String id)
	{
		StringBuilder sb = new StringBuilder();
		for(String entry : new String[] {"pohybDopredu", "pohybDozadu", "zatocVlevo", "zatocVpravo"})
		{
			sb.append(id + "." + entry + "();\n");
		}
		return sb.toString().replace("\n", "").replace("\r", "");
	}

	private static String testChovani(Auto vozidlo)
	{
		PrintStream prevStream = System.out;
		ByteArrayOutputStream myStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(myStream));

		vozidlo.pohybDopredu();
		vozidlo.pohybDozadu();
		vozidlo.zatocVlevo();
		vozidlo.zatocVpravo();

		System.out.flush();
		System.setOut(prevStream);

		return myStream.toString().replace("\n", "").replace("\r", "");
	}

	private static String testChovani(Motorka vozidlo)
	{
		PrintStream prevStream = System.out;
		ByteArrayOutputStream myStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(myStream));

		vozidlo.pohybDopredu();
		vozidlo.pohybDozadu();
		vozidlo.zatocVlevo();
		vozidlo.zatocVpravo();

		System.out.flush();
		System.setOut(prevStream);

		return myStream.toString().replace("\n", "").replace("\r", "");
	}
}
