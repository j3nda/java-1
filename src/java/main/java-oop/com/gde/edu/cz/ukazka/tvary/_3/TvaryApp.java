package com.gde.edu.cz.ukazka.tvary._3;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.gde.edu.cz.ukazka.tvary._1.primitiva.Bod;

public class TvaryApp
extends ApplicationAdapter
{
	private SpriteBatch batch;
	private Bod bod;

	@Override
	public void create()
	{
		batch = new SpriteBatch(); // vytvori kontejner, ktery vykresluje pomoci OpenGL

		bod = new Bod();
		bod.setX(100);
		bod.setY(200);

		// TODO: tvary#3: ukol.1) pomoci tvary#2 "smajlik" vytvor smajlik, ktery leta po obrazovce
		// TODO: tvary#3: ukol.2.1) v okamziku, kdy uzivatel klikne, smajlik leti nejkratsi cestou na [x,y] pozici, kam klikl
		// TODO: tvary#3: ukol.2.2) kdyz smajlik doleti na [x,y] pozici; nasleduji smer/rychlost se urci nahodne (~kam poleti)
		// TODO: tvary#3: ukol.3) namisto 1x poletujiciho 'smajliku' udelej hada, tj.
		// TODO: tvary#3: ukol.3.1) prvni smajlik bude nejvetsi (~150% normalni velikosti)
		// TODO: tvary#3: ukol.3.2) posledni smajlik bude nejmensi (~15% normalni velikosti)
		// TODO: tvary#3: ukol.3.3) vychozi pocet smajliku (~hada) bude 10x
		// TODO: tvary#3: ukol.3.4) 'hlava-hada' definuje rychlost/smer a 'telo-hada' nasleduje hlavu
		// TODO: tvary#3: ukol.4) smajlik 'hlava-hada' sleduje ocima 'pohyb-kurzoru-mysi' (~kouli oci)
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		// detekce kliknuti mysi
		detectMouseClick();

		// vykresli elementy @see: IRenderable
		bod.draw(batch);
	}

	private final Vector2 lastMouseClick = new Vector2();
	private void detectMouseClick()
	{
		if (
			   Gdx.input.isButtonPressed(Input.Buttons.LEFT)
			&& lastMouseClick.x != Gdx.input.getX()
			&& lastMouseClick.y != Gdx.input.getY()
		   )
		{
			lastMouseClick.x = Gdx.input.getX();
			lastMouseClick.y = Gdx.input.getY();
			onMouseClick(Gdx.input.getX(), Gdx.input.getY());
		}
	}

	/** metoda se zavola v okamziku, kdyz se klikne mysi */
	private void onMouseClick(int screenX, int screenY)
	{
		// TIP: idealni pro doplneni implementace ukolu: 2.1
		System.out.println("onMouseClick(" + screenX + ", " + screenY + ")");
	}

	@Override
	public void dispose()
	{
		batch.dispose();
	}
}
