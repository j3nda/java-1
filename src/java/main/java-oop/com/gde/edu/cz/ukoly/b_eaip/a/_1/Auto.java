package com.gde.edu.cz.ukoly.b_eaip.a._1;

class Auto
implements IVozidlo
{
	private int pocetMist;
	private int pocetDveri;

	Auto(int pocetMist, int pocetDveri)
	{
		this.pocetMist = pocetMist;
		this.pocetDveri = pocetDveri;
	}

	@Override
	public int getPocetMist()
	{
		return pocetMist;
	}

	void pohybDopredu()
	{
		System.out.println("Auto.pohybDopredu();");
	}

	void pohybDozadu()
	{
		System.out.println("Auto.pohybDozadu();");
	}

	void zatocVlevo()
	{
		System.out.println("Auto.zatocVlevo();");
	}

	void zatocVpravo()
	{
		System.out.println("Auto.zatocVpravo();");
	}

	@Override
	public String toString()
	{
		return "Auto="
			+ "pocetMist:" + pocetMist
			+ ",pocetDveri:" + pocetDveri
		;
	}
}
