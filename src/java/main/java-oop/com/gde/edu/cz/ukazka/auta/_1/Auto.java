package com.gde.edu.cz.ukazka.auta._1;

class Auto
extends Object
{
	String vyrobce;
	TypAuta typ;

	@Override
	public String toString()
	{
		return ""
			+ "vyrobce: \"" + vyrobce + "\""
			+ ", typ: \"" + typ + "\""
		;
	}
}
