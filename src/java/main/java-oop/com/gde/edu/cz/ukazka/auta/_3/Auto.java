package com.gde.edu.cz.ukazka.auta._3;

/**
 * "abstract" VYNUCUJE IMPLEMENTACI,
 * tj. mam zajisteno, ze vsechno, co je uvozeno klicovym slovem (abstract), bude obsahovat implementaci!
 *
 * a zaroven si zde mohu dovolit skryt (~tj. private) vnitrni data!
 * a najit 'spolecny jmenovatel' vsech aut (~tj. final) 'vyrobce'!
 */
abstract class Auto
extends Object
implements IAuto
{
	private final String vyrobce;

	protected Auto(String vyrobce)
	{
		this.vyrobce = vyrobce;
	}

	@Override
	public String getVyrobce()
	{
		return vyrobce;
	}

	@Override
	public String toString()
	{
		return ""
			+ "vyrobce: \"" + getVyrobce() + "\""
			+ ", typ: \"" + getTyp() + "\""
		;
	}
}
