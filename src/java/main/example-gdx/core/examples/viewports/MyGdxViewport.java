package examples.viewports;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import space.earlygrey.shapedrawer.ShapeDrawer;

public class MyGdxViewport
extends ApplicationAdapter
{
	private Texture texture;
	private Stage stage1;
	private Stage stage2;


	@Override
	public void create()
	{
		texture = new Texture("badlogic.jpg"); // vytvori texturu ze souboru

		int width = Gdx.graphics.getWidth() / 2;
		int height = Gdx.graphics.getHeight();

		stage1 = new MyStage(new MyFillViewport(width, height, createCamera(width, height)));
		stage2 = new MyStage(new MyFitViewport(width, height, createCamera(width, height)));

		stage1.getViewport().setScreenBounds(0, 0, 100, 100);
		stage1.getViewport().setWorldSize(100, 100);

		stage2.getViewport().setScreenBounds(100, 100, 200, 200);
		stage2.getViewport().setWorldSize(100, 100);

		// TODO: pripravit ukazku, jak pracovat s viewportama[roman]
		// prida 3x obrazky do: stage1(horizontal), stage2(vertical);
		int max = 3;
		for(int i = 0; i < max; i++)
		{
			Image image1 = new Image(texture);
			image1.setOrigin(Align.center);
			image1.setSize(
				(stage1.getViewport().getWorldWidth() / (2 * max)),
				(stage1.getViewport().getWorldHeight() / (2 * max))
			);
			image1.setPosition(
				(width / max) * i,
				height / 2
			);

			stage1.addActor(image1);

			Image image2 = new Image(texture);
			image2.setOrigin(Align.center);
			image2.setSize(64, 64);
			image2.setPosition(
				width / 2,
				(height / max) * i
			);

			stage2.addActor(image2);
		}
	}

	private Camera createCamera(float width, float height)
	{
		OrthographicCamera camera = new OrthographicCamera(width, height);

		camera.translate(width / 2f, height / 2f);
		camera.update();

		return camera;
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(1, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		stage1.getCamera().update();
		stage1.getBatch().setProjectionMatrix(stage1.getCamera().combined);
		stage1.getViewport().apply(true);
		stage1.act();
		stage1.draw();

		stage2.getCamera().update();
		stage2.getBatch().setProjectionMatrix(stage1.getCamera().combined);
		stage2.getViewport().apply(true);
		stage2.act();
		stage2.draw();
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
		stage1.getViewport().update(width, height, true);
		stage2.getViewport().update(width, height, true);
	}

	@Override
	public void dispose()
	{
		stage1.dispose();
		stage2.dispose();
		texture.dispose();
	}

	private class MyStage
	extends Stage
	{
		private final ShapeDrawer drawer;

		public MyStage(Viewport viewport)
		{
			super(viewport);
			drawer = new ShapeDrawer(
				getBatch(),
				new TextureRegion(new Texture("tint/square16x16.png"))
			);
		}

		@Override
		public void draw()
		{
			super.draw();
			getBatch().begin();

			drawer.line(
				0, 0, 100, 100,
				Color.YELLOW
			);

			drawer.line(
				getViewport().getScreenX(),
				getViewport().getScreenY(),
				getViewport().getScreenX() + getViewport().getScreenWidth(),
				getViewport().getScreenY() + getViewport().getScreenHeight(),
				Color.YELLOW
			);
			getBatch().end();
		}
	}

	private class MyFillViewport
	extends FillViewport
	{
		public MyFillViewport(float worldWidth, float worldHeight, Camera camera)
		{
			super(worldWidth, worldHeight, camera);
		}
	}

	private class MyFitViewport
	extends FitViewport
	{
		public MyFitViewport(float worldWidth, float worldHeight, Camera camera)
		{
			super(worldWidth, worldHeight, camera);
		}
	}
}
