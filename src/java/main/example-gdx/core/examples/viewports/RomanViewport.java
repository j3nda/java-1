package examples.viewports;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class RomanViewport
extends ApplicationAdapter
{
	private Stage uiStage;
	private Label text;
	private Table table;

	@Override
	public void create()
	{
		super.create();

		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();

		Camera camera = createCamera(width, height);
		uiStage = new Stage(new ScreenViewport(camera));
		uiStage.getViewport().update((int)width, (int)height);


		table = new Table();
		LabelStyle ls = new LabelStyle(new BitmapFont(), Color.RED);
		text = new Label(			"x12345x",
			new LabelStyle(new BitmapFont(), Color.RED)
		);

		table.add(new Label("col1", ls)).grow();
		table.add(new Label("col2", ls)).grow();
		table.row();
		table.add(new Label("bottom", ls)).colspan(2).grow();
		table.row();

		table.setSize(width, height);
//		table.setSize(100, 100);
//		table.setPosition(uiStage.getViewport().getWorldWidth() - 100, uiStage.getViewport().getWorldHeight() - 100);
		table.debug();
//		table.setFillParent(true);

		uiStage.addActor(table);
	}

	private Camera createCamera(float width, float height)
	{
		OrthographicCamera camera = new OrthographicCamera(width, height);

		camera.translate(width / 2f, height / 2f);
		camera.update();

		return camera;
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		uiStage.getViewport().apply();

		uiStage.act();
		uiStage.draw();
	}

	@Override
	public void dispose()
	{
		uiStage.dispose();
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);

		ScreenViewport s = (ScreenViewport) uiStage.getViewport();

		System.out.println(s.getUnitsPerPixel());
		System.out.println(s.getWorldWidth() + "," + s.getWorldHeight());
		System.out.println("-");

		uiStage.getViewport().update(width, height);

		System.out.println(s.getUnitsPerPixel());
		System.out.println(s.getWorldWidth() + "," + s.getWorldHeight());
		System.out.println();

//		uiStage.get
//		table.setPosition(uiStage.getViewport().getWorldWidth() - 100, uiStage.getViewport().getWorldHeight() - 100);
//		table.pack();
//		table.setPosition(0, 0);
		table.invalidate();
	}
}
