package examples.input;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.gde.common.graphics.ui.SpriteActor;

import game.ExampleColorArray;

public class TouchpadDemo
extends ApplicationAdapter
{
	private SpriteBatch batch;
	private Texture texture;
	private Image image;
	private ExampleColorArray exampleArray;
	private Stage uiStage;
	private Stage gameStage;


	@Override
	public void create()
	{
		float deadzoneRadius = 10;
		float movingGuySpeed = 20;
		Skin skin = new Skin(Gdx.files.internal("skins/c64/uiskin.json"));

		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		Camera camera = createCamera(width, height);
		uiStage = new Stage(new ScalingViewport(Scaling.stretch, width, height, camera));

		Table table = new Table(skin);
		table.setPosition(0, 0);
		table.setSize(200, 200);

		Touchpad touchpad = new Touchpad(deadzoneRadius, skin);

		uiStage.addActor(table);
		table.add(touchpad);

		gameStage = new Stage(new ScalingViewport(Scaling.stretch, width, height, camera));
		final SpriteActor movingGuy = new SpriteActor(new TextureRegion(new Texture("tint/square16x16.png")));
		movingGuy.setSize(128, 128);
		movingGuy.setColor(Color.YELLOW);
		movingGuy.setOrigin(Align.center);
		movingGuy.setPosition(width / 2, height / 2);

		gameStage.addActor(movingGuy);

// -- https://gamedev.stackexchange.com/questions/127733/libgdx-how-to-handle-touchpad-input

// -- https://stackoverflow.com/questions/48896579/horizontal-only-touchpad-in-libgdx

// -- https://stackoverflow.com/questions/35068654/libgdx-touchpad
// -- http://www.java2s.com/example/java-api/com/badlogic/gdx/scenes/scene2d/ui/touchpad/touchpad-2-0.html
// -- https://www.javatips.net/api/com.badlogic.gdx.scenes.scene2d.ui.touchpad


// TODO: kdyz se s tim hybe; je to OKEJ! ale kdyz to drzis na miste ->  negeneruje to changeEvent; a nic to nedela! i kdyz by melo!
		touchpad.addListener(new ChangeListener()
		{
		    @Override
		    public void changed(ChangeEvent event, Actor actor)
		    {
		        // This is run when anything is changed on this actor.
		        float deltaX = ((Touchpad) actor).getKnobPercentX();
		        float deltaY = ((Touchpad) actor).getKnobPercentY();
		        System.out.println(deltaX + ", " + deltaY);
		        movingGuy.setPosition(
		        	movingGuy.getX() + (movingGuySpeed * deltaX),
		        	movingGuy.getY() + (movingGuySpeed * deltaY)
	        	);
		    }
		});
//		touchpad.addListener(new DragListener()
//		{
//			@Override
//			public void drag(InputEvent event, float x, float y, int pointer)
//			{
//				Actor actor = event.getTarget();
//		        // This is run when anything is changed on this actor.
//		        float deltaX = ((Touchpad) actor).getKnobPercentX();
//		        float deltaY = ((Touchpad) actor).getKnobPercentY();
//		        System.out.println(deltaX + ", " + deltaY + " E");
//		        movingGuy.setPosition(
//		        	movingGuy.getX() + (movingGuySpeed * deltaX),
//		        	movingGuy.getY() + (movingGuySpeed * deltaY)
//	        	);
//			}
//		});
//		touchpad.addListener(new EventListener()
//		{
//
//			@Override
//			public boolean handle(Event event)
//			{
//				System.out.println(event);
//				return false;
//			}
//		});
//		{
//			@Override
//			public void drag(InputEvent event, float x, float y, int pointer)
//			{
//				Actor actor = event.getTarget();
//		        // This is run when anything is changed on this actor.
//		        float deltaX = ((Touchpad) actor).getKnobPercentX();
//		        float deltaY = ((Touchpad) actor).getKnobPercentY();
//		        System.out.println(deltaX + ", " + deltaY + " E");
//		        movingGuy.setPosition(
//		        	movingGuy.getX() + (movingGuySpeed * deltaX),
//		        	movingGuy.getY() + (movingGuySpeed * deltaY)
//	        	);
//			}
//		});
//		touchpad.addListener(new InputListener()
//		{
//			@Override
//			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
//			{
////				if (touchpad.isTouched()) { return false; }
//			System.out.println("aaa");
////				return super.touchDown(event, x, y, pointer, button);
////				event.stop();
//				return true;
//			}
//			@Override
//			public void touchDragged(InputEvent event, float x, float y, int pointer)
//			{
//			System.out.println("bbb");
//				super.touchDragged(event, x, y, pointer);
//			}
//			@Override
//			public boolean handle(Event e)
//			{
//				System.out.println(e);
//				boolean state = super.handle(e);
//				if (state)
//				{
//					return state;
//				}
//				if (touchpad.isTouched())
//				{
//					System.out.println("cccc");
//					return true;
//				}
//				return false;
//			}
//		});

		Gdx.input.setInputProcessor(uiStage);

	}

	private Camera createCamera(float width, float height)
	{
		OrthographicCamera camera = new OrthographicCamera(width, height);

		camera.translate(width / 2f, height / 2f);
		camera.update();

		return camera;
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		gameStage.act();
		gameStage.draw();

		uiStage.act();
		uiStage.draw();
	}

	@Override
	public void dispose()
	{
		uiStage.dispose();
	}
}
