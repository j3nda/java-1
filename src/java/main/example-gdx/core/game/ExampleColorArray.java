package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ExampleColorArray
{
	private final Vector2 size;
	private final Color[][] colorTable;
	private final Table renderTable;
	private final Texture texture;


	public ExampleColorArray(int size)
	{
		this(size, null);
	}

	public ExampleColorArray(int size, Texture texture)
	{
		this.size = new Vector2(
			Gdx.app.getGraphics().getWidth(),
			Gdx.app.getGraphics().getHeight()
		);
		this.texture = texture;
		colorTable = createColorTable(size);
		renderTable = createRenderTable(size, colorTable);

//		renderTable.debug();

	}

	public void render(Batch batch)
	{
		batch.begin();
		renderTable.draw(batch, 1.0f);
		batch.end();
		renderTexture(
			batch,
			Gdx.graphics.getWidth() / 2 - 128,
			Gdx.graphics.getHeight()/ 2 - 128
		);
	}

	private void renderTexture(Batch batch, int x, int y)
	{
		if (batch == null || texture == null)
		{
			return;
		}
		batch.begin();
		batch.draw(texture, x, y);
		batch.end();
	}

	private Color[][] createColorTable(int size)
	{
		int rows = (int) (this.size.x / size);
		int cols = (int) (this.size.y / size);

		Color[][] colors = new Color[rows][cols];
		for(int i = 0; i < rows; i++)
		{
			for(int j = 0; j < cols; j++)
			{
				colors[i][j] = createColor(i, j);
			}
		}
		return colors;
	}

	private Color createColor(int row, int col)
	{
		return new Color(
			(float)Math.random(),
			(float)Math.random(),
			(float)Math.random(),
			1.0f
		);
	}

	private Table createRenderTable(int size, Color[][] colorTable)
	{
		int rows = (int) (this.size.x / size);
		int cols = (int) (this.size.y / size);

		Table table = new Table();
		table.setWidth(this.size.x);
		table.setHeight(this.size.y);

		for(int i = 0; i < rows; i++)
		{
			for(int j = 0; j < cols; j++)
			{
				Table bgTable = new Table();
				bgTable.setBackground(
					createBackgroundColor(
						colorTable[i][j]
					)
				);
				table
					.add(bgTable)
					.grow()
				;
			}
			table.row();
		}

		return table;
	}

	private TextureRegionDrawable createBackgroundColor(Color color)
	{
		Pixmap bgPixmap = new Pixmap(1,1, Pixmap.Format.RGB888);
		bgPixmap.setColor(color);
		bgPixmap.fill();
		return new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));
	}
}
