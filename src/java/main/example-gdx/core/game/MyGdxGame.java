package game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

public class MyGdxGame
extends ApplicationAdapter
{
	private SpriteBatch batch;
	private Texture texture;
	private Image image;
	private ExampleColorArray exampleArray;


	@Override
	public void create()
	{
		batch = new SpriteBatch(); // vytvori kontejner, ktery vykresluje pomoci OpenGL

		texture = new Texture("badlogic.jpg"); // vytvori texturu ze souboru

		exampleArray = new ExampleColorArray(256, texture); // vytvori ukazku barevneho pole

		// vytvori obrazek, ktery obsahuje texturu; nastavi pozici; a priradi "vizualni" akce...
		image = new Image(texture);
		image.setPosition(0, 0);
		image.setOrigin(Align.center);
		image.addAction(
			Actions.sequence(
				Actions.moveTo(
					Gdx.graphics.getWidth() - (image.getWidth() * 1.1f),
					Gdx.graphics.getHeight() - (image.getHeight() * 1.1f),
					1.5f,
					Interpolation.bounceOut
				),
				Actions.forever(
					Actions.parallel(
						Actions.sequence(
							Actions.rotateBy(135f, 6f),
							Actions.rotateBy(-90f, 3f)
						),
						Actions.sequence(
							Actions.scaleTo(0.25f, 0.25f, 1.5f, Interpolation.bounceOut),
							Actions.scaleTo(1.25f, 1.25f, 1.5f, Interpolation.bounceOut)
						)
					)
				)
			)
		);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(1, 0, 0, 1); // nastavi barvu pozadi(RGBA), kterou se "vycisti" obrazovka
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // "vycisti" obrazovku nastavenou barvou

		exampleArray.render(batch); // vyrenderuje ukazku barevneho pole

		// vyrenderuje texturu (ulozenou v promenne "img")
		batch.begin();
		batch.draw(texture, 0, 0);
		batch.end();

		// vyrenderuje image, vc. associovanych akci (posun, zvetseni, rotace, aj)
		image.act(Gdx.graphics.getDeltaTime()); // .act(); provadi akce!
		batch.begin();
		image.draw(batch, 1.0f); // .draw(); provadi vykresleni!
		batch.end();
	}

	@Override
	public void dispose()
	{
		batch.dispose();
		texture.dispose();
	}
}
