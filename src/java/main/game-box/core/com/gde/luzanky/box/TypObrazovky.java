package com.gde.luzanky.box;

public enum TypObrazovky
{
	/** menu: hlavni nabidka */
	MENU,
	/** menu: nastaveni hry */
	NASTAVENI_HRY,
	/** debug: editor 'grafiky' boxera */
	DEBUG_EDITOR,
	/** hra */
	HRA,
}
