package com.gde.luzanky.box.menu.nastaveni;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gde.luzanky.box.BoxScreenResources;
import com.gde.luzanky.box.resources.FontEnum;

public class NastaveniHrace
extends Table
{
	private final BoxScreenResources resources;

	public NastaveniHrace(BoxScreenResources resources, String titulek, int fontSizeNormal)
	{
		this.resources = resources;
		createContent(this, titulek, fontSizeNormal);
	}

	private void createContent(Table wrapper, String titulek, int fontSizeNormal)
	{
		LabelStyle style = new LabelStyle(
			resources.getResourcesManager().getFont(
				FontEnum.DEFAULT,
				fontSizeNormal
			),
			Color.GOLD
		);

		Label jmenoHrace = new Label(titulek, style);
		jmenoHrace.setAlignment(Align.center);

		wrapper
			.add(jmenoHrace)
			.growX()
		;
		wrapper.row();

		wrapper
			.add(new VlevoVpravoSelektor(resources))
			.growX()
		;
		wrapper.row();

		wrapper
			.add(new Actor())
			.grow()
		;
		wrapper.row();
	}
}



