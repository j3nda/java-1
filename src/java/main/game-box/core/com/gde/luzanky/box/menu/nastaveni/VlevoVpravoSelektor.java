package com.gde.luzanky.box.menu.nastaveni;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.gde.luzanky.box.BoxScreenResources;
import com.gde.luzanky.box.resources.FontEnum;

public class VlevoVpravoSelektor
extends Table
{
	private final BoxScreenResources resources;
	private Label vlevo;
	private Label vpravo;

	public VlevoVpravoSelektor(BoxScreenResources resources)
	{
		this.resources = resources;
		createContent(this);
	}

	private void createContent(Table wrapper)
	{
// TODO: lode(commitId: bf83a2dd); refactor: IFontsManager <-- common.GdeFontsManager <-- {pexeso, lode}
// TODO: xhonza: toto musi byt v masteru ~ jinak se nehnu...

		LabelStyle style = new LabelStyle(
			resources.getResourcesManager().getFont(
				FontEnum.DEFAULT,
				44
			),
			Color.GOLD
		);
		vlevo = new Label("<", style);
		vpravo = new Label(">", style);

		add(vlevo)
		;
		add(new Actor())
			.growX()
		;
		add(vpravo)
		;
		row();
		debug();
	}
}
