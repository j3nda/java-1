package com.gde.luzanky.box.hra.hraci;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.box.resources.ResourcesManager;

public class ClovekGrafika
extends Image
{
	private final Clovek clovek;

	public ClovekGrafika(
		ResourcesManager resources,
		Clovek clovek
	)
	{
		super(resources.get(CommonResources.Tint.square16x16, Texture.class));
		this.clovek = clovek;
	}
}
