package com.gde.luzanky.box.hra;

import com.gde.luzanky.box.BoxGame;
import com.gde.luzanky.box.SdilenaObrazovka;
import com.gde.luzanky.box.TypObrazovky;
import com.gde.luzanky.box.hra.hraci.Clovek;
import com.gde.luzanky.box.hra.hraci.ClovekGrafika;

public class ObrazovkaHry
extends SdilenaObrazovka
{
	private ClovekGrafika hrac1;
	private Clovek hrac2;

	public ObrazovkaHry(BoxGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	@Override
	public void show()
	{
		super.show();
		hrac1 = new ClovekGrafika(
			resources.getResourcesManager(),
			new Clovek("honza", 1234)
		);
		hrac2 = new Clovek("zahon", 4321);

		getStage().addActor(hrac1);
	}

	@Override
	protected void renderScreen(float delta)
	{
		System.out.println(delta);
		System.out.println(hrac1);
		System.out.println(hrac2);
		getStage().act(delta);
		getStage().draw();
	}

}
