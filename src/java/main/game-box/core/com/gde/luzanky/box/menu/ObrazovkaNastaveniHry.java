package com.gde.luzanky.box.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.luzanky.box.BoxGame;
import com.gde.luzanky.box.SdilenaObrazovka;
import com.gde.luzanky.box.TypObrazovky;
import com.gde.luzanky.box.editor.ObrazovkaEditor;
import com.gde.luzanky.box.hra.ObrazovkaHry;
import com.gde.luzanky.box.menu.nastaveni.NastaveniHrace;
import com.gde.luzanky.box.resources.FontEnum;

/** nastaveni hry (vyber hrace, vlastnosti, protivnik) */
public class ObrazovkaNastaveniHry
extends SdilenaObrazovka
{
	public ObrazovkaNastaveniHry(BoxGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.NASTAVENI_HRY;
	}

	@Override
	public void show()
	{
		super.show();

		int fontSizeNormal = 33;

		Table wrapper = new Table();
		wrapper.setSize(getWidth(), getHeight());
		wrapper
			.add(new NastaveniHrace(resources, "PLAYER", fontSizeNormal))
			.grow()
		;
		wrapper
			.add(new NastaveniHrace(resources, "COMPUTER", fontSizeNormal))
			.grow()
		;
		wrapper.row();

		Label startHry = new Label(
			"ring!",
			new LabelStyle(
				resources.getResourcesManager().getFont(
					FontEnum.DEFAULT,
					fontSizeNormal * 2.5f
				),
				Color.GOLD
			)
		);
		startHry.setAlignment(Align.center);
		startHry.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				System.out.println("click/tap: na napis \"[start-hry]\"...");
				// TODO: HRA: vytvor tridu ObrazovkaHry, ktera dedi SdilenaObrazovka a prepis logiku z DostanesFlakanec (a umisti do: luzanky.box.hra)
				parentGame.setScreen(
					new ObrazovkaHry(parentGame, TypObrazovky.MENU)
				);
			}
		});
		wrapper
			.add(startHry)
			.colspan(2)
			.expandX()
		;
		wrapper.row();

		if (BoxGame.isDebug())
		{
			wrapper
				.add(createEditor("[boxer editor]", fontSizeNormal * 1.5f))
				.colspan(2)
				.expandX()
			;
			wrapper.row();
		}

		getStage().addActor(wrapper);
	}

	private Label createEditor(String text, float fontSize)
	{
		Label editor = new Label(
			text,
			new LabelStyle(
				resources.getResourcesManager().getFont(
					FontEnum.DEFAULT,
					fontSize
				),
				Color.CHARTREUSE
			)
		);
		editor.setAlignment(Align.center);
		editor.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				parentGame.setScreen(
					new ObrazovkaEditor(parentGame, getScreenType())
				);
			}
		});
		return editor;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}
}
