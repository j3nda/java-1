package com.gde.luzanky.box.hra.hraci;

public class Clovek
{
	private int hp;
	public final String jmeno;

	public Clovek(String jmeno, int hp)
	{
		this.jmeno = jmeno;
		this.hp = hp;
	}

	public int getHitpoints()
	{
		return hp;
	}

	public boolean jsiZivy()
	{
		return (hp > 0 ? true : false);
//		return (hp > 0);
//		if (hp > 0) { return true; } else { return false; }
	}

	void dejFlaakanec(int hp, Clovek komu)
	{
		System.out.println(jmeno
			+ " -> flakanec("
			+ (-1 * hp) + ")"
			+ " -> " + komu
		);
		komu.prijmiFlakanec(hp, this);
	}

	private int nextInt(int min, int max)
	{
		return min + (int)(Math.random() * max);
	}

	private boolean prijmiFlakanec(int hp, Clovek odKoho)
	{
		if (nextInt(0, 100) % 3 == 0)
		{
			System.out.println(jmeno + " uhnul!");
			return false;
		}
		this.hp -= hp;
		return true;
	}

	@Override
	public String toString()
	{
		return jmeno
			+ "(" + hp + ")"
			+ ";"
		;
	}
}
