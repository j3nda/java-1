package com.gde.luzanky.box.editor;

import java.util.ArrayList;

import com.gde.common.graphics.vectors.VectorGroupConfiguration;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorGroup;

class VectorGroupManager
{
	private final VectorGroupConfiguration config;
	Create create;
	Rename rename;
	Delete delete;
	Find find;

	VectorGroupManager(VectorGroupConfiguration config)
	{
		this.config = config;
		this.create = new Create(this);
		this.rename = new Rename(this);
		this.delete = new Delete(this);
		this.find = new Find(this);
		validate(config);
	}

	private void validate(VectorGroupConfiguration config)
	{
		if (config.groups == null)
		{
			config.groups = new ArrayList<>();
		}
	}

	void addGroup(VectorGroup group, String parentName)
	{
		group.parentName = (parentName.length() == 0 ? null : parentName);
		group.parentGroup = getParentGroup(parentName);
		config.groups.add(group);
	}

	VectorGroup getParentGroup(String name)
	{
		return null; // TODO
	}

	String[] getGroupNames()
	{
		String[] names = new String[config.groups.size()];
		for (int i = 0; i < names.length; i++)
		{
			VectorGroup group = config.groups.get(i);
			names[i] = group.name;
		}
		return names;
	}

	boolean groupExists(String name)
	{
		for(VectorGroup group : config.groups)
		{
			if (group.name.contains(name))
			{
				return true;
			}
		}
		return false;
	}

	static class Create
	{
		private final VectorGroupManager manager;

		Create(VectorGroupManager manager)
		{
			this.manager = manager;
		}

		VectorGroup group(String name)
		{
			VectorGroup group = new VectorGroup();

			group.name = name;

			return group;
		}
	}

	static class Rename
	{
		private final VectorGroupManager manager;

		Rename(VectorGroupManager manager)
		{
			this.manager = manager;
		}

		void group(String prevName, String newName)
		{
			for(VectorGroup group : manager.config.groups)
			{
				if (group.name.contains(prevName))
				{
					group.name = newName;
				}
			}
		}
	}

	static class Delete
	{
		private final VectorGroupManager manager;

		Delete(VectorGroupManager manager)
		{
			this.manager = manager;
		}

		void group(String name)
		{
			for (int i = 0; i < manager.config.groups.size(); i++)
			{
				VectorGroup group = manager.config.groups.get(i);
				if (group.name.contains(name))
				{
					manager.config.groups.remove(i);
					return;
				}
			}
		}
	}

	static class Find
	{
		private final VectorGroupManager manager;

		Find(VectorGroupManager manager)
		{
			this.manager = manager;
		}

		VectorGroup byName(String name)
		{
			for(VectorGroup group : manager.config.groups)
			{
				if (group.name.contains(name))
				{
					return group;
				}
			}
			return null;
		}

		VectorGroup findByParentName(String name)
		{
			for(VectorGroup group : manager.config.groups)
			{
				if (group.parentName.contains(name))
				{
					return group;
				}
			}
			return null;
		}
	}
}
