package com.gde.luzanky.box;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.ScreenResources;
import com.gde.luzanky.box.resources.ResourcesManager;


public class BoxScreenResources
extends ScreenResources
{
	private final ResourcesManager resources;

	public BoxScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		this.resources = new ResourcesManager();
	}

	public ResourcesManager getResourcesManager()
	{
		return resources;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		resources.dispose();
	}
}
