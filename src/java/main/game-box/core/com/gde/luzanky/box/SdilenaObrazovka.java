package com.gde.luzanky.box;

import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky} a {@link IBoxScreenResources})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, BoxScreenResources, BoxGame>
{
	public SdilenaObrazovka(BoxGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		resources.getResourcesManager().dispose();
	}
}
