package com.gde.luzanky.box.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.box.BoxGame;
import com.gde.luzanky.box.SdilenaObrazovka;
import com.gde.luzanky.box.TypObrazovky;
import com.gde.luzanky.box.resources.FontEnum;

/** hlavni nabidka */
public class ObrazovkaMenu
extends SdilenaObrazovka
{
	public ObrazovkaMenu(BoxGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MENU;
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
	}

	@Override
	public void show()
	{
		super.show();

		// TODO: REFAKTOR: [kolecko] refaktoruj kod tak, aby v metode show() byl kod s "koleckem" uklizeny (idealne do vlastni metody)
		// TODO: GRAFIKA: [kolecko#1] uprav vizual kolecka tak, v case menilo menilo barvu
		// TODO: GRAFIKA: [kolecko#2] uprav vizual kolecka tak, v case animovalo barvu 'duhy' (pouzij: assets/tintColorTextureCircle512x512-outline*.png)
		Image kolecko = new Image(resources.getResourcesManager().get(CommonResources.Tint.circle512x512_full, Texture.class));

		kolecko.setColor(Color.FIREBRICK);
		kolecko.setSize(
			Math.min(getWidth(), getHeight()),
			Math.min(getWidth(), getHeight())
		);
		kolecko.setPosition(
			getWidth() / 2f,
			getHeight()  / 2f,
			Align.center
		);
		kolecko.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				parentGame.setScreen(
					new ObrazovkaNastaveniHry(parentGame, getScreenType())
				);
			}
		});

		// TODO: REFAKTOR: [napis] refaktoruj kod tak, aby v metode show() byl kod s "napisem" uklizeny (idealne do vlastni metody; zamer se na LabelStyle)
		Label napis = new Label(
			"box",
			new LabelStyle(
				resources.getResourcesManager().getFont(
					FontEnum.DEFAULT,
					(int) (kolecko.getHeight() / 2)
				),
				Color.GOLD
			)
		);
		napis.setPosition(
			getWidth() / 2f,
			getHeight()  / 2f,
			Align.center
		);
		napis.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				parentGame.setScreen(
					new ObrazovkaNastaveniHry(parentGame, getScreenType())
				);
			}
		});

		// TODO: MENU: pridej tlacitko 'about' pro vysvetleni hry, autorech aj
		// TODO: MENU: pridej tlacitko 'exit' pro ukonceni hry

		getStage().addActor(kolecko);
		getStage().addActor(napis);
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}
}
