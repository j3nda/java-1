package com.gde.luzanky.box.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;
import com.gde.common.resources.CommonResources;
import com.gde.common.resources.GdeAssetManager;
import com.gde.common.resources.GdeFontsManager;
import com.gde.common.resources.IFontsManager;

public class ResourcesManager
extends GdeAssetManager
{
	private final IFontsManager fontsManager;
	private boolean isDisposed = false;
	private final BitFontMaker2Configuration fontConfiguration;
	private final TextureRegion tintPixel;
	private Texture tintPixelTexture;

	public ResourcesManager()
	{
		fontsManager = new GdeFontsManager();
		fontConfiguration = new BitFontMaker2Configuration(CommonResources.Font.born2bSporty16x16);
		tintPixel = createTintPixel(1);
	}

	private TextureRegion createTintPixel(int size)
	{
		Pixmap pixmap = new Pixmap(size, size, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.drawPixel(0, 0);
		tintPixelTexture = new Texture(pixmap);
		pixmap.dispose();
		return new TextureRegion(tintPixelTexture, 0, 0, 1, 1);
	}

	public TextureRegion getTintPixel()
	{
		return tintPixel;
	}

	public BitmapFont loadFont(FontEnum font, int size)
	{
		return fontsManager.loadFont(font.getResourceId(), size);
	}

	public BitmapFont getFont(FontEnum font, float size)
	{
		return getFont(font, (int)size);
	}

	public BitmapFont getFont(FontEnum font, int size)
	{
		return fontsManager.getFont(font.getResourceId(), size);
	}

	public void disposeFont()
	{
		fontsManager.dispose();
	}

	@Override
	public synchronized void dispose()
	{
		if (isDisposed)
		{
			return;
		}
		isDisposed = true;
		super.dispose();
		disposeFont();
		tintPixelTexture.dispose();
	}

	public BitFontMaker2Configuration getFontConfiguration()
	{
		return fontConfiguration;
	}
}
