package com.gde.luzanky.box.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.gde.common.graphics.vectors.VectorDebug.CrossInfo;
import com.gde.common.graphics.vectors.VectorGroupConfiguration;
import com.gde.common.graphics.vectors.VectorGroupConfigurationTest;
import com.gde.common.graphics.vectors.VectorGroupDrawable;
import com.gde.common.resources.TestResourcesUtils;
import com.gde.luzanky.box.BoxGame;
import com.gde.luzanky.box.SdilenaObrazovka;
import com.gde.luzanky.box.TypObrazovky;
import com.gde.luzanky.box.hra.hraci.Clovek;
import com.gde.luzanky.box.hra.hraci.ClovekGrafika;
import com.gde.luzanky.box.resources.FontEnum;

import space.earlygrey.shapedrawer.ShapeDrawer;

/** editor: boxera (~postavicky) */
public class ObrazovkaEditor
extends SdilenaObrazovka
{
	private ShapeDrawer drawer;
	private EditorConfiguration config;
	private Render render;
	private VectorGroupConfiguration vectorConfig;
	private VectorGroupDrawable vectorDrawable;
	private VectorGroupManager manager;
	private UI ui;

	public ObrazovkaEditor(BoxGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.DEBUG_EDITOR;
	}

	@Override
	public void show()
	{
		super.show();
		drawer = new ShapeDrawer(getBatch(), resources.getResourcesManager().getTintPixel());
		config = new EditorConfiguration(getWidth(), getHeight());
		render = new Render(drawer, config);

//		vectorConfig = new VectorGroupConfiguration();
		vectorConfig = new Json().fromJson(
			VectorGroupConfiguration.class,
			TestResourcesUtils.readFileAsString(VectorGroupConfigurationTest.resource_treeUp2)
		);
		vectorDrawable = new VectorGroupDrawable(
			drawer,
			vectorConfig
		);
		vectorDrawable.setPosition(
			config.pos.centerBottom.x,
			config.pos.centerBottom.y
		);
		manager = new VectorGroupManager(vectorConfig);
		ui = new UI(manager);

		ClovekGrafika hrac1 = new ClovekGrafika(
			resources.getResourcesManager(),
			new Clovek("honza", 1234)
		);

		getStage().addActor(vectorDrawable);
		getStage().addActor(createUiMenuRight(0.25f, new Skin(Gdx.files.internal("skins/c64/uiskin.json"))));
		getStage().addActor(createUiMenuLeft(0.25f, new Skin(Gdx.files.internal("skins/c64/uiskin.json"))));
		getStage().addActor(hrac1);

		ui.left.groupBox(manager.getGroupNames());
	}

	private Table createUiMenuRight(float width, Skin skin)
	{
		Table wrapper = new Table();
		Label addLabel;
		TextField groupName, parentName;

		wrapper
			.add(createLabel("group"))
			.colspan(3)
		;
		wrapper.row();

		wrapper.add(createLabel("name:"));
		wrapper.add(groupName = new TextField("name", skin));
		wrapper.add(addLabel = createLabel("add"));
		wrapper.row();

		wrapper.add(createLabel("parent:"));

		wrapper.add(parentName = new TextField("name", skin)).colspan(2);
		wrapper.row();

		addLabel.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				String name = groupName.getText();
				if (name.length() == 0 || manager.groupExists(name)) { return; }
				manager.addGroup(
					manager.create.group(name),
					parentName.getText()
				);
				ui.left.groupBox(manager.getGroupNames(), name);
			}
		});
		wrapper.setPosition(getWidth() - getWidth() * width, 0);
		wrapper.setSize(getWidth() * width, getHeight());
		if (BoxGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Table createUiMenuLeft(float width, Skin skin)
	{
		Label renameNameLabel, renameParentLabel, delLabel;
		Table wrapper = new Table();

		ui.left.groupBox = new SelectBox<String>(skin);
		ui.left.groupBox.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				ui.left.groupBox(ui.left.groupBox.getSelected());
			}
		});

		wrapper.add(delLabel = createLabel("del"));
		wrapper.add(ui.left.groupBox).colspan(2);
		wrapper.row();

		wrapper.add(createLabel("name:"));
		wrapper.add(ui.left.groupName = new TextField("name", skin));
		wrapper.add(renameNameLabel = createLabel("rename"));
		wrapper.row();

		wrapper.add(createLabel("parent:"));

		wrapper.add(ui.left.parentName = new TextField("name", skin));
		wrapper.add(renameParentLabel = createLabel("rename"));
		wrapper.row();

		renameNameLabel.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				String name = ui.left.groupName.getText();
				String prevName = ui.left.groupBox.getItems().get(ui.left.groupBox.getSelectedIndex());
				if (name.length() == 0 || name.equals(UI.NOT_AVAILABLE) || !manager.groupExists(prevName))
				{
					return;
				}
				manager.rename.group(prevName, name);
				ui.left.groupBox(manager.getGroupNames(), name);
			}
		});
		renameParentLabel.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				String name = ui.left.parentName.getText();
				String prevName = ui.left.groupBox.getItems().get(ui.left.groupBox.getSelectedIndex());
				if (name.length() == 0 || name.equals(UI.NOT_AVAILABLE) || !manager.groupExists(prevName))
				{
					return;
				}
				System.out.println("TODO: rename: parent");//TODO: rename
			}
		});
		delLabel.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				String name = ui.left.groupBox.getSelected();
				if (name.length() == 0 || name.equals(UI.NOT_AVAILABLE) || !manager.groupExists(name))
				{
					return;
				}
				manager.delete.group(name);
				ui.left.groupBox(manager.getGroupNames());
			}
		});

		wrapper.setPosition(0, 0);
		wrapper.setSize(getWidth() * width, getHeight());
		if (BoxGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Label createLabel(String text)
	{
		Label label = new Label(
			text,
			new LabelStyle(
				resources.getResourcesManager().getFont(
					FontEnum.DEFAULT,
					20
				),
				Color.CHARTREUSE
			)
		);
		label.setAlignment(Align.center);

		return label;
	}

	@Override
	protected void beforeRenderScreen(float delta)
	{
		super.beforeRenderScreen(delta);
		render.grid(delta);
		render.origin(delta);
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}

	@Override
	protected void afterRenderScreen(float delta)
	{

	}

	private class UI
	{
		private final VectorGroupManager manager;
		public Left left;
		public Right right;
		public static final String NOT_AVAILABLE = "n/a";
		public UI(VectorGroupManager manager)
		{
			this.manager = manager;
			this.left = new Left(this);
			this.right = new Right(this);
		}
		public class Left
		{
			public SelectBox<String> groupBox;
			public TextField groupName;
			public TextField parentName;
			private final UI ui;

			public Left(UI ui)
			{
				this.ui = ui;
			}

			public void groupBox(String[] items)
			{
				groupBox(items, groupBox.getSelected());
			}

			public void groupBox(String[] items, String selected)
			{
				groupBox.setItems(items);
				boolean exists = false;
				for (int i = 0; i < items.length; i++)
				{
					if (items[i].equals(selected))
					{
						exists = true;
						break;
					}
				}
				if (exists)
				{
					groupBox.setSelected(selected);
					groupBox(selected);
				}
				else
				{
					if (items.length > 0)
					{
						groupBox.setSelected(items[0]);
						groupBox(items[0]);
					}
					else
					{
						groupBox(UI.NOT_AVAILABLE);
					}
				}
			}

			public void groupBox(String item)
			{
				Array<String> items = groupBox.getItems();
				if (items.size == 0 || item.length() == 0 || groupBox.getSelectedIndex() == -1)
				{
					groupName.setText(NOT_AVAILABLE);
					parentName.setText(NOT_AVAILABLE);
					return;
				}
				int index = groupBox.getSelectedIndex();
				if (index > -1)
				{
					String selected = groupBox.getItems().get(index);
					groupName.setText(selected);
					parentName.setText(ui.manager.find.byName(selected).parentName);
				}
			}
		}
		public class Right
		{
			private final UI ui;
			public Right(UI ui)
			{
				this.ui = ui;
			}

		}
	}

	private class Render
	{
		private final ShapeDrawer drawer;
		private final EditorConfiguration config;
		private final Batch batch;

		public Render(ShapeDrawer drawer, EditorConfiguration config)
		{
			this.drawer = drawer;
			this.config = config;
			this.batch = drawer.getBatch();
		}

		public void grid(float delta)
		{
			int width = (int) (config.width - config.pos.center.x);
			int height = (int) (config.height - config.pos.center.y);
			int size = config.grid.size;
			int maxX = (width  / size) + 1;
			int maxY = (height / size) + 1;

			batch.begin();
			for(int i = 0; i < maxX; i++)
			{
				drawer.line(
					config.pos.center.x + (i * size),
					0,
					config.pos.center.x + (i * size),
					config.height,
					config.grid.lineColor,
					config.grid.lineSize
				);
				drawer.line(
					config.pos.center.x - (i * size),
					0,
					config.pos.center.x - (i * size),
					config.height,
					config.grid.lineColor,
					config.grid.lineSize
				);
			}
			for(int i = 0; i < maxY; i++)
			{
				drawer.line(
					0,
					config.pos.center.y + (i * size),
					config.width,
					config.pos.center.y + (i * size),
					config.grid.lineColor,
					config.grid.lineSize
				);
				drawer.line(
					0,
					config.pos.center.y - (i * size),
					config.width,
					config.pos.center.y - (i * size),
					config.grid.lineColor,
					config.grid.lineSize
				);
			}
			cross(delta, config.pos.center);
			batch.end();
		}

		public void origin(float delta)
		{
			batch.begin();
			cross(delta, config.pos.centerBottom);
			batch.end();
		}

		private void cross(float delta, CrossInfo cross)
		{
			boolean isDrawing = batch.isDrawing();
			if (!isDrawing)
			{
				batch.begin();
			}
			drawer.line(
				cross.x,
				cross.y - cross.size,
				cross.x,
				cross.y + cross.size,
				cross.color,
				cross.lineSize
			);
			drawer.line(
				cross.x - cross.size,
				cross.y,
				cross.x + cross.size,
				cross.y,
				cross.color,
				cross.lineSize
			);
			if (!isDrawing)
			{
				batch.end();
			}
		}
	}

	private static class EditorConfiguration
	{
		public final Grid grid;
		public final Pos pos;
		public final Pad pad;
		public final int width;
		public final int height;
		public final Cross cross;


		public EditorConfiguration(int width, int height)
		{
			this.width = width;
			this.height = height;
			this.cross = new Cross();
			this.grid = new Grid(this);
			this.pad = new Pad(this);
			this.pos = new Pos(this);
		}

		public static class Cross
		{
			public final Color color = Color.WHITE;
			public final int size = 4;
			public final int lineSize = 2;
		}

		public static class Grid
		{
			public static int size = 100; // TODO: 1x normal vector!
			public static Color lineColor = Color.GRAY;
			public static int lineSize = 1;

			public Grid(EditorConfiguration config)
			{

			}
		}

		public static class Pos
		{
			public final CrossInfo center;
			public final CrossInfo centerBottom;

			public Pos(EditorConfiguration config)
			{
				center = new CrossInfo(
					config.width  / 2f,
					config.height / 2f,
					config.cross.color,
					config.cross.size,
					config.cross.lineSize
				);


				int centerBottomMaxY = (int) ((config.height - center.y) / config.grid.size);
				centerBottom = new CrossInfo(
					center.x,
					center.y - (centerBottomMaxY * config.grid.size),
					Color.CHARTREUSE,
					config.cross.size * 2,
					config.cross.lineSize
				);
			}
		}

		public static class Pad
		{
			public final float bottom;

			public Pad(EditorConfiguration config)
			{
				bottom = config.height * 0.05f;
			}
		}
	}
}
