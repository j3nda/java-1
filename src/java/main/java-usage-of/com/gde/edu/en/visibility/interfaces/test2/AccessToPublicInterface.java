package com.gde.edu.en.visibility.interfaces.test2;

import com.gde.edu.en.visibility.interfaces.App;

public class AccessToPublicInterface
implements VisibilityInterfaceToPublic
{
	@Override
	public void printVisibility()
	{
		System.out.println("public");
		System.out.println("\t-- " + getClass().getName());
		System.out.println("\t-- visible to All classes");
		System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceToPublic.class.getModifiers()));
		System.out.println();
	}
}
