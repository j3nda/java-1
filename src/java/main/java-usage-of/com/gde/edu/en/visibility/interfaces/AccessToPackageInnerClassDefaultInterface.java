package com.gde.edu.en.visibility.interfaces;

import com.gde.edu.en.visibility.interfaces.App.VisibilityInterfaceInnerClassDefault;

public class AccessToPackageInnerClassDefaultInterface
implements VisibilityInterfaceInnerClassDefault
{
	@Override
	public void printVisibility()
	{
		System.out.println("package protected (inner-class)");
		System.out.println("\t-- " + getClass().getName());
		System.out.println("\t-- visible to the classes with the package");
		System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceInnerClassDefault.class.getModifiers()));
		System.out.println();
	}
}
