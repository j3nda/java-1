package com.gde.edu.en.visibility.interfaces.test1;

public class AccessToPackageInterfaceError
//	implements VisibilityInterfaceToPackage
{
//	@Override
	public void printVisibility()
	{
		System.out.println("package protected (ERROR)");
		System.out.println("\t-- " + getClass().getName());
		System.out.println("\t-- uncomment 2x lines above to see that error!");
		System.out.println(
			"\t-- the problem is, that interface:"
			+ " VisibilityInterfaceToPackage"
			+ " is not in this package(" + getClass().getPackage().getName() + ")"
			+ ", but it is in upper package!"
		);
//		System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceToPackage.class.getModifiers()));
		System.out.println();
	}
}
