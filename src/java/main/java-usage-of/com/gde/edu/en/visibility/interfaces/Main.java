package com.gde.edu.en.visibility.interfaces;

public class Main
{
	public static void main(String[] args)
	{
		System.out.println("@see: https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html");
		System.out.println();

		App app = new App();
		app.printVisibility();
	}
}
