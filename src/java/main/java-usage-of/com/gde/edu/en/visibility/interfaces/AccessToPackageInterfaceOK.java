package com.gde.edu.en.visibility.interfaces;

public class AccessToPackageInterfaceOK
implements VisibilityInterfaceToPackage
{
	@Override
	public void printVisibility()
	{
		System.out.println("package protected");
		System.out.println("\t-- " + getClass().getName());
		System.out.println("\t-- visible to the classes with the package");
		System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceToPackage.class.getModifiers()));
		System.out.println();
	}
}
