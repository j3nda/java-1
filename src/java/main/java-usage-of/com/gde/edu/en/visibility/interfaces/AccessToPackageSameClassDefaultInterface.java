package com.gde.edu.en.visibility.interfaces;

class AccessToPackageSameClassDefaultInterface
implements VisibilityInterfaceSameClassFilenameDefault
{
	@Override
	public void printVisibility()
	{
		System.out.println("package protected (inner-class)");
		System.out.println("\t-- " + getClass().getName());
		System.out.println("\t-- visible to the classes with the package");
		System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceSameClassFilenameDefault.class.getModifiers()));
		System.out.println();
	}
}
