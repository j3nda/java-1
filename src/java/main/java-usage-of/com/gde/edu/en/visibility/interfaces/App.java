package com.gde.edu.en.visibility.interfaces;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

import com.gde.edu.en.visibility.interfaces.test1.AccessToPackageInterfaceError;
import com.gde.edu.en.visibility.interfaces.test2.AccessToPublicInterface;
import com.gde.edu.en.visibility.interfaces.test2.VisibilityInterfaceToPublic;

public class App
implements IApp
{
	private interface VisibilityInterfaceToPrivate
	extends VisibilityInterfaceToPublic
	{

	}

	private class AccessToPrivateInterface
	implements VisibilityInterfaceToPrivate
	{
		@Override
		public void printVisibility()
		{
			System.out.println("private");
			System.out.println("\t-- " + getClass().getName());
			System.out.println("\t-- visible with in the class. It is not accessible outside the class");
			System.out.println("\t-- " + App.getModifiers(VisibilityInterfaceToPrivate.class.getModifiers()));
			System.out.println();
		}
	}

	interface VisibilityInterfaceInnerClassDefault
	extends VisibilityInterfaceToPublic
	{

	}

	private final VisibilityInterfaceToPublic visibilityToPublic;
	private final VisibilityInterfaceToPackage visibilityToPackage;
	private final AccessToPackageInterfaceError visibilityToPackageError;
	private final VisibilityInterfaceToPrivate visibilityToPrivate;
	private final VisibilityInterfaceInnerClassDefault visibilityToInnerDefault;
	private final VisibilityInterfaceSameClassFilenameDefault visibilityToSameClassDefault;


	App()
	{
		visibilityToPublic = new AccessToPublicInterface();
		visibilityToPrivate = new AccessToPrivateInterface();
		visibilityToPackage = new AccessToPackageInterfaceOK();
		visibilityToPackageError = new AccessToPackageInterfaceError();
		visibilityToInnerDefault = new AccessToPackageInnerClassDefaultInterface();
		visibilityToSameClassDefault = new AccessToPackageSameClassDefaultInterface();
	}

	@Override
	public void printVisibility()
	{
		visibilityToPublic.printVisibility();
		visibilityToPackage.printVisibility();
		visibilityToPackageError.printVisibility();
		visibilityToPrivate.printVisibility();
		visibilityToInnerDefault.printVisibility();
		visibilityToSameClassDefault.printVisibility();
	}

	public static Set<String> getModifiers(int modifiers)
	{
		Set<String> visibility = new HashSet<String>();

		if (Modifier.isInterface(modifiers)) { visibility.add("interface"); }
//		if (Modifier.isAbstract(modifiers)) { visibility.add("abstract"); }
//		if (Modifier.isFinal(modifiers)) { visibility.add("final"); }
		if (Modifier.isPrivate(modifiers)) { visibility.add("private"); }
		if (Modifier.isProtected(modifiers)) { visibility.add("protected"); }
		if (Modifier.isPublic(modifiers)) { visibility.add("public"); }

		return visibility;
	}
}

interface VisibilityInterfaceSameClassFilenameDefault
extends VisibilityInterfaceToPublic
{

}
