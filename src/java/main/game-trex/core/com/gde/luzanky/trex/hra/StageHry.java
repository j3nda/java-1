package com.gde.luzanky.trex.hra;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class StageHry
extends Stage
{
	private final TouchDetector touches;
	private final ObrazovkaHry parentScreen;
	private float longTapThreshold;

	public StageHry(Viewport viewport, Batch batch, ObrazovkaHry parentScreen)
	{
		super(viewport, batch);
		this.parentScreen = parentScreen;
		this.touches = new TouchDetector();
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		if (Gdx.input.isTouched())
		{
			if (touches.tapCounter == 0)
			{
				touches.x = Gdx.input.getX();
				touches.y = Gdx.input.getY();
			}
			touches.tapCounter++;
			touches.tapDuration += delta;

			if (touches.tapDuration > longTapThreshold)
			{
				touches.longTapCounter++;
				touches.longTapDuration += delta;

				parentScreen.longTap(touches);
			}
		}
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		if (touches.tapDuration <= longTapThreshold)
		{
			parentScreen.tap(touches);
		}
		else
		{
			touches.longTapIsOver = true;
			parentScreen.longTap(touches);
		}
		touches.x = null;
		touches.y = null;
		touches.tapCounter = 0;
		touches.tapDuration = 0;
		touches.longTapCounter = 0;
		touches.longTapDuration = 0;
		touches.longTapIsOver = false;

		return false;
	}

	void longTapThreshold(float threshold)
	{
		this.longTapThreshold = threshold;
	}

	static class TouchDetector
	{
		public Integer x;
		public Integer y;
		public int tapCounter;
		public float tapDuration;
		public int longTapCounter;
		public float longTapDuration;
		public boolean longTapIsOver;

		@Override
		public String toString()
		{
			return "{"
				+ "x=" + x
				+ ", y=" + y
				+ ", counter=" + tapCounter
				+ ", duration=" + tapDuration
				+ ", longTapIsOver=" + longTapIsOver
				+ ", longTapCounter=" + longTapCounter
				+ ", longTapDuration=" + longTapDuration
				+ "}"
			;
		}
	}
}
