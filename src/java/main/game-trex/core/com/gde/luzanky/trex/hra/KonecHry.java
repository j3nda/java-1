package com.gde.luzanky.trex.hra;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.luzanky.trex.SdilenaObrazovka;
import com.gde.luzanky.trex.TRexGame;
import com.gde.luzanky.trex.TypObrazovky;

public class KonecHry
extends SdilenaObrazovka
{
	protected boolean muzesPokracovat = false;

	public KonecHry(TRexGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	public boolean muzuZacitNovouHru()
	{
		return muzesPokracovat;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.KONEC;
	}

	@Override
	protected void renderScreen(float delta)
	{
		getStage().act(delta);
		getStage().draw();
	}

	@Override
	public void show()
	{
		super.show();

//		Image klik = new Image(new Texture("player/dead-1.png"));
		AnimationActor klik = new AnimationActor(
			0.2f,
			new TextureRegion[] {
				new TextureRegion(new Texture("player/dead-1.png")),
				new TextureRegion(new Texture("player/dead-2.png"))
			},
			PlayMode.LOOP
		);
		klik.setPosition(0, 0);
		klik.setSize(getWidth(), getHeight());
		klik.setColor(Color.PURPLE);
		klik.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				muzesPokracovat = true;
			}
		});

		getStage().addActor(klik);
	}

}
