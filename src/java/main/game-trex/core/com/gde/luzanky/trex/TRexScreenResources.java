package com.gde.luzanky.trex;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.ScreenResources;
import com.gde.common.resources.GdeAssetManager;
import com.gde.common.resources.GdeFontsManager;
import com.gde.common.resources.IFontsManager;

public class TRexScreenResources
extends ScreenResources
{
	private static final String DINO_FONT = "fonts/joystix-monospace.ttf";
	private final IFontsManager fontsManager;
	private final IAssetManager assetmanager;

	public TRexScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		this.fontsManager = new GdeFontsManager();
		this.assetmanager = new GdeAssetManager();
	}

	public IFontsManager getFontsManager()
	{
		return fontsManager;
	}

	public IAssetManager getAssetManager()
	{
		return assetmanager;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		fontsManager.dispose();
		assetmanager.dispose();
	}

	// TODO: XHONZA/resources/refactor: createLabel, etc -> getLabelFactory() {box,dguy}
	public Label createLabel(String name)
	{
		return createLabel(name, 28);
	}

	public Label createLabel(String name, int fontSize)
	{
		return createLabel(name, fontSize, Align.center);
	}

	public Label createLabel(String name, int fontSize, Color color)
	{
		return createLabel(name, fontSize, color, Align.center);
	}

	public Label createLabel(String name, int fontSize, int align)
	{
		return createLabel(name, fontSize, Color.RED, align);
	}

	public Label createLabel(String name, int fontSize, Color color, int align)
	{
		Label label = new Label(
			name,
			fontsManager.getLabelStyle(DINO_FONT, fontSize, color)
		);
		label.setAlignment(align);
		return label;
	}
}
