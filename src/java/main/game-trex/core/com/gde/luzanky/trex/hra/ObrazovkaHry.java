package com.gde.luzanky.trex.hra;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.screens.IScreenResources;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.trex.SdilenaObrazovka;
import com.gde.luzanky.trex.TRexGame;
import com.gde.luzanky.trex.TypObrazovky;
import com.gde.luzanky.trex.hra.StageHry.TouchDetector;

import space.earlygrey.shapedrawer.ShapeDrawer;

public class ObrazovkaHry
extends SdilenaObrazovka
{
	private static final boolean isDebug = true;

	private static final int TREX_IDLE = 0;
	private static final int TREX_RUN = 1;
	private static final int TREX_DUCK = 2;
	private static final int TREX_DEAD = 3;
	private static final int TREX_JUMP = 4;

	private static final float POSUN_LEVEL = 0.5f;

	private static final int PREKAZKA_NIC = 0;
	private static final int PREKAZKA_DOLE = 1;
	private static final int PREKAZKA_NAHORE = 2;
	private static final int PREKAZKA_UPROSTRED = 3;

	private static final int MALY_JEDEN = 0;
	private static final int MALY_DVA = 1;

	private static final float DELKA_DNE = 2f;

	private Actor[] levelGrafika;
	private int[] levelLogika;
	private float levelCas;
	private float levelRychlost = POSUN_LEVEL;
	private AnimationActor[] trexGrafika;
	private int yLinkaZem;
	private int trexLogikaX;
	private int trexLogikaStav;
	private Actor uiJump;
	private Actor uiDuck;
	private ShapeDrawer debugDrawer;
	private Image[] zemGrafika;
	private Label uiSkore;
	private Image[] kaktusGrafika = new Image[2];
	private Image[] mrakGrafika = new Image[3];
	private Label uiHighSkore;
	private int highSkore;
	private float cas;
	int casBarvaIndex = 0;
	Color[] casBarvaPozadi = new Color[] {
		Color.GRAY, Color.YELLOW,     // den
		Color.BROWN, Color.CORAL,


		Color.LIGHT_GRAY, Color.BLACK, // noc
		Color.RED, Color.ROYAL,
	};
	private boolean jeDen = true;

	private float casBarva;

	private StartHry zacniHru;

	private KonecHry konecHry;

	public ObrazovkaHry(TRexGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	@Override
	public void show()
	{
		super.show();

		zacniHru = new StartHry();
		startHry();
	}

	void startHry()
	{
		int sirkaDilku = 88; // 1x dilek je: 88x94 pixelu
		int pocetDilku = Gdx.graphics.getWidth() / sirkaDilku;

		yLinkaZem = 222; // graficka cara levelu, kde je zem

		trexLogikaX = 1; // index levelLogika, tj. kde zobrazim trex-e (pocet sloupcu logiky zleva)
		trexLogikaStav = TREX_IDLE; // stav idle je dokud nekliknu...

		trexGrafika = new AnimationActor[4];
		trexGrafika[TREX_IDLE] = vytvorAnimaci("player/idle", sirkaDilku);
		trexGrafika[TREX_RUN] = vytvorAnimaci("player/run", sirkaDilku);
		trexGrafika[TREX_DUCK] = vytvorAnimaci("player/duck", sirkaDilku);
		trexGrafika[TREX_DEAD] = vytvorAnimaci("player/dead", sirkaDilku);

		for(int i = 0; i < trexGrafika.length; i++)
		{
			trexGrafika[i].setPosition(trexLogikaX * sirkaDilku, yLinkaZem);
		}

		zemGrafika = new Image[pocetDilku];
		levelGrafika = new Actor[pocetDilku];
		levelLogika = new int[pocetDilku];

		for (int i = 0; i < levelGrafika.length; i++)
		{
			levelGrafika[i] = null; //vytvorAnimaci("obstacles/bird", sirkaDilku);
			levelLogika[i] = PREKAZKA_NIC; //RandomUtils.nextInt(0, 3); // 0=nic, 1=dole, 2=nahore, 3=uprostred

			zemGrafika[i] = new Image(new Texture(
				"horizon/ground-"
				+ String.format("%02d", RandomUtils.nextInt(1, 26))
				+ ".png"
			));
		}

		kaktusGrafika[MALY_JEDEN] = new Image(new Texture("obstacles/cactus-small-1.png"));
		kaktusGrafika[MALY_DVA]	= new Image(new Texture("obstacles/cactus-small-2.png"));

		for(int i = 0; i < mrakGrafika.length; i = i + 1)
		{
			mrakGrafika[i] = new Image(new Texture("horizon/cloud.png"));
			updateMrak(mrakGrafika[i]);
		}

		Skin skin = new Skin(Gdx.files.internal("skore/uiskin.json"));

		uiSkore = new Label("0", skin);
		uiSkore.setPosition(
			Gdx.graphics.getWidth() / 2f,
			Gdx.graphics.getHeight() - 100
		);
//		uiSkore.setColor(Color.WHITE);

		uiHighSkore = new Label("HI 0", skin);
		uiHighSkore.setPosition(
			Gdx.graphics.getWidth() - 300,
			Gdx.graphics.getHeight() - 100
		);
		updateHighSkore(highSkore);

		uiDuck = new Actor();
		uiDuck.setSize(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);
		uiDuck.setPosition(0, 0);

		uiJump = new Actor();
		uiJump.setSize(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);
		uiJump.setPosition(Gdx.graphics.getWidth() / 2f, 0);

		StageHry stage = (StageHry) getStage();
		stage.longTapThreshold(0.25f);
		stage.addActor(uiDuck);
		stage.addActor(uiJump);

		trexLogikaStav = TREX_RUN;
		if (isDebug)
		{
			debugDrawer = new ShapeDrawer(getBatch(), new TextureRegion(new Texture("tint/square16x16.png")));
		}

		// TODO: grafika: horizon/ground-{01, 26}, size: 88x24
		// TODO: grafika: horizon/moon-{1, 7}, size: 80x80
		// -- https://cs.wikipedia.org/wiki/M%C4%9Bs%C3%AD%C4%8Dn%C3%AD_f%C3%A1ze // 1=nov,2,3,4,5=uplnek,6,7,8
		// TODO: grafika: horizon/star-{1,2,3}, size: 18x18
		// TODO: grafika: obstacles/bird-{1,2}, size: 88x94
		// TODO: grafika: obstacles/<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< kaktus!
		// TODO: grafika: player/{dead, idle, run}-{1,2}, size: 88x94
		// TODO: grafika: player/duck-{1,2}, size: 118x60

		// TODO: game-skore: "HI 01533 00060" // +1 za kazdy posunLevel
		// TODO: game-skore: pri dosazeni kazde "100"ky ve skore, skore problikne(4s)...
		// TODO: game-skore: hi-score si hra pamatuje!
		// TODO: game-skore: score > hi-score, "hi-score" problikne(4s)

		// TODO: game-play: 0-4s .... neni zadna prekazka
		// TODO: game-play: >=5s .... zacina se kaktusama (1x)
		// TODO: game-play: >=8-10s . pridava se pocet kaktusu (2-3)
		// TODO: game-play: 41s  .... objevuje se ptak (nahore, dole) // POZOR: ptaci jsou i uprostred, ze se musis "duck"

		// TODO: game-play: >=8s .... zrychluje se level (vypada, ze kazdych 8s) ~ kazdych 8s "nahodne" se urci, zda se zrychli anebo ne
		// TODO: game-play: levelRychlost -> po urcite dobe

		// TODO: game-gfx: ve dne: mracky
		// TODO: game-gfx: v noci: mracky (zustavaji) + mesic(1x ~ nahodne + jeho faze) a hvezdicky(2x ~ nahodne)
		// TODO: game-gfx: oboji ma paralax!
	}

	@Override
	protected Stage createStage(IScreenResources resources)
	{
		return new StageHry(
			resources.getViewport(),
			resources.getBatch(),
			this
		);
	}

	void tap(TouchDetector tap)
	{
		System.out.println("TAP: " + tap);

		boolean tapRight = ( tap.x >= Gdx.graphics.getWidth() / 2f ? true : false );
		boolean canJump  = ! trexGrafika[TREX_RUN].hasActions();

		float yJump = trexGrafika[TREX_RUN].getHeight() * 1.25f;
		float yJumpUp = 0.25f;
		float yJumpWait = 0.15f;
		float yJumpDown = 0.5f;

		float yDuckWait = 0.5f;

		if (tapRight && canJump)
		{
			Sound sfx = Gdx.audio.newSound(Gdx.files.internal("sounds/LittleRobotSoundFactory/hit-03__270332.mp3"));
			sfx.play();

			Music m = Gdx.audio.newMusic(Gdx.files.internal("sounds/LittleRobotSoundFactory/jingle-win-01__270319.mp3"));
			m.play();

			trexLogikaStav = TREX_JUMP;
			trexGrafika[TREX_RUN].addAction(
				Actions.sequence(
					Actions.moveBy(0, +yJump, yJumpUp),
					Actions.delay(yJumpWait),
					Actions.moveBy(0, -yJump, yJumpDown),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							Gdx.app.postRunnable(new Runnable()
							{
								@Override
								public void run()
								{
									trexLogikaStav = TREX_RUN;
								}
							});
						}
					})
				)
			);
		}
		else
		if (tapRight == false)
		{
			trexLogikaStav = TREX_DUCK;
			trexGrafika[TREX_DUCK].addAction(
				Actions.sequence(
					Actions.delay(yDuckWait),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							Gdx.app.postRunnable(new Runnable()
							{
								@Override
								public void run()
								{
									trexLogikaStav = TREX_RUN;
								}
							});
						}
					})
				)
			);
		}
	}

	void longTap(TouchDetector tap)
	{
		System.out.println("LONG-TAP: " + tap);

		boolean tapRight = ( tap.x >= Gdx.graphics.getWidth() / 2f ? true : false );
		boolean canJump  = ! trexGrafika[TREX_RUN].hasActions();
		boolean canDuck  = canJump;

		float tapDuration = tap.tapDuration;
		if (tapDuration > 5f)
		{
			tapDuration = 5f;
		}
		float yJumpMax = trexGrafika[TREX_RUN].getHeight() * 3.25f;
		float yJumpMin = trexGrafika[TREX_RUN].getHeight() * 1.25f;
		float yJumpCur = yJumpMax * (tapDuration / 5f);
		if (yJumpCur < yJumpMin)
		{
			yJumpCur = yJumpMin;
		}
		float yJumpUp = 0.25f;
		float yJumpWait = 0.15f;
		float yJumpDown = 0.5f;

		if (tapRight && canJump && tap.longTapIsOver)
		{
			trexLogikaStav = TREX_JUMP;
			trexGrafika[TREX_RUN].addAction(
				Actions.sequence(
					Actions.moveBy(0, +yJumpCur, yJumpUp),
					Actions.delay(yJumpWait),
					Actions.moveBy(0, -yJumpCur, yJumpDown),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							Gdx.app.postRunnable(new Runnable()
							{
								@Override
								public void run()
								{
									trexLogikaStav = TREX_RUN;
								}
							});
						}
					})
				)
			);
		}
		else
		if (tapRight == false && canDuck)
		{
			trexLogikaStav = TREX_DUCK;
			if (tap.longTapIsOver)
			{
				trexLogikaStav = TREX_RUN;
			}
		}
	}

	@Override
	public void render(float delta)
	{
		if (zacniHru == null)
		{
			return;
		}

		if (zacniHru.muzuZacit())
		{
			if (konecHry != null)
			{
				if (konecHry.muzuZacitNovouHru())
				{
					konecHry = null;
					show();
					return;
				}
				konecHry.render(delta);
			}
			else
			{
				super.render(delta);
			}
		}
		else
		{
			zacniHru.render(delta);
		}
	}


	@Override
	protected void renderScreen(float delta)
	{
		if (levelGrafika == null)
		{
			return;
		}
		levelCas += delta;
		if (levelCas >= levelRychlost)
		{
			if (existujeKolize(trexLogikaStav, trexLogikaX + 1))
			{
				konecHry();
				return;
			}
			levelCas = 0;
			posunLevel();
		}
		renderDenNoc(delta);
		renderMrak(delta);
		renderTrex(delta);
		renderLevel(delta);
		renderScreenStage(delta);
		renderUI(delta);
	}

	private void renderDenNoc(float delta)
	{
		cas += delta;
		casBarva += delta;
		if (cas > DELKA_DNE)
		{
			cas = 0;
			jeDen = !jeDen;
		}
		int fazeDneMax = casBarvaPozadi.length / 2;
		if (casBarva > DELKA_DNE / fazeDneMax)
		{
			casBarva = 0;
			casBarvaIndex = casBarvaIndex + 1;
			if (casBarvaIndex >= casBarvaPozadi.length)
			{
				casBarvaIndex = 0;
			}
		}
	}

	@Override
	protected Color getClearColor()
	{
		return casBarvaPozadi[casBarvaIndex];
	}

	private void renderMrak(float delta)
	{
		for (int i = 0; i < mrakGrafika.length; i++)
		{
			mrakGrafika[i].act(delta);

			getBatch().begin();
			mrakGrafika[i].draw(getBatch(), 1f);
			getBatch().end();
		}
	}

	void updateMrak(Image mrak)
	{
		int minX = (Gdx.graphics.getWidth() / 2) + 50;
		int maxX = Gdx.graphics.getWidth() - 100;
		int minY = Gdx.graphics.getHeight() / 2;
		int maxY = Gdx.graphics.getHeight() - 222;
		mrak.setPosition(
			RandomUtils.nextInt(minX, maxX),
			RandomUtils.nextInt(minY, maxY)
		);
		mrak.addAction(Actions.sequence(
			Actions.fadeIn(0.25f),
			Actions.moveBy(-444, 0, 5f),
			Actions.fadeOut(1f),
			Actions.delay(RandomUtils.nextFloat(1f, 5f)),
			Actions.run(new Runnable()
			{
				@Override
				public void run()
				{
					updateMrak(mrak);
				}
			})
		));
	}

	private void updateHighSkore(int skore)
	{
		uiHighSkore.setText("HI " + skore);
	}

	private boolean existujeKolize(int trexStav, int prekazkaIndex)
	{
		if ((trexStav == TREX_RUN || trexStav == TREX_DUCK) && levelLogika[prekazkaIndex] == PREKAZKA_DOLE)
		{
			return true;
		}

		if (trexStav == TREX_JUMP && levelLogika[prekazkaIndex] != PREKAZKA_NIC)
		{
			float trexY = trexGrafika[TREX_RUN].getY();
			float trexVyska = trexGrafika[TREX_RUN].getHeight();
			float ptakY = levelGrafika[prekazkaIndex].getY();
			float ptakVyska = levelGrafika[prekazkaIndex].getHeight();

			if (trexY + trexVyska >= ptakY && trexY <= ptakY + ptakVyska)
			{
				return true;
			}
		}
		return false;
	}

	private void konecHry()
	{
		int skore = Integer.valueOf(uiSkore.getText().toString());
		if (skore > highSkore)
		{
			highSkore = skore;
		}
		updateHighSkore(highSkore);
//		show();

		konecHry = new KonecHry(parentGame, getScreenType());
		konecHry.show();
	}

	private void renderUI(float delta)
	{
		getBatch().begin();
		uiSkore.draw(getBatch(), 1f);
		uiHighSkore.draw(getBatch(), 1f);
		getBatch().end();
	}

	private void renderTrex(float delta)
	{
		Batch batch = getBatch();
		batch.begin();

		if (trexLogikaStav == TREX_DUCK)
		{
			trexGrafika[TREX_DUCK].act(delta);
			trexGrafika[TREX_DUCK].draw(batch, 1f);
		}
		else
		{
			trexGrafika[TREX_RUN].act(delta);
			trexGrafika[TREX_RUN].draw(batch, 1f);
		}

		batch.end();
	}

	private void renderLevel(float delta)
	{
		Batch batch = getBatch();
		batch.begin();

		int prekazkaGrafikaY = 0;
		for (int i = 0; i < levelGrafika.length; i++)
		{
			zemGrafika[i].setPosition(
				i * zemGrafika[i].getWidth(),
				yLinkaZem - zemGrafika[i].getHeight()
			);
			zemGrafika[i].draw(batch, 1f);

			if (levelLogika[i] == PREKAZKA_NIC)
			{
				continue;
			}
			else
			if (levelLogika[i] == PREKAZKA_NAHORE)
			{
				prekazkaGrafikaY = (int) levelGrafika[i].getHeight() * 2;
				levelGrafika[i].setColor(Color.RED);
			}
			else
			if (levelLogika[i] == PREKAZKA_UPROSTRED)
			{
				prekazkaGrafikaY = (int) levelGrafika[i].getHeight();
				levelGrafika[i].setColor(Color.CYAN);
			}
			else
			if (levelLogika[i] == PREKAZKA_DOLE)
			{
				prekazkaGrafikaY = 0;
				levelGrafika[i].setColor(Color.YELLOW);
			}
			levelGrafika[i].setPosition(i * levelGrafika[i].getWidth(), yLinkaZem + prekazkaGrafikaY);
			levelGrafika[i].act(delta);
			levelGrafika[i].draw(batch, 1f);
		}
		batch.end();

		if (isDebug)
		{
			renderLevelDebug();
		}
	}

	private void renderLevelDebug()
	{
		boolean isBatch = getBatch().isDrawing();
		if (!isBatch)
		{
			getBatch().begin();
		}
		int prekazkaGrafikaY = 0;
		for (int i = 0; i < levelLogika.length; i++)
		{
			int pole = levelLogika[i];
			float x = i * zemGrafika[i].getWidth();

			// sloupec (dole, uprostred, nahore)
			debugDrawer.rectangle(
				x,
				yLinkaZem,
				88,
				94 * 3,
				Color.RED
			);

			// zem
			debugDrawer.rectangle(
				x,
				yLinkaZem - 24,
				88,
				24,
				Color.CYAN
			);

			// labels
			Actor debugLabel = getStage().getRoot().findActor("debugCol.index" + i);
			if (debugLabel == null)
			{
				Label label = resources.createLabel("" + i);
				label.setPosition(
					x + (zemGrafika[i].getWidth() / 2f),
					yLinkaZem - 24 - 33,
					Align.center
				);
				label.setName("debugCol.index" + i);
				getStage().addActor(label);
			}
			debugLabel = getStage().getRoot().findActor("debugCol.state" + i);
			if (debugLabel == null)
			{
				Label label = (Label)(debugLabel = resources.createLabel("" + i));
				label.setPosition(
					x + (zemGrafika[i].getWidth() / 2f),
					yLinkaZem + 94 * 3 + 33,
					Align.center
				);
				label.setName("debugCol.state" + i);
				getStage().addActor(label);
			}
			((Label)debugLabel).setText("" + pole);
		}
		if (!isBatch)
		{
			getBatch().end();
		}
	}

	private void posunLevel()
	{
		for (int i = 1; i < levelLogika.length; i++)
		{
			levelLogika[i - 1] = levelLogika[i];
			levelGrafika[i - 1] = levelGrafika[i];
			zemGrafika[i - 1] = zemGrafika[i];
		}
		levelLogika[levelLogika.length - 1] = vytvorPrekazkuNeboNic();

		switch(levelLogika[levelLogika.length - 1])
		{
			case PREKAZKA_DOLE:
			{
				if (RandomUtils.nextInt(1, 10) % 2 == 0)
				{
					levelGrafika[levelGrafika.length - 1] = kaktusGrafika[MALY_JEDEN];
				}
				else
				{
					levelGrafika[levelGrafika.length - 1] = kaktusGrafika[MALY_DVA];
				}
				break;
			}
			case PREKAZKA_NAHORE:
			case PREKAZKA_UPROSTRED:
			{
				levelGrafika[levelGrafika.length - 1] = vytvorAnimaci("obstacles/bird", 88);
				break;
			}
		}

		zemGrafika[zemGrafika.length - 1] = new Image(new Texture(
			"horizon/ground-"
			+ String.format("%02d", RandomUtils.nextInt(1, 26))
			+ ".png"
		));

		int skore = Integer.valueOf(uiSkore.getText().toString());
		skore = skore + 10;
		uiSkore.setText(skore);

		if (isDebug)
		{
			System.out.println(Arrays.toString(levelLogika));
		}
	}

	private int vytvorPrekazkuNeboNic()
	{
		// TODO: vytvor logiku, za jakych podminek generovat prekazku
		boolean muzuMitPrekazku = false;
		int pocetDilku = 4;
		int nemamPrekazku = 0;
		for (int i = 0; i < pocetDilku; i++)
		{
			if (levelLogika[levelLogika.length - 2 - i] == PREKAZKA_NIC)
			{
				nemamPrekazku++;
			}
		}
		if (nemamPrekazku == 4) // TODO: >= 2) ~ chova se podivne...
		{
			muzuMitPrekazku = true;
		}
		if (muzuMitPrekazku)
		{
			return RandomUtils.nextInt(0, 3);
		}
		return PREKAZKA_NIC;
	}

	private AnimationActor vytvorAnimaci(String animacePath, int sirka)
	{
		// TODO: respektuj pocetSnimku (~star-{1,2,3} vs dino-{1,2})
		AnimationActor animace = new AnimationActor(
			RandomUtils.nextFloat(0.25f, 1f),
			new TextureRegion[] {
				new TextureRegion(new Texture(animacePath + "-1.png")),
				new TextureRegion(new Texture(animacePath + "-2.png"))
			},
			PlayMode.LOOP
		);
		animace.setSize(sirka, sirka);
		return animace;
	}
}
