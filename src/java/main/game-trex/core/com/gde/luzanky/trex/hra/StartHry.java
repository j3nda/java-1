package com.gde.luzanky.trex.hra;

public class StartHry
{
	int odpocet = 5;
	private float deltaTotal;

	public boolean muzuZacit()
	{
		return (odpocet == 0);
	}

	public void render(float delta)
	{
		deltaTotal += delta;
		if (deltaTotal > 1)
		{
			odpocet--;
			deltaTotal = 0;
		}
		System.out.println(
			delta
			+ " / " + deltaTotal
			+ " / " + odpocet
		);
	}
}
