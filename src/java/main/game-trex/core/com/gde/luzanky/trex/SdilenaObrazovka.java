package com.gde.luzanky.trex;

import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky} a {@link TRexScreenResources})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, TRexScreenResources, TRexGame>
{
	public SdilenaObrazovka(TRexGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}
}
