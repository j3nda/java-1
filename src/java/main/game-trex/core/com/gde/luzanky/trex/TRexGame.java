package com.gde.luzanky.trex;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.game.GdeGame;
import com.gde.luzanky.trex.hra.ObrazovkaHry;

/**
 * hra: Chrome T-Rex Runner dino game (chrome://dino) clone<br/>
 * cil:<br/><ul>
 * <li>1) procvicit: OOP (konstruktory, objekty, komunikace mezi objekty a jejich chovani)</li>
 * <li>2) pochopit: herni-smycka, chovani elementu vuci sobe, spojeni vseho do hry</li>
 * </ul>
 * tato trida predstavuje zakladni hru<br/><ul>
 * <li>{@link GdeGame#create} vytvari vse potrebne pro hru (~zdroje, tridy aj)</li>
 * <li>{@link GdeGame#render} vykresluje vsechny prvky a obsahuje vyhodnocovani logiky hry</li>
 * </ul>
 */
// klicova slova:
// - public   = rika, ze viditelnost je 'verejna' (vsichni vidi)
// - class    = rika, ze se jedna o 'tridu' (objekt)
// - DGuyGame = pojmenovani 'tridy' (objektu)
//
// - extends  = rika, ze dedim vlasnosti z jine 'tridy' (objektu)
// - GdeGame  = rika, ze ktere tridy tyto vlastnosti dedim
//              <> spicatych zavorek si nevsimej ~ jsou to generika, viz java-2/oop
public class TRexGame
extends GdeGame<TypObrazovky, TRexScreenResources>
{
	/**
	 * true, pokud mam hru v 'debug' rezimu. jinak false
	 * (
	 *  private = viditelnost; soukroma, tj. nikdo mimo me (objekt: {@link TRexGame}) promennou neuvidi
	 *  static  = ulozeni v pameti je prave 1x! (tj. vsechny instance objektu {@link TRexGame} sdili tuto hodnotu)
	 *  final   = nemuzu menit; tj. priradim prave jednou a nemuzu zmenit
	 *  boolean = datovy typ
	 *  isDebug = nazev promenne
	 * )
	 */
	private static final boolean isDebug = true;

	/**
	 * metoda, ktera: vraci true, pokud se jedna o 'debug' rezim. jinak vraci false
	 * (
	 *  public    = viditelnost; verejna, tj. vsichni vidi
	 *  static    = mohu volat {@link TRexGame#isDebug()} odkudkoliv (nevolam: promenna.isDebug() !!!)
	 *  boolean   = datovy typ, ktery funkce vraci
	 *  isDebug() = nazev metody; bez parametru, protoze mam prazdne ()
	 *              mam navratovy typ, tj. funkce
	 * )
	 */
	public static boolean isDebug()
	{
		return isDebug;
	}

	@Override
	protected TRexScreenResources createScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		return new TRexScreenResources(camera, viewport, batch);
	}

	/**
	 * nastavi obrazovku, ve ktere se bude vykonavat hra
	 * (
	 *  protected       = viditelnost; omezena, tj. vidi ji pouze moje 'trida' (objekt) anebo odvozene 'tridy'
	 *  void            = metoda bez navratove hodnoty ~ procedura
	 *  setNextScreen() = nazev metody
	 * )
	 */
	@Override
	protected void setNextScreen()
	{
		// vytvori obrazovku (~screen)...
		// (muzu mit vice obrazovek, napr: konecHry, nabidka, nejvyssiSkore aj)
		// a tady musim zajistit, abych obrazovky spravne prepinal mezi sebou
		SdilenaObrazovka obrazovka = new ObrazovkaHry(this, null);

		// nastavi obrazovku jako nasledujici do ktere se prepnu!
		setScreen(obrazovka);
	}
}
