package com.gde.common.graphics.display;

public interface CountdownDisplayListener
{
	void onUpdateCountdown(int counter, String current);
	void onFinishCountdown();
}
