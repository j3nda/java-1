package com.gde.common.graphics.display;

import com.badlogic.gdx.scenes.scene2d.EventListener;

public interface ICelkoveScore
{
	int getScore();
	void setScore(int score);

	/* from LibGdx */
	boolean addListener(EventListener listener);
	boolean removeListener(EventListener listener);
}
