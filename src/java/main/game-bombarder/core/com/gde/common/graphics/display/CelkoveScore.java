package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.Pools;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class CelkoveScore
extends DisplayRowLetters
implements ICelkoveScore
{
	private int lastScore = -1;
	private int score;
	private int maximalniScore;


	public CelkoveScore(BitFontMaker2Configuration font, Color color)
	{
		this(font, 0, 99999, color);
	}

	public CelkoveScore(
		BitFontMaker2Configuration font,
		int pocatecniBody,
		int maximalniBody,
		Color color
	)
	{
		this(
			font,
			Integer.valueOf(maximalniBody).toString().length(),
			color,
			Color.BLACK
		);
		this.maximalniScore = maximalniBody;
		setScore(pocatecniBody);
	}

	protected CelkoveScore(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor,
		Color bgColor
	)
	{
		super(fontConfiguration, length, fgColor, bgColor);
	}

	@Override
	public int getScore()
	{
		return score;
	}

	@Override
	public void setScore(int score)
	{
		if (score < 0)
		{
			score = 0;
		}
		else
		if (score > maximalniScore)
		{
			score = maximalniScore;
		}
		this.score = score;
		nastavScoreJakoText(this.score);
		if (lastScore != score)
		{
			fireChangeEvent(score, lastScore);
		}
		lastScore = score;
	}

	private void fireChangeEvent(int current, int previous)
	{
		ScoreChangeEvent changeEvent = Pools.obtain(ScoreChangeEvent.class);
		changeEvent.reset();
		changeEvent.setScore(current, previous);
		fire(changeEvent);
		Pools.free(changeEvent);
	}

	protected void nastavScoreJakoText(int score)
	{
		String text = String.format("%0" + getLength() + "d", score);
		for(int i = 0; i < text.length(); i++)
		{
			updateLetter(i, text.charAt(i));
		}
	}

	/**
	 * datova obalka, ktera drzi informace o tzv "ChangeEvent", tj. zmene skore;
	 * ktera je ohandlovana v ramci LibGdx "Evene" eko-systemu
	 */
	public static class ScoreChangeEvent
	extends ChangeEvent
	{
		private int currentScore;
		private int previousScore;
		public int getScore()
		{
			return currentScore;
		}
		public int getPreviousScore()
		{
			return previousScore;
		}
		public void setScore(int current, int previous)
		{
			currentScore = current;
			previousScore = previous;
		}
	}
}
