package com.gde.common.graphics.display;

public interface ICountdownDisplay
extends IDisplayRowLetters
{
	void addListener(CountdownDisplayListener listener);
	void removeListener(CountdownDisplayListener listener);
}
