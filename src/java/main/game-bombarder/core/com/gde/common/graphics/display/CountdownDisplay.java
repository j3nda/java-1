package com.gde.common.graphics.display;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class CountdownDisplay
extends DisplayRowLetters
implements ICountdownDisplay
{
	private final String[] countdown;
	private final List<CountdownDisplayListener> listeners;

	public CountdownDisplay(BitFontMaker2Configuration fontConfiguration, float duration, String[] countdown, Color color)
	{
		super(
			fontConfiguration,
			calculateLength(countdown),
			color,
			null
		);
		this.countdown = countdown;
		this.listeners = new ArrayList<>();
		addAction(createCountdownAction(duration));
	}

	private static int calculateLength(String[] text)
	{
		int max = 0;
		for(String entry : text)
		{
			if (entry.length() > max)
			{
				max = entry.length();
			}
		}
		return max;
	}

	@Override
	public void addListener(CountdownDisplayListener listener)
	{
		listeners.add(listener);
	}

	@Override
	public void removeListener(CountdownDisplayListener listener)
	{
		listeners.remove(listener);
	}

	private Action createCountdownAction(float duration)
	{
		updateCountdown(2);
		Runnable runnable = new Runnable()
		{
			private int counter = -1;
			@Override
			public void run()
			{
				counter++;
				if (counter < countdown.length)
				{
					updateCountdown(counter);
				}
				else
				if (counter == countdown.length)
				{
					finishCountdown();
				}
			}
		};
		return Actions.delay(0f);
//		return Actions.repeat(
//			countdown.length,
//			Actions.sequence(
//				Actions.delay(
//					duration,
//					Actions.run(new Runnable()
//					{
//						@Override
//						public void run()
//						{
//							Gdx.app.postRunnable(runnable);
//						}
//					})
//				)
//			)
//		);
	}

	private void updateCountdown(int counter)
	{
		String current = countdown[counter];
		for(int i = 0; i < getLength(); i++)
		{
			if (i >= current.length())
			{
				break;
			}
			updateLetter(i, current.charAt(i));
		}
		for(CountdownDisplayListener listener : listeners)
		{
			listener.onUpdateCountdown(counter, current);
		}
	}

	private void finishCountdown()
	{
		for(CountdownDisplayListener listener : listeners)
		{
			listener.onFinishCountdown();
		}
	}
}
