package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.gde.luzanky.bombarder.logika.hra.HerniLogickeListenery;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

class LogikaHryHandler
implements ILogikaHryHandler
{
	private final List<HerniLogickeListenery> listenery = new ArrayList<>();

	@Override
	public void addLogicListener(HerniLogickeListenery listener)
	{
		// 1x classa == 1x listener (~nechci resit vice-nasobne listenery; zjednodusene na pochopeni)
		if (!listenery.contains(listener))
		{
			listenery.add(listener);
		}
	}

	@Override
	public void removeLogicListener(HerniLogickeListenery listener)
	{
		listenery.remove(listener);
	}

	void clear()
	{
		listenery.clear();
	}

	@Override
	public void onStartHry(LevelConfiguration levelConfiguration)
	{
		Gdx.app.debug(getClass().getName(), "onStartHry()");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onStartHry(levelConfiguration);
		}
	}

	@Override
	public void onKonecHry(boolean jsemVitez)
	{
		Gdx.app.debug(getClass().getName(), "onKonecHry(" + jsemVitez + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onKonecHry(jsemVitez);
		}
	}

	@Override
	public void onLetadloVleteloDoMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		Gdx.app.debug(getClass().getName(), "onLetadloVleteloDoMesta(" + letadlo + ", " + mesto + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onLetadloVleteloDoMesta(letadlo, mesto);
		}
	}

	@Override
	public void onLetadloVyleteloZMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		Gdx.app.debug(getClass().getName(), "onLetadloVyleteloZMesta(" + letadlo + ", " + mesto + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onLetadloVyleteloZMesta(letadlo, mesto);
		}
	}

	@Override
	public void onLetadloNabouraloDoZeme(ILLetadlo letadlo)
	{
		Gdx.app.debug(getClass().getName(), "onLetadloNabouraloDoZeme(" + letadlo + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onLetadloNabouraloDoZeme(letadlo);
		}
	}

	@Override
	public void onLetadloStrili(ILLeticiProjektil projektil)
	{
		Gdx.app.debug(getClass().getName(), "onLetadloStrili(" + projektil + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onLetadloStrili(projektil);
		}
	}

	@Override
	public void onLetadloZtratiloZivoty(ILLetadlo letadlo, float hitpoints)
	{
		Gdx.app.debug(getClass().getName(), "onLetadloZtratiloZivoty(" + hitpoints + "): zbyva: " + letadlo.getZivoty());
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onLetadloZtratiloZivoty(letadlo, hitpoints);
		}
	}

	@Override
	public void onProjektilVystrelil(ILLeticiProjektil projektil)
	{
		Gdx.app.debug(getClass().getName(), "onProjektilVystrelil(" + projektil + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onProjektilVystrelil(projektil);
		}
	}

	@Override
	public void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil)
	{
		Gdx.app.debug(getClass().getName(), "onProjektilExplodovalNaprazdno(" + projektil + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onProjektilExplodovalNaprazdno(projektil);
		}
	}

	@Override
	public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
	{
		Gdx.app.debug(getClass().getName(), "onProjektilExplodovalDoDomu(" + zniceneBlokyDomu + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onProjektilExplodovalDoDomu(projektil, zniceneBlokyDomu);
		}
	}

	@Override
	public void onProjektilSePohl(ILLeticiProjektil projektil)
	{
		Gdx.app.debug(getClass().getName(), "onProjektilSePohl(" + projektil + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onProjektilSePohl(projektil);
		}
	}

	@Override
	public void onDumJeKompletneZnicen(ILDum dum)
	{
		Gdx.app.debug(getClass().getName(), "onDumJeKompletneZnicen(" + dum + ")");
		for(HerniLogickeListenery listener : listenery)
		{
			listener.onDumJeKompletneZnicen(dum);
		}
	}
}
