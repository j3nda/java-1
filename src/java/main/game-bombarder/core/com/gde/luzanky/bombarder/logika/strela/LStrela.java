package com.gde.luzanky.bombarder.logika.strela;

import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.prvky.LLeticiProjektil;

/**
 * logicka cast strely vpred
 * trida dedi vsechny vlastnosti {@link LLeticiProjektil}
 *
 * a definuje vlastni chovani, tj. leti vpred
 */
public class LStrela
extends LLeticiProjektil
implements ILStrela
{
	private float damage;


	public LStrela(float rychlost, float rozsahSkod, int dostrel)
	{
		super(rychlost, rozsahSkod, dostrel);
		damage = rozsahSkod;
	}

	@Override
	protected boolean pohniSeRychlosti()
	{
		// TODO: ukol/LOGIKA: zarid, aby "strela vpred" letela dopredu
		setLogickaPozice(getX() + 1, getY());
		return true;
	}

	@Override
	public float getRozsahSkod()
	{
		return damage;
	}

	@Override
	public void uberRozsahSkod(float damage, ILBlokDomu blokDomu)
	{
		// TODO ukol/LOGIKA: zarid, aby strela utrpela poskozeni a vybuchla 'kdyz ji dojdou zivoty'
		this.damage -= damage;
		if (this.damage <= 0)
		{
			exploduj(blokDomu);
		}
	}
}
