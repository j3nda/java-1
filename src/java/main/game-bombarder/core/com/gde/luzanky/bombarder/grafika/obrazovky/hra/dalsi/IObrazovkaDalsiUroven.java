package com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi;

import com.gde.common.graphics.screens.IScreen;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

public interface IObrazovkaDalsiUroven
extends IScreen<TypObrazovky>
{
	void setLevelConfiguration(LevelConfiguration levelConfiguration);
}
