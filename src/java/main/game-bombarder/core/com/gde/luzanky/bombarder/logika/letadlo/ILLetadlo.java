package com.gde.luzanky.bombarder.logika.letadlo;

import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

/**
 * interfejs (~rozhranni) pro letadlo
 * dedi vsechny vlastnosti:
 * - {@link ILogickaCast}
 * - {@link ILLeticiProjektil}
 *   (protoze kdyz letadlo narazi, je to vlastne projektil s destrukci)
 */
public interface ILLetadlo
extends ILogickaCast, ILLeticiProjektil
{
	float getZivoty();
	void uberZivoty(float delta);
	/** vrati true, kdyz se vystreleni povede. jinak vraci false */
	boolean vystrel(ILLeticiProjektil projektil);
}
