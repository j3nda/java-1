package com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.luzanky.bombarder.BombarderGame;
import com.gde.luzanky.bombarder.grafika.obrazovky.SdilenaObrazovka;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ObrazovkaHry;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;
import com.gde.luzanky.bombarder.zvuky.TypZvuku;

/** obrazovka pro uplny konec hry */
public class ObrazovkaDalsiUroven
extends SdilenaObrazovka
{
	private MultiLineDisplay dalsiUroven;
	private final Color dalsiUrovenColor;
	private final LevelConfiguration dalsiUrovenConfig;


	public ObrazovkaDalsiUroven(
		BombarderGame parentGame,
		TypObrazovky previousScreenType,
		Color gameOverColor,
		LevelConfiguration predchoziUrovenConfig,
		int celkoveBody,
		float letadloZivoty
	)
	{
		super(parentGame, previousScreenType);
		this.dalsiUrovenColor = gameOverColor;
		this.dalsiUrovenConfig = LevelConfiguration.dalsiUroven(
			celkoveBody,
			letadloZivoty,
			predchoziUrovenConfig
		);
	}

	@Override
	public void show()
	{
		super.show();
		setAlphaBackground(0.3f);

		dalsiUroven = createDalsiUroven(dalsiUrovenColor, dalsiUrovenConfig);
		dalsiUroven.addAction(
			Actions.sequence(
				Actions.fadeIn(1.0f),
				Actions.delay(2.0f),
				Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						dalsiUroven();
					}
				})
			)
		);

		getStage().addActor(dalsiUroven);

		// TODO: pimp/zvuky: muzes prehrat zvuk pro postup do dalsiho levelu
		resources.getSoundsManager().play(TypZvuku.DalsiLevel);
	}

	private MultiLineDisplay createDalsiUroven(Color color, LevelConfiguration levelConfig)
	{
		MultiLineDisplay display = new MultiLineDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			new String[] {
				"LEVEL " + levelConfig.level,
				levelConfig.nazevMesta
			},
			color
		);
		display.setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
			)
		);
		return display;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.DALSI_UROVEN;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderBackground();
		renderScreenStage(delta);
	}

	private void dalsiUroven()
	{
		ObrazovkaHry novaHra = new ObrazovkaHry(parentGame, getScreenType());
		novaHra.setLevelConfiguration(dalsiUrovenConfig);
		parentGame.setScreen(novaHra);
	}

	@Override
	protected void onBackButtonPressed()
	{
		// nic nedelej, back-button zde neni podporovany...
	}
}
