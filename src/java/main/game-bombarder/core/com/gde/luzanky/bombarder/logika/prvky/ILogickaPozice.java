package com.gde.luzanky.bombarder.logika.prvky;

/**
 * interfejs (~rozhranni),
 * ktere definuje chovani pro nastaveni a zmenu pozice logickeho objektu
 * (logicka cast je v celych cislech ~ int! [pro zjednoduseni])
 */
public interface ILogickaPozice
{
	int getX();
	int getY();
	void setLogickaPozice(int x, int y);
}
