package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import com.gde.common.graphics.display.CelkoveScore;
import com.gde.luzanky.bombarder.logika.bomba.LBomba;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.strela.ILStrela;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;
import com.gde.luzanky.bombarder.zvuky.HerniZvukoveListenery;
import com.gde.luzanky.bombarder.zvuky.IZvukyManager;
import com.gde.luzanky.bombarder.zvuky.TypZvuku;

public class ObrazovkaHryZvuky
implements HerniZvukoveListenery
{
	private final IZvukyManager zvukyManager;


	public ObrazovkaHryZvuky(IZvukyManager zvukyManager)
	{
		this.zvukyManager = zvukyManager;
	}

	@Override
	public void onStartHry(LevelConfiguration levelConfiguration)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onKonecHry(boolean jsemVitez)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onCelkoveScoreChanged(CelkoveScore score, int prevScore)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage)
	{
		zvukyManager.play(TypZvuku.Letadlo_explodujeDoDomu_OK);
	}

	@Override
	public void onLetadloStrili(ILLetadlo letadlo, ILLeticiProjektil projektil)
	{
		if (projektil instanceof ILStrela)
		{
			zvukyManager.play(TypZvuku.Letadlo_striliStreluVpred);
		}
		else
		if (projektil instanceof LBomba)
		{
			zvukyManager.play(TypZvuku.Letadlo_hazeBombuDolu);
		}
	}

	@Override
	public void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onDumJeKompletneZnicen(ILDum dum)
	{
		// TODO Auto-generated method stub
	}
}
