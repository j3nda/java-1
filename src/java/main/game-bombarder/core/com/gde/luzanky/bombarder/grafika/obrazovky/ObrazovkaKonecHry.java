package com.gde.luzanky.bombarder.grafika.obrazovky;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.bombarder.BombarderGame;

/** obrazovka pro uplny konec hry */
public class ObrazovkaKonecHry
extends SdilenaObrazovka
{
	private MultiLineDisplay gameOver;
	private final Color gameOverColor;


	public ObrazovkaKonecHry(BombarderGame parentGame, TypObrazovky previousScreenType, Color gameOverColor)
	{
		super(parentGame, previousScreenType);
		this.gameOverColor = gameOverColor;
	}

	@Override
	public void show()
	{
		super.show();
		setAlphaBackground(0.3f);

		gameOver = createGameOver(gameOverColor);

		getStage().clear();
		getStage().addActor(gameOver);
	}

	private MultiLineDisplay createGameOver(Color color)
	{
		MultiLineDisplay display = new MultiLineDisplay(
			// vytvori font pro zobrazeni pismen. jako font se pouzije nize znimeny.
			// jedna se o: .ttf prevedene do .json formatu pomoci:
			// -- https://www.pentacom.jp/pentacom/bitfontmaker2/
			new BitFontMaker2Configuration(CommonResources.Font.born2bSporty16x16),
			new String[] {
				"GAME",
				"OVER",
			},
			color
		);
		display.setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
			)
		);
		return display;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.KONEC_HRY;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderBackground();
		renderScreenStage(delta);
	}
}
