package com.gde.luzanky.bombarder.logika.mesto.dum;

import com.gde.luzanky.bombarder.logika.prvky.LogickaCast;

/**
 * logicka cast 1x patra (~bloku-domu)
 * trida dedi vsechny vlastnosti {@link LogickaCast}
 */
public class LBlokDomu
extends LogickaCast
implements ILBlokDomu
{
	/** konstanta, reprezentujici zniceny blok-domu */
	public static final ILBlokDomu ZNICEN = null;
	/** zbyvajici zivoty, tj. damage ktery musim vynalozit na kompletni zniceni */
	protected float zivoty;
	/** nakumulovany damage, tj. sila, kterou jsem nashromazdil pro zniceni */
	protected float damage;
	protected TypPoskozeni poskozeni;
	/** dum, ke kteremu {@link LBlokDomu} patri (~rodic) */
	protected ILDum dum;


	public LBlokDomu(float zivoty, ILDum dum)
	{
		this.damage = 0;
		this.zivoty = zivoty;
		this.dum = dum;
		this.poskozeni = TypPoskozeni.Vubec;
	}

	@Override
	public float getZivoty()
	{
		return zivoty;
	}

	@Override
	public float getDamage()
	{
		return damage;
	}

	@Override
	public void znicit(float damage)
	{
		// TODO: ukol/LOGIKA: znic blok domu
		if (zivoty < 0)
		{
			// nekonecno zivotu...
			return;
		}

		// kdyz mam 'velky-bada-bum'...
		if (damage >= zivoty)
		{
			this.damage += zivoty;
			zivoty = 0;
		}
		else
		{	// jinak...
			this.damage += damage;
			zivoty -= damage;
		}
	}

	@Override
	public TypPoskozeni getPoskozeni()
	{
		if (zivoty == 0)
		{
			return TypPoskozeni.Kompletne;
		}
		else
		if (zivoty > 0 && damage > 0)
		{
			return TypPoskozeni.Castecne;
		}
		else
		{
			return TypPoskozeni.Vubec;
		}
	}

	@Override
	public void act(float delta)
	{
		// TODO Auto-generated method stub
	}
}
