package com.gde.luzanky.bombarder.logika.bomba;

import com.gde.luzanky.bombarder.logika.prvky.LLeticiProjektil;

/**
 * logicka cast bomby dolu
 * trida dedi vsechny vlastnosti {@link LLeticiProjektil}
 *
 * a definuje vlastni chovani, tj. pada dolu
 */
public class LBomba
extends LLeticiProjektil
{
	public LBomba(float rychlost, float rozsahSkod, int dostrel)
	{
		super(rychlost, rozsahSkod, dostrel);
	}

	@Override
	protected boolean pohniSeRychlosti()
	{
		// TODO: ukol/LOGIKA: zarid, aby "bomba" padala dolu
		setLogickaPozice(getX(), getY() - 1);
		return true;
	}
}
