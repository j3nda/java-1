package com.gde.luzanky.bombarder.grafika.obrazovky;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.gde.common.graphics.screens.ScreenBase;
import com.gde.luzanky.bombarder.BombarderGame;
import com.gde.luzanky.bombarder.BombarderScreenResources;
import com.gde.luzanky.bombarder.fonty.FontsManager;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, BombarderScreenResources, BombarderGame>
{
	protected FontsManager fontsFactory;
	/** alfa pruhlednost (~pro zobrazeni 'tmavsiho' pozadi) */
	protected float alphaBackground = 0f;
	/** alfa pruhlednost (~textura) */
	protected Texture alphaBackgroundTexture;

	public SdilenaObrazovka(BombarderGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	protected void renderBackground()
	{
		super.renderBackground();
		if (alphaBackground > 0.0f && alphaBackgroundTexture != null)
		{
			renderAlphaBackground(getBatch());
		}
	}

	/** vykresleni 'polo-pruhledneho' pozadi */
	protected void renderAlphaBackground(Batch batch)
	{
		Color prevColor = getBatch().getColor().cpy();
		getBatch().setColor(
			prevColor.r,
			prevColor.g,
			prevColor.b,
			alphaBackground
		);
		boolean isBatch = batch.isDrawing();
		if (!isBatch)
		{
			batch.begin();
		}
		batch.draw(alphaBackgroundTexture, 0, 0);
		batch.setColor(prevColor);
		if (!isBatch)
		{
			batch.end();
		}
	}

	/** nastaveni pruhlednosti 'polo-pruhledneho' pozadi */
	protected void setAlphaBackground(float alpha)
	{
		System.out.println(this);
		alphaBackground = alpha;
		if (alpha > 0.0f)
		{
			if (alphaBackgroundTexture == null)
			{
				alphaBackgroundTexture = createAlphaBackgroundTexture(
					getWidth(),
					getHeight()
				);
			}
		}
		else
		{
			if (alpha < 0.0f)
			{
				alphaBackground = 0.0f;
			}
			if (alphaBackgroundTexture != null)
			{
				alphaBackgroundTexture.dispose();
			}
			alphaBackgroundTexture = null;
		}
	}

	/** vytvoreni 'polo-pruhledneho' pozadi jako textury */
	protected Texture createAlphaBackgroundTexture(int width, int height)
	{
		Pixmap pm = new Pixmap(width, height, Pixmap.Format.Alpha);
		pm.setColor(0, 0, 0, 1);
		pm.fillRectangle(0, 0, width, height);

		Texture texture = new Texture(pm);
		pm.dispose();
		return texture;
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
		if (alphaBackgroundTexture != null)
		{
			alphaBackgroundTexture.dispose();
			alphaBackgroundTexture = createAlphaBackgroundTexture(getWidth(), getHeight());
		}
	}

	@Override
	public void dispose()
	{
		super.dispose();
		if (alphaBackgroundTexture != null)
		{
			alphaBackgroundTexture.dispose();
		}
	}
}
