package com.gde.luzanky.bombarder.grafika.hra.mesto.dum;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.luzanky.bombarder.grafika.hra.GrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.IGrafickaCast;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;

/**
 * graficka cast celeho domu
 * trida dedi vsechny vlastnosti {@link GrafickaCast}
 *
 * 1x dum se sklada z nekolika pater (~bloku)
 */
public class GDum
extends GrafickaCast
{
	private final GBlokDomu[] patraDomu;

	public GDum(ILDum logickyDum, Color color)
	{
		super(logickyDum);
		patraDomu = new GBlokDomu[logickyDum.getVyskaDomu()];
		vytvorDum(logickyDum, color);
	}

	private void vytvorDum(ILDum dum, Color color)
	{
		setColor(color);
		for(int i = 0; i < dum.getVyskaDomu(); i++)
		{
			patraDomu[i] = new GBlokDomu(
				dum.getPatraDomu()[i],
				color
			);
		}
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for(Actor blokDomu : patraDomu)
		{
			blokDomu.act(delta);
		}
	}

	@Override
	public void render(Batch batch, float parentAlpha)
	{
		super.render(batch, parentAlpha);
		for(Actor blokDomu : patraDomu)
		{
			blokDomu.draw(batch, parentAlpha);
		}
	}

	@Override
	protected void onGraphicPixelToLogicBlockChanged()
	{
		// dum se vykresluje ze samotnych blokDomu,
		// -> takze nepotrebuju: GrafickaCast.colorPixel
		// -> takze nezavolam metodu predka, kde se toto resi...

		// TODO: xhonza: sprite-texture-actor-mechanism...
	}

	@Override
	public void setGraphicPixelsToLogicBlock(Vector2 graphicPixelsToLogicSize)
	{
		super.setGraphicPixelsToLogicBlock(graphicPixelsToLogicSize);
		for(Actor blokDomu : patraDomu)
		{
			if (blokDomu instanceof IGrafickaCast)
			{
				((IGrafickaCast)blokDomu).setGraphicPixelsToLogicBlock(graphicPixelsToLogicSize);
			}
		}
	}

	@Override
	public Actor findActor(ILogickeUuid logickeUuid)
	{
		Actor actor = super.findActor(logickeUuid);
		if (actor == null)
		{
			for(GBlokDomu blokDomu : patraDomu)
			{
				if ((actor = blokDomu.findActor(logickeUuid)) != null)
				{
					break;
				}
			}
		}
		return actor;
	}
}
