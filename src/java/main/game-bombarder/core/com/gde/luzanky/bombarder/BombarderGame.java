package com.gde.luzanky.bombarder;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.game.GdeGame;
import com.gde.luzanky.bombarder.grafika.obrazovky.SdilenaObrazovka;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.menu.ObrazovkaMainMenu;

/**
 * hra: bombarder<br/>
 * cil:<br/><ul>
 * <li>1) procvicit pole[][] (~a plynule prejit na kolekce (~Collections))</li>
 * <li>2) naucit se: OOP (~<a href="https://cs.wikipedia.org/wiki/Objektov%C4%9B_orientovan%C3%A9_programov%C3%A1n%C3%AD">Objektove Orientovane Programovani</a>)</li>
 * <li>3) pochopit listenery (~<a href="https://cs.wikipedia.org/wiki/Observer_(n%C3%A1vrhov%C3%BD_vzor)">Observer pattern</a>)</li>
 * </ul>
 * tato trida predstavuje zakladni hru<br/><ul>
 * <li>{@link GdeGame#create} vytvari vse potrebne pro hru (~zdroje, tridy aj)</li>
 * <li>{@link GdeGame#render} vykresluje vsechny prvky a obsahuje i volani logiky (~ovlada tak: LOGIKU i GRAFIKU)</li>
 * </ul>
 * cely projekt je rozdelen do:<br/><ul>
 * <li>com.gde.luzanky.bombarder.grafika ~ obsahuje pouze graficke casti (~vizual; textury; efekty)</li>
 * <li>com.gde.luzanky.bombarder.logika  ~ obsahuje pouze logicke casti (~chovani hry; interakce)</li>
 * </ul>
 */
public class BombarderGame
extends GdeGame<TypObrazovky, BombarderScreenResources>
{
	/** true, pokud mam hru v 'debug' rezimu. jinak false */
	private static final boolean isDebug = true;

	/** vraci true, pokud se jedna o 'debug' rezim. jinak vraci false */
	public static boolean isDebug()
	{
		return isDebug;
	}

	@Override
	protected BombarderScreenResources createScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		return new BombarderScreenResources(camera, viewport, batch);
	}

	@Override
	protected void setNextScreen()
	{
		// vytvori obrazovku (~screen)...
		// (muzu mit vice obrazovek, napr: konecHry, nabidka, nejvyssiSkore aj)
		// a tady musim zajistit, abych obrazovky spravne prepinal mezi sebou
		SdilenaObrazovka obrazovka = new ObrazovkaMainMenu(this, null);

		// nastavi obrazovku jako nasledujici do ktere se prepnu!
		setScreen(obrazovka);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		getScreenResources().getSoundsManager().dispose();
	}
}
