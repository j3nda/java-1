package com.gde.luzanky.bombarder.logika.mesto.dum;

import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;

/**
 * interfejs (~rozhranni) logiky celeho domu
 * dedi vsechny vlastnosti {@link ILBlokDomu},
 * protoze 1x dum se zklada z vice pater (~bloku)
 */
public interface ILDum
extends ILBlokDomu
{
	int getVyskaDomu();
	ILBlokDomu[] getPatraDomu();
	/** znicim dum leticim projektilem (~strela, bomba, letadlo, ...) */
	void znicit(ILLeticiProjektil projektil);
}
