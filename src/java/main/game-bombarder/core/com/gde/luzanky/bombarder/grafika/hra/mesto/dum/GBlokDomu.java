package com.gde.luzanky.bombarder.grafika.hra.mesto.dum;

import com.badlogic.gdx.graphics.Color;
import com.gde.luzanky.bombarder.grafika.hra.GrafickaCast;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;

public class GBlokDomu
extends GrafickaCast
{
	public GBlokDomu(ILBlokDomu logickyBlokDomu, Color color)
	{
		super(logickyBlokDomu);
		setColor(color);
	}

	@Override
	protected void onGraphicPixelToLogicBlockChanged()
	{
		super.onGraphicPixelToLogicBlockChanged();
		setSize(
			graphicToLogicSize.x,
			graphicToLogicSize.y
		);
	}
}
