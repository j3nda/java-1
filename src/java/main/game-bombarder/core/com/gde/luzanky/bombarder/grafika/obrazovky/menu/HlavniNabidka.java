package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.display.IDisplayPixelArray.RenderMode;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

class HlavniNabidka
extends MultiLineDisplay
{
	public HlavniNabidka(BitFontMaker2Configuration fontConfiguration, String[] text, Color color)
	{
		super(
			fontConfiguration,
			text,
			createColors(text.length, color),
			new Color(0x11223344),
			RenderMode.Normal
		);
	}

}
