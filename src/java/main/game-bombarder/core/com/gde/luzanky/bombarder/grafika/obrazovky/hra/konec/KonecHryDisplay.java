package com.gde.luzanky.bombarder.grafika.obrazovky.hra.konec;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.common.graphics.display.DisplayActionType;
import com.gde.common.graphics.display.IDisplayLetter;
import com.gde.common.graphics.display.IDisplayRowLetters;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

class KonecHryDisplay
extends MultiLineDisplay
{
	/** budu si pamatovat, zda delam vlnku poprve */
	private boolean prvniVlnka = true;

	public KonecHryDisplay(BitFontMaker2Configuration fontConfiguration, String[] text, Color color)
	{
		this(fontConfiguration, text, new Color[] {color, color});
	}

	public KonecHryDisplay(BitFontMaker2Configuration fontConfiguration, String[] text, Color[] color)
	{
		super(fontConfiguration, text, color);
	}

	void efektVlnka(float delayStart, float rowDuration, float nextIterationDuration)
	{
		int maxRows = rows.length;
		float delay = delayStart;
		for(int i = 0; i < maxRows; i++)
		{
			if (prvniVlnka)
			{
				// cely napis zmensim na minimum
				// a provedu: "act()" at se to provede!
				rows[i].addAction(Actions.scaleTo(0, 0));
				rows[i].act(0);
			}

			// potom udelam efektni zobrazeni napisu
			delay += vytvorEfekt_vlnkaZlevaDoprava(
				rows[i],
				delay,
				rowDuration / rows[i].getLength(),
				nextIterationDuration,
				prvniVlnka
			);
		}
		prvniVlnka = false;
		addAction(
			Actions.sequence(
				Actions.delay(nextIterationDuration),
				new Action()
				{
					@Override
					public boolean act(float delta)
					{
						efektVlnka(delayStart, rowDuration, nextIterationDuration);
						return true;
					}
				}
			)
		);
	}

	/**
	 * effekt:
	 * vezme pismena(zleva -> doprava) a postupne, nad kazdym pismenem(zleva -> doprava) prida akci:
	 * delay -> fadeIn
	 */
	private float vytvorEfekt_vlnkaZlevaDoprava(
		IDisplayRowLetters display, float delayStart, float rowDuration, float nextIterationDuration, boolean isFirstIteration
	)
	{
		float totalDuration = 0;
		float durationDelay = delayStart;

		// projede text: zleva -> doprava
		for(int i = 0; i < display.getLength(); i++)
		{
			IDisplayLetter letter = display.getLetter(i);
			float durationCol = rowDuration / letter.getCols();
			for(int col = 0; col < letter.getCols(); col++)
			{
				if (isFirstIteration)
				{
					letter.addAction(
						Actions.sequence(
							Actions.delay(durationDelay),
							Actions.scaleTo(1.0f, 1.0f, (0.25f + 0.20f)) // soucet duration scaleTo nize
						),
						DisplayActionType.Actor | DisplayActionType.Foreground | DisplayActionType.Cols,
						new int[] {col}
					);
				}
				else
				{
					letter.addAction(
						Actions.sequence(
							Actions.delay(durationDelay),
							Actions.scaleTo(0.2f, 0.2f, 0.25f),
							Actions.scaleTo(1.0f, 1.0f, 0.20f)
						),
						DisplayActionType.Actor | DisplayActionType.Foreground | DisplayActionType.Cols,
						new int[] {col}
					);
				}
				totalDuration += /*durationScale1 + durationScale1 + */durationCol;
				durationDelay += durationCol;
			}
		}

		// vrati celkove trvani cele akce pro 1x radek (~aby nasl. radek mohl nastat potom)
		return totalDuration;
	}
}
