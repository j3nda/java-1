package com.gde.luzanky.bombarder.grafika.hra;

import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

public abstract class GLeticiProjektil
extends GrafickaCast
{
	private final Vector2 movingSmooth;

	public GLeticiProjektil(ILogickaCast logickaCast)
	{
		super(logickaCast);
		movingSmooth = new Vector2(0, 0);
	}

	protected boolean isMovingSmooth()
	{
		return true;
	}

	protected abstract Vector2 getMovingVector();

	@Override
	public void act(float delta)
	{
		super.act(delta);

		// mam se hybat plynule?
		if (isMovingSmooth())
		{
			movingSmooth.set(0,  0);
			ILogickaCast logickyObjekt = getLogika();

			// vrat mi velikost, kolik chybi do dalsiho pohybu v logice <0.0 az 1.0>
			float movingSmoothAmount = logickyObjekt.getRemainingToNextLogicAction();
			if (movingSmoothAmount < 1.0f)
			{
				movingSmooth.set(
					movingSmoothAmount * graphicToLogicSize.x * getMovingVector().x,
					movingSmoothAmount * graphicToLogicSize.y * getMovingVector().y
				);
			}
			setGrafickaPozice(
				movingSmooth.x + (logickyObjekt.getX() * graphicToLogicSize.x),
				movingSmooth.y + (logickyObjekt.getY() * graphicToLogicSize.y)
			);
		}
	}
}
