package com.gde.luzanky.bombarder;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.ScreenResources;
import com.gde.luzanky.bombarder.fonty.FontsManager;
import com.gde.luzanky.bombarder.fonty.IFontsManager;
import com.gde.luzanky.bombarder.zvuky.IZvukyManager;
import com.gde.luzanky.bombarder.zvuky.ZvukyManager;


public class BombarderScreenResources
extends ScreenResources
{
	private final IFontsManager fontsManager;
	private final IZvukyManager soundsManager;


	public BombarderScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		fontsManager = new FontsManager();
		soundsManager = new ZvukyManager();
	}

	public IFontsManager getFontsManager()
	{
		return fontsManager;
	}

	public IZvukyManager getSoundsManager()
	{
		return soundsManager;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		soundsManager.dispose();
	}
}
