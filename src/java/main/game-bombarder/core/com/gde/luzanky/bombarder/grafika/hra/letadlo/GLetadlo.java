package com.gde.luzanky.bombarder.grafika.hra.letadlo;

import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.bombarder.grafika.hra.GLeticiProjektil;
import com.gde.luzanky.bombarder.grafika.hra.GrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.bomba.GBomba;
import com.gde.luzanky.bombarder.grafika.hra.mesto.dum.GDum;
import com.gde.luzanky.bombarder.grafika.hra.strela.GStrela;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.letadlo.LLetadlo;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

/**
 * graficka cast letadla
 * trida dedi vsechny vlastnosti {@link GrafickaCast}
 *
 * a definuje vlastni graficke chovani
 *
 * letadlo umi:
 * - strilet vprad {@link GStrela}
 * - hazet bombu {@link GBomba}
 * - a muze narazit do {@link GDum}
 */
public class GLetadlo
extends GLeticiProjektil
implements IGLetadlo
{
	public GLetadlo(float rychlost, float zivoty)
	{
		super(
			new LLetadlo(rychlost, zivoty)
		);
	}

	protected GLetadlo(ILogickaCast logickaCast)
	{
		super(logickaCast);
	}

	/** nastavim, kam se pohybuju */
	@Override
	protected Vector2 getMovingVector()
	{
		// vpravo o 1x policko
		return new Vector2(1, 0);
	}

	@Override
	public boolean vystrel(GLeticiProjektil projektil)
	{
		return ((ILLetadlo)getLogika()).vystrel(
			(ILLeticiProjektil) projektil.getLogika()
		);
	}
}
