package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.display.CelkoveScore;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.bombarder.BombarderScreenResources;
import com.gde.luzanky.bombarder.grafika.hra.bomba.GBomba;
import com.gde.luzanky.bombarder.grafika.hra.letadlo.GLetadlo;
import com.gde.luzanky.bombarder.grafika.hra.mesto.GMesto;
import com.gde.luzanky.bombarder.grafika.hra.strela.GStrela;
import com.gde.luzanky.bombarder.grafika.prvky.CelkoveZivoty;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

public class HerniPrvkyTovarna
implements IHerniPrvkyTovarna
{
	protected LevelConfiguration levelConfiguration;
	protected BombarderScreenResources resources;
	protected ILogikaHryHandler logikaHandler;


	protected HerniPrvkyTovarna(BombarderScreenResources resources)
	{
		this.resources = resources;
	}

	@Override
	public void setLevelConfiguration(LevelConfiguration levelConfiguration)
	{
		this.levelConfiguration = levelConfiguration;
	}

	@Override
	public void setLogikaHry(ILogikaHryHandler logikaHandler)
	{
		this.logikaHandler = logikaHandler;
	}

	@Override
	public GLetadlo createLetadlo()
	{
		GLetadlo letadlo = new GLetadlo(
			levelConfiguration.rychlostLetadla,
			levelConfiguration.pocetZivotuLetadla
		);
		letadlo.getLogika().setLogikaHry(logikaHandler);
		return letadlo;
	}

	@Override
	public GMesto createMesto()
	{
		GMesto mesto = new GMesto(
			levelConfiguration.maximalniPocetPaterDomu,
			levelConfiguration.maximalniPocetDomu
		);
		mesto.getLogika().setLogikaHry(logikaHandler);
		return mesto;
	}

	@Override
	public CelkoveScore createCelkoveBody(int startovniBody, Color barva)
	{
		return new CelkoveScore(
			resources.getFontsManager().getBitFontConfiguration(),
			startovniBody,
			99999,
			barva
		);
	}

	@Override
	public CelkoveZivoty createCelkoveZivoty(float zivoty, Color prazdneZivoty, Color plneZivoty)
	{
		return new CelkoveZivoty(
			(int)zivoty,
			CommonResources.Tint.square16x16_9patch,
			prazdneZivoty,
			plneZivoty
		);
	}

	@Override
	public GStrela createStrela(GLetadlo letadlo)
	{
		GStrela strela = new GStrela(
			5.0f,
			150 //999
		);
		strela.getLogika().setLogikaHry(logikaHandler);
		return strela;
	}

	@Override
	public GBomba createBomba(GLetadlo letadlo)
	{
		GBomba bomba = new GBomba(
			5.0f,
			999,
			999
		);
		bomba.getLogika().setLogikaHry(logikaHandler);
		return bomba;
	}
}
