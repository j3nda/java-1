package com.gde.luzanky.bombarder.zvuky;

import com.badlogic.gdx.audio.Sound;

public interface IZvukyManager
{
	void play(TypZvuku zvuk);
	Sound getSound(TypZvuku zvuk);
	void dispose();
	void dispose(TypZvuku... zvuk);
}
