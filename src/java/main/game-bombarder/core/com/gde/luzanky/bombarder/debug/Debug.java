package com.gde.luzanky.bombarder.debug;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.bombarder.BombarderGame;

public class Debug
{
	private static boolean isDebug()
	{
		return BombarderGame.isDebug();
	}

	public static void dispose()
	{
		Font.dispose();
	}

	public static class Font
	{
		private static final Map<String, LabelStyle> labelStyles = new HashMap<>();
		public static LabelStyle createLabelStyle(int size, Color color)
		{
			LabelStyle style = labelStyles.get(size + "." + color);
			if (Debug.isDebug() && style == null)
			{
				labelStyles.put(
					size + "." + color,
					(style = createFont(size, color, CommonResources.Font.vt323))
				);
			}
			return style;
		}

		private static LabelStyle createFont(int size, Color color, String filename)
		{
		    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
		    	Gdx.files.internal(filename)
			);
		    FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		    parameter.size = size;
//		    parameter.borderWidth = 1;
		    parameter.color = Color.YELLOW;
//		    parameter.shadowOffsetX = 3;
//		    parameter.shadowOffsetY = 3;
//		    parameter.shadowColor = new Color(0, 0.5f, 0, 0.75f);
		    BitmapFont bitmapFont = generator.generateFont(parameter);
		    generator.dispose();

		    LabelStyle labelStyle = new LabelStyle();
		    labelStyle.font = bitmapFont;

		    return labelStyle;
		}

		private static void dispose()
		{

		}
	}
}
