package com.gde.luzanky.bombarder.grafika.hra;

/**
 * interfejs (~rozhranni),
 * ktere definuje chovani pro nastaveni a zmenu pozice grafickeho objektu
 * (graficka cast je v realnych cislech ~ float! [pro plynulost])
 */
public interface IGrafickaPozice
{
	void setGrafickaPozice(float x, float y);

	/* from LibGdx */
	float getX();
	float getY();
}
