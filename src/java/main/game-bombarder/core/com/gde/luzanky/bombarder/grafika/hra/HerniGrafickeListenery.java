package com.gde.luzanky.bombarder.grafika.hra;

import com.gde.common.graphics.display.CelkoveScore;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

/** obsahuji seznam udalosti, ktere "stinuji" herni logiku a vizualne ji interpretuji */
public interface HerniGrafickeListenery
{
	void onStartHry(LevelConfiguration levelConfiguration);
	void onKonecHry(boolean jsemVitez);
	void onCelkoveScoreChanged(CelkoveScore score, int prevScore);
	void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage);
	void onLetadloStrili(ILLetadlo letadlo, ILLeticiProjektil projektil);
	void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil);
	void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu);
	void onDumJeKompletneZnicen(ILDum dum);
}
