package com.gde.luzanky.bombarder.logika.hra;

import com.gde.common.graphics.display.ICelkoveScore;
import com.gde.luzanky.bombarder.grafika.hra.HerniGrafickeListenery;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;
import com.gde.luzanky.bombarder.logika.hra.LogikaScore.ScoreTyp;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;
import com.gde.luzanky.bombarder.zvuky.HerniZvukoveListenery;

/**
 * trida, ktera obstarava samotnou logiku cele hry,
 * vc. vyhodnocovani, strileni, reseni kolizi a nasledku, ktere z toho plynou
 */
public class LogikaHry
extends HerniLogickeListeneryAdapter
implements ILogikaHry
{
	private final HerniGrafickeListenery grafika;
	private final HerniZvukoveListenery zvuky;
	private final ICelkoveScore body;
	private final ILMesto mesto;
	private final ILLetadlo letadlo;
	private final LevelConfiguration levelConfiguration;
	private final LogikaScore logikaScore;
	private final ILogikaHryHandler logikaHandler;


	public LogikaHry(
		HerniGrafickeListenery grafika,
		HerniZvukoveListenery zvuky,
		ICelkoveScore body,
		ILMesto mesto,
		ILLetadlo letadlo,
		LevelConfiguration levelConfiguration,
		ILogikaHryHandler logikaHandler
	)
	{
		this.grafika = grafika;
		this.zvuky = zvuky;
		this.body = body;
		this.mesto = mesto;
		this.letadlo = letadlo;
		this.levelConfiguration = levelConfiguration;
		this.logikaScore = new LogikaScore(levelConfiguration);
		this.logikaHandler = logikaHandler;

		// zaregistruju se (~LogikaHry) jako 'posluchac'!
		logikaHandler.addLogicListener(this);
	}

	@Override
	public ILogikaHryHandler getHandler()
	{
		return logikaHandler;
	}

	@Override
	public void onLetadloVleteloDoMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		// tento kod se spusti, kdyz:
		// - letadlo vleti do mesta
		//   (muzu tady osetrit dusledek chovani letadla x mesta; napr:
		//    - reload munice (strely, bomby)
		//    - pokud je letadlo prilis nizko - a nenabouralo - udelat nejaky gfx/sfx
		// )
	}

	@Override
	public void onLetadloVyleteloZMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		// tento kod se spusti, kdyz:
		// - letadlo vyleteli z mesta
		//   (muzu tady osetrit dusledek chovani letadla x mesta; napr:
		//    - pridat nasobic bodu (~multiplier), kdyz letadlo nenabouralo
		//    - anebo pridat energii/zivoty, kdyz letadlo nenabouralo
		// )
		// TODO: ukol/LOGIKA: presun letadlo doleva o 1x radek nize!
		letadlo.setLogickaPozice(0, letadlo.getY() - 1);

		// TODO: ukol/HRA: (a)docasny nasobic bodu, pokud letadlo nenabouralo;
		//       - v okamziku, kdy naboura - nasobic se vynuluje;
		//       - pokud i nadale nenaboura - nasobic se muze zvysovat (maximalne vsak 3x)

		// TODO: ukol/HRA: (b)pridej energii/zivoty letadu, pokud nenabouralo;
	}

	@Override
	public void onLetadloNabouraloDoZeme(ILLetadlo letadlo)
	{
		// tento kod se spusti, kdyz:
		// - letadlo naboura do zeme,
		//   - pokud ma letadlo hodne zivotu; muzes je odecist podle typu letadla a nastavit na zacatek
		//
		// TODO: ukol/HRA: podle typu letadla odecti pocet zivotu
		letadlo.uberZivoty(letadlo.getZivoty());

		// TODO: ukol/HRA: pokud ma letadlo zivoty, nastav jej na zacatek a uber body
		if (letadlo.getZivoty() > 0)
		{
			body.setScore(
				body.getScore() + logikaScore.getScore(ScoreTyp.LetadloNabouraloDoZeme)
			);
			letadlo.setLogickaPozice(
				0,
				mesto.getMaximalniPocetPaterDomu() - 1
			);
		}
	}

	@Override
	public void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage)
	{
		// tento kod se spusti, kdyz:
		// - letadlo ztratilo zivoty

		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		grafika.onLetadloZtratiloZivoty(letadlo, damage);

		// TODO: ukol/HRA: mel bys ponizit skore, kdyz letadlo ztrati zivoty...
		// TODO: pimp/logika: u skore bys mel zohlednit 'damage' poskozeni
		body.setScore(
			body.getScore() + logikaScore.getScore(ScoreTyp.LetadloZtratiloZivoty)
		);

		// TODO: pimp/zvuku: muzes prehrat zvuk, ktery zvyrazni, ze letadlo prislo o zivoty
		// TODO: ukol/HRA: kdyz nema letadlo zivoty -> nastava konec hry
		if (letadlo.getZivoty() <= 0)
		{
			konecHry(false);
		}
		else
		{
			zvuky.onLetadloZtratiloZivoty(letadlo, damage);
		}
	}

	@Override
	public void onLetadloStrili(ILLeticiProjektil projektil)
	{
		// tento kod se spusti, kdyz:
		// - letadlo vystreli strelu
		//
		// TODO: ukol/HRA: podle typu vystreleneho projektilu pripocti body
		// TODO: pimp/logika: mel bys zohlednit typ projektilu (~popr: jak narocne jej vystrelit 'cooldown', a toto reflektovat ve score)
		// TODO: pimp/grafika: vizualizuj 'praaaask!' (~zes prave vystrelil)
		// TODO: pimp/zvuky: prehraj zvuk 'praaaask!' (~zes prave vystrelil)
		body.setScore(
			body.getScore() + logikaScore.getScore(ScoreTyp.LetadloStrili)
		);
		grafika.onLetadloStrili(letadlo, projektil);
		zvuky.onLetadloStrili(letadlo, projektil);
	}

	@Override
	public void onProjektilSePohl(ILLeticiProjektil projektil)
	{
		if (projektil instanceof ILLetadlo)
		{
			onLetadloSePohlo((ILLetadlo) projektil);
		}
	}

	private void onLetadloSePohlo(ILLetadlo letadlo)
	{
		// tento kod se spusti, kdyz:
		// - se letadlo pohne
		//   (idealni na "kumulaci" efektu nad letadlem...)
		//
	}

	@Override
	public void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil)
	{
		// tento kod se spusti, kdyz:
		// - letici projektil (strela nebo bomba) exploduje naprazdno, tj. nezasahne cil
		// - letici projektil muze byt i letadlo. kdyz exploduje mel by nastat konec hry

		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		grafika.onProjektilExplodovalNaprazdno(projektil);

		// TODO: ukol/HRA: pokud je projektil letadlo => zobraz konec-hry => jako porazeny!
		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		if (projektil instanceof ILLetadlo)
		{
			konecHry(false);
		}

		// TODO: ukol/HRA: mel bys snizit body, kdyz strela nebo bomba exploduje naprazdno (~je to promarnena sance)
		body.setScore(
			body.getScore() + logikaScore.getScore(ScoreTyp.ProjektilExplodovalNaprazdno)
		);
	}

	@Override
	public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
	{
		// tento kod se spusti, kdyz:
		// - letici projektil (strela nebo bomba) narazi do domu, tj. zasahne cil!
		// - letici projektil muze byt i letadlo...
		//
		// TODO: ukol/HRA: strela nebo bomba/zasahne cil -> mel bych zvysit skore
		// TODO: pimp/logika: mel bys zohlednit 'damage' znicenych bloku
		if (!(projektil instanceof ILLetadlo))
		{
			body.setScore(
				body.getScore() + logikaScore.getScore(ScoreTyp.ProjektilExplodovalDoDomu)
			);
		}

		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		grafika.onProjektilExplodovalDoDomu(projektil, zniceneBlokyDomu);

		// TODO: ukol/HRA: letadlo naboura do domu -> mel bych letadlu ubrat zivoty, podle poskozeni domu
		if (projektil instanceof ILLetadlo && zniceneBlokyDomu.length > 0)
		{
			// a letadlu snizim zivoty, podle damage nad znicenymi bloky-domu...
			float damage = 0;
			for(ILBlokDomu blokDomu : zniceneBlokyDomu)
			{
				damage += blokDomu.getDamage();
			}
			letadlo.uberZivoty(damage);
		}
	}

	@Override
	public void onDumJeKompletneZnicen(ILDum dum)
	{
		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		grafika.onDumJeKompletneZnicen(dum);

		// TODO: ukol/HRA: zajisti, ze po zniceni vsech domu nastane konec hry
		// TODO: ukol/HRA: informuj grafiku o logicke udalosti...
		if (mesto.getAktualniPocetDomu() == 0)
		{
			konecHry(true);
		}
	}

	@Override
	public void startHry()
	{
		// nastavim pocatecni pozici letadla
		letadlo.setLogickaPozice(
			0,
			levelConfiguration.maximalniPocetPaterDomu - 1
		);
		body.setScore(0);
		logikaHandler.onStartHry(levelConfiguration);
		grafika.onStartHry(levelConfiguration);
	}

	private void konecHry(boolean jsemVitez)
	{
		logikaHandler.onKonecHry(jsemVitez);
		grafika.onKonecHry(jsemVitez);
	}
}
