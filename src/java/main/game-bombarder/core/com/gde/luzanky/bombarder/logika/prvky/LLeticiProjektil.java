package com.gde.luzanky.bombarder.logika.prvky;

import java.util.ArrayList;
import java.util.List;

import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.LBlokDomu;

/**
 * letici projektil, tj. neco co leti a jakmile narazi, tak zpusobi skodu.
 * typicky: strela vpred anebo bomba dolu.
 *
 * projektil dedi vsechny vlastnosti tridy {@link LogickaCast}
 * a doplnuje o: "rychlost" a "rozsahSkod"
 *
 * - diky pretizeni metod: act() a onPositionChanged() muzeme ovladat logiku projektilu,
 *   tj. co se stane kdyz "xxx" (~napr: narazi do budovy)
 *
 *
 * slovicko "abstract" zde vynucuje, ze nekdo musi tento objekt zdedit,
 * tj. sdilet vsechny vlastnosti zde uvedene!
 */
public abstract class LLeticiProjektil
extends LogickaCast
implements ILLeticiProjektil
{
	private final float pohniSeRychlostiLimit;
	private float rozsahSkod;
	private float rychlost;
	private float pohniSeRychlosti;
	private int dostrel;
	/** rodic: kdo me vystrelil? (muze byt null, pokud nema rodice!) */
	protected ILogickaCast kdoMeVystrelil;


	public LLeticiProjektil(float rychlost, float rozsahSkod, int dostrel)
	{
		this.rychlost = rychlost;
		this.rozsahSkod = rozsahSkod;
		this.dostrel = dostrel;
		this.pohniSeRychlosti = 0;
		this.pohniSeRychlostiLimit = 1.0f / rychlost;
	}

	@Override
	public float getRozsahSkod()
	{
		return rozsahSkod;
	}

	@Override
	public float getRychlost()
	{
		return rychlost;
	}

	@Override
	public int getDostrel()
	{
		return dostrel;
	}

	@Override
	public ILogickaCast getKdoStrilel()
	{
		return kdoMeVystrelil;
	}

	@Override
	public void act(float delta)
	{
		if (rychlost == 0)
		{
			return;
		}
		pohniSeRychlosti += delta * rychlost;
		if (pohniSeRychlosti >= pohniSeRychlostiLimit)
		{
			pohniSeRychlosti = 0;
			if (pohniSeRychlosti() && getDostrel() >= 0)
			{
				dostrel--;
				if (dostrel == 0)
				{
					exploduj();
				}
			}
		}
	}

	@Override
	public float getRemainingToNextLogicAction()
	{
		return pohniSeRychlosti / pohniSeRychlostiLimit;
	}

	/**
	 * metoda pohniSeRychlosti() se zavola v okamziku, kdy se projektil ma pohnout na dalsi logicke policko
	 * vraci true, pokud se pohyb podari, jinak vraci false.
	 */
	protected abstract boolean pohniSeRychlosti();

	@Override
	public boolean vystrel(ILogickaPozice pozice)
	{
		return vystrel(pozice, null);
	}

	@Override
	public boolean vystrel(ILogickaPozice pozice, ILogickaCast kdoStrili)
	{
		// TODO: ukol/LOGIKA: nastav spravou pozici vystrelene strely; vcetne toho, kdo strili
		// TODO: ukol/LOGIKA: o vystreleni bys mel informovat vsechny posluchace (~listenery)
		setLogickaPozice(
			pozice.getX(),
			pozice.getY()
		);
		kdoMeVystrelil = kdoStrili;
		handler.onProjektilVystrelil(this);
		return true;
	}

	@Override
	public boolean exploduj(ILogickaCast... logickaKolize)
	{
		// TODO: ukol/LOGIKA: kdyz exploduje, uz by dal nemel letet...
		rychlost = 0;

		// TODO: ukol/LOGIKA: o explozi bys mel informovat vsechny posluchace (~listenery)
		// uvazuj varianty:
		// a/ explodoval naprazdno
		// b/ explodoval do domu (~vc. predani spravnych vstupu)
		if (logickaKolize.length == 0 || getRozsahSkod() == 0)
		{
			handler.onProjektilExplodovalNaprazdno(this);
		}
		else
		{
			List<ILBlokDomu> blokyDomu = new ArrayList<>(); // kolekce (~neznam presny pocet)
			for(ILogickaCast kolize : logickaKolize)
			{
				if (kolize instanceof ILBlokDomu)
				{
					// pokud se jedna o blok-domu; pridam jej do kolekce
					blokyDomu.add((ILBlokDomu)kolize);
				}
			}
			// informuju vsechny 'posluchace' o kolizi do domu; a predam jednotlive bloky-domu
			// jako LBlokDomu[] pole! (~abych nemusel slozite resit nested-iterator; popr. kopirovat koleci znovu a znovu)
			handler.onProjektilExplodovalDoDomu(
				this,
				blokyDomu.toArray(new LBlokDomu[blokyDomu.size()])
			);
		}
		return true;
	}

	@Override
	protected void onPositionChanged()
	{
		super.onPositionChanged();
		handler.onProjektilSePohl(this);
	}
}
