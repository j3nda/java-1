package com.gde.luzanky.bombarder.logika.hra;

import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;

public interface ILogikaHry
extends HerniLogickeListenery
{
	void startHry();
	ILogikaHryHandler getHandler();
}
