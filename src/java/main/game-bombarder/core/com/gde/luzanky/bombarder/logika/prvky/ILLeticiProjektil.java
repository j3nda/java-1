package com.gde.luzanky.bombarder.logika.prvky;

/** interfejs (~rozhranni) pro letici projektil */
public interface ILLeticiProjektil
extends ILogickaPozice, ILogickeUuid
{
	float getRozsahSkod();
	float getRychlost();
	int getDostrel();
	ILogickaCast getKdoStrilel();
	/** vrati true, kdyz se vystreleni povede. jinak vraci false */
	boolean vystrel(ILogickaPozice pozice);
	/** vrati true, kdyz se vystreleni povede. jinak vraci false */
	boolean vystrel(ILogickaPozice pozice, ILogickaCast kdoStrili);
	/**
	 * vrati true, kdyz exploduje. jinak vraci false.
	 * @param kolize pole objektu, se kterymi dojde ke kolizi v pripade exploze
	 */
	boolean exploduj(ILogickaCast... kolize);
}
