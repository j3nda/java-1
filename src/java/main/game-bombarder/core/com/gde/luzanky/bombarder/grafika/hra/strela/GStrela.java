package com.gde.luzanky.bombarder.grafika.hra.strela;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.bombarder.grafika.hra.GLeticiProjektil;
import com.gde.luzanky.bombarder.logika.strela.LStrela;

/**
 * graficka cast strely vpred
 * trida dedi vsechny vlastnosti {@link GLeticiProjektil}
 *
 * a definuje vlastni graficke chovani
 */
public class GStrela
extends GLeticiProjektil
{
	public GStrela(float rychlost, float rozsahSkod)
	{
		super(
			new LStrela(rychlost, rozsahSkod, 999)
		);
		setColor(Color.YELLOW);
	}

	@Override
	protected Vector2 getMovingVector()
	{
		// vpravo o 1x policko
		return new Vector2(+1, 0);
	}
}
