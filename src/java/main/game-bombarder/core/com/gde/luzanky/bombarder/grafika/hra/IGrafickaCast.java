package com.gde.luzanky.bombarder.grafika.hra;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;

/**
 * interfejs (~rozhranni),
 * ktere definuje chovani graficke casti
 *
 * obdobne jako logika {@link ILogickaCast#act} ma svou metodu pro ovladani co se deje,
 * tak grafika ma take "act()", protoze grafika potrebuje animovat efekty aj.
 */
public interface IGrafickaCast
extends IGrafickaPozice
{
	/** vraci logickou cast objektu */
	ILogickaCast getLogika();

	/** uvolnuje zdroje z pameti (~napr: alokovane textury, fonty, aj.) */
	void dispose();

	/** nastavi velikost 1x logicke kosticky na 1x grafickou kosticku (~prepocet: logika[int] => grafika[pixely]) */
	void setGraphicPixelsToLogicBlock(Vector2 graphicPixelsToLogicSize);

	/**
	 * najde grafickaCast podle logickeUuid
	 * (~uzitecne, kdyz graficky objekt shlukuje anebo kombinuje vice objektu do sebe)
	 */
	Actor findActor(ILogickeUuid logickeUuid);

	/* from LibGdx */
	void act(float delta);
	void draw(Batch batch, float parentAlpha);
	boolean remove();
	void setColor(Color color);
	String getName();
}
