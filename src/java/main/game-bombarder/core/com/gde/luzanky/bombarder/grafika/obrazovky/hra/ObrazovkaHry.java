package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import com.gde.common.graphics.display.CelkoveScore;
import com.gde.common.graphics.display.CelkoveScore.ScoreChangeEvent;
import com.gde.common.graphics.effects.ShakeEffect;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.bombarder.BombarderGame;
import com.gde.luzanky.bombarder.grafika.hra.GrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.HerniGrafickeListenery;
import com.gde.luzanky.bombarder.grafika.hra.IGrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.bomba.GBomba;
import com.gde.luzanky.bombarder.grafika.hra.letadlo.GLetadlo;
import com.gde.luzanky.bombarder.grafika.hra.mesto.GMesto;
import com.gde.luzanky.bombarder.grafika.hra.strela.GStrela;
import com.gde.luzanky.bombarder.grafika.obrazovky.SdilenaObrazovka;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi.IObrazovkaDalsiUroven;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi.ObrazovkaDalsiUroven;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.konec.ObrazovkaKonecHry;
import com.gde.luzanky.bombarder.grafika.prvky.CelkoveZivoty;
import com.gde.luzanky.bombarder.logika.hra.ILogikaHry;
import com.gde.luzanky.bombarder.logika.hra.LogikaHry;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;
import com.gde.luzanky.bombarder.logika.prvky.LogickaCast;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;
import com.gde.luzanky.bombarder.zvuky.HerniZvukoveListenery;
import com.gde.luzanky.bombarder.zvuky.TypZvuku;

/** herni obrazovka, zde se odehrava nase bombarder hra! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements IObrazovkaHry
{
	/** barva pro zvyrazneni, ze letadlu dochazi zivoty (~cervena) */
	private static final Color ZIVOTY_BARVA_PRAZDNE = new Color(0xCC0000ff);
	/** barva pro zvyrazneni, ze letadlo ma dostatek zivotu (~zelena) */
	private static final Color ZIVOTY_BARVA_PLNE = new Color(0x009900ff);
	/** barva pro zobrazeni 'CelkoveScore' */
	protected static final Color CELKOVE_BODY_BARVA = Color.GRAY;
	protected GMesto mesto;
	protected GLetadlo letadlo;
	/** sdruzuje/zaobaluje logiku cele hry */
	private ILogikaHry logikaHry;
	/** efekt zatreseni obrazovkou */
	private ShakeEffect shakeEffect;
	private ParticleEffect pe;
	/** celkove zivoty, jako reflexe, kdyz letadlo narazi... */
	private CelkoveZivoty zivoty;
	/** odpocet, ze hra zacne za 3...2...1 */
	private Stage startHryStage;
	/** UI: pro zivoty a nektere veci, ktere nesouvisi s 'shakeEffect'! */
	protected Stage uiStage;
	/** UI: musi mit svou kameru, aby jej neovlivnoval 'shakeEffect'! */
	protected OrthographicCamera uiCamera;
	/** nastaveni levelu, ktery vytvari mesto */
	protected LevelConfiguration levelConfiguration;
	/** tovarnicka, ktera vytvari 'vsechny' herni prvky, vc. inicializace aj. */
	private IHerniPrvkyTovarna tovarnicka;
	/** nastal konec hry? */
	private boolean isKonecHry;
	/** UI ovladani: tap vlevo (~leva cast obrazovky): shazuje bombu */
	protected Actor uiTapVlevoBomba;
	/** UI ovladani: tap vpravo (~prava cast obrazovky): strili bombu */
	protected Actor uiTapVpravoStrela;


	public ObrazovkaHry(BombarderGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public void show()
	{
		super.show();
		uiStage = new Stage(
			getStage().getViewport()
		);
		uiCamera = new OrthographicCamera(
			getStage().getViewport().getScreenWidth(),
			getStage().getViewport().getScreenHeight()
		);
		startHryStage = new Stage(
			getStage().getViewport()
		);
//		createParticleEffects(); // je tam buga - nechce to nacist ten particle file!

		startHry(levelConfiguration.pocatecniBody);
	}

	private void createParticleEffects()
	{
//		ParticleEffectParameter pep = new ParticleEffectParameter();
//		pep.atlasFile
//		manager.load
//
//		AssetManager
		pe = new ParticleEffect();
		pe.load(
			Gdx.files.internal("8bit-bum.pe"),
			Gdx.files.internal(CommonResources.Tint.square16x16)
		);
		pe.getEmitters().first().setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		pe.start();
	}

	/**
	 * vytvori 'tovarnicku' na vytvareni hernich (~grafickych) prvku
	 * (dulezite: abych mohl menit graficky vzhled jednotlivych urovni)
	 */
	protected IHerniPrvkyTovarna createHerniPrvkyTovarnicka()
	{
		return new HerniPrvkyTovarna(resources);
	}

	/** vytvori UI hernich ovladacich prvku */
	protected boolean createHerniUI()
	{
		uiTapVlevoBomba = new Actor();
		uiTapVlevoBomba.setPosition(0, 0);
		uiTapVlevoBomba.setSize(getWidth() / 2f, getHeight());
		uiTapVlevoBomba.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				onVystrelBombu();
			}
		});

		uiTapVpravoStrela = new Actor();
		uiTapVpravoStrela.setPosition(getWidth() / 2f, 0);
		uiTapVpravoStrela.setSize(getWidth() / 2f, getHeight());
		uiTapVpravoStrela.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				onVystrelStrelu();
			}
		});
		return true;
	}

	/**
	 * vytvori display pro zobrazeni bodu.
	 * vse je zalozeno na {@link DisplayPixelArray} ktery znas ze hry hang-man
	 */
	private CelkoveScore createCelkoveBody(int startovniBody)
	{
		float lineHeight = getHeight() / 8f;

		CelkoveScore body = tovarnicka.createCelkoveBody(
			startovniBody,
			CELKOVE_BODY_BARVA
		);
		body.setName("celkoveBody");
		body.setViewport(
			new Rectangle(
				0,
				Gdx.graphics.getHeight() - (2 * lineHeight),
				Gdx.graphics.getWidth(),
				lineHeight
			)
		);

		// zaregistruju listener; kdyz se skore zmeni -> provolam: onCelkoveScoreChanged();
		// abych mohl delat graficke efekty...
		body.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				if (event instanceof ScoreChangeEvent)
				{
					onCelkoveScoreChanged(
						(CelkoveScore)actor,
						((ScoreChangeEvent)event).getPreviousScore()
					);
				}
			}
		});
		return body;
	}

	/** vytvoreni 'ProgressBar' pro zobrazeni celkovych zivotu */
	private CelkoveZivoty createCelkoveZivoty(GLetadlo letadlo)
	{
		CelkoveZivoty zivoty = tovarnicka.createCelkoveZivoty(
			((ILLetadlo)letadlo.getLogika()).getZivoty(),
			ZIVOTY_BARVA_PRAZDNE,
			ZIVOTY_BARVA_PLNE
		);
		zivoty.setWidth(Gdx.graphics.getWidth());
		zivoty.setVisible(
			// zivoty nezobrazim, kdyz je mam plne...
			zivoty.getPercentage() == 1.0f ? false : true
		);
		return zivoty;
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
		uiStage.getViewport().setScreenSize(width, height);
		uiStage.getViewport().update(width, height, true);
		startHryStage.getViewport().setScreenSize(width, height);
		startHryStage.getViewport().update(width, height, true);
		updateGraphicPixelsToLogicBlock();
	}

	/** aktualizace mapovani: 1x logicky-blok na 1x graficky-blok */
	protected void updateGraphicPixelsToLogicBlock()
	{
		// nemuzu pocitat logicke bloky, kdyz nemam mesto! ...ukoncuju prepocet.
		if (this.mesto == null)
		{
			return;
		}

		// musim nastavit mapovani: 1x logicky blok na 1x graficky, tj.
		// provolat vsechny prvky, ktere implementuji: IGrafickaCast
		// a informovat je o velikosti 1x grafickeho bloku
		Vector2 graphicPixelsToLogicSize = new Vector2(
			getWidth()  / (float)((ILMesto)mesto.getLogika()).getMaximalniPocetDomu(),
			getHeight() / (float)((ILMesto)mesto.getLogika()).getMaximalniPocetPaterDomu()
		);

		// pouzivam 2x stage; abych je prosel, vytvorim novou kolekci, kde vsechny 'herce' sloucim
		Array<Actor> actors = new Array<>(getStage().getActors());
		actors.addAll(uiStage.getActors());
		actors.addAll(startHryStage.getActors());

		for(Actor actor : actors)
		{
			if (actor instanceof IGrafickaCast)
			{
				((IGrafickaCast)actor).setGraphicPixelsToLogicBlock(graphicPixelsToLogicSize);
			}
			if (actor instanceof CelkoveZivoty)
			{
				float zivotyHeight = graphicPixelsToLogicSize.y / 3f;
				actor.setHeight(zivotyHeight);
				actor.setY(getHeight() - zivotyHeight);
			}
		}
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	@Override
	protected void beforeRenderScreen(float delta)
	{
		super.beforeRenderScreen(delta);
		if (!isPaused() && shakeEffect != null && shakeEffect.getDurationLeft() > 0)
		{
			resources.getCamera().translate(
				shakeEffect.act(delta)
			);
			resources.getCamera().update();
			getBatch().setProjectionMatrix(resources.getCamera().combined);
			getStage().getViewport().apply();
		}
	}

	@Override
	protected void renderScreen(float delta)
	{
		// zobrazeni vseho, co je ve scene, tj. mesto, letadlo, ...
		renderScreenStage(delta);
		renderParticleEffects(delta);
	}

	/** renderuje 'particle-effects, napr: strela, bomba explodovala aj */
	private void renderParticleEffects(float delta)
	{
		if (pe == null)
		{
			return;
		}
		pe.update(delta);
		getBatch().begin();
		pe.draw(getBatch());
		getBatch().end();
		if (pe.isComplete())
		{
			pe.reset();
		}
	}

	@Override
	public void render(float delta)
	{
		if (startHryStage.getRoot().hasChildren())
		{
			// pokud mam co zobrazovat ve fazi 'startHry', tak zobrazim odpocet...
			renderStartHryOdpocet(delta);
		}
		else
		{
			super.render(delta);
			renderUI(delta); // UI vykreslim jako posledni
		}
	}

	private void renderUI(float delta)
	{
		// UI: aktualizace kamery
		uiCamera.update();

		// nastaveni metriky/pozice kamery pro vykresleni UI sceny ve spravnych souradnicich
		uiStage.getBatch().setProjectionMatrix(uiCamera.combined);
		uiStage.getViewport().apply(true);

		// vykonani logiky nad UI, pokud neni 'zapauzovana'
		if (!isPaused())
		{
			uiStage.act(delta);
		}

		// vykresleni grafiky UI
		uiStage.draw();
	}

	private void renderStartHryOdpocet(float delta)
	{
		// UI: aktualizace kamery
		uiCamera.update();

		// nastaveni metriky/pozice kamery pro vykresleni 'odpoctu' sceny ve spravnych souradnicich
		startHryStage.getBatch().setProjectionMatrix(uiCamera.combined);
		startHryStage.getViewport().apply(true);

		// vykonani logiky nad 'odpoctu'
		startHryStage.act(delta);

		// vykresleni grafiky 'odpoctu'
		startHryStage.draw();
	}

	@Override
	public void onStartHry(LevelConfiguration levelConfiguration)
	{
		// TODO: xhonza: ten odpocet vypada divne, resp. reseni je divne...
		// (lepsi by bylo vyresit to, ze logika zbori vic kosticek a grafika to reflektuje ihned!)
		// COZ: TOTO: se da pouzit jako 'ucebnicovy priklad', ze se to ma resit event-driven,
		// kdy 1x eventa produkuje dalsi a muze to jit v kaskade



//		// TODO: ukol/GRAFIKA: kdyz nastane startHry, zobraz odpocet: 3...2...1
//		// vytvorim display pro odpocet: 3..2..1
//		CountdownDisplay odpocet = new CountdownDisplay(
//			resources.getFontsManager().getBitFontConfiguration(),
//			10.0f,
//			new String[] { "3", "2", "1" },
//			Color.GOLDENROD
//		);
//		int size = (int)(Math.min(getWidth(), getHeight()) * 0.15f);
//		odpocet.setViewport(
//			new Rectangle(
//				(getWidth() / 2) - (size / 2),
//				(getHeight() / 2) - (size / 2),
//				size,
//				size
//			)
//		);
//		odpocet.addListener(new CountdownDisplayListener()
//		{
//			@Override
//			public void onUpdateCountdown(int counter, String current)
//			{
//				// TODO Auto-generated method stub
//				System.out.println("onUpdateCountdown: " + counter + " $" + current);
//			}
//
//			@Override
//			public void onFinishCountdown()
//			{
//				// uz nebudu zobrazovat odpocet, tim, ze odeberu actora ze sceny...
//				odpocet.remove();
//
//				// a odpauzuju hru...
//				resume();
//			}
//		});
//
//		// vycistim scenu s 'odpoctem' a pridam do ni 'display' a tim, se zacne odpocer renderovat...
//		startHryStage.clear();
//		startHryStage.addActor(odpocet);
//
//		// zapauzuju 'herni-logiku'
//		pause();
//
//		// TODO: xhonza: 3..2..1 mas to 2x proc???
//		// TODO: xhonza: 3..2..1 nemas to ve stredu proc???
//
////		- musim zapauzovat logiku
////		- jakmile skonci odpocet, tak ji odpauzovat a hra zacne
//
		// TODO: pimp/zvuky: muzes prehrat zvuk pro zacatek hry (~bonus: v menu zvuk hrat nebude)
		resources.getSoundsManager().play(TypZvuku.StartHry);
	}

	/** zmeni obrazovku - podle toho, zda jsem vitez anebo porazeny */
	@Override
	public void onKonecHry(boolean jsemVitez)
	{
		// 'konec-hry' muze nastat pouze 1x!
		if (isKonecHry)
		{
			return;
		}
		isKonecHry = true;

		// najdu 'CelkoveScore'
		// (zde ukazka: jak mohu hledat 'Actor' ve scene podle jeho jmena)
		CelkoveScore body = getStage().getRoot().findActor("celkoveBody");

		// TODO: ukol/HRA: podle jsemVitez zarid zobrazeni spravne obrazovky
		if (jsemVitez)
		{
			parentGame.setScreen(
				new ObrazovkaDalsiUroven(
					parentGame,
					null, // TODO: prevScreen will be MainMenu!
					CELKOVE_BODY_BARVA,
					levelConfiguration,
					body.getScore(),
					((ILLetadlo)letadlo.getLogika()).getZivoty()
				)
			);
		}
		else
		{
			// TODO: pimp/zvuky: muzes prehrat zvuk, ze je konec
			resources.getSoundsManager().play(TypZvuku.Letadlo_explodujeDoDomu_KONEC);

			// vykreslim posledni stav hry
			getStage().draw();

			// vytvorim obrazovku konce-hry
			IObrazovkaDalsiUroven konecHry = new ObrazovkaKonecHry(
				parentGame,
				getPreviousScreen(), // predchozi-obrazovka: main-menu
				ZIVOTY_BARVA_PRAZDNE
			);

			// nastavim:
			// - pozadi jako posledni snimek z frame-bufferu, aby 'game over' vypadal verohodneji...
			// - level, abych vedel az kam hrac dosel (vc. celkovych bodu, abych mohl resit hi-score)
			konecHry.setBackground(new Sprite(ScreenUtils.getFrameBufferTexture()));
			konecHry.setLevelConfiguration(
				new LevelConfiguration(
					body.getScore(),
					levelConfiguration
				)
			);

			// zmenim obrazovku
			parentGame.setScreen(konecHry);
		}
	}

	/** vytvori vsechny potrebne objekty pro zacatek hry z {@link LevelConfiguration} */
	protected void startHry(int startovniBody)
	{
		// vytvoreni 'observeru', ktery informuje vsechny 'posluchace' o udalostech ve hre
		// a stara se tak o 'probublavani' logickych informaci, ke vsem, kteri o tom chteji vedet
		ILogikaHryHandler logikaHandler = new LogikaHryHandler();

		// vytvoreni tovarnicky: pro vytvareni hernich prvku
		// - musim to mit zde, abych mohl menit graficky vzhled 'levelu', napr: jine domy, letadla, strely aj
		// - a mohl jsem to 'pretezovat' v pripade potreby 'specialnich obrazovek', napr: MainMenu
		tovarnicka = createHerniPrvkyTovarnicka();
		tovarnicka.setLevelConfiguration(levelConfiguration);
		tovarnicka.setLogikaHry(logikaHandler);

		// vytvoreni efektu 'zahybani kamerou' (~kdyz letadlo narazi do budovy)
		shakeEffect = new ShakeEffect(1.0f, 0.2f);

		// vytvoreni 'score'
		CelkoveScore body = createCelkoveBody(startovniBody);

		// vytvoreni graficke reprezentace letadla
		letadlo = tovarnicka.createLetadlo();

		// vytvoreni celkovych zivotu 'letadla'
		zivoty = createCelkoveZivoty(letadlo);

		// vytvoreni grafickeho reprezentace mesta
		mesto = tovarnicka.createMesto();

		// vytvori UI ovladani (~pro tapani: vlevo x vpravo)
		boolean mamUI = createHerniUI();

		// hra: pridam jednotlive herce, tj: mesto, letadlo
		getStage().clear();
		getStage().addActor(body); // body   musi byt jako 1. (aby byly v pozadi!)
		getStage().addActor(mesto);
		getStage().addActor(letadlo);

		if (mamUI)
		{	// pokud mam UI, pridam jej do sceny, abych mohl tapat: vlevo x vpravo
			getStage().addActor(uiTapVlevoBomba);
			getStage().addActor(uiTapVpravoStrela);
		}

		// ui: pridam jednotlive herce do 'specialni' ui-sceny
		// (aby 'shakeEffect' neovlivnoval ui. vypada to lepe)
		uiStage.clear();
		uiStage.addActor(zivoty);

		// vytvorim instanci logiky cele hry, kde vyresim vazby:
		// - letadlo x mesto
		// - projektil x dum (~bloky-domu)
		// - konec-hry, skore, atd
		logikaHry = createLogikaHry(
			this,
			createZvuky(),
			body,
			(ILMesto)mesto.getLogika(),
			(ILLetadlo)letadlo.getLogika(),
			levelConfiguration,
			logikaHandler
		);

		// zavolam resize, protoze grafika musi vedet, jak ma byt velka
		// mapovani: 1x logickehoBlogu => 1x grafickyBlok
		resize(getWidth(), getHeight());

		isKonecHry = false;
		logikaHry.startHry();
	}

	/**
	 * vytvorim logiku cele hry, vc. vyreseni vazeb:
	 * - letadlo x mesto
	 * - projektil x dum (~bloky-domu)
	 * - konec-hry, skore, atd
	 */
	protected LogikaHry createLogikaHry(
		HerniGrafickeListenery grafikaListenery,
		HerniZvukoveListenery zvukyListenery,
		CelkoveScore body,
		ILMesto mesto,
		ILLetadlo letadlo,
		LevelConfiguration levelConfiguration,
		ILogikaHryHandler logikaHandler
	)
	{
		return new LogikaHry(
			grafikaListenery,
			zvukyListenery,
			body,
			mesto,
			letadlo,
			levelConfiguration,
			logikaHandler
		);
	}

	protected HerniZvukoveListenery createZvuky()
	{
		return new ObrazovkaHryZvuky(
			resources.getSoundsManager()
		);
	}

	/** zavola se, kdyz ma letadlo vystrelit strelu */
	private void onVystrelStrelu()
	{
		// TODO: ukol/HRA: vystrel strelu
		System.out.println("Letadlo: Vystrel Strelu!");

		// vytvorim strelu (podle typu letadla)
		GStrela strela = tovarnicka.createStrela(letadlo);

		// reknu, ze ma letadlo strilet
		if (!letadlo.vystrel(strela))
		{
			// kdyz se vystel nepovede; neprovadim dalsi kod
			return;
		}

		// a pridam bombu do sceny, aby se kreslila a resila jeji logika
		getStage().addActor(strela);

		// zaktualizuju pro nove pridane graficke objekty velikost policka
		updateGraphicPixelsToLogicBlock();
	}

	/** zavola se, kdyz ma letadlo vystrelit bombu */
	private void onVystrelBombu()
	{
		// TODO: ukol/HRA: vystrel bombu
		System.out.println("Letadlo: Shod Bombu!");

		// vytvorim bombu (podle typu letadla)
		GBomba bomba = tovarnicka.createBomba(letadlo);

		// reknu, ze ma letadlo strilet
		if (!letadlo.vystrel(bomba))
		{
			// kdyz se vystel nepovede; neprovadim dalsi kod
			return;
		}

		// a pridam bombu do sceny, aby se kreslila a resila jeji logika
		getStage().addActor(bomba);

		// zaktualizuju pro nove pridane graficke objekty velikost policka
		updateGraphicPixelsToLogicBlock();
	}

	@Override
	protected void createInputProcessors(List<InputProcessor> inputs)
	{
		// zaregistruju 'back-button' a getStage() jako input-processor, tj. zavolanim rodice!
		super.createInputProcessors(inputs);

		// a pridam vlastni input-processor na mackani klaves
		// TODO: xhonza: tap-{left,right}
		inputs.add(
			new InputAdapter()
			{
				@Override
				public boolean keyUp(int keycode)
				{
			        switch (keycode)
			        {
						case Keys.NUM_1:
						{
							if (BombarderGame.isDebug())
							{
								// pokud mam zapnuty debug -> a zmacknu '1' -> presunu se na dalsi level...
								onKonecHry(true);
							}
							return true;
						}
						case Keys.RIGHT:
						{
							onVystrelStrelu();
							return true;
						}
						case Keys.DOWN:
						{
							onVystrelBombu();
							return true;
						}
						default:
							return false;
			        }
				}
			}
		);
	}

	@Override
	protected TypObrazovky getPreviousScreen()
	{
		return TypObrazovky.HLAVNI_NABIDKA;
	}

	@Override
	protected void onBackButtonPressed()
	{
		onKonecHry(false);
	}

	/**
	 * vrati {@link GrafickaCast} grafickou interpretaci logiky (~tj. najde a vrati "Actora" ve scene podle {@link LogickaCast} objektu).
	 * pokud grafiku nenajde, vraci null.
	 */
	@Override
	public IGrafickaCast findGrafika(ILogickaCast logickyObjekt)
	{
		try
		{
			return getStage()
				.getRoot()
				.findActor(logickyObjekt.getUuid().toString())
			;
		}
		catch(Exception e)
		{
			Gdx.app.debug(getClass().getName(), "findGrafika(" + logickyObjekt + "): exception! not-found!");
			Gdx.app.debug(getClass().getName(), "findGrafika(" + logickyObjekt + "): " + e.getMessage());
			return null;
		}
	}

	/** najde a vrati "Actora" podle uuid */
	@Override
	public Actor findActor(ILogickeUuid logickeUuid)
	{
		try
		{
			return getStage()
				.getRoot()
				.findActor(
					// vazba mezi: GrafickaCast x LogickeUuid ~ je, ze objekt pojmenuju s prefixem
					// a jsem schopen ho podle nazvu dohledat
					GrafickaCast.UUID_PREFIX + logickeUuid.getUuid()
				)
			;
		}
		catch(Exception e)
		{
			Gdx.app.debug(getClass().getName(), "findActor(" + logickeUuid + "): exception! not-found!");
			Gdx.app.debug(getClass().getName(), "findActor(" + logickeUuid + "): " + e.getMessage());
			return null;
		}
	}

	@Override
	public void onCelkoveScoreChanged(CelkoveScore score, int prevScore)
	{
		// tento kod se spusti, kdyz:
		// - se zmeni celkove score

		// TODO: ukol/GRAFIKA: kdyz skore dosahne nove maximalni hodnoty (~tj. prekroci high score), mel by to zvititelnit (~napr nejaky efekt)
	}

	@Override
	public void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil)
	{
		// tento kod se spusti, kdyz:
		// - letici projektil (strela nebo bomba) exploduje naprazdno, tj. nezasahne cil
		// - letici projektil muze byt i letadlo. kdyz exploduje je konec hry.

		// TODO: ukol/GRAFIKA: odstran projektil ze sceny, protoze explodoval a nema jiz vyznam...
		Actor actor = findActor(projektil);
		if (actor != null)
		{
			actor.remove();
		}

		// TODO: pimp/grafika: muzes prehrat animaci, napr: exploze projektilu (~podle typu)
	}

	@Override
	public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
	{
		// tento kod se spusti, kdyz:
		// - letici projektil (strela nebo bomba) narazi do domu, tj. zasahne cil!
		// - letici projektil (strela nebo bomba) by mel zmizet!
		// - letici projektil muze byt i letadlo; tady neresim (~resim v onLetadloZtratiloZivoty)

		// TODO: ukol/GRAFIKA: letici strela nebo bomba by mela zmizet!
		if (!(projektil instanceof ILLetadlo))
		{
			Actor actor = findActor(projektil);
			if (actor != null)
			{
				actor.remove();
			}
		}

		// TODO: ukol/GRAFIKA: vizualizace zboreni "bloku domu"
		for(ILBlokDomu blokDomu : zniceneBlokyDomu)
		{
			Actor actor = mesto.findActor(blokDomu);
			if (actor == null)
			{
				continue;
			}
			// TODO: pimp/grafika: muze se napr odlomit a spadnout dolu... (~anebo vybuchnout)
			actor.remove();
		}

		// TODO: ukol/GRAFIKA: kolize letadla s blokem domu
		// TODO: pimp/grafika: kdyz letadlo narazi do domu, muzes "zatrest s obrazovkou" (~shake effect)
		shakeEffect.reset(15.0f,  0.5f);
	}

	@Override
	public void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage)
	{
		// tento kod se spusti, kdyz:
		// - letadlo ztratilo zivoty

		// TODO: ukol/GRAFIKA: letadlo prislo o zivoty, tj. vizualizace o kolik (~napr: -123)
		zivoty.updateValues(
			letadlo.getZivoty(),
			zivoty.getMaximalniZivoty()
		);
		zivoty.setVisible(
			// zivoty nezobrazim, kdyz je mam plne...
			zivoty.getPercentage() == 1.0f ? false : true
		);
	}

	@Override
	public void onLetadloStrili(ILLetadlo letadlo, ILLeticiProjektil projektil)
	{
		// tento kod se spusti, kdyz:
		// - letadlo vystreli strelu

		// TODO: ukol/GRAFIKA: letadlo vystrelilo strelu, tj. vizualizace 'praaask!'
	}


	@Override
	public void onDumJeKompletneZnicen(ILDum dum)
	{
		// tento kod se spusti, kdyz:
		// - dum je kompletne znicen

		// TODO: ukol/GRAFIKA: muzes vymyslet nejaky efekt
	}

	@Override
	public void setLevelConfiguration(LevelConfiguration levelConfiguration)
	{
		this.levelConfiguration = levelConfiguration;
//		tovarnicka.setLevelConfiguration(levelConfiguration);
	}
}
