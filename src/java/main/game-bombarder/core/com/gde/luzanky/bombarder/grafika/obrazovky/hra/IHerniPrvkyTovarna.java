package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.display.CelkoveScore;
import com.gde.luzanky.bombarder.grafika.hra.bomba.GBomba;
import com.gde.luzanky.bombarder.grafika.hra.letadlo.GLetadlo;
import com.gde.luzanky.bombarder.grafika.hra.mesto.GMesto;
import com.gde.luzanky.bombarder.grafika.hra.strela.GStrela;
import com.gde.luzanky.bombarder.grafika.prvky.CelkoveZivoty;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

public interface IHerniPrvkyTovarna
{
	void setLevelConfiguration(LevelConfiguration levelConfiguration);
	void setLogikaHry(ILogikaHryHandler logikaHandler);

	CelkoveScore createCelkoveBody(int startovniBody, Color barva);
	CelkoveZivoty createCelkoveZivoty(float zivoty, Color prazdneZivoty, Color plneZivoty);

	GLetadlo createLetadlo();
	GStrela createStrela(GLetadlo letadlo);
	GBomba createBomba(GLetadlo letadlo);

	GMesto createMesto();
}
