package com.gde.luzanky.bombarder.grafika.prvky;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gde.common.graphics.ui.items.progress.horizontal.ProgressBar;
import com.gde.lwjgl.util.vector.Vector4f;


/** zivoty jako progressBar (0 - 100%) vcetne barevneho gradientu a pruhlednosti */
public class CelkoveZivoty
extends ProgressBar
{
	private int maximalniZivoty;
	private final Color color0;
	private final float color0alpha;
	private final Color color100;
	private final float color100alpha;


	public CelkoveZivoty(int maximalniZivoty, String textureFilename, Color color0, Color color100)
	{
		this(
			maximalniZivoty,
			maximalniZivoty,
			color0,
			1.0f,
			color100,
			0.25f,
			textureFilename
		);
	}

	private CelkoveZivoty(
		int pocatecniZivoty,
		int maximalniZivoty,
		Color color0,        // barva, kdyz 'skoro' nemam zadne zivoty
		float color0alpha,   // pruhlednost, kdyz 'skoro' nemam zadne zivoty
		Color color100,      // barva, kdyz jsem 'skoro' zdravy
		float color100alpha, // pruhlednost, kdyz jsem 'skoro' zdravy
		String textureFilename
	)
	{
		this.maximalniZivoty = pocatecniZivoty;
		this.color0 = color0.cpy();
		this.color0alpha = color0alpha;
		this.color100 = color100.cpy();
		this.color100alpha = color100alpha;
		setIncreasingNinePatch(createNinePatch(textureFilename));
		setIncreasingTintColor(color0.cpy());
		setDecreasingNinePatch(createNinePatch(textureFilename));
		setDecreasingTintColor(color100.cpy());
		updateValues(pocatecniZivoty, maximalniZivoty);
	}

	/** vytvoreni 9-patch z textury, @see https://developer.android.com/studio/write/draw9patch */
	private NinePatch createNinePatch(String filename)
	{
		final Texture t = new Texture(Gdx.files.internal(filename));
		final int width = t.getWidth() - 2;
		final int height = t.getHeight() - 2;
		return new NinePatch(new TextureRegion(t, 1, 1, width, height));
	}

	/**  zavola se vzdy, kdyz se zmeni procenta... (~vypocitavam novou barvu jako gradient) */
	@Override
	protected void percentageChanged(float previous, float current)
	{
		super.percentageChanged(previous, current);
		Vector4f gradient = lerpRGB(color0, color100, getPercentage());
		increasingColor.set(
			gradient.x,
			gradient.y,
			gradient.z,
			gradient.w * color0.a * color0alpha
		);
		decreasingColor.set(
			gradient.x,
			gradient.y,
			gradient.z,
			gradient.w * color100.a * color100alpha
		);
	}

	/** vytvorim 'prechodnou' promennou pro uchovani vypoctu gradientu za pomoci 'lerp' z duvodu rychlosti */
	private final Vector4f lerp = new Vector4f();
	/** vypoctu gradient mezi 2-mi barvami; @see: https://www.alanzucconi.com/2016/01/06/colour-interpolation/ */
	private Vector4f lerpRGB(Color a, Color b, float t)
	{
		// -- https://www.alanzucconi.com/2016/01/06/colour-interpolation/
		lerp.set(
	        a.r + (b.r - a.r) * t,
	        a.g + (b.g - a.g) * t,
	        a.b + (b.b - a.b) * t,
	        a.a + (b.a - a.a) * t
	    );
		return lerp;
	}

	public int getMaximalniZivoty()
	{
		return maximalniZivoty;
	}
}
