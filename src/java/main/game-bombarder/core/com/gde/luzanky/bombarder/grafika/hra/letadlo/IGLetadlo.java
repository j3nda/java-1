package com.gde.luzanky.bombarder.grafika.hra.letadlo;

import com.gde.luzanky.bombarder.grafika.hra.GLeticiProjektil;

public interface IGLetadlo
{
	boolean vystrel(GLeticiProjektil projektil);
}
