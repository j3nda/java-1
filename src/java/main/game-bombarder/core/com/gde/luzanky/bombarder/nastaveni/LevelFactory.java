package com.gde.luzanky.bombarder.nastaveni;

import com.gde.common.utils.RandomUtils;

/**
 * tovarnicka na vytvoreni nastaveni mesta (~LevelConfiguration)
 * - vsimni si viditelnosti
 * - a ze metoda 'create' je staticka
 */
class LevelFactory
{
	/**
	 * vyctovy typ 'enum' pro vyjmenovani a nastaveni jednotlivych mest
	 * - vsimni si, ze 'enum' je vlastne objekt
	 * - tj. muzu pretezovat metody a pouzivat 'custom' vlastnosti
	 */
	private enum Mesto
	{
		// cz
		Praha(1335084),
		Brno(382405),
		Ostrava(284982),
		Plzen(175219),
		Liberec(104261),
		Olomouc(100514),
		Kladno(68896),
		Most(65341),
		Opava(55996),
		Jihlava(51125),
		Trebic(35107),
		Breclav(24554),
		Vyskov(20676),
		Zubri(5535),

		// sk
		Bratislava(424207),

		// world-wide[capital]
		Beijing(21542000),
		Tokyo(13929286),
		Moscow(12691000),
		Cairo(9848576),
		London(8908081),
		Bangkok(8305218),
		Hanoi(8053663),
		HongKong(7482500),
		Berlin(3748148),
		Madrid(3266126),
		Pyongyang(3144005),
		AddisAbaba(3040740),
		Rome(2873104),
		Taipei(2668572),
		Paris(2241346),
		Havana(2135498),
		Vienna(1749673),
		Budapest(1729040),
		Warsaw(1711324),
		Dublin(1173179),
		Ottawa(934243),
		Jerusalem(927000),
		Tunis(767629),
		Oslo(645701),
		Helsinki(635591),
		Reykjavik(11500),
		Monaco(35986),
		;
		public final int pocetObyvatel;

		Mesto(int pocetObyvatel)
		{
			this.pocetObyvatel = pocetObyvatel;
		}

		@Override
		public String toString()
		{
			switch(this)
			{
				default:
					return name().toLowerCase();
			}
		}

		/** vrati nahodne mesto */
		static Mesto random()
		{
			return values()[RandomUtils.nextInt(0, values().length - 1)];
		}
	}

	/** vytvori novy nastaveni pro dalsi uroven z predchozi urovne */
	static LevelConfiguration create(int body, float letadloZivoty, int level, LevelConfiguration predchoziUroven)
	{
		switch(level)
		{
			case 1:
			{
				return new LevelConfiguration(
					level,
					Mesto.Brno.toString(),
					0, // celkoveBody
					1*2.5f, // rychlostLetadla: normal(2.5), debug(12.5)
					10000, // pocet zivotu letadla
					20, // maximalni pocet pater domu
					20 // maximalni pocet domu
				);
			}
			// TODO: level: 2-4 bych mohl mit pred-nastaveny! ...zbytek nahodne...
			default:
			{
				return new LevelConfiguration(
					level,
					Mesto.random().toString(),
					body,
					1.5f * predchoziUroven.rychlostLetadla,
					(int)(0.9f * letadloZivoty),
					20, // maximalni pocet pater domu
					20 // maximalni pocet domu
				);
			}
		}
	}
}
