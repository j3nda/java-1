package com.gde.luzanky.bombarder.logika.mesto.dum;

public enum TypPoskozeni
{
	/** zadne poskozeni, tj. 100% health! */
	Vubec,
	/** castecne poskozeni... */
	Castecne,
	/** kompletni poskozeni, tj. 0% health! */
	Kompletne,
}
