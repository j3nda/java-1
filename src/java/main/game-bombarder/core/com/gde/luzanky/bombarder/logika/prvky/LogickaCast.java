package com.gde.luzanky.bombarder.logika.prvky;

import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;
import com.gde.luzanky.bombarder.logika.hra.HerniLogickeListenery;

/**
 * sdilena logicka cast: ze ktere dedi kazdy logicky objekt!
 * - vsechno co je ve hre ma svou logickou souradnici (x, y)
 *   - kterou vraci: getX(), getY() ~ jako cele cislo int
 *   - anebo nastavuje: setPozice()
 *
 * - obsahuje metodu "act()" ktera ovlada logiku!
 * - LOGIKA NIKDY NEOBSAHUJE GRAFICKOU INTERPRETACI!
 *
 * slovicko "abstract" zde vynucuje, ze nekdo musi tuto tridu zdedit,
 * tj. sdilet vsechny vlastnosti zde uvedene!
 */
public abstract class LogickaCast
implements ILogickaCast
{
	/** unikatni identifikator, kazdeho {@link LogickaCast} objektu */
	private final int uuid;
	private static volatile int uuidCounter = 0;
	protected int x = 0;
	protected int y = 0;
	/** 'ohandluje' provolavani vsech {@link HerniLogickeListenery} listeneru, kteri chteji vedet o udalostech ve hre */
	protected ILogikaHryHandler handler;
	private boolean onPositionChangedFirstTime = true;


	protected LogickaCast()
	{
		 uuid = createUUID();
	}

	/** vytvori unikatni identifikator */
	private synchronized int createUUID()
	{
		// 1/ reseno jako 'synchronized', aby nedoslo ke kolizim
		// 2/ nejprve se "vrati", potom se inkrementuje
		// 3/ 'uuidCounter' je 'static', tj. 1x instance pro vsechny 'logickaCast'
		return uuidCounter++;
	}

	@Override
	public Integer getUuid()
	{
		return uuid;
	}

	@Override
	public int getX()
	{
		return x;
	}

	@Override
	public int getY()
	{
		return y;
	}

	@Override
	public void setLogickaPozice(int x, int y)
	{
		boolean zmenilaSePozice = false;
		if (this.x != x || this.y != y)
		{
			zmenilaSePozice = true;
		}
		this.x = x;
		this.y = y;
		if (zmenilaSePozice || onPositionChangedFirstTime)
		{
			onPositionChangedFirstTime = false;
			onPositionChanged();
		}
	}

	/**
	 * metoda se vola pri kazdem "render" cyklu a vykonava logickou cast,
	 * tj. je to "mozek" dane tridy, ktery se stara o spravne logicke chovani.
	 * nazev "act()" je prevzat z knihovny LibGdx ~ a nasleduje jeji logiku
	 * (i kdyz je snaha mit hru a volani metod v cestine)
	 *
	 * (!) metoda je zamerne "abstract" (~predpis), abych me to nutilo pri jejim
	 *     dedeni - metodu pretizit "@Override", a UJASNIT SI CO LOGIKA DELA!
	 */
	@Override
	public abstract void act(float delta);

	/** metoda se vola pokazde, kdyz se zmeni pozice */
	protected void onPositionChanged()
	{

	}

	@Override
	public float getRemainingToNextLogicAction()
	{
		return 0;
	}

	@Override
	public void setLogikaHry(ILogikaHryHandler logikaHandler)
	{
		handler = logikaHandler;
	}
}
