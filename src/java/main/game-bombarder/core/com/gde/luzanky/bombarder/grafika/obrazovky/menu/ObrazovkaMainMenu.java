package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.gde.common.game.GdeGame;
import com.gde.common.graphics.display.IDisplayRowLetters;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.common.graphics.display.MultiLineDisplayListener;
import com.gde.luzanky.bombarder.BombarderGame;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.IHerniPrvkyTovarna;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ObrazovkaHry;
import com.gde.luzanky.bombarder.grafika.obrazovky.menu.MenuLetadlo.LetadloLetiNahoruDolu;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;


/** obrazovka pro hlavni nabidku */
public class ObrazovkaMainMenu
extends ObrazovkaHry
{
	/** enum jako vyjadreni 'MainMenu' */
	private enum Nabidka {
		NEW_GAME("new game"),
		SEPARATOR1(" "),
		QUIT("quit"),
		;
		private final String name;
		private Nabidka(String name)
		{
			this.name = name;
		}
		public static String[] toStringArray()
		{
			String[] names = new String[values().length];
			Nabidka[] values = values();
			for(int i = 0; i < values.length; i++)
			{
				names[i] = values[i].name;
			}
			return names;
		}
	}
	private MultiLineDisplay menu;


	public ObrazovkaMainMenu(BombarderGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
		setLevelConfiguration(
			// nastaveni 'levelu' pro MainMenu!
			new LevelConfiguration(
				1, // cislo levelu (~nepotrebuju, je to main-menu)
				"cokoliv", // nazev mesta (~nepotrebuju, je to main-menu)
				0, // celkoveBody
				3.0f, // rychlostLetadla: normal(2.5), debug(12.5)
				10000, // pocet zivotu letadla
				20, // maximalni pocet pater domu
				20 // maximalni pocet domu
			)
		);
	}

	private void startNoveHry()
	{
		ObrazovkaHry novaHra = new ObrazovkaHry(parentGame, getScreenType());
		novaHra.setLevelConfiguration(
			// 1. uroven ma 'default' konstruktor, ktery vytvori mesto pro 1. level
			new LevelConfiguration()
		);
		parentGame.setScreen(novaHra);
	}

	@Override
	protected void startHry(int startovniBody)
	{
		super.startHry(startovniBody);

		// nastaveni pozice letadla; do leve casti obrazovky, protoze vpravo je nabidka
		// (~tak aby to bylo pekne videt)
		letadlo.getLogika().setLogickaPozice(
			((ILMesto)mesto.getLogika()).getMaximalniPocetDomu() / 4,
			((ILMesto)mesto.getLogika()).getMaximalniPocetPaterDomu() - ((MenuLetadlo)letadlo).amplitude
		);

		// nastaveni listeneru, kdyz se letadlo pohne
		((LetadloLetiNahoruDolu)letadlo.getLogika()).setListener((MenuMesto)mesto);

		// 'ugly' reseni, abych vytvoril hlavni nabidku pouze 1x!
		// startHry() se vola onKonecHry(); kdyz je letadlo zniceno - tak, abych neprisel o UI
		if (menu == null)
		{
			menu = createMainMenu(
				Gdx.graphics.getWidth() / 2f
			);
		}
		uiStage.clear();
		uiStage.addActor(menu);

		setInputProcessor();
	}

	@Override
	public void onStartHry(LevelConfiguration levelConfiguration)
	{
		// nevolam 'rodice', abych nemel v 'main-menu' zobrazeni odpoctu: 3...2...1
	}

	@Override
	public void onKonecHry(boolean jsemVitez)
	{
		// listener na konec hry ignoruju (~konec hry by nemel nikdy nastat)
		// TODO: xhonza: pro ted zavolam start nove hry...
		startHry(0);
	}

	@Override
	protected IHerniPrvkyTovarna createHerniPrvkyTovarnicka()
	{
		return new HerniPrvkyTovarna(resources);
	}

	@Override
	protected boolean createHerniUI()
	{
		return false;
	}

	private HlavniNabidka createMainMenu(float width)
	{
		HlavniNabidka menu = new HlavniNabidka(
			resources.getFontsManager().getBitFontConfiguration(),
			Nabidka.toStringArray(),
			Color.WHITE
		);
		menu.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() - width,
				0,
				width,
				Gdx.graphics.getHeight()
			)
		);
		menu.addListener(new MultiLineDisplayListener()
		{	// pridam listener na zpracovani kliknuti/tapnuti na radek s displayem...
			@Override
			public void onTapRowDisplay(int index, IDisplayRowLetters display)
			{
				// prijde mi 'index' jako poradi radku (~od shora)
				// - to mohu sparovat s 'Nabidka', protoze jsem to s tim vytvarel
				// - a snadno pomoci 'switch' zjistit, na co jsem tapnul
				switch(Nabidka.values()[index])
				{
					case NEW_GAME:
					{
						startNoveHry();
						break;
					}
					case QUIT:
					{
						quit();
						break;
					}
					default:
						break;
				}
			}
		});
		return menu;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HLAVNI_NABIDKA;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderBackground();
		renderScreenStage(delta);
	}

	@Override
	protected void onBackButtonPressed()
	{
		quit();
	}

	/** ukonci celou hru */
	private void quit()
	{
		BombarderGame.exit(GdeGame.ReturnCodes.ALL_OK);
	}

	@Override
	protected void createInputProcessors(List<InputProcessor> inputs)
	{
		// POZOR: nevolam: super.createInputProcessors(); abych 'v menu' vypnul input-processory ze hry!
		inputs.clear();
		inputs.add(new BackButtonInputProcessor());
		inputs.add(uiStage);
	}
}
