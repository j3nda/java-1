package com.gde.luzanky.bombarder.fonty;

import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public interface IFontsManager
{
	BitFontMaker2Configuration getBitFontConfiguration();
}
