package com.gde.luzanky.bombarder.logika.mesto;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;
import com.gde.luzanky.bombarder.logika.hra.HerniLogickeListeneryAdapter;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.mesto.dum.LDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.LogickaCast;

/**
 * logicka cast celeho mesta
 * trida dedi vsechny vlastnosti {@link LogickaCast}
 *
 * abych drzel vsechnu logiku "pod jednou strechou", mesto obsahuje:
 * - letadlo
 * - domy
 *
 * v okamziku, kdy se vykonava metoda "act()" resici logiku,
 * musi se dana logika provolavat na vsechny prvky
 * (zamerne zde neni pouzit LibGdx {@link Stage} - z duvodu uceni se principu!)
 *
 * zaroven, kolize se resi v okamziku, kdy se 'neco' hybe...
 */
public class LMesto
extends LogickaCast
implements ILMesto
{
	private final int maximalniPocetPaterDomu;
	private final int maximalniPocetDomu;
	private int aktualniPocetDomu;
	/** domy ve meste */
	private final LDum[] domy;
	/** drzim si informace o 'letici projektil', ktery se pohybuje, abych mohl resit kolize: projektil x domy */
	private final Map<Integer, ProjektilSePohlInfo> projektilSePohl;
	/** struktura pro uchovavani informaci o pohybujicich se projektilech */
	private static class ProjektilSePohlInfo
	{
		public boolean isMoved;
		public boolean isMovedX;
		public boolean isMovedY;
		public int lastX;
		public int lastY;
		public final ILLeticiProjektil projektil;

		public ProjektilSePohlInfo(ILLeticiProjektil projektil)
		{
			isMoved = isMovedX = isMovedY = true;
			lastX = projektil.getX();
			lastY = projektil.getY();
			this.projektil = projektil;
		}

		@Override
		public String toString()
		{
			return ""
				+ isMoved + "[" + isMovedX + "," + isMovedY + "]: "
				+ lastX + "," + lastY
			;
		}
	}

	public LMesto(int maximalniPocetPaterDomu, int maximalniPocetDomu)
	{
		this.maximalniPocetPaterDomu = maximalniPocetPaterDomu;
		this.maximalniPocetDomu = maximalniPocetDomu;
		this.domy = new LDum[maximalniPocetDomu];
		this.projektilSePohl = new HashMap<>();

		// vytvorim mesto
		vytvorMesto();
	}

	private void vytvorMesto()
	{
		aktualniPocetDomu = 0;
		for(int i = 0; i < getMaximalniPocetDomu(); i++)
		{
			domy[i] = vytvorDum(i);
			domy[i].setLogickaPozice(i, 0);
			aktualniPocetDomu++;
		}
	}

	protected LDum vytvorDum(int poziceDomuZleva)
	{
		return new LDum(
			// TODO: ukol/DIFFICULTY: v zavislosti na obtiznosti se mohou cisla menit
			// vrati nahodne cislo mezi: 1 az maximalniVyskaDomu
			// (zaroven muzes udelat pimp; aby vyska predchoziho a noveho nebyla nijak zavratne rozdilna)
			RandomUtils.nextInt(
				1,
				(0
					+ getMaximalniPocetPaterDomu() // pocet pater
					- RandomUtils.nextInt(1, 3)    // korekce, abych ihned nemel v ceste neco (~dokazal se rozkoukat)
				)
			)
		);
	}

	@Override
	public void setLogikaHry(ILogikaHryHandler logikaHandler)
	{
		super.setLogikaHry(logikaHandler);

		// 'probublam' nastaveni na jednotlive domy
		for(ILDum dum : domy)
		{
			dum.setLogikaHry(logikaHandler);
		}

		// pridam listener na 'onProjektilSePohl', abych mohl resit kolize
		addListener_projektilSePohl(logikaHandler);
		addListener_dumJeZnicen(logikaHandler);
	}

	/** zaregistruju udalost: kdyz je dum kompletne znicen; aktualizuju pocet 'neznicenych' domu */
	private void addListener_dumJeZnicen(ILogikaHryHandler handler)
	{
		handler.addLogicListener(new HerniLogickeListeneryAdapter()
		{
			@Override
			public void onDumJeKompletneZnicen(ILDum dum)
			{
				aktualniPocetDomu--;
				if (aktualniPocetDomu < 0)
				{
					aktualniPocetDomu = 0;
				}
				Gdx.app.debug(
					getClass().getName(),
					"onDumJeKompletneZnicen(" + dum + "): zbyva: " + aktualniPocetDomu
				);
			}
		});
	}

	private void addListener_projektilSePohl(ILogikaHryHandler logikaHandler)
	{
		projektilSePohl.clear();

		// vytvorim instanci "adapteru", kde se dozvim:
		// - ze se letadlo pohlo, tj. muzu resit pripadne kolize
		// - ze byl projektil vystrelen, abych mohl resit pripadne kolize
		// a pridam je do 'ILogikaHryHandler', abych se o ni 'jako posluchac' dozvedel
		logikaHandler.addLogicListener(new HerniLogickeListeneryAdapter()
		{
			@Override
			public void onProjektilSePohl(ILLeticiProjektil currentProjektil)
			{
				ProjektilSePohlInfo info = pohlSeProjektil(currentProjektil);
				if (info.isMoved)
				{
					if (info.projektil instanceof ILLetadlo)
					{
						letadloSePohlo(info, (ILLetadlo)info.projektil);
					}
					kolizeProjektiluADomu(info.projektil);
					info.isMoved = info.isMovedX  = info.isMovedY = false;
				}
			}
		});
	}

	private ProjektilSePohlInfo pohlSeProjektil(ILLeticiProjektil projektil)
	{
		ProjektilSePohlInfo info = projektilSePohl.get(projektil.getUuid());
		if (info == null)
		{
			info = new ProjektilSePohlInfo(projektil);
		}
		else
		{
			info.isMovedX = (projektil.getX() != info.lastX);
			info.isMovedY = (projektil.getY() != info.lastY);
			info.isMoved  = (info.isMovedX || info.isMovedY);
			info.lastX    = projektil.getX();
			info.lastY    = projektil.getY();
		}
		projektilSePohl.put(
			projektil.getUuid(),
			info
		);
		return info;
	}

	@Override
	public void act(float delta)
	{
		for(ILDum dum : domy)
		{
			if (dum == LDum.ZNICEN)
			{
				continue;
			}
			dum.act(delta);
		}
	}

	/** letadlo se pohlo: musim resit logiku, tj. vletelo do mesta; vyletelo z mesta; nabouralo do zeme, aj. */
	private void letadloSePohlo(ProjektilSePohlInfo pohlSe, ILLetadlo letadlo)
	{
		if (pohlSe.isMovedX && letadlo.getX() == 0)
		{
			handler.onLetadloVleteloDoMesta(letadlo, this);
		}

		if (pohlSe.isMovedX && letadlo.getX() >= getMaximalniPocetDomu())
		{
			handler.onLetadloVyleteloZMesta(letadlo, this);
		}

		if (pohlSe.isMovedY && letadlo.getY() < 0)
		{
			handler.onLetadloNabouraloDoZeme(letadlo);
		}
	}

	/** reseni kolize: projektil x dum */
	protected void kolizeProjektiluADomu(ILLeticiProjektil projektil)
	{
		// TODO: ukol/LOGIKA: nastala kolize? (projektil x dum x blok-domu)
		// - zvaz varianty, kdy kolize nedavaji smysl => ukonci pomoci 'return' (~a zbytek kodu nema jiz smysl)
		// - kdyz nastane kolize, mel bys znicit dum (~anebo jeho casti)

		// kdyz jsem mimo souradnice anebo mimo mesto, nedochazi ke kolizi
		if (projektil.getX() < 0 || projektil.getY() < 0 || projektil.getX() >= domy.length)
		{
			return;
		}

		ILDum dum = domy[projektil.getX()];
		if (dum == LDum.ZNICEN)
		{
			// dum tam byt nemusi... nahodne se generuje; muze tam byt prazdne policko, tj. preskoci se
			// (a ja jako programator na to musim myslet)
			return;
		}

		if (projektil.getY() >= dum.getVyskaDomu())
		{
			// projektil leti nad domem. k zadne kolizi nedoslo...
			return;
		}

		// projektil narazi do domu...
		dum.znicit(projektil);
	}

	@Override
	public LDum[] getDomy()
	{
		return domy;
	}

	@Override
	public int getMaximalniPocetDomu()
	{
		return maximalniPocetDomu;
	}

	@Override
	public int getMaximalniPocetPaterDomu()
	{
		return maximalniPocetPaterDomu;
	}

	@Override
	public int getAktualniPocetDomu()
	{
		return aktualniPocetDomu;
	}
}
