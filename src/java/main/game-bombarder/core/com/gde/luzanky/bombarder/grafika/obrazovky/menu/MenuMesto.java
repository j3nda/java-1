package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.bombarder.grafika.hra.mesto.GMesto;
import com.gde.luzanky.bombarder.grafika.hra.mesto.dum.GDum;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;
import com.gde.luzanky.bombarder.grafika.obrazovky.menu.MenuLetadlo.OnLetadloSePohloRychlosti;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.LMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.mesto.dum.LBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.LDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

class MenuMesto
extends GMesto
implements OnLetadloSePohloRychlosti
{
	/**
	 * indiktor, zda prave posouvam mesto?
	 * DULEZITE: abych posun mesta delal jenom 1x (~triggeruje se listenerem)
	 */
	private boolean posouvamMesto;
	/** true, kdyz posouvam mesto a 1. dum posouvam jako posledni; false, pro vytvoreni noveho domu na konci mesta. */
	private boolean posouvamMestoVytvarimNovyDum = true;


	public MenuMesto(int maximalniPocetPaterDomu, int maximalniPocetDomu)
	{
		super(
			new MenuLogickeMesto(
				maximalniPocetPaterDomu,
				maximalniPocetDomu
			)
		);
		posouvamMesto = false;
	}

	@Override
	public void onLetadloSePohloRychlosti(ILLetadlo letadlo)
	{
		// letadlo se pohlo...
		// posunu mesto doleva a vytvorim dojem, ze letadlo leti
		// (letadlo (~projektil) predavam, abych vyresil pripadnou kolizi: dum x letadlo)
		posunMestoDoleva(letadlo);
	}

	@Override
	protected GDum vytvorDum(int pozice, ILDum logickyDum)
	{
		// vytvoreni 'MenuDum' (~graficky dum, ktery obsahuje logicke chovani 'menu'!)
		return new MenuDum(
			// logickyDum je vytvoren z LMesto.vytvorDum(),
			// ktery pomoci MenuLogickeMesto.vytvorDum() pretezuju,
			// takze mam 100% jistotu, ze to je MenuLogickyDum!
			logickyDum,
			Color.NAVY
		);
	}

	/** posune mesto o -1 (~vlevo), tj. vsechny domy */
	void posunMestoDoleva(ILLeticiProjektil projektil)
	{
		// pokud prave posouvam mesto, nic nebudu delat
		if (posouvamMesto)
		{
			return;
		}
		posouvamMesto = true; // jinak nastavim, ze 'prave ted' posouvam mesto

		// grafika: posunuti domu (~ve meste)
		MenuDum prvniDumZmiziBudePosledni = (MenuDum)domy.get(0);
		for(int i = 1; i < domy.size(); i++)
		{
			domy.set(i - 1, domy.get(i));
		}

		// logika: posunuti domu (~ve meste) a ziskani noveho domu, uplne na konci mesta!
		MenuLogickyDum novyDum = ((MenuLogickeMesto)getLogika()).posunMestoDoleva(posouvamMestoVytvarimNovyDum);

		// pokud vytvarim cely novy objekt...
		if (posouvamMestoVytvarimNovyDum)
		{
			// nastaveni novyDum grafice; a uprava pozice (~posledni dum ve meste)
			prvniDumZmiziBudePosledni = (MenuDum) vytvorDum(domy.size() -1, novyDum);
			prvniDumZmiziBudePosledni.setGraphicPixelsToLogicBlock(graphicToLogicSize);
			prvniDumZmiziBudePosledni.setLogika(novyDum);
		}
		prvniDumZmiziBudePosledni.setLogika(novyDum); // TODO: NullPointerException, if (posouvamMestoVytvarimNovyDum == false)

		domy.set(
			domy.size() - 1,
			prvniDumZmiziBudePosledni
		);

		// logika: aktualizace pozice vsech domu ve meste
		LDum[] logickeDomy = ((MenuLogickeMesto)getLogika()).getDomy();
		for(int i = 0; i < logickeDomy.length; i++)
		{
			if (logickeDomy[i] == LDum.ZNICEN)
			{
				continue;
			}
			logickeDomy[i].setLogickaPozice(i, 0);
		}

		// musim poresit i kolizi ~ protoze letadlo(~projektil) se nehybe v ose-y;
		// tj. kdyz leti rovne - hybe se mesto...
		((MenuLogickeMesto)getLogika()).kolizeProjektiluADomu(projektil);

		// 'posouvani mesta' jsem dokoncil
		posouvamMesto = false;
	}

	private class MenuDum
	extends GDum
	{
		public MenuDum(ILDum logickyDum, Color color)
		{
			super(logickyDum, color);
		}

		/**
		 * diky inner-class, mohu 'za behu' vymenovat logiku;
		 * a zbytecne nevytvarim znovu a znovu new GDum()
		 * (ad performance && garbage-collector)
		 */
		@Override
		protected void setLogika(ILogickaCast logickaCast)
		{
			super.setLogika(logickaCast);
		}
	}

	static class MenuLogickyDum
	extends LDum
	{
		public MenuLogickyDum(int vyskaDomu)
		{
			super(vyskaDomu);
		}
		public MenuLogickyDum(ILDum dum)
		{
			super(dum.getVyskaDomu());
			setLogickaPozice(dum.getX(), dum.getY());
		}

		/** diky inner-class, mam pristup z: MenuLogickeMesto */
		@Override
		protected void updateDum(ILBlokDomu[] zbyleBlokyDomu, List<ILBlokDomu> zniceneBlokyDomu)
		{
			super.updateDum(zbyleBlokyDomu, zniceneBlokyDomu);
		}
	}

	static class MenuLogickeMesto
	extends LMesto
	{
		private final List<ILBlokDomu> znicenePatra = new ArrayList<>();
		private final List<ILBlokDomu> zbyvajiciPatra = new ArrayList<>();

		public MenuLogickeMesto(int maximalniPocetPaterDomu, int maximalniPocetDomu)
		{
			super(maximalniPocetPaterDomu, maximalniPocetDomu);
		}

		/** getter na {@link ILogikaHryHandler}, abych ho mohl zjistit zpatky... */
		public ILogikaHryHandler getLogikaHry()
		{
			return handler;
		}

		@Override
		protected LDum vytvorDum(int poziceDomuZleva)
		{
			LDum dum = new MenuLogickyDum(
				RandomUtils.nextInt(
					1,
					(0
						+ getMaximalniPocetPaterDomu() // pocet pater
						- RandomUtils.nextInt(1, 3)    // korekce, abych ihned nemel v ceste neco (~dokazal se rozkoukat)
					)
				)
			);
			dum.setLogickaPozice(poziceDomuZleva, 0);
			return dum;
		}

		/** posune 'logicke' domy ve meste o -1 (~vlevo) a vrati 'posledni' (~nove vytvoreny) */
		private MenuLogickyDum posunMestoDoleva(boolean vytvorNovyDum)
		{
			LDum[] domy = getDomy();
			LDum prvniDum = domy[0];

			// posunuti vsech domu o: -1 (~vlevo)
			for(int i = 1; i < domy.length; i++)
			{
				domy[(i - 1)] = domy[i];
				domy[(i - 1)].setLogickaPozice(
					i - 1,
					domy[(i - 1)].getY()
				);
			}

			int posledniDum = domy.length - 1;
			if (vytvorNovyDum)
			{
				// vytvoreni noveho domu "na konci mesta"
				domy[posledniDum] = vytvorDum(posledniDum);
			}
			else
			{
				// anebo znovu-pouziti 1. domu ve meste, ktery se objevi nakonci mesta
				prvniDum.setLogickaPozice(posledniDum, 0);
				domy[posledniDum] = prvniDum;
			}
			domy[posledniDum].setLogikaHry(handler);

			return (MenuLogickyDum)domy[posledniDum];
		}

		/** 'korekce', kdyz letadlo narazi do domu, aby tam nezustala 'uprostred' dira */
		void onLetadloNaraziloDoDomu(int x, int y)
		{
			LDum[] domy = getDomy();
			if (x < 0 || x >= domy.length)
			{
				return;
			}
			LDum dum = domy[x];
			if (dum == null)
			{
				return;
			}
			if (dum.getVyskaDomu() >= y) // dum je vyssi, nez projektil (~letadlo)
			{
				znicenePatra.clear();
				zbyvajiciPatra.clear();

				ILBlokDomu[] patra = dum.getPatraDomu();
				for(int i = 0; i < patra.length; i++)
				{
					if (patra[i] == LBlokDomu.ZNICEN)
					{
						continue;
					}
					if (i >= y)
					{
						// patra jsou vys nez projektil (~letadlo) => znicit krtka!
						patra[i].znicit(Float.MAX_VALUE);
						znicenePatra.add(patra[i]);
						patra[i] = LBlokDomu.ZNICEN;
					}
					else
					{
						// dole, jsou zbyvajici patra, ktera necham, jak jsou...
						zbyvajiciPatra.add(patra[i]);
					}
				}
				// zaktualizuju domu: zivoty/damage aj.
				((MenuLogickyDum)dum).updateDum(
					zbyvajiciPatra.toArray(new LBlokDomu[0]),
					znicenePatra
				);
			}
		}

		/** diky inner-class, mohu probublat 'kolizeProjektiluADomu' do LMesta */
		@Override
		protected void kolizeProjektiluADomu(ILLeticiProjektil projektil)
		{
			super.kolizeProjektiluADomu(projektil);
		}
	}
}
