package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.common.graphics.screens.IScreen;
import com.gde.luzanky.bombarder.grafika.hra.HerniGrafickeListenery;
import com.gde.luzanky.bombarder.grafika.hra.IGrafickaCast;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi.IObrazovkaDalsiUroven;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;

public interface IObrazovkaHry
extends HerniGrafickeListenery, IObrazovkaDalsiUroven, IScreen<TypObrazovky>
{
	/** najde a vrati actora podle logickeho objektu. vraci null, pokud nic nenajde */
	IGrafickaCast findGrafika(ILogickaCast logickyObjekt);

	/** najde a vrati actora podle logickeho objektu. vraci null, pokud nic nenajde */
	Actor findActor(ILogickeUuid logickeUuid);
}
