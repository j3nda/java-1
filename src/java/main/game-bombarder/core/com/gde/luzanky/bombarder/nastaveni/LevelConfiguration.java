package com.gde.luzanky.bombarder.nastaveni;

/**
 * nastaveni urovne...
 * (prozatim mam 1x level... vyhledove pocitam s tim, ze:
 *  - muzu mit vice typu urovni (jak obtiznosti, tak typu letadel, tak druhu mest)
 *  - nastaveni muzu uchovavat v .json souboru!
 */
public class LevelConfiguration
{
	public final int level;
	public final String nazevMesta;
	public final int pocatecniBody;
	public final float rychlostLetadla;
	public final int pocetZivotuLetadla;
	public final int maximalniPocetPaterDomu;
	public final int maximalniPocetDomu;


	public LevelConfiguration(
		int level,
		String nazevMesta,
		int pocatecniBody,
		float rychlostLetadla,
		int pocetZivotuLetadla,
		int maximalniPocetPaterDomu,
		int maximalniPocetDomu
	)
	{
		this.level = level;
		this.nazevMesta = nazevMesta;
		this.pocatecniBody = pocatecniBody;
		this.rychlostLetadla = rychlostLetadla;
		this.pocetZivotuLetadla = pocetZivotuLetadla;
		this.maximalniPocetPaterDomu = maximalniPocetPaterDomu;
		this.maximalniPocetDomu = maximalniPocetDomu;
	}

	public LevelConfiguration(
		int pocatecniBody,
		LevelConfiguration levelConfiguration
	)
	{
		this(
			levelConfiguration.level,
			levelConfiguration.nazevMesta,
			pocatecniBody,
			levelConfiguration.rychlostLetadla,
			levelConfiguration.pocetZivotuLetadla,
			levelConfiguration.maximalniPocetPaterDomu,
			levelConfiguration.maximalniPocetDomu
		);
	}

	private static final LevelConfiguration prvniLevel = LevelFactory.create(0, 0, 1, null);
	public LevelConfiguration()
	{
		this(
			prvniLevel.level,
			prvniLevel.nazevMesta,
			prvniLevel.pocatecniBody,
			prvniLevel.rychlostLetadla,
			prvniLevel.pocetZivotuLetadla,
			prvniLevel.maximalniPocetPaterDomu,
			prvniLevel.maximalniPocetDomu
		);
	}

	/** staticka metoda jako 'tovarnicka' na vytvoreni nove urovne podle cisla levelu */
	public static LevelConfiguration dalsiUroven(int body, float letadloZivoty, LevelConfiguration predchoziUroven)
	{
		// vsimni si, ze:
		// - ma 'public' viditelnost (~takze ji muzu volat odkudkoliv z projektu)
		// - a ze 'LevelFactory' ma 'package-private' viditelnost (~tj. muzu ji volat pouze z aktualniho 'package')
		return LevelFactory.create(
			body,
			letadloZivoty,
			predchoziUroven.level + 1,
			predchoziUroven
		);
	}
}
