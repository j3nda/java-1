package com.gde.luzanky.bombarder.grafika.hra.mesto;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.luzanky.bombarder.grafika.hra.GrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.IGrafickaCast;
import com.gde.luzanky.bombarder.grafika.hra.mesto.dum.GDum;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.LMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.mesto.dum.LDum;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;

/**
 * graficka cast mesta
 * trida dedi vsechny vlastnosti {@link GrafickaCast}
 *
 * a definuje vlastni graficke chovani
 */
public class GMesto
extends GrafickaCast
{
	protected final List<GDum> domy;


	public GMesto(int maximalniPocetPaterDomu, int maximalniPocetDomu)
	{
		this(
			new LMesto(
				maximalniPocetPaterDomu,
				maximalniPocetDomu
			)
		);
	}

	protected GMesto(ILMesto mesto)
	{
		super(mesto);
		this.domy = new ArrayList<>();
		vytvorMesto();
	}

	private void vytvorMesto()
	{
		ILMesto logickeMesto = (ILMesto) getLogika();
		LDum[] logickeDomy = logickeMesto.getDomy();
		for(int i = 0; i < logickeDomy.length; i++)
		{
			if (logickeDomy[i] != null)
			{
				domy.add(
					vytvorDum(i, logickeDomy[i])
				);
			}
		}
	}

	protected GDum vytvorDum(int pozice, ILDum logickyDum)
	{
		return new GDum(
			logickyDum,
			Color.NAVY
		);
	}

	@Override
	protected void render(Batch batch, float parentAlpha)
	{
		for(IGrafickaCast dum : domy)
		{
			dum.draw(batch, parentAlpha);
		}
	}

	@Override
	public void setGraphicPixelsToLogicBlock(Vector2 graphicPixelsToLogicSize)
	{
		super.setGraphicPixelsToLogicBlock(graphicPixelsToLogicSize);
		for(IGrafickaCast dum : domy)
		{
			dum.setGraphicPixelsToLogicBlock(graphicPixelsToLogicSize);
		}
	}

	@Override
	public Actor findActor(ILogickeUuid logickeUuid)
	{
		Actor actor = super.findActor(logickeUuid);
		if (actor == null)
		{
			for(GDum dum : domy)
			{
				if ((actor = dum.findActor(logickeUuid)) != null)
				{
					break;
				}
			}
		}
		return actor;
	}
}
