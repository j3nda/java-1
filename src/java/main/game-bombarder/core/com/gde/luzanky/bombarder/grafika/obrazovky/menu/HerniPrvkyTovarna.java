package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.display.CelkoveScore;
import com.gde.luzanky.bombarder.BombarderScreenResources;
import com.gde.luzanky.bombarder.grafika.hra.letadlo.GLetadlo;
import com.gde.luzanky.bombarder.grafika.hra.mesto.GMesto;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.IHerniPrvkyTovarna;
import com.gde.luzanky.bombarder.grafika.obrazovky.menu.MenuMesto.MenuLogickeMesto;
import com.gde.luzanky.bombarder.logika.hra.HerniLogickeListeneryAdapter;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;

class HerniPrvkyTovarna
extends com.gde.luzanky.bombarder.grafika.obrazovky.hra.HerniPrvkyTovarna
implements IHerniPrvkyTovarna
{

	HerniPrvkyTovarna(BombarderScreenResources resources)
	{
		super(resources);
	}

	@Override
	public GLetadlo createLetadlo()
	{
		MenuLetadlo letadlo = new MenuLetadlo(
			levelConfiguration.rychlostLetadla,
			levelConfiguration.pocetZivotuLetadla
		);
		letadlo.getLogika().setLogikaHry(logikaHandler);
		return letadlo;
	}

	@Override
	public GMesto createMesto()
	{
		MenuMesto mesto = new MenuMesto(
			levelConfiguration.maximalniPocetPaterDomu,
			levelConfiguration.maximalniPocetDomu
		);
		mesto.getLogika().setLogikaHry(logikaHandler);

		// zaregistruju listener, kdyz letadlo narazi do domu,
		// abych udelal korekci ~ tj. v dome nebyla 'uprostred' dira - a vypadalo to pekne
		// bloky-domu od narazu nahoru se automaticky zbori
		logikaHandler.addLogicListener(new HerniLogickeListeneryAdapter()
		{
			@Override
			public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
			{
				if (projektil instanceof ILLetadlo)
				{
					((MenuLogickeMesto)mesto.getLogika()).onLetadloNaraziloDoDomu(
						projektil.getX(),
						projektil.getY()
					);
				}
			}
		});
		return mesto;
	}

	@Override
	public CelkoveScore createCelkoveBody(int startovniBody, Color barva)
	{
		return new NapisBombarder(
			resources.getFontsManager().getBitFontConfiguration(),
			barva,
			"bombarder"
		);
	}
}
