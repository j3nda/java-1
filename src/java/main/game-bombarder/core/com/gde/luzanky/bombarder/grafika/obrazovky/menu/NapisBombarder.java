package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.display.CelkoveScore;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

/**
 * vlastni implementace {@link CelkoveScore},
 * tak abych v {@link ObrazovkaMainMenu} mohl zobrazit napis, namisto score!
 */
class NapisBombarder
extends CelkoveScore
{
	private String text;

	public NapisBombarder(BitFontMaker2Configuration font, Color color, String text)
	{
		this(font, text.length(), color, Color.BLACK);
		this.text = text;
		nastavScoreJakoText(0);
	}

	protected NapisBombarder(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor,
		Color bgColor
	)
	{
		super(fontConfiguration, length, fgColor, bgColor);
	}

	@Override
	protected void nastavScoreJakoText(int score)
	{
		if (text == null)
		{
			return;
		}
		for(int i = 0; i < text.length(); i++)
		{
			updateLetter(i, text.charAt(i));
		}
	}
}
