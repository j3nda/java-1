package com.gde.luzanky.bombarder.logika.mesto.dum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;
import com.gde.luzanky.bombarder.logika.bomba.LBomba;
import com.gde.luzanky.bombarder.logika.letadlo.LLetadlo;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.strela.ILStrela;
import com.gde.luzanky.bombarder.logika.strela.LStrela;

/**
 * logicka cast celeho domu
 * trida dedi vsechny vlastnosti {@link LBlokDomu}
 *
 * 1x dum se sklada z nekolika pater (~bloku),
 * kazdy blok by mel mit zivoty - aby jej nezborilo pouze 1x spusteni bomby...
 * (diky dedeni z LBlokDomu mam atributy jako: zivoty a damage)
 */
public class LDum
extends LBlokDomu
implements ILDum
{
	/** konstanta, reprezentujici zniceny dum */
	public static final ILDum ZNICEN = null;
	private final ILBlokDomu[] patraDomu;
	private int vyskaDomu;


	public LDum(int vyskaDomu)
	{
		super(-1, null);
		this.dum = this;
		this.patraDomu = new LBlokDomu[vyskaDomu];
		this.vyskaDomu = vyskaDomu;
		vytvorDum(100f);
	}

	private void vytvorDum(float zivotBlokuDomu)
	{
		zivoty = 0;
		for(int i = 0; i < vyskaDomu; i++)
		{
			patraDomu[i] = new LBlokDomu(zivotBlokuDomu, this);
			zivoty += patraDomu[i].getZivoty();
		}
	}

	@Override
	public int getVyskaDomu()
	{
		return vyskaDomu;
	}

	@Override
	public ILBlokDomu[] getPatraDomu()
	{
		return patraDomu;
	}

	@Override
	public void znicit(ILLeticiProjektil projektil)
	{
		if (projektil.getY() >= getVyskaDomu())
		{
			return;
		}

		if (projektil instanceof LBomba)
		{
			// TODO: ukol/LOGIKA: znic dum bombu (minimalne 1x dilek bloku-domu)
			znicProjektilemDolu(projektil);
		}
		else
		if (projektil instanceof LStrela)
		{
			// TODO: ukol/LOGIKA: znic 1x blok domu
			znicProjektilemVpravo((ILStrela) projektil);
		}
		else
		if (projektil instanceof LLetadlo)
		{
			// TODO: ukol/LOGIKA: znic dum letadlem (pocet dilku: podle typu letadla)
			znicProjektilemNahoru(projektil);
			znicProjektilemDolu(
				projektil,
				new float[] { 1.0f, 0.5f }
			);
		}
	}

	/** zaktualizuji celkove zivoty a damage celeho domu; podle zbylych a znicenych bloku-domu */
	protected void updateDum(ILBlokDomu[] zbyleBlokyDomu, List<ILBlokDomu> zniceneBlokyDomu)
	{
		zivoty = 0;
		damage = 0;

		// vytvorim si sjednocenou kolekci
		List<ILBlokDomu> blokyDomu = new ArrayList<>(Arrays.asList(zbyleBlokyDomu));
		blokyDomu.addAll(zniceneBlokyDomu);

		// iteruju nad vsim, co neni null...
		for(ILBlokDomu blokDomu : blokyDomu)
		{
			if (blokDomu == LBlokDomu.ZNICEN)
			{
				continue;
			}
			// aktualizuju zivoty a damage (pro cely dum)
			zivoty += blokDomu.getZivoty();
			damage += blokDomu.getDamage();
		}

		// aktualizuju vysku domu
		vyskaDomu = 0;
		for(ILBlokDomu blokDomu : patraDomu)
		{
			if (blokDomu == LBlokDomu.ZNICEN)
			{
				continue;
			}
			vyskaDomu++;
		}

		// informuju, kdyz je cely dum znicen
		if (vyskaDomu == 0)
		{
			handler.onDumJeKompletneZnicen(this);
		}
	}

	/**
	 * znici bloky domu projektilem; s moznosti "nasobitele" vybuchu smerem dolu
	 *
	 * @param projektil
	 * @param damageMultiplierDown velikost vybuchu; velikost pole udava, kolik dilu se znici "dolu"; hodnoty potom nasobitele zivotu jako damage!
	 * @return
	 */
	private void znicProjektilemDolu(ILLeticiProjektil projektil, float[] damageMultiplierDown)
	{
		int projektilY = projektil.getY();
		List<ILBlokDomu> zniceneBlokyDomu = new ArrayList<>();
		for(int i = 0; i < damageMultiplierDown.length; i++)
		{
			if (projektilY < 0)
			{
				// uz neni kam padat... preskakuju!
				continue;
			}
			ILBlokDomu blokDomu = patraDomu[projektilY];
			if (blokDomu != LBlokDomu.ZNICEN)
			{
				// nad znicenym domem uberu zivoty podle nasobice a pridam ho do poskozenych
				blokDomu.znicit(
					// mam co nicit... pocitam damage vc. nasobitele
					blokDomu.getZivoty() * damageMultiplierDown[i]
				);

				// podle typu poskozeni - provedu logiku
				switch(blokDomu.getPoskozeni())
				{
					case Castecne:
					{
						zniceneBlokyDomu.add(blokDomu);
						break;
					}
					case Kompletne:
					{
						zniceneBlokyDomu.add(blokDomu);
						patraDomu[projektilY] = LBlokDomu.ZNICEN; // pokud je blok-domu kompletne zniceny, smazu ho
						break;
					}
					default:
						// vzdy se slusi uvest "default"!
						break;
				}
			}
			// projektil pada dolu, tj. prechazim o 1x uroven niz
			projektilY--;
		}

		// provedu logiku, kdyz projektil opravdu narazil a znicil bloky-domu...
		if (zniceneBlokyDomu.size() > 0)
		{
			// informuju, ze projektil naboural do domu
			// a predam jednotlive bloky-domu jako LBlokDomu[] pole!
			// (~abych nemusel slozite resit nested-iterator; popr. kopirovat koleci znovu a znovu)
			handler.onProjektilExplodovalDoDomu(
				projektil,
				zniceneBlokyDomu.toArray(new LBlokDomu[zniceneBlokyDomu.size()])
			);

			// aktualizuji: zivoty vs damage celeho domu
			updateDum(patraDomu, zniceneBlokyDomu);
		}
	}

	/** znici bloky domu projektilem smerem dolu */
	private void znicProjektilemDolu(ILLeticiProjektil projektil)
	{
		int projektilY = projektil.getY();
		float projektilDamage = projektil.getRozsahSkod();
		List<ILBlokDomu> zniceneBlokyDomu = new ArrayList<>();
		while(
			   projektilY >= 0 // uz neni kam padat...
			&& projektilDamage > 0 // mam 'silu unagi' a muzu nicit...
			&& patraDomu[projektilY] != LBlokDomu.ZNICEN // blok-domu neni znicen...
		)
		{
			ILBlokDomu blokDomu = patraDomu[projektilY];

			// nad znicenym domem uberu zivoty podle nasobice a pridam ho do poskozenych
			blokDomu.znicit(projektilDamage);

			// snizim poskozeni 'o naakumulovany damage'
			projektilDamage -= blokDomu.getDamage();

			// podle typu poskozeni - provedu logiku
			switch(blokDomu.getPoskozeni())
			{
				case Castecne:
				{
					zniceneBlokyDomu.add(blokDomu);
					break;
				}
				case Kompletne:
				{
					zniceneBlokyDomu.add(blokDomu);
					patraDomu[projektilY] = LBlokDomu.ZNICEN; // pokud je blok-domu kompletne zniceny, smazu ho
					break;
				}
				default:
					// vzdy se slusi uvest "default"!
					break;
			}
			// projektil pada dolu, tj. prechazim o 1x uroven niz
			projektilY--;
		}

		// provedu logiku, kdyz projektil opravdu narazil a znicil bloky-domu...
		if (zniceneBlokyDomu.size() > 0)
		{
			// aktualizuji: zivoty vs damage celeho domu
			updateDum(patraDomu, zniceneBlokyDomu);

			// informuju, ze projektil naboural do domu
			// a predam jednotlive bloky-domu jako LBlokDomu[] pole!
			// (~abych nemusel slozite resit nested-iterator; popr. kopirovat koleci znovu a znovu)
			handler.onProjektilExplodovalDoDomu(
				projektil,
				zniceneBlokyDomu.toArray(new LBlokDomu[zniceneBlokyDomu.size()])
			);
		}
	}

	/**
	 * znici bloky domu projektilem smerem nahoru
	 * (vhodne k osetreni situace: ze projektil ztrati vysku -> a zbydou tam blokyDomu...)
	 */
	private void znicProjektilemNahoru(ILLeticiProjektil projektil)
	{
		List<ILBlokDomu> zniceneBlokyDomu = new ArrayList<>();
		for(int i = projektil.getY() + 1; i < vyskaDomu; i++)
		{
			ILBlokDomu blokDomu = patraDomu[i];
			blokDomu.znicit(blokDomu.getZivoty());
			switch(blokDomu.getPoskozeni())
			{
				case Kompletne:
				{
					zniceneBlokyDomu.add(blokDomu);
					patraDomu[i] = LBlokDomu.ZNICEN;
					break;
				}
				case Castecne:
				default:
					throw new RuntimeException("Invalid damage!");
			}
			zniceneBlokyDomu.add(blokDomu);
		}
		if (zniceneBlokyDomu.size() > 0)
		{
			updateDum(patraDomu, zniceneBlokyDomu);
			handler.onProjektilExplodovalDoDomu(
				projektil,
				zniceneBlokyDomu.toArray(new LBlokDomu[zniceneBlokyDomu.size()])
			);
		}
	}

	/**
	 * znici bloky domu projektilem smerem vpravo
	 * a projektil utrpi damage (~snizi se rozsah skod)
	 */
	private void znicProjektilemVpravo(ILStrela projektil)
	{
		ILBlokDomu blokDomu = patraDomu[projektil.getY()];
		if (blokDomu == LBlokDomu.ZNICEN || projektil.getRozsahSkod() < 0)
		{
			return;
		}

		// abych zjistil na-akumulovany damage, musim vedet hodnotu na zacatku
		float zivotyPred = blokDomu.getZivoty();

		// znicim blok domu silou projektilu
		blokDomu.znicit(projektil.getRozsahSkod());

		// snizim rozsah skod projektilu o 'zivoty' bloku domu, ktere ubral
		// (aby projektil nemel stale stejny rozsah skod ~ a nenicil uplne vsechno, co mu prijde do cesty)
		projektil.uberRozsahSkod(
			zivotyPred - blokDomu.getZivoty(),
			blokDomu
		);

		// pokud je uplne 'blok-domu' zniceny: pre-nastavim, ze je znicen
		// 'blokDomu' obsahuje 'pointer' na pole: 'patraDomu[projektil.getY()]'
		if (blokDomu.getPoskozeni() == TypPoskozeni.Kompletne)
		{
			patraDomu[projektil.getY()] = LBlokDomu.ZNICEN;
		}
	}

	@Override
	public void act(float delta)
	{
		for(ILBlokDomu blokDomu : patraDomu)
		{
			if (blokDomu == LBlokDomu.ZNICEN)
			{
				continue;
			}
			blokDomu.act(delta);
		}
	}

	@Override
	public void znicit(float damage)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void setLogikaHry(ILogikaHryHandler logikaHandler)
	{
		super.setLogikaHry(logikaHandler);

		// 'probublam' nastaveni na jednotlive bloky-domu
		for(ILBlokDomu blokDomu : patraDomu)
		{
			blokDomu.setLogikaHry(logikaHandler);
		}
	}

	@Override
	protected void onPositionChanged()
	{
		super.onPositionChanged();

		// musim zaridit, ze i jednotlive blokyDomu (~patra) upravi svou pozici podle pozice celeho domu!
		for(int i = 0; i < vyskaDomu; i++)
		{
			ILBlokDomu blokDomu = patraDomu[i];
			if (blokDomu == LBlokDomu.ZNICEN)
			{
				continue;
			}
			blokDomu.setLogickaPozice(
				getX(),
				getY() + i
			);
		}
	}
}
