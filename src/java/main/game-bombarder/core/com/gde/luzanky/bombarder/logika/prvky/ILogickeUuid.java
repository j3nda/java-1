package com.gde.luzanky.bombarder.logika.prvky;

public interface ILogickeUuid
{
	/** vrati unikatni identifikator objektu */
	Integer getUuid();
}
