package com.gde.luzanky.bombarder.grafika.obrazovky.hra.konec;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.gde.luzanky.bombarder.BombarderGame;
import com.gde.luzanky.bombarder.grafika.obrazovky.SdilenaObrazovka;
import com.gde.luzanky.bombarder.grafika.obrazovky.TypObrazovky;
import com.gde.luzanky.bombarder.grafika.obrazovky.hra.dalsi.IObrazovkaDalsiUroven;
import com.gde.luzanky.bombarder.grafika.obrazovky.menu.ObrazovkaMainMenu;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;
import com.gde.luzanky.bombarder.zvuky.TypZvuku;

/** obrazovka pro uplny konec hry */
public class ObrazovkaKonecHry
extends SdilenaObrazovka
implements IObrazovkaDalsiUroven
{
	private KonecHryDisplay gameOver;
	private final Color gameOverColor;
	private LevelConfiguration levelConfiguration;
	private int celkoveBody;


	public ObrazovkaKonecHry(BombarderGame parentGame, TypObrazovky previousScreenType, Color gameOverColor)
	{
		super(parentGame, previousScreenType);
		this.gameOverColor = gameOverColor;
	}

	@Override
	public void show()
	{
		super.show();
		setAlphaBackground(0.3f);

		// vytvorim napis 'game-over'
		gameOver = createGameOver(gameOverColor, new String[] { "GAME", "OVER" });
		gameOver.efektVlnka(0, 1.5f, 5f);

		// TODO: xhonza: backButton -> do menu

		// vytvorim 'tapActor', abych mohl odchytit tapnuti na obrazovku,
		// tj. neviditelny actor pres celou obrazovku a pridani listenera
		// (tap-listener nemuzu to pridat na gameOver ~ pze tam mam efektVlnka [a ze zahadneho duvodu mi to neodchytava tapEventy!])
		Actor tapActor = new Actor();
		tapActor.setSize(getWidth(), getHeight());
		tapActor.setPosition(0, 0);
		tapActor.addListener(new ActorGestureListener()
		{
			private boolean firstTap = false;
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				if (!firstTap)
				{
					firstTap = true;
					// TODO: xhonza: tap kdekoliv -> is hi-score? [yes: goto leaderboard // no: goto main menu]
					setNextScreen();
				}
			}
		});

		getStage().addActor(gameOver);
		getStage().addActor(tapActor);

		// TODO: pimp/zvuky: muzes prehrat zvuk pro konec hry
		resources.getSoundsManager().play(TypZvuku.KonecHry);
	}

	/** vytvori display pro zobrazeni 'GAME OVER' textu */
	private KonecHryDisplay createGameOver(Color color, String[] text)
	{
		KonecHryDisplay display = new KonecHryDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			text,
			color
// TODO: xhonza: nejak efektne mikro-radek po mikro-radku zmenit barvu textu (gradienty cervene)
//				Color.RED,
//				Color.FIREBRICK,
//			}
//			new Color(0xff000011)
//			color
		);
		display.setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
			)
		);
		return display;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.KONEC_HRY;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderBackground();
		renderScreenStage(delta);
	}

	@Override
	public void setLevelConfiguration(LevelConfiguration levelConfiguration)
	{
		this.levelConfiguration = levelConfiguration;
	}

	@Override
	protected void createInputProcessors(List<InputProcessor> inputs)
	{
		// zaregistruju 'back-button' input-processor ~ zavolanim rodice!
		super.createInputProcessors(inputs);

		// a pridam vlastni input-processor na mackani klaves
		// 'ENTER' nebo 'MEZERA' me posle na nasl. obrazovku
		inputs.add(
			new InputAdapter()
			{
				@Override
				public boolean keyUp(int keycode)
				{
			        switch (keycode)
			        {
						case Keys.ENTER:
						case Keys.SPACE:
						{
							setNextScreen();
							return true;
						}
						default:
							return false;
			        }
				}
			}
		);
	}

	private void setNextScreen()
	{
		parentGame.setScreen(
			new ObrazovkaMainMenu(
				parentGame,
				null // main-menu nema predchozi obrazovku!
			)
		);
	}
}
