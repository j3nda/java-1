package com.gde.luzanky.bombarder.grafika.hra;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.common.graphics.display.pixels.ColorPixel;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;
import com.gde.luzanky.bombarder.logika.prvky.ILogickeUuid;

/**
 * sdilena graficka cast: ze ktere dedi kazdy graficky objekt!
 * - vsechno co je ve hre ma svou grafickou souradnici (x, y)
 *   - kterou vraci: getX(), getY() ~ jako cele realne cislo float
 *   - anebo nastavuje: setPozice()
 *
 * - obsahuje metodu "act()" ktera ovlada grafiku (~napr: animace)
 * - GRAFIKA BY MELA VEDET O LOGICE A GRAFICKY REFLEKTOVAT JEJI STAVY!
 *
 * slovicko "abstract" zde vynucuje, ze nekdo musi tuto tridu zdedit,
 * tj. sdilet vsechny vlastnosti zde uvedene!
 */
public abstract class GrafickaCast
extends Actor
implements IGrafickaCast
{
	public static final String UUID_PREFIX = "guid-";
	/** logicky objekt */
	protected ILogickaCast logickaCast;
	/** chci znat posledni pozici logiky, abych mohl 'automaticky' nastavit pozici 'grafiky' podle logiky! */
	private final Vector2 logickaCastPosledniPozice = new Vector2();
	/** velikost 1x logicke policko mapovane na pocet pixelu v grafice */
	protected final Vector2 graphicToLogicSize;
	/** vykresleni "ctverecku" (~prozatim DRY ze hry hangman); vykresleni textury; prechodne reseni... */
	private ColorPixel colorPixel;
	private Color colorPixelColor;
	/**
	 * trochu overkill mechanismus!@#$%^&*()
	 * (~protoze ne uplne vsechno mam ve stage a chci pouzivat '.remove()' metodu, tak si ji doimplementuju...)
	 */
	protected boolean isActorRemoved;

	public GrafickaCast(ILogickaCast logickaCast)
	{
		this.graphicToLogicSize = new Vector2(1, 1);
		this.colorPixel = null;
		this.colorPixelColor = Color.WHITE;
		this.isActorRemoved = false;
		super.setName(UUID_PREFIX + logickaCast.getUuid());
		setLogika(logickaCast);
	}

	@Override
	public void setName(String name)
	{
		// do nothing, just use: getLogika().getUuid() to search object in list/scene/whatever...
	}

	protected void setLogika(ILogickaCast logickaCast)
	{
		this.logickaCast = logickaCast;
	}

	@Override
	public ILogickaCast getLogika()
	{
		return logickaCast;
	}

	/** vykresleni grafiky */
	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (isActorRemoved)
		{
			return;
		}
		positionChanged(logickaCast);
		beforeRender(batch, parentAlpha);
		render(batch, parentAlpha);
		afterRender(batch, parentAlpha);
	}

	protected void beforeRender(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
	}

	protected void render(Batch batch, float parentAlpha)
	{
		if (colorPixel == null)
		{
			return;
		}
		colorPixel.draw(batch, parentAlpha);
	}

	protected void afterRender(Batch batch, float parentAlpha)
	{

	}

	@Override
	public void setGrafickaPozice(float x, float y)
	{
		// protoze dedim z LibGdx/Actor, tak znasilnuju "cze" -> "eng"
		// a volam adekvatni metodu pro nastaveni graficke pozice
		super.setPosition(x, y);
	}

	protected void positionChanged(ILogickaCast logickaCast)
	{
		if (logickaCast == null)
		{
			return;
		}
		// mam 'logickaCast', tj. herni objekt a chci 'automaticky' nastavit pozici grafiky podle logiky!
		if (
			   logickaCast.getX() != logickaCastPosledniPozice.x
			|| logickaCast.getY() != logickaCastPosledniPozice.y
		   )
		{
			logickaCastPosledniPozice.x = logickaCast.getX();
			logickaCastPosledniPozice.y = logickaCast.getY();
			setGrafickaPozice(
				(logickaCastPosledniPozice.x * graphicToLogicSize.x),
				(logickaCastPosledniPozice.y * graphicToLogicSize.y)
			);
		}
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		if (colorPixel == null)
		{
			onGraphicPixelToLogicBlockChanged();
		}
		if (colorPixel != null)
		{
			colorPixel.setPosition(getX(), getY());
		}
	}

	@Override
	public void act(float delta)
	{
		if (isActorRemoved)
		{
			return;
		}
		super.act(delta);
		if (logickaCast != null)
		{
			actLogika(delta);
		}
	}

	/**
	 * provede vykonani logiky,
	 * pokud graficka cast neni odebrana anebo nijak zmenozneno provedeni logiky...
	 */
	protected void actLogika(float delta)
	{
		logickaCast.act(delta);
		setGrafickaPozice(
			logickaCast.getX() * graphicToLogicSize.x,
			logickaCast.getY() * graphicToLogicSize.y
		);
	}

	/** uvolnuje zdroje z pameti (~napr: alokovane textury, fonty, aj.) */
	@Override
	public void dispose()
	{
		if (colorPixel != null)
		{
			colorPixel.dispose();
		}
	}

	/**
	 * nastavi velikost 1x logicke kosticky na 1x grafickou kosticku.
	 * jedna se o prepocet: logika[int] => grafika[pixely]
	 */
	@Override
	public void setGraphicPixelsToLogicBlock(Vector2 graphicPixelsToLogicSize)
	{
		boolean graphicPixelsToLogicSizeChanged = false;
		if (
			   this.graphicToLogicSize.x != graphicPixelsToLogicSize.x
			|| this.graphicToLogicSize.y != graphicPixelsToLogicSize.y
		   )
		{
			graphicPixelsToLogicSizeChanged = true;
		}
		this.graphicToLogicSize.x = graphicPixelsToLogicSize.x;
		this.graphicToLogicSize.y = graphicPixelsToLogicSize.y;
		if (graphicPixelsToLogicSizeChanged)
		{
			onGraphicPixelToLogicBlockChanged();
		}
	}

	protected void onGraphicPixelToLogicBlockChanged()
	{
		// TODO: xhonza: refactor! (~size matters?)
		if (colorPixel != null)
		{
			colorPixel.dispose();
		}
		colorPixel = new ColorPixel(
			(int)graphicToLogicSize.x,
			(int)graphicToLogicSize.y,
			colorPixelColor
		);
	}

	@Override
	public boolean remove()
	{
		isActorRemoved = true;
		return super.remove();
	}

	@Override
	public void setColor(Color color)
	{
		boolean colorChanged = (colorPixelColor != color);
		colorPixelColor = color;
		if (colorChanged)
		{
			onGraphicPixelToLogicBlockChanged();
		}
	}

	@Override
	public Actor findActor(ILogickeUuid logickeUuid)
	{
		return ((UUID_PREFIX + logickeUuid.getUuid()).equals(getName())
			? this
			: null
		);
	}
}
