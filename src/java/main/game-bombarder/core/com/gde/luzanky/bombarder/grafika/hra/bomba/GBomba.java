package com.gde.luzanky.bombarder.grafika.hra.bomba;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.gde.luzanky.bombarder.grafika.hra.GLeticiProjektil;
import com.gde.luzanky.bombarder.logika.bomba.LBomba;

/**
 * graficka cast bomby dolu
 * trida dedi vsechny vlastnosti {@link GLeticiProjektil}
 *
 * a definuje vlastni graficke chovani
 */
public class GBomba
extends GLeticiProjektil
{
	public GBomba(float rychlost, float rozsahSkod, int dolet)
	{
		super(
			new LBomba(rychlost, rozsahSkod, dolet)
		);
		setColor(Color.OLIVE);
	}

	/** nastavim, kam se pohybuju */
	@Override
	protected Vector2 getMovingVector()
	{
		// dolu o 1x policko
		return new Vector2(0, -1);
	}
}
