package com.gde.luzanky.bombarder.logika.hra;

import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

/** handluje "logiku skore", abych mohl mit vice druhu letadel, mesta i obtiznosti */
class LogikaScore
{
	/** typ bodu a jeho ohodnoceni */
	enum ScoreTyp
	{
		LetadloZtratiloZivoty(-100),
		LetadloStrili(5),
		LetadloNabouraloDoZeme(-1000),
		ProjektilExplodovalNaprazdno(4),
		ProjektilExplodovalDoDomu(50),
		;
		private final int body;
		ScoreTyp(int body)
		{
			this.body = body;
		}
	}
	private final LevelConfiguration levelConfiguration;


	LogikaScore(LevelConfiguration levelConfiguration)
	{
		this.levelConfiguration = levelConfiguration;
	}

	int getScore(ScoreTyp typ)
	{
		// TODO: zatim takto jednoduse (~potom: zohlednit damage; popr. cetnost; a levelConfiguration aj)
		return typ.body;
	}
}
