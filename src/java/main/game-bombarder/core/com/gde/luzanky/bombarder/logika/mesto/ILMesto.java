package com.gde.luzanky.bombarder.logika.mesto;

import com.gde.luzanky.bombarder.logika.mesto.dum.LDum;
import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

/** interfejs (~rozhranni) pro logickou cast mesta */
public interface ILMesto
extends ILogickaCast
{
	LDum[] getDomy();
	int getMaximalniPocetDomu();
	int getMaximalniPocetPaterDomu();
	int getAktualniPocetDomu();
}
