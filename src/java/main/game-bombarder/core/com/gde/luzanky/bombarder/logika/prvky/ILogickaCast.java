package com.gde.luzanky.bombarder.logika.prvky;

import com.gde.luzanky.bombarder.grafika.obrazovky.hra.ILogikaHryHandler;

/**
 * interfejs (~rozhranni),
 * ktere definuje chovani logicke casti
 */
public interface ILogickaCast
extends ILogickaPozice, ILogickeUuid
{
	/**
	 * vrati [0 - 1.0] podle toho kolik zbyva do dalsi logicke akce
	 * (~ma to vyuziti v grafice, aby se prvky hybali na obrazovce plynule)
	 */
	float getRemainingToNextLogicAction();

	/** nastavim 'logicky handler', ktery informuje vsechny 'posluchace' o logickych udalostech ve hre */
	void setLogikaHry(ILogikaHryHandler logikaHandler);

	/* from LiGdx */
	void act(float delta);
}
