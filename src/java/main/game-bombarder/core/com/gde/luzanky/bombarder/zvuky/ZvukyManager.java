package com.gde.luzanky.bombarder.zvuky;

import java.util.EnumMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class ZvukyManager
implements IZvukyManager
{
	/**
	 * cache, jako mapa pro ulozeni {@link TypZvuku} a konkretni instance {@link Sound}.
	 * - cache jsem zvolil z duvodu, abych neustale nedelal loading zvuku...
	 * - je to 'static', protoze {@link TypZvuku} je nemenny 'enum' a mohu vse mit pouze 1x!
	 */
	private static final Map<TypZvuku, Sound> cache = new EnumMap<TypZvuku, Sound>(TypZvuku.class);
	/**
	 * abych byl schopen synchronizovat data mezi thready -> musim zvolit nejaky spolecny objekt,
	 * ktery pouziju jako 'zamek' pro synchronizaci dat
	 */
	private final IZvukyManager synchronizacniMonitorProVlaknoCoNacitaZvuky = this;


	public ZvukyManager()
	{
		// protoze nacitani 'assets' ma nejakou rezii, udelam to asynchronne!
		Thread nacitaciThread = nactiZvukAsynchronne(TypZvuku.values());
		nacitaciThread.start();
	}

	/** metoda pro asynchronni nacitani zvuku; kdyz nejsou nactene */
	private Thread nactiZvukAsynchronne(TypZvuku... typZvuku)
	{
		Thread vlaknoProNacitaniZvuku = new Thread()
		{
			@Override
			public void run()
			{
				// iteruju nad predanymi TypZvuku
				for(TypZvuku typ: typZvuku)
				{
					// pokud TypZvuku nema zadny resource anebo je uz nacteny,
					// -> tak: nic nenacitam a preskakuju
					if (
						   typ.toResourceFilename() == null
						|| typ.toResourceFilename().isEmpty()
						|| isLoaded(typ)
					   )
					{
						continue;
					}

					// vypisu, co nacitam a nactu zvuk
					System.out.println(getClass().getName() + ": load(" + typ + ")");
					Sound zvuk = Gdx.audio.newSound(
						Gdx.files.internal(
							typ.toResourceFilename()
						)
					);

					// kdyz mam nacteno, musim ulozit do 'cache'
					// a mam to asynchronni, tak musim udelat 'synchronizacni-blok'!
					synchronized (synchronizacniMonitorProVlaknoCoNacitaZvuky)
					{
						cache.put(typ, zvuk);
					}
				}
			}
		};
		return vlaknoProNacitaniZvuku;
	}

	/** metoda, ktera prehraje zvuk */
	@Override
	public void play(TypZvuku zvuk)
	{
		// pokud neni nacteno -> pokusim se o nacteni...
		if (!isLoaded(zvuk))
		{
			if (zvuk.toResourceFilename() == null || zvuk.toResourceFilename().isEmpty())
			{
				// pokud zvuk nema 'resource', vim, ze ho nemam nacteny -> preskakuju...
				return;
			}
			try
			{
				// jinak nactu zvuk; opet asynchronne
				Thread nacitaciThread = nactiZvukAsynchronne(zvuk);
				nacitaciThread.start(); // nastartuju...
				nacitaciThread.join();  // ...pockam, dokud thread nedobehne
			}
			catch (Exception e)
			{
				// muze to vyhodit vyjimku, tak ji osetrim...
				System.out.println(getClass().getName() + ": play(" + zvuk + "); WARNING: thread interrupted!");
			}

			// zkontroluju, ze mam spravne nacteno
			// a pokud ne -> tak pri 'do-nacitani' se stala chyba -> WARNING a return (~konec)
			if (!isLoaded(zvuk))
			{
				System.out.println(getClass().getName() + ": play(" + zvuk + "); WARNING: re-load error!");
				return;
			}
		}
		Sound sfx = getSound(zvuk);
		if (sfx != null)
		{
			// pokud mam zvuk -> prehravam...
			System.out.println(getClass().getName() + ": play(" + zvuk + ")");
			sfx.play();
		}
	}

	/** vrati zvuk z cache a pocita z asynchronitou, tj. metoda je 'synchronized' */
	@Override
	public synchronized Sound getSound(TypZvuku zvuk)
	{
		return cache.get(zvuk);
	}

	/**
	 * vratim dostupnost zvuku, tj. zda je nacteny anebo ne.
	 * (protoze mam asynchronni nacitani a uvolnovani, tak musim zajistit synchronizovany pristup k datum)
	 */
	private synchronized boolean isLoaded(TypZvuku typZvuku)
	{
		return (cache.get(typZvuku) != null);
	}


	@Override
	public void dispose()
	{
		dispose(
			cache.keySet().toArray(
				new TypZvuku[cache.keySet().size()]
			)
		);
	}

	@Override
	public void dispose(TypZvuku... zvuk)
	{
		for(TypZvuku typ : zvuk)
		{
			synchronized (synchronizacniMonitorProVlaknoCoNacitaZvuky)
			{
				Sound sfx = cache.get(typ);
				if (sfx != null)
				{
					sfx.dispose();
				}
				cache.put(typ, null);
			}
		}
	}
}
//--https://gamefromscratch.com/libgdx-tutorial-8-audio/
//Gdx.audio.newSound(Gdx.files.internal("data/wav.wav"));
//wavSound.play();
//oggSound.play(0.5f);//volume 50%
//------------------------------------------loop
//long id = mp3Sound.loop();
//Timer.schedule(new Task(){
//   @Override
//   public void run(){
//      mp3Sound.stop(id);
//      }
//   }, 5.0f);
//------------------------------------------dispose
//wavSound.dispose();
//oggSound.dispose();
//mp3Sound.dispose();
//------------------------------------------pitch
//long id = wavSound.play();
//wavSound.setPitch(id,0.5f);
//------------------------------------------volume
//long id = wavSound.play();
//wavSound.setVolume(id,1.0f);
//------------------------------------------pan
//long id = wavSound.play();
//wavSound.setPan(id, 1f, 1f);
//------------------------------------------music
//Music mp3Music = Gdx.audio.newMusic(Gdx.files.internal("data/RideOfTheValkyries.mp3"));
//mp3Music.play();
//------------------------------------------example
//Music mp3Music = Gdx.audio.newMusic(Gdx.files.internal("data/RideOfTheValkyries.mp3"));
//mp3Music.play();
//mp3Music.setVolume(1.0f);
//mp3Music.pause();
//mp3Music.stop();
//mp3Music.play();
//Gdx.app.log("SONG",Float.toString(mp3Music.getPosition()));
