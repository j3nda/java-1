package com.gde.luzanky.bombarder.logika.letadlo;

import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.logika.prvky.LLeticiProjektil;

/**
 * logicka cast letadla
 * trida dedi vsechny vlastnosti {@link LLeticiProjektil}
 * (protoze kdyz letadlo narazi, je to vlastne projektil s destrukci)
 *
 * a definuje vlastni chovani, tj. leti, strili, aj...
 */
public class LLetadlo
extends LLeticiProjektil
implements ILLetadlo
{
	private float zivoty;


	public LLetadlo(float rychlost, float zivoty)
	{
		super(rychlost, 0, -1);
		this.zivoty = zivoty;
	}

	@Override
	public float getZivoty()
	{
		return zivoty;
	}

	@Override
	public void uberZivoty(float damage)
	{
		if (zivoty < 0)
		{
			// nekonecno zivotu...
			return;
		}
		zivoty -= damage;
		// TODO: ukol/LOGIKA: pokud letadlo nema zivoty, melo by explodovat
		// TODO: ukol/LOGIKA: pokud mam stale nejake zivoty, mel bych o tom informovat...
		if (zivoty <= 0)
		{
			zivoty = 0;
			handler.onProjektilExplodovalNaprazdno(this);
		}
		else
		{
			handler.onLetadloZtratiloZivoty(this, damage);
		}
	}

	@Override
	protected boolean pohniSeRychlosti()
	{
		setLogickaPozice(getX() + 1, getY());
		return true;
	}

	@Override
	public boolean vystrel(ILLeticiProjektil projektil)
	{
		// TODO: ukol/HRA: zarid, aby na 1x radku sli vystrelit pouze (2x strely vpred; a 1x bomba dolu)
		//       (kdyz strela zbori BlokDomu anebo vyleti z mesta, muzes tento citac vynulovat)
		projektil.vystrel(this, this);
		handler.onLetadloStrili(projektil);
		return true;
	}
}
