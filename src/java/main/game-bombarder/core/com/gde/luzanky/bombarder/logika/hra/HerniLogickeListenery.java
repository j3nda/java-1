package com.gde.luzanky.bombarder.logika.hra;

import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

/**
 * obsahuji seznam udalosti, ktere mohou nastat v herni logice
 * a hra na ne musi nejak reagovat.
 * tj. zbytek hry se chce dozvedet, ze se takova/makova udalost prave stala!
 */
public interface HerniLogickeListenery
{
	void onStartHry(LevelConfiguration levelConfiguration);
	void onKonecHry(boolean jsemVitez);

	void onLetadloVleteloDoMesta(ILLetadlo letadlo, ILMesto mesto);
	void onLetadloVyleteloZMesta(ILLetadlo letadlo, ILMesto mesto);
	void onLetadloNabouraloDoZeme(ILLetadlo letadlo);
	void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage);
	void onLetadloStrili(ILLeticiProjektil projektil);

	void onProjektilVystrelil(ILLeticiProjektil projektil);
	void onProjektilSePohl(ILLeticiProjektil projektil);
	/** projektil explodoval napradno, tj. jeho dostrel byl vycerpan */
	void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil);
	/** projektil explodoval do domu, tj. dum utrpel damage (~realne zniceni domu) */
	void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu);

	/** cely dum je kompletne znicen */
	void onDumJeKompletneZnicen(ILDum dum);
}
