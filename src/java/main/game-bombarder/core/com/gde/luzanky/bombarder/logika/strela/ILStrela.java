package com.gde.luzanky.bombarder.logika.strela;

import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;

public interface ILStrela
extends ILLeticiProjektil
{
	/** ubere rozsah skod projektilu (~poskozeni, kdyz narazi) */
	void uberRozsahSkod(float damage, ILBlokDomu blokDomu);
}
