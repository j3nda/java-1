package com.gde.luzanky.bombarder.zvuky;

import com.gde.common.utils.RandomUtils;

public enum TypZvuku
{
	StartHry("sounds/LittleRobotSoundFactory/pickup-03__270342.mp3"),
	DalsiLevel("sounds/LittleRobotSoundFactory/jingle-achievement-01__270330.mp3"),
	KonecHry(new String[] {
		"sounds/LittleRobotSoundFactory/jingle-lose-00__270329.mp3",
		"sounds/LittleRobotSoundFactory/jingle-lose-01__270334.mp3",
	}),

	Letadlo_leti,
	Letadlo_striliStreluVpred("letadlo/strili-strela.wav"),

	// TODO: zvuky: zkus implementovat zvuk hozeni bomby (~inspiruje se okolim)
	Letadlo_hazeBombuDolu("sounds/LittleRobotSoundFactory/menu-navigate-03__270315.mp3"),

	Letadlo_explodujeDoZeme("sounds/LittleRobotSoundFactory/hero-death-00__270328.mp3"),
	Letadlo_explodujeDoDomu_OK(new String[] {
		"sounds/LittleRobotSoundFactory/hit-01__270326.mp3",
		"sounds/LittleRobotSoundFactory/craft-00__270309.mp3",
	}),
	Letadlo_explodujeDoDomu_KONEC("sounds/LittleRobotSoundFactory/hero-death-00__270328.mp3"),

	Strela_Leti,
	Strela_explodujeNaprazdno,
	Strela_explodujeDoDomu,

	Bomba_pada,
	Bomba_explodujeNaprazdno,
	Bomba_explodujeDoDomu,
	;
	private final String[] resources;

	private TypZvuku()
	{
		this.resources = null;
	}

	private TypZvuku(String... resource)
	{
		this.resources = resource;
	}

	/** vrati cestu ke zdroji zvuku, ktery je ulozeny v 'assets' */
	public String toResourceFilename()
	{
		if (resources == null || resources.length == 0)
		{
			return null;
		}
		else
		if (resources.length > 1)
		{
			return resources[RandomUtils.nextInt(0, resources.length - 1)];
		}
		else
		{
			return resources[0];
		}
	}
}
