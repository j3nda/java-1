package com.gde.luzanky.bombarder.grafika.obrazovky.hra;

import com.gde.luzanky.bombarder.logika.hra.HerniLogickeListenery;

public interface ILogikaHryHandler
extends HerniLogickeListenery
{
	void addLogicListener(HerniLogickeListenery listener);
	void removeLogicListener(HerniLogickeListenery listener);
}
