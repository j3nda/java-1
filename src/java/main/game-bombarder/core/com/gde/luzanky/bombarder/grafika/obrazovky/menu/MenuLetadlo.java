package com.gde.luzanky.bombarder.grafika.obrazovky.menu;

import com.badlogic.gdx.math.Vector2;
import com.gde.common.utils.Point;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.bombarder.grafika.hra.letadlo.GLetadlo;
import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.letadlo.LLetadlo;


/** letadlo v hlavni nabidce; pohybuje se: nahoru/dolu/leti */
class MenuLetadlo
extends GLetadlo
{
	private enum SmerPohybu { NAHORU, DOLU, LETI }
	/** smer pohybu */
	private SmerPohybu smerPohybu = SmerPohybu.DOLU;
	/** pocitadlo 'smeru pohybu'; kdyz dosahne '0' (~nuly) zmeni se 'smerPohybu' */
	private int smerPohybuCounter = 0;
	/** minimalni a maximalni hodnota pro nahodne urceni 'smerPohybuCounter' */
	private final Vector2 smerPohybuCounterLimit = new Vector2(8, 16);
	/** vektor pohybu 'grafiky' */
	private final Vector2 movingVector = new Vector2(0, -1);
	/** 'amplituda' pro urceni limitu (~jak vysoko) letadlo poleti */
	int amplitude = 4;


	public MenuLetadlo(float rychlost, float zivoty)
	{
		super(
			new LetadloLetiNahoruDolu(
				rychlost,
				zivoty
			)
		);
		((LetadloLetiNahoruDolu)getLogika()).setParent(this);
	}

	@Override
	protected Vector2 getMovingVector()
	{
		return movingVector;
	}

	static class LetadloLetiNahoruDolu
	extends LLetadlo
	{
		private final Point posLimitY = new Point();
		private final Point pos = new Point();
		private MenuLetadlo parent;
		/** listener pro informovani, ze se letadlo pohlo rychlosti */
		private OnLetadloSePohloRychlosti letadloSePohloRychlostiListener;


		public LetadloLetiNahoruDolu(float rychlost, float zivoty)
		{
			super(rychlost, zivoty);
		}

		void setListener(OnLetadloSePohloRychlosti listener)
		{
			this.letadloSePohloRychlostiListener = listener;
		}

		/** nastaveni 'rodice', aby logika "snadno" pristupovala, napr. k amplitude, limitum, aj. */
		public void setParent(MenuLetadlo parent)
		{
			this.parent = parent;
		}

		@Override
		public void setLogickaPozice(int x, int y)
		{
			// zavolam 'predka' a nastavim limity podle 1/2 amplitudy
			super.setLogickaPozice(x, y);
			posLimitY.set(
				getY() - (parent.amplitude / 2),
				getY() + (parent.amplitude / 2)
			);
		}

		/** zmenim pohyb */
		private void zmenPohyb()
		{
			parent.smerPohybuCounter = RandomUtils.nextInt(
				(int)parent.smerPohybuCounterLimit.x,
				(int)parent.smerPohybuCounterLimit.y
			);
			// nahodne urcit pocet opakovani 'noveho pohybu'
			SmerPohybu prevSmerPohybu = parent.smerPohybu;
			do
			{
				// nahodne urcim 'smer pohybu'
				parent.smerPohybu = SmerPohybu.values()[RandomUtils.nextInt(0, SmerPohybu.values().length - 1)];
			}
			while(prevSmerPohybu == parent.smerPohybu); // a budu toto opakovat, dokud hodnota bude jina, nez predchozi

			// podle 'smerPohybu' urcim 'vektor pro pohyb v grafice'
			switch(parent.smerPohybu)
			{
				case NAHORU:
				{
					parent.movingVector.y = +1;
					break;
				}
				case DOLU:
				{
					parent.movingVector.y = -1;
					break;
				}
				case LETI:
				default:
				{
					parent.movingVector.y = 0;
					break;
				}
			}
		}

		@Override
		protected boolean pohniSeRychlosti()
		{
			// zmen typ pohybu, pokud dojde pocitadlo
			parent.smerPohybuCounter--;
			if (parent.smerPohybuCounter < 0)
			{
				zmenPohyb();
			}

			// nastavim novou pozici (~do pomocne promenne)
			pos.set(
				getX() + (int)parent.movingVector.x,
				getY() + (int)parent.movingVector.y
			);

			// zkontroluju limity (~podle amplitudy)
			if (pos.getY() < posLimitY.getX())
			{
				pos.setY(posLimitY.getX());
				parent.movingVector.y *= -1;
			}
			if (pos.getY() > posLimitY.getY())
			{
				pos.setY(posLimitY.getY());
				parent.movingVector.y *= -1;
			}

			// nastavim novou logickou pozici podle vypoctene pozice...
			super.setLogickaPozice(pos.getX(), pos.getY());

			if (letadloSePohloRychlostiListener != null)
			{
				// informuju, ze se letadlo pohlo
				letadloSePohloRychlostiListener.onLetadloSePohloRychlosti(this);
			}
			return true;
		}
	}

	interface OnLetadloSePohloRychlosti
	{
		void onLetadloSePohloRychlosti(ILLetadlo letadlo);
	}
}
