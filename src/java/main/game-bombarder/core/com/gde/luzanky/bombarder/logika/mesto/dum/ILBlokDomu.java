package com.gde.luzanky.bombarder.logika.mesto.dum;

import com.gde.luzanky.bombarder.logika.prvky.ILogickaCast;

/**
 * interfejs (~rozhranni) logiky 1x patra domu (~blok)
 * dedi vsechny vlastnosti {@link ILogickaCast}
 */
public interface ILBlokDomu
extends ILogickaCast
{
	/** vrati typ poskozeni */
	TypPoskozeni getPoskozeni();
	/** vrati pocet zbyvajicich zivotu (~getZivoty() + getDamage() = puvodni zivody) */
	float getZivoty();
	/** vrati na-akumulovany damage (~getZivoty() + getDamage() = puvodni zivody) */
	float getDamage();
	/** znici blok domu */
	void znicit(float damage);
}
