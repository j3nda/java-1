package com.gde.luzanky.bombarder.zvuky;

import com.gde.luzanky.bombarder.grafika.hra.HerniGrafickeListenery;

/** obsahuji seznam udalosti, ktere "stinuji" herni logiku a zvukove ji interpretuji */
public interface HerniZvukoveListenery
extends HerniGrafickeListenery
{

}
