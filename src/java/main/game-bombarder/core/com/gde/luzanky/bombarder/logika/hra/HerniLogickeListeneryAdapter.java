package com.gde.luzanky.bombarder.logika.hra;

import com.gde.luzanky.bombarder.logika.letadlo.ILLetadlo;
import com.gde.luzanky.bombarder.logika.mesto.ILMesto;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILBlokDomu;
import com.gde.luzanky.bombarder.logika.mesto.dum.ILDum;
import com.gde.luzanky.bombarder.logika.prvky.ILLeticiProjektil;
import com.gde.luzanky.bombarder.nastaveni.LevelConfiguration;

/**
 * prazdna implementace {@link HerniLogickeListenery},
 * abych mohl pohodlne pretezovat jednotlive metody a neupsal se k smrti ;)
 */
public abstract class HerniLogickeListeneryAdapter
implements HerniLogickeListenery
{
	@Override
	public void onStartHry(LevelConfiguration levelConfiguration)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onKonecHry(boolean jsemVitez)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloVleteloDoMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloVyleteloZMesta(ILLetadlo letadlo, ILMesto mesto)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloNabouraloDoZeme(ILLetadlo letadlo)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloZtratiloZivoty(ILLetadlo letadlo, float damage)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onLetadloStrili(ILLeticiProjektil projektil)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onProjektilVystrelil(ILLeticiProjektil projektil)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onProjektilExplodovalNaprazdno(ILLeticiProjektil projektil)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onProjektilExplodovalDoDomu(ILLeticiProjektil projektil, ILBlokDomu[] zniceneBlokyDomu)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onProjektilSePohl(ILLeticiProjektil projektil)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void onDumJeKompletneZnicen(ILDum dum)
	{
		// TODO Auto-generated method stub
	}
}
