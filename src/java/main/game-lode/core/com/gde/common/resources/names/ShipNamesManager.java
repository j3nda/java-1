package com.gde.common.resources.names;

import java.util.HashSet;
import java.util.Set;

import com.gde.common.utils.RandomUtils;

/** jednoduchy manazer pro spravu informaci o lodich: WWI a WWII */
public class ShipNamesManager
extends NamesManager<ShipNameInfo>
{
	private final Set<Integer> imagesTaken;

	public ShipNamesManager()
	{
		super("ship-names.csv");
		imagesTaken = new HashSet<>();
	}

	@Override
	protected ShipNameInfo createFromCsv(String csvLine)
	{
		return new ShipNameInfo(csvLine);
	}

	@Override
	public void clear()
	{
		imagesTaken.clear();
	}

	public ShipNameInfo random(int size)
	{
		return random(size, true);
	}

	public ShipNameInfo random(int size, boolean withImage)
	{
		// TODO: XHONZA: nahodne jmeno MUSI mit vzdy jine pocatecni pismeno pro 1x velikost (~ale pro 1x hrace)
		ShipNameInfo info = null;
		do
		{
			int index = RandomUtils.nextInt(0, entry.size() - 1);
			info = entry.get(index);
			info.image = 103;//(RandomUtils.nextInt(0, 100) % 2 == 0 ? 103 : 128);
		}
		while(
			   entriesTaken.contains(info)
			|| (withImage && (info.image == 0 || imagesTaken.contains(info.image)))
		);
		entriesTaken.add(info);
//		shipsImageTaken.add(info.image);//TODO: XHONZA/ShipNameManager.random(shipsImageTaken);
		info.requiredSize = size;

		return info;
	}

	public enum ShipSource
	{
		BattleShip("bs"),
		BattleCruiser("bc"),
		Cruiser("cs"),
		CoastalDefense("cd"),
		Monitor("m"),
		Destroyer("d"),
		Fregatte("f"),
		MinorWarship("mr"),
		MineWarfare("me"),
		;
		final String csvId;
		static int maxLength;
		static
		{
			for(ShipSource entry : values())
			{
				if (entry.name().length() > maxLength)
				{
					maxLength = entry.name().length();
				}
			}
		}

		private ShipSource(String csvId)
		{
			this.csvId = csvId;
		}

		static ShipSource fromString(String name)
		{
			String nameLower = name.toLowerCase();
			for(ShipSource source : values())
			{
				if (source.csvId.equals(nameLower))
				{
					return source;
				}
			}
			throw new IllegalArgumentException("Invalid name(" + name + ")!");
		}
	}

	public enum ShipOperator
	{
		Argentina("ar"),//		sed -i -r 's/,Argentine Navy,/,ar,/ig' ${filename}
		Belgium("be"),//		sed -i -r 's/,Belgian Navy,/,be,/ig' ${filename}
		Venezuela("ve"),//		sed -i -r 's/,Bolivarian Navy of Venezuela,/,ve,/ig' ${filename}
		Brazil("br"),//		sed -i -r 's/,Brazilian Navy,/,br,/ig' ${filename}
		Chile("cl"),//		sed -i -r 's/,Chilean Navy,/,cl,/ig' ${filename}
		Colombia("co"),//		sed -i -r 's/,Colombian National Navy,/,co,/ig' ${filename}
		Cuba("cu"),//		sed -i -r 's/,Cuban Navy,/,cu,/ig' ${filename}
		Egypt("eg"),//		sed -i -r 's/,Egyptian Navy,/,eg,/ig' ${filename}
		Finland("fi"),//		sed -i -r 's/,Finnish Navy,/,fi,/ig' ${filename}
		France("fr"),//		sed -i -r 's/,Free French Naval Forces,/,fr,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,French Navy,/,fr,/ig' ${filename}
		Greece("gr"),//		sed -i -r 's/,Hellenic Navy,/,gr,/ig' ${filename}
		Japan("jp"),//		sed -i -r 's/,Imperial Japanese Navy,/,jp,/ig' ${filename}
		Germany("de"),//		sed -i -r 's/,Kriegsmarine,/,de,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,Manchukuo Imperial Navy,/,jp,/ig' ${filename}
		Croatia("hr"),//		sed -i -r 's/,Navy of the Independent State of Croatia,/,hr,/ig' ${filename}
		Paraguay("py"),//		sed -i -r 's/,Paraguayan Navy,/,py,/ig' ${filename}
		Peru("pe"),//		sed -i -r 's/,Peruvian Navy,/,pe,/ig' ${filename}
		Poland("pl"),//		sed -i -r 's/,Poland,/,pl,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,Polish Navy,/,pl,/ig' ${filename}
		Portugal("pt"),//		sed -i -r 's/,Portuguese Navy,/,pt,/ig' ${filename}
		Italy("it"),//		sed -i -r 's/,Regia Marina,/,it,/ig' ${filename}
		Taiwan("tw"),//		sed -i -r 's/,Republic of China Navy,/,tw,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,Republic of China-Nanjing Navy,/,jp,/ig' ${filename}
		Australia("au"),//		sed -i -r 's/,Royal Australian Navy,/,au,/ig' ${filename}
		Canada("ca"),//		sed -i -r 's/,Royal Canadian Navy,/,ca,/ig' ${filename}
		Denmark("dk"),//		sed -i -r 's/,Royal Danish Navy,/,dk,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,Royal Hellenic Navy,/,gr,/ig' ${filename}
		India("in"),//		sed -i -r 's/,Royal Indian Navy,/,in,/ig' ${filename}
		UnitedKingdom("gb"),//		sed -i -r 's/,Royal Navy,/,gb,/ig' ${filename}
		Netherland("nl"),//		sed -i -r 's/,Royal Netherlands Navy,/,nl,/ig' ${filename}
		NewZealand("nz"),//		sed -i -r 's/,Royal New Zealand Navy,/,nz,/ig' ${filename}
		Norway("no"),//		sed -i -r 's/,Royal Norwegian Navy,/,no,/ig' ${filename}
		Romaina("ro"),//		sed -i -r 's/,Royal Romanian Navy,/,ro,/ig' ${filename}
		Thailand("th"),//		sed -i -r 's/,Royal Thai Navy,/,th,/ig' ${filename}
		Yugoslavia("yu"),//		sed -i -r 's/,Royal Yugoslav Navy,/,yu,/ig' ${filename}
		SouthArfica("za"),//		sed -i -r 's/,South African Navy,/,za,/ig' ${filename}
		Russia("ru"),//		sed -i -r 's/,Soviet Navy,/,ru,/ig' ${filename}
		Spain("es"),//		sed -i -r 's/,Spanish Navy,/,es,/ig' ${filename}
		Sweden("se"),//		sed -i -r 's/,Swedish Navy,/,se,/ig' ${filename}
		Turkey("tr"),//		sed -i -r 's/,Turkish Naval Forces,/,tr,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,Turkish Navy,/,tr,/ig' ${filename}
		UnitedStates("us"),//		sed -i -r 's/,United States Coast Guard,/,us,/ig' ${filename}
//		xxx("cl"),//		sed -i -r 's/,United States Navy,/,us,/ig' ${filename}
		;
		String csvId;
		static int maxLength;
		static
		{
			for(ShipOperator entry : values())
			{
				if (entry.name().length() > maxLength)
				{
					maxLength = entry.name().length();
				}
			}
		}

		ShipOperator(String csvId)
		{
			this.csvId = csvId;
		}

		static ShipOperator fromString(String name)
		{
			String nameLower = name.toLowerCase();
			for(ShipOperator operator : values())
			{
				if (operator.csvId.equals(nameLower))
				{
					return operator;
				}
			}
			throw new IllegalArgumentException("Invalid name(" + name + ")!");
		}
	}
}
