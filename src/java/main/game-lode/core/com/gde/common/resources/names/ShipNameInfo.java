package com.gde.common.resources.names;

import com.gde.common.resources.names.NamesManager.NameInfoWithIdCounter;
import com.gde.common.resources.names.ShipNamesManager.ShipOperator;
import com.gde.common.resources.names.ShipNamesManager.ShipSource;

public class ShipNameInfo
extends NameInfoWithIdCounter
{
	public int image;
	public final ShipSource source;
	public final String name;
	public final int displacement;
	public final String clazz;
	public final String type;
	public ShipOperator operator;
	int requiredSize = 0;

	ShipNameInfo(String csv)
	{
		super();
		//0-------|1-----|2-----------|3-------|4---|5----|6---|7----|8-----------|9---|
		//hasImage|source|displacement|operator|ship|class|type|first|endOfService|fate|
		String[] col = max(csv.split(","));
		this.image = (col[0] == null || col[0].length() == 0 ? 0 : Integer.valueOf(col[0].replace("\"", "")));
		this.source = ShipSource.fromString(col[1].replace("\"", ""));
		this.displacement = Integer.valueOf(col[2].replace("\"", ""));
		this.operator = ShipOperator.fromString(col[3].replace("\"", ""));
		this.name = col[4].replace("\"", "");
		this.clazz = col[5].replace("\"", "");
		this.type = col[6].replace("\"", "");
	}

	ShipNameInfo(
		int image, ShipSource source, int displacement, ShipOperator operator,
		String name, String clazz, String type/*, String firstCommisioned, String endOfService, String fate*/
	)
	{
		super();
		this.image = image;
		this.source = source;
		this.displacement = displacement;
		this.name = name;
		this.clazz = clazz;
		this.type = type;
	}

	public int getRequiredSize()
	{
		return requiredSize;
	}

	@Override
	public String toString()
	{
		return ""
			+  "| " + String.format("%-" + (counterId() + "").length() + "s", id)
			+ " | " + String.format("%-" + max(0) + "s", image)
			+ " | " + String.format("%-" + max(1) + "s", source)
			+ " | " + String.format("%-" + max(2) + "s", displacement)
			+ " | " + String.format("%-" + max(3) + "s", operator)
			+ " | " + String.format("%-" + max(4) + "s", name)
			+ " | " + String.format("%-" + max(5) + "s", clazz)
			+ " | " + String.format("%-" + max(6) + "s", type)
		;
	}
}
