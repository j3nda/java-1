package com.gde.common.graphics.screens;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.gde.common.resources.IFontsManager;
import com.gde.common.resources.names.PlayerNamesManager;
import com.gde.common.resources.names.ShipNamesManager;

public interface ILodeScreenResources
extends IScreenResources
{
	IFontsManager getFontsManager();
	IAssetManager getAssetManager();
	ShipNamesManager getShipNamesManager();
	PlayerNamesManager getPlayerNamesManager();
	Label createLabel(String name);
	Label createLabel(String name, int fontSize);
	Label createLabel(String name, int fontSize, int align);
	Label createLabel(String name, int fontSize, Color color);
	Label createLabel(String name, int fontSize, Color color, int align);
}
