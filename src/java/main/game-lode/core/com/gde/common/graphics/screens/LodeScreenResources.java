package com.gde.common.graphics.screens;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.resources.CommonResources;
import com.gde.common.resources.GdeAssetManager;
import com.gde.common.resources.GdeFontsManager;
import com.gde.common.resources.IFontsManager;
import com.gde.common.resources.names.PlayerNamesManager;
import com.gde.common.resources.names.ShipNamesManager;

public class LodeScreenResources
extends ScreenResources
implements ILodeScreenResources
{
	private final IFontsManager fontsManager;
	private final IAssetManager assetManager;
	private final ShipNamesManager shipNamesManager;
	private final PlayerNamesManager playerNamesManager;

	public LodeScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		this.fontsManager = new GdeFontsManager();
		this.assetManager = new GdeAssetManager();
		this.shipNamesManager = new ShipNamesManager();
		this.playerNamesManager = new PlayerNamesManager();
	}

	@Override
	public IFontsManager getFontsManager()
	{
		return fontsManager;
	}

	@Override
	public IAssetManager getAssetManager()
	{
		return assetManager;
	}

	@Override
	public ShipNamesManager getShipNamesManager()
	{
		return shipNamesManager;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		fontsManager.dispose();
		assetManager.dispose();
	}

	@Override
	public PlayerNamesManager getPlayerNamesManager()
	{
		return playerNamesManager;
	}

	@Override
	public Label createLabel(String name)
	{
		return createLabel(name, 33);
	}

	@Override
	public Label createLabel(String name, int fontSize)
	{
		return createLabel(name, fontSize, Align.center);
	}

	@Override
	public Label createLabel(String name, int fontSize, Color color)
	{
		return createLabel(name, fontSize, color, Align.center);
	}

	@Override
	public Label createLabel(String name, int fontSize, int align)
	{
		return createLabel(name, fontSize, Color.RED, align);
	}

	@Override
	public Label createLabel(String name, int fontSize, Color color, int align)
	{
		Label label = new Label(
			name,
			fontsManager.getLabelStyle(CommonResources.Font.vt323, fontSize, color)
		);
		label.setAlignment(align);
		return label;
	}
}
