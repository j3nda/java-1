package com.gde.luzanky.lode.setup;

import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.setup.SetupManager.OnLodSetupListener;
import com.gde.luzanky.lode.setup.SetupManager.SetupListeners;

class LodInfoSetup
extends LodInfo
{
	final int hracId;
	private int setup = 0;
	private final SetupListeners setupListeners;

	LodInfoSetup(int hracId, LodInfo info, SetupListeners setupListeners)
	{
		super(info.id, info.velikost, info.jmeno);
		this.hracId = hracId;
		this.setupListeners = setupListeners;
	}


	/** vraci true, pokud je lod nakonfigurovana. jinak vraci false */
	boolean jeKompletni()
	{
		// TODO: NASTAVENI: vrat true, pokud je lod nakonfigurovana. jinak vrat false.
		return (setup == velikost);
	}

	void setupOn()
	{
		setup++;
		if (setup > velikost)
		{
			setup = velikost;
		}
		for(OnLodSetupListener listener : setupListeners.lodSetup)
		{
			listener.setupOn(this);
		}
	}

	void setupOff()
	{
		setup--;
		if (setup < 0)
		{
			setup = 0;
		}
		for(OnLodSetupListener listener : setupListeners.lodSetup)
		{
			listener.setupOff(this);
		}
	}

	void reset()
	{
		while(setup > 0)
		{
			setupOff();
		}
	}

	/** vrati pocet vybranych dilku lode */
	int selected()
	{
		return setup;
	}

	@Override
	public String toString()
	{
		return super.toString()
			+ ", setup=" + setup
			+ ", kompletni=" + jeKompletni()
			+ ", hracId=" + hracId
		;
	}
}
