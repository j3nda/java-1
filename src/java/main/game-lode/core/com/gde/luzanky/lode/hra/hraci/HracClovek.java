package com.gde.luzanky.lode.hra.hraci;

import com.gde.common.utils.Point;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.setup.ObrazovkaSetup;

public class HracClovek
extends HracInfo
{
	/** volano z: {@link LodeGameConfiguration} pri vytvoreni hracu */
	public HracClovek(LodInfo[] lode, String jmeno)
	{
		super(lode, true, jmeno);
	}

	/** volano z: {@link ObrazovkaSetup} pri startHry(); */
	public HracClovek(HracInfo hrac, int[][] rozmisteniLodi)
	{
		super(hrac, rozmisteniLodi);
	}

	/** volano z: {@link ObrazovkaSetup} pri vytvorDebugGameplayCheckbox(); */
	public HracClovek(int id, LodInfo[] lode, int[][] rozmisteniLodi, String jmeno)
	{
		super(id, lode, rozmisteniLodi, true, jmeno);
	}

	@Override
	public Point tahHrace(int utocimHracId)
	{
		// clovek: resi 'tah' tak, ze klika...
		return null;
	}
}
