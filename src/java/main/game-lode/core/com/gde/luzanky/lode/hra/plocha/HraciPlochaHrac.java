package com.gde.luzanky.lode.hra.plocha;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolickoHrac;

/** hraci plocha: kde se odehrava bitva (~z pohledu hrajiciho hrace) */
public class HraciPlochaHrac
extends HraciPlocha
{
	public HraciPlochaHrac(ILodeScreenResources resources, LodeGameConfiguration gameConfig, HracInfo hrac, OnTahHrace onTahHraceListener)
	{
		super(resources, gameConfig, hrac, onTahHraceListener);
	}

	/** vytvori hraci policko */
	@Override
	protected HraciPolicko vytvorHraciPolicko(int radek, int sloupec)
	{
		int lodId = getRozmisteniLodi()[radek][sloupec];

		HraciPolickoHrac pol = new HraciPolickoHrac(resources, radek, sloupec);

		// TODO: hra/GRAFIKA: vizual HraciPolicko
		if (lodId == VODA_ID)
		{
			pol.setText(VODA_TEXT);
			pol.setPozadi(Color.BLUE);
		}
		else
		{
			Color barvaLode = vratBarvuLode(lodId);

			pol.setText(lodId + "");
			pol.setPozadi(barvaLode);
		}
		return pol;
	}
}
