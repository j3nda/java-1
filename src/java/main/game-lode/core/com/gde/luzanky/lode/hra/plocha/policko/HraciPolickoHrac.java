package com.gde.luzanky.lode.hra.plocha.policko;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko.IHraciPolickoWithText;


public class HraciPolickoHrac
extends HraciPolicko
implements IHraciPolickoWithText
{
	private final Label text;
	private String textValue;
	private final Image pozadi;

	public HraciPolickoHrac(ILodeScreenResources resources, int radek, int sloupec)
	{
		super(radek, sloupec);

		text = resources.createLabel("P" + formatPozice());
		pozadi = new Image(new Texture("tint/square16x16.png"));

		add(pozadi);
		add(text);
		///* + formatPozice()*/, FontSize.HraciPlocha.Policko.NAZEV));
	}

	@Override
	public void setText(String t)
	{
		text.setText(textValue = t);
	}

	@Override
	public String getText()
	{
		return textValue;
	}

	public void setPozadi(Color barva)
	{
		pozadi.setColor(barva);
	}
}
