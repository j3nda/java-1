package com.gde.luzanky.lode.menu;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.ui.IAnimationActor;
import com.gde.common.graphics.ui.IAnimationActorListener;
import com.gde.common.resources.CommonResources;
import com.gde.common.resources.names.ShipNameInfo;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.LodeGameConfiguration.ObtiznostHry;
import com.gde.luzanky.lode.SdilenaObrazovka;
import com.gde.luzanky.lode.TypObrazovky;
import com.gde.luzanky.lode.resources.MissouriAnimation;
import com.gde.luzanky.lode.resources.MissouriAnimation.MissouriAction;
import com.gde.luzanky.lode.setup.ObrazovkaSetup;

public class ObrazovkaMenu
extends SdilenaObrazovka
implements IAnimationActorListener
{
	private final static String font = CommonResources.Font.vt323;
	private Texture texture;
	private Image image;
	private VelikostHraciPlochy boardSize;
	private ObtiznostTable difficulty;
	private Showcase showcase;
	private Map<Integer, MissouriAnimation> missouri;
	private MissouriAction missouriAction;
	private TextureRegion tvOverlay;

	public ObrazovkaMenu(LodeGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
		missouriAction = MissouriAction.values()[RandomUtils.nextInt(0, MissouriAction.values().length - 1)];
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MENU;
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
	}

	@Override
	public void show()
	{
		super.show();

		missouri = createMissouri(missouriAction);
		tvOverlay = createTvOverlay(resources.getAssetManager(), CommonResources.Gde.tv1280x960);

		texture = new Texture(CommonResources.Tint.square16x16);

		// vytvori obrazek, ktery obsahuje texturu; nastavi pozici; a priradi "vizualni" akce...
		image = new Image(texture);
		image.setPosition(0, 0);
		image.setOrigin(Align.center);
		image.addAction(
			Actions.sequence(
				Actions.moveTo(
					Gdx.graphics.getWidth() - (image.getWidth() * 1.1f),
					Gdx.graphics.getHeight() - (image.getHeight() * 1.1f),
					1.5f,
					Interpolation.bounceOut
				),
				Actions.forever(
					Actions.parallel(
						Actions.sequence(
							Actions.rotateBy(135f, 6f),
							Actions.rotateBy(-90f, 3f)
						),
						Actions.sequence(
							Actions.scaleTo(0.25f, 0.25f, 1.5f, Interpolation.bounceOut),
							Actions.scaleTo(1.25f, 1.25f, 1.5f, Interpolation.bounceOut)
						)
					)
				)
			)
		);
//		showcase = new Showcase(resources);
//		showcase.setSize(getWidth(), getHeight());
//		showcase.update(resources.getShipNameManager().random(1));

//		getStage().addActor(showcase);
		getStage().addActor(image);
		getStage().addActor(createMenu());
	}

	private Map<Integer, MissouriAnimation> createMissouri(MissouriAction first)
	{
		Map<Integer, MissouriAnimation> map = new HashMap<>();
		MissouriAnimation anim = null;
		map.put(
			first.ordinal(),
			anim = new MissouriAnimation(first, resources.getAssetManager())
		);
		anim.setSize(getWidth(), getHeight());
		anim.setListener(this);
		final ObrazovkaMenu THIS = this;
		Gdx.app.postRunnable(new Runnable()
		{
			@Override
			public void run()
			{
				for(MissouriAction action : MissouriAction.values())
				{
					if (action == first)
					{
						continue;
					}
					MissouriAnimation anim = null;
					map.put(
						action.ordinal(),
						anim = new MissouriAnimation(action, resources.getAssetManager())
					);
					anim.setSize(getWidth(), getHeight());
					anim.setListener(THIS);
				}
			}
		});
		return map;
	}

	private Table createShowcase()
	{
		Table wrapper = new Table();

		wrapper.setSize(getWidth(), getHeight());
//		wrapper
//			.add(showcaseImage = new Image())
//			.align(Align.center)
//			.grow()
//		;
//		wrapper.row();

		Point tile = new Point(150, 150);
		int cols = getWidth() / tile.x;
		int rows = getHeight() / tile.y;
		Texture tex0 = resources.getAssetManager().get("ships/103.jpg", Texture.class);

//		wrapper
//			.add(new Image(tex0))
//			.width(tile.x)
//			.height(tile.y)
//		;
//		wrapper.row();


		for(int i = 0; i <= rows; i++)
		{
			for(int j = 0; j <= cols; j++)
			{
				ShipNameInfo ship = resources.getShipNamesManager().random(1);
				String fn = "ships/" + ship.image + ".jpg";

				Texture tex = resources.getAssetManager().get(fn, Texture.class);
System.out.println(ship);
System.out.println(tex);
				wrapper
					.add(new Image(tex0))
					.width(tile.x)
					.height(tile.y)
				;
			}
			wrapper.row();
		}
		if (LodeGame.isDebug())
		{
			wrapper.debugAll();
		}
		return wrapper;
	}

	private Table createMenu()
	{
		Table wrapper = new Table();
		wrapper.setSize(getWidth(), getHeight());
		wrapper
			.add(new Label(
				"napis:lode",
				resources.getFontsManager().getLabelStyle(font, 33, Color.RED)
			))
			.colspan(2)
			.expandX()
			.height(getHeight() / 3f)
		;
		wrapper.row();


		wrapper
			.add(createGameOptions())
			.grow()
		;
		wrapper
			.add(createStartGameButton("play!"))
			.grow()
		;
		wrapper.row();

		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Table createGameOptions()
	{
		Table wrapper = new Table();

		wrapper
			.add(boardSize = new VelikostHraciPlochy(resources))
			.colspan(ObtiznostHry.values().length)
			.grow()
		;
		wrapper.row();

		wrapper
			.add(difficulty = new ObtiznostTable(resources))
			.align(Align.center)
			.expandX()
		;
		wrapper.row();

		return wrapper;
	}

	private Label createStartGameButton(String text)
	{
		Label button = new Label(
			text,
			resources.getFontsManager().getLabelStyle(font, 33, Color.RED)
		);
		button.setAlignment(Align.center);
		button.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				resources.getShipNamesManager().clear();
				resources.getPlayerNamesManager().clear();
				parentGame.setScreen(
					new ObrazovkaSetup(
						parentGame,
						getScreenType(),
						new LodeGameConfiguration(
							resources.getShipNamesManager(),
							resources.getPlayerNamesManager(),
							boardSize.getVelikost(),
							difficulty.getObtiznost(),
							false
						)
					)
				);
			}
		});
		return button;
	}

	@Override
	protected void renderScreen(float delta)
	{
		getBatch().begin();
		renderMissouri(delta, missouriAction);
		renderTvOverlay(delta);
		getBatch().end();

		renderScreenStage(delta);
	}

	private void renderMissouri(float delta, MissouriAction action)
	{
		MissouriAnimation anim = missouri.get(action.ordinal());

		Batch batch = getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		anim.act(delta);
		anim.draw(batch, 1.0f);
		if (isDrawing)
		{
			batch.flush();
		}
		else
		{
			batch.end();
		}
	}

	private void renderTvOverlay(float delta)
	{
		Batch batch = getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		batch.draw(tvOverlay, 0, 0);
		if (isDrawing)
		{
			batch.flush();
		}
		else
		{
			batch.end();
		}
	}


	@Override
	public void onAnimationFinished(IAnimationActor animation)
	{
		MissouriAction prevAction = missouriAction;
		do
		{
			missouriAction = MissouriAction.values()[RandomUtils.nextInt(0, MissouriAction.values().length - 1)];
		}
		while(prevAction == missouriAction || missouriAction == MissouriAction.odjezd1);

		MissouriAnimation anim = missouri.get(missouriAction.ordinal());
		anim.reset();
		anim.setFrameDuration(0.25f);
		anim.setFrameDuration(0.15f);
	}

	private TextureRegion createTvOverlay(IAssetManager resources, String resourceName)
	{
		Texture textureToRepeat = resources.get(resourceName, Texture.class);
		textureToRepeat.setWrap(
			TextureWrap.Repeat,
			TextureWrap.Repeat
		);
		TextureRegion repeatedTexture = new TextureRegion(textureToRepeat);
		repeatedTexture.setRegion(
			0,
			0,
			getWidth(), // width  after repeat
			getHeight() // height after repeat
		);

		return repeatedTexture;
	}


}
//TODO:menu:board-size
//TODO:menu:start
//TODO:umisteni-lodi >> asi separatni screen
//TODO:gamesa >> pole[][] s tim, ze '1', '2', '3', '4', '5' lode (ale jak jejich pocet)
//TODO:gamesa >> {vystrel, zasah, voda} ~~ asi pocet tahu >> tap na tabulku >> resim vypaleni >> nejaka animace shlitz

//Battle Ships for the Atari 8-bit family
//--https://www.youtube.com/watch?v=HvGp3vJ6h-4

//silent-service/loading
//--https://www.google.com/imgres?imgurl=https%3A%2F%2Fc8.alamy.com%2Fcomp%2F2CGMW2F%2Fsilent-service-nintendo-entertainment-system-nes-videogame-editorial-use-only-2CGMW2F.jpg&imgrefurl=https%3A%2F%2Fwww.alamy.com%2Fstock-photo%2Fsilent-service.html&tbnid=5HLnR7bYo8YXKM&vet=12ahUKEwi6kKGJ6K_7AhULVqQEHV3EA8IQMygqegUIARCKAg..i&docid=MgmekFtfvA5KyM&w=1300&h=1042&q=%20silent%20service%20loading%20image&ved=2ahUKEwi6kKGJ6K_7AhULVqQEHV3EA8IQMygqegUIARCKAg
//--https://www.google.com/imgres?imgurl=https%3A%2F%2Fdtc-wsuv.org%2Fell-catalog%2Fsite2%2Fimg%2Fgames%2Fsilent-service%2Ffull.png&imgrefurl=https%3A%2F%2Fdtc-wsuv.org%2Fell-catalog%2Fsite2%2Fgames.php%3Fp%3D124&tbnid=o21z-KDQqqvb7M&vet=12ahUKEwi6kKGJ6K_7AhULVqQEHV3EA8IQMyhVegQIARB2..i&docid=ZLa09mH5I8qTcM&w=850&h=500&q=%20silent%20service%20loading%20image&ved=2ahUKEwi6kKGJ6K_7AhULVqQEHV3EA8IQMyhVegQIARB2
//--https://www.youtube.com/results?search_query=silent+service+wwii

//bismarck/animation/bw
//--https://www.youtube.com/watch?v=oWvZ8EEoovM

//missouri/fire
//--https://www.youtube.com/watch?v=vj-15O-BTDw

// missouri/history/document
//-- https://www.youtube.com/watch?v=cJb_zT1ZBsI&ab_channel=WorldofWarshipsOfficialChannel

// missouri/battle-ship[movie]
// -- https://www.youtube.com/watch?v=nCqDdsZY7RA&ab_channel=UniversalPictures
// -- https://www.youtube.com/watch?v=-iGp8D9832Y&ab_channel=GuardianImages

//libgdx/play-mp4
//--https://gamedev.stackexchange.com/questions/30607/play-videos-with-libgdx
//--https://stackoverflow.com/questions/35940543/video-playing-in-libgdx
//--https://stackoverflow.com/questions/41651387/options-to-efficiently-draw-a-stream-of-byte-arrays-to-display-in-android
//--https://github.com/libgdx/gdx-video
//WARNING: gdx-video is piece of shit! it doesnt work well! in y2023!

//plujici-logo/lode~~~~asi nejvic cool
//--https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F41%2F77%2F87%2F4177871e8979435e57df2b9443cc4656.gif&imgrefurl=https%3A%2F%2Fwww.pinterest.com%2Fpin%2F667447607244621143%2F&tbnid=qgD5zivho17acM&vet=12ahUKEwjUqYbe7K_7AhUBdRoKHZ7MADgQMygHegUIARD2AQ..i&docid=lKY3WcaIse_VmM&w=360&h=270&q=us%20navy%20wwii%20gif&ved=2ahUKEwjUqYbe7K_7AhUBdRoKHZ7MADgQMygHegUIARD2AQ
//--https://www.google.com/imgres?imgurl=http%3A%2F%2Fxbradtc.typepad.com%2F.a%2F6a01b8d19a8034970c01b8d1cc73e7970c-pi&imgrefurl=https%3A%2F%2Fblog.xbradtc.com%2F2016%2F04%2Fboom.html&tbnid=zDPfqhMjX-ReXM&vet=12ahUKEwiG2NL27K_7AhVLrxoKHaGRANgQMyg2egQIARA2..i&docid=cUWB9BhSYGOJgM&w=366&h=281&itg=1&q=us%20navy%20wwii%20gif&hl=cs&ved=2ahUKEwiG2NL27K_7AhVLrxoKHaGRANgQMyg2egQIARA2
//--https://www.google.com/imgres?imgurl=https%3A%2F%2Fthumbs.gfycat.com%2FHairyWhimsicalIndianjackal-max-1mb.gif&imgrefurl=https%3A%2F%2Fgfycat.com%2Fdiscover%2Fbomb-test-gifs&tbnid=L_eV_B3rHny4nM&vet=12ahUKEwiG2NL27K_7AhVLrxoKHaGRANgQMygGegUIARDPAQ..i&docid=4YKID3sHB1kU0M&w=480&h=270&itg=1&q=us%20navy%20wwii%20gif&hl=cs&ved=2ahUKEwiG2NL27K_7AhVLrxoKHaGRANgQMygGegUIARDPAQ
//--https://www.google.com/imgres?imgurl=https%3A%2F%2Fthumbs.gfycat.com%2FKaleidoscopicContentAxolotl-max-1mb.gif&imgrefurl=https%3A%2F%2Fgfycat.com%2Fgifs%2Ftag%2Ffat%2Bguy&tbnid=AW9HNYsMud91iM&vet=12ahUKEwjl1tv57K_7AhVn8rsIHaWVDBkQxiAoA3oECAAQKw..i&docid=9z-PwyBC2h6DoM&w=640&h=360&itg=1&q=us%20navy%20wwii%20gif&hl=cs&ved=2ahUKEwjl1tv57K_7AhVn8rsIHaWVDBkQxiAoA3oECAAQKw
