package com.gde.luzanky.lode.hra.hraci;

import com.gde.common.utils.Point;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha;
import com.gde.luzanky.lode.setup.ObrazovkaSetup;

/**
 * datova obalka pro 'obecne' informace o 1x hraci
 * (id, jmeno, seznam-lodi, rozmisteni-lodi)
 */
public abstract class HracInfo
{
	/** staticke pocitadlo pro generovani unikatniho id hrace */
	private static int idCounter = 0;
	/** unikatni identifikator hrace */
	public final int id;
	public final LodInfo[] lode;
	public final boolean jsemClovek;
	public final String jmeno;
	/** 2d pole, ktere reprezentuje rozmisteni/pozici lodi ([radek][sloupec] vs {@link LodInfo}={lod.id vs {@link HraciPlocha#VODA_ID}, aj}) */
	public int[][] rozmisteniLodi;

	/** volano z: {@link LodeGameConfiguration} pri vytvoreni hracu */
	protected HracInfo(LodInfo[] lode, boolean jsemClovek, String jmeno)
	{
		this(idCounter++, lode, null, jsemClovek, jmeno);
	}

	/** volano z: {@link ObrazovkaSetup} pri startHry(); */
	protected HracInfo(HracInfo hrac, int[][] rozmisteniLodi)
	{
		this(hrac.id, hrac.lode, rozmisteniLodi, hrac.jsemClovek, hrac.jmeno);
	}

	/** volano z: {@link ObrazovkaSetup} pri vytvorDebugGameplayCheckbox(); */
	protected HracInfo(int id, LodInfo[] lode, int[][] rozmisteniLodi, boolean jsemClovek, String jmeno)
	{
		this.id = id;
		this.lode = lode;
		this.jsemClovek = jsemClovek;
		this.jmeno = jmeno;
		this.rozmisteniLodi = rozmisteniLodi;
	}

	/** vykonani tahu, tj. vraceni policka, na ktere utocim */
	public abstract Point tahHrace(int utocimHracId);

	@Override
	public String toString()
	{
		return ""
			+ "id=" + id
			+ ", jsemClovek=" + jsemClovek
			+ ", jmeno=" + jmeno
		;
	}
}
