package com.gde.luzanky.lode.setup;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.setup.SetupManager.OnLodSetupListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberHraceListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberLodeListener;

class VyberLode
extends Table
implements OnVyberHraceListener, OnVyberLodeListener, OnLodSetupListener
{
	private int index;
	private Label vlevo;
	private Label vpravo;
	private Label jmeno;
	private VyberLodeVelikost velikost;
	private final ILodeScreenResources resources;
	private final SetupManager setupManager;
	private HracInfoSetup aktualniHrac;

	VyberLode(ILodeScreenResources resources, SetupManager setupManager)
	{
		this.resources = resources;
		this.setupManager = setupManager;
		this.aktualniHrac = setupManager.hrac.aktualni();
		this.index = setupManager.lode.findIndex(aktualniHrac);
		createContent(this);
		this.setupManager.listeners.addVyberHraceListener(this);
		this.setupManager.listeners.addVyberLodListener(this);
		this.setupManager.listeners.addLodSetupListener(this);
	}

	private Table createContent(Table wrapper)
	{
		LodInfoSetup lod = setupManager.lode.findByIndex(aktualniHrac.id, index);

		wrapper
			.add(jmeno = resources.createLabel("jmeno", FontSize.VyberLode.JMENO))
			.colspan(3)
			.growX()
		;
		wrapper.row();
		wrapper
			.add(vlevo = resources.createLabel("<", FontSize.VyberLode.VLEVO_VPRAVO))
		;
		wrapper
			.add(resources.createLabel("o", FontSize.VyberLode.JMENO)) // int
			.grow()
		;
		wrapper
			.add(vpravo = resources.createLabel(">", FontSize.VyberLode.VLEVO_VPRAVO))
		;
		wrapper.row();
		wrapper
			.add(velikost = new VyberLodeVelikost(resources, jmeno.getHeight()))
			.colspan(3)
			.growX()
		;
		wrapper.row();
		vlevo.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				index--;
				if (index < 0)
				{
					index = setupManager.lode.max() - 1;
				}
				onClickIndex(index);
			}
		});
		vpravo.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				index++;
				if (index >= setupManager.lode.max())
				{
					index = 0;
				}
				onClickIndex(index);
			}
		});
		update(setupManager.lode.findByIndex(aktualniHrac.id, index));
		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private void onClickIndex(int index)
	{
		LodInfoSetup lodInfo = setupManager.lode.findByIndex(aktualniHrac.id, index);
		update(lodInfo);
		for(OnVyberLodeListener listener : setupManager.listeners.vyberLode)
		{
			listener.onVyberLodeChanged(lodInfo);
		}
	}

	private void update(LodInfoSetup lodInfo)
	{
		jmeno.setText("[" + index + "] " + lodInfo.jmeno);
		velikost.update(lodInfo);
	}

	@Override
	public void onVyberHraceChanged(HracInfoSetup hracInfo)
	{
		aktualniHrac = hracInfo;
		index = setupManager.lode.findIndex(hracInfo);
		onClickIndex(index);
	}

	@Override
	public void onVyberLodeChanged(LodInfoSetup lodInfo)
	{
		index = setupManager.lode.findIndex(lodInfo);
		update(lodInfo);
	}

	@Override
	public void setupOn(LodInfo lodInfo)
	{
		LodInfoSetup lodSetup = (LodInfoSetup)lodInfo;
		if (lodSetup.hracId != aktualniHrac.id || lodSetup.id != aktualniHrac.vybranaLodId)
		{
			return;
		}
		update(lodSetup);
	}

	@Override
	public void setupOff(LodInfo lodInfo)
	{
		LodInfoSetup lodSetup = (LodInfoSetup)lodInfo;
		if (lodSetup.hracId != aktualniHrac.id || lodSetup.id != aktualniHrac.vybranaLodId)
		{
			return;
		}
		update(lodSetup);
	}
}
