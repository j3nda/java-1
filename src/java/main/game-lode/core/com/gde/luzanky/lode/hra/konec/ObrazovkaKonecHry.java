package com.gde.luzanky.lode.hra.konec;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.SdilenaObrazovka;
import com.gde.luzanky.lode.TypObrazovky;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.menu.ObrazovkaMenu;

/** herni obrazovka, zde se odehrava nase hra lode! */
public class ObrazovkaKonecHry
extends SdilenaObrazovka
{
	private final LodeGameConfiguration gameConfig;
	private final HracInfo vitez;
	private final HracInfo porazeny;
	private final boolean jsemVitez;
	private AnimationActor clickAnimation;
	/** cas, kdy nelze klikat na 'OK' pro presun do {@link ObrazovkaMenu} */
	private final float waitTimeLimit = 3f;
	private float waitTimeCurrent = 0f;

	public ObrazovkaKonecHry(
		LodeGame parentGame,
		TypObrazovky previousScreenType,
		LodeGameConfiguration gameConfig,
		HracInfo vitez,
		HracInfo porazeny,
		boolean jsemVitez
	)
	{
		super(parentGame, previousScreenType);
		this.gameConfig = gameConfig;
		this.vitez = vitez;
		this.porazeny = porazeny;
		this.jsemVitez = jsemVitez;

		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaKonecHry(jsemVitez): " + jsemVitez);
			System.out.println("ObrazovkaKonecHry(vitez): " + vitez);
			System.out.println("ObrazovkaKonecHry(porazeny): " + porazeny);
		}
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA_KONEC;
	}

	private AnimationActor createClickAnimation()
	{
		AnimationActor animation = new AnimationActor(0.015f, createClickFrames2(false), PlayMode.NORMAL);
		animation.setOrigin(Align.center);
		return animation;
	}

	private TextureRegion[] createClickFrames1(boolean flip)
	{
		int start = 395;
		int maxFrames = 35;
		TextureRegion[] frames = new TextureRegion[maxFrames];
		for(int i = start; i < start + maxFrames; i++)
		{
			frames[i - start] = new TextureRegion(
				resources.getAssetManager().get(String.format("animations/click/72x96/%03d.png", i), Texture.class)
			);
			if (flip)
			{
				frames[i - start].flip(true, false);
			}
		}
		return frames;
	}

	private TextureRegion[] createClickFrames2(boolean flip)
	{
		int start = 395;
		int maxFrames = 35;
		TextureRegion[] frames = new TextureRegion[maxFrames];
		for(int i = start; i < start + maxFrames; i++)
		{
			frames[i - start] = new TextureRegion(
				resources.getAssetManager().get(String.format("animations/click/679x903/%03d.png", i), Texture.class)
			);
			if (flip)
			{
				frames[i - start].flip(true, false);
			}
		}
		return frames;
	}

	@Override
	public void show()
	{
		super.show();
		clickAnimation = createClickAnimation();
		clickAnimation.setPosition(
			getWidth() / 2f,
			getHeight()/ 2f
		);
		clickAnimation.setColor(Color.YELLOW);

		getStage().addActor(clickAnimation);

		Table wrapper = new Table();

		wrapper.setSize(getWidth(), getHeight());
		wrapper
			.add(jsemVitez
				? resources.createLabel("VITEZ", FontSize.VyberHrace.JMENO, Color.GOLD)
				: resources.createLabel("PORAZENY", FontSize.VyberHrace.JMENO, Color.FIREBRICK)
			)
			.growX()
			.align(Align.center)
		;
		wrapper.row();

		wrapper
			.add(jsemVitez
				? resources.createLabel(vitez.jmeno, FontSize.VyberHrace.JMENO, Color.GOLD)
				: resources.createLabel(porazeny.jmeno, FontSize.VyberHrace.JMENO, Color.FIREBRICK)
			)
			.align(Align.center)
			.grow()
		;
		wrapper.row();

		wrapper
			.add(resources.createLabel("OK"))
			.growX()
		;
		wrapper.row();
		wrapper.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				if (waitTimeCurrent < waitTimeLimit)
				{
					return;
				}
				parentGame.setScreen(
					parentGame.getScreen(TypObrazovky.MENU)
				);
			}
		});
		getStage().addActor(wrapper);
	}

	@Override
	protected void renderScreen(float delta)
	{
		waitTimeCurrent += delta;

		getStage().act(delta);
		getStage().draw();

		if (clickAnimation != null && clickAnimation.isAnimationFinished())
		{
			clickAnimation.reset();
			clickAnimation.setColor(jsemVitez
				? Palette.removeColors(Palette.Hardware.Commodore.C64.Default(), 0)[RandomUtils.nextInt(0, Palette.Hardware.Commodore.C64.Default().length - 2)]
				: Palette.Hardware.Atari.Default()[RandomUtils.nextInt(0, Palette.Hardware.Atari.Default().length - 1)]
			);
			clickAnimation.setSize(
				RandomUtils.nextInt(100, 222),
				RandomUtils.nextInt(100, 222)
			);
			clickAnimation.setPosition(
				RandomUtils.nextInt(clickAnimation.getWidth(), getWidth() - clickAnimation.getWidth()),
				RandomUtils.nextInt(clickAnimation.getHeight(), getHeight() - clickAnimation.getHeight())
			);
			System.out.println(""
				+ clickAnimation.getX() + "x" + clickAnimation.getY()
				+ ", size=" + clickAnimation.getWidth() + "x" + clickAnimation.getHeight()
				+ ", color=" + clickAnimation.getColor()
			);
		}
	}
}
