package com.gde.luzanky.lode.setup;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.graphics.ui.SpriteActor;
import com.gde.common.utils.AnimationUtils;

class ObrazovkaSetupTlacitka
extends Table
{
	private static final String ICON_RANDOM = "icon-random-512x512.png";
	private static final String ICON_CLEAR = "icon-clear-512x512.png";
	private static final String ICON_BATTLE = "icon-battle-512x512.png";
	private final ILodeScreenResources resources;
	Actor randomButton;
	Actor clearButton;
	Actor battleButton;

	ObrazovkaSetupTlacitka(ILodeScreenResources resources)
	{
		this.resources = resources;
		createContent(this);
	}

	private void createContent(Table wrapper)
	{
		wrapper
			.add(clearButton = createButton(ICON_CLEAR))
			.growX()
		;
		wrapper
			.add(randomButton = createProgressButton(ICON_RANDOM))
			.growX()
		;
		wrapper.row();


//		Texture texture = new Texture(Gdx.files.internal("icon-random-512x512.png"));
		Texture texture = new Texture(Gdx.files.internal(ICON_BATTLE));
		TextureRegion textureRegion = new TextureRegion(texture);
		TextureRegionDrawable texRegionDrawable = new TextureRegionDrawable(textureRegion);
		ImageButton button = new ImageButton(texRegionDrawable); // Set the button up
		button.setColor(Color.RED);
//		button.getImage().setFillParent(true);

		wrapper
//			.add(button)
			.add(battleButton = createButton("icon-battle-512x512.png"))
			.colspan(2)
//			.size(128, 128)
//			.size(64, 64)
//			.height(111)
//			.width(111)
			.growX()
		;
	}

	private Actor createButton(String icon)
	{
		Image button = new Image(resources.getAssetManager().get(icon, Texture.class));
//		button.setScale(0.2f);
		button.setSize(50,50);
		return button;
	}

	private Actor createProgressButton(String icon)
	{
		Actor button = new ProgressButton(
			resources.getAssetManager().get(icon, Texture.class),
			new AnimationActor(
//				0.085f,
				0.025f,
				AnimationUtils.createFrames(
					resources.getAssetManager(),
//					"animations/clock-sand/187x187/%02d.png",
//					0, 2, 5, 7, 10, 12, 15, 17, 19, 22, 24, 27, 29, 32, 34, 36, 39, 41, 44, 46, 48
					"animations/clock-sand/110x110/%02d.png",
					0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
				),
				PlayMode.LOOP
			)
		);
		button.setSize(50, 50);
		return button;
	}

	private Actor createButtonS(String icon)
	{
		Sprite sprite = new Sprite(new Texture(Gdx.files.internal(icon)));
//		sprite.setScale(0.2f);
		Actor button = new SpriteActor(sprite);
		button.setScale(0.2f);
//		button.setWidth(100f);
//		button.getImage().setScale(0.25f);
//		button.pack();
//		button.layout();
//		button.validate();
//		button.getImage().setOrigin(Align.center);
//		button.debugAll();
		return button;
	}

	private Actor createButtonB(String icon)
	{
		ImageButton button = new ImageButton(
			new TextureRegionDrawable(
				new TextureRegion(
					new Texture(
						Gdx.files.internal(icon)
					)
				)
			)
		);
		button.setWidth(100f);
		button.getImage().setScale(0.25f);
		button.pack();
		button.layout();
		button.validate();
		button.getImage().setOrigin(Align.center);
		button.debugAll();
		return button;
	}

	class ProgressButton
	extends Stack
	{
		private boolean processing = false;
		private float processingTime;
		private final AnimationActor processingAnimation;
		private final Image icon;

		ProgressButton(Texture texture, AnimationActor processingAnimation)
		{
			super();
			add(this.icon = new Image(texture));
			add(this.processingAnimation = processingAnimation);
			stop();
		}

		boolean isProcessing()
		{
			return processing;
		}

		void start()
		{
			if (processing)
			{
				return;
			}
			processing = true;
			processingTime = 0;
			processingAnimation.reset();
			processingAnimation.setVisible(true);
		}

		void stop()
		{
			processing = false;
			processingAnimation.setVisible(false);
		}

		@Override
		public void act(float delta)
		{
			if (processing)
			{
				processingTime += delta;
				processingAnimation.act(delta);
			}
			icon.act(delta);
		}
	}
}
