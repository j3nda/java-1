package com.gde.luzanky.lode.hra;

import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;

public interface IHraLogika
{
	void onLodJeKompletneZnicena(LodInfo lod, HracInfo hrac, HraciPolicko policko);
	void onLodDostalaZasah(LodInfo lod, HracInfo hrac, HraciPolicko policko);
	void onLodDostalaZnovuZasah(LodInfo lod, HracInfo hrac, HraciPolicko policko);
	void onStrilimDoVody(HracInfo hrac, HraciPolicko policko);
}
