package com.gde.luzanky.lode.hra;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.resources.CommonResources;
import com.gde.common.utils.Point;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.SdilenaObrazovka;
import com.gde.luzanky.lode.TypObrazovky;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.hraci.HracPocitac;
import com.gde.luzanky.lode.hra.konec.ObrazovkaKonecHry;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha.OnTahHrace;
import com.gde.luzanky.lode.hra.plocha.HraciPlochaHrac;
import com.gde.luzanky.lode.hra.plocha.HraciPlochaPocitac;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;

/** herni obrazovka, zde se odehrava nase hra lode! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements OnTahHrace
{
	/** index hrace 'clovek' v poli {@link LodeGameConfiguration#hrac} */
	public static final int CLOVEK = 0;
	/** index hrace 'pocitac' v poli {@link LodeGameConfiguration#hrac} */
	public static final int POCITAC = 1;
	private final LodeGameConfiguration gameConfig;
	/** pole s hracimi plochou vsech hracu */
	private final HraciPlocha[] plocha;
	/** seznam posluchacu, kteri reaguji na logiku hry, napr: {@link HracPocitac} */
	private final List<IHraLogika> hraListeners;

	public ObrazovkaHry(LodeGame parentGame, TypObrazovky previousScreenType, LodeGameConfiguration gameConfig)
	{
		super(parentGame, previousScreenType);
		this.gameConfig = gameConfig;
		this.plocha = new HraciPlocha[gameConfig.hrac.length];
		this.hraListeners = new ArrayList<>();
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	@Override
	public void show()
	{
		super.show();

		Table wrapper = new Table();
		int velikostHraciPlochy = Math.min(getWidth(), getHeight());

		wrapper.setSize(getWidth(), getHeight());
		wrapper
			.add(vytvorHraciPlochu(gameConfig.hrac[POCITAC], POCITAC, gameConfig.velikost, velikostHraciPlochy))
			.align(Align.center)
			.width(velikostHraciPlochy)
			.height(velikostHraciPlochy)
		;

		wrapper
			.add(vytvorInformacniPanel(gameConfig, (int)((getWidth() - velikostHraciPlochy) * 0.3f)))
			.grow()
		;
		wrapper.row();
		getStage().addActor(wrapper);

		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry(pocetHracu): " + gameConfig.hrac.length);
			for (int i = 0; i < gameConfig.hrac.length; i++)
			{
				HracInfo hrac = gameConfig.hrac[i];
				System.out.println("ObrazovkaHry(hrac): {" + hrac + "}");

				LodInfo[] lode = hrac.lode;
				for (int j = 0; j < lode.length; j++)
				{
					LodInfo lod = lode[j];
					System.out.println("ObrazovkaHry(hracId=" + hrac.id + ", lod): {" + lod + "}");
				}
				int[][] R = hrac.rozmisteniLodi;
				for (int radek = 0; radek < R.length; radek++)
				{
					System.out.print("ObrazovkaHry(hracId=" + hrac.id + ", rozmisteniLodi(" + lode.length + "x):");
					for (int sloupec = 0; sloupec < R[radek].length; sloupec++)
					{
						int lodId = hrac.rozmisteniLodi[radek][sloupec];
						System.out.print(String.format("%3s" , lodId) + " | ");
					}
					System.out.println();
				}
			}
		}
	}

	private Color vratBarvuLode(int lodId)
	{
		// TODO: XHONZA: vratBarvuLode() sjednotit barevnou paletu
		// TODO: XHONZA: vratBarvuLode() indexBarvy odvodit od Math.min(lodId); abych vzdy zacinal index 0!
		Color[] barvy = Palette.Hardware.Commodore.C64.Default();
		int indexBarvy = lodId - 1;

		if (indexBarvy < barvy.length)
		{
			return barvy[indexBarvy];
		}
		return Color.WHITE;
	}

	private Table vytvorInformacniPanel(LodeGameConfiguration gameConfig, int velikostHraciPlochy)
	{
		Table wrapper = new Table();
		HracInfo clovek = gameConfig.hrac[CLOVEK];
		HracInfo pocitac= gameConfig.hrac[POCITAC];

		// clovek
		wrapper.add(resources.createLabel(">> " + clovek.jmeno)).growX();
		wrapper.row();

		wrapper
			.add(vytvorSeznamLodi(clovek))
			.grow()
		;
		wrapper.row();

		wrapper.add(resources.createLabel("111")).grow();
		wrapper.row();

		wrapper
			.add(vytvorHraciPlochu(clovek, CLOVEK, gameConfig.velikost, velikostHraciPlochy))
			.align(Align.center)
			.width(velikostHraciPlochy)
			.height(velikostHraciPlochy)
		;
		wrapper.row();

		wrapper.add(resources.createLabel("222")).grow();
		wrapper.row();

		// pocitac
		wrapper
			.add(resources.createLabel("<< " + pocitac.jmeno))
			.grow()
		;
		wrapper.row();

		if (LodeGame.isDebug())
		{
			wrapper.debugAll();
		}
		return wrapper;
	}

	private Actor vytvorSeznamLodi(HracInfo hracInfo)
	{
		Table seznam = new Table();
		for (int i = 0; i < hracInfo.lode.length; i++)
		{
			LodInfo lod = hracInfo.lode[i];
			Color barvaLode = vratBarvuLode(lod.id);

			Image barvaLodeImage = new Image(new Texture(CommonResources.Tint.square16x16));
			barvaLodeImage.setColor(barvaLode);

			seznam.add(resources.createLabel(lod.id + ""));
			seznam.add(barvaLodeImage).padLeft(10).padRight(10);
			seznam.add(resources.createLabel(lod.jmeno)).expandX();
			seznam.row();
		}
		return seznam;
	}

	/** vytvori hraci plochu pro konkretniho hrace podle 'chovaniHraciPlochy' (~clovek x pocitac) */
	private Actor vytvorHraciPlochu(HracInfo hrac, int chovaniHraciPlochy, int velikostDilku, int velikostPixelu)
	{
		HraciPlocha plocha;
		switch(chovaniHraciPlochy)
		{
			case CLOVEK:
			{
				plocha = new HraciPlochaHrac(resources, gameConfig, hrac, null);
				break;
			}
			case POCITAC:
			{
				plocha = new HraciPlochaPocitac(resources, gameConfig, hrac, this);
				break;
			}
			default:
			{
				throw new RuntimeException("invalid chovaniHraciPlochy!");
			}
		}
		plocha.setName("plocha-" + hrac.id);
		plocha.setSize(velikostDilku, velikostDilku);

		/**
		 * nove vytvorenou {@link HraciPlocha} (~promenna 'plocha')
		 * priradim do pole 'this.plocha' (~slovicko 'this' pouzivam, kdyz dojde ke kolizi jmen!)
		 */
		int indexHrace = vratIndexHrace(hrac.id);
		this.plocha[indexHrace] = plocha;

		if (hrac instanceof IHraLogika)
		{
			hraListeners.add((IHraLogika) hrac);
		}

		Stack stack = new Stack();

		stack.add(plocha);
		stack.add(vytvorHraciMrizku(
			velikostDilku,
			velikostPixelu,
			5,
//			(int)Math.max(Math.min(1, velikostPixelu * 0.01f), 2),
			Color.BLACK
		));

		return stack;
	}

	// TODO: XHONZA: refactor: toto by mohlo byt jako static na HraciPlocha
	private Image vytvorHraciMrizku(int dilku, int pixelSize, int lineSize, Color color)
	{
		Pixmap pm = new Pixmap(pixelSize, pixelSize, Format.RGBA8888);
		pm.setColor(color);

		// oramovani
		for(int i = 0; i < lineSize; i++)
		{
			pm.drawRectangle(
				i,
				i,
				pixelSize - (2 * i),
				pixelSize - (2 * i)
			);
		}
		// linky
		int lineDistance = pixelSize / dilku;
		for (int i = lineDistance; i < pixelSize; i += lineDistance)
		{
			pm.fillRectangle(0, i - lineSize / 2, pm.getWidth(), lineSize); // horizontal
			pm.fillRectangle(i - lineSize / 2, 0, lineSize, pm.getHeight()); // vertical
		}

		Image mrizka = new Image(new Texture(pm));
		mrizka.setSize(pixelSize, pixelSize);
		mrizka.setTouchable(Touchable.disabled);

		pm.dispose();

		return mrizka;
	}

	// TODO: XHONZA: hra/GAME-DESIGN: vykreslit board v dane velikosti a oznackovat ho (a1, b2, ...)
	// TODO: XHONZA: hra/GAME-DESIGN: vymyslet moznost prepinani (player x computer) --> stridani tahu
	// TODO: XHONZA: hra/GAME-DESIGN: vymyslet vizualizaci znicenych lodi (player x computer)
	// TODO: XHONZA: hra/GAME-DESIGN: tapani na okynka -> vyvola akci
	// TODO: XHONZA: hra/GAME-DESIGN: UI-cko ~ asi skore (??tahy nikoho nezajimaji??)
	// TODO: XHONZA: hra/GAME-DESIGN: ObrazovkaUmisteniLodi ~ vymyslet nejake easy ui-cko --> libgdx, zaklady ~ ?tableLayout?
	// TODO: XHONZA: hra/GAME-DESIGN: timeout pro aktualni tah (at to netrva vecne)

	@Override
	protected void renderScreen(float delta)
	{
		getStage().act(delta);
		getStage().draw();
	}

	/**
	 * zavola se v okamziku, kdyz kliknu na {@link HraciPolicko}(radek, sloupec) na {@link HraciPlocha}
	 * typicke pro:
	 * - jako 'hrajici hrac' utocim na 'pocitac', tj. klikam na jeho policka
	 * - zacnu provolavat herni logiku {@link ObrazovkaHry#tahHrace}
	 */
	@Override
	public void onTahHrace(int hracId, HraciPolicko policko)
	{
		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();
		do
		{
			tahHrace(hracId, radek, sloupec);

			// TODO: XHONZA: hra/GRAFIKA: pokracuju, az dobehnou vsechny animace...
			// TODO: hra/LOGIKA: nastal konec hry?
			if (jeKonecHry(hracId))
			{
				int porazenyIndex = vratIndexHrace(hracId);
				int vitezIndex = porazenyIndex + 1;
				if (vitezIndex >= gameConfig.hrac.length)
				{
					vitezIndex = 0;
				}
				onKonecHry(
					gameConfig.hrac[vitezIndex],
					gameConfig.hrac[porazenyIndex]
				);
				return;
			}

			// TODO: hra/LOGIKA: proved dalsi tah hrace, pokud jsem pocitac...
			HracInfo hrac = gameConfig.findHracById(hracId);
			if (hrac.jsemClovek)
			{
				// nepokracuju...
				// (utok na cloveka: nepokracuju! protoze clovek urcuje sam, na koho a kam bude utocit!)
				break;
			}

			// dalsi-tah...
			// (utoci pocitac, tj. pocitac definuje na koho a kam bude dal utocit)
			// TODO: grafika[animace dalsi-tah]

			// musim zjistit: dalsiho hrace v poradi, na ktereho vedu utok
			// TODO: hra/LOGIKA/bonus: HracInfo.tahHrace() by mohl tyto informace vracet
			int dalsiHracIndex = vratIndexHrace(hracId) + 1;
			if (dalsiHracIndex >= gameConfig.hrac.length)
			{
				dalsiHracIndex = 0;
			}
			HracInfo dalsiHrac = gameConfig.hrac[dalsiHracIndex];

			// TODO: hra/GRAFIKA: pocitac by mel premyslet (~vc. vizualizace); az skonci s premyslenim, je potreba jit dal...
			// TODO: XHONZA: hra/GRAFIKA: pokracuju, az dobehnou vsechny animace...
			Point utocimNaPolickoDalsihoHrace = hrac.tahHrace(dalsiHrac.id);

			// jsem v cyklu... nastavim: {hracId, radek, sloupec} pro dalsi utok
			hracId = dalsiHrac.id;
			radek = utocimNaPolickoDalsihoHrace.getY();
			sloupec = utocimNaPolickoDalsihoHrace.getX();
		}
		while(true);
	}

	/**
	 * herni logika tahu
	 * @param hracId na ktereho hrace probiha utok (~hracId)
	 * @param radek pozice policka, na ktere se utoci (~radek)
	 * @param sloupec pozice policka, na ktere se utoci (~sloupec)
	 */
	private void tahHrace(int hracId, int radek, int sloupec)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.tahHrace(hracId=" + hracId + ", hit=[" + radek + "," + sloupec + "])");
		}
		HraciPolicko policko = getStage().getRoot().findActor(HraciPolicko.formatPozice(hracId, radek, sloupec));
		boolean isPolickoClicked = policko.isClicked();
		onClickHraciPolicko(hracId, policko);

		// TODO: XHONZA: hra/GRAFIKA: pokracuju, az dobehnou vsechny animace...
		// TODO: hra/LOGIKA: strilim na lod? anebo do vody?
		if (jeTamLod(hracId, radek, sloupec))
		{
			LodInfo lod = vratLod(hracId, radek, sloupec);
			if (!isPolickoClicked)
			{
				lod.zasah();
				if (lod.jeZnicena())
				{
					onLodJeKompletneZnicena(lod, hracId, policko);
				}
				else
				{
					onLodDostalaZasah(lod, hracId, policko);
				}
			}
			else
			{
				onLodDostalaZnovuZasah(lod, hracId, policko);
			}
		}
		else
		{
			onStrilimDoVody(hracId, policko);
		}
	}

	/** vykonani 'grafiky', pokazde, kdyz metoda 'tahHrace()' provede tah */
	private void onClickHraciPolicko(int hracId, HraciPolicko policko)
	{
		// TODO: hra/GRAFIKA: vystel projektil, ktery zacili na dane hraciPolicko
		// TODO: XHONZA: hra/GRAFIKA: pokracuju, az dobehnou vsechny animace...
		HraciPlocha plocha = getStage().getRoot().findActor("plocha-" + hracId);
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onClickHraciPolicko(" + hracId + "): policko=[" + policko + "]");
		}
		plocha.onClickHraciPolicko(policko);
		// TODO: XHONZA: hra/GRAFIKA: pokracuju, az dobehnou vsechny animace...
		policko.click();
	}

	private void onKonecHry(HracInfo vitez, HracInfo porazeny)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onKonecHry(vitez):    hrac={" + vitez + "}");
			System.out.println("ObrazovkaHry.onKonecHry(porazeny): hrac={" + porazeny + "}");
		}
		// TODO: hra/GRAFIKA: ...chvili pauza(~vypnout klikani) -> pak: game over
		parentGame.setScreen(
			new ObrazovkaKonecHry(
				parentGame,
				TypObrazovky.MENU,
				gameConfig,
				vitez,
				porazeny,
				(gameConfig.hrac[CLOVEK].id == vitez.id)
			)
		);
	}

	private void onLodJeKompletneZnicena(LodInfo lod, int hracId, HraciPolicko policko)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onLodJeKompletneZnicena(" + hracId + "): hit=[" + policko + "]");
			System.out.println("ObrazovkaHry.onLodJeKompletneZnicena(" + hracId + "): lod={" + lod + "}");
		}
		// hra/logika: musim provolat funkcionalitu, aby se pocitac dozvedel, co se logickeho deje...
		for(IHraLogika listener : hraListeners)
		{
			listener.onLodJeKompletneZnicena(lod, gameConfig.hrac[vratIndexHrace(hracId)], policko);
		}
		// TODO: hra/GRAFIKA: onLodJeKompletneZnicena()
	}

	private void onLodDostalaZasah(LodInfo lod, int hracId, HraciPolicko policko)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onLodDostalaZasah(" + hracId + "): hit=[" + policko + "]");
			System.out.println("ObrazovkaHry.onLodDostalaZasah(" + hracId + "): lod={" + lod + "}");
		}
		// hra/logika: musim provolat funkcionalitu, aby se pocitac dozvedel, co se logickeho deje...
		for(IHraLogika listener : hraListeners)
		{
			listener.onLodDostalaZasah(lod, gameConfig.hrac[vratIndexHrace(hracId)], policko);
		}
		// TODO: hra/GRAFIKA: onLodDostalaZasah()
	}

	private void onLodDostalaZnovuZasah(LodInfo lod, int hracId, HraciPolicko policko)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onLodDostalaZnovuZasah(" + hracId + "): hit=[" + policko + "]");
			System.out.println("ObrazovkaHry.onLodDostalaZnovuZasah(" + hracId + "): lod={" + lod + "}");
		}
		// hra/logika: musim provolat funkcionalitu, aby se pocitac dozvedel, co se logickeho deje...
		for(IHraLogika listener : hraListeners)
		{
			listener.onLodDostalaZnovuZasah(lod, gameConfig.hrac[vratIndexHrace(hracId)], policko);
		}
		// TODO: hra/GRAFIKA: onLodDostalaZnovuZasah()
	}

	private void onStrilimDoVody(int hracId, HraciPolicko policko)
	{
		if (LodeGame.isDebug())
		{
			System.out.println("ObrazovkaHry.onStrilimDoVody(" + hracId + "): hit=[" + policko + "]");
		}
		// hra/logika: musim provolat funkcionalitu, aby se pocitac dozvedel, co se logickeho deje...
		for(IHraLogika listener : hraListeners)
		{
			listener.onStrilimDoVody(gameConfig.hrac[vratIndexHrace(hracId)], policko);
		}
		// TODO: hra/GRAFIKA: voda()
	}

	/** vraci true, pokud hracId prohral */
	boolean jeKonecHry(int hracId)
	{
		int hracIndex = vratIndexHrace(hracId);
		int pocetZasazenychLodi = 0;
		HracInfo hrac = gameConfig.hrac[hracIndex];
		for(int i = 0; i < hrac.lode.length; i++)
		{
			LodInfo lod = gameConfig.hrac[hracIndex].lode[i];
			if (lod.jeZnicena())
			{
				pocetZasazenychLodi++;
			}
		}
		if (LodeGame.isDebug())
		{
			System.out.println(
				"ObrazovkaHry.jeKonecHry(" + hracId + "): "
				+ (pocetZasazenychLodi == gameConfig.hrac[hracIndex].lode.length)
				+ " ~ (" + pocetZasazenychLodi + " ? " + gameConfig.hrac[hracIndex].lode.length + ")"
				+ ", hrac[" + hracIndex + "]={" + hrac + "}"
			);
		}
		return (pocetZasazenychLodi == gameConfig.hrac[hracIndex].lode.length);
	}

	LodInfo vratLod(int hracId, int radek, int sloupec)
	{
		int hracIndex = vratIndexHrace(hracId);
		int lodId = gameConfig.hrac[hracIndex].rozmisteniLodi[radek][sloupec];
		for (int i = 0; i < gameConfig.hrac[hracIndex].lode.length; i++)
		{
			if (gameConfig.hrac[hracIndex].lode[i].id == lodId)
			{
				return gameConfig.hrac[hracIndex].lode[i];
			}
		}
		return null;
	}

	boolean jeTamLod(int hracId, int radek, int sloupec)
	{
		int hracIndex = vratIndexHrace(hracId);
		int lodId = gameConfig.hrac[hracIndex].rozmisteniLodi[radek][sloupec];
		if (lodId == HraciPlocha.VODA_ID)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private int vratIndexHrace(int hracId)
	{
		for (int i = 0; i < gameConfig.hrac.length; i++)
		{
			if (gameConfig.hrac[i].id == hracId)
			{
				return i;
			}
		}
		return 0;
	}
}
