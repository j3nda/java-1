package com.gde.luzanky.lode;

import com.gde.common.exceptions.NotImplementedException;
import com.gde.common.resources.names.PlayerNamesManager;
import com.gde.common.resources.names.ShipNamesManager;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.hraci.HracClovek;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.hraci.HracPocitac;

public class LodeGameConfiguration
{
	public final int velikost;
	public final HracInfo[] hrac;
	private final ShipNamesManager jmenoLode;
	private final PlayerNamesManager jmenoHrace;
	public boolean debugGameplay;
	public final ObtiznostHry obtiznost;
	public enum ObtiznostHry { EASY, HARD }

	public LodeGameConfiguration(
		ShipNamesManager shipNameManager,
		PlayerNamesManager playerNamesManager,
		int velikost,
		ObtiznostHry obtiznost,
		boolean debugGameplay
	)
	{
		this.jmenoLode = shipNameManager;
		this.jmenoHrace = playerNamesManager;
		this.velikost = velikost;
		this.obtiznost = obtiznost;
		this.debugGameplay = debugGameplay;
		this.hrac = vytvorHrace(obtiznost);
	}

	private HracInfo[] vytvorHrace(ObtiznostHry obtiznost)
	{
		if (debugGameplay)
		{
			return new HracInfo[] {
				new HracPocitac(vytvorLode(velikost), obtiznost, jmenoHrace.random().name),
				new HracPocitac(vytvorLode(velikost), obtiznost, jmenoHrace.random().name),
			};
		}
		else
		{
			return new HracInfo[] {
				new HracClovek(vytvorLode(velikost), "P1"),
				new HracPocitac(vytvorLode(velikost), obtiznost, jmenoHrace.random().name),
			};
		}
	}

	private LodInfo[] vytvorLode(int velikost)
	{
		// TODO: HRA: pole[] ~ vytvor pole lodi, se kteryma budes hrat (~tj ktere umistis na hraci plochu)
		// TODO: HRA: switch ~ podle parametru 'velikost' muzes upravit velikost pole (~napr: 5 a 10)
		LodInfo[] lode;
		int i = 0;
		switch(velikost)
		{
			case 5:
			{
				//25x ~ 5x=1; 3x=2; 1x=5; ~ zabira:44%
				lode = new LodInfo[9];
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(5));
				break;
			}
			case 10:
			{
				//100x ~ 4x=1; 6x=2; 2x=4; 1x=6; ~ zabira:30%
				lode = new LodInfo[13];
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(1));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(2));
				lode[i++] = new LodInfo(jmenoLode.random(4));
				lode[i++] = new LodInfo(jmenoLode.random(4));
				lode[i++] = new LodInfo(jmenoLode.random(6));
				break;
			}
			default:
				throw new NotImplementedException("invalid velikost!");
		}
		return lode;
	}

	/** najde hrace podle hracId a vrati ho */
	public HracInfo findHracById(int hracId)
	{
		// TODO: HRA: najdi hracInfo podle id
		for(int i = 0; i < hrac.length; i++)
		{
			if (hracId == hrac[i].id)
			{
				return hrac[i];
			}
		}
		throw new NotImplementedException("tato situace nikdy nenastane!");
	}

	/** najde lod podle lodId a vrati ji */
	public LodInfo findLodById(int lodId)
	{
		// TODO: HRA: najdi lodInfo podle id
		for(int i = 0; i < hrac.length; i++)
		{
			for(int j = 0; j < hrac[i].lode.length; j++)
			{
				LodInfo lod = hrac[i].lode[j];
				if (lodId == lod.id)
				{
					return lod;
				}
			}
		}
		throw new NotImplementedException("tato situace nikdy nenastane!");
	}
}
