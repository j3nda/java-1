package com.gde.luzanky.lode.setup;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.graphics.ui.SpriteActor;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.setup.SetupManager.OnLodSetupListener;

class VyberLodeVelikost
extends Table
implements OnLodSetupListener
{
	private final ILodeScreenResources resources;
	private final TextureRegion[] tintTextures;
	private final Color[] tintColors = new Color[] {
		Color.WHITE, // available
		Color.BLUE,  // selected
		Color.GREEN, // completed
		null,        // custom(~replacedByConstructor)
	};
	private LodInfoSetup lodInfo;
	private final float height;
	private final Label nazevLode;

	VyberLodeVelikost(ILodeScreenResources resources, float height)
	{
		this(resources, height, false);
	}

	VyberLodeVelikost(ILodeScreenResources resources, float height, boolean zobrazNazev)
	{
		this.resources = resources;
		this.height = height;
		this.tintColors[3] = null; // TODO: tintColor;
		tintTextures = new TextureRegion[] {
			new TextureRegion(resources.getAssetManager().get(CommonResources.Tint.square16x16, Texture.class)),
			new TextureRegion(resources.getAssetManager().get(CommonResources.Tint.square16x16, Texture.class)),
		};
		setHeight(height);
		nazevLode = (zobrazNazev
			? resources.createLabel("n/a", FontSize.VyberLode.JMENO, Color.FIREBRICK)
			: null
		);
	}

	private void updateContent()
	{
		clear();
		createContent(this);
	}

	private void createContent(Table wrapper)
	{
		Table sizeTable = new Table();
		SpriteActor img;
		for(int i = 0; i < lodInfo.velikost; i++)
		{
			boolean selected = lodInfo.selected() > i;
			boolean completed = lodInfo.jeKompletni();
			Color color = (tintColors[3] == null
				? tintColors[completed ? 2 : (selected ? 1 : 0)]
				: tintColors[3]
			);
			img = new SpriteActor(tintTextures[selected ? 1 : 0]);
			img.setSize(height, height);
			img.setColor(color);

			sizeTable
				.add(img)
			;
		}
		Actor rowElement = sizeTable;
		if (nazevLode != null)
		{
			Stack stack = new Stack();
			stack.add(sizeTable);
			stack.add(nazevLode);

// TODO: XHONZA: lepsi zobrazeni nazvu lode - vc. indikace, zda je completed aj.
//			Color color = Color.BLUE;
//			color.a = 0.5f;
//			nazevLode.getStyle().fontColor = color;

			rowElement = stack;
		}
		wrapper
			.add(rowElement)
			.align(Align.center)
			.height(height)
			.expandX()
		;
		wrapper
			.row()
			.height(height)
		;
		if (LodeGame.isDebug())
		{
			sizeTable.debug();
			wrapper.debug();
		}
	}

	void update(LodInfoSetup lodInfo)
	{
		this.lodInfo = lodInfo;
		updateContent();
		if (nazevLode != null)
		{
			nazevLode.setText(lodInfo.jmeno);
		}
	}

	@Override
	public void setupOn(LodInfo lodInfo)
	{
		if (this.lodInfo != lodInfo)
		{
			return;
		}
		updateContent();
	}

	@Override
	public void setupOff(LodInfo lodInfo)
	{
		if (this.lodInfo != lodInfo)
		{
			return;
		}
		updateContent();
	}
}
