package com.gde.luzanky.lode.setup;

import java.util.Set;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;
import com.gde.luzanky.lode.setup.SetupManager.OnRozmisteniLodeListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberLodeListener;

class HraciPlochaSetup
extends HraciPlocha
implements OnVyberLodeListener, OnRozmisteniLodeListener
{
	private LodInfoSetup aktualniLod = null;
	private final SetupManager setupManager;
	RunnableAction rozmisteniAction;
	private final Set<Integer> mojeLode;
	private boolean rozmisteniInProgress;

	HraciPlochaSetup(ILodeScreenResources resources, SetupManager setupManager, HracInfo hrac)
	{
		super(resources, setupManager.gameConfig, hrac, null);
		this.setupManager = setupManager;
		this.setupManager.listeners.addVyberLodListener(this);
		this.setupManager.listeners.addLodRozmisteniListener(this);
		this.mojeLode = setupManager.hrac.lode(hrac.id);
		// TODO: XHONZA: oramovani-kolem-dokola; tenke carky uvnitr;
	}

	@Override
	protected HraciPolicko vytvorHraciPolicko(int radek, int sloupec)
	{
		HraciPolickoSetup policko = new HraciPolickoSetup(resources, radek, sloupec);
		policko.addListener(new HraciPolickoClickListener(policko)
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				onClickHraciPolicko(policko);
			}
		});
		return policko;
	}

	@Override
	public void onClickHraciPolicko(HraciPolicko policko)
	{
		if (rozmisteniInProgress)
		{
			return;
		}
		HraciPolickoSetup setup = (HraciPolickoSetup) policko;
		onUpdateHraciPolicko(setup, false);

		if (LodeGame.isDebug())
		{
			System.out.println("onClickHraciPolicko: pos=" + setup + ", lod{" + aktualniLod + "}");
			System.out.println(toString());
		}
	}

	/** aktualizuje umisteni {@link HraciPolickoSetup} */
	private void onUpdateHraciPolicko(HraciPolickoSetup policko, boolean rozmisteni)
	{
		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();
		int[][] poziceLodi = getRozmisteniLodi();

		// TODO: __deleteImplementation NASTAVENI: 1) kdyz vyberu 'prazdny dilek' -> umisti aktualniLod.id; a proved kontrolu, ze:
		// TODO: __deleteImplementation NASTAVENI: 1.1) 'aktualni lod' neni plne nakonfigurovana (~aktualniLod.jeKompletni)
		// TODO: NASTAVENI: 1.2) vyber dilku vuci 'aktualni lod' je souvisle (~tj. ctverecky navazuji na sebe)
		// TODO: __deleteImplementation NASTAVENI: 3) kdyz vyberu 'obsazene pole' (obsazenePole.id == aktualniLod.id) -> umisti 'prazdny dilek'
		// TODO: __deleteImplementation NASTAVENI: 4) kdyz vyberu 'obsazene pole' (obsazenePole.id != aktualniLod.id) -> aktualizuj vyber lode
		// TODO: __deleteImplementation NASTAVENI: aktualizuj grafiku 'HraciPolicko'
		if (poziceLodi[radek][sloupec] == HraciPlocha.VODA_ID && !aktualniLod.jeKompletni())
		{
			poziceLodi[radek][sloupec] = aktualniLod.id;
			policko.nastavLod(aktualniLod);
		}
		else
		if (rozmisteni)
		{
			return;
		}
		else
		if (poziceLodi[radek][sloupec] == aktualniLod.id)
		{
			poziceLodi[radek][sloupec] = HraciPlocha.VODA_ID;
			policko.prazdne(aktualniLod);
		}
		else
		if (poziceLodi[radek][sloupec] != HraciPlocha.VODA_ID && poziceLodi[radek][sloupec] != aktualniLod.id)
		{
			LodInfoSetup lodInfo = setupManager.lode.findById(poziceLodi[radek][sloupec]);
			for(OnVyberLodeListener listener : setupManager.listeners.vyberLode)
			{
				listener.onVyberLodeChanged(lodInfo);
			}
		}
	}

	@Override
	public void onVyberLodeChanged(LodInfoSetup lodInfo)
	{
		if (!mojeLode.contains(lodInfo.id) || (aktualniLod != null && aktualniLod.id == lodInfo.id))
		{
			return;
		}
		aktualniLod = lodInfo;
	}

	public void vycisti()
	{
		// TODO: __deleteImplementation NASTAVENI: napis implementaci, ktera vycisti hraciPlochu i HraciPolicko od vsech nastaveni
		// - NEZAPOMEN: hraciPolicko najdes pomoci: HraciPolicko.formatPozice(radek, sloupec);
		for(int radek = 0; radek < velikost; radek++)
		{
			for(int sloupec = 0; sloupec < velikost; sloupec++)
			{
				getRozmisteniLodi()[radek][sloupec] = VODA_ID;
				HraciPolickoSetup hraciPolicko = findActor(hraciPolickoName(radek, sloupec));
				if (hraciPolicko != null)
				{
					hraciPolicko.prazdne(aktualniLod);
				}
			}
		}
	}

	public void nahodneRozmistiZbyvajiciLode()
	{
		if (rozmisteniAction != null)
		{
			return;
		}
		HraciPlochaSetup hraciPlocha = this;
		addAction(rozmisteniAction = Actions.run(new Runnable()
		{
			@Override
			public void run()
			{
				setupManager.lode.rozmistiLode(hraciPlocha);
			}
		}));
	}

	@Override
	public void onRozmisteniStarted(HraciPlochaSetup hraciPlocha)
	{
		if (hraciPlocha.getHracId() != getHracId())
		{
			return;
		}
		rozmisteniInProgress = true;
	}

	@Override
	public void onRozmisteniFinished(HraciPlochaSetup hraciPlocha)
	{
		if (hraciPlocha.getHracId() != getHracId())
		{
			return;
		}
		rozmisteniInProgress = false;
	}

	@Override
	public void onRozmisteniCancelled(HraciPlochaSetup hraciPlocha)
	{
		if (hraciPlocha.getHracId() != getHracId())
		{
			return;
		}
		rozmisteniInProgress = false;
	}

	@Override
	public void onRozmisteniUpdate(HraciPolicko hraciPolicko, HraciPlochaSetup hraciPlocha, LodInfoSetup lodInfo)
	{
		if (hraciPlocha.getHracId() != getHracId())
		{
			return;
		}
		onUpdateHraciPolicko((HraciPolickoSetup)hraciPolicko, rozmisteniInProgress);
	}

	class HraciPolickoSetup
	extends HraciPolicko
	{
		private final Label poziceLabel;

		HraciPolickoSetup(ILodeScreenResources resources, int radek, int sloupec)
		{
			super(radek, sloupec);
			// TODO: XHONZA: pozadi ~ voda
			// TODO: XHONZA: vybrane pole s lod.id (aktualni)
			// TODO: XHONZA: potvrzene pole s lod.id (jiz vybrane)
			add(poziceLabel = resources.createLabel(formatPozice(), FontSize.HraciPlocha.Policko.NAZEV));
		}

		void prazdne(LodInfoSetup lodInfo)
		{
			poziceLabel.setText(formatPozice());
			lodInfo.setupOff();
		}

		void nastavLod(LodInfoSetup lodInfo)
		{
			poziceLabel.setText(
				"["
				+ lodInfo.velikost
				+ "."
				+ lodInfo.jmeno.charAt(0)
				+ "]"
			);
			lodInfo.setupOn();
		}
	}
}
