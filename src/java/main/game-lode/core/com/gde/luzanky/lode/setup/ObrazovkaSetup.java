package com.gde.luzanky.lode.setup;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.ui.SpriteActor;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.SdilenaObrazovka;
import com.gde.luzanky.lode.TypObrazovky;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.ObrazovkaHry;
import com.gde.luzanky.lode.hra.hraci.HracClovek;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.hraci.HracPocitac;
import com.gde.luzanky.lode.setup.SetupManager.MyClickListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberHraceListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberLodeListener;

/** setup obrazovka, tady nastavuju a rozmistuju jednotlive lode! */
public class ObrazovkaSetup
extends SdilenaObrazovka
implements OnVyberHraceListener, OnVyberLodeListener
{
	private final static String font = CommonResources.Font.vt323;
	private final SetupManager setupManager;
	private VyberLode vyberLode;
	private LodeStatusTable infoTable;
	private ObrazovkaSetupTlacitka ovladaciTlacitka;
	private Table hraciTable;
	private final Map<Integer, HraciPlochaSetup> hraciPlocha;
	private DebugGameplayCheckBox debugGameplayButton = null;

	public ObrazovkaSetup(LodeGame parentGame, TypObrazovky previousScreenType, LodeGameConfiguration gameConfig)
	{
		super(parentGame, previousScreenType);
		this.setupManager = new SetupManager(gameConfig);
		this.hraciPlocha = new HashMap<>();
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA_SETUP;
	}

	@Override
	protected void renderScreen(float delta)
	{
		getStage().act(delta);
		getStage().draw();
	}

	@Override
	public void show()
	{
		super.show();

		Table wrapper = new Table();
		int velikostPixel = Math.min(getWidth(), getHeight());
		wrapper.setSize(getWidth(), getHeight());
		wrapper
			.add(vytvorHraciPlochyProVsechnyHrace(setupManager.gameConfig.hrac))
			.align(Align.center)
			.width(velikostPixel)
			.height(velikostPixel)
		;
		wrapper
			.add(vytvorOvladaciPrvky())
			.align(Align.center)
			.grow()
		;
		getStage().addActor(wrapper);

		setupManager.listeners.addVyberHraceListener(this);
		setupManager.listeners.addVyberLodListener(this);

		// musim nastartovat logiku pro vymenu obsahu... (~hrac, lod)
		HracInfoSetup startujiciHrac = setupManager.hrac.aktualni();
		for(OnVyberHraceListener listener : setupManager.listeners.vyberHrace)
		{
			listener.onVyberHraceChanged(startujiciHrac);
		}
	}

	/** vytvori hraci plochu pro vsechny hrace, tj. vc. pocitace */
	private Stack vytvorHraciPlochyProVsechnyHrace(HracInfo[] hraci)
	{
		Stack stack = new Stack();
		for (int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];

			HraciPlochaSetup hraciPlocha = vytvorHraciPlochu(hrac);
			hraciPlocha.setVisible(i == 0);

			stack.add(hraciPlocha);
			this.hraciPlocha.put(hrac.id, hraciPlocha);
		}
		return stack;
	}

	/** vytvori hraci plochu pro konkretniho hrace */
	private HraciPlochaSetup vytvorHraciPlochu(HracInfo hrac)
	{
		HraciPlochaSetup wrapper = new HraciPlochaSetup(resources, setupManager, hrac);

		int velikostPixel = Math.min(getWidth(), getHeight());
		wrapper.setName("plocha-" + hrac.id);
		wrapper.setSize(velikostPixel, velikostPixel);

		return wrapper;
	}

	private Table vytvorOvladaciPrvky()
	{
		vyberLode = new VyberLode(resources, setupManager);
		infoTable = new LodeStatusTable(resources, setupManager);

		Table wrapper = new Table();

		if (LodeGame.isDebug())
		{
			wrapper
				.add(hraciTable = vytvorHrace())
				.grow()
			;
			wrapper.row();
		}

		wrapper
			.add(infoTable)
			.grow()
		;
		wrapper.row();

		wrapper
			.add(ovladaciTlacitka = vytvorOvladaciTlacitka())
		;
		wrapper.row();

		wrapper
			.add(vyberLode)
			.height(getHeight() * 0.25f)
			.growX()
		;
		wrapper.row();

		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Table vytvorHrace()
	{
		Table wrapper = new Table();
		boolean first = false;
		HracInfo[] hraci = setupManager.gameConfig.hrac;
		for(int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];
			if (!LodeGame.isDebug() && !hrac.jsemClovek)
			{
				continue;
			}
			Label hracLabel = resources.createLabel(hrac.jmeno, FontSize.VyberHrace.JMENO);
			hracLabel.addListener(new MyClickListener<HracInfoSetup>(i == 0 ? setupManager.hrac.aktualni() : new HracInfoSetup(hrac))
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					for(OnVyberHraceListener listener : setupManager.listeners.vyberHrace)
					{
						listener.onVyberHraceChanged(clickInfo);
					}
				}
			});
			SpriteActor selected = new SpriteActor(
				new TextureRegion(resources.getAssetManager().get(CommonResources.Tint.square16x16, Texture.class))
			);
			selected.setColor(Color.YELLOW);
			selected.setSize(50, 50);
			selected.setName("hrac-" + hrac.id);
			selected.setVisible(!first);

			Stack stack = new Stack();
			stack.add(selected);
			stack.add(hracLabel);

			wrapper
				.add(stack)
				.grow()
			;

			// checkbox: pro debugGameplay
			if (LodeGame.isDebug() && !first)
			{
				wrapper
					.add(debugGameplayButton = vytvorDebugGameplayCheckbox())
				;
			}

			if (!first)
			{
				first = true;
			}
		}
		wrapper.row();
		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private DebugGameplayCheckBox vytvorDebugGameplayCheckbox()
	{
		Pixmap pm = new Pixmap(1, 1, Format.RGB888);

		// off
		pm.setColor(Color.BLACK);
		pm.fill();
		Drawable debugGameplayOff = new TextureRegionDrawable(new TextureRegion(new Texture(pm)));

		// on
		pm.setColor(Color.WHITE);
		pm.fill();
		Drawable debugGameplayOn = new TextureRegionDrawable(new TextureRegion(new Texture(pm)));

		pm.dispose();
		CheckBox checkbox = new CheckBox("", new CheckBoxStyle(
			debugGameplayOff,
			debugGameplayOn,
			resources.getFontsManager().getFont(font, FontSize.VyberHrace.JMENO),
			Color.WHITE
		));
		checkbox.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				// tweaking debugGameplay into gameConfig in dirty way!
				setupManager.gameConfig.debugGameplay = debugGameplayButton.checkbox.isChecked();
				HracInfo hrac = setupManager.gameConfig.hrac[ObrazovkaHry.CLOVEK];
				setupManager.gameConfig.hrac[ObrazovkaHry.CLOVEK] = (setupManager.gameConfig.debugGameplay
					? new HracPocitac(
						hrac.id,
						hrac.lode,
						hrac.rozmisteniLodi,
						setupManager.gameConfig.obtiznost,
						resources.getPlayerNamesManager().random().name
					  )
					: new HracClovek (
						hrac.id,
						hrac.lode,
						hrac.rozmisteniLodi,
						resources.getPlayerNamesManager().random().name
					  )
				);
			}
		});
		debugGameplayOff.setMinWidth(FontSize.VyberHrace.JMENO);
		debugGameplayOff.setMinHeight(FontSize.VyberHrace.JMENO);
		debugGameplayOn.setMinWidth(FontSize.VyberHrace.JMENO);
		debugGameplayOn.setMinHeight(FontSize.VyberHrace.JMENO);

		return new DebugGameplayCheckBox(
			checkbox,
			new Label(
				"vs",
				resources.getFontsManager().getLabelStyle(font, 33, Color.GRAY)
			)
		);
	}

	private ObrazovkaSetupTlacitka vytvorOvladaciTlacitka()
	{
		ObrazovkaSetupTlacitka tlacitka = new ObrazovkaSetupTlacitka(resources);
		tlacitka.randomButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				setupManager.lode.rozmistiLode(
					hraciPlocha.get(setupManager.hrac.aktualni().id)
				);
			}
		});
		tlacitka.clearButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				hraciPlocha.get(setupManager.hrac.aktualni().id).vycisti();

				LodInfo[] lode = setupManager.hrac.aktualni().lode;
				for(int i = 0; i < lode.length; i++)
				{
					LodInfo lod = lode[i];
					setupManager.lode.findById(lod.id).reset();
				}
			}
		});
		tlacitka.battleButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				startHry();
			}
		});
		return tlacitka;
	}

	private void startHry()
	{
		// nahodne rozmisti lode, pokud (hrac == pocitac)
		HracInfo[] hraci = setupManager.gameConfig.hrac;
		for(int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];
			if (hrac.jsemClovek || setupManager.lode.jeRozmisteniLodiKompletni(hrac.id))
			{
				continue;
			}
			setupManager.lode.rozmistiLode(
				hraciPlocha.get(hrac.id)
			);
		}
		if (setupManager.lode.jeRozmisteniLodiKompletni())
		{
			// aktualizuju rozmisteniLodi v 'gameConfig' pro jednotlive hrace!
			// (trosku prasarna, ale ObrazovkaSetup ma zodpovednost tady a ObrazovkaHry zase jinde!)
			for(int i = 0; i < hraci.length; i++)
			{
				HracInfo hrac = hraci[i];
				setupManager.gameConfig.hrac[i] = (hrac.jsemClovek
					? new HracClovek(hrac, hraciPlocha.get(hrac.id).getRozmisteniLodi())
					: new HracPocitac((HracPocitac)hrac, hraciPlocha.get(hrac.id).getRozmisteniLodi())
				);
			}
			parentGame.setScreen(
				new ObrazovkaHry(
					parentGame,
					getPreviousScreen(),
					setupManager.gameConfig
				)
			);
		}
		else
		{
			// TODO: XHONZA: nejaky warning, ze neni rozmisteni kompletni!
		}
	}

	@Override
	public void onVyberHraceChanged(HracInfoSetup hracInfo)
	{
		for(HraciPlochaSetup plocha : hraciPlocha.values())
		{
			plocha.setVisible(false);
			hraciTable.findActor("hrac-" + plocha.getHracId()).setVisible(false);
		}
		hraciPlocha.get(hracInfo.id).setVisible(true);
		hraciTable.findActor("hrac-" + hracInfo.id).setVisible(true);
	}

	@Override
	public void onVyberLodeChanged(LodInfoSetup lodInfo)
	{
		HracInfoSetup aktualniHrac = setupManager.hrac.aktualni();
		if (aktualniHrac.id != lodInfo.hracId)
		{
			return;
		}
		setupManager.hrac.aktualni().vybranaLodId = lodInfo.id;
	}

	private class DebugGameplayCheckBox
	extends Stack
	{
		public final CheckBox checkbox;
		public final Label label;

		DebugGameplayCheckBox(CheckBox checkbox, Label label)
		{
			this.checkbox = checkbox;
			this.label = label;
			this.label.setAlignment(Align.center);
			this.label.setTouchable(Touchable.disabled);
			add(checkbox);
			add(label);
		}
	}
}
