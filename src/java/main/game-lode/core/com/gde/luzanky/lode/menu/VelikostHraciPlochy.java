package com.gde.luzanky.lode.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.lode.LodeGame;

class VelikostHraciPlochy
extends Table
{
	private final static String font = CommonResources.Font.vt323;
	private final ILodeScreenResources resources;
	private final VelikostInfo[] variants = new VelikostInfo[] {
		new VelikostInfo(5, "5x5"),
		new VelikostInfo(10, "10x10"),
	};
	private int current = 0;
	private Table board;
	private Label label;
	private final ClickListener clickListener;

	VelikostHraciPlochy(ILodeScreenResources resources)
	{
		this.resources = resources;
		add(board = createBoard(variants[current].size));
		row();
		add(label = createLabel(variants[current].name));
		row();

		clickListener = new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				onSwitchBoard();
			}
		};
		addListener(clickListener);
		board.addListener(clickListener);
		label.addListener(clickListener);

		if (LodeGame.isDebug())
		{
			debug();
		}
	}

	private void onSwitchBoard()
	{
		current++;
		if (current >= variants.length)
		{
			current = 0;
		}
		Cell<Table> cell = getCell(board);
		cell.setActor(
			board = createBoard(variants[current].size)
		);
		board.addListener(clickListener);
		label.setText(variants[current].name);
		layout();
	}

	private Table createBoard(int size)
	{
		Table wrapper = new Table();
		for(int row = 0; row < size; row++)
		{
			for(int col = 0; col < size; col++)
			{
				wrapper
					.add(new Label(
						row + "x" + col,
						resources.getFontsManager().getLabelStyle(font, 33, Color.RED)
					))
				;
			}
			wrapper.row();
		}
		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Label createLabel(String name)
	{
		return new Label(
			name,
			resources.getFontsManager().getLabelStyle(font, 33, Color.RED)
		);
	}

	int getVelikost()
	{
		return variants[current].size;
	}

	private static class VelikostInfo
	{
		public final int size;
		public final String name;

		VelikostInfo(int size, String name)
		{
			this.size = size;
			this.name = name;
		}
	}
}
