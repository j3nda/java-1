package com.gde.luzanky.lode.hra.plocha.policko;

import com.badlogic.gdx.scenes.scene2d.ui.Stack;


public abstract class HraciPolicko
extends Stack
{
	/** radek (~pozice policka) */
	protected final int radek;
	/** sloupec (~pozice policka) */
	protected final int sloupec;
	/** pocet kliknuti na policko */
	private int pocetKliknuti = 0;

	protected HraciPolicko(int radek, int sloupec)
	{
		this.radek = radek;
		this.sloupec = sloupec;
		setName(formatPozice());
	}

	public static String formatPozice(int radek, int sloupec)
	{
		return radek + "," + sloupec;
	}

	public static String formatPozice(int hracId, int radek, int sloupec)
	{
		return hracId + ":" + formatPozice(radek, sloupec);
	}

	protected String formatPozice()
	{
		return formatPozice(radek, sloupec);
	}

	public int getRadek()
	{
		return radek;
	}

	public int getSloupec()
	{
		return sloupec;
	}

	@Override
	public String toString()
	{
		return formatPozice();
	}

	/** klikne na policko (~zaregistruje kliknuti) */
	public void click()
	{
		pocetKliknuti++;
	}

	/** vrati pocet kliknuti na policko */
	public int getClickCounter()
	{
		return pocetKliknuti;
	}

	/** vrati true, pokud bylo na policko kliknuto */
	public boolean isClicked()
	{
		return (pocetKliknuti > 0);
	}

	public interface IHraciPolickoWithText
	{
		void setText(String text);
		String getText();
	}
}
