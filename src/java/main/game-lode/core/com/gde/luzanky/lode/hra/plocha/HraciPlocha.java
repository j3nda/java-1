package com.gde.luzanky.lode.hra.plocha;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.utils.MathUtils;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.LodeGameConfiguration.ObtiznostHry;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko.IHraciPolickoWithText;

/** hraci plocha: kde se odehrava bitva */
public abstract class HraciPlocha
extends Table
implements IHraciPlocha
{
	/** id policka, predstavujici 'prazdne policko', tj. 'voda' */
	public static final int VODA_ID = -1;
	/** nazev policka, predstavujici 'prazdne policko', tj. 'voda' */
	protected static final String VODA_TEXT = "voda";
	protected final ILodeScreenResources resources;
	protected final int velikost;
	protected final HracInfo hrac;
	protected final OnTahHrace onTahHraceListener;
	protected final ObtiznostHry obtiznost;
	protected final LodeGameConfiguration gameConfig;

	public HraciPlocha(ILodeScreenResources resources, LodeGameConfiguration gameConfig, HracInfo hrac, OnTahHrace onTahHraceListener)
	{
		this.resources = resources;
		this.gameConfig = gameConfig;
		this.velikost = gameConfig.velikost;
		this.obtiznost = gameConfig.obtiznost;
		this.hrac = hrac;
		this.onTahHraceListener = onTahHraceListener;
		vytvorHraciPlochu(this, hrac.rozmisteniLodi);
		if (LodeGame.isDebug())
		{
			debugAll();
		}
	}

	@Override
	public int getHracId()
	{
		return hrac.id;
	}

	@Override
	public int[][] getRozmisteniLodi()
	{
		return hrac.rozmisteniLodi;
	}

	/** vytvori hraci plochu */
	protected void vytvorHraciPlochu(Table wrapper, int[][] poziceLodi)
	{
		for(int radek = 0; radek < velikost; radek++)
		{
			for(int sloupec = 0; sloupec < velikost; sloupec++)
			{
				HraciPolicko policko = vytvorHraciPolicko(radek, sloupec);
				policko.setName(hraciPolickoName(radek, sloupec));
				wrapper
					.add(policko)
					.align(Align.center)
					.grow()
				;
			}
			wrapper.row();
		}
	}

	public String hraciPolickoName(int radek, int sloupec)
	{
		return hrac.id + ":" + HraciPolicko.formatPozice(radek, sloupec);
	}

	protected Color vratBarvuLode(int lodId)
	{
		int minLodId = Integer.MAX_VALUE;
		for(LodInfo lod : hrac.lode)
		{
			if (lod.id < minLodId)
			{
				minLodId = lod.id;
			}
		}
		Color[] barvy = Palette.Hardware.Commodore.C64.Default();
		int indexBarvy = lodId - minLodId;
		if (indexBarvy < barvy.length)
		{
			return barvy[indexBarvy];
		}
		return Color.WHITE;
	}

	/** vytvori hraci policko */
	protected abstract HraciPolicko vytvorHraciPolicko(int radek, int sloupec);

	/** zavola se v pripade, ze na policko kliknu */
	@Override
	public void onClickHraciPolicko(HraciPolicko policko)
	{
		// TODO: hra/GAME-PLAY: indikace, kam jsem klikl => obtiznost{EASY = vzdy!; HARD = pouze u hrac[0]!}
		if (!policko.isClicked())
		{
			IHraciPolickoWithText hpolicko = (IHraciPolickoWithText) policko;
			hpolicko.setText("[" + hpolicko.getText() + "]");
		}
	}

	@Override
	public String toString()
	{
		return ToString.fromInteger(getRozmisteniLodi());
	}

	protected class HraciPolickoClickListener
	extends ClickListener
	{
		protected final HraciPolicko policko;

		protected HraciPolickoClickListener(HraciPolicko policko)
		{
			this.policko = policko;
		}
	}

	public interface OnTahHrace
	{
		void onTahHrace(int hracId, HraciPolicko policko);
	}

	public static class ToString
	{
		public static String fromBoolean(boolean[][] pozice)
		{
			String voda = "-";
			String hit = "x";
			int velikost = pozice.length;
			int sirka = MathUtils.max(
				(velikost + "").length(),
				voda.length(),
				hit.length()
			);
			StringBuilder sb = new StringBuilder();
			String colTitle = ".";
			String colRow = "|";
			String colNum = " ";
			String formatTitle = "%" + (sirka + colTitle.length()) + "s";
			String formatTitleRow = "%" + (sirka + colRow.length()) + "s";
			String formatTitleNum = "%" + (sirka + colNum.length()) + "s";

			sb.append(String.format(formatTitleRow, "").replace(" ", "_"));
			for(int sloupec = 0; sloupec < velikost; sloupec++)
			{
				sb.append(String.format(formatTitle, sloupec + "."));
			}
			sb.append("\n");

			for(int radek = 0; radek < velikost; radek++)
			{
				sb.append(String.format(formatTitle, radek + colRow));
				for(int sloupec = 0; sloupec < velikost; sloupec++)
				{
					sb.append(
						String.format(
							formatTitleNum,
							(pozice[radek][sloupec] ? hit : voda) + colNum
						)
					);
				}
				sb.append("\n");
			}

			return sb.toString();
		}

		public static String fromInteger(int[][] pozice)
		{
			int velikost = pozice.length;
			int sirka = MathUtils.max(
				(velikost + "").length(),
				(MathUtils.max(pozice) + "").length(),
				(MathUtils.min(pozice) + "").length()
			);
			StringBuilder sb = new StringBuilder();
			String colTitle = ".";
			String colRow = "|";
			String colNum = " ";
			String formatTitle = "%" + (sirka + colTitle.length()) + "s";
			String formatTitleRow = "%" + (sirka + colRow.length()) + "s";
			String formatTitleNum = "%" + (sirka + colNum.length()) + "s";

			sb.append(String.format(formatTitleRow, "").replace(" ", "_"));
			for(int sloupec = 0; sloupec < velikost; sloupec++)
			{
				sb.append(String.format(formatTitle, sloupec + "."));
			}
			sb.append("\n");

			for(int radek = 0; radek < velikost; radek++)
			{
				sb.append(String.format(formatTitle, radek + colRow));
				for(int sloupec = 0; sloupec < velikost; sloupec++)
				{
					int lodId = pozice[radek][sloupec];
					sb.append(String.format(formatTitleNum, lodId + colNum));
				}
				sb.append("\n");
			}

			return sb.toString();
		}
	}
}
