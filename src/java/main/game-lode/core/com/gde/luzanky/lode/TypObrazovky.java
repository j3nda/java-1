package com.gde.luzanky.lode;

public enum TypObrazovky
{
	/** hlavni nabidka */
	MENU,
	/** hra */
	HRA,
	/** nastaveni hraci plochy */
	HRA_SETUP,
	/** konec hry */
	HRA_KONEC,
}
