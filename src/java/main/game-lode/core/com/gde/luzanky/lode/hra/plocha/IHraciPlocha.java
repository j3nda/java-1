package com.gde.luzanky.lode.hra.plocha;

import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;

public interface IHraciPlocha
{
	int getHracId();
	int[][] getRozmisteniLodi();
	void onClickHraciPolicko(HraciPolicko policko);
}
