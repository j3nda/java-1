package com.gde.luzanky.lode.hra.hraci;

import com.gde.common.utils.Point;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.LodeGameConfiguration.ObtiznostHry;
import com.gde.luzanky.lode.hra.IHraLogika;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;
import com.gde.luzanky.lode.setup.ObrazovkaSetup;

public class HracPocitac
extends HracInfo
implements IHraLogika
{
	private final Point utokNaPolicko = new Point();
	public final ObtiznostHry obtiznost;
	/** 2d-pole[radek][sloupec] pro pamatovani si: kam jsem strilel, do vody */
	private boolean[][] strilimDoVody;
	/** 2d-pole[radek][sloupec] pro pamatovani si: kam jsem strilel, na lod */
	private int[][] strilimNaLod;

	/** volano z: {@link LodeGameConfiguration} pri vytvoreni hracu */
	public HracPocitac(LodInfo[] lode, ObtiznostHry obtiznost, String jmeno)
	{
		super(lode, false, jmeno);
		this.obtiznost = obtiznost;
	}

	/** volano z: {@link ObrazovkaSetup} pri startHry(); */
	public HracPocitac(HracPocitac hrac, int[][] rozmisteniLodi)
	{
		super(hrac, rozmisteniLodi);
		this.obtiznost = hrac.obtiznost;

		int velikost = rozmisteniLodi.length;
		strilimDoVody = new boolean[velikost][velikost];
		strilimNaLod = new int[velikost][velikost];
	}

	/** volano z: {@link ObrazovkaSetup} pri vytvorDebugGameplayCheckbox(); */
	public HracPocitac(int id, LodInfo[] lode, int[][] rozmisteniLodi, ObtiznostHry obtiznost, String jmeno)
	{
		super(id, lode, rozmisteniLodi, false, jmeno);
		this.obtiznost = obtiznost;
	}

	@Override
	public Point tahHrace(int utocimHracId)
	{
		// TODO: hra/LOGIKA: chovani pocitace, kdyz tahne... zatim random();
		int velikostHraciPlochy = rozmisteniLodi[0].length;
		do
		{
			utokNaPolicko.set(
				RandomUtils.nextInt(0, velikostHraciPlochy - 1), // x ~ sloupec
				RandomUtils.nextInt(0, velikostHraciPlochy - 1)  // y ~ radek
			);
			if (LodeGame.isDebug())
			{
				debug(".tahHrace(" + utocimHracId + "): utokNaPolicko=[" + utokNaPolicko.y + "," + utokNaPolicko.x + "]");
			}
		}
		while(
			// 2/ nesmi tam byt voda (~nestrilel jsem do vody)
			   strilimDoVody[utokNaPolicko.y][utokNaPolicko.x] == true

			// 1/ nesmi tam byt lod (~nestrilel jsem na lod)
			|| strilimNaLod[utokNaPolicko.y][utokNaPolicko.x] > 0
		);
		if (LodeGame.isDebug())
		{
			debug(".tahHrace(" + utocimHracId + ")---[strilimDoVody]---\n" + HraciPlocha.ToString.fromBoolean(strilimDoVody));
			debug(".tahHrace(" + utocimHracId + ")---[strilimNaLode]---\n" + HraciPlocha.ToString.fromInteger(strilimNaLod));
		}
		return utokNaPolicko;
	}

	@Override
	public void onLodJeKompletneZnicena(LodInfo lod, HracInfo hrac, HraciPolicko policko)
	{
		if (hrac.id == id)
		{
			// pokud se utoci na me samotneho, nic nedelam a preskakuji...
			return;
		}
		if (LodeGame.isDebug())
		{
			debug(".onLodJeKompletneZnicena(" + lod.id + "): policko=[" + policko + "]");
		}

		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();

		// chci si zapamatovat, ze jsem vystrelil na lod (kompletne)
		strilimNaLod[radek][sloupec]++;
	}

	@Override
	public void onLodDostalaZasah(LodInfo lod, HracInfo hrac, HraciPolicko policko)
	{
		if (hrac.id == id)
		{
			// pokud se utoci na me samotneho, nic nedelam a preskakuji...
			return;
		}
		if (LodeGame.isDebug())
		{
			debug(".onLodDostalaZasah(" + lod.id + "): policko=[" + policko + "]");
		}

		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();

		// chci si zapamatovat, ze jsem vystrelil na lod (poprve)
		strilimNaLod[radek][sloupec]++;
	}

	@Override
	public void onLodDostalaZnovuZasah(LodInfo lod, HracInfo hrac, HraciPolicko policko)
	{
		if (hrac.id == id)
		{
			// pokud se utoci na me samotneho, nic nedelam a preskakuji...
			return;
		}
		if (LodeGame.isDebug())
		{
			debug(".onLodDostalaZnovuZasah(" + lod.id + "): policko=[" + policko + "]");
		}

		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();

		// chci si zapamatovat, ze jsem vystrelil na lod (opakovane)
		strilimNaLod[radek][sloupec]++;
	}

	@Override
	public void onStrilimDoVody(HracInfo hrac, HraciPolicko policko)
	{
		if (hrac.id == id)
		{
			// pokud se utoci na me samotneho, nic nedelam a preskakuji...
			return;
		}
		debug(".onStrilimDoVody(): policko=[" + policko + "]");

		int radek = policko.getRadek();
		int sloupec = policko.getSloupec();

		// chci si zapamatovat, ze jsem vystrelil do vody
		strilimDoVody[radek][sloupec] = true;
	}

	private void debug(String message)
	{
		if (LodeGame.isDebug())
		{
			System.out.println(getClass().getSimpleName() + "(" + id + ", " + jmeno + ")" + message);
		}
	}
}
