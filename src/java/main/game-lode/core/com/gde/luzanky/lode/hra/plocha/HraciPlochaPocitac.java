package com.gde.luzanky.lode.hra.plocha;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolickoPocitac;

/** hraci plocha: kde se odehrava bitva (~z pohledu pocitace jako protivnika) */
public class HraciPlochaPocitac
extends HraciPlocha
{
	public HraciPlochaPocitac(ILodeScreenResources resources, LodeGameConfiguration gameConfig, HracInfo hrac, OnTahHrace onTahHraceListener)
	{
		super(resources, gameConfig, hrac, onTahHraceListener);
	}

	/** vytvori hraci policko */
	@Override
	protected HraciPolicko vytvorHraciPolicko(int radek, int sloupec)
	{
		HraciPolickoPocitac pol = new HraciPolickoPocitac(resources, radek, sloupec);
		pol.setText(VODA_TEXT);
		pol.setPozadi(Color.BLUE);
		pol.addListener(new HraciPolickoClickListener(pol)
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				if (onTahHraceListener != null)
				{
					onTahHraceListener.onTahHrace(hrac.id, policko);
				}
			}
		});
		return pol;
	}

	@Override
	public void onClickHraciPolicko(HraciPolicko policko)
	{
		HraciPolickoPocitac pol = (HraciPolickoPocitac) policko;
		int lodId = getRozmisteniLodi()[pol.getRadek()][pol.getSloupec()];
		if (lodId == HraciPlocha.VODA_ID)
		{
			pol.setText("[" + VODA_TEXT + "]");
			pol.setPozadi(Color.BLUE);
		}
		else
		{
			pol.setText("[" + lodId + "]");
			pol.setPozadi(vratBarvuLode(lodId));
		}
	}
}
