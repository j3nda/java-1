package com.gde.luzanky.lode.setup;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.gde.common.graphics.colors.TintableRegionDrawable;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.luzanky.lode.FontSize;
import com.gde.luzanky.lode.LodeGame;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.setup.SetupManager.MyClickListener;
import com.gde.luzanky.lode.setup.SetupManager.OnLodSetupListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberHraceListener;
import com.gde.luzanky.lode.setup.SetupManager.OnVyberLodeListener;

class LodeStatusTable
extends Table
implements OnVyberHraceListener, OnVyberLodeListener, OnLodSetupListener
{
	private final ILodeScreenResources resources;
	private Label titleLabel;
	private LodInfoSetup prevSelectedLod;
	private Map<Integer, LodInfoEntry> vsechnyLode;
	private final SetupManager setupManager;
	private HracInfoSetup aktualniHrac;

	LodeStatusTable(ILodeScreenResources resources, SetupManager setupManager)
	{
		this.resources = resources;
		this.setupManager = setupManager;
		this.aktualniHrac = setupManager.hrac.aktualni();
		this.prevSelectedLod = null;
		createContent(this);
		setupManager.listeners.addVyberHraceListener(this);
		setupManager.listeners.addVyberLodListener(this);
		setupManager.listeners.addLodSetupListener(this);
	}

	private Map<Integer, LodInfoEntry> vytvorVsechnyLode(float height)
	{
		Map<Integer, LodInfoEntry> vsechnyLode = new HashMap<>();
		HracInfo[] hraci = setupManager.gameConfig.hrac;
		for(int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];
			LodInfo[] lode = hrac.lode;
			for(int j = 0; j < lode.length; j++)
			{
				LodInfo lod = lode[j];
				vsechnyLode.put(
					lod.id,
					new LodInfoEntry(
						i,
						new VyberLodeVelikost(resources, height, true),
						new VybranaLodIndikator(lod.velikost)
					)
				);
			}
		}
		return vsechnyLode;
	}

	private void createContent(Table wrapper)
	{
		wrapper
			.add(titleLabel = resources.createLabel("status"))
		;
		wrapper.row();

		vsechnyLode = vytvorVsechnyLode(titleLabel.getHeight());
		LodInfo[] lode = aktualniHrac.lode;
		for(int i = 0; i < lode.length; i++)
		{
			LodInfo lodInfo = lode[i];
			LodInfoSetup lodSetup = setupManager.lode.findById(lodInfo.id);
			LodInfoEntry setup = vsechnyLode.get(lodInfo.id);

			if (lodInfo.id == aktualniHrac.vybranaLodId)
			{
				prevSelectedLod = lodSetup;
			}
			setup
				.velikost
				.update(lodSetup)
			;
			setup
				.selected
				.setSelected(lodInfo.id == aktualniHrac.vybranaLodId)
			;

			Table radekTable = new Table();
// TODO: XHONZA: Table.hit() ~ whole row; instead of inner elements!
			radekTable.add(setup.selected);
			radekTable
				.add(setup.velikost)
				.growX()
			;
			radekTable.row();
			radekTable.addListener(new MyClickListener<Integer>(i)
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					onClickRowIndex(clickInfo);
				}
			});

			wrapper
				.add(radekTable)
				.grow()
			;
			wrapper.row();
		}
		if (LodeGame.isDebug())
		{
			wrapper.debug();
		}
	}

	private void onClickRowIndex(int index)
	{
		HracInfo[] hraci = setupManager.gameConfig.hrac;
		for(int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];
			if (hrac.id != aktualniHrac.id)
			{
				continue;
			}
			LodInfo[] lode = hrac.lode;
			for(int j = 0; j < lode.length; j++)
			{
				if (index == j)
				{
					LodInfo lodInfo = lode[index];
					LodInfoSetup lodSetup = setupManager.lode.findById(lodInfo.id);
					onVyberLodeChanged(lodSetup);
					for(OnVyberLodeListener listener : setupManager.listeners.vyberLode)
					{
						listener.onVyberLodeChanged(lodSetup);
					}
					break;
				}
			}
		}
	}

	@Override
	public void setupOn(LodInfo lodInfo)
	{
		vsechnyLode.get(lodInfo.id).velikost.setupOn(lodInfo);
	}

	@Override
	public void setupOff(LodInfo lodInfo)
	{
		vsechnyLode.get(lodInfo.id).velikost.setupOff(lodInfo);
	}

	@Override
	public void onVyberHraceChanged(HracInfoSetup hracInfo)
	{
		if (aktualniHrac.id == hracInfo.id)
		{
			return;
		}
		aktualniHrac = hracInfo;
		prevSelectedLod = null;
		clearActions();
		clearChildren();
		createContent(this);
		onVyberLodeChanged(
			setupManager.lode.findById(aktualniHrac.vybranaLodId)
		);
	}

	@Override
	public void onVyberLodeChanged(LodInfoSetup lodInfo)
	{
		if (prevSelectedLod != null)
		{
			if (prevSelectedLod.id == lodInfo.id)
			{
				return;
			}
			vsechnyLode.get(prevSelectedLod.id).selected.setSelected(false);
		}
		VybranaLodIndikator selectedTable = vsechnyLode.get(lodInfo.id).selected;
		selectedTable.setSelected(true);
		prevSelectedLod = lodInfo;
	}

	private class VybranaLodIndikator
	extends Table
	{
		public final TintableRegionDrawable bg;

		VybranaLodIndikator(int velikost)
		{
			bg = new TintableRegionDrawable(Color.YELLOW);
			add(resources.createLabel(velikost + "x", FontSize.VyberLode.VELIKOST));
		}

		public void setSelected(boolean selected)
		{
			setBackground(selected ? bg : null);
		}
	}

	/** ui informace o 1x lodi, ktera je zobrazena (~selected, velikost, nazev, ...) */
	private class LodInfoEntry
	{
		public final int id;
		public final VyberLodeVelikost velikost;
		public final VybranaLodIndikator selected;

		LodInfoEntry(int lodId, VyberLodeVelikost velikost, VybranaLodIndikator selected)
		{
			this.id = lodId;
			this.velikost = velikost;
			this.selected = selected;
		}
	}
}
