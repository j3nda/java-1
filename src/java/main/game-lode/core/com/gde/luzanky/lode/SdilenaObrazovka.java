package com.gde.luzanky.lode;

import com.gde.common.graphics.screens.LodeScreenResources;
import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky} a {@link LodeScreenResources})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, LodeScreenResources, LodeGame>
{
	public SdilenaObrazovka(LodeGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		resources.getFontsManager().dispose();
	}
}
