package com.gde.luzanky.lode.resources;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.utils.AnimationUtils;

public class MissouriAnimation
extends AnimationActor
{
	private static final String frameFormat = "missouri/%s/%02d.jpg";
	public enum MissouriAction
	{
		prid(10),
		rovne(14),
		zBoku(9, "z-boku"),
		odjezd1(9),
		odjezd2(5),
		odjezd3(15),
		odjezd4(13),
		;
		public final int frames;
		private final String id;
		private MissouriAction(int frames)
		{
			this(frames, null);
		}
		private MissouriAction(int frames, String id)
		{
			this.frames = frames;
			this.id = id;
		}
		public String id()
		{
			return (id == null ? name() : id);
		}
	}

	public MissouriAnimation(MissouriAction action, IAssetManager resources)
	{
		super(
			0.015f,
			AnimationUtils.createFrames(
				resources,
				frameFormat.replace("%s", action.id()),
				AnimationUtils.createFramesRange(action.frames)
			),
			PlayMode.LOOP
		);
		setOrigin(Align.center);
	}
}
