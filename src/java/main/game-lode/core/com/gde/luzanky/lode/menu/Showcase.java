package com.gde.luzanky.lode.menu;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.resources.names.ShipNameInfo;

public class Showcase
extends Actor
{
	private final ILodeScreenResources resources;
	private final float changeLimit = 3f;
	private float changeCurrent = 0;
	final Background background;
	private final List<Image> movingImages;
	private final String resourceFilename = "ships/%d.jpg";

	Showcase(ILodeScreenResources resources)
	{
		this.resources = resources;
		this.background = new Background(resources.getAssetManager());
		this.movingImages = new ArrayList<>();
	}

	@Override
	public void act(float delta)
	{
		if (background.texture == null)
		{
			return;
		}
		super.act(delta);
		background.act(delta);
		for(Image img : movingImages)
		{
			img.act(delta);
		}
		changeCurrent += delta;
		if (changeCurrent >= changeLimit)
		{
			changeCurrent = 0;
			update(resources.getShipNamesManager().random(1));
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		background.draw(batch, parentAlpha);
		for(Image img : movingImages)
		{
			img.draw(batch, parentAlpha);
		}
	}

	void update(ShipNameInfo ship)
	{
		if (background.texture == null)
		{
			int tiles = (int) (getWidth() / 300) + 1;

			for (int i = 0; i < tiles; i++)
			{
				Image image = new Image();
				image.setSize(300, 300);
				image.setPosition(0 + (i * 300), getHeight() - 300);
				image.setDrawable(
					new TextureRegionDrawable(
						new TextureRegion(
							getShipTexture(ship)
						)
					)
				);

				float sec = ((getWidth() + 300 - (i * 300)) / getWidth()) * 5;

				image.addAction(Actions.sequence(
					Actions.moveBy(getWidth() - (i * 300), 0, sec),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							image.setX(-300);
							x(image);
						}
					})
				));
				movingImages.add(image);
			}
		}
		background.update(ship);
	}

	private void x(Image img)
	{
		img.addAction(Actions.sequence(
			Actions.moveBy(getWidth() + 300, 0, 5f),
			Actions.run(new Runnable()
			{
				@Override
				public void run()
				{
					img.setX(-300);
					x(img);
				}
			})
		));
	}

	private Texture getShipTexture(ShipNameInfo ship)
	{
		return resources.getAssetManager().get(
			String.format(resourceFilename, ship.image),
			Texture.class
		);
	}

	@Override
	public void setSize(float width, float height)
	{
		super.setSize(width, height);
		background.setSize(width, height);
	}

	class Background
	extends Image
	{
		private final IAssetManager resources;
		private Texture texture;


		Background(IAssetManager resources)
		{
			super();
			this.resources = resources;
		}

		void update(ShipNameInfo ship)
		{
			if (texture == null)
			{
				setDrawable(
					new TextureRegionDrawable(
						new TextureRegion(
							texture = getShipTexture(ship)
						)
					)
				);
			}
			else
			{
				addAction(Actions.sequence(
					Actions.fadeOut(1f),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							setDrawable(
								new TextureRegionDrawable(
									new TextureRegion(
										texture = getShipTexture(ship)
									)
								)
							);
						}
					}),
					Actions.fadeIn(1f)
				));
			}
		}
	}
}
