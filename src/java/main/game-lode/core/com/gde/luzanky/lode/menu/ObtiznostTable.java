package com.gde.luzanky.lode.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.TintableRegionDrawable;
import com.gde.common.graphics.screens.ILodeScreenResources;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.lode.LodeGameConfiguration.ObtiznostHry;

class ObtiznostTable
extends Table
{
	private static final Color onColor = Color.WHITE;
	private static final Color offColor = Color.BLACK;
	private ObtiznostHry obtiznost = ObtiznostHry.EASY;
	private final Texture tintTexture;
	private final TintableRegionDrawable[] tintDrawable;


	ObtiznostTable(ILodeScreenResources resources)
	{
		super();
		tintTexture = resources.getAssetManager().get(CommonResources.Tint.square16x16, Texture.class);
		tintDrawable = new TintableRegionDrawable[ObtiznostHry.values().length];
		for (int i = 0; i < ObtiznostHry.values().length; i++)
		{
			tintDrawable[i] = new TintableRegionDrawable(
				new TextureRegion(tintTexture),
				(ObtiznostHry.values()[i] == obtiznost ? onColor : offColor)
			);
		}
		createContent(this, resources);
	}

	private Table createContent(Table wrapper, ILodeScreenResources resources)
	{
		for (int i = 0; i < ObtiznostHry.values().length; i++)
		{
			int diffIndex = i;
			Table diffWrapper = new Table();

			diffWrapper.setBackground(tintDrawable[i]);
			diffWrapper
				.add(resources.createLabel(ObtiznostHry.values()[i].name()))
				.align(Align.center)
				.expandX()
			;
			diffWrapper.setName("diff-" + diffIndex);
			diffWrapper.addListener(new ClickListener()
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					obtiznost = ObtiznostHry.values()[diffIndex];
					for (int j = 0; j < ObtiznostHry.values().length; j++)
					{
						tintDrawable[j].setColor(offColor);
					}
					tintDrawable[diffIndex].setColor(onColor);
				}
			});
			wrapper
				.add(diffWrapper)
				.align(Align.center)
				.expandX()
			;
		}
		wrapper.row();

		return wrapper;
	}

	ObtiznostHry getObtiznost()
	{
		return obtiznost;
	}
}
