package com.gde.luzanky.lode.setup;

import com.gde.common.utils.Point;
import com.gde.luzanky.lode.hra.hraci.HracInfo;

class HracInfoSetup
extends HracInfo
{
	int vybranaLodId;

	HracInfoSetup(HracInfo hrac)
	{
		super(hrac.id, hrac.lode, null, hrac.jsemClovek, hrac.jmeno);
		vybranaLodId = lode[0].id;
	}

	@Override
	public String toString()
	{
		return super.toString()
			+ ", vybranaLodId=" + vybranaLodId
		;
	}

	@Override
	public Point tahHrace(int utocimHracId)
	{
		// hrac[setup]: neresi zadny dalsi tah...
		return null;
	}
}
