package com.gde.luzanky.lode.hra;

import com.gde.common.resources.names.ShipNameInfo;

/** datova obalka pro popis lode (~napric hrou) */
public class LodInfo
{
	/** staticke pocitadlo pro generovani unikatniho id lode */
	private static int idCounter = 0;
	/** unikatni identifikator lode */
	public final int id;
	/** velikost lode, tj. kolik zabira mista */
	public final int velikost;
	/** jmeno lode */
	public final String jmeno;
	/** pocet zasahu lode */
	private int zasah;

	public LodInfo(ShipNameInfo shipInfo)
	{
		this(shipInfo.getRequiredSize(), shipInfo.name);
	}

	public LodInfo(int velikost, String jmeno)
	{
		this(++idCounter, velikost, jmeno);
	}

	protected LodInfo(int id, int velikost, String jmeno)
	{
		this(id, velikost, jmeno, 0);
	}

	private LodInfo(int id, int velikost, String jmeno, int zasah)
	{
		this.id = id;
		this.velikost = velikost;
		this.jmeno = jmeno;
		this.zasah = zasah;
	}

	public void zasah()
	{
		// TODO: HRA: napis implementaci, kdyz zasahnu lod (~hit)
		if (zasah < velikost)
		{
			zasah++;
		}
	}

	/** vraci true, pokud je lod znicena. jinak vraci false */
	public boolean jeZnicena()
	{
		// TODO: HRA: vrat true, pokud je lod znicena. jinak vrat false.
		return (zasah == velikost);
	}

	@Override
	public String toString()
	{
		return ""
			+ "id=" + id
			+ ", velikost=" + velikost
			+ ", zasah=" + zasah
			+ ", jmeno=" + jmeno
		;
	}
}
