package com.gde.luzanky.lode.setup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.utils.Point;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.lode.LodeGameConfiguration;
import com.gde.luzanky.lode.hra.LodInfo;
import com.gde.luzanky.lode.hra.hraci.HracInfo;
import com.gde.luzanky.lode.hra.plocha.HraciPlocha;
import com.gde.luzanky.lode.hra.plocha.policko.HraciPolicko;

class SetupManager
{
	final LodeGameConfiguration gameConfig;
	final Hrac hrac;
	final Lod lode;
	final LodRozmisteni rozmisteni;
	SetupListeners listeners;
	private final Map<Integer, LodInfoSetup> setupInfo;
	private final NahodneRozmistiLode najdiRozmisteni;

	SetupManager(LodeGameConfiguration gameConfig)
	{
		this.gameConfig = gameConfig;
		this.hrac = new Hrac();
		this.lode = new Lod();
		this.rozmisteni = new LodRozmisteni();
		this.najdiRozmisteni = new NahodneRozmistiLode(this);
		this.listeners = new SetupListeners();
		this.setupInfo = createSetupInfo(listeners);
		this.listeners.addVyberHraceListener(hrac);
	}

	private Map<Integer, LodInfoSetup> createSetupInfo(SetupListeners listeners)
	{
		Map<Integer, LodInfoSetup> setupInfo = new HashMap<>();
		HracInfo[] hraci = gameConfig.hrac;
		int velikost = gameConfig.velikost;
		for(int i = 0; i < hraci.length; i++)
		{
			HracInfo hrac = hraci[i];
			hrac.rozmisteniLodi = prazdnePoziceLodi(new int[velikost][velikost]);
			LodInfo[] lode = hrac.lode;
			for(int j = 0; j < lode.length; j++)
			{
				LodInfo lod = lode[j];
				LodInfoSetup info = new LodInfoSetup(
					hrac.id,
					lod,
					listeners
				);
				setupInfo.put(info.id, info);
			}
		}
		return setupInfo;
	}

	private int[][] prazdnePoziceLodi(int[][] poziceLodi)
	{
		int velikost = poziceLodi[0].length;
		for(int radek = 0; radek < velikost; radek++)
		{
			for(int sloupec = 0; sloupec < velikost; sloupec++)
			{
				poziceLodi[radek][sloupec] = HraciPlocha.VODA_ID;
			}
		}
		return poziceLodi;
	}

	class Hrac
	implements OnVyberHraceListener
	{
		private HracInfoSetup aktualni = new HracInfoSetup(gameConfig.hrac[0]);

		Set<Integer> lode(int hracId)
		{
			Set<Integer> mojeLode = new HashSet<>();
			HracInfo[] hraci = gameConfig.hrac;
			for(int i = 0; i < hraci.length; i++)
			{
				HracInfo hrac = hraci[i];
				if (hrac.id == hracId)
				{
					LodInfo[] lode = hrac.lode;
					for(int j = 0; j < lode.length; j++)
					{
						LodInfo lod = lode[j];
						mojeLode.add(lod.id);
					}
				}
			}
			return mojeLode;
		}

		HracInfo findById(int hracId)
		{
			HracInfo[] hraci = gameConfig.hrac;
			for(int i = 0; i < hraci.length; i++)
			{
				HracInfo hrac = hraci[i];
				if (hrac.id == hracId)
				{
					return hrac;
				}
			}
			return null;
		}

		HracInfoSetup aktualni()
		{
			return aktualni;
		}

		@Override
		public void onVyberHraceChanged(HracInfoSetup hracInfo)
		{
			aktualni = hracInfo;
		}
	}

	class Lod
	{
		public int max()
		{
			return gameConfig.hrac[0].lode.length;
		}

		public LodInfoSetup findById(int lodId)
		{
			return setupInfo.get(lodId);
		}

		public LodInfoSetup findByIndex(int hracId, int index)
		{
			LodInfo[] lode = hrac.findById(hracId).lode;
			int lodId = lode[0].id;
			for(int i = 0; i < lode.length; i++)
			{
				if (index == i)
				{
					lodId = lode[i].id;
					break;
				}
			}
			return findById(lodId);
		}

		public int findIndex(HracInfoSetup hracInfo)
		{
			LodInfo[] lode = hracInfo.lode;
			for(int i = 0; i < lode.length; i++)
			{
				LodInfo lod = lode[i];
				if (lod.id == hracInfo.vybranaLodId)
				{
					return i;
				}
			}
			return 0;
		}

		public int findIndex(LodInfoSetup lodInfo)
		{
			LodInfo[] lode = hrac.findById(lodInfo.hracId).lode;
			for(int i = 0; i < lode.length; i++)
			{
				LodInfo lod = lode[i];
				if (lod.id == lodInfo.id)
				{
					return i;
				}
			}
			return 0;
		}

		public List<LodInfoSetup> findByJeKompletni(int hracId, boolean jeKompletni)
		{
			List<LodInfoSetup> results = new ArrayList<>();
			LodInfo[] lode = hrac.findById(hracId).lode;
			for(int i = 0; i < lode.length; i++)
			{
				LodInfoSetup lodInfo = findById(lode[i].id);
				if (jeKompletni == lodInfo.jeKompletni())
				{
					results.add(lodInfo);
				}
			}
			return results;
		}

		public void rozmistiLode(HraciPlochaSetup hraciPlocha)
		{
			najdiRozmisteni.rozmistiLode(hraciPlocha);
		}

		public boolean jeRozmisteniLodiKompletni()
		{
			for(HracInfo hrac : gameConfig.hrac)
			{
				if (!jeRozmisteniLodiKompletni(hrac.id))
				{
					return false;
				}
			}
			return true;
		}

		public boolean jeRozmisteniLodiKompletni(int hracId)
		{
			int counter = 0;
			for(LodInfo info : hrac.findById(hracId).lode)
			{
				LodInfoSetup setup = findById(info.id);
				if (setup.jeKompletni())
				{
					counter++;
				}
			}
			return (counter == lode.max());
		}
	}

	class LodRozmisteni
	implements OnRozmisteniLodeListener
	{
		@Override
		public void onRozmisteniStarted(HraciPlochaSetup hraciPlocha)
		{
			for(OnRozmisteniLodeListener listener : listeners.lodRozmisteni)
			{
				listener.onRozmisteniStarted(hraciPlocha);
			}
		}

		@Override
		public void onRozmisteniCancelled(HraciPlochaSetup hraciPlocha)
		{
			for(OnRozmisteniLodeListener listener : listeners.lodRozmisteni)
			{
				listener.onRozmisteniCancelled(hraciPlocha);
			}
		}

		@Override
		public void onRozmisteniFinished(HraciPlochaSetup hraciPlocha)
		{
			for(OnRozmisteniLodeListener listener : listeners.lodRozmisteni)
			{
				listener.onRozmisteniFinished(hraciPlocha);
			}
		}

		@Override
		public void onRozmisteniUpdate(HraciPolicko hraciPolicko, HraciPlochaSetup hraciPlocha, LodInfoSetup lodInfo)
		{
			for(OnRozmisteniLodeListener listener : listeners.lodRozmisteni)
			{
				listener.onRozmisteniUpdate(hraciPolicko, hraciPlocha, lodInfo);
			}
		}
	}

	private class NahodneRozmistiLode
	extends LodRozmisteni
	{
		private final SetupManager setupManager;
		private final int velikost;
		private final Point pos = new Point();

		NahodneRozmistiLode(SetupManager setupManager)
		{
			this.setupManager = setupManager;
			this.velikost = setupManager.gameConfig.velikost;
		}

		public void rozmistiLode(HraciPlochaSetup hraciPlocha)
		{
			int hracId = hraciPlocha.getHracId();
			onRozmisteniStarted(hraciPlocha);
			for(LodInfoSetup lodInfo : setupManager.lode.findByJeKompletni(hracId, false))
			{
				for(OnVyberLodeListener listener : setupManager.listeners.vyberLode)
				{
					listener.onVyberLodeChanged(lodInfo);
				}
				do
				{
					Point pos = najdiUmisteni(
						hraciPlocha.getRozmisteniLodi(),
						lodInfo
					);
					onRozmisteniUpdate(
						hraciPlocha.findActor(
							hraciPlocha.hraciPolickoName(pos.getX(), pos.getY())
						),
						hraciPlocha,
						lodInfo
					);
				}
				while(!lodInfo.jeKompletni());
			}
			onRozmisteniFinished(hraciPlocha);
		}

		private Point najdiUmisteni(int[][] hraciPlocha, LodInfoSetup lodInfo)
		{
			pos.set(
				RandomUtils.nextInt(0, velikost - 1),
				RandomUtils.nextInt(0, velikost - 1)
			);
			return pos;
		}
	}

	class SetupListeners
	{
		final List<OnVyberHraceListener> vyberHrace;
		final List<OnVyberLodeListener> vyberLode;
		final List<OnLodSetupListener> lodSetup;
		final List<OnRozmisteniLodeListener> lodRozmisteni;

		SetupListeners()
		{
			vyberHrace = new ArrayList<>();
			vyberLode = new ArrayList<>();
			lodSetup = new ArrayList<>();
			lodRozmisteni = new ArrayList<>();
		}

		void addVyberHraceListener(OnVyberHraceListener listener)
		{
			if (!vyberHrace.contains(listener))
			{
				vyberHrace.add(listener);
			}
		}

		void addVyberLodListener(OnVyberLodeListener listener)
		{
			if (!vyberLode.contains(listener))
			{
				vyberLode.add(listener);
			}
		}

		void addLodSetupListener(OnLodSetupListener listener)
		{
			if (!lodSetup.contains(listener))
			{
				lodSetup.add(listener);
			}
		}

		void addLodRozmisteniListener(OnRozmisteniLodeListener listener)
		{
			if (!lodRozmisteni.contains(listener))
			{
				lodRozmisteni.add(listener);
			}
		}
	}

	static class MyClickListener<T>
	extends ClickListener
	{
		public final T clickInfo;

		MyClickListener(T clickInfo)
		{
			this.clickInfo = clickInfo;
		}
	}

	/** [hraci plocha] operace nad 1x dilkem lode */
	interface OnLodSetupListener
	{
		void setupOn(LodInfo lodInfo);
		void setupOff(LodInfo lodInfo);
	}

	/** [ovladani] operace nad vyberem lodi */
	interface OnVyberLodeListener
	{
		void onVyberLodeChanged(LodInfoSetup lodInfo);
	}

	/** [ovladani] operace nad vyberem hracu */
	interface OnVyberHraceListener
	{
		void onVyberHraceChanged(HracInfoSetup hracInfo);
	}

	interface OnRozmisteniLodeListener
	{
		void onRozmisteniStarted(HraciPlochaSetup hraciPlocha);
		void onRozmisteniFinished(HraciPlochaSetup hraciPlocha);
		void onRozmisteniCancelled(HraciPlochaSetup hraciPlocha);
		void onRozmisteniUpdate(HraciPolicko hraciPolicko, HraciPlochaSetup hraciPlocha, LodInfoSetup lodInfo);
	}
}
