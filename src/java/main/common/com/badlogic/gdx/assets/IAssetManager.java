/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 * - Lode -- http://gde.tips/games/luzanky/lode
 * - Box -- http://gde.tips/games/luzanky/box
 * - DoomGuy -- http://gde.tips/games/luzanky/doom-guy
 ******************************************************************************/
package com.badlogic.gdx.assets;

import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Logger;

/** it reflects LibGdx's {@link AssetManager} functionality */
public interface IAssetManager
extends Disposable
{
	<T> T get(String fileName);
	<T> T get(String fileName, Class<T> type);
	<T> T get(String fileName, Class<T> type, boolean required);
	<T> T get(String fileName, boolean required);
	<T> T get(AssetDescriptor<T> assetDescriptor);
	<T> Array<T> getAll(Class<T> type, Array<T> out);
	<T> String getAssetFileName(T asset);
	int getLoadedAssets();
	int getQueuedAssets();
	float getProgress();
	FileHandleResolver getFileHandleResolver();
	Array<String> getAssetNames();
	Array<String> getDependencies(String fileName);
	@SuppressWarnings("rawtypes")
	Class getAssetType(String fileName);
	String getDiagnostics();

	boolean contains(String fileName);
	@SuppressWarnings("rawtypes")
	boolean contains(String fileName, Class type);
	<T> boolean containsAsset(T asset);

	<T> void load(String fileName, Class<T> type);
	<T> void load(String fileName, Class<T> type, AssetLoaderParameters<T> parameter);
	@SuppressWarnings("rawtypes")
	void load(AssetDescriptor desc);

	void unload(String fileName);

	@SuppressWarnings("rawtypes")
	boolean isLoaded(AssetDescriptor assetDesc);
	boolean isLoaded(String fileName);
	@SuppressWarnings("rawtypes")
	boolean isLoaded(String fileName, Class type);

	boolean update();
	boolean update(int millis);

	boolean isFinished();
	void finishLoading();
	@SuppressWarnings("rawtypes")
	<T> T finishLoadingAsset(AssetDescriptor assetDesc);
	<T> T finishLoadingAsset(String fileName);

	@SuppressWarnings("rawtypes")
	<T> AssetLoader getLoader(final Class<T> type);
	@SuppressWarnings("rawtypes")
	<T> AssetLoader getLoader(final Class<T> type, final String fileName);
	<T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, AssetLoader<T, P> loader);
	<T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, String suffix, AssetLoader<T, P> loader);

	void setErrorListener(AssetErrorListener listener);

	void clear();

	Logger getLogger();
	void setLogger(Logger logger);

	int getReferenceCount(String fileName);
	void setReferenceCount(String fileName, int refCount);
}
