/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.badlogic.scenes.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import com.badlogic.gdx.scenes.scene2d.actions.AddListenerAction;
import com.badlogic.gdx.scenes.scene2d.actions.AfterAction;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.LayoutAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveActorAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveListenerAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.actions.SizeByAction;
import com.badlogic.gdx.scenes.scene2d.actions.SizeToAction;
import com.badlogic.gdx.scenes.scene2d.actions.TimeScaleAction;
import com.badlogic.gdx.scenes.scene2d.actions.TouchableAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;


/**
 * tovarnicka, ktera aplikuje vytvoreni nove LibGdx.{@link Action}.
 * TODO: toto chovani v LibGdx chybi a ma smysl udelat pull-request a fixnout jim to!
 */
class ActionsFactory
{
	public AddAction add(AddAction action)
	{
		return Actions.addAction(action, action.getTarget());
	}

	public RemoveAction remove(RemoveAction action)
	{
		return Actions.removeAction(action, action.getTarget());
	}

	public Action moveTo(MoveToAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action moveBy(MoveByAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action sizeTo(SizeToAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action sizeBy(SizeByAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action scaleTo(ScaleToAction action)
	{
		return Actions.scaleTo(
			action.getX(),
			action.getY(),
			action.getDuration(),
			action.getInterpolation()
		);
	}

	public Action scaleBy(ScaleByAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action rotateTo(RotateToAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action rotateBy(RotateByAction action)
	{
		return Actions.rotateBy(
			action.getAmount(),
			action.getDuration(),
			action.getInterpolation()
		);
	}

	public Action color(ColorAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action alpha(AlphaAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action visible(VisibleAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action touchable(TouchableAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action removeActor(RemoveActorAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action delay(DelayAction action)
	{
		return Actions.delay(
			action.getDuration(),
			action.getAction()
		);
	}

	public Action timeScale(TimeScaleAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public SequenceAction sequence(SequenceAction action)
	{
		SequenceAction a = Actions.sequence();
		for(Action act : action.getActions())
		{
			a.addAction(ActionsUtils.newInstance(act));
		}
		return a;
	}

	public ParallelAction parallel(ParallelAction action)
	{
		ParallelAction a = Actions.parallel();
		for(Action act : action.getActions())
		{
			a.addAction(ActionsUtils.newInstance(act));
		}
		return a;
	}

	public RepeatAction repeat(RepeatAction action)
	{
		return Actions.repeat(
			action.getCount(),
			ActionsUtils.newInstance(action.getAction())
		);
	}

	public Action runnable(RunnableAction action)
	{
		return Actions.run(
			action.getRunnable()
		);
	}

	public Action layout(LayoutAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action after(AfterAction action)
	{
		return Actions.after(
			Actions.sequence(
				Actions.rotateBy(33f, 1f),
				action.getAction()
			)
		);
	}

	public Action addListener(AddListenerAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}

	public Action removeListener(RemoveListenerAction action)
	{
		// auto-generated method stub ~ naimplementuj, pokud to potrebujes...
		return null;
	}
}
