/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.badlogic.scenes.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import com.badlogic.gdx.scenes.scene2d.actions.AddListenerAction;
import com.badlogic.gdx.scenes.scene2d.actions.AfterAction;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.LayoutAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveActorAction;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveListenerAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.actions.SizeByAction;
import com.badlogic.gdx.scenes.scene2d.actions.SizeToAction;
import com.badlogic.gdx.scenes.scene2d.actions.TimeScaleAction;
import com.badlogic.gdx.scenes.scene2d.actions.TouchableAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;


/**
 * pomocna trida ActionUtils, ktera:
 * - zabaluje vytvareni novych LibGdx.{@link Action}, tj. hybani, rotaci aj.
 * toto vytvareni se realizuje pres "tovarnicku" {@link ActionsFactory}, ktera neni staticka!
 */
public class ActionsUtils
extends Actions
{
	private static final ActionsFactory factory = new ActionsFactory();


	public static Action newInstance(Action action)
	{
		// FYI: stupid typo formating :-/ due quick changes and better readability in one page!
		     if (action instanceof AddAction)            { return factory.add((AddAction) action); }
		else if (action instanceof RemoveAction)         { return factory.remove((RemoveAction) action); }
		else if (action instanceof MoveToAction)         { return factory.moveTo((MoveToAction) action); }
		else if (action instanceof MoveByAction)         { return factory.moveBy((MoveByAction) action); }
		else if (action instanceof SizeToAction)         { return factory.sizeTo((SizeToAction) action); }
		else if (action instanceof SizeByAction)         { return factory.sizeBy((SizeByAction) action); }
		else if (action instanceof ScaleToAction)        { return factory.scaleTo((ScaleToAction) action); }
		else if (action instanceof ScaleByAction)        { return factory.scaleBy((ScaleByAction) action); }
		else if (action instanceof RotateToAction)       { return factory.rotateTo((RotateToAction) action); }
		else if (action instanceof RotateByAction)       { return factory.rotateBy((RotateByAction) action); }
		else if (action instanceof ColorAction)          { return factory.color((ColorAction) action); }
		else if (action instanceof AlphaAction)          { return factory.alpha((AlphaAction) action); }
		else if (action instanceof VisibleAction)        { return factory.visible((VisibleAction) action); }
		else if (action instanceof TouchableAction)      { return factory.touchable((TouchableAction) action); }
		else if (action instanceof RemoveActorAction)    { return factory.removeActor((RemoveActorAction) action); }
		else if (action instanceof DelayAction)          { return factory.delay((DelayAction) action); }
		else if (action instanceof TimeScaleAction)      { return factory.timeScale((TimeScaleAction) action); }
		else if (action instanceof SequenceAction)       { return factory.sequence((SequenceAction) action); }
		else if (action instanceof ParallelAction)       { return factory.parallel((ParallelAction) action); }
		else if (action instanceof RepeatAction)         { return factory.repeat((RepeatAction) action); }
		else if (action instanceof RunnableAction)       { return factory.runnable((RunnableAction) action); }
		else if (action instanceof LayoutAction)         { return factory.layout((LayoutAction) action); }
		else if (action instanceof AfterAction)          { return factory.after((AfterAction) action); }
		else if (action instanceof AddListenerAction)    { return factory.addListener((AddListenerAction) action); }
		else if (action instanceof RemoveListenerAction) { return factory.removeListener((RemoveListenerAction) action); }
		else { throw new RuntimeException("Unimplemented action, @see: " + action.getClass().getName() + "!"); }
	}
}
