/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.inputs;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.utils.Array;

public class KeyboardInputProcessorXXX
extends InputAdapter
implements IKeyboardInputProcessor
{
	private boolean isKeyDown = false;
	private int keyPressed = Keys.ANY_KEY;
	private Array<IKeyboardInputListeners> listeners;


	public KeyboardInputProcessorXXX()
	{
		listeners = new Array<IKeyboardInputListeners>();
	}

	@Override
	public int getKeyPressed()
	{
		return keyPressed;
	}

	@Override
	public boolean keyDown(int keycode)
	{
		// TODO: handle keyDown for some amount of time - to react on it, eg: cummulative speedUp
		if (!isKeyDown)
		{
			keyPressed = keycode;
		}
		isKeyDown = true;
		return super.keyDown(keycode);
	}

	@Override
	public boolean keyUp(int keycode)
	{
		isKeyDown = false;
		if (super.keyUp(keycode))
		{
			return true;
		}
		for(IKeyboardInputListeners listener : listeners)
		{
			if (listener.handleKeyUp(keycode))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isKeyPressed()
	{
		return isKeyDown;
	}

	@Override
	public void addKeyUpListener(IKeyboardInputListeners listener)
	{
		if (!listeners.contains(listener, false))
		{
			listeners.add(listener);
		}
	}

	@Override
	public void removeKeyUpListener(IKeyboardInputListeners listener)
	{
		listeners.removeValue(listener, false);
	}
}
