/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - DoomGuy -- http://gde.tips/games/luzanky/doom-guy
 ******************************************************************************/
package com.gde.common.inputs;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

public abstract class KeyboardInputProcessor
extends InputAdapter
{
	protected final Set<Integer> pressedKeys;
	protected final Set<Integer> requiredKeys;
	private final Set<Integer> tmpKeys;
	private Integer lastPressedKey = null;
	private volatile boolean act = false;

	public KeyboardInputProcessor()
	{
		this(new HashSet<>(Arrays.asList(Input.Keys.ANY_KEY)));
	}

	public KeyboardInputProcessor(Set<Integer> requiredKeys)
	{
		this.pressedKeys = new HashSet<>();
		this.tmpKeys = new HashSet<>();
		this.requiredKeys = requiredKeys;
	}

	@Override
	public boolean keyDown(int keycode)
	{
		return keyDown(keycode, canProcessKeys());
	}

	protected boolean keyDown(int keycode, boolean keyProcess)
	{
		if (!keyProcess)
		{
			return super.keyDown(keycode);
		}
		pressedKeys.add(keycode);
		return checkForKeyCombinations();
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return keyUp(keycode, canProcessKeys());
	}

	protected boolean keyUp(int keycode, boolean keyProcess)
	{
		boolean myKeys = checkForKeyCombinations();
		pressedKeys.remove(keycode);
		lastPressedKey = keycode;
		return myKeys;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return keyTyped(character, canProcessKeys());
	}

	protected boolean keyTyped(char character, boolean keyProcess)
	{
		if (!keyProcess)
		{
			return super.keyTyped(character);
		}
		// TODO
		return false;
	}

	protected boolean checkForKeyCombinations()
	{
		if (requiredKeys.contains(Input.Keys.ANY_KEY))
		{
			return true;
		}
		tmpKeys.clear();
		tmpKeys.addAll(requiredKeys);
		tmpKeys.retainAll(pressedKeys);

		return (tmpKeys.size() > 0);
	}

//	@Override//TODO
	public Integer getLastPressedKey()
	{
		return lastPressedKey;
	}

	public void act()
	{
		act(Gdx.graphics.getDeltaTime());
	}

	public synchronized void act(float delta)
	{
		act = true;
		if (requiredKeys.contains(Input.Keys.ANY_KEY))
		{
			// TODO
		}
		else
		{
			for(int keycode : requiredKeys)
			{
				if (Gdx.input.isKeyPressed(keycode))
				{
					keyDown(keycode, true);
					keyTyped((char) keycode, true);
				}
			}
		}
		act = false;
	}

	private synchronized boolean canProcessKeys()
	{
		return !act;
	}
}
