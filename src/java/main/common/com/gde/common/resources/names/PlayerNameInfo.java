package com.gde.common.resources.names;

import com.gde.common.resources.names.NamesManager.NameInfoWithIdCounter;

public class PlayerNameInfo
extends NameInfoWithIdCounter
{
	public final String name;
	public final int frequency;
	public final String url;

	PlayerNameInfo(String csv)
	{
		super();
		//0-----|1----|2----|3--|
		//poradi|jmeno|pocet|url
		String[] col = max(csv.split(","));
		this.name = col[1].replace("\"", "");
		this.frequency = Integer.valueOf(col[2].replace("\"", ""));
		this.url = (col.length >= 4 ? col[3].replace("\"", "") : "");
	}

	@Override
	public String toString()
	{
		return ""
			+  "| " + String.format("%-" + (counterId() + "").length() + "s", id)
			+ " | " + String.format("%-" + max(1) + "s", name)
			+ " | " + String.format("%-" + max(2) + "s", frequency)
			+ " | " + url
		;
	}
}
