/*******************************************************************************
 * Copyright 2019 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.resources.preferences;

public interface IPreferencesOption<OptionEnum extends Enum<OptionEnum> & IPreferencesOptionEnum>
{
	void setOption(OptionEnum option, boolean value);
	void setOption(OptionEnum option, int value);
	void setOption(OptionEnum option, long value);
	void setOption(OptionEnum option, float value);
	void setOption(OptionEnum option, String value);
}
