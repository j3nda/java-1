/*******************************************************************************
 * Copyright 2019 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.resources.preferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;


/**
 * base class for multiple preferences, eg: game, in-app-purchases, ...
 * @see: PreferencesManager.PreferencesType
 *
 * FYI: maximum amout of data stored is cca: 1.42MB!
 * (but remember, that it can take a long time! so for huge data use NoSql)
 * @see:
 * -- https://stackoverflow.com/questions/15617825/shared-preferences-limit
 * -- https://stackoverflow.com/questions/8516812/shared-preferences-max-length-of-a-single-value/8517309
 */
public abstract class PreferencesOption<OptionEnum extends Enum<OptionEnum> & IPreferencesOptionEnum>
implements IPreferencesOption<OptionEnum>
{
	private final Preferences preferences;
	private final String name;

	/**
	 * namespace for preferences stored in java/android innerStorageDevice.
	 * @see "
	 *    linux: ${HOME}/.prefs/#PREF_NAME#;
	 *  windows: %UserProfile%/.prefs/#PREF_NAME#;
	 *  android: system's SharedPreferences class is used.This means preferences will survive app updates,
	 *           but are deleted when the app is uninstalled.
	 *           stored-in: /data/user/#android_user_id#/my.package.name/shared_prefs/my.package.#PREF_NAME#.xml;
	 *      iOS: an NSMutableDictionary will be written to the given file.
	 *  "
	 *  @see: https://stackoverflow.com/questions/15617825/shared-preferences-limit
	 */
	protected PreferencesOption(String name)
	{
		this.preferences = Gdx.app.getPreferences(name);
		this.name = name;
	}

	@Override
	public void setOption(OptionEnum option, boolean value)
	{
		getPreferences().putBoolean(option.getName(), value);
		getPreferences().flush();
	}

	@Override
	public void setOption(OptionEnum option, int value)
	{
		getPreferences().putInteger(option.getName(), value);
		getPreferences().flush();
	}

	@Override
	public void setOption(OptionEnum option, long value)
	{
		getPreferences().putLong(option.getName(), value);
		getPreferences().flush();
	}

	@Override
	public void setOption(OptionEnum option, float value)
	{
		getPreferences().putFloat(option.getName(), value);
		getPreferences().flush();
	}

	@Override
	public void setOption(OptionEnum option, String value)
	{
		getPreferences().putString(option.getName(), value);
		getPreferences().flush();
	}

	protected Object getOption(OptionEnum option)
	{
		Preferences pref = getPreferences();
		if (option.getDefaultValue() instanceof Boolean)
		{
			return pref.getBoolean(option.getName(), (Boolean)option.getDefaultValue());
		}
		else
		if (option.getDefaultValue() instanceof Integer)
		{
			return pref.getInteger(option.getName(), (Integer)option.getDefaultValue());
		}
		else
		if (option.getDefaultValue() instanceof Long)
		{
			return pref.getLong(option.getName(), (Long)option.getDefaultValue());
		}
		else
		if (option.getDefaultValue() instanceof Float)
		{
			return pref.getFloat(option.getName(), (Float)option.getDefaultValue());
		}
		else
		if (option.getDefaultValue() instanceof String)
		{
			return pref.getString(option.getName(), (String)option.getDefaultValue());
		}
		else
		{
			throw new RuntimeException(
				"Unimplemented preference at \"" + getName() + "\" for "
				+ option.getDefaultValue().getClass().getSimpleName()
				+ "(" + option.name() + " aka \"" + option.getName() + "\")!"
			);
		}
	}

	protected String getName()
	{
		return name;
	}

	protected Preferences getPreferences()
	{
		return preferences;
	}
}
