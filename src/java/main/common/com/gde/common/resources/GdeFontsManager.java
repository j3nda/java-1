/*******************************************************************************
 * Copyright 2022 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 * - Lode -- http://gde.tips/games/luzanky/lode
 ******************************************************************************/
package com.gde.common.resources;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.gde.common.utils.HashUtils;

public class GdeFontsManager
implements IFontsManager
{
	protected static final String FONT_CHARS = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!-_.,<>[]:;";
	private final HashMap<String, BitmapFont> fonts = new HashMap<>();
	private final HashMap<String, LabelStyle> labelStyles = new HashMap<>();


	@Override
	public BitmapFont loadFont(String fontFilename, int size)
	{
		return loadFont(fontFilename, size, FONT_CHARS);
	}

	@Override
	public BitmapFont loadFont(String filename, int size, String chars)
	{
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(filename));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.characters = chars;

		BitmapFont bitmapFont = generator.generateFont(parameter);
		generator.dispose();

		fonts.put(
			createUUID(filename, size),
			bitmapFont
		);
		return bitmapFont;
	}

	protected String createUUID(String filename, int size)
	{
		return HashUtils.MD5(filename + ":" + size);
	}

	protected String createUUID(String filename, int size, Color color)
	{
		return HashUtils.MD5(filename + ":" + size + ":" + color.toIntBits());
	}

	@Override
	public BitmapFont getFont(String filename, int size)
	{
		BitmapFont bitmapFont = fonts.get(createUUID(filename, size));
		return (bitmapFont == null
			? loadFont(filename, size)
			: bitmapFont
		);
	}

	@Override
	public LabelStyle getLabelStyle(String filename, int size, Color color)
	{
		String labelId;
		BitmapFont font = getFont(filename, size);
		LabelStyle style = labelStyles.get(labelId = createUUID(filename, size, color));
		if (style == null)
		{
			labelStyles.put(
				labelId,
				(style = new LabelStyle(font, color))
			);
		}
		return style;
	}

	@Override
	public void dispose()
	{
		for(BitmapFont font : fonts.values())
		{
			font.dispose();
		}
		fonts.clear();
		labelStyles.clear();
	}
}
