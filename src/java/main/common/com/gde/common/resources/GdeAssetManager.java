/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 * - Lode -- http://gde.tips/games/luzanky/lode
 * - Box -- http://gde.tips/games/luzanky/box
 * - DoomGuy -- http://gde.tips/games/luzanky/doom-guy
 ******************************************************************************/
package com.gde.common.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.IAssetManager;

public class GdeAssetManager
extends AssetManager
implements IAssetManager
{
	@Override
	public synchronized <T> T get(String fileName, Class<T> type)
	{
		if (!isLoaded(fileName, type))
		{
			load(fileName, type);
			finishLoading();
		}
		return super.get(fileName, type);
	}
}
