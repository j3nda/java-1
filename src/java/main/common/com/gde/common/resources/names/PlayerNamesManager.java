package com.gde.common.resources.names;

import com.gde.common.resources.CommonResources;

/** simple Player's Names manager */
public class PlayerNamesManager
extends NamesManager<PlayerNameInfo>
{
	public PlayerNamesManager()
	{
		super(CommonResources.Names.players);
	}

	@Override
	protected PlayerNameInfo createFromCsv(String csvLine)
	{
		PlayerNameInfo player = new PlayerNameInfo(csvLine);
		if (
			   player.name.length() < 3 || player.name.length() > 10
			|| !player.name.toLowerCase().matches("[a-z]+")
		   )
		{
			return null;
		}
		return player;
	}
}
