package com.gde.common.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;

public abstract class GdeCsvReader<T>
{
	protected List<T> loadCsv(FileHandle file)
	{
		List<T> data = new ArrayList<>();
		String line = "";
		int index = 0;
		try(InputStreamReader is = new InputStreamReader(file.read(), StandardCharsets.UTF_8))
		{
			BufferedReader reader = new BufferedReader(is);
			while ((line = reader.readLine()) != null)
			{
				if (index++ == 0)
				{
					continue;
				}
				try
				{
					T entry = createFromCsv(line);
					if (entry != null)
					{
						data.add(entry);
					}
				}
				catch (Exception e)
				{
					System.out.println("ERROR[" + index + "]: " + line);
					e.printStackTrace();
				}
			}
		}
		catch (IOException e)
		{

		}
		return data;
	}

	protected abstract T createFromCsv(String csvLine);
}
