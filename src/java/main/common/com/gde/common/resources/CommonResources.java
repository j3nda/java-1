/*******************************************************************************
 * Copyright 2022 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.resources;

/**
 * popisuje 'resources', ktere je mozne pouzit ve 'vsech' hrach.
 * (popr. tyto resources rozsirit, pro konkretni pripad hry, eg: PexesoResources extends CommonResources)
 */
public final class CommonResources
{
	public static final String versionJson = "version.json";
	public static class Tint
	{
		public static final String square16x16 = "tint/square16x16.png";
		public static final String square16x16_9patch = "tint/square16x16.9.png";
		public static final String circle512x512_full = "tint/circle512x512-full.png";
		public static final String circle512x512_outline8 = "tint/circle512x512-outline8px.png";
		public static final String circle512x512_outline16 = "tint/circle512x512-outline16px.png";
		public static final String circle512x512_outline32 = "tint/circle512x512-outline32px.png";
		public static final String circle512x512_outline64 = "tint/circle512x512-outline64px.png";
	}
	public static class Gde
	{
		public static final String buttonExit = "gde/button-exit.png";
		public static final String buttonGdeLightbulb = "gde/button-gde-lightBulb.png";
		public static final String buttonGde = "gde/button-gde.png";
		public static final String buttonGde_9patch = "gde/button-gde.9.png";
		public static final String copyrightJson = "gde/copyright.json";
		public static final String tv1280x960 = "gde/tv-1280x960.png";
	}
	public static class Gesture
	{
		public static final String indexFinger82x96 = "gestures/indexFinger82x96.png";
	}
	public static class Animation
	{
		public static final String click72x96 = "animations/click/72x96/%03d.png";
		public static final String click679x903 = "animations/click/679x903/%03d.png";
		public static String frame(String animation, int frame)
		{
			return String.format(animation, frame);
		}
	}
	public static class Font
	{
		public static final String born2bSporty16x16 = "fonts/Born2bSportyV2-16x16.json";
		public static final String vt323 = "fonts/VT323-Regular.ttf";
	}
	public static class Shader
	{
		public static final String[] blackAndWhite = new String[] {"shaders/black-n-white.vertex", "shaders/black-n-white.fragment"};
		public static final String[] pixelate = new String[] {"shaders/pixelate.vertex", "shaders/pixelate.fragment"};
	}
	public static class Names
	{
		public static final String players = "names/players-cze.csv";
	}
}
