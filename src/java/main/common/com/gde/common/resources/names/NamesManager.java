package com.gde.common.resources.names;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.gde.common.resources.GdeCsvReader;
import com.gde.common.resources.names.NamesManager.NameInfoWithIdCounter;
import com.gde.common.utils.RandomUtils;

abstract class NamesManager<T extends NameInfoWithIdCounter>
extends GdeCsvReader<T>
{
	protected final List<T> entry;
	/** list of id, which was taked by game (~to've unique names) */
	protected final Set<T> entriesTaken;

	protected NamesManager(String csvFilename)
	{
		entry = loadCsv(Gdx.files.internal(csvFilename));
		entriesTaken = new HashSet<>();
	}

	public void clear()
	{
		entriesTaken.clear();
	}

	public T random()
	{
		T info = null;
		do
		{
			int index = RandomUtils.nextInt(0, entry.size() - 1);
			info = entry.get(index);
		}
		while(entriesTaken.contains(info));

		entriesTaken.add(info);

		return info;
	}

	public void debugNames()
	{
		for(T info : entry)
		{
			System.out.println(info);
		}
	}

	public void debugTaken()
	{
		List<NameInfoWithIdCounter> sorted = new ArrayList<>(entriesTaken);
		Collections.sort(sorted);

		System.out.println(sorted);
	}

	abstract static class NameInfoWithIdCounter
	implements Comparable<NameInfoWithIdCounter>
	{
		private static int counterId = 0;
		private static int[] max;
		public final int id;

		protected NameInfoWithIdCounter()
		{
			this.id = counterId++;
		}

		protected NameInfoWithIdCounter(int id)
		{
			this.id = id;
		}

		protected static int max(int index)
		{
			return max[index];
		}

		protected static String[] max(String[] csv)
		{
			if (max == null)
			{
				max = new int[csv.length];
				for (int i = 0; i < max.length; i++)
				{
					max[i] = 1;
				}
			}
			for (int i = 0; i < max.length; i++)
			{
				if (i < csv.length && csv[i].length() > max[i])
				{
					max[i] = csv[i].length();
				}
			}
			return csv;
		}

		protected int counterId()
		{
			return counterId;
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			NameInfoWithIdCounter other = (NameInfoWithIdCounter) obj;
			if (id != other.id)
			{
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(NameInfoWithIdCounter o)
		{
			return (this.id == o.id
				? 0
				: (this.id < o.id
					? -1
					: +1
				)
			);
		}
	}
}
