/*******************************************************************************
 * Copyright 2020 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.utils;

import java.util.Random;

public class RandomUtils
{
	private static final Random random = new Random();

	/** return random number in range [min, max] included both */
	public static int nextInt(int min, int max)
	{
		if (min == max)
		{
			return min;
		}
		return min + random.nextInt(max - min + 1);
	}

	/** return random number in range [min, max] included both */
	public static int nextInt(float min, float max)
	{
		return nextInt((int)min, (int)max);
	}

	/** return random number in range [min, max] included both */
	public static float nextFloat(float min, float max)
	{
		if (min == max)
		{
			return min;
		}
		return min + random.nextFloat() * (max - min);
	}
}
