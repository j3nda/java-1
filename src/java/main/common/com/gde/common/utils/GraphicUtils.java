/*******************************************************************************
 * Copyright 2012 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class GraphicUtils
{
	/**
	 * Set alpha in batch and return previous color to restore settings.
	 *
	 * @param batch         handle where to set alpha
	 * @param newAlpha      alpha value
	 *
	 * @return previous color of batch, so it can be restored back by batch.setColor(previous);
	 */
	public static Color setAlpha(Batch batch, float newAlpha)
	{
		Color previousColor = batch.getColor().cpy();
		batch.setColor(previousColor.r, previousColor.g, previousColor.b, newAlpha);
		return previousColor;
	}

	public static Color setAlpha(BitmapFont font, float newAlpha)
	{
		Color previousColor = font.getColor();
		font.setColor(previousColor.r, previousColor.g, previousColor.b, newAlpha);
		return previousColor;
	}

	public static Color setAlpha(Color color, float alpha)
	{
		Color alphaColor = color.cpy();
		alphaColor.a = alpha;
		return alphaColor;
	}

	public static Color setColor(Batch batch, Color color)
	{
		Color previousColor = batch.getColor().cpy();
		batch.setColor(color);
		return previousColor;
	}

	public static Texture createAlphaBackgroundTexture(int width, int height)
	{
		Pixmap pm = new Pixmap(width, height, Pixmap.Format.Alpha);
		pm.setColor(0, 0, 0, 1);
		pm.fillRectangle(0, 0, width, height);

		Texture texture = new Texture(pm);
		pm.dispose();
		return texture;
	}

	public static Vector2 calculateBestFitSize(Vector2 origSize, Vector2 maxSize)
	{
		Vector2 newSize = new Vector2(origSize);
		if (maxSize.x == -1.0f)
		{
			// count-by: maxSize.y
			newSize.x = (origSize.x / origSize.y) * maxSize.y;
			newSize.y = maxSize.y;
		}
		else
		if (maxSize.y == -1.0f)
		{
			// count-by: maxSize.x
			newSize.x = maxSize.x;
			newSize.y = (origSize.y / origSize.x) * maxSize.x;
		}
		else
		if (maxSize.x > 0.0f && maxSize.y > 0.0f)
		{
			// count-by: size orientation (portrait/landscape)
			if (origSize.x > origSize.y)
			{
				newSize.x = maxSize.x;
				newSize.y = (origSize.y / origSize.x) * maxSize.x;
			}
			else
			{
				newSize.x = (origSize.x / origSize.y) * maxSize.y;
				newSize.y = maxSize.y;
			}
		}
		return newSize;
	}

	public static float scaleToFit(float sourceWidth, float sourceHeight, float targetWidth, float targetHeight)
	{
		// -- https://docs.imgix.com/apis/rendering/size/fit
//		return Math.min(
//			dstWidth  / srcWidth,
//			dstHeight / srcHeight
//		);
//		return (outerWidth / outerHeight > srcWidth / srcHeight)
//		    ? outerHeight / srcHeight
//		    : outerWidth / srcWidth
//	    ;
//        float sourceAspectRatio = sourceWidth / sourceHeight;
//        float targetAspectRatio = targetWidth / targetHeight;
//		if (sourceAspectRatio < targetAspectRatio)
//		{
//			return targetHeight / sourceHeight;
//		}
//		else
//		{
//			return targetWidth / targetWidth;
//		}
		return 0;
	}

	public static Rectangle getScaled(float imgWidth, float imgHeight, float boundaryWidth, float boundaryHeight)
	{
		float original_width = imgWidth;
		float original_height = imgHeight;
		float bound_width = boundaryWidth;
		float bound_height = boundaryHeight;
		float new_width = original_width;
		float new_height = original_height;

		// first check if we need to scale width
		if (original_width > bound_width)
		{
			// scale width to fit
			new_width = bound_width;
			// scale height to maintain aspect ratio
			new_height = (new_width * original_height) / original_width;
		}

		// then check if we need to scale even with the new height
		if (new_height > bound_height)
		{
			// scale height to fit instead
			new_height = bound_height;
			// scale width to maintain aspect ratio
			new_width = (new_height * original_width) / original_height;
		}

		return new Rectangle(0, 0, new_width, new_height);
	}
}
