/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Lode -- http://gde.tips/games/luzanky/lode
 ******************************************************************************/
package com.gde.common.utils;

import com.badlogic.gdx.assets.IAssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationUtils
{
	public static TextureRegion[] createFrames(IAssetManager resources, String resourceId, int... frameIds)
	{
		TextureRegion[] frames = new TextureRegion[frameIds.length];
		for(int i = 0; i < frameIds.length; i++)
		{
			frames[i] = new TextureRegion(
				resources.get(String.format(resourceId, frameIds[i]), Texture.class)
			);
		}
		return frames;
	}

	public static int[] createFramesRange(int frames)
	{
		int[] result = new int[frames];
		for (int i = 0; i < frames; i++)
		{
			result[i] = i + 1;
		}
		return result;
	}
}
