/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.utils;

/**
 * As android doesn't have java.awt.Point this is substitution to it.
 */
public class Point
implements Point2D
{
	public int x;
	public int y;


	public Point()
	{

	}

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX()
	{
		return x;
	}

	@Override
	public int getY()
	{
		return y;
	}

	public static double getDistance(Point pointA, Point pointB)
	{
		return Math.sqrt(
			  Math.pow((double)pointA.x - pointB.x, 2)
			+ Math.pow((double)pointA.y - pointB.y, 2)
		);
	}

	@Override
	public double getDistanceTo(Point anotherPoint)
	{
		return getDistance(this, anotherPoint);
	}

	@Override
	public Point set(int x, int y)
	{
		this.x = x;
		this.y = y;
		return this;
	}

	@Override
	public void setX(int x)
	{
		this.x = x;
	}

	@Override
	public void setY(int y)
	{
		this.y = y;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Point other = (Point) obj;
		if (x != other.x)
		{
			return false;
		}
		if (y != other.y)
		{
			return false;
		}
		return true;
	}
}
