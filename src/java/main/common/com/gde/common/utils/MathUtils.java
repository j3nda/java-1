/*******************************************************************************
 * Copyright 2022 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.utils;

import java.util.List;

public class MathUtils
{
	public static int max(int... numbers)
	{
		int max = Integer.MIN_VALUE;
		for(int num : numbers)
		{
			if (num > max)
			{
				max = num;
			}
		}
		return max;
	}

	public static int max(int[][] numbers)
	{
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < numbers.length; i++)
		{
			for(int j = 0; j < numbers[i].length; j++)
			{
				if (numbers[i][j] > max)
				{
					max = numbers[i][j];
				}
			}
		}
		return max;
	}

	public static int max(Integer[] numbers)
	{
		int max = Integer.MIN_VALUE;
		for(Integer num : numbers)
		{
			if (num > max)
			{
				max = num;
			}
		}
		return max;
	}

	public static int max(List<Integer> numbers)
	{
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < numbers.size(); i++)
		{
			if (numbers.get(i) > max)
			{
				max = numbers.get(i);
			}
		}
		return max;
	}

	public static int min(int[][] numbers)
	{
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < numbers.length; i++)
		{
			for(int j = 0; j < numbers[i].length; j++)
			{
				if (numbers[i][j] < min)
				{
					min = numbers[i][j];
				}
			}
		}
		return min;
	}

	public static int min(List<Integer> numbers)
	{
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < numbers.size(); i++)
		{
			if (numbers.get(i) < min)
			{
				min = numbers.get(i);
			}
		}
		return min;
	}
}
