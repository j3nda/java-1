/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2019 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.IScreen;
import com.gde.common.graphics.screens.IScreenResources;

public abstract class GdeGame<TScreenType extends Enum<TScreenType>, TScreenResources extends IScreenResources>
extends Game
implements IGdeGame<TScreenType, TScreenResources>
{
	private final ObjectMap<TScreenType, IScreen<TScreenType>> gameScreensUsed;
	public static class ReturnCodes
	{
		public static final int ALL_OK = 0;
		public static final int USER_ENDED = 1;
		public static final int ERROR_EXCEPTION = 2;
		public static final int ERROR_NO_PREVIOUS_SCREEN = 3;
	}
	/** on android devices application shutdown is not immediate, this will ensure render won't be called, after we call exit */
	private static boolean end = false;
	private final RuntimeException inputException = null;
	private TScreenResources screenResources;


	public GdeGame()
	{
		this.gameScreensUsed = new ObjectMap<>();
	}

	@Override
	public TScreenResources getScreenResources()
	{
		return screenResources;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IScreen<TScreenType> getScreen()
	{
		return (IScreen<TScreenType>)super.getScreen();
	}

	@Override
	public IScreen<TScreenType> getScreen(TScreenType screenType)
	{
		return gameScreensUsed.get(screenType);
	}

	@Override
	public void setScreen(IScreen<TScreenType> screen)
	{
		IScreen<TScreenType> prevScreen = getScreen();
		if (prevScreen != null)
		{
			Gdx.app.debug(
				getClass().getName(),
				"Switching from screen " + prevScreen.getScreenType() + " to " + screen.getScreenType()
			);
			if (prevScreen.getScreenType() == screen.getScreenType())
			{
				Gdx.app.debug(
					getClass().getName(),
					"setScreen: (prev,next)"
						+ " x ("
						+ prevScreen.getScreenType().name()+","+screen.getScreenType().name()
						+ ") // SAME SCREEN!!!"
				);
				return;
			}
		}
		else
		{
			Gdx.app.debug(getClass().getName(), "Switching to screen " + screen.getScreenType());
		}

		try
		{
			gameScreensUsed.put(screen.getScreenType(), screen);
			super.setScreen(screen);
		}
		catch (RuntimeException e)
		{
			end(e);
		}
	}

	@Override
	public void end()
	{
		dispose();
		GdeGame.exit(GdeGame.ReturnCodes.USER_ENDED);
	}

	protected void end(Exception e)
	{
		Gdx.app.error(getClass().getName(), "Current screen: " + getScreen().getClass().getName());
		Gdx.app.error(getClass().getName(), "Application detected fatal exception!", e);
		GdeGame.exit(ReturnCodes.ERROR_EXCEPTION);
	}

	/**
	 * Because its not possible to return anything from libgdx app,
	 * it is recommended to print something on std out and parse that shit
	 * out, when you require something like this.
	 *
	 * @param returnCode
	 */
	public static final void exit(int returnCode)
	{
		end = true;
		// don't replace by ILogger! tests depends on this string!
		Gdx.app.log("__RETURN_CODE__", "" + returnCode);
		Gdx.app.exit();
	}

	/**
	 * Method overriden, so we can perform global Exception catching.
	 */
	@Override
	public void render()
	{
		if (end)
		{
			return;
		}
		try
		{
			if (inputException != null)
			{
				end(inputException);
				return;
			}
			super.render();
		}
		catch(Exception e)
		{
			end(e);
		}
	}

	/**
	 * Method overriden, so we can perform global Exception catching.
	 */
	@Override
	public void pause()
	{
		try
		{
			super.pause();
		}
		catch(Exception e)
		{
			end(e);
		}
	}

	/**
	 * Method overriden, so we can perform global Exception catching.
	 */
	@Override
	public void resume()
	{
		try
		{
			super.resume();
		}
		catch(Exception e)
		{
			end(e);
		}
	}

	/**
	 * Method overriden, so we can perform global Exception catching.
	 */
	@Override
	public void resize(int width, int height)
	{
		try
		{
			super.resize(width, height);
		}
		catch(Exception e)
		{
			end(e);
		}
	}

	@Override
	public void dispose()
	{
		for(IScreen<TScreenType> screenUsed : gameScreensUsed.values())
		{
			screenUsed.dispose();
		}
		screenResources.dispose();
	}

	@Override
	public void create()
	{
		Camera camera = createCamera(
			Gdx.graphics.getWidth(),
			Gdx.graphics.getHeight()
		);
		screenResources = createScreenResources(
			camera,
			new ScalingViewport(
				Scaling.stretch,
				camera.viewportWidth,
				camera.viewportHeight,
				camera
			),
			new SpriteBatch()
		);
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		// keeps framerate at 60. its ok when in profiler app spent most time
		// in graphic.Sync.sync(); method as normaly it would have 600 FPS :D
		Gdx.graphics.setVSync(true);

		setNextScreen();
		Gdx.app.log(getClass().getName(), "Current density:" + Gdx.graphics.getDensity());
	}

	protected Camera createCamera(int width, int height)
	{
		OrthographicCamera camera = new OrthographicCamera(width, height);

		camera.translate(width / 2f, height / 2f);
		camera.update();

		return camera;
	}

	protected abstract TScreenResources createScreenResources(Camera camera, Viewport viewport, Batch batch);
	protected abstract void setNextScreen();
}
