/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.display.pixels;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.display.DebugDraw;

/**
 * simuluje vytvoreni 1x pixelu s moznosti vykresleni libovolnou barvou.
 * (urceno hlavne pro studijni ucely; k hlubokemu pochopeni: array[], display, alokovani/uvolneni pameti)
 */
public class ColorPixel
extends Actor
implements IColorPixel
{
	/** pro rychle debugovani grafiky... */
	private static boolean debug = false;
	private Texture texture;
	private ColorPixelType textureType;
	private IColorPixelFactory textureFactory;


	public ColorPixel(int width, int height, Color color)
	{
		this(
			width,
			height,
			color,
			ColorPixelFactory.getInstance(),
			ColorPixelType.tintTextureFile
		);
	}

	private ColorPixel(int width, int height, Color color, IColorPixelFactory factory, ColorPixelType type)
	{
		textureFactory = factory;
		setColor(color);
		setWidth(width);
		setHeight(height);
		setOrigin(Align.center);
		setTextureType(type);
	}

	@Override
	public void setTextureType(ColorPixelType type)
	{
		boolean isChanged = (textureType != type);
		textureType = type;
		if (isChanged)
		{
			texture = textureFactory.createTexture(type);
		}
	}

	@Override
	public ColorPixelType getTextureType()
	{
		return textureType;
	}

	@Override
	public void dispose()
	{
		// pokud mam custom pixel a ma nejake zdroje, mel bych je umet uvolnit...
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (texture == null)
		{
			return;
		}
		Color prevColor = batch.getColor();
		Color currColor = getColor();
		currColor.a *= parentAlpha;
		batch.setColor(currColor);
		batch.draw(
			texture,
			getX(),
			getY(),
			getOriginX(),
			getOriginY(),
			getWidth(),
			getHeight(),
			getScaleX(),
			getScaleY(),
			getRotation(),
			0, // srcX
			0, // srcY
			texture.getWidth(),  // srcWidth
			texture.getHeight(), // srcHeight
			false, // flipX
			false  // flipY
		);
		batch.setColor(prevColor);
		if (debug)
		{
			DebugDraw.begin(batch);
			DebugDraw.rectangle(
				Color.GOLD,
				new Rectangle(getX(), getY(), getWidth(), getHeight())
			);
			DebugDraw.end(batch);
		}
	}
}
