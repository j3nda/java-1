/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.vectors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorGroup;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorPoint;

import space.earlygrey.shapedrawer.ShapeDrawer;

public class VectorDebug
{
	public static void cross(ShapeDrawer drawer, CrossInfo cross)
	{
		cross(drawer, cross, cross.x, cross.y);
	}

	public static void cross(ShapeDrawer drawer, CrossInfo cross, float x, float y)
	{
		Batch batch = drawer.getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		drawer.line(
			x,
			y - cross.size,
			x,
			y + cross.size,
			cross.color,
			cross.lineSize
		);
		drawer.line(
			x - cross.size,
			y,
			x + cross.size,
			y,
			cross.color,
			cross.lineSize
		);
		if (!isDrawing)
		{
			batch.end();
		}
	}

	public static void line(ShapeDrawer drawer, float x1, float y1, float x2, float y2)
	{
		Batch batch = drawer.getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		drawer.line(x1, y1, x2, y2);
		if (!isDrawing)
		{
			batch.end();
		}
	}

	public static void group(ShapeDrawer drawer, VectorGroup group, float x, float y, float unitScale, CrossInfo cross)
	{
		Batch batch = drawer.getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		cross(
			drawer,
			cross,
			x,
			y
		);
		groupVector(drawer, group, x, y, unitScale);
		groupPoints(drawer, group, x, y, unitScale);

		if (!isDrawing)
		{
			batch.end();
		}
	}

	private static void groupVector(ShapeDrawer drawer, VectorGroup group, float x, float y, float unitScale)
	{
		Batch batch = drawer.getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		Vector2 tmpCurr = new Vector2(x, y);
		Vector2 tmpPrev = new Vector2();
		while(group.parentGroup != null)
		{
			tmpPrev.set(tmpCurr);
			tmpCurr.sub(
				group.origin.x * unitScale,
				group.origin.y * unitScale
			);
			drawer.line(
				tmpPrev.x,
				tmpPrev.y,
				tmpCurr.x,
				tmpCurr.y
			);
			group = group.parentGroup;
		}

		if (!isDrawing)
		{
			batch.end();
		}
	}

	private static void groupPoints(ShapeDrawer drawer, VectorGroup group, float x, float y, float unitScale)
	{
		if (group.points == null)
		{
			return;
		}
		Batch batch = drawer.getBatch();
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}
		for(VectorPoint point : group.points)
		{
			drawer.rectangle(
				x + (point.x * unitScale),
				y + (point.y * unitScale),
				10,
				10
			);
		}
		if (!isDrawing)
		{
			batch.end();
		}
	}

	public static class CrossInfo
	{
		public float x;
		public float y;
		public final Color color;
		public final int size;
		public final int lineSize;

		public CrossInfo(Color color, int size, int lineSize)
		{
			this(0, 0, color, size, lineSize);
		}

		public CrossInfo(float x, float y, Color color, int size, int lineSize)
		{
			this.x = x;
			this.y = y;
			this.color = color;
			this.size = size;
			this.lineSize = lineSize;
		}
	}
}
