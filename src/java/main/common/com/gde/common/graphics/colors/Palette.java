/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pong -- http://gde.tips/games/luzanky/pong
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 * - Lode -- http://gde.tips/games/luzanky/lode
 ******************************************************************************/
package com.gde.common.graphics.colors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.exceptions.NotImplementedException;


/**
 * various color palletes
 * -- https://en.wikipedia.org/wiki/List_of_8-bit_computer_hardware_graphics
 * -- https://en.wikipedia.org/wiki/List_of_color_palettes
 * -- https://lospec.com/palette-list/
 */
public class Palette
{
	// TODO: colorblind ppl
	// -- https://www.datylon.com/blog/data-visualization-for-colorblind-readers
	// -- https://medium.com/swlh/how-i-designed-a-colorblind-friendly-palette-8a91a49f1220
	// -- https://thenode.biologists.com/data-visualization-with-flying-colors/research/
	// -- https://venngage.com/blog/color-blind-friendly-palette/
	/** true-to-hardware palettes */
	public static class Hardware
	{
		/** The Amstrad CPC (Colour Personal Computer) is a series of 8-bit home computers produced by Amstrad between 1984 and 1990 */
		public static class Amstrad
		{
			/** -- https://lospec.com/palette-list/amstrad-cpc */
			public static final Color[] Default() { if (_default == null) { _default = new Color[] {
				new Color(0x040404FF), new Color(0x808080FF), new Color(0xffffffFF), new Color(0x800000FF),
				new Color(0xff0000FF), new Color(0xff8080FF), new Color(0xff7f00FF), new Color(0xffff80FF),
				new Color(0xffff00FF), new Color(0x808000FF), new Color(0x008000FF), new Color(0x01ff00FF),
				new Color(0x80ff00FF), new Color(0x80ff80FF), new Color(0x01ff80FF), new Color(0x008080FF),
				new Color(0x01ffffFF), new Color(0x80ffffFF), new Color(0x0080ffFF), new Color(0x0000ffFF),
				new Color(0x00007fFF), new Color(0x7f00ffFF), new Color(0x8080ffFF), new Color(0xff80ffFF),
				new Color(0xff00ffFF), new Color(0xff0080FF), new Color(0x800080FF),
			};} return _default; }
			private static Color[] _default;
		}
		public static class Atari
		{
			// FYI: atari:rainbow:FX (~assembler)
			// -- http://www.wbswiki.com/doku.php?id=notes:atari:rainbow
			// -- https://makezine.com/2006/12/31/generate-the-atari-rainbow-eff/
			/**
			 * -- https://www.color-hex.com/color-palette/67887
			 * {brown1(644240), brown2(322424), white(f0e6da), red(821510), purple(b1a1df)}
			 */
			public static final Color[] Default() { if (_default == null) { _default = new Color[] {
				new Color(0x644240ff),//brown1
				new Color(0x322424ff),//brown2
				new Color(0xF0E6DAff),//white
				new Color(0x821510ff),//red
				new Color(0xB1A1DFff),//purple
			};} return _default; }
			private static Color[] _default;
			/** -- https://en.wikipedia.org/wiki/CTIA_and_GTIA#/media/File:Atari_CTIA_&_TIA_NTSC_palette.png */
			public static final Color[] CTIA() { throw new NotImplementedException(); } // TODO
			/**
			 * -- https://lospec.com/palette-list/atari-8-bit-family-gtia
			 * -- https://commons.wikimedia.org/wiki/File:Atari_8_bit_GTIA_NTSC_palette.png
			 */
			public static final Color[] GTIA() { throw new NotImplementedException(); } // TODO

		}
		public static class Commodore
		{
			/** Commodore64 was an 8-bit home computer released in 1982. */
			// -- https://www.aaronbell.com/secret-colours-of-the-commodore-64/
			public static class C64
			{
				/**
				 * original: 16x colors palette
				 * -- https://www.c64-wiki.com/wiki/Color
				 */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000ff),//black
					new Color(0xFFFFFFff),//white
					new Color(0x880000ff),//red
					new Color(0xAAFFEEff),//cyan
					new Color(0xCC44CCff),//violet/purple
					new Color(0x00CC55ff),//green
					new Color(0x0000AAff),//blue
					new Color(0xEEEE77ff),//yellow
					new Color(0xDD8855ff),//orange
					new Color(0x664400ff),//brown
					new Color(0xFF7777ff),//light-red
					new Color(0x333333ff),//dark-grey/grey1
					new Color(0x777777ff),//grey2
					new Color(0xAAFF66ff),//light-green
					new Color(0x0088FFff),//light-blue
					new Color(0xBBBBBBff),//light-grey/grey3
				};} return _default; }
				private static Color[] _default;
				/**
				 * alternative: 16x colors palette
				 * -- https://www.c64-wiki.com/wiki/Color
				 */
				public static final Color[] Alternative() { if (_alternative == null) { _alternative = new Color[] {
					new Color(0x000000ff),//black
					new Color(0xFFFFFFff),//white
					new Color(0x9F4E44ff),//red(~dark-red)
					new Color(0x6ABFC6ff),//cyan
					new Color(0xA057A3ff),//violet/purple
					new Color(0x5CAB5Eff),//green(~dark-green)
					new Color(0x50459Bff),//blue(~dark-blue)
					new Color(0xC9D487ff),//yellow(~dark-yellow)
					new Color(0xA1683Cff),//orange(~brown1)
					new Color(0x6D5412ff),//brown(~brown2)
					new Color(0xCB7E75ff),//light-red(~red)
					new Color(0x626262ff),//dark-grey/grey1
					new Color(0x898989ff),//grey2
					new Color(0x9AE29Bff),//light-green
					new Color(0x887ECBff),//light-blue
					new Color(0xADADADff),//light-grey/grey3
				};} return _alternative; }
				private static Color[] _alternative;
			}
			/** The Commodore VIC-20 (known as the VC-20 in Germany and the VIC-1001 in Japan) is an 8-bit home computer announced in 1980 */
			public static class Vic20
			{
				/**
				 * original: 16x colors palette
				 * -- https://lospec.com/palette-list/commodore-vic-20
				 */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000ff),//black
					new Color(0xFFFFFFff),//white
					new Color(0xA8734Aff),//dark-brown(~brown1)
					new Color(0xE9B287ff),//light-brown(~brown2)
					new Color(0x772D26ff),//brown3
					new Color(0xB66862ff),//brown4
					new Color(0x85D4DCff),//dark-cyan
					new Color(0xC5FFFFff),//cyan
					new Color(0xA85FB4ff),//dark-magenta
					new Color(0xE99DF5ff),//magenta
					new Color(0x559E4Aff),//dark-green
					new Color(0x92DF87ff),//green
					new Color(0x42348Bff),//dark-blue
					new Color(0x7E70CAff),//blue
					new Color(0xBDCC71ff),//dark-yellow
					new Color(0xFFFFB0ff),//yellow
				};} return _default; }
				private static Color[] _default, _logo, _pepto;
				/** https://www.color-hex.com/color-palette/45292 */
				public static final Color[] Logo() { if (_logo == null) { _logo = new Color[] {
					new Color(0x0A00FBff),//blue
					new Color(0xE60059ff),//purple
					new Color(0xFF2E00ff),//red
					new Color(0xFFB008ff),//yellow
					new Color(0x55DA00ff),//green
				};} return _logo; }
				/**
				 * recalculated palette for the VIC II (Commodore 64) by Pepto
				 * -- https://lospec.com/palette-list/colodore
				 * -- https://www.pepto.de/projects/colorvic/
				 */
				public static final Color[] Pepto() { if (_pepto == null) { _pepto = new Color[] {
					new Color(0x000000FF), new Color(0x4a4a4aFF), new Color(0x7b7b7bFF), new Color(0xb2b2b2FF),
					new Color(0xffffffFF), new Color(0x813338FF), new Color(0xc46c71FF), new Color(0x553800FF),
					new Color(0x8e5029FF), new Color(0xedf171FF), new Color(0xa9ff9fFF), new Color(0x56ac4dFF),
					new Color(0x75cec8FF), new Color(0x706debFF), new Color(0x2e2c9bFF), new Color(0x8e3c97FF),
				};} return _pepto; }
			}
		}
		/** ZX Spectrum was an 8-bit home computer released in 1982. */
		public static class ZxSpectrum
		{
			/**
			 * true-to-hardware palette
			 * -- https://lospec.com/palette-list/zx-spectrum
			 */
			public static final Color[] Default() { if (_default == null) { _default = new Color[] {
				new Color(0x000000ff),//black
				new Color(0x0000D8ff),//dark-blue
				new Color(0x0000FFff),//blue
				new Color(0xD80000ff),//dark-red
				new Color(0xFF0000ff),//red
				new Color(0xD800D8ff),//dark-purple
				new Color(0xFF00FFff),//purple
				new Color(0x00D800ff),//dark-green
				new Color(0x00FF00ff),//green
				new Color(0x00D8D8ff),//dark-cyan
				new Color(0x00FFFFff),//cyan
				new Color(0xD8D800ff),//dark-yellow
				new Color(0xFFFF00ff),//yellow
				new Color(0xD8D8D8ff),//grey
				new Color(0xFFFFFFff),//white
			};} return _default; }
			private static Color[] _default, _light, _dark;
			public static final Color[] Light() { if (_light == null) { _light = removeColors(Default(), 0, 1, 3, 5, 7, 9, 10); } return _light; }
			public static final Color[] Dark()  { if (_dark == null)  { _dark = removeColors(Default(), 2, 4, 6, 8, 10, 12, 14); } return _dark; }
		}
		public static class Apple
		{
			/**
			 * The Macintosh II was a personal computer released by Apple in 1987.
			 * -- https://lospec.com/palette-list/macintosh-ii
			 * -- https://en.m.wikipedia.org/wiki/Macintosh_II
			 */
			public static final Color[] Macintosh2() { if (_mac2 == null) { _mac2 = new Color[] {
				new Color(0xffffffFF),//white
				new Color(0xffff00FF),//yellow
				new Color(0xff6500FF),//orange
				new Color(0xdc0000FF),//red
				new Color(0xff0097FF),//magenta
				new Color(0x360097FF),//pink
				new Color(0x0000caFF),//blue
				new Color(0x0097ffFF),//light-blue
				new Color(0x00a800FF),//light-green
				new Color(0x006500FF),//dark-green
				new Color(0x653600FF),//dark-brown
				new Color(0x976536FF),//light-brown
				new Color(0xb9b9b9FF),//light-gray
				new Color(0x868686FF),//gray
				new Color(0x454545FF),//dark-gray
				new Color(0x000000FF),//black
			};} return _mac2; }
			private static Color[] _mac2, _apple2;
			/** -- https://lospec.com/palette-list/apple-ii */
			public static final Color[] Apple2() { if (_apple2 == null) { _apple2 = new Color[] {
				new Color(0x000000FF),//black
				new Color(0x515c16FF),//brown
				new Color(0x843d52FF),//dark-red
				new Color(0xea7d27FF),//orange
				new Color(0x514888FF),//dark-blue
				new Color(0xe85defFF),//magenta
				new Color(0xf5b7c9FF),//pink
				new Color(0x006752FF),//dark-green
				new Color(0x00c82cFF),//light-green
				new Color(0x919191FF),//gray
				new Color(0xc9d199FF),//dark-yellow
				new Color(0x00a6f0FF),//light-blue
				new Color(0x98dbc9FF),//froggy-green
				new Color(0xc8c1f7FF),//light-magenta
				new Color(0xffffffFF),//white
			};} return _apple2; }
		}
		public static class Casio
		{
			/** -- https://lospec.com/palette-list/casio-basic */
			public static final Color[] Default() { if (_default == null) { _default = new Color[] {
				new Color(0x000000FF),//black
				new Color(0x83b07eFF),//green
			};} return _default; }
			private static Color[] _default;
		}
		public static class Nintendo
		{
			/** -- https://en.wikipedia.org/wiki/Game_Boy */
			public static class GameBoy
			{
				/**
				 * A grayscale palette calculated using 2-bits per color.
				 * It was used by the original gameboy and a few other computer systems.
				 * -- https://lospec.com/palette-list/2-bit-grayscale
				 * -- https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_color_formats#2-bit_Grayscale
				 */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000FF),
					new Color(0x676767FF),
					new Color(0xb6b6b6FF),
					new Color(0xffffffFF),
				};} return _default; }
				private static Color[] _default, _super;
				/**
				 * The default palette for the Nintendo Super Gameboy, a SNES catridge that allowed you to play Gameboy and Gameboy Color games on the SNES.
				 * -- https://lospec.com/palette-list/nintendo-super-gameboy
				 */
				public static final Color[] Super() { if (_super == null) { _super = new Color[] {
					new Color(0x331e50FF),//dark-blue
					new Color(0xa63725FF),//dark-brown
					new Color(0xd68e49FF),//light-brown
					new Color(0xf7e7c6FF),//light-yellow
				};} return _super; }
			}
			/** The Nintendo Entertainment System was an 8-bit video game console released by Nintendo in 1983. */
			public static class NES
			{
				/** -- https://lospec.com/palette-list/nintendo-entertainment-system */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000FF), new Color(0xfcfcfcFF), new Color(0xf8f8f8FF), new Color(0xbcbcbcFF),
					new Color(0x7c7c7cFF), new Color(0xa4e4fcFF), new Color(0x3cbcfcFF), new Color(0x0078f8FF),
					new Color(0x0000fcFF), new Color(0xb8b8f8FF), new Color(0x6888fcFF), new Color(0x0058f8FF),
					new Color(0x0000bcFF), new Color(0xd8b8f8FF), new Color(0x9878f8FF), new Color(0x6844fcFF),
					new Color(0x4428bcFF), new Color(0xf8b8f8FF), new Color(0xf878f8FF), new Color(0xd800ccFF),
					new Color(0x940084FF), new Color(0xf8a4c0FF), new Color(0xf85898FF), new Color(0xe40058FF),
					new Color(0xa80020FF), new Color(0xf0d0b0FF), new Color(0xf87858FF), new Color(0xf83800FF),
					new Color(0xa81000FF), new Color(0xfce0a8FF), new Color(0xfca044FF), new Color(0xe45c10FF),
					new Color(0x881400FF), new Color(0xf8d878FF), new Color(0xf8b800FF), new Color(0xac7c00FF),
					new Color(0x503000FF), new Color(0xd8f878FF), new Color(0xb8f818FF), new Color(0x00b800FF),
					new Color(0x007800FF), new Color(0xb8f8b8FF), new Color(0x58d854FF), new Color(0x00a800FF),
					new Color(0x006800FF), new Color(0xb8f8d8FF), new Color(0x58f898FF), new Color(0x00a844FF),
					new Color(0x005800FF), new Color(0x00fcfcFF), new Color(0x00e8d8FF), new Color(0x008888FF),
					new Color(0x004058FF), new Color(0xf8d8f8FF), new Color(0x787878FF),
				};} return _default; }
				private static Color[] _default, _wiki;
				/** -- https://lospec.com/palette-list/nes-wikipedia */
				public static final Color[] Wiki() { if (_wiki == null) { _wiki = new Color[] {
					new Color(0x000000FF), new Color(0x666666FF), new Color(0xaeaeaeFF), new Color(0x002a88FF),
					new Color(0x155fdaFF), new Color(0x64b0feFF), new Color(0xc1e0feFF), new Color(0x1412a8FF),
					new Color(0x4240feFF), new Color(0x9390feFF), new Color(0xd4d3feFF), new Color(0x3b00a4FF),
					new Color(0x7627ffFF), new Color(0xc777feFF), new Color(0xe9c8feFF), new Color(0x5c007eFF),
					new Color(0xa11bcdFF), new Color(0xf36afeFF), new Color(0xfbc3feFF), new Color(0x6e0040FF),
					new Color(0xb81e7cFF), new Color(0xfe6ecdFF), new Color(0xfec5ebFF), new Color(0x6c0700FF),
					new Color(0xb53220FF), new Color(0xfe8270FF), new Color(0xfecdc6FF), new Color(0x571d00FF),
					new Color(0x994f00FF), new Color(0xeb9f23FF), new Color(0xf7d9a6FF), new Color(0x343500FF),
					new Color(0x6c6e00FF), new Color(0xbdbf00FF), new Color(0xe5e695FF), new Color(0x0c4900FF),
					new Color(0x388700FF), new Color(0x89d900FF), new Color(0xd0f097FF), new Color(0x005200FF),
					new Color(0x0d9400FF), new Color(0x5de530FF), new Color(0xbef5abFF), new Color(0x004f08FF),
					new Color(0x009032FF), new Color(0x45e182FF), new Color(0xb4f3cdFF), new Color(0x00404eFF),
					new Color(0x007c8eFF), new Color(0x48cedfFF), new Color(0xb5ecf3FF), new Color(0x4f4f4fFF),
					new Color(0xb8b8b8FF), new Color(0xfefefeFF),
				};} return _wiki; }
			}
		}
		public static class TV
		{
			/**
			 * SECAM was a color television released in France, 1967.
			 * -- https://lospec.com/palette-list/secam
			 * -- https://en.wikipedia.org/wiki/SECAM
			 */
			public static class SECAM
			{
				/** The palette is a 3-bit RGB made by mapping the luminance to color and ignoring the hue. */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000FF), new Color(0x2121ffFF), new Color(0xf03c79FF), new Color(0xff50ffFF),
					new Color(0x7fff00FF), new Color(0x7fffffFF), new Color(0xffff3fFF), new Color(0xffffffFF),
				};} return _default; }
				private static Color[] _default;
			}
			/** CGA was a graphics card released in 1981 for the IBM PC. */
			public static class CGA
			{
				/**
				 * The standard mode uses one of two 4-color palettes (each with a low-intensity and high-intensity mode),
				 * but a hack allows use of all 16.
				 * -- https://lospec.com/palette-list/color-graphics-adapter
				 */
				public static final Color[] Full() { return EGA.Default(); }

				/** -- https://en.wikipedia.org/wiki/Color_Graphics_Adapter */
				public static class Mode4
				{
					public static class Palette0
					{
						/**
						 * standard-mode (high) ~ black can be replaced by any of the other colors in full cga-palette
						 * -- https://lospec.com/palette-list/cga-palette-0-high
						 */
						public static final Color[] High() { if (_high == null) { _high = new Color[] {
							new Color(0x000000ff),//black
							new Color(0x55FF55ff),//light-green
							new Color(0xFF5555ff),//light-red
							new Color(0xFFFF55ff),//yellow
						};} return _high; }
						private static Color[] _high, _low;
						/**
						 * standard-mode (low) ~ black can be replaced by any of the other colors in full cga-palette
						 * -- https://lospec.com/palette-list/cga-palette-0-low
						 */
						public static final Color[] Low() { if (_low == null) { _low = new Color[] {
							new Color(0x000000ff),//black
							new Color(0x00AA00ff),//green
							new Color(0xAA0000ff),//red
							new Color(0xAA5500ff),//brown
						};} return _low; }
					}
					public static class Palette1
					{
						/**
						 * standard-mode (high) ~ black can be replaced by any of the other colors in full cga-palette
						 * -- https://lospec.com/palette-list/cga-palette-1-high
						 */
						public static final Color[] High() { if (_high == null) { _high = new Color[] {
							new Color(0x000000ff),//black
							new Color(0x55FFFFff),//light-cyan
							new Color(0xFF55FFff),//magenta
							new Color(0xFFFFFFff),//white
						};} return _high; }
						private static Color[] _high, _low;
						/**
						 * standard-mode (low) ~ black can be replaced by any of the other colors in full cga-palette
						 * -- https://lospec.com/palette-list/cga-palette-1-low
						 */
						public static final Color[] Low() { if (_low == null) { _low = new Color[] {
							new Color(0x000000ff),//black
							new Color(0x00AAAAff),//cyan
							new Color(0xAA00AAff),//magenta
							new Color(0xAAAAAAff),//light-grey
						};} return _low; }
					}
				}
				public static class Mode5
				{
					/**
					 * standard-mode (high) ~ black can be replaced by any of the other colors in full cga-palette
					 * -- https://lospec.com/palette-list/cga-palette-2-high
					 */
					public static final Color[] High() { if (_high == null) { _high = new Color[] {
						new Color(0x000000ff),//black
						new Color(0x55FFFFff),//light-cyan
						new Color(0xFF5555ff),//light-red
						new Color(0xFFFFFFff),//white
					};} return _high; }
					private static Color[] _high, _low;
					/**
					 * unoffical (low) ~ black can be replaced by any of the other colors in full cga-palette
					 * -- https://lospec.com/palette-list/cga-palette-1-low
					 */
					public static final Color[] Low() { if (_low == null) { _low = new Color[] {
						new Color(0x000000ff),//black
						new Color(0x00AAAAff),//cyan
						new Color(0xAA0000ff),//red
						new Color(0xAAAAAAff),//light-grey
					};} return _low; }
				}
				public static final Color[] Default() { return Mode4.Palette1.High(); }
			}
			/** EGA was a graphics card released on October 1984 for the IBM PC. */
			public static class EGA
			{
				/** EGA default palete: 16x colors */
				public static final Color[] Default() { if (_default == null) { _default = new Color[] {
					new Color(0x000000ff),//Black
					new Color(0x0000AAff),//Blue
					new Color(0x00AA00ff),//Green
					new Color(0x00AAAAff),//Cyan
					new Color(0xAA0000ff),//Red
					new Color(0xAA00AAff),//Magenta
					new Color(0xAA5500ff),//Brown
					new Color(0xAAAAAAff),//White / light gray
					new Color(0x555555ff),//Dark gray / bright black
					new Color(0x5555FFff),//Bright Blue
					new Color(0x55FF55ff),//Bright green
					new Color(0x55FFFFff),//Bright cyan
					new Color(0xFF5555ff),//Bright red
					new Color(0xFF55FFff),//Bright magenta
					new Color(0xFFFF55ff),//Bright yellow
					new Color(0xFFFFFFff),//Bright white
				};} return _default; }
				private static Color[] _default, _full;
				/**
				 * It produces a display of up to 16 colors either using a fixed palette
				 * or one selected from a gamut of 64 colors depending on the mode.
				 * -- https://lospec.com/palette-list/enhanced-graphics-adapter
				 */
				public static final Color[] Full() { if (_full == null) { _full = new Color[] {
					new Color(0x000000ff), new Color(0x0000AAff), new Color(0x00AA00ff), new Color(0x00AAAAff),
					new Color(0xAA0000ff), new Color(0xAA00AAff), new Color(0xAAAA00ff), new Color(0xAAAAAAff),
					new Color(0x000055ff), new Color(0x0000FFff), new Color(0x00AA55ff), new Color(0x00AAFFff),
					new Color(0xAA0055ff), new Color(0xAA00FFff), new Color(0xAAAA55ff), new Color(0xAAAAFFff),
					new Color(0x005500ff), new Color(0x0055AAff), new Color(0x00FF00ff), new Color(0x00FFAAff),
					new Color(0xAA5500ff), new Color(0xAA55AAff), new Color(0xAAFF00ff), new Color(0xAAFFAAff),
					new Color(0x005555ff), new Color(0x0055FFff), new Color(0x00FF55ff), new Color(0x00FFFFff),
					new Color(0xAA5555ff), new Color(0xAA55FFff), new Color(0xAAFF55ff), new Color(0xAAFFFFff),
					new Color(0x550000ff), new Color(0x5500AAff), new Color(0x55AA00ff), new Color(0x55AAAAff),
					new Color(0xFF0000ff), new Color(0xFF00AAff), new Color(0xFFAA00ff), new Color(0xFFAAAAff),
					new Color(0x550055ff), new Color(0x5500FFff), new Color(0x55AA55ff), new Color(0x55AAFFff),
					new Color(0xFF0055ff), new Color(0xFF00FFff), new Color(0xFFAA55ff), new Color(0xFFAAFFff),
					new Color(0x555500ff), new Color(0x5555AAff), new Color(0x55FF00ff), new Color(0x55FFAAff),
					new Color(0xFF5500ff), new Color(0xFF55AAff), new Color(0xFFFF00ff), new Color(0xFFFFAAff),
					new Color(0x555555ff), new Color(0x5555FFff), new Color(0x55FF55ff), new Color(0x55FFFFff),
					new Color(0xFF5555ff), new Color(0xFF55FFff), new Color(0xFFFF55ff), new Color(0xFFFFFFff),
				};} return _full; }
			}
		}
	}
	public static class OS
	{
		public static class Windows
		{
			/** -- https://lospec.com/palette-list/microsoft-windows */
			public static final Color[] v3_x() { if (_3x == null) { _3x = new Color[] {
				new Color(0x000000FF),//black
				new Color(0x7e7e7eFF),//dark-gray
				new Color(0xbebebeFF),//light-gray
				new Color(0xffffffFF),//white
				new Color(0x7e0000FF),//dark-red
				new Color(0xfe0000FF),//red
				new Color(0x047e00FF),//dark-green
				new Color(0x06ff04FF),//light-green
				new Color(0x7e7e00FF),//dark-yellow
				new Color(0xffff04FF),//light-yellow
				new Color(0x00007eFF),//dark-blue
				new Color(0x0000ffFF),//light-blue
				new Color(0x7e007eFF),//dark-magenta
				new Color(0xfe00ffFF),//light-magenta
				new Color(0x047e7eFF),//dark-cyan
				new Color(0x06ffffFF),//light-cyan
			};} return _3x; }
			private static Color[] _3x, _10cn, _10cl;
			/** -- https://devblogs.microsoft.com/commandline/updating-the-windows-console-colors/ */
			public static final Color[] v10consoleLegacy() { if (_10cl == null) { _10cl = new Color[] {
				new Color(0x000000FF),//black
				new Color(0x000080FF),//dark-blue
				new Color(0x008000FF),//dark-green
				new Color(0x008080FF),//dark-cyan
				new Color(0x800000FF),//dark-red
				new Color(0x800080FF),//dark-magenta
				new Color(0x808000FF),//dark-yellow
				new Color(0xc0c0c0FF),//dark-white
				new Color(0x808080FF),//light-black
				new Color(0x0000ffFF),//light-blue
				new Color(0x00ff00FF),//light-green
				new Color(0x00ffffFF),//light-cyan
				new Color(0xff0000FF),//light-red
				new Color(0xff00ffFF),//light-magenta
				new Color(0xffff00FF),//light-yellow
				new Color(0xffffffFF),//white
			};} return _10cl; }
			/**
			 * This is the new default color scheme used in the Windows Console starting in Windows 10 build 16257.
			 * It is designed to emulate the old IBM / DOS defaults while being significantly more readable on modern LCD screens.
			 * -- https://lospec.com/palette-list/campbell-new-windows-console
			 * -- https://devblogs.microsoft.com/commandline/updating-the-windows-console-colors/
			 */
			public static final Color[] v10console() { if (_10cn == null) { _10cn = new Color[] {
				new Color(0x0c0c0cFF),//black
				new Color(0x0037daFF),//dark-blue
				new Color(0x13a10eFF),//dark-green
				new Color(0x3a96ddFF),//dark-cyan
				new Color(0xc50f1fFF),//dark-red
				new Color(0x881798FF),//dark-magenta
				new Color(0xc19c00FF),//dark-yellow
				new Color(0xccccccFF),//dark-white
				new Color(0x767676FF),//light-black
				new Color(0x3b78ffFF),//light-blue
				new Color(0x16c60cFF),//light-green
				new Color(0x61d6d6FF),//light-cyan
				new Color(0xe74856FF),//light-red
				new Color(0xb4009eFF),//light-magenta
				new Color(0xf9f1a5FF),//light-yellow
				new Color(0xf2f2f2FF),//white
			};} return _10cn; }
		}
		/**
		 * RISC OS was an operating system designed by Acorn Computers in 1987.
		 * -- https://en.m.wikipedia.org/wiki/RISC_OS
		 * -- https://en.m.wikipedia.org/wiki/Acorn_Computers
		 */
		public static class Risc
		{
			/** -- https://lospec.com/palette-list/risc-os */
			public static final Color[] Default() { if (_default == null) { _default = new Color[] {
				new Color(0xffffffFF),//white
				new Color(0xdcdcdcFF),//light-gray
				new Color(0xb9b9b9FF),//gray
				new Color(0x979797FF),//dark-gray0
				new Color(0x767676FF),//dark-gray1
				new Color(0x555555FF),//dark-gray2
				new Color(0x363636FF),//dark-gray3
				new Color(0x000000FF),//black
				new Color(0x004597FF),//blue
				new Color(0xeded04FF),//yellow
				new Color(0x05ca00FF),//green
				new Color(0xdc0000FF),//red
				new Color(0xededb9FF),//light-yellow
				new Color(0x558600FF),//dark-green
				new Color(0xffb900FF),//orange
				new Color(0x04b9ffFF),//light-blue
			};} return _default; }
			private static Color[] _default;
		}
	}
	/** various graphic effects */
	public static class Gfx
	{
		/**
		 * rainbow colors
		 * -- https://www.krishnamani.in/color-codes-for-rainbow-vibgyor-colours/
		 * ~ ROYGBIV or VIBGYOR Rainbow Color Codes
		 * ~ VIBGYOR is an acronym for Violet, Indigo, Blue, Green, Yellow, Orange and Red while ROYGBIV is in the reverse order.
		 */
		public static class Rainbow
		{
			/**
			 * -- https://colorswall.com/palette/102
			 * 7: {red(FF0000), orange(FFA500), yellow(FFFF00), green(008000), blue(0000FF), indigo(4B0082), violet(EE82EE)}
			 */
			public static final Color[] roygbiv1() { if (_roygbiv1 == null) { _roygbiv1 = new Color[] {
				new Color(0xFF0000ff),//red
				new Color(0xFFA500ff),//orange
				new Color(0xFFFF00ff),//yellow
				new Color(0x008000ff),//green
				new Color(0x0000FFff),//blue
				new Color(0x4B0082ff),//indigo
				new Color(0xEE82EEff),//violet
			};} return _roygbiv1; }
			private static Color[] _roygbiv1, _roygbiv2, _forKidsRoygbv, _roygb, _repeated, _alternative;
			/**
			 * -- https://www.krishnamani.in/color-codes-for-rainbow-vibgyor-colours/
			 * 7: {red(FF0000), orange(FF7F00), yellow(FFFF00), green(00FF00), blue(0000FF), indigo(FF7F00), violet(9400D3)}
			 */
			public static final Color[] roygbiv2() { if (_roygbiv2 == null) { _roygbiv2 = new Color[] {
				new Color(0xFF0000ff),//red
				new Color(0xFF7F00ff),//orange
				new Color(0xFFFF00ff),//yellow
				new Color(0x00FF00ff),//green
				new Color(0x0000FFff),//blue
				new Color(0xFF7F00ff),//indigo
				new Color(0x9400D3ff),//violet
			};} return _roygbiv2; }
			/**
			 * -- https://www.schemecolor.com/rainbow-colors-for-kids.php
			 * 6: {red(F60000), orange(FF8C00), yellow(FFEE00), green(4DE94C), blue(3783FF), violet(4815AA)}
			 */
			public static final Color[] forKidsRoygbv() { if (_forKidsRoygbv == null) { _forKidsRoygbv = new Color[] {
				new Color(0xF60000ff),//red
				new Color(0xFF8C00ff),//orange
				new Color(0xFFEE00ff),//yellow
				new Color(0x4DE94Cff),//green
				new Color(0x3783FFff),//blue
				new Color(0x4815AAff),//violet
			};} return _forKidsRoygbv; }
			/**
			 * -- https://www.color-hex.com/color-palette/7094
			 * 5: {red(FF1A00), orange(FF8D00), yellow(E3FF00), green(00FF04), blue(0051FF)}
			 */
			public static final Color[] roygb() { if (_roygb == null) { _roygb = new Color[] {
				new Color(0xFF1A00ff),//red
				new Color(0xFF8D00ff),//orange
				new Color(0xE3FF00ff),//yellow
				new Color(0x00FF04ff),//green
				new Color(0x0051FFff),//blue
			};} return _roygb; }
			public static final Color[] Default() { return roygbiv1(); }
			/** repeated colors of {@link Palette#RainbowColors1}: [3, 1, 2, 2, 2, 1, 2] */
			public static final Color[] Repeated() { if (_repeated == null) { _repeated = repeatColors(Default(), new int[] {3, 1, 2, 2, 2, 1, 2});
			}; return _repeated; }
			/** -- https://www.creativefabrica.com/product/rainbow-alternative-color-palette/ */
			public static final Color[] Alternative() { if (_alternative == null) { _alternative = new Color[] {
				new Color(0xBD0311ff), new Color(0xFA0612ff), new Color(0xF34173ff), new Color(0xDF73A4ff),
				new Color(0xE28AA2ff), new Color(0xEF95AFff), new Color(0xFAC8C9ff), new Color(0xFA4A4Cff),
				new Color(0xF56E12ff), new Color(0xE67F20ff), new Color(0xFC8A56ff), new Color(0xFDB078ff),
				new Color(0xF7B70Fff), new Color(0xF0CF64ff), new Color(0xEFE085ff), new Color(0xF0DD99ff),
				new Color(0x50C8C6ff), new Color(0x93CB8Eff), new Color(0x9DD86Aff), new Color(0xB6E3A8ff),
				new Color(0x06549Eff), new Color(0x36A2D1ff), new Color(0x83BFE4ff), new Color(0x93D7DAff),
				new Color(0x7EEBE8ff), new Color(0xD7F2EBff), new Color(0xBC65F2ff), new Color(0xD17DD5ff),
				new Color(0xCD8EADff), new Color(0xC1AFBFff),
			};} return _alternative; }
		}
	}


	public static Color[] repeatColors(Color[] colors, int[] repeatedBy)
	{
		if (colors.length != repeatedBy.length)
		{
			throw new RuntimeException(
				"colors.size(" + colors.length + ") != repeatedBy.size(" + repeatedBy.length + ") // size doesnt fit!"
			);
		}
		int size = 0;
		for(int rep : repeatedBy)
		{
			if (rep > 0)
			{
				size += rep;
			}
		}
		Color[] repeatColors = new Color[size];
		int index = 0;
		for(int i = 0; i < repeatedBy.length; i++)
		{
			int rep = repeatedBy[i];
			while(rep > 0)
			{
				repeatColors[index++] = colors[i];
				rep--;
			}
		}
		return repeatColors;
	}

	public static Color[] removeColors(Color[] colors, Integer... index)
	{
		Set<Integer> indexes = new HashSet<>(Arrays.asList(index));
		List<Color> palette = new ArrayList<>(colors.length - indexes.size());
		for(int i = 0; i < colors.length; i++)
		{
			if (indexes.contains(i))
			{
				continue;
			}
			palette.add(colors[i]);
		}
		return palette.toArray(new Color[0]);
	}
}
