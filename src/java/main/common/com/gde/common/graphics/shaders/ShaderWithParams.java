/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.shaders;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

abstract class ShaderWithParams
extends ShaderProgram
implements IShaderWithParams
{
	private ObjectMap<IShaderParam, Object> params;


	protected ShaderWithParams(FileHandle vertexShader, FileHandle fragmentShader)
	{
		this(vertexShader.readString(), fragmentShader.readString());
	}

	protected ShaderWithParams(String vertexShader, String fragmentShader)
	{
		super(vertexShader, fragmentShader);
		params = new ObjectMap<IShaderParam, Object>();
	}

	@Override
	public Object getParam(IShaderParam param)
	{
		return params.get(param);
	}

	@Override
	public void rebind()
	{
		bind();
		for(Entry<IShaderParam, Object>item : params)
		{
			setParamToShader(
				item.key.getName(),
				item.key.getType(),
				item.value
			);
		}
	}

	protected void updateParam(IShaderParam param, Object value)
	{
		params.put(param, value);
		bind();
		setParamToShader(param.getName(), param.getType(), value);
	}

	protected void setParamToShader(String paramName, Class<?> paramType, Object value)
	{
		int uniformLocation = getUniformLocation(paramName);
		if (paramType == Color.class)
		{
			Color color = (Color)value;
			setUniformf(uniformLocation, new Vector3(color.r, color.g, color.b));
		}
		else
		if (paramType == Integer.class)
		{
			setUniformi(uniformLocation, (Integer)value);
		}
		else
		if (paramType == Float.class)
		{
			setUniformf(uniformLocation, (Float)value);
		}
		else
		if (paramType == Vector2.class)
		{
			setUniformf(uniformLocation, (Vector2)value);
		}
		else
		if (paramType == Vector3.class)
		{
			setUniformf(uniformLocation, (Vector3)value);
		}
		else
		if (paramType == Matrix3.class)
		{
			setUniformMatrix(uniformLocation, (Matrix3)value);
		}
		else
		if (paramType == Matrix4.class)
		{
			setUniformMatrix(uniformLocation, (Matrix4)value);
		}
		else
		{
			throw new RuntimeException(
				"Unsupported paramType(" + paramType.getSimpleName() + ") for value: " + value.toString()
			);
		}
	}
}
