/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pong -- http://gde.tips/games/luzanky/pong
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.display.rainbow;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.gde.common.graphics.display.IDisplayLetter;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class RainbowDisplay
extends MultiLineDisplay
{
	private float colorMoveOnDeltaLimit = -0.5f;
	private float colorMoveOnDeltaRandom = -0.5f;
	private float currentColorTime = 0f;
	private float currentRandomTime = 0f;
	private final ColorPointer[][] currentColorPointers;
	private final int[] currentColorRow;
	private Vector2 letterSize = new Vector2();
	private final Color[] rainbowColors;
	private static final int ROW_ID = 0;


	public RainbowDisplay(
		BitFontMaker2Configuration fontConfiguration, String[] text, Color[] color, Color[] rainbowColors
	)
	{
		super(fontConfiguration, text, color);
		this.rainbowColors = rainbowColors;
		currentColorPointers = createColorPointers(ROW_ID, color[ROW_ID]);
		currentColorRow = createColorRowPointers(currentColorPointers[0].length);
	}

	private int[] createColorRowPointers(int length)
	{
		int colorPointer = 0;
		int[] pointers = new int[length];
		for(int i = 0; i < length; i++)
		{
			pointers[i] = colorPointer;
			colorPointer++;
			if (colorPointer >= rainbowColors.length)
			{
				colorPointer = 0;
			}
		}
		return pointers;
	}

	private ColorPointer[][] createColorPointers(int rowId, Color color)
	{
		int rows = 0;
		int cols = 0;
		ColorPointer[][] pointers;
		for(int i = 0; i < this.rows[rowId].getLength(); i++)
		{
			IDisplayLetter letter = this.rows[rowId].getLetter(i);
			cols += letter.getCols();
			rows += letter.getRows();
		}
		pointers = new ColorPointer[cols][rows];
		for(int col = 0; col < cols; col++)
		{
			for(int row = 0; row < rows; row++)
			{
				pointers[col][row] = new ColorPointer(color);
			}
		}
		return pointers;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		currentColorTime += delta;
		if (currentColorTime > colorMoveOnDeltaLimit)
		{
			currentColorTime = 0f;
			moveOn(ROW_ID);
		}
		currentRandomTime += delta;
		if (currentRandomTime >= colorMoveOnDeltaRandom)
		{
//			colorMoveOnDeltaRandom = RandomUtils.nextFloat(0.05f, 0.059f);
			colorMoveOnDeltaLimit = 0.08f;
		}
	}

	private void moveOn(int rowId)
	{
		for(int i = 0; i < currentColorRow.length; i++)
		{
			currentColorRow[i]++;
			if (currentColorRow[i] >= rainbowColors.length)
			{
				currentColorRow[i] = 0;
			}
			updateColorPointersAtRow(i);
		}

		int letters = rows[rowId].getLength();
		for(int i = 0; i < letters; i++)
		{
			IDisplayLetter letter = rows[rowId].getLetter(i);
			letterSize.set(letter.getCols(), letter.getRows());
			boolean[][] letterUpdates = letter.getUpdatedPixels();

			for(int col = 0; col < letterSize.x; col++)
			{
				for(int row = 0; row < letterSize.y; row++)
				{
					if (!letterUpdates[row][col])
					{
						continue;
					}
					ColorPointer pointer = currentColorPointers[col][row];
					letter.updateColor(
						row,
						col,
						(pointer.colorIndex < 0
							? pointer.color
							: rainbowColors[pointer.colorIndex]
						)
					);
				}
			}
		}
	}

	/** whole row-word! */
	private void updateColorPointers()
	{
		int maxCols = currentColorPointers.length;
		int maxRows = currentColorPointers[0].length;
		for(int row = 0; row < maxRows; row++)
		{
			for(int col = 0; col < maxCols; col++)
			{
				ColorPointer pointer = currentColorPointers[col][row];
				pointer.colorIndex++;
				if (pointer.colorIndex >= rainbowColors.length)
				{
					pointer.colorIndex = 0;
				}
			}
		}
	}

	/** only 1x specific row! */
	private void updateColorPointersAtRow(int rowId)
	{
		int maxCols = currentColorPointers.length;
		int maxRows = currentColorPointers[0].length;
		for(int row = 0; row < maxRows; row++)
		{
			if (rowId != row)
			{
				continue;
			}
			for(int col = 0; col < maxCols; col++)
			{
				currentColorPointers[col][row].colorIndex = currentColorRow[row];
			}
		}
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		viewport.x = getX();
		viewport.y = getY();
		for(int i = 0; i < rows.length; i++)
		{
			rows[i].setViewport(viewport);
		}
	}

	private class ColorPointer
	{
		public Color color;
		public int colorIndex;

		public ColorPointer(Color color)
		{
			this.colorIndex = -1;
			this.color = color;
		}
	}
	// TODO: RainbowLetter
}
