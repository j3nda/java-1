/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.shaders;

import com.badlogic.gdx.Gdx;
import com.gde.common.resources.CommonResources;

public class PixelateShader
extends ShaderWithParams
{
	public enum ShaderParam implements IShaderParam
	{
		ViewportWidth("viewport_w"),
		ViewportHeight("viewport_h"),
		PixelateWidth("pixel_w"),
		PixelateHeight("pixel_h"),
		;
		private ShaderParam(String glslParamName)
		{
			this(glslParamName, Float.class);
		}
		private ShaderParam(String glslParamName, Class<?> classType)
		{
			this.glslParamName = glslParamName;
			this.glslParamType = classType;
		}
		@Override
		public String getName()
		{
			return glslParamName;
		}
		@Override
		public Class<?> getType()
		{
			return glslParamType;
		}
		private String glslParamName;
		private Class<?> glslParamType;
	}


	public PixelateShader(int pixelate, int viewportWidth, int viewportHeight)
	{
		this(pixelate, pixelate, viewportWidth, viewportHeight);
	}

	public PixelateShader(int pixelateWidth, int pixelateHeight, int viewportWidth, int viewportHeight)
	{
		super(
			Gdx.files.internal(CommonResources.Shader.pixelate[0]).readString(),
			Gdx.files.internal(CommonResources.Shader.pixelate[1]).readString()
		);
		updatePixelate(pixelateWidth, pixelateHeight);
		updateViewport(viewportWidth, viewportHeight);
	}

	public void updatePixelate(int pixelate)
	{
		updatePixelate(pixelate, pixelate);
	}

	public void updatePixelate(int pixelateWidth, int pixelateHeight)
	{
		updateParam(ShaderParam.PixelateWidth,  (float)pixelateWidth);
		updateParam(ShaderParam.PixelateHeight, (float)pixelateHeight);
	}

	public void updateViewport(int viewportWidth, int viewportHeight)
	{
		updateParam(ShaderParam.ViewportWidth,  (float)viewportWidth);
		updateParam(ShaderParam.ViewportHeight, (float)viewportHeight);
	}
}
