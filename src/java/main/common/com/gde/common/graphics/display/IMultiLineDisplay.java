/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;

public interface IMultiLineDisplay
extends MultiLineDisplayListener
{
	void addListener(MultiLineDisplayListener listener);
	void removeListener(MultiLineDisplayListener listener);

	/* from DisplayRowLetters */
	void setViewport(Rectangle viewport);

	/* from Actor */
	void act(float delta);
	void draw(Batch batch, float parentAlpha);
	void addAction(Action action);
	void clearActions();
	public boolean hasActions();
}
