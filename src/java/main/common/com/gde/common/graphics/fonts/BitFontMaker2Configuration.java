/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;


/**
 * configuration of bitmap font.
 * any .ttf font is possible to transfer into .json,
 * by: @see: https://www.pentacom.jp/pentacom/bitfontmaker2/
 */
public class BitFontMaker2Configuration
{
	/**
	 * map: characters(~ascii code) x (their binary implementation)
	 * - key: asciiCode
	 * - value: Array(Integer) counted from left(2^0; 2^1; ...; 2^15)
	 * ! it's natively 16x16
	 */
	private ObjectMap<Integer, int[]> chars;
	private final boolean debug;
	public int width = 16;
	public int height = 16;
	/** (left, top, right, bottom) which pixels are drawn. just don't waste space! */
	public Rectangle draw;


	public BitFontMaker2Configuration(String filename)
	{
		this(false, filename);
	}

	public BitFontMaker2Configuration(boolean debug, String filename)
	{
		this.debug = debug;
		JsonValue fromJson = new JsonReader().parse(
			Gdx.files.internal(filename).readString()
		);
		chars = new ObjectMap<Integer, int[]>();
		draw = new Rectangle(0, 0, width, height);
		for(int i = 0; i < fromJson.size; i++)
		{
			JsonValue entry = fromJson.get(i);
			createChar(entry);
			updateParams(entry);
		}
	}

	private void createChar(JsonValue entry)
	{
		try
		{
			int asciiCode = Integer.valueOf(entry.name);
			if (asciiCode >= ' ' && asciiCode <= 'z')
			{
				chars.put(asciiCode, entry.asIntArray());
			}
		}
		catch(NumberFormatException e)
		{
			if (debug)
			{
				System.out.println("debug: bitmapFont: key \"" + entry.name + "\" neni ascii-code, PRESKAKUJI!");
			}
		}
	}

	private void updateParams(JsonValue entry)
	{
		if (entry.name.equals("size"))
		{
			int[] draw = entry.asIntArray();
			if (draw != null)
			{
				if (draw.length > 0) { this.draw.x = draw[0]; }
				if (draw.length > 1) { this.draw.y = draw[1]; }
				if (draw.length > 2) { this.draw.width = draw[2]; }
				if (draw.length > 3) { this.draw.height = draw[3]; }
			}
		}
	}

	public int[] getChar(int asciiCode)
	{
		return chars.get(asciiCode);
	}
}
