/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.ui.items.progress.horizontal;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.gde.common.graphics.ui.items.progress.IProgressBar;
import com.gde.common.graphics.ui.items.progress.ProgressActor;


public class ProgressBar
extends ProgressActor
implements IProgressBar
{
	protected NinePatch increasing;
	protected Color increasingColor;
	protected NinePatch decreasing;
	protected Color decreasingColor;


	@Override
	public void setIncreasingNinePatch(NinePatch increasing)
	{
		this.increasing = increasing;
	}

	@Override
	public void setIncreasingTintColor(Color increasingColor)
	{
		this.increasingColor = increasingColor;
	}

	@Override
	public void setDecreasingNinePatch(NinePatch decreasing)
	{
		this.decreasing = decreasing;
	}

	@Override
	public void setDecreasingTintColor(Color decreasingColor)
	{
		this.decreasingColor = decreasingColor;
	}

	@Override
	protected void render(Batch batch, float alpha)
	{
		int width = (int)(getWidth() * getPercentage());
		if (increasing != null)
		{
			renderIncreaingBar(batch, alpha, width);
		}
		if (decreasing != null)
		{
			renderDecreasingBar(batch, alpha, width);
		}
	}

	protected void renderIncreaingBar(Batch batch, float alpha, int width)
	{
		if (increasingColor == null)
		{
			increasing.draw(batch, getX(), getY(), width, getHeight());
		}
		else
		{
			Color prevColor = batch.getColor().cpy();
			batch.setColor(increasingColor);
			increasing.draw(batch, getX(), getY(), width, getHeight());
			batch.setColor(prevColor);
		}
	}

	protected void renderDecreasingBar(Batch batch, float alpha, int width)
	{
		if (decreasingColor == null)
		{
			decreasing.draw(batch, getX() + width, getY(), getWidth() - width, getHeight());
		}
		else
		{
			Color prevColor = batch.getColor().cpy();
			batch.setColor(decreasingColor);
			decreasing.draw(batch, getX() + width, getY(), getWidth() - width, getHeight());
			batch.setColor(prevColor);
		}
	}

	@Override
	protected void percentageChanged(float previous, float current)
	{

	}

	@Override
	public float getWidth()
	{
		return super.getWidth() * getScaleX();
	}

	@Override
	public float getHeight()
	{
		return super.getHeight() * getScaleY();
	}
}
