/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public interface IAnimationActor
{
	Animation<TextureRegionDrawable> getAnimation();
	void reset();
	boolean isAnimationFinished();

	/** libGdx/animation */
	TextureRegionDrawable getKeyFrame(float stateTime, boolean looping);
	TextureRegionDrawable getKeyFrame(float stateTime);
	int getKeyFrameIndex(float stateTime);
	TextureRegionDrawable[] getKeyFrames();
	PlayMode getPlayMode();
	void setPlayMode(PlayMode playMode);
	boolean isAnimationFinished(float stateTime);
	void setFrameDuration(float frameDuration);
	float getFrameDuration();
	float getAnimationDuration();
	void setListener(IAnimationActorListener listener);

	/** libGdx/actor */
	void act(float delta);
	void draw(Batch batch, float parentAlpha);
	void setPosition(float x, float y);
	void setColor(Color color);
	void setOrigin(int align);
	float getWidth();
	float getScaleX();
}
