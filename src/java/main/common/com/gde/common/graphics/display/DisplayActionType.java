/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.display;

/** sdruzuje typy akci {@link Action} nad displayem */
public class DisplayActionType
{
	/** typ akce: pouze pro dany 'Actor' */
	public static final int Actor = 1;
	/** typ akce: pouze pro vsechny pixely v displayi! */
	public static final int Deep = 2;
	/** typ akce: pouze pro pozadi (~background) */
	public static final int Background = 4;
	/** typ akce: pouze pro popredi (~foreground), tj. vse rozdilne nez pozadi! */
	public static final int Foreground = 8;
	/** typ akce: pouze pro popredi (~foreground), ktere odpovida urcite barve! DATA: Color */
	public static final int ForegroundColor = 16;
	/** typ akce: pouze pro zvolene radky; DATA: int[] */
	public static final int Rows = 32;
	/** typ akce: pouze pro zvolene sloupce; DATA: int[] */
	public static final int Cols = 64;
	/** typ akce: pouze pro zvolene pozice [col, row]; DATA: Point[] */
	public static final int PosXY = 128;
}
