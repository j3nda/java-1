/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.vectors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.common.graphics.vectors.VectorDebug.CrossInfo;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorGroup;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorPoint;

import space.earlygrey.shapedrawer.JoinType;
import space.earlygrey.shapedrawer.ShapeDrawer;

public class VectorPointsDrawable
extends Actor
{
	static final boolean isDebug = VectorGroupDrawable.isDebug;
	private static final CrossInfo debugCross = new CrossInfo(Color.CYAN, 1, 10);
	private final VectorGroup group;
	private float unitScale;
	private final ShapeDrawer drawer;
	private final float[] vertices;

	public VectorPointsDrawable(ShapeDrawer drawer, VectorGroup group)
	{
		this.drawer = drawer;
		this.group = group;
		this.vertices = new float[group.points.size() * 2];

//			com.badlogic.gdx.math.EarClippingTriangulator#computeTriangles(float[])

	}

	VectorGroup getGroup()
	{
		return group;
	}

	void setUnitScale(float unitScale)
	{
		this.unitScale = unitScale;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		boolean isDrawing = batch.isDrawing();
		if (!isDrawing)
		{
			batch.begin();
		}

		float x = getX();
		float y = getY();

		if (group.points == null)
		{
			return;
		}

		if (group.isPolygon || group.isFilled)
		{
			if (group.isFilled)
			{
				calculateVertices(x, y); // TODO: xhonza: performance shlitz!
				drawer.filledPolygon(vertices);
			}
			else
			{
				calculateVertices(x, y); // TODO: xhonza: performance shlitz!
				drawer.polygon(
					vertices,
					group.lineSize,
					JoinType.NONE
				);
			}
		}
		else
		{
			calculateVertices(x, y); // TODO: xhonza: performance shlitz!
			for(int i = 2; i < vertices.length; i = i + 2)
			{
				float x1 = vertices[i - 2];
				float y1 = vertices[i - 1];
				float x2 = vertices[i];
				float y2 = vertices[i + 1];
				drawer.line(x1, y1, x2, y2, Color.MAROON, 2); //group.lineSize);
			}
//
//			for(VectorPoint point : group.points)
//			{
//				drawer.filledRectangle(
//					x + (point.x * unitScale),
//					y + (point.y * unitScale),
//					10,
//					10,
//					Color.YELLOW
//				);
//			}

//			drawer.get
//			drawer.setColor(Color.YELLOW);
		}

		if (isDebug)
		{
			for(VectorPoint point : group.points)
			{
				VectorDebug.cross(
					drawer,
					debugCross,
					x + (point.x * unitScale),
					y + (point.y * unitScale)
				);
			}
		}

		if (!isDrawing)
		{
			batch.end();
		}
	}

	private void calculateVertices(float x, float y)
	{
		int index = 0;
		for(VectorPoint point : group.points)
		{
			vertices[index++] = x + (point.x * unitScale);
			vertices[index++] = y + (point.y * unitScale);
		}
	}
}
