/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.display;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.gde.common.graphics.display.IDisplayPixelArray.RenderMode;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

/** zobrazi vice radku nad sebou */
public class MultiLineDisplay
extends VerticalGroup
implements IMultiLineDisplay
{
	protected IDisplayRowLetters[] rows;
	protected final Rectangle viewport;
	private final List<MultiLineDisplayListener> listeners;


	public MultiLineDisplay(
		BitFontMaker2Configuration fontConfiguration,
		String[] text,
		Color color
	)
	{
		this(fontConfiguration, text, createColors(text.length, color));
	}

	protected static Color[] createColors(int count, Color color)
	{
		Color[] colors = new Color[count];
		Arrays.fill(colors, color);
		return colors;
	}

	public MultiLineDisplay(
		BitFontMaker2Configuration fontConfiguration,
		String[] text,
		Color[] color
	)
	{
		this(fontConfiguration, text, color, Color.BLACK, RenderMode.NoBackground);
	}

	public MultiLineDisplay(
		BitFontMaker2Configuration fontConfiguration,
		String[] text,
		Color[] color,
		Color bgColor,
		RenderMode renderMode
	)
	{
		listeners = new ArrayList<>();
		viewport = new Rectangle();
		rows = new DisplayRowLetters[text.length];
		for(int i = 0; i < text.length; i++)
		{
			final int index = i;
			final Actor actor = new DisplayRowLetters(
				fontConfiguration,
				text[i].length(),
				color[i],
				bgColor
			);
			actor.setName(text[i]);
			addActor(actor);

			rows[i] = (IDisplayRowLetters) actor;
			rows[i].setRenderMode(renderMode);
			rows[i].addListener(new ActorGestureListener()
			{
				@Override
				public void tap(InputEvent event, float x, float y, int count, int button)
				{
					onTapRowDisplay(index, (IDisplayRowLetters)actor);
				}
			});
			for(int j = 0; j < text[i].length(); j++)
			{
				rows[i].updateLetter(j, text[i].charAt(j));
			}
		}
	}

	@Override
	public void setViewport(Rectangle viewport)
	{
		this.viewport.set(viewport);
		float height = viewport.height / rows.length;
		Rectangle rect = new Rectangle(
			viewport.x,
			viewport.y,
			viewport.width,
			height
		);
		for(int i = 0; i < rows.length; i++)
		{
			rect.y = (viewport.height - (height * (i + 1))) + viewport.y;
			rows[i].setViewport(rect);
		}
		setPosition(viewport.x, viewport.y);
		setSize(viewport.width, viewport.height);
	}

	@Override
	public void addAction(Action action)
	{
		super.addAction(action);
	}


	@Override
	public void draw(Batch batch, float parentAlpha)
	{
//		super.draw(); // TODO: xhonza: performance: shlitz! idk why, but this is not rendering members of group!@#$%^&*() WTF!
		validate();
		for(int i = 0; i < rows.length; i++)
		{
			rows[i].draw(batch, parentAlpha);
		}
	}

	@Override
	public void addListener(MultiLineDisplayListener listener)
	{
		listeners.add(listener);
	}

	@Override
	public void removeListener(MultiLineDisplayListener listener)
	{
		listeners.remove(listener);
	}

	@Override
	public void onTapRowDisplay(int index, IDisplayRowLetters display)
	{
		for(MultiLineDisplayListener listener : listeners)
		{
			listener.onTapRowDisplay(index, display);
		}
	}
}
