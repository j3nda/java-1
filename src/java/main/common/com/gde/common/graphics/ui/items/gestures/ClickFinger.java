/*******************************************************************************
 * Copyright 2022 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.ui.items.gestures;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.graphics.ui.IAnimationActor;

public class ClickFinger
extends Image
{
	protected final IAnimationActor clickAnimation;
	protected final Vector2 handOffset;
	protected final Vector2 clickOffset;
	private volatile int clickCounter = 0;
	protected float handAlpha;
	protected float clickAlpha;

	public ClickFinger(
		float duration,
		Texture handTexture, Vector2 handOffset,
		TextureRegion[] clickFrames, Vector2 clickOffset
	)
	{
		this(
			duration,
			new TextureRegion(handTexture), handOffset,
			clickFrames, clickOffset
		);
	}

	public ClickFinger(
		float duration,
		TextureRegion handTexture, Vector2 handOffset,
		TextureRegion[] clickFrames, Vector2 clickOffset
	)
	{
		super(handTexture);
		this.handOffset = handOffset;
		this.clickOffset = clickOffset;
		setOrigin(Align.center);
		clickAnimation = new AnimationActor(duration, clickFrames, PlayMode.NORMAL);
		clickAnimation.setOrigin(Align.center);
	}

	public synchronized void reset()
	{
		clickCounter = 0;
		clickAnimation.reset();
		clearActions();
	}

	public void click()
	{
		click(1);
	}

	public synchronized void click(int repeat)
	{
		reset();
		setVisible(true);
		clickCounter = Math.abs(repeat);
	}

	public void setColor(Color handColor, Color clickColor)
	{
		setColor(handColor);
		clickAnimation.setColor(clickColor);
		clickAlpha = clickColor.a;
	}

	@Override
	public void setColor(Color color)
	{
		super.setColor(color);
		handAlpha = color.a;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (clickCounter > 0)
		{
			clickAnimation.draw(batch, parentAlpha);
		}
		super.draw(batch, parentAlpha);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		if (clickCounter > 0)
		{
			clickAnimation.act(delta);
			if (clickAnimation.isAnimationFinished())
			{
				synchronized (this)
				{
					clickCounter--;
				}
				onClick(clickCounter);
			}
		}
	}

	@Override
	protected void positionChanged()
	{
		super.positionChanged();
		clickAnimation.setPosition(
			getX() + handOffset.x - clickOffset.x,
			getY() + handOffset.y - clickOffset.y
		);
	}

	protected void onClick(int counter)
	{
		if (clickCounter > 0)
		{
			clickAnimation.reset();
		}
	}
}
