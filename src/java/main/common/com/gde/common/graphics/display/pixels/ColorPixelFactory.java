/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.display.pixels;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.gde.common.resources.CommonResources;

class ColorPixelFactory
implements IColorPixelFactory
{
	/** performance: cacheovani vytvareni textur podle typu */
	private static final Map<ColorPixelType, Texture> textures = new HashMap<>(ColorPixelType.values().length);
	/** singleton ~ design-pattern */
	private static IColorPixelFactory instance;


	public static IColorPixelFactory getInstance()
	{
		if (instance == null)
		{
			instance = new ColorPixelFactory();
		}
		return instance;
	}

	@Override
	public Texture createTexture(ColorPixelType type)
	{
		Texture texture = textures.get(type);
		if (texture != null)
		{
			return texture;
		}
		switch(type)
		{
			case tintPixmap:
			{
				texture = createTextureFromPixmap(16, 16, Color.WHITE);
				break;
			}
			case tintTextureFile:
			{
				texture = new Texture(CommonResources.Tint.square16x16);
				break;
			}
			default:
				throw new RuntimeException("Invalid textureType!");
		}
		textures.put(type, texture);
		return texture;
	}

	private Texture createTextureFromPixmap(int width, int height, Color color)
	{
		Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
		pixmap.setColor(color);
		pixmap.fillRectangle(0, 0, width, height);
		Texture texture = new Texture(pixmap);
		pixmap.dispose();
		return texture;
	}

	@Override
	public void disposeTexture(ColorPixelType type)
	{
		Texture texture = textures.get(type);
		if (texture != null)
		{
			texture.dispose();
			texture = null;
		}
		textures.put(type, texture);
	}

	@Override
	public void dispose()
	{
		for(ColorPixelType type : ColorPixelType.values())
		{
			Texture texture = textures.get(type);
			if (texture != null)
			{
				texture.dispose();
				texture = null;
			}
			textures.put(type, texture);
		}
	}
}
