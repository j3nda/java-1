/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2019 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gde.common.game.GdeGame;
import com.gde.common.game.IGdeGame;

public abstract class ScreenBase<
	TScreenType extends Enum<TScreenType>,
	TScreenResources extends IScreenResources,
	ParentGame extends IGdeGame<TScreenType, TScreenResources>
>
implements Screen, IScreen<TScreenType>
{
	private boolean paused;
	private boolean visible;
	protected final ParentGame parentGame;
	protected final TScreenResources resources;
	private final TScreenType previousScreenType;
	private Sprite background;
	private final Stage stage;


	public ScreenBase(ParentGame parentGame, TScreenType previousScreenType)
	{
		this.parentGame = parentGame;
		this.previousScreenType = previousScreenType;
		this.resources = parentGame.getScreenResources();
		this.paused = false;
		this.stage = createStage(resources);
	}

	protected Stage createStage(IScreenResources resources)
	{
		return new Stage(
			resources.getViewport(),
			resources.getBatch()
		);
	}

	protected boolean isPaused()
	{
		return paused;
	}

	protected boolean isVisible()
	{
		return visible;
	}

	@Override
	public void pause()
	{
		synchronized(this)
		{
			paused = true;
		}
	}

	@Override
	public void resume()
	{
		synchronized(this)
		{
			paused = false;
		}
	}

	@Override
	public void show()
	{
		if (resources == null)
		{
			throw new IllegalArgumentException(
				getClass().getName() + " is missing " + IScreenResources.class.getName() + " dependency!"
			);
		}
		getBatch().setProjectionMatrix(resources.getCamera().combined);
		getStage().clear();

		// TODO: GUI resit dalsi stage!
//		if(screenTable == null)
//		{
//			screenTable = createFullScreenTable();
//		}
//
//		screenTable.clear();
//		screenStage.addActor(screenTable);

		// TODO: refactor: getStage() it must return List<Stage> ~ now it has limitation only to 1x Stage! which isnt enough due UI f.e.
		setInputProcessor(getStage());

		visible = true;
	}

	@Override
	public void hide()
	{
		visible = false;
	}

	@Override
	public void resize(int width, int height)
	{
		if (getWidth() == width && getHeight() == height)
		{
			return;
		}
		if (getStage() != null)
		{
			getStage().getViewport().setScreenSize(width, height);
			getStage().getViewport().update(width, height, true);
		}
		else
		{
			resources.getViewport().setScreenSize(width, height);
			resources.getViewport().update(width, height, true);
		}
	}

	@Override
	public void dispose()
	{
		getStage().dispose();
		resources.dispose();
	}

	@Override
	public void render(float delta)
	{
		// pokud nejsem videt - nic nekreslim, je to zbytecne...
		// navrhovy vzor: Fail/Silent (~A fail-silent system is a type of system that either provides
		//   the correct service, or provides no service at all (becomes silent))
		// @see: https://en.wikipedia.org/wiki/Fail-fast
		if (!isVisible())
		{
			return;
		}

		// zobrazeni grafiky
		beforeRenderScreen(delta);
		renderScreen(delta);
		afterRenderScreen(delta);
	}

	protected Color getClearColor()
	{
		return new Color(0x000000FF); // RGBA x (0, 0, 0, 1)
	}

	protected void beforeRenderScreen(float delta)
	{
		// zjisteni barvy v "RGBA" pro vyplneni celeho okna nastavenou barvou
		Color clearColor = getClearColor();
		if (clearColor != null)
		{
			Gdx.gl.glClearColor(
				clearColor.r,
				clearColor.g,
				clearColor.b,
				clearColor.a
			);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		}

		// aktualizace kamery, ktera snima scenu
		resources.getCamera().update();

		// nastaveni metriky/pozice kamery pro vykresleni sceny ve spravnych souradnicich do okna
		getBatch().setProjectionMatrix(resources.getCamera().combined);
		getStage().getViewport().apply(true);

	}

	protected abstract void renderScreen(float delta);

	protected void renderBackground()
	{
		if (background != null)
		{
			getStage().getBatch().begin();
			background.draw(getStage().getBatch());
			getStage().getBatch().end();
		}
	}

	protected void renderScreenStage(float delta)
	{
		// vykonani logiky, pokud neni 'zapauzovana'
		if (!isPaused())
		{
			getStage().act(delta);
		}

		// zobrazeni grafiky
		getStage().draw();
	}

	protected void afterRenderScreen(float delta)
	{

	}

	/**
	 * return screen's width (~alias: getViewport().getScreenWidth());
	 */
	@Override
	public int getWidth()
	{
		Stage stage = getStage();
		if (stage != null)
		{
			return stage.getViewport().getScreenWidth();
		}
		return resources.getViewport().getScreenWidth();
	}

	/**
	 * return screen's width (~alias: getViewport().getScreenHeight());
	 */
	@Override
	public int getHeight()
	{
		Stage stage = getStage();
		if (stage != null)
		{
			return stage.getViewport().getScreenHeight();
		}
		return resources.getViewport().getScreenHeight();
	}

	protected Batch getBatch()
	{
		return getStage().getBatch();
	}

	protected Stage getStage()
	{
		return stage;
	}

	@Override
	public void setInputProcessor(InputProcessor... inputProcessor)
	{
		List<InputProcessor> inputs = new ArrayList<>(Arrays.asList(inputProcessor));
		InputMultiplexer multiplex = new InputMultiplexer();

		createInputProcessors(inputs);
		for(InputProcessor current : inputs)
		{
			addInputProcessorToInputMultiplexer(current, multiplex);
		}
		Gdx.input.setCatchKey(Input.Keys.BACK, true);
		Gdx.input.setInputProcessor(multiplex);
	}

	private void addInputProcessorToInputMultiplexer(InputProcessor inputProcessor, InputMultiplexer inputMultiplexer)
	{
		if (inputProcessor instanceof InputMultiplexer)
		{
			for(InputProcessor current : ((InputMultiplexer)inputProcessor).getProcessors())
			{
				Gdx.app.debug(
					getClass().getName(),
					"addInputProcessor: expandInputMultiplexer("
						+ ((InputMultiplexer)inputProcessor).getProcessors().size
						+ ")"
					);
				addInputProcessorToInputMultiplexer(current, inputMultiplexer);
			}
		}
		else
		if (
			   inputProcessor != null
			&& !inputMultiplexer.getProcessors().contains(inputProcessor, true)
			)
		{
			Gdx.app.debug(getClass().getName(), "addInputProcessor: " + inputProcessor.getClass().getName());
			inputMultiplexer.addProcessor(inputProcessor);
		}
	}

	/** create and/or re-order input-processors */
	protected void createInputProcessors(List<InputProcessor> inputs)
	{
		inputs.add(new BackButtonInputProcessor());
	}

	public class BackButtonInputProcessor
	extends InputAdapter
	{
		@Override
		public boolean keyDown(int keycode)
		{
	        switch (keycode)
	        {
				case Keys.ESCAPE:
				case Keys.BACK:
				{
					onBackButtonPressed();
					return true;
				}
				default:
					return false;
	        }
		}
	}

	protected void onBackButtonPressed()
	{
		if (getPreviousScreen() == null)
		{
			Gdx.app.error("ScreenBase", "No previous screen!");
			parentGame.dispose();
			GdeGame.exit(GdeGame.ReturnCodes.ERROR_NO_PREVIOUS_SCREEN);
		}
		else
		{
			setPreviousScreen();
		}
	}

	protected TScreenType getPreviousScreen()
	{
		return previousScreenType;
	}

	protected void setPreviousScreen()
	{
		TScreenType prevScreen = getPreviousScreen();
		if (prevScreen == null)
		{
			Gdx.app.debug("ScreenBase", "Unable to setPreviousScreen(); previousScreen is null!");
			return;
		}
		parentGame.setScreen(parentGame.getScreen(prevScreen));
	}

	@Override
	public void setBackground(Sprite background)
	{
		background.setSize(getWidth(), getHeight());
		this.background = background;
		this.background.getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	}
}
