/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.display;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.scenes.scene2d.actions.ActionsUtils;
import com.gde.common.graphics.display.IDisplayPixelArray.RenderMode;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;


/** zobrazi 1x slovo (~nekolik pismen) na 1x radku */
class DisplayRowLetters
extends Actor // TODO: XHONZA: extends HorizontalGroup!
implements IDisplayRowLetters
{
	private final DisplayLetter[] displayLetters;
	private final int length;
	private Color fgColor;
	private Color bgColor;
	private Action updateLetterGfxBefore;
	private Action updateLetterGfxAfter;


	public DisplayRowLetters(BitFontMaker2Configuration fontConfiguration, int wordLength)
	{
		this(fontConfiguration, wordLength, Color.WHITE, Color.BLACK);
	}

	public DisplayRowLetters(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor,
		Color bgColor
	)
	{
		this.displayLetters = new DisplayLetter[length];
		this.fgColor = fgColor;
		this.bgColor = bgColor;
		this.length = length;
		clearGfxActions();
		createDisplay(
			fontConfiguration,
			length,
			(bgColor == null ? RenderMode.NoBackground : RenderMode.Normal)
		);
		setViewport(
			new Rectangle(
				0,
				0,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
			)
		);
	}

	@Override
	public IDisplayLetter getLetter(int index)
	{
		return displayLetters[index];
	}

	@Override
	public void addAction(Action action)
	{
		super.addAction(action);
		for(DisplayLetter letter : displayLetters)
		{
			letter.addAction(
				ActionsUtils.newInstance(action)
			);
		}
	}

	@Override
	public void clearActions()
	{
		super.clearActions();
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].clearActions();
		}
	}

	@Override
	public boolean hasActions()
	{
		int actions = 0;
		if (super.hasActions())
		{
			actions += super.getActions().size;
		}
		for(DisplayLetter letter : displayLetters)
		{
			if (letter.hasActions())
			{
				actions++;
			}
		}
		return (actions > 0 ? true : false);
	}

	/** nastavi pozici a velikost displaye (a prepocte vsechny dulezite veci) */
	@Override
	public void setViewport(Rectangle viewport)
	{
		float letterX = 0;
		float letterWidth = viewport.width / displayLetters.length;
		float rowHeight = viewport.height;

		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].setViewport(
				new Rectangle(
					viewport.x + letterX,
					viewport.y,
					letterWidth,
					rowHeight
				)
			);
			letterX += letterWidth;
		}

		setWidth(viewport.width);
		setHeight(viewport.height);
		setPosition(viewport.x, viewport.y);
	}

	private void createDisplay(BitFontMaker2Configuration fontConfiguration, int length, RenderMode renderMode)
	{
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i] = new DisplayLetter(fontConfiguration);
			displayLetters[i].setRenderMode(renderMode);
		}
	}

	/** zaktualizuje pismeno na dane pozici na pozadovane pismeno (~zadane ascii kodem) */
	@Override
	public void updateLetter(int index, int asciiCode)
	{
		updateLetter(index, asciiCode, fgColor, bgColor);
	}

	@Override
	public void updateLetter(int index, int asciiCode, Color fgColor)
	{
		updateLetter(index, asciiCode, fgColor, bgColor);
	}

	@Override
	public void updateLetter(int index, final int asciiCode, final Color fgColor, final Color bgColor)
	{
		if (index < 0 || index >= displayLetters.length)
		{
			return;
		}
		final DisplayLetter singleLetter = displayLetters[index];
		if (updateLetterGfxBefore == null && updateLetterGfxAfter == null)
		{
			singleLetter.update(fgColor, asciiCode, bgColor);
		}
		else
		{
			SequenceAction actions = Actions.sequence();
			if (updateLetterGfxBefore != null)
			{
				actions.addAction(updateLetterGfxBefore);
			}
			actions.addAction(Actions.run(
				new Runnable()
				{
					@Override
					public void run()
					{
						singleLetter.update(fgColor, asciiCode, bgColor);
					}
				}
			));
			if (updateLetterGfxAfter != null)
			{
				actions.addAction(updateLetterGfxAfter);
			}
			if (actions.getActions().size > 1)
			{
				singleLetter.addAction(actions);
			}
		}
	}

	@Override
	public void setGfxActionBefore(Action action)
	{
		updateLetterGfxBefore = action;
	}

	@Override
	public void setGfxActionAfter(Action action)
	{
		updateLetterGfxAfter = action;
	}

	@Override
	public void clearGfxActions()
	{
		updateLetterGfxBefore = null;
		updateLetterGfxAfter = null;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].act(delta);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].draw(batch, parentAlpha);
		}
	}

	@Override
	public void dispose()
	{
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].dispose();
		}
	}

	@Override
	public void setRenderMode(RenderMode renderMode)
	{
		for(int i = 0; i < displayLetters.length; i++)
		{
			displayLetters[i].setRenderMode(renderMode);
		}
	}

	@Override
	public int getLength()
	{
		return length;
	}
}
