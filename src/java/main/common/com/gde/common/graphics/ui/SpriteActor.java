/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.common.utils.GraphicUtils;

public class SpriteActor
extends Actor
implements ISpriteActor
{
	protected final Sprite actorSprite;
	protected final NinePatch actorNinePatch;


	public SpriteActor(Sprite actorSprite)
	{
		this.actorNinePatch = null;
		this.actorSprite = actorSprite;
		super.setSize(actorSprite.getWidth(), actorSprite.getHeight());
	}

	public SpriteActor(NinePatch actorNinePatch)
	{
		this.actorSprite = null;
		this.actorNinePatch = actorNinePatch;
		super.setSize(
			actorNinePatch.getTexture().getWidth(),
			actorNinePatch.getTexture().getHeight()
		);
	}

	public SpriteActor(TextureRegion textureRegion)
	{
		this(new Sprite(textureRegion));
		textureRegion.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (isNinePatch())
		{
			drawNinePatch(batch, parentAlpha);
		}
		else
		{
			drawSprite(batch, parentAlpha);
		}
	}

	protected void drawNinePatch(Batch batch, float parentAlpha)
	{
		Color tintColor = actorNinePatch.getColor();
		tintColor.a *= parentAlpha;

		Color prevColor = GraphicUtils.setColor(batch, tintColor);
		actorNinePatch.draw(
			batch,
			getX(),
			getY(),
			getOriginX(),
			getOriginY(),
			getWidth(),
			getHeight(),
			getScaleX(),
			getScaleY(),
			getRotation()
		);
		batch.setColor(prevColor);
	}

	protected void drawSprite(Batch batch, float parentAlpha)
	{
		Color tintColor = actorSprite.getColor();
		tintColor.a *= parentAlpha;

		Color prevColor = GraphicUtils.setColor(batch, tintColor);
		batch.draw(
			actorSprite,
			getX(),
			getY(),
			getOriginX(),
			getOriginY(),
			getWidth(),
			getHeight(),
			getScaleX(),
			getScaleY(),
			getRotation()
		);
		batch.setColor(prevColor);
	}

	@Override

	protected void positionChanged()
	{
		if (!isNinePatch())
		{
			actorSprite.setPosition(getX(), getY());
		}
	}

	@Override
	public void setRotation(float degrees)
	{
		super.setRotation(degrees);
		if (!isNinePatch())
		{
			actorSprite.setRotation(degrees);
		}
	}

	@Override
	public Sprite getSprite()
	{
		return actorSprite;
	}

	@Override
	public NinePatch getSpriteAsNinePatch()
	{
		return actorNinePatch;
	}

	@Override
	public void setSize(float width, float height)
	{
		super.setSize(width, height);
		if (!isNinePatch())
		{
			actorSprite.setSize(width, height);
		}
	}

	protected boolean isNinePatch()
	{
		return actorNinePatch != null;
	}

	@Override
	public void setColor(Color color)
	{
		if (isNinePatch())
		{
			actorNinePatch.setColor(color);
		}
		else
		{
			actorSprite.setColor(color);
		}
	}
}
