/*******************************************************************************
 * Copyright 2019 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.ui.items.progress;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;


public abstract class ProgressActor
extends Actor
implements IProgressActor
{
	private float percentage = 0;


	protected abstract void percentageChanged(float previous, float current);
	protected abstract void render(Batch batch, float parentAlpha);


	@Override
	public void updateValues(float current, float max)
	{
		float prev = percentage;
		percentage = current / max;
		if ((int)(prev * 100) != (int)(percentage * 100))
		{
			percentageChanged(prev, percentage);
		}
	}

	@Override
	public void updatePercentage(float percent)
	{
		if (percent < 0 || percent > 1)
		{
			return;
		}
		if ((int)(percent * 100) != (int)(percentage * 100))
		{
			percentageChanged(percentage, percent);
		}
		percentage = percent;
	}

	@Override
	public float getPercentage()
	{
		return percentage;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		boolean isBatch = batch.isDrawing();
		if (!isBatch)
		{
			batch.begin();
		}
		render(batch, parentAlpha);
		if (!isBatch)
		{
			batch.end();
		}
	}
}
