/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

class DisplayLetter
extends DisplayPixelArray
implements IDisplayLetter
{
	private BitFontMaker2Configuration font;


	public DisplayLetter(BitFontMaker2Configuration font)
	{
		super(
			(int)font.draw.height,
			(int)font.draw.width
		);
		this.font = font;
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano ascii kodem) */
	public void update(Color fgColor, int asciiCode)
	{
		update(fgColor, asciiCode, Color.BLACK);
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano ascii kodem) vcetne zmeny pozadi */
	public void update(Color fgColor, int asciiCode, Color bgColor)
	{
		clearDisplay(bgColor);
		updateForeground(fgColor, font.getChar(asciiCode));
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano stringem) */
	public void update(Color fgColor, String letter)
	{
		if (letter == null || letter.isEmpty())
		{
			return;
		}
		update(fgColor, letter.charAt(0));
	}

	/** provede aktualizaci zobrazovaci mrizky a zobrazi pismeno (zadano stringem) vcetne zmeny pozadi */
	public void update(Color fgColor, String letter, Color bgColor)
	{
		if (letter == null || letter.isEmpty())
		{
			return;
		}
		update(fgColor, letter.charAt(0), bgColor);
	}

	/** provede aktualizaci zobrazovaci mrizky tak, ze vstupem je barva a binary-encoded-char 16x16! */
	private void updateForeground(Color color, int[] charInBinary)
	{
		if (charInBinary == null || charInBinary.length != font.width)
		{
			return;
		}
		for(int fontRow = 0; fontRow < font.height; fontRow++)
		{
			for(int fontCol = 0; fontCol < font.width; fontCol++)
			{
				if (
					   fontRow >= font.draw.y && fontRow <= font.draw.y + font.draw.height
					&& fontCol >= font.draw.x && fontCol <= font.draw.x + font.draw.width
				   )
				{
					int binaryValue = charInBinary[fontRow];
					if (((binaryValue >> fontCol) & 1) == 1)
					{
						updateColor(
							getRows() - 1 - (fontRow - (int)font.draw.y), // TODO: BUGA! ArrayOutOfBoundException - 1, eg: // 11 - 1 == 10 - (fontRow:13) - (font.draw.y:2) == 11 => 10 - 11 == -1
//							getRows() - (fontRow - (int)font.draw.y), // TODO: BUGA! ArrayOutOfBoundException - 1

							// TODO: tyto 2x radky - upravuji paddingTop vs Bottom => zkus: NEXT LEVEL vs pojmenovane

							fontCol - (int)font.draw.x, // 3 - 2 = 1
							color
						);
					}
				}
			}
		}
	}
}
