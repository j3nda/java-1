/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.vectors;

import java.util.ArrayList;
import java.util.Objects;

import com.badlogic.gdx.graphics.Color;

public class VectorGroupConfiguration
{
	/** unit to multiply all VectorPoints and origins */
	public float unitScale;
	/** origin vector from [x,y] */
	public VectorPoint origin;
	public ArrayList<VectorGroup> groups;

	public static class VectorGroup
	{
		/** used as part of json */
		public String parentName;
		/** used internaly to speed-up foreach */
		public VectorGroup parentGroup;

		public int index;
		public String name;
		public Color color;
		public int lineSize;
		public boolean isFilled;
		public boolean isPolygon;
		// TODO: xhonza: isLinkedToParent?
		public VectorPoint origin;
		public ArrayList<VectorPoint> points;

		@Override
		public int hashCode()
		{
			return Objects.hash(
				parentName, parentGroup,
				index, color, name, color, lineSize, isFilled, isPolygon, origin, points
			);
		}
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			VectorGroup other = (VectorGroup) obj;
			return
				   Objects.equals(parentName, other.parentName)
				&& Objects.equals(parentGroup, other.parentGroup)
				&& index == other.index
				&& Objects.equals(name, other.name)
				&& Objects.equals(color, other.color)
				&& lineSize == other.lineSize
				&& isFilled == other.isFilled
				&& isPolygon == other.isPolygon
				&& Objects.equals(origin, other.origin)
				&& Objects.equals(points, other.points)
			;
		}
	}

	public static class VectorPoint
	{
		public float x;
		public float y;

		@Override
		public int hashCode()
		{
			return Objects.hash(x, y);
		}
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			VectorPoint other = (VectorPoint) obj;
			return
				   Float.floatToIntBits(x) == Float.floatToIntBits(other.x)
				&& Float.floatToIntBits(y) == Float.floatToIntBits(other.y)
			;
		}
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(unitScale, origin, groups);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		VectorGroupConfiguration other = (VectorGroupConfiguration) obj;
		return
			   Float.floatToIntBits(unitScale) == Float.floatToIntBits(other.unitScale)
			&& Objects.equals(origin, other.origin)
			&& Objects.equals(groups, other.groups)
		;
	}
}
