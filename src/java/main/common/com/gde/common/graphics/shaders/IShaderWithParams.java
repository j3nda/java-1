/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2021 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.shaders;

/**
 * HOWTO: <br/>
 * -- Tutorial: <br/>
 * -- -- http://www.gamefromscratch.com/post/2014/07/08/LibGDX-Tutorial-Part-12-Using-GLSL-Shaders-and-creating-a-Mesh.aspx <br/>
 * -- -- https://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson4 <br/>
 * <br/>
 * -- Reference: <br/>
 * -- -- https://github.com/libgdx/libgdx/wiki/Shaders <br/>
 * -- --  http://www.shaderific.com/glsl-qualifiers/ <br/>
 * -- --  http://stackoverflow.com/questions/13780609/what-does-precision-mediump-float-mean <br/>
 * -- --  http://stackoverflow.com/questions/5366416/in-opengl-es-2-0-glsl-where-do-you-need-precision-specifiers <br/>
 * <br/>
 * -- Examples: <br/>
 * -- -- http://www.sovapps.com/play-with-shaders-black-and-white-part-1/ <br/>
 * -- -- http://stackoverflow.com/questions/26835347/no-uniform-with-name-u-proj-in-shader <br/>
 * -- -- http://www.glbasic.com/forum/index.php?topic=8025.0 <br/>
 * <br/>
 * -- Notes: <br/>
 * -- -- On systems that DO support highp, you will see a performance hit, and should use mediump and lowp wherever possible. A good rule of thumb that I saw was: <br/>
 * -- -- -   highp for vertex  positions, <br/>
 * -- -- - mediump for texture coordinates, <br/>
 * -- -- -    lowp for colors. <br/>
 * <br/>
 * -- LibGdx usage: <br/>
 * -- -- ShaderProgram.pedantic = false; // toto NENI RESENI - NIC SE NERENDERUJE!!! <br/>
 * -- -- <br/>
 * -- -- shader's setting: from app/system -> gfx-glsl-shader <br/>
 * -- -- - shader.setUniformf(shader.getUniformLocation("particlesize"), 0); //pSystem.getParticleRadius()); <br/>
 * -- -- - shader.setUniformf(shader.getUniformLocation("scale"), getScaleX()); <br/>
 * -- -- - shader.setUniformMatrix(shader.getUniformLocation("u_projTrans"), pProjMatrix); <br/>
 * -- -- - shader.setUniformMatrix("u_projTrans", camera.combined); <br/>
 * -- -- - shader.setUniformf("n", n); // n ~ is like deltaTimeBullshit! <br/>
 * -- -- <br/>
 * -- -- batch.setShader(new ShaderProgram(..., ...)); // set shader <br/>
 * -- -- batch.setShader(null); // Call this method with a null argument to use the default shader. [@source: LibGdx documentation / forum] <br/>
 * <br/>
 * -- PERFORMANCE: (on Android!) <br/>
 * -- -- Your problem is in your fragment shader in default-2d-tex.shader. You have uniform vec4 ucolor; That means, that each of your color's components (RGBA) will be converted to 32-bits float value. And this drops performance heavily. Should be: uniform lowp vec4 ucolor; <br/>
 * -- -- ================ <br/>
 * -- -- In my experience mobile GPU performance is roughly proportional to the number of texture2D calls. You have 21 which really is a lot. Generally memory lookups are on the order of hundreds of times slower than calculations, so you can do a lot of calculation and still be bottlenecked on the texture lookups. (This also means optimising the rest of your code probably will have little effect, since it means instead of being busy while it waits for the texture lookups, it will be idle while it waits for the texture lookups.) So you need to reduce the number of texture2Ds you call. <br/>
 * -- -- <br/>
 * -- -- It's difficult to say how to do this since I don't really understand your shader, but some ideas: <br/>
 * -- -- -- o) separate it in to a horizontal pass then a vertical pass. This only works for some shaders e.g. blurs, but it can seriously reduce the number of texture lookups. For example a 5x5 gaussian blur naively does 25 texture lookups; if done horizontally then vertically, it only uses 10. <br/>
 * -- -- -- o) use linear filtering to 'cheat'. If you sample exactly between 4 pixels instead of the middle of 1 pixel with linear filtering enabled, you get the average of all 4 pixels for free. However I don't know how it affects your shader. In the blur example again, using linear filtering to sample two pixels at once either side of the middle pixel allows you to sample 5 pixels with 3 texture2D calls, reducing the 5x5 blur to just 6 samples for both horizontal and vertical. <br/>
 * -- -- -- o) just use a smaller kernel (so you don't take so many samples). This affects the quality, so you'd probably want some way to detect the device performance and switch to a lower quality shader when the device appears to be slow. <br/>
 * -- -- ================= <br/>
 * -- -- http://forums.androidcentral.com/android-4-4-kitkat/434155-shaders-performance.html <br/>
 * -- <br/>
 * -- -- Absolutely. Try Monjori Shader Benchmark, it will give you a rough idea about the GPU's performance:
 * -- -- -- https://play.google.com/store/apps/details?id=de.swagner.monjori <br/>
 * -- -- -- demoscene: <br/>
 * -- -- -- -- https://play.google.com/store/apps/details?id=com.kewlers.nvscene14 <br/>
 * -- -- -- -- https://play.google.com/store/apps/details?id=com.kewlers.spongiform <br/>
 * -- -- -- NVIDIA Tegra K1 Benchmark Performance Review (Xiaomi Mi Pad) <br/>
 * -- -- -- -- https://www.youtube.com/watch?v=bUJuYhySSzw <br/>
 * -- -- =================
 * -- -- -- http://stackoverflow.com/questions/9209700/opengl-es-2-0-multiple-programs-or-multiple-shaders-or-what-how-does-it-work <br/>
 * <br/>
 * -- Links: <br/>
 * -- -- https://github.com/molp/libgdx-shaders <br/>
 * -- -- https://www.youtube.com/watch?v=gCR_ikOdaUc (~shaderToy + libGdx)<br/>
 */
public interface IShaderWithParams
{
	public Object getParam(IShaderParam param);

	/**
	 * Regenerates and/or rebinds owned resources when needed, eg. when the OpenGL context is lost.
	 * (Concrete objects shall be responsible to recreate or rebind its own resources whenever its needed,
	 *  usually when the OpenGL context is lost. Eg., framebuffer textures should be updated
	 *  and shader parameters should be reuploaded/rebound)
	 */
	public void rebind();
}
