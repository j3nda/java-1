/*******************************************************************************
 * Copyright 2013 Straitjacket Entertainment
 * Copyright 2022 gde.tips
 * -- http://straitjacket-entertainment.com
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Planet Gula -- http://gde.tips/games/sjet/planet-gula
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 ******************************************************************************/
package com.gde.common.graphics.ui;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class AnimationActor
extends Image
implements IAnimationActor
{
	private final Animation<TextureRegionDrawable> animation;
	private float currentTime;
	private float updateFrameTime;
	private IAnimationActorListener listener;

	public AnimationActor(float duration, TextureRegion[] frames)
	{
		this(duration, frames, PlayMode.NORMAL);
	}

	public AnimationActor(float duration, TextureRegion[] frames, PlayMode playMode)
	{
		super(frames[0]);
		TextureRegionDrawable[] drawables = new TextureRegionDrawable[frames.length];
		for(int i = 0; i < frames.length; i++)
		{
			drawables[i] = new TextureRegionDrawable(new TextureRegion(frames[i]));
		}
		animation = new Animation<TextureRegionDrawable>(duration, drawables);
		animation.setFrameDuration(duration);
		animation.setPlayMode(playMode);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		currentTime += delta;
		if (isAnimationFinished())
		{
			onAnimationFinished();
		}
		updateFrameTime += delta;
		if (updateFrameTime >= animation.getFrameDuration())
		{
			setDrawable(
				animation.getKeyFrame(currentTime)
			);
		}
	}

	@Override
	public void reset()
	{
		currentTime = 0;
	}

	@Override
	public Animation<TextureRegionDrawable> getAnimation()
	{
		return animation;
	}

	@Override
	public TextureRegionDrawable getKeyFrame(float stateTime, boolean looping)
	{
		return animation.getKeyFrame(stateTime, looping);
	}

	@Override
	public TextureRegionDrawable getKeyFrame(float stateTime)
	{
		return animation.getKeyFrame(stateTime);
	}

	@Override
	public int getKeyFrameIndex(float stateTime)
	{
		return animation.getKeyFrameIndex(stateTime);
	}

	@Override
	public TextureRegionDrawable[] getKeyFrames()
	{
		return animation.getKeyFrames();
	}

	@Override
	public PlayMode getPlayMode()
	{
		return animation.getPlayMode();
	}

	@Override
	public void setPlayMode(PlayMode playMode)
	{
		animation.setPlayMode(playMode);
	}

	@Override
	public boolean isAnimationFinished()
	{
		return animation.isAnimationFinished(currentTime);
	}

	@Override
	public boolean isAnimationFinished(float stateTime)
	{
		return animation.isAnimationFinished(stateTime);
	}

	@Override
	public void setFrameDuration(float frameDuration)
	{
		animation.setFrameDuration(frameDuration);
	}

	@Override
	public float getFrameDuration()
	{
		return animation.getAnimationDuration();
	}

	@Override
	public float getAnimationDuration()
	{
		return animation.getAnimationDuration();
	}

	@Override
	public void setListener(IAnimationActorListener listener)
	{
		this.listener = listener;
	}

	private void onAnimationFinished()
	{
		if (listener == null)
		{
			return;
		}
		listener.onAnimationFinished(this);
	}
}
