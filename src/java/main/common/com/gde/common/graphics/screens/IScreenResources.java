/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.screens;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;

public interface IScreenResources
{
	Camera getCamera();
	Viewport getViewport();
	Batch getBatch();

	/** LibGdx */
	void dispose();
}
