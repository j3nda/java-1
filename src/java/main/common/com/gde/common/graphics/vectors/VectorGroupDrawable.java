/*******************************************************************************
 * Copyright 2023 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Box -- http://gde.tips/games/luzanky/box
 ******************************************************************************/
package com.gde.common.graphics.vectors;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gde.common.graphics.vectors.VectorDebug.CrossInfo;
import com.gde.common.graphics.vectors.VectorGroupConfiguration.VectorGroup;

import space.earlygrey.shapedrawer.ShapeDrawer;

public class VectorGroupDrawable
extends Actor
{
	static final boolean isDebug = true;
	private static int id = 0;

	/** immutable {@link VectorGroupConfiguration} */
	private VectorGroupConfiguration config;
	private final ShapeDrawer drawer;
	private CrossInfo debugOrigin;
	private final List<Integer> drawOrder;
	private final List<VectorPointsDrawable> drawActors;// TODO: xhonza: change it to 'Group' to respect group inside group as isLinkedToParent?
	private float unitScale;

	public VectorGroupDrawable(ShapeDrawer drawer)
	{
		this(drawer, null);
	}

	public VectorGroupDrawable(ShapeDrawer drawer, VectorGroupConfiguration config)
	{
		this.id = id++; // TODO: xhonza: used as prefix to actor.setName(id+groupName)
		this.drawer = drawer;
		this.drawOrder = new ArrayList<>();
		this.drawActors = new ArrayList<>();
		if (isDebug)
		{
			debugOrigin = new CrossInfo(0, 0, Color.RED, 23, 1);
		}
		setConfig(config);
	}

	public void setConfig(VectorGroupConfiguration config)
	{
		boolean isConfigChanged = !(
			   (this.config == null && config == null)
			|| (this.config != null && this.config.equals(config))
			|| (config != null && config.equals(this.config))
		);
		this.config = config;
		if (isConfigChanged)
		{
			configChanged();
		}
	}

	/** called when {@link VectorGroupConfiguration} changed */
	protected void configChanged()
	{
		drawOrder.clear();
		if (config != null)
		{
			drawOrder.addAll(
				createDrawOrder(
					config.groups,
					null
				)
			);
			createDrawActors(drawOrder);
			normalizeParent(config.groups);
			setUnitScale(config.unitScale);

			setUnitScale(100f); // TODO: xhonza! config.unitScale! ~100f
		}
	}

	void setUnitScale(float unitScale)
	{
		this.unitScale = unitScale;
		for(VectorPointsDrawable actor : drawActors)
		{
			actor.setUnitScale(unitScale);
		}
	}

	private void createDrawActors(List<Integer> drawOrder)
	{
		// TODO: xhonza: nereflektuje to spravne grupu v grupe, abych mel 'TODO: xhonza: isLinkedToParent?' (~zatim to seru)
		drawActors.clear();
		for(int index : drawOrder)
		{
			VectorGroup group = config.groups.get(index);

			VectorPointsDrawable actor = new VectorPointsDrawable(drawer, group);
			actor.setName(group.name);

			drawActors.add(actor);
		}
	}

	private void normalizeParent(List<VectorGroup> groups)
	{
		if (groups == null)
		{
			return;
		}
		for(VectorGroup group : config.groups)
		{
			if (group.parentName == null)
			{
				continue;
			}
			group.parentGroup = findByName(group.parentName);
		}
	}

	private VectorGroup findByName(String name)
	{
		for(VectorGroup group : config.groups)
		{
			if (name.equals(group.name))
			{
				return group;
			}
		}
		return null;
	}

	private List<Integer> createDrawOrder(List<VectorGroup> groups, String parentName)
	{
		if (groups == null)
		{
			return new ArrayList<>(0);
		}
		List<Integer> order = new ArrayList<>();
		for (int i = 0; i < groups.size(); i++)
		{
			VectorGroup group = groups.get(i);
			if (
				   (parentName == null && (group.parentName == null | group.parentName.length() == 0)) // no-parent!
				|| (parentName != null && parentName.equals(group.parentName))                         // has-parent!
			   )
			{
				order.add(i);
				List<Integer> innerOrder = createDrawOrder(groups, group.name);
				order.addAll(innerOrder);
				// TODO: xhonza: sortBy VectorGroup.index
				// TODO: xhonza: setName() to group-actions!

// TODO: xhonza: this is bullshit! nereflektuje to spravne grupu v grupe, abych mel 'TODO: xhonza: isLinkedToParent?'
//				if (innerOrder.isEmpty())
//				{
//					Actor actor = new VectorPointsDrawable(group);
//					actor.setName(group.name);
//
//					addActor(actor);
//				}
//				else
//				{
//					Group innerGroup = new Group();
//					Actor actor = new VectorPointsDrawable(group);
//					actor.setName(group.name);
//
//					innerGroup.addActor(actor);
//					for(int index : innerOrder)
//					{
//						VectorGroup vectorGroup = groups.get(index);
//						actor = new VectorPointsDrawable(vectorGroup);
//						actor.setName(vectorGroup.name);
//
//						innerGroup.addActor(actor);
//					}
//				}
			}
		}
		return order;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		float x = getX();
		float y = getY();

		drawOrigin(batch, parentAlpha, x, y);
		for(int index : drawOrder)
		{
			VectorGroup group = config.groups.get(index);

			// return vector as origin for current group
			Vector2 groupOrigin = getGroupOrigin(group);

			drawGroup(
				batch,
				parentAlpha,
				drawActors.get(index),
				x + (groupOrigin.x * unitScale),
				y + (groupOrigin.y * unitScale)
			);


//[1]left-3.1	> []
//[1]left-3.2	> []
//[1]left-2.1	> [3, 4]
//[1]left-3.3	> []
//[1]left-3.4	> []
//[1]left-2.2	> [5, 6]
//[0]left-1		> [1, 3, 4, 2, 5, 6]
//[2]right-3.1	> []
//[1]right-2.1	> [10]
//[2]right-3.2	> []
//[1]right-2.2	> [11]
//[0]right-1	> [8, 10, 9, 11]

//			VectorDebug.line(
//				drawer,
//				x,
//				y,
//				x + (groupOrigin.x * us),
//				y + (groupOrigin.y * us)
//			);
			if (isDebug)
			{
				VectorDebug.group(
					drawer,
					group,
					x + (groupOrigin.x * unitScale),
					y + (groupOrigin.y * unitScale),
					unitScale,
					debugOrigin
				);
				System.out.println(""
					+ group.name + "[" + group.origin.x + "," + group.origin.y + "]"
					+ ", origin: " + groupOrigin
					+ ", points: " + group.points
				);

//				System.out.println();
//				for(Actor actor : getChildren())
//				{
//					System.out.println(actor.getName());
//				}
//				System.out.println();
			}
		}
	}

	private void drawOrigin(Batch batch, float parentAlpha, float x, float y)
	{
		if (!isDebug)
		{
			return;
		}
		VectorDebug.cross(drawer, debugOrigin, x, y);
	}

	private void drawGroup(Batch batch, float parentAlpha, VectorPointsDrawable actor, float x, float y)
	{

		// TODO: xhonza: group.validate() instead to do it in each render-frame!
		actor.setPosition(x, y);
		actor.draw(batch, parentAlpha);

		if (!isDebug)
		{
			return;
		}
		VectorDebug.cross(
			drawer,
			debugOrigin,
			x,
			y
		);
	}

	private final Vector2 tmpGroupOrigin = new Vector2();
	private Vector2 getGroupOrigin(VectorGroup group)
	{
		tmpGroupOrigin.set(0, 0);
		VectorGroup tmpGroup = group;
		boolean hasParent;
		do
		{
			hasParent = (tmpGroup.parentGroup != null);
			tmpGroupOrigin.add(
				tmpGroup.origin.x,
				tmpGroup.origin.y
			);
			if (hasParent)
			{
				tmpGroup = tmpGroup.parentGroup;
			}
		}
		while(hasParent);

		return tmpGroupOrigin;
	}

	@Override
	protected void positionChanged()
	{
		System.out.println(
			"positionChanged():"
			+ " [" + getX() + "," + getY() + "]"
			+ " TODO: xhonza: this is bullshit!"
		);
		// TODO: xhonza: this is bullshit!
//		config.origin.x = getX();
//		config.origin.y = getY();
//		if (isDebug)
//		{
//			debugOrigin.x = config.origin.x;
//			debugOrigin.y = config.origin.y;
//			System.out.println("origin={" + config.origin + "}");
//		}
	}
}
//   +---+
//   |   |
//   +---+
//         ++
//    +-+  ++
// ++ | |
// ++ | |
//    | |
//    +-+
//
//     |
//   origin
