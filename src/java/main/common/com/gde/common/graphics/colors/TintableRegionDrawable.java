/*******************************************************************************
 * Copyright 2022 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Pexeso -- http://gde.tips/games/luzanky/pexeso
 * - Lode -- http://gde.tips/games/luzanky/lode
 ******************************************************************************/
package com.gde.common.graphics.colors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public class TintableRegionDrawable
extends TextureRegionDrawable
{
	private static final Color tmpColor = new Color();
	private final Color tintColor = Color.WHITE.cpy();


	public TintableRegionDrawable(Color color)
	{
		super(createTexture(color));
	}

	private static TextureRegion createTexture(Color color)
	{
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(color);
		pixmap.fill();

		TextureRegion texture = new TextureRegion(
			new Texture(pixmap)
		);
		pixmap.dispose();

		return texture;
	}

	public TintableRegionDrawable(TextureRegion texture)
	{
		super(texture);
	}

	public TintableRegionDrawable(TextureRegion texture, Color color)
	{
		super(texture);
		setColor(color);
	}

	public void setColor(Color color)
	{
		this.tintColor.set(color);
	}

	@Override
	public void draw(Batch batch, float x, float y, float width, float height)
	{
		Color tintColor = tmpColor.set(batch.getColor()).mul(this.tintColor);
		Color prevColor = batch.getColor();

		batch.setColor(tintColor);
		super.draw(batch, x, y, width, height);
		batch.setColor(prevColor);
	}

	@Override
	public void draw(
		Batch batch,
		float x, float y,
		float originX, float originY,
		float width, float height,
		float scaleX, float scaleY,
		float rotation
	)
	{
		Color tintColor = tmpColor.set(batch.getColor()).mul(this.tintColor);
		Color prevColor = batch.getColor();

		batch.setColor(tintColor);
		super.draw(batch, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
		batch.setColor(prevColor);
	}
}
