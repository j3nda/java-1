/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.common.graphics.effects;

import java.util.Random;

import com.badlogic.gdx.math.Vector3;


public class ShakeEffect
{
	private float duration;
	private float currentTime;
	private float power;
	private float currentPower;
	private Random random;
	private Vector3 pos = new Vector3();

	public ShakeEffect()
	{
		this(0, 0);
	}

	public ShakeEffect(float shakePower, float shakeDuration)
	{
		random = new Random();
		reset(shakePower, shakeDuration);
	}

	public void reset(float shakePower, float shakeDuration)
	{
		power = shakePower;
		duration = shakeDuration;
		currentTime = 0;
		currentPower = 0;
	}

	public Vector3 act(float delta)
	{
		if (currentTime <= duration)
		{
			currentPower = power * ((duration - currentTime) / duration);

			pos.x = (random.nextFloat() - 0.5f) * 2 * currentPower;
			pos.y = (random.nextFloat() - 0.5f) * 2 * currentPower;

			currentTime += delta;
		}
		else
		if (duration > 0)
		{
			duration = 0;
			pos = Vector3.Zero;
		}
		return pos;
	}

	/** return duration left for  rest of effect */
	public float getDurationLeft()
	{
		return duration;
	}

	/** return [x,y] position to move something by this effect */
	public Vector3 getShake()
	{
		return pos;
	}
}
