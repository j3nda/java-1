package com.gde.common.graphics.ui;

public interface IAnimationActorListener
{
	void onAnimationFinished(IAnimationActor animation);
}
