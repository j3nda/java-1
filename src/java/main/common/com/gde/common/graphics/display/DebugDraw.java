/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class DebugDraw
{
	private static final ShapeRenderer sr = new ShapeRenderer();
	private static boolean isBatchDrawing;


	public static void line(Color color, Vector2 a, Vector2 b)
	{
		sr.begin(ShapeType.Line);
		sr.setColor(color);
		sr.line(a, b);
		sr.end();
	}

	public static void point(Color color, Vector2 pos)
	{
		point(color, pos, 1);
	}

	public static void point(Color color, Vector2 pos, int size)
	{
		sr.begin(ShapeType.Filled);
		sr.setColor(color);
		sr.rect(
			pos.x,
			pos.y,
			size,
			size,
			color, // bottom-left
			color, // bottom-right
			color, // top-righ
			color  // top-left
		);
		sr.end();
	}

	public static void rectangle(Color color, Rectangle rectangle)
	{
		rectangle(color, rectangle, 1);
	}

	public static void rectangle(Color color, Rectangle rectangle, int size)
	{
		if (size < 2)
		{
			sr.begin(ShapeType.Line);
			sr.setColor(color);
			sr.rect(
				rectangle.x,
				rectangle.y,
				rectangle.width,
				rectangle.height
			);
		}
		else
		{
			sr.begin(ShapeType.Filled);
			sr.setColor(color);
			int halfSize = size / 2;
			sr.rect( // left
				rectangle.x - halfSize,
				rectangle.y - halfSize,
				size,
				rectangle.height + size
			);
			sr.rect( // right
				rectangle.x - halfSize + rectangle.width,
				rectangle.y - halfSize,
				size,
				rectangle.height + size
			);
			sr.rect( // top
				rectangle.x - halfSize,
				rectangle.y - halfSize + rectangle.height,
				rectangle.width + size,
				size
			);
			sr.rect( // bottom
				rectangle.x - halfSize,
				rectangle.y - halfSize,
				rectangle.width + size,
				size
			);
		}
		sr.end();
	}

	public static ShapeRenderer sr()
	{
		return sr;
	}

	public static void begin(Batch batch)
	{
		isBatchDrawing = batch.isDrawing();
		if (isBatchDrawing)
		{
			batch.end();
		}
	}

	public static void end(Batch batch)
	{
		if (isBatchDrawing)
		{
			batch.begin();
		}
	}
}
