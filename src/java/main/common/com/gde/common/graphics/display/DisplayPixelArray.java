/*******************************************************************************
 * Copyright 2019 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Hangman -- http://gde.tips/games/luzanky/obesenec
 ******************************************************************************/
package com.gde.common.graphics.display;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.badlogic.scenes.scene2d.actions.ActionsUtils;
import com.gde.common.graphics.display.pixels.ColorPixel;
import com.gde.common.utils.Point;


/**
 * Zobrazovaci display pro presny pocet: radek x sloupec.
 * Kdy velikost jednoho "pixelu" se dopocte na zaklade realne velikosti displaye.
 * Priklad:
 * - okno je velke(1024x768) a display bude v leve polovine, tj.
 *   velikost-displaye(512x768) pixelu,
 *   pri pozadovanem-poctu(radek=8, sloupec=16),
 *
 *   potom: 1x pixel bude: sirka=64pixel, vyska=48pixel
 */
class DisplayPixelArray
extends Actor
implements IDisplayPixelArray
{
	/** pro rychle debugovani grafiky... */
	private static boolean debug = false;
	/** dvoj-rozmernne pole "barevnych pixelu" [radky][sloupce], @see: {@link ColorPixel} */
	private final ColorPixel[][] colorPixels;
	/** pozadi displaye (~vhodne, kdyz se animuji pixely - tak nechci videt skrz, tj. vykreslim background) */
	private final ColorPixel background;
	/** pocet (sloupcu, radku), ulozeno jako Vector2.(x, y) */
	private final Vector2 size;
	/** velikost 1x bodu/pixelu v ramci displaye */
	private final Vector2 pixelSize;
	/** velikost displaye vcetne jeho pozice */
	private final Rectangle viewport;
	/** dynamicky generovana textura pro 1x barevny bod. performance killer */
	private Pixmap pixmap;
	/** true/false hodnota nad pixely, ktere zmenily barvu a nebylo to pozadi */
	private final boolean[][] updatedPixels;
	/** mod vykreslovani */
	private RenderMode renderMode;


	public DisplayPixelArray(int maxRows, int maxCols)
	{
		this.colorPixels = new ColorPixel[maxRows][maxCols];
		this.size = new Vector2(maxCols, maxRows);
		this.pixelSize = new Vector2(1, 1);
		this.viewport = new Rectangle(0, 0, 0, 0);
		this.updatedPixels = new boolean[maxRows][maxCols];
		this.renderMode = RenderMode.Normal;
		setViewport(new Rectangle(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		this.background = new ColorPixel((int)viewport.width, (int)viewport.height, Color.BLACK);
	}

	@Override
	public void addAction(Action action)
	{
		addAction(
			action,
			DisplayActionType.Actor | DisplayActionType.Deep
		);
	}

	@Override
	public void addAction(Action action, int actionTypeFlags)
	{
		addAction(action, actionTypeFlags, null);
	}

	@Override
	public void addAction(Action action, int actionTypeFlags, Object flagsData)
	{
		if ((actionTypeFlags & DisplayActionType.Actor) != 0)
		{
			super.addAction(action);
		}
		if ((actionTypeFlags & DisplayActionType.Background) != 0)
		{
			background.addAction(action);
		}
		if (
			   (actionTypeFlags & DisplayActionType.Deep) != 0
			|| (actionTypeFlags & DisplayActionType.Foreground) != 0
			|| (actionTypeFlags & DisplayActionType.ForegroundColor) != 0
			|| (actionTypeFlags & DisplayActionType.Rows) != 0
			|| (actionTypeFlags & DisplayActionType.Cols) != 0
			|| (actionTypeFlags & DisplayActionType.PosXY) != 0
		   )
		{
			addActionEach(action, actionTypeFlags, flagsData);
		}
	}

	private void addActionEach(Action action, int actionTypeFlags, Object flagsData)
	{
		boolean checkDeep = ((actionTypeFlags & DisplayActionType.Deep) != 0);
		boolean checkColor = false;
		boolean checkRows = ((actionTypeFlags & DisplayActionType.Rows) != 0);
		boolean checkCols = ((actionTypeFlags & DisplayActionType.Cols) != 0);
		boolean checkPos = ((actionTypeFlags & DisplayActionType.PosXY) != 0);

		Color onlyColor = null;
		int[] onlyRows = null;
		int[] onlyCols = null;
		Point[] onlyPos = null;

		if ((actionTypeFlags & DisplayActionType.Foreground) != 0 && background != null)
		{
			onlyColor = background.getColor();
			checkColor = true;
		}
		if ((actionTypeFlags & DisplayActionType.ForegroundColor) != 0)
		{
			onlyColor = (Color)flagsData;
			checkColor = true;
		}
		if (checkRows)
		{
			onlyRows = (int[])flagsData;
		}
		if (checkCols)
		{
			onlyCols = (int[])flagsData;
		}
		if (checkPos)
		{
			onlyPos = (Point[])flagsData;
		}

		for(int row = 0; row < getRows(); row++)
		{
			if (checkRows && !addActionAllowed(onlyRows, row))
			{
				continue;
			}
			for(int col = 0; col < getCols(); col++)
			{
				if (
					   (checkCols && !addActionAllowed(onlyCols, col))
					|| (checkPos  && !addActionAllowed(onlyPos, col, row))
				   )
				{
					continue;
				}
				ColorPixel pixel = colorPixels[row][col];
				if (pixel != null)
				{
					boolean addActionColor = (
						   checkColor
						&& onlyColor != null
						&& (
							   pixel.getColor().r != onlyColor.r
							|| pixel.getColor().g != onlyColor.g
							|| pixel.getColor().b != onlyColor.b
						)
					);
					if (checkDeep || addActionColor || checkRows || checkCols || checkPos)
					{
						pixel.addAction(
							ActionsUtils.newInstance(action)
						);
					}
				}
			}
		}
	}

	private boolean addActionAllowed(int[] values, int current)
	{
		if (values == null)
		{
			return false;
		}
		for(int entry : values)
		{
			if (entry == current)
			{
				return true;
			}
		}
		return false;
	}

	private boolean addActionAllowed(Point[] values, int x, int y)
	{
		if (values == null)
		{
			return false;
		}
		for(Point entry : values)
		{
			if (entry.getX() == x && entry.getY() == y)
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void clearActions()
	{
		super.clearActions();
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel != null)
				{
					pixel.clearActions();
				}
			}
		}
	}

	@Override
	public boolean hasActions()
	{
		int actions = 0;
		if (super.hasActions())
		{
			actions += super.getActions().size;
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel != null && pixel.hasActions())
				{
					actions++;
				}
			}
		}
		return (actions > 0 ? true : false);
	}

	/** nastavi pozici a velikost displaye (a prepocte vsechny dulezite veci) */
	@Override
	public void setViewport(Rectangle viewport)
	{
		if (background != null)
		{	// uplne hnusne osetreny stav: konstruktor vola setViewport() a az potom se vytvari background objekt!
			background.setPosition(viewport.x, viewport.y);
			background.setWidth(viewport.width);
			background.setHeight(viewport.height);
			background.setOrigin(Align.center);
		}

		pixelSize.x = viewport.width  / size.x;
		pixelSize.y = viewport.height / size.y;
		this.viewport.set(viewport);
		pixmap = new Pixmap(
			(int)pixelSize.x,
			(int)pixelSize.y,
			Pixmap.Format.RGBA8888
		);
		recreateColorPixels(pixmap, pixelSize, colorPixels);
		setPosition(viewport.x, viewport.y);
		setWidth(viewport.width);
		setHeight(viewport.height);
		setOrigin(Align.center);

		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				pixel.setPosition(
					viewport.x + (col * pixelSize.x),
					viewport.y + (row * pixelSize.y)
				);
			}
		}
		if (debug)
		{
			System.out.println(
				getClass().getSimpleName() + ".setViewport(): "
				+ "size: " + size
				+ ", viewport: " + viewport
				+ ", pixelSize: " + pixelSize
			);
		}
	}

	/** znovu-vytvori {@link ColorPixel} jako pole (sloupcu, radku), vcetne barvy */
	private void recreateColorPixels(Pixmap pixmap, Vector2 pixelSize, ColorPixel[][] colorPixels)
	{
//		boolean hasActions = hasActions(); // TODO: hasActions
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				ColorPixel pixel = colorPixels[row][col];
				if (pixel == null)
				{	// pokud pixel neexistuje, vytvor ho...
					pixel = colorPixels[row][col] = new ColorPixel(
						(int)pixelSize.x + 1, // +1 is correction for crazy gaps between cells!@#$%^&()
						(int)pixelSize.y + 1, // TODO: u can fix it, but i will waste a quite time here...
						Color.BLACK
					);
					// TODO: hasActions
//					if (hasActions)
//					{
//						for(Action action : getActions())
//						{
//							colorPixels[row][col].addAction(
//								ActionsUtils.newInstance(action)
//							);
//						}
//					}
				}
				else
				if (pixelSize.x != pixel.getWidth() || pixelSize.y != pixel.getHeight())
				{	// pokud se zmenila velikost 1x pixelu, uprav velikost
					pixel.setWidth(pixelSize.x);
					pixel.setHeight(pixelSize.y);
					pixel.setOrigin(Align.center);
				}
			}
		}
	}

	/** vrati pocet radku displaye */
	@Override
	public int getRows()
	{
		return (int) size.y;
	}

	/** vrati pocet sloupcu displaye */
	@Override
	public int getCols()
	{
		return (int) size.x;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				colorPixels[row][col].act(delta);
			}
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (renderMode != RenderMode.NoBackground && background != null)
		{
			background.draw(batch, parentAlpha);
		}
		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				if (!updatedPixels[row][col])
				{
					continue;
				}
				ColorPixel pixel = colorPixels[row][col];
				pixel.draw(batch, parentAlpha);
				if (debug)
				{
					DebugDraw.begin(batch);
					DebugDraw.point(
						Color.PINK,
						new Vector2(
							viewport.x + (col * pixelSize.x) + (pixelSize.x / 2f) - 2,
							viewport.y + (row * pixelSize.y) + (pixelSize.y / 2f) - 2
						),
						4
					);
					DebugDraw.end(batch);
				}
			}
		}
		if (debug)
		{
			DebugDraw.begin(batch);
			DebugDraw.rectangle(Color.WHITE, viewport, 4);
			DebugDraw.end(batch);
		}
	}

	/** aktualizuje barvu v displayi na urcite pozici */
	@Override
	public void updateColor(int row, int col, Color color)
	{
		if (row < 0 || col < 0)
		{
			return;
		}
		if (row < size.y && col < size.x)
		{
			colorPixels[row][col].setColor(color);
			updatedPixels[row][col] = true;
		}
	}

	/** aktualizuje alpha (~pruhlednost) v displayi na urcite pozici */
	@Override
	public void updateAlpha(int row, int col, float alpha)
	{
		if (row < 0 || col < 0)
		{
			return;
		}
		if (row < size.y && col < size.x)
		{
			Color color = colorPixels[row][col].getColor();
			color.a = alpha;

			colorPixels[row][col].setColor(color);
		}
	}

	/** vymaze display danou barvou */
	@Override
	public void clearDisplay(Color color)
	{
		if (renderMode != RenderMode.NoBackground && background != null)
		{
			background.setColor(color);
		}
		clearUpdatedPixels();
		if (renderMode == RenderMode.NoBackground)
		{
			return;
		}
		for(int row = 0; row < size.y; row++)
		{
			for(int col = 0; col < size.x; col++)
			{
				updateColor(row, col, color);
			}
		}
	}

	/** uvolni zdroje z pameti */
	@Override
	public void dispose()
	{
		if (pixmap != null)
		{
			pixmap.dispose();
			pixmap = null;
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				if (colorPixels[row][col] != null)
				{
					colorPixels[row][col].dispose();
				}
			}
		}
	}

	/** vrati rozdil true/false mezi 2-ma obrazky (~pouziva se v grafickych efektech, abych vedel, co mam animovat) */
	@Override
	public boolean[][] getUpdatedPixels()
	{
		return updatedPixels;
	}

	@Override
	public void clearUpdatedPixels()
	{
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				updatedPixels[row][col] = false;
			}
		}
	}

	@Override
	public void setRenderMode(RenderMode renderMode)
	{
		this.renderMode = renderMode;
	}

	@Override
	public void flipX()
	{
		flip(false, true);
	}

	@Override
	public void flipY()
	{
		flip(true, false);
	}

	private void flip(boolean horizontal, boolean vertical)
	{
		if (!horizontal && !vertical)
		{
			return;
		}
		Color[][] tmp = new Color[(int) size.y][(int) size.x];
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				if (horizontal)
				{
					tmp[(getRows() - 1 - row)][col] = new Color(colorPixels[row][col].getColor());
				}
				if (vertical)
				{
					tmp[row][(getCols() - 1 - col)] = new Color(colorPixels[row][col].getColor());
				}
			}
		}
		for(int row = 0; row < getRows(); row++)
		{
			for(int col = 0; col < getCols(); col++)
			{
				colorPixels[row][col].setColor(tmp[row][col]);
			}
		}
		tmp = null;
	}
}
