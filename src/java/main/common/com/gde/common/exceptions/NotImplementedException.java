package com.gde.common.exceptions;

public class NotImplementedException
extends RuntimeException
{
	private static final long serialVersionUID = -611790405526374621L;

	public NotImplementedException()
	{
		super();
	}

	public NotImplementedException(String message)
	{
		super(message);
	}
}
