/*******************************************************************************
 * Copyright 2021 gde.tips
 * -- http://gde.tips
 *
 * Licensed under (CC BY 4.0),
 * "Creative Commons Attribution International", Version 4.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   https://creativecommons.org/licenses/by/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developed as part of game(s):
 * - Bombarder -- http://gde.tips/games/luzanky/bombarder
 ******************************************************************************/
package com.gde.lwjgl.util.vector;

/**
 * As android doesn't have org.lwjgl.util.vector.Vector4f this is substitution to it.
 * (WARNING: inpired by lwjgl 2.x! just be sure, its not full vector functionality!)
 * -- https://github.com/LWJGL/lwjgl/blob/master/src/java/org/lwjgl/util/vector/Vector4f.java
 */
public class Vector4f
{
	public float x, y, z, w;

	/**
	 * Constructor for Vector4f.
	 */
	public Vector4f()
	{
		super();
	}

	/**
	 * Constructor
	 */
	public Vector4f(float x, float y, float z, float w)
	{
		set(x, y, z, w);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.lwjgl.util.vector.WritableVector4f#set(float, float, float, float)
	 */
	public void set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	@Override
	public String toString()
	{
		return "Vector4f: " + x + " " + y + " " + z + " " + w;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Vector4f other = (Vector4f) obj;

		if (x == other.x && y == other.y && z == other.z && w == other.w)
		{
			return true;
		}

		return false;
	}
}
