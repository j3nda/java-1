package com.gde.luzanky.pexeso;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class DesktopLauncher
{
	public static void main(String[] arg)
	{
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		config.setForegroundFPS(60);
//		config.setWindowedMode(1024, 768);
		config.setWindowedMode(1280, 768);
//		config.setWindowedMode(1920, 1080);
//		config.setWindowedMode(1024, 500); // gplay/main-gfx

		new Lwjgl3Application(new PexesoGame(), config);
	}
}
