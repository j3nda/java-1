package com.gde.luzanky.pexeso.hraci.pocitac;

import java.util.Arrays;

import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.hraci.Hrac;

/**
 * {@link HracPocitac} "Tonda", chovani:
 * - pamatovat si posl. 3x kola
 * - kdyz da 1x kartu nahodne -> tak se snazi vybrat: z toho co si pamatuje (s uspesnosti 30%)
 *
 * (ze zapamatovanych karet se pokousi uhadnout dvojici, s uspesnosti 80%)
 */
public class HracTonda
extends HracPocitac
{
	private Karta[] zapamatovaneKarty = new Karta[6]; // TODO: HRA/NPC/Tonda: uprava struktury Karta[] vs prazdne bunky -> Set<Karta>
	private int indexKarty = 0;


	public HracTonda()
	{
		super("Tonda");
	}

	@Override
	public void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2)
	{
		super.onTah_FAILED(hrac, karta1, karta2);

		zapamatujSiKartu(karta1);
		zapamatujSiKartu(karta2);
	}

	@Override
	protected void zapamatujSiKartu(Karta karta)
	{
		zapamatovaneKarty[indexKarty] = karta;
		indexKarty++;
		if (indexKarty >= zapamatovaneKarty.length)
		{
			indexKarty = 0;
		}
		debug(
			getClass().getSimpleName() + ".tah(#" + odehraneKolo + "): "
			+ "zapamatujSiKartu: " + karta.id
			+ ", zapamatovaneKarty: " + Arrays.toString(zapamatovaneKarty)
		);
	}

	@Override
	protected void zapomenKarty(Karta karta1, Karta karta2)
	{
		// zapomene: karta1, karta2 (~pze dvojici nekdo mezitim uhadl)
		for(int i = 0; i < zapamatovaneKarty.length; i++)
		{
			if (karta1.equals(zapamatovaneKarty[i]) || karta2.equals(zapamatovaneKarty[i]))
			{
				zapamatovaneKarty[i] = null;
			}
		}
	}

	@Override
	public ChovaniHrace tah(Karta[][] hraciKarty)
	{
		// TODO: HRA/POCITAC: implementuj chovani hrace 'tonda'
		// (k dispozici mas:
		//  - odehraneKolo
		//  - odehranyCasTah
		//  - odehranyCasCelkem
		//  - uhadnuteKarty
		// )

		// hledani dvojice v zapamatovanych kartach...
		for(int i = 0; i < zapamatovaneKarty.length; i++)
		{
			for(int j = 0; j < zapamatovaneKarty.length; j++)
			{
				if (
					   zapamatovaneKarty[i] != null
					&& zapamatovaneKarty[j] != null
					&& zapamatovaneKarty[i].id == zapamatovaneKarty[j].id
					&& (
						   zapamatovaneKarty[i].getLogickaPozice().x != zapamatovaneKarty[j].getLogickaPozice().x
					    || zapamatovaneKarty[i].getLogickaPozice().y != zapamatovaneKarty[j].getLogickaPozice().y
					   )
					&& RandomUtils.nextInt(0, 100) < 80 // uspesnost 80%
				   )
				{
					return new ChovaniHrace(
						0, zapamatovaneKarty[i],
						0, zapamatovaneKarty[j]
					);
				}
			}
		}

		ChovaniHrace pc = super.tah(hraciKarty); // karta1, karta2 (~random)

		// 1x kartu dam nahodne -> a 2-hou zkusim hledat v zapamatovanych kartach
		for(int i = 0; i < zapamatovaneKarty.length; i++)
		{
			if (
				   zapamatovaneKarty[i] != null
				&& zapamatovaneKarty[i].id == pc.karta1.id
				&& (
					   zapamatovaneKarty[i].getLogickaPozice().x != pc.karta1.getLogickaPozice().x
				    || zapamatovaneKarty[i].getLogickaPozice().y != pc.karta1.getLogickaPozice().y
				   )
				&& RandomUtils.nextFloat(0, 100) < 30 // uspesnost 30%
			   )
			{
				return new ChovaniHrace(
					cekej(1),
					zapamatovaneKarty[i],
					cekej(2),
					pc.karta1
				);
			}
		}
		// vse bylo neuspesne -> vracim puvodni, nahodne: karta1, karta2
		return pc;
	}
}
