package com.gde.luzanky.pexeso.hraci.pocitac;

import java.util.Arrays;

import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.hra.Karta;

/**
 * {@link HracPocitac} "Roman", chovani:
 * - zapamatuje si karty napr s 80% uspesnosti
 * - a po nekolika tazich je zapomene
 *
 * (ze zapamatovanych karet se pokousi uhadnout dvojici)
 */
public class HracRoman
extends HracPocitac
{
	private Karta[] zapamatovaneKarty = new Karta[10]; // TODO: HRA/NPC/Roman: uprava struktury Karta[] vs prazdne bunky -> Set<Karta>
	private int indexKarty = 0;


	public HracRoman()
	{
		super("Roman");
	}

	@Override
	protected void zapamatujSiKartu(Karta karta)
	{
		// zapamatuje si: karta (~s 80% uspesnosti)
		int uspesnost = RandomUtils.nextInt(0, 100);
		if (uspesnost < 80)
		{
			zapamatovaneKarty[indexKarty] = karta;
			indexKarty++;
			if (indexKarty >= zapamatovaneKarty.length)
			{
				indexKarty = 0;
			}
			debug(
				getClass().getSimpleName() + ".tah(#" + odehraneKolo + "): "
				+ "zapamatujSiKartu: " + karta.id
				+ ", zapamatovaneKarty: " + Arrays.toString(zapamatovaneKarty)
			);
		}
		else
		{
			debug(
				getClass().getSimpleName() + ".tah(#" + odehraneKolo + "): "
				+ "zapamatujSiKartu: " + karta.id
				+ " ~ " + uspesnost + "%"
			);
		}
	}

	@Override
	protected void zapomenKarty(Karta karta1, Karta karta2)
	{
		// zapomene: karta1, karta2 (~pze dvojici nekdo mezitim uhadl)
		for(int i = 0; i < zapamatovaneKarty.length; i++)
		{
			if (karta1.equals(zapamatovaneKarty[i]) || karta2.equals(zapamatovaneKarty[i]))
			{
				zapamatovaneKarty[i] = null;
			}
		}
	}

	@Override
	public ChovaniHrace tah(Karta[][] hraciKarty)
	{
		// TODO: HRA/POCITAC: implementuj chovani hrace 'roman'
		// (k dispozici mas:
		//  - odehraneKolo
		//  - odehranyCasTah
		//  - odehranyCasCelkem
		//  - uhadnuteKarty
		// )

		// hledani dvojice v zapamatovanych kartach...
		for(int i = 0; i < zapamatovaneKarty.length; i++)
		{
			for(int j = 0; j < zapamatovaneKarty.length; j++)
			{
				if (
					   zapamatovaneKarty[i] != null
					&& zapamatovaneKarty[j] != null
					&& zapamatovaneKarty[i].id == zapamatovaneKarty[j].id
					&& (
						   zapamatovaneKarty[i].getLogickaPozice().x != zapamatovaneKarty[j].getLogickaPozice().x
					    || zapamatovaneKarty[i].getLogickaPozice().y != zapamatovaneKarty[j].getLogickaPozice().y
					   )
				   )
				{
					return new ChovaniHrace(
						cekej(1),
						zapamatovaneKarty[i],
						cekej(2),
						zapamatovaneKarty[j]
					);
				}
			}
		}
		// vse bylo neuspesne -> vracim puvodni, nahodne: karta1, karta2
		return super.tah(hraciKarty);
	}
}
