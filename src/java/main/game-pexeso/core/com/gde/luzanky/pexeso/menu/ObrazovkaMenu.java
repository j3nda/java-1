package com.gde.luzanky.pexeso.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.resources.CommonResources;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.PexesoGameConfiguration;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.HraciPlocha;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.Napoveda;
import com.gde.luzanky.pexeso.SdilenaObrazovka;
import com.gde.luzanky.pexeso.TypObrazovky;
import com.gde.luzanky.pexeso.hra.ObrazovkaHry;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac.JmenaHracu;
import com.gde.luzanky.pexeso.menu.OptionsButton.HintOptionsButton;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.SadaKaret;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies.PexesoPrefOption;
import com.gde.luzanky.pexeso.ui.FlyingPexeso;
import com.gde.luzanky.pexeso.ui.InfoPanel.InfoPanelConfig;
import com.gde.luzanky.pexeso.ui.PrstNapoveda;

public class ObrazovkaMenu
extends SdilenaObrazovka
{
	private FlyingPexeso flyingPexeso;
	private MenuControlsTable menuControls;
	private InfoPanelConfig infoPanelConfig;
	private MenuIndexFinger indexFinger;

	public ObrazovkaMenu(PexesoGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MENU;
	}

	@Override
	public void show()
	{
		super.show();

		Actor about = createButton(CommonResources.Gde.buttonGdeLightbulb);
		about.setVisible(false); // TODO: XHONZA: about/gde.tip && copyright && stuff
		about.setPosition(
			0,
			getHeight() - (about.getHeight() * about.getScaleY())
		);

		Actor exit = createButton(CommonResources.Gde.buttonExit);
		exit.setPosition(0, 0);
		exit.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				indexFinger.click(event, x, y);
				parentGame.end();
			}
		});

		infoPanelConfig = new InfoPanelConfig(
			getWidth(),
			getHeight(),
			(exit.getWidth() * exit.getScaleX()) * 2.5f,
			(exit.getWidth() * exit.getScaleX())
		);

		menuControls = createControls(
			infoPanelConfig.menuRightControlsWidth,
			(exit.getHeight() * exit.getScaleY()) * 2f
		);
		Actor controls = menuControls;
		controls.setSize(
			getWidth() - infoPanelConfig.menuLeftControlsWidth,
			getHeight()
		);
		controls.setPosition(
			(exit.getWidth() * exit.getScaleX()),
			0
		);

		getStage().addActor(
			flyingPexeso = new FlyingPexeso(
				resources.getResourcesManager(),
				new Rectangle(0, 0, getWidth(), getHeight())
			)
		);
		getStage().addActor(about);
		getStage().addActor(exit);
		getStage().addActor(controls);

		// napoveda: prst-ukazovacek
		indexFinger = new MenuIndexFinger(
			resources.getResourcesManager(),
			menuControls.boardHints
		);
		getStage().addActor(indexFinger);


		updateFlyingPexeso();
		// TODO: XHONZA/menu: vyber gameplay (na cas + limity)
	}

	private Actor createButton(String filename)
	{
		float size = getHeight() / 8f;

		Image tmp = new Image(new Texture(Gdx.files.internal(filename)));
		tmp.setAlign(Align.center);
		tmp.setSize(size, size);
		tmp.setColor(Color.WHITE);
		return tmp;
	}

	private MenuControlsTable createControls(float widthRight, float heightStart)
	{
		MenuControlsTable controls = new MenuControlsTable(resources.getResourcesManager(), widthRight, heightStart);
		controls.startButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				indexFinger.click(event, x, y);
				PexesoGameConfiguration nastaveniHry = new PexesoGameConfiguration(
					SadaKaret.VSECHNO[controls.cards.getCounter()],
					controls.opponent.getCounter(),
					HraciPlocha.values()[controls.boardSize.getCounter()],
					(Napoveda.values()[controls.boardHints.getCounter()] == Napoveda.On)
				);
				if ( // debug: pocitac x pocitac
					   PexesoGame.isDebug()
					&& HracPocitac.JmenaHracu.values()[controls.opponent.getCounter()] == JmenaHracu.Hrnec
				   )
				{
					nastaveniHry = new PexesoGameConfiguration(
						SadaKaret.VSECHNO[controls.cards.getCounter()],
						HraciPlocha.values()[controls.boardSize.getCounter()],
						(Napoveda.values()[controls.boardHints.getCounter()] == Napoveda.On)
					);
				}
				parentGame.setScreen(
					new ObrazovkaHry(
						parentGame,
						getScreenType(),
						nastaveniHry,
						infoPanelConfig
					)
				);
			}
		});
		controls.cards.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				indexFinger.click(event, x, y);
				int counter = controls.cards.onClick(event);
				resources
					.getPreferencies()
					.setOption(
						PexesoPrefOption.MenuGameSettings,
						PexesoPreferencies.toMenuGame(
							SadaKaret.VSECHNO[counter],
							JmenaHracu.values()[controls.opponent.getCounter()],
							HraciPlocha.values()[controls.boardSize.getCounter()],
							(Napoveda.values()[controls.boardHints.getCounter()] == Napoveda.On)
						)
					)
				;
				controls.cards.indicator.setText((counter + 1) + "");
				controls.cards.option.setText(SadaKaret.VSECHNO[counter].nazev);

				updateFlyingPexeso();
			}
		});
		controls.opponent.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				indexFinger.click(event, x, y);
				int counter = controls.opponent.onClick(event);
				resources
					.getPreferencies()
					.setOption(
						PexesoPrefOption.MenuGameSettings,
						PexesoPreferencies.toMenuGame(
							SadaKaret.VSECHNO[controls.cards.getCounter()],
							JmenaHracu.values()[counter],
							HraciPlocha.values()[controls.boardSize.getCounter()],
							(Napoveda.values()[controls.boardHints.getCounter()] == Napoveda.On)
						)
					)
				;
				controls.opponent.indicator.setText(HracPocitac.JmenaHracu.values()[counter].id());
				controls.opponent.option.setText(HracPocitac.JmenaHracu.values()[counter].nazev());
			}
		});
		controls.boardSize.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				indexFinger.click(event, x, y);
				int counter = controls.boardSize.onClick(event);
				resources
					.getPreferencies()
					.setOption(
						PexesoPrefOption.MenuGameSettings,
						PexesoPreferencies.toMenuGame(
							SadaKaret.VSECHNO[controls.cards.getCounter()],
							JmenaHracu.values()[controls.opponent.getCounter()],
							HraciPlocha.values()[counter],
							(Napoveda.values()[controls.boardHints.getCounter()] == Napoveda.On)
						)
					)
				;
				controls.boardSize.indicator.setText(
					controls.formatBoardSizeIndicator(
						HraciPlocha.values()[counter],
						Napoveda.values()[controls.boardHints.getCounter()]
					)
				);
				controls.boardSize.option.setText(HraciPlocha.values()[counter].nazev());

				updateFlyingPexeso();
			}
		});
		controls.boardHints.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				int counter = controls.boardHints.onClick(event);
				resources
					.getPreferencies()
					.setOption(
						PexesoPrefOption.MenuGameSettings,
						PexesoPreferencies.toMenuGame(
							SadaKaret.VSECHNO[controls.cards.getCounter()],
							JmenaHracu.values()[controls.opponent.getCounter()],
							HraciPlocha.values()[controls.boardSize.getCounter()],
							(Napoveda.values()[counter] == Napoveda.On)
						)
					)
				;
				controls.boardHints.update(Napoveda.values()[counter]);
//				controls.boardHints.option.setText(Napoveda.values()[counter].nazev());
				controls.boardSize.indicator.setText(
					controls.formatBoardSizeIndicator(
						HraciPlocha.values()[controls.boardSize.getCounter()],
						Napoveda.values()[counter]
					)
				);
				indexFinger.click(event, x, y);
			}
		});

		if (PexesoGame.isDebug())
		{
			controls.debug();
		}

		return controls;
	}

	private void updateFlyingPexeso()
	{
		int minKaret, maxKaret;
		switch (HraciPlocha.values()[menuControls.boardSize.getCounter()])
		{
			case Velka:
			{
				minKaret = 20;
				maxKaret = 36;
				break;
			}
			case Stredni:
			{
				minKaret = 18;
				maxKaret = 24;
				break;
			}
			default:
			case Mala:
			{
				minKaret = 8;
				maxKaret = 16;
				break;
			}
		}
		flyingPexeso.update(
			SadaKaret.VSECHNO[menuControls.cards.getCounter()],
			minKaret,
			maxKaret - 1
		);
		// TODO: XHONZA: vice sad karet -> vetsi pamet -> chce to uvolnovat! (prev,cur,next)
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}

	private class MenuIndexFinger
	extends PrstNapoveda
	{
		private final HintOptionsButton hints;

		public MenuIndexFinger(ResourcesManager resources, HintOptionsButton hints)
		{
			super(resources, (RandomUtils.nextInt(0, 100) % 2 == 0 ? true : false));
			this.hints = hints;
		}

		@Override
		public void click(InputEvent event, float x, float y)
		{
			if (Napoveda.values()[hints.getCounter()] != Napoveda.On)
			{
				return;
			}
			super.click(event, x, y);
		}
	}
}
