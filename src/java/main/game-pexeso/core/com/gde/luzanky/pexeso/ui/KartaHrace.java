package com.gde.luzanky.pexeso.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hra.IHerniLogika;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

/**
 * karta hrace (~vpravo)
 * - jmeno
 * - body
 * - uhadnute karty
 * - odehrany cas
 * - ...
 */
public class KartaHrace
extends Table
implements IHerniLogika
{
	final Hrac hrac;
	private final ResourcesManager resources;
	private Label hracLabel;
	Label bodyLabel;
	Label odehranyCasLabel;
	private Label pocetKaretLabel;
	private HorizontalGroup uhadnuteKarty;
	public final boolean isTop;


	KartaHrace(Hrac hrac, ResourcesManager resources, int align, float cardHeight)
	{
		super();
		this.hrac = hrac;
		this.resources = resources;
		this.isTop = (align == Align.top);
		createLayout(this, align);
	}

	private void createLayout(Table wrapper, int align)
	{
		switch(align)
		{
			case Align.top:
			{
				createLayoutTop(wrapper);
				break;
			}
			case Align.bottom:
			{
				createLayoutBottom(wrapper);
				break;
			}
		}
		if (PexesoGame.isDebug())
		{
			wrapper.debug();
		}
	}

	private void createLayoutTop(Table wrapper)
	{
		wrapper
			.add(hracLabel = new Label(hrac.jmeno, InfoPanel.labelStyles.hrac))
			.align(Align.center)
		;
		wrapper.row();
		wrapper
			.add(bodyLabel = new Label(hrac.celkoveBody + "", InfoPanel.labelStyles.hracSkore))
			.expandX()
			.align(Align.center)
		;
		wrapper.row();
		wrapper
			.add(odehranyCasLabel = new Label("", InfoPanel.labelStyles.hracCas))
			.expandX()
			.align(Align.right)
		;
		wrapper.row();
		wrapper
			.add(createUhadnuteKarty(InfoPanel.labelStyles.hracKarty, Align.top))
			.expandY()
			.growX()
			.align(Align.topRight)
		;
		wrapper.row();
	}

	private void createLayoutBottom(Table wrapper)
	{
		wrapper
			.add(createUhadnuteKarty(InfoPanel.labelStyles.hracKarty, Align.bottom))
			.expandY()
			.growX()
			.align(Align.bottomRight)
		;
		wrapper.row();
		wrapper
			.add(odehranyCasLabel = new Label("", InfoPanel.labelStyles.hracCas))
			.expandX()
			.align(Align.right)
		;
		wrapper.row();
		wrapper
			.add(bodyLabel = new Label(hrac.celkoveBody + "", InfoPanel.labelStyles.hracSkore))
			.expandX()
			.align(Align.center)
		;
		wrapper.row();
		wrapper
			.add(hracLabel = new Label(hrac.jmeno, InfoPanel.labelStyles.hrac))
			.align(Align.center)
		;
		wrapper.row();
	}

	private String formatPocetKaret(int pocet)
	{
		return (pocet > 0 ? pocet + "x" : "");
	}

	private Table createUhadnuteKarty(LabelStyle style, int align)
	{
		Table wrapper = new Table();

		wrapper
			.add(uhadnuteKarty = createKarty())
			.height(96) // TODO: XHONZA: uhadnuteKarty(height)
			.growX()
			.align(Align.center)
		;
		wrapper
			.add(pocetKaretLabel = new Label(formatPocetKaret(hrac.uhadnuteKarty.size() / 2), style))
			.align(align)
		;
		wrapper.row();
		wrapper.align(align);

		return wrapper;
	}

	private HorizontalGroup createKarty()
	{
		HorizontalGroup group = new HorizontalGroup();
		group.align(Align.center);
		if (PexesoGame.isDebug())
		{
			group.debug();
		}
		return group;
	}

	private final Rectangle uhadnuteKartyInfo = new Rectangle();
	private final Vector2 uhadnuteKartyPos = new Vector2();
	public Rectangle getUhadnuteKartyInfo()
	{
		uhadnuteKartyPos.set(uhadnuteKarty.getX(), uhadnuteKarty.getY());
		uhadnuteKarty.localToStageCoordinates(uhadnuteKartyPos);
		uhadnuteKartyInfo.set(
			uhadnuteKartyPos.x,
			uhadnuteKartyPos.y,
			uhadnuteKarty.getWidth() * uhadnuteKarty.getScaleX(),
			uhadnuteKarty.getHeight() * uhadnuteKarty.getScaleY()
		);
		return uhadnuteKartyInfo;
	}

	@Override
	public void setColor(Color color)
	{
		hracLabel.setColor(color);
		bodyLabel.setColor(color);
		odehranyCasLabel.setColor(color);
		pocetKaretLabel.setColor(color);
		uhadnuteKarty.getColor().a = color.a;
	}

	@Override
	public void onTah_OK(Hrac hrac, Karta karta1, Karta karta2)
	{
		int pocet = hrac.uhadnuteKarty.size() / 2;
		pocetKaretLabel.setText(formatPocetKaret(pocet));
		if (pocet > 1)
		{
			int space = karta1.getCardTexture().getWidth() / pocet;
//			float space = (karta1.getCardTexture().getWidth() * ka / pocet; // TODO: XHONZA: uhadnuteKarty(space between cards)
			uhadnuteKarty.space(space - karta1.getCardTexture().getWidth());
			uhadnuteKarty.layout();
		}
		Image card = new Image(karta1.getCardTexture());
		uhadnuteKarty.addActor(card);
	}

	@Override
	public void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2)
	{

	}
}
