package com.gde.luzanky.pexeso.resources.preferences;

import java.util.List;

import com.gde.luzanky.pexeso.PexesoGameConfiguration;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.HraciPlocha;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac.JmenaHracu;
import com.gde.luzanky.pexeso.resources.SadaKaret;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies.PlayerWinnerCard;

public interface IPexesoPreferencies
{
	/* game-id */
	SadaKaret getSadaKaret();
	JmenaHracu getNPC();
	HraciPlocha getHraciPlocha();
	boolean hasNapoveda();

	/* player-info */
	int getCelkoveSkore();
	long getCelkovyCas();
	int getCelkemHer();

	/* player-winner-cards */
	int getCelkemHer(SadaKaret sadaKaret);
	void setUhadnuteKartyIds(SadaKaret sadaKaret, int[] uhadnuteKartyIds);
	List<PlayerWinnerCard> getUhadnuteKartyIds(SadaKaret sadaKaret);

	/* player-stats: per{NPC, boardSize, cardSet}, return int[] {total, winner}; */
	void setPlayerStats(boolean vitez, PexesoGameConfiguration nastaveniHry);
	int[] getPlayerPerNpc(HracPocitac npc);
	int[] getPlayerPerBoard(HraciPlocha hraciPlocha);
	int[] getPlayerPerCardSet(SadaKaret sadaKaret);
}
