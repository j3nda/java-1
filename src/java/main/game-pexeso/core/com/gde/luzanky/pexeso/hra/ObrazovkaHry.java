package com.gde.luzanky.pexeso.hra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.utils.MathUtils;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.PexesoGameConfiguration;
import com.gde.luzanky.pexeso.SdilenaObrazovka;
import com.gde.luzanky.pexeso.TypObrazovky;
import com.gde.luzanky.pexeso.hra.vitez.ObrazovkaKonecHry;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.SadaKaret;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies.PexesoPrefOption;
import com.gde.luzanky.pexeso.ui.InfoPanel;
import com.gde.luzanky.pexeso.ui.InfoPanel.InfoPanelConfig;
import com.gde.luzanky.pexeso.ui.PozadiHraciPlochy;

/** herni obrazovka, zde se odehrava nase pexeso hra! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements IHerniGrafika
{
	/** stage (~divadelni jeviste), do ktereho pridavam 'herce', kterou jsou videt */
	private Stage stage;
	private ResourcesManager myResources;
	/** 2d-pole hracich karet */
	private Karta[][] hraciKarty;
	/** trida 'HerniLogika' mi zaobaluje chovani cele herniLogika.tah() */
	private HerniLogika herniLogika;
	/** 1. hrac je clovek, abych mohl navesit ClickListener -> tap/click na kartu */
	private final Hrac clovek;
	/** informacni panel 'napravo' */
	private InfoPanel infoPanel;
	/** predchozi {@InputProcessor}, abych si ho uchoval a nastavil zpatky po skonceni hry */
	private InputProcessor prevInputProcessor;
	private final PexesoGameConfiguration nastaveniHry;
	private PozadiHraciPlochy hraciPlochaPozadi;
	/** napoveda (~pokud je zapnuta), usnadnuje orientaci... */
	private Napoveda napoveda;
	/** informacni panel(~vpravo) s hraci, body a uhadnutyma kartama */
	private final InfoPanelConfig infoPanelConfig;
	/** indikator uspesneho tahu (~karty leti -> do informacniho panelu) */
	private final Image[] uspesnyTah = new Image[]{
		new Image(),
		new Image(),
	};


	public ObrazovkaHry(
		PexesoGame parentGame,
		TypObrazovky previousScreenType,
		PexesoGameConfiguration gameConfiguration,
		InfoPanelConfig infoPanelConfig
	)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
		this.nastaveniHry = gameConfiguration;
		this.infoPanelConfig = infoPanelConfig;
		this.clovek = gameConfiguration.hraci[0];
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA;
	}

	/** 'renderScreen' se vola neustale, tj. 30fps */
	@Override
	protected void renderScreen(float delta)
	{
		// renderuje vsechny objekty, ktere jsou ve 'stage'
		// - mam tam i HerniLogiku -> takze se provolava herniLogika.act();
		// - a vsechny prvky 'grafiky', tj. stage.draw() -> takze vsechny hraci karty a UI
		renderScreenStage(delta);
	}

	private static final Color clearColor = new Color(0x323232FF);
	@Override
	protected Color getClearColor()
	{
		return clearColor;
	}

	@Override
	public void show()
	{
		// zapamatuju si predchozi 'input-processor'
		prevInputProcessor = Gdx.input.getInputProcessor();

		// zavolam 'show() rodice'
		super.show();

		// vytvori kontejner, kam budu davat hraci-karty
		// 'stage' ~ je neco jako jeviste v divadle -> vyobrazuje vsechny herce na jevisti
		// (zde: dedim vlastnosti SdilenaObrazovka --> ScreenBase; ktery obsahuje pred-vytvorenou stage, takze: get)
		stage = getStage();

		// vratim si 'ResourcesManager' pro pexeso z 'PexesoScreenResources',
		// protoze vyuzivam DRY: 'obrazovka<typ-obrazovky>' a GdeGame<TypObrazovky>
		myResources = resources.getResourcesManager();

		// pozadi, obrazek 1. uhadnute karty: aby to nevypadalo blbe (~cerne diry po odebrani uhadnutych karet)
		hraciPlochaPozadi = new PozadiHraciPlochy(myResources);
		hraciPlochaPozadi.setSize(
			infoPanelConfig.gameViewport.width,
			infoPanelConfig.gameViewport.height
		);
		hraciPlochaPozadi.setPosition(
			infoPanelConfig.gameViewport.x,
			infoPanelConfig.gameViewport.y
		);
		hraciPlochaPozadi.setOrigin(Align.center);
		hraciPlochaPozadi.setTvOverlay(new Rectangle(
			infoPanelConfig.gameViewport.x,
			infoPanelConfig.gameViewport.y,
			getWidth() - (infoPanelConfig.gameLeftControlsWidth + infoPanelConfig.gameRightControlsWidth),
			infoPanelConfig.gameViewport.height
		));
		stage.addActor(hraciPlochaPozadi);

		// a vytvorim 'ukazatel na prave otacenou kartu'
		napoveda = new Napoveda(
			nastaveniHry.napoveda,
			myResources,
			new Vector2(getWidth(), getHeight())
		);

		// a spustim hru
		startHry();

		// inicializuju napovedu
		inicializujNapovedu();

		// vytvorim a nastavim novy 'input-processor', ktery umi hybat palkou...
		setInputProcessor(stage);
	}

	/** 'startHry' se zavola na zacatku; ovlada vytvoreni a nastaveni vseho co hra potrebuje */
	private void startHry()
	{
		// zavola metodu: 'vytvorKarty' pro vytvoreni 2d-pole karet
		hraciKarty = vytvorKarty(
			nastaveniHry.maxSloupcu,
			nastaveniHry.maxRadek,
			nastaveniHry.sadaKaret
		);

		// vytvori 'herni-logiku' cele hry
		herniLogika = new HerniLogika(
			hraciKarty,
			this,
			nastaveniHry.hraci
		);
		stage.addActor(herniLogika);

		// TODO: GRAFIKA: vizualizuj oba hrace (~jejich skore, a uhadnute karty)
		infoPanel = vytvorInfoPanel(herniLogika);
		stage.addActor(infoPanel);

		// pripravi hru...
		herniLogika.priprav();

		// upravi velikost 'hraciKarty' podle velikosti obrazovky a nastavi jim spravnou pozici (~do mrizky)
		// (volam to schvalne az po 'logika.startHry', protoze logika karty micha!)
		upravVelikostKaret(hraciKarty);

		// spusti hru...
		herniLogika.start();
	}

	private void inicializujNapovedu()
	{
		// pridam 'napovedu' do 'stage' jako posledni (~aby byla vzdy nahore!)
		napoveda.inicializuj(stage);

		// a pridam 'indikator uspesneho tahu' (~letajici karticky)
		for(Image image : uspesnyTah)
		{
			stage.addActor(image);
		}
	}

	private Karta[][] vytvorKarty(int maxSloupcu, int maxRadku, SadaKaret sadaKaret)
	{
		// kontrola! maxSloupcu * maxRadku musi byt sude cislo!
		int velikostPoleKaret = maxSloupcu * maxRadku;
		if (velikostPoleKaret % 2 != 0)
		{
			throw new RuntimeException(
				"CHYBA! maxSloupcu(" + maxSloupcu + ") * maxRadku(" + maxRadku + ")"
				+ " = " + velikostPoleKaret
				+ "! a to neni sude cislo!"
				+ " (~nelze zarucit presne dvojice karet)"
			);
		}

		// ziskam textury karet...
		Map<String, Texture> texturyKaret = myResources.get(sadaKaret);
		Texture pozadiKarty = texturyKaret.get(ResourcesManager.BACK_ID);
		List<Integer> kartyIds = new ArrayList<>(Arrays.asList(sadaKaret.kartyIds));
		Collections.shuffle(kartyIds);

		// zkontroluju, zda mam dostatek karet k pokryti plochy...
		if (
			   texturyKaret.values().size() - 1 < velikostPoleKaret / 2
			|| kartyIds.size() < maxSloupcu * maxRadku
		   )
		{
			if (!vytvorKarty_doplnKdyzChybi(maxSloupcu, maxRadku, texturyKaret, kartyIds, sadaKaret))
			{
				throw new RuntimeException(
					"CHYBA! sadaKaret("
					+ (texturyKaret.values().size() - 1)
					+ ") nema dostatek karet k pokryti hraci plochy("
					+ "x: " + maxSloupcu
					+ ", y: " + maxRadku
					+ ")!"
				);
			}
		}

		Karta[][] karty = new Karta[maxSloupcu][maxRadku];
		int sloupcu = karty.length;
		int radku = karty[0].length;
		int bodyMax = sloupcu * radku;
		for (int x = 0; x < sloupcu; x++)
		{
			for (int y = 0; y < radku; y++)
			{
				// spoctu 'id' karty
				int id = ((y * karty.length) + x) % (velikostPoleKaret / 2);

				// vytvorim novou kartu!
				final Karta karta = new Karta(
					id,
					RandomUtils.nextInt(10, bodyMax * 5),
					texturyKaret.get( // vrati texturu karty
						kartyIds.get(id) + "" // z pole(id karet), na zaklade 'id' precte cislo karty
					),
					pozadiKarty
				);
				karta.setResourcesManager(myResources);
				karta.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						// TODO: HRA: realizuj 'gameplay' mechaniku (~tj. chovani) hry pexeso!
						if (herniLogika.jsemNaTahu(clovek))
						{
							herniLogika.tahClovek(karta);
						}
					}
				});
				stage.addActor(karta); // pridam kartu do 'stage' (~jeviste)
				karty[x][y] = karta;   // na konkretni misto v mrizce (~poli)
			}
		}
		return karty;
	}

	/**
	 * snazi se doplnit kolekci 'texturyKaret' o hraci karty z nasleduji sady karet.
	 * vrati true, pokud se to povede. jinak false.
	 */
	private boolean vytvorKarty_doplnKdyzChybi(
		int maxSloupcu, int maxRadku,
		Map<String, Texture> texturyKaret, List<Integer> kartyIds,
		SadaKaret sadaKaret
	)
	{
		// snazim se najit 'index' pro aktualni sadaKaret, abych se mohl odkazat na: nasledujici, popr. predchozi...
		int index = SadaKaret.findOrdinal(sadaKaret);

		// nic jsem nenasel, koncim...
		if (index < 0)
		{
			return false;
		}

		// zkusim nasledujici sadu
		index++;
		if (index >= SadaKaret.VSECHNO.length)
		{
			index = 0;
		}
		SadaKaret nasledujiciSada = SadaKaret.VSECHNO[index];
		List<Integer> nasledujiciIds = new LinkedList<Integer>(Arrays.asList(nasledujiciSada.kartyIds));
		Collections.shuffle(nasledujiciIds);

		Map<String, Texture> nasledujiciTextury = myResources.get(nasledujiciSada);
		do
		{
			int kartaId = MathUtils.max(kartyIds) + 1; // nove 'id karty' bude: max() + 1
			int findId = nasledujiciIds.remove(0);     // zjistim 'id karty' z noveho setu; a ihned jej odeberu z kolekce
			kartyIds.add(kartaId);                     // pridam nove 'id karty' -> do seznamu, ze ktereho se karty vybiraji do hry
			texturyKaret.put(                          // zapamatuju si texturu karty (~pod novym 'id karty')
				kartaId + "",
				nasledujiciTextury.get(findId + "")
			);
			if (PexesoGame.isDebug())
			{
				System.out.println(
					"vytvorKarty_doplnKdyzChybi: "
					+ "sadaId=" + nasledujiciSada.id
					+ ", fintId=" + findId
					+ ", kartaId=" + kartaId
					+ ", size=" + kartyIds.size() + "/" + nasledujiciIds.size()
				);
			}
			if (nasledujiciIds.size() == 0)
			{ // pokud nemam zadne moznosti, ze kterych karet mohu doplnovat...
				if (PexesoGame.isDebug())
				{
					System.out.println("vytvorKarty_doplnKdyzChybi: rekurze...");
				}
				// rekurzivne! zavolam sam sebe -> a posunu sadaKaret + 1 -> aby to melo smysl ;)
				vytvorKarty_doplnKdyzChybi(
					maxRadku, maxSloupcu,
					texturyKaret, kartyIds,
					nasledujiciSada
				);
			}
		}
		// iteruju, dokud to dava smysl
		while(kartyIds.size() < maxSloupcu * maxRadku && nasledujiciIds.size() > 0);

		// zamicham...
		Collections.shuffle(kartyIds);
		return true;
	}

	private void upravVelikostKaret(Karta[][] karty)
	{
		upravVelikostKaret(
			hraciKarty,
			infoPanelConfig.gameViewport
		);
	}

	/** upravi velikost karet podle pozadovane velikosti vcetne mezery mezi kartami */
	private void upravVelikostKaret(Karta[][] karty, Rectangle hraciPlochaViewport)
	{
		int sirkaHraciPlochy = (int) hraciPlochaViewport.width;
		int vyskaHraciPlochy = (int) hraciPlochaViewport.height;
		int sloupcu = karty.length;
		int radku = karty[0].length;
		Vector2 pos = new Vector2();
		for (int x = 0; x < sloupcu; x++)
		{
			for (int y = 0; y < radku; y++)
			{
				Karta karta = karty[x][y];
				pos.set(karta.getLogickaPozice());

				karta.setScaleX( (sirkaHraciPlochy / (float)sloupcu) / karta.getWidth()  );
				karta.setScaleY( (vyskaHraciPlochy / (float)radku)   / karta.getHeight() );

				karta.setX( (pos.x * karta.getWidth()  * karta.getScaleX()) + hraciPlochaViewport.x );
				karta.setY( (pos.y * karta.getHeight() * karta.getScaleY()) + hraciPlochaViewport.y);
			}
		}

		// 'indikator uspesneho tahu' (~letajici karticky)
		float helpWidth = hraciKarty[0][0].getWidth();
		float helpHeight = hraciKarty[0][0].getHeight();
		for(Image image : uspesnyTah)
		{
			image.setVisible(false);
			image.setScaleX( (sirkaHraciPlochy / (float)sloupcu) / helpWidth  );
			image.setScaleY( (vyskaHraciPlochy / (float)radku)   / helpHeight );
			image.setSize(helpWidth, helpHeight);
		}
	}

	private InfoPanel vytvorInfoPanel(HerniLogika herniLogika)
	{
		InfoPanel panel = new InfoPanel(herniLogika, myResources);
		panel.setSize(
			infoPanelConfig.gameRightControlsWidth,
			getHeight()
		);
		panel.setPosition(
			getWidth() - (panel.getWidth() * panel.getScaleX()),
			0
		);
		if (PexesoGame.isDebug())
		{
			panel.debug();
		}
		return panel;
	}

	@Override
	public void onTah(Hrac hrac, Karta karta)
	{
		debug("grafika.onTah(" + karta.id + "): " + hrac.jmeno);
		napoveda.onTah(karta);
	}

	@Override
	public void onTah_OK(Hrac hrac, Karta karta1, Karta karta2)
	{
		debug("grafika.onTah_OK(" + karta1.id + ", " + karta2.id + "): " + hrac.jmeno);

		// 1. uhadnutou kartu pouziju pro zobrazeni pozadi
		// (aby, kdyz se karty odebiraji z hraci plochy -> nebyla videt dira (~jen 'cerna' barva)
		hraciPlochaPozadi.aktualizuj(karta1);

		// aktualizace: infoPanel.KartaHrace
		if (infoPanel instanceof IHerniLogika)
		{
			infoPanel.onTah_OK(hrac, karta1, karta2);
		}

		// TODO: GRAFIKA: animace uspesneho tahu
		onTah_OK_animaceUspesnehoTahu(hrac, karta1, uspesnyTah[0]);
		onTah_OK_animaceUspesnehoTahu(hrac, karta2, uspesnyTah[1]);
	}

	private void onTah_OK_animaceUspesnehoTahu(Hrac hrac, Karta karta, Image animace)
	{
		animace.setPosition(
			(karta.getLogickaPozice().x * karta.getWidth()  * karta.getScaleX()) + infoPanelConfig.gameViewport.x,
			(karta.getLogickaPozice().y * karta.getHeight() * karta.getScaleY()) + infoPanelConfig.gameViewport.y
		);
		animace.setSize(
			karta.getWidth(),
			karta.getHeight()
		);
		animace.setScale(
			karta.getScaleX(),
			karta.getScaleY()
		);
		animace.setDrawable(new TextureRegionDrawable(new TextureRegion(karta.getCardTexture())));
		animace.clear();

		Rectangle targetPos = infoPanel.findKartaHrace(hrac).getUhadnuteKartyInfo();
		float scale = 0.25f; //targetPos.height / (animace.getHeight() * animace.getScaleY()); // TODO: XHONZA: animaceUspesnehoTahu(scale)
		float duration = 0.5f;
		animace.addAction(Actions.sequence(
			Actions.alpha(1f),
			Actions.visible(true),
			Actions.parallel(
				Actions.moveTo(
					targetPos.x + (targetPos.width  / 2f),
					targetPos.y + (targetPos.height / 2f),
					duration
				),
				Actions.scaleTo(scale, scale, duration)
			),
			Actions.alpha(0),
			Actions.visible(false)
		));
	}

	@Override
	public void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2)
	{
		debug("grafika.onTah_FAILED(" + karta1.id + ", " + karta2.id +  "): " + hrac.jmeno);

		// aktualizace: infoPanel.KartaHrace
		if (infoPanel instanceof IHerniLogika)
		{
			infoPanel.onTah_FAILED(hrac, karta1, karta2);
		}

		// TODO: GRAFIKA: animace neuspesneho tahu
	}

	@Override
	public void onTah_NeuplnyPokracuj(Hrac hrac, Karta karta)
	{
		debug("grafika.onTah_NeuplnyPokracuj(" + karta.id + "): " + hrac.jmeno);
	}

	@Override
	public void onKonecHry(Hrac vitez)
	{
		debug("grafika.onKonecHry(" + vitez.jmeno + ")");
		// TODO: STATISTIKA/BONUS: uloz dosazene body (a odehrany cas), pokud je vitezem 'clovek'
		if (!vitez.jsiPocitac)
		{
			((PexesoPreferencies)myResources.getPreferencies()).setOption(
				PexesoPrefOption.PlayerTotalScore,
				myResources.getPreferencies().getCelkoveSkore() + vitez.celkoveBody
			);
			((PexesoPreferencies)myResources.getPreferencies()).setOption(
				PexesoPrefOption.PlayerTotalTime,
				myResources.getPreferencies().getCelkovyCas() + (long)vitez.odehranyCasCelkem
			);

			// TODO: STATISTIKA/BONUS: uloz uhadnute karty, pokud je vitezem 'clovek'
			int[] uhadnuteKartyIds = new int[vitez.uhadnuteKarty.size()];
			if (uhadnuteKartyIds.length > 0)
			{
				for(int i = 0; i < uhadnuteKartyIds.length; i++)
				{
					uhadnuteKartyIds[i] = vitez.uhadnuteKarty.get(i).id;
				}
				myResources.getPreferencies().setUhadnuteKartyIds(
					nastaveniHry.sadaKaret,
					uhadnuteKartyIds
				);
			}
		}
		boolean hrajeClovek = false;
		for(Hrac hrac : nastaveniHry.hraci)
		{
			if (!hrac.jsiPocitac)
			{
				hrajeClovek = true;
				break;
			}
		}
		if (hrajeClovek)
		{
			// TODO: STATISTIKA/BONUS: uloz celkovy pocet her, pokud hraje 'clovek'
			((PexesoPreferencies)myResources.getPreferencies()).setOption(
				PexesoPrefOption.PlayerTotalGames,
				myResources.getPreferencies().getCelkemHer() + 1
			);
			// TODO: STATISTIKA/BONUS: uloz pomer: celkovy pocet her vs vitezstvi vs NPC (pokud hraje 'clovek')
			myResources.getPreferencies().setPlayerStats(
				!vitez.jsiPocitac,
				nastaveniHry
			);
			// TODO: STATISTIKA/BONUS: vypis: celkoveSkore, odehranyCas, celkemHer, pokud hraje 'clovek'
			debug("\tcelkoveSkore: " + myResources.getPreferencies().getCelkoveSkore());
			debug("\tcelkovyCas--: " + myResources.getPreferencies().getCelkovyCas() + "s");
			debug("\tcelkemHer---: " + myResources.getPreferencies().getCelkemHer() + "x");
		}
		// TODO: GRAFIKA: animace konec hry vc. zobrazeni viteze
		parentGame.setScreen(
			new ObrazovkaKonecHry(
				parentGame,
				nastaveniHry,
				vitez,
				infoPanelConfig,
				herniLogika.odehraneKolo,
				herniLogika.odehranyCas,
				hraciPlochaPozadi.getKarta()
			)
		);
	}

	@Override
	public void onZmenaHrace(Hrac aktualniHrac, Hrac predchoziHrac)
	{
		debug("grafika.onZmenaHrace(" + predchoziHrac.jmeno + " -> " + aktualniHrac.jmeno + ")");
		// TODO: GRAFIKA: animace zmeny hrace
		infoPanel.onZmenaHrace(aktualniHrac, predchoziHrac);
	}

	@Override
	public void onZmenaSkore(Hrac hrac, int bodyPrirustek)
	{
		debug("grafika.onZmenaSkore(" + hrac.jmeno + ", " + bodyPrirustek + ")");
		// TODO: GRAFIKA: animace zmeny bodu u hrace
		infoPanel.onZmenaSkore(hrac, bodyPrirustek);
	}

	private void debug(String message)
	{
		if (PexesoGame.isDebug())
		{
			System.out.println(message);
		}
	}

	@Override
	public void hide()
	{
		Gdx.input.setInputProcessor(prevInputProcessor);
		super.hide();
	}
}
