package com.gde.luzanky.pexeso.resources;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SadaKaret
{
	public final String id;
	public final String nazev;
	public final String[] tags;
	public final Integer[] kartyIds;
	public static final SadaKaret[] VSECHNO = new SadaKaret[] {
		new SadaKaret("04B61", "Vecernicek 1", new String[] {"vecernicek", "nejlepsi"}, fill(23)),
		new SadaKaret("M5U1V", "Vecernicek 2", new String[] {"vecernicek", "nejlepsi"}, fill(32, 26)),
	};

	private SadaKaret(String sada, String nazev, String[] tags, Integer[] kartyIds)
	{
		this.id = sada;
		this.nazev = nazev;
		this.tags = tags;
		this.kartyIds = kartyIds;
	}

	private static Integer[] fill(int max)
	{
		Integer[] ids = new Integer[max];
		for(int i = 0; i < max; i++)
		{
			ids[i] = i;
		}
		return ids;
	}

	private static Integer[] fill(int max, int... without)
	{
		Set<Integer> ids = new HashSet<Integer>(Arrays.asList(fill(max)));
		for(Integer i : without)
		{
			ids.remove(i);
		}
		return ids.toArray(new Integer[ids.size()]);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		SadaKaret other = (SadaKaret) obj;
		if (id == null)
		{
			if (other.id != null)
			{
				return false;
			}
		}
		else
			if (!id.equals(other.id))
			{
				return false;
			}
		return true;
	}

	/** vrati 'poradi' (~index/ordinal) v ramci pole {@link SadaKaret#VSECHNO} */
	public static int findOrdinal(SadaKaret sadaKaret)
	{
		for(int i = 0; i < SadaKaret.VSECHNO.length; i++)
		{
			if (SadaKaret.VSECHNO[i].id.equals(sadaKaret.id))
			{
				return i;
			}
		}
		return -1;
	}

	public static SadaKaret fromId(String id)
	{
		for(int i = 0; i < SadaKaret.VSECHNO.length; i++)
		{
			if (SadaKaret.VSECHNO[i].id.equals(id))
			{
				return SadaKaret.VSECHNO[i];
			}
		}
		return SadaKaret.VSECHNO[0];
	}
}
