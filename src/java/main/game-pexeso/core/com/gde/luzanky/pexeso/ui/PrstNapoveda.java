package com.gde.luzanky.pexeso.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.common.graphics.ui.items.gestures.ClickFinger;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

public class PrstNapoveda
extends ClickFinger
{
	private static final Color[] LEVY = new Color[] {Color.LIME, Color.RED};
	private static final Color[] PRAVY = new Color[] {Color.YELLOW, Color.RED};
	private static final Vector2 indexFingerOffset = new Vector2(43, 89);
	private static final Vector2 clickAnimationOffset = new Vector2(28, 48);
	private final int parentHeight;


	public PrstNapoveda(ResourcesManager resources)
	{
		this(resources, false);
	}

	public PrstNapoveda(ResourcesManager resources, boolean flip)
	{
		this(resources, flip, 0.015f, Gdx.graphics.getHeight());

		Color[] colors = (flip ? LEVY : PRAVY);
		setColor(colors[0], colors[1]);
	}

	private PrstNapoveda(ResourcesManager resources, boolean flip, float duration, int parentHeight)
	{
		super(
			duration,
			flipTexture(resources.get(CommonResources.Gesture.indexFinger82x96, Texture.class), flip),
			indexFingerOffset,
			createClickFrames(resources, flip),
			clickAnimationOffset
		);
		flipOffsets(flip);
		setVisible(false);
		this.parentHeight = parentHeight;
	}

	private static TextureRegion flipTexture(Texture texture, boolean flip)
	{
		TextureRegion region = new TextureRegion(texture);
		region.flip(flip, false);
		return region;
	}

	private void flipOffsets(boolean flip)
	{
		if (flip)
		{
			handOffset.x = (getWidth() * getScaleX()) - handOffset.x;
			clickOffset.x = (clickAnimation.getWidth() * clickAnimation.getScaleX()) - clickOffset.x;
		}
	}

	private static TextureRegion[] createClickFrames(ResourcesManager resources, boolean flip)
	{
		int start = 395;
		int maxFrames = 35;
		TextureRegion[] frames = new TextureRegion[maxFrames];
		for(int i = start; i < start + maxFrames; i++)
		{
			frames[i - start] = new TextureRegion(
				resources.get(CommonResources.Animation.frame(CommonResources.Animation.click72x96, i), Texture.class)
			);
			if (flip)
			{
				frames[i - start].flip(true, false);
			}
		}
		return frames;
	}

	@Override
	protected void onClick(int counter)
	{
		super.onClick(counter);
		if (counter > 0)
		{
			return;
		}
		addAction(Actions.sequence(
			Actions.alpha(0, 0.1f),
			Actions.visible(false)
		));
	}

	public void click(InputEvent event, float x, float y)
	{
		Actor target = event.getTarget();
		if (target != null && target.getStage() != null)
		{
			Vector2 actorPos = target.localToStageCoordinates(new Vector2(x, y));
			Vector2 screenPos = target.getStage().stageToScreenCoordinates(actorPos);
			x = screenPos.x;
			y = parentHeight - screenPos.y;
		}
		click(x, y);
	}

	public void click(float x, float y)
	{
		reset();
		setVisible(true);

		x -= handOffset.x;
		y -= handOffset.y;

		addAction(Actions.sequence(
			Actions.alpha(0),
			Actions.parallel(
				Actions.alpha(handAlpha, 0.15f),
				Actions.moveTo(x, y, 0.25f, Interpolation.pow3In)
			),
			Actions.run(new Runnable()
			{
				@Override
				public void run()
				{
					click();
				}
			})
		));
	}
}
