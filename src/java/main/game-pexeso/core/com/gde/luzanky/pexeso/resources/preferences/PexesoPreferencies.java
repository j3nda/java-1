package com.gde.luzanky.pexeso.resources.preferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.kobjects.base64.Base64;

import com.gde.common.resources.preferences.IPreferencesOptionEnum;
import com.gde.common.resources.preferences.PreferencesOption;
import com.gde.common.utils.MathUtils;
import com.gde.luzanky.pexeso.PexesoGameConfiguration;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.HraciPlocha;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac.JmenaHracu;
import com.gde.luzanky.pexeso.resources.SadaKaret;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies.PexesoPrefOption;

public class PexesoPreferencies
extends PreferencesOption<PexesoPrefOption>
implements IPexesoPreferencies
{
	public enum PexesoPrefOption
	implements IPreferencesOptionEnum
	{
		MenuGameSettings(
			"menuGame",
			toMenuGame(SadaKaret.VSECHNO[0], HracPocitac.JmenaHracu.values()[0], HraciPlocha.values()[0], true),
			"nastaveni hry v menu"
		),
		PlayerTotalScore("p1-total-score", Integer.valueOf(0), "P1: celkove skore"),
		PlayerTotalTime("p1-total-time", Long.valueOf(0), "P1: celkovy cas"),
		PlayerTotalGames("p1-total-games", Integer.valueOf(0), "P1: celkovy pocet her"),
		PlayerWinnerCards("p1-winner-%s", "", "P1: uhadnute karty v sade"),
		PlayerPerNpc("p1-x-npc-%s", "", "P1 vs NPC"),
		PlayerPerBoard("p1-x-board-%s", "", "P1 vs hraciPlocha"),
		PlayerPerCardSet("p1-x-cards-%s", "", "P1 vs sadaKaret"),
		;
		private final String name;
		private final Object defaultValue;
		private final String description;

		private PexesoPrefOption(String name, Object defaultValue, String description)
		{
			this.name = name;
			this.defaultValue = defaultValue;
			this.description = description;
		}
		@Override
		public Object getDefaultValue()
		{
			return defaultValue;
		}
		@Override
		public String getName()
		{
			return name;
		}
	}

	public PexesoPreferencies()
	{
		this("PexesoGame-luzanky");
	}

	protected PexesoPreferencies(String name)
	{
		super(name);
	}

	@Override
	public SadaKaret getSadaKaret()
	{
		try
		{
			String[] parts = ((String)getOption(PexesoPrefOption.MenuGameSettings)).split(toMenuGameSeparator);
			return SadaKaret.fromId(parts[0]);
		}
		catch(Exception e)
		{
			return SadaKaret.VSECHNO[0];
		}
	}

	@Override
	public JmenaHracu getNPC()
	{
		try
		{
			String[] parts = ((String)getOption(PexesoPrefOption.MenuGameSettings)).split(toMenuGameSeparator);
			return JmenaHracu.fromId(parts[1]);
		}
		catch(Exception e)
		{
			return JmenaHracu.values()[0];
		}
	}

	@Override
	public HraciPlocha getHraciPlocha()
	{
		try
		{
			String[] parts = ((String)getOption(PexesoPrefOption.MenuGameSettings)).split(toMenuGameSeparator);
			return HraciPlocha.fromId(parts[2]);
		}
		catch(Exception e)
		{
			return HraciPlocha.values()[0];
		}
	}

	@Override
	public boolean hasNapoveda()
	{
		try
		{
			String[] parts = ((String)getOption(PexesoPrefOption.MenuGameSettings)).split(toMenuGameSeparator);
			return (parts.length > 3 && Integer.valueOf(parts[3]) == 1 ? true : false);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
	}

	private static final String toMenuGameSeparator = ";";
	public static String toMenuGame(SadaKaret sadaKaret, JmenaHracu npc, HraciPlocha hraciPlocha, boolean napoveda)
	{
		return String.join(
			toMenuGameSeparator,
			sadaKaret.id,
			npc.id(),
			hraciPlocha.id,
			(napoveda ? "1" : "0")
		);
	}

	private static final String toPlayerStatsSeparator = ";";
	@Override
	public void setPlayerStats(boolean vitez, PexesoGameConfiguration nastaveniHry)
	{
		for(Hrac hrac : nastaveniHry.hraci)
		{
			if (!hrac.jsiPocitac || JmenaHracu.fromNazev(hrac.jmeno) == null)
			{
				continue;
			}
			updatePlayerPerNpc(vitez, (HracPocitac)hrac);
			updatePlayerPerBoard(vitez, nastaveniHry.hraciPlocha);
			updatePlayerPerCardSet(vitez, nastaveniHry.sadaKaret);
		}
	}

	private void updatePlayerPerNpc(boolean vitez, HracPocitac npc)
	{
		PexesoPrefOption option = PexesoPrefOption.PlayerPerNpc;
		int[] stats = getPlayerPerNpc(npc);
		getPreferences().putString(
			String.format(option.getName(), npc.jmeno.toLowerCase()),
			String.join(
				toPlayerStatsSeparator,
				(stats[0] + 1) + "",
				(stats[1] + (vitez ? +1 : 0)) + ""
			)
		);
	}

	private void updatePlayerPerBoard(boolean vitez, HraciPlocha hraciPlocha)
	{
		PexesoPrefOption option = PexesoPrefOption.PlayerPerBoard;
		int[] stats = getPlayerPerBoard(hraciPlocha);
		getPreferences().putString(
			String.format(option.getName(), hraciPlocha.id.toLowerCase()),
			String.join(
				toPlayerStatsSeparator,
				(stats[0] + 1) + "",
				(stats[1] + (vitez ? +1 : 0)) + ""
			)
		);
	}

	private void updatePlayerPerCardSet(boolean vitez, SadaKaret sadaKaret)
	{
		PexesoPrefOption option = PexesoPrefOption.PlayerPerCardSet;
		int[] stats = getPlayerPerCardSet(sadaKaret);
		getPreferences().putString(
			String.format(option.getName(), sadaKaret.id.toLowerCase()),
			String.join(
				toPlayerStatsSeparator,
				(stats[0] + 1) + "",
				(stats[1] + (vitez ? +1 : 0)) + ""
			)
		);
	}

	@Override
	public int getCelkoveSkore()
	{
		return (Integer)getOption(PexesoPrefOption.PlayerTotalScore);
	}

	@Override
	public long getCelkovyCas()
	{
		return (Long)getOption(PexesoPrefOption.PlayerTotalTime);
	}

	@Override
	public int getCelkemHer()
	{
		return (Integer)getOption(PexesoPrefOption.PlayerTotalGames);
	}

	private static final PexesoPrefOption playerWinnerCardsKey = PexesoPrefOption.PlayerWinnerCards;
	@Override
	public void setUhadnuteKartyIds(SadaKaret sadaKaret, int[] uhadnuteKartyIds)
	{
		// default values (~aka: previously saved cards)
		ArrayList<PlayerWinnerCard> cards = getUhadnuteKartyIds(sadaKaret);

		// uhadnuteKartyIds -> Set<Integer>
		Set<Integer> uhadnuteKarty = new HashSet<>();
		for(int cardId : uhadnuteKartyIds)
		{
			uhadnuteKarty.add(cardId);
		}

		// recognize 'uhadnute karty'
		for(int cardId : sadaKaret.kartyIds)
		{
			if (uhadnuteKarty.contains(cardId))
			{
				int counter = cards.get(cardId).counter;
				if (counter + 1 < Character.MAX_VALUE)
				{
					counter++;
				}
				cards.set(
					cardId,
					new PlayerWinnerCard(cardId, counter)
				);
			}
		}

		// find 'max' of card.id
		int max = Integer.MIN_VALUE;
		for(PlayerWinnerCard card : cards)
		{
			if (card.id > max)
			{
				max = card.id;
			}
		}

		// transform 'counter' -> char
		char[] cardsToStore = new char[max + 1];
		for(PlayerWinnerCard card : cards)
		{
			cardsToStore[card.id] = (char) card.counter;
		}

		// store -> preferences
		PexesoPrefOption option = playerWinnerCardsKey;
		getPreferences().putString(
			String.format(option.getName(), sadaKaret.id),
			(getCelkemHer(sadaKaret) + 1)
				+ toMenuGameSeparator
				+ Base64.encode(new String(cardsToStore).getBytes())
		);
		getPreferences().flush();
	}

	@Override
	public ArrayList<PlayerWinnerCard> getUhadnuteKartyIds(SadaKaret sadaKaret)
	{
		PexesoPrefOption option = playerWinnerCardsKey;
		String storedCardsAsString = getPreferences().getString(
			String.format(option.getName(), sadaKaret.id),
			(String)option.getDefaultValue()
		);
		if (storedCardsAsString.length() > 0)
		{
			// stored value: "1234;base64"
			String[] parts = storedCardsAsString.split(toMenuGameSeparator);
			if (parts.length >= 2)
			{
				storedCardsAsString = new String(
					Base64.decode(parts[1])
				);
			}
			else
			{
				storedCardsAsString = "";
			}
		}

		// default values...
		int maxCards = MathUtils.max(sadaKaret.kartyIds);
		ArrayList<PlayerWinnerCard> cards = new ArrayList<>(maxCards);
		for(int i = 0 ; i < maxCards; i++)
		{
			int cardId = (i < sadaKaret.kartyIds.length
				? sadaKaret.kartyIds[i]
				: i
			);
			cards.add(
				new PlayerWinnerCard(cardId, 0)
			);
		}

		char[] storedCardsAsChars = storedCardsAsString.toCharArray();
		if (storedCardsAsChars.length == 0)
		{
			return cards;
		}

		// stored 'counter' -> List<PlayerWinnerCard>
		for(int cardId = 0; cardId < storedCardsAsChars.length; cardId++)
		{
			cards.set(
				cardId,
				new PlayerWinnerCard(cardId, storedCardsAsChars[cardId])
			);
		}

		return cards;
	}

	@Override
	public int getCelkemHer(SadaKaret sadaKaret)
	{
		try
		{
			PexesoPrefOption option = playerWinnerCardsKey;
			String storedCardsAsString = getPreferences().getString(
				String.format(option.getName(), sadaKaret.id),
				(String)option.getDefaultValue()
			);
			if (storedCardsAsString.length() == 0)
			{
				return 0;
			}
			// stored value: "1234;base64"
			String[] parts = storedCardsAsString.split(toMenuGameSeparator);
			if (parts.length < 1)
			{
				throw new NumberFormatException("Invalid stored value of: " + option.getName() + "!");
			}
			return Integer.valueOf(parts[0]);
		}
		catch(NumberFormatException e)
		{
			return 0;
		}
	}

	@Override
	public int[] getPlayerPerNpc(HracPocitac npc)
	{
		return getPlayerPerOption(PexesoPrefOption.PlayerPerNpc);
	}

	@Override
	public int[] getPlayerPerBoard(HraciPlocha hraciPlocha)
	{
		return getPlayerPerOption(PexesoPrefOption.PlayerPerBoard);
	}

	@Override
	public int[] getPlayerPerCardSet(SadaKaret sadaKaret)
	{
		return getPlayerPerOption(PexesoPrefOption.PlayerPerCardSet);
	}

	private int[] getPlayerPerOption(PexesoPrefOption option)
	{
		try
		{
			String value = (String)getOption(option);
			String[] parts = value.split(toPlayerStatsSeparator);
			return new int[] {
				Integer.valueOf(parts[0]),
				Integer.valueOf(parts[1])
			};
		}
		catch(Exception e)
		{
			return new int[] {0, 0};
		}
	}

	public class PlayerWinnerCard
	{
		public final int id;
		public final int counter;
		PlayerWinnerCard(int id, int counter)
		{
			this.id = id;
			this.counter = counter;
		}

		@Override
		public String toString()
		{
			return "{id: " + id + ", counter: " + counter + "}";
		}
	}
}
