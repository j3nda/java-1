package com.gde.luzanky.pexeso.resources;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;
import com.gde.common.resources.CommonResources;
import com.gde.common.resources.GdeFontsManager;
import com.gde.common.resources.GdeAssetManager;
import com.gde.common.resources.IFontsManager;
import com.gde.luzanky.pexeso.resources.preferences.IPexesoPreferencies;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies;

public class ResourcesManager
extends GdeAssetManager
{
	private static final String KARTY_RESOURCE = "karty/{0}/{1}.png";
	public static final String BACK_ID = "back";
	private final Map<SadaKaret, Map<String, Texture>> nacteneKarty;
	private final IFontsManager fontsManager;
	private final PexesoPreferencies preferencies;
	private boolean isDisposed = false;
	private final BitFontMaker2Configuration fontConfiguration;

	public ResourcesManager()
	{
		nacteneKarty = new HashMap<>();
		fontsManager = new GdeFontsManager();
		preferencies = new PexesoPreferencies();
		fontConfiguration = new BitFontMaker2Configuration(CommonResources.Font.born2bSporty16x16);
	}

	public IPexesoPreferencies getPreferencies()
	{
		return preferencies;
	}

	@Override
	public synchronized <T> T get(String fileName, Class<T> type)
	{
		if (!isLoaded(fileName, type))
		{
			load(fileName, type);
			finishLoading();
		}
		return super.get(fileName, type);
	}

	public void loadKarty(SadaKaret sada)
	{
		if (nacteneKarty.containsKey(sada))
		{
			return;
		}

		// nacteni 'back' karticky
		Map<String, Texture> nacteneKarty = new HashMap<>();
		nacteneKarty.put(
			BACK_ID,
			get(
				MessageFormat.format(KARTY_RESOURCE, sada.id, BACK_ID),
				Texture.class
			)
		);

		// nacteni cele sady
		for(int id : sada.kartyIds)
		{
			nacteneKarty.put(
				id + "",
				get(
					MessageFormat.format(KARTY_RESOURCE, sada.id, String.format("%02d", id)),
					Texture.class
				)
			);
		}
		this.nacteneKarty.put(
			sada,
			nacteneKarty
		);
	}

	public Map<String, Texture> get(SadaKaret sada)
	{
		loadKarty(sada);
		return nacteneKarty.get(sada);
	}

	public void disposeKarty()
	{
		for(SadaKaret sada : nacteneKarty.keySet())
		{
			disposeKarty(sada);
		}
	}

	public void disposeKarty(SadaKaret sada)
	{
		if (!nacteneKarty.containsKey(sada))
		{
			return;
		}
		Map<String, Texture> toDispose = nacteneKarty.get(sada);
		for(Entry<String, Texture> entry : toDispose.entrySet())
		{
			if (!containsAsset(entry.getValue()))
			{
				continue;
			}
			String resourceId = null;
			try
			{
				resourceId = MessageFormat.format(
					KARTY_RESOURCE,
					sada.id,
					String.format("%02d", Integer.valueOf(entry.getKey()))
				);
			}
			catch(NumberFormatException e)
			{
				resourceId = MessageFormat.format(KARTY_RESOURCE, sada.id, entry.getKey());
			}
			unload(resourceId);
		}
	}

	public BitmapFont loadFont(FontEnum font, int size)
	{
		return fontsManager.loadFont(font.getResourceId(), size);
	}

	public BitmapFont getFont(FontEnum font, int size)
	{
		return fontsManager.getFont(font.getResourceId(), size);
	}

	public void disposeFont()
	{
		fontsManager.dispose();
	}

	@Override
	public synchronized void dispose()
	{
		if (isDisposed)
		{
			return;
		}
		isDisposed = true;
		super.dispose();
		disposeFont();
		disposeKarty();
	}

	public BitFontMaker2Configuration getFontConfiguration()
	{
		return fontConfiguration;
	}
}
