package com.gde.luzanky.pexeso.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.TintableRegionDrawable;
import com.gde.common.utils.GraphicUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.HraciPlocha;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.Napoveda;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac.JmenaHracu;
import com.gde.luzanky.pexeso.menu.OptionsButton.BoardSizeOptionsButton;
import com.gde.luzanky.pexeso.menu.OptionsButton.HintOptionsButton;
import com.gde.luzanky.pexeso.resources.FontEnum;
import com.gde.luzanky.pexeso.resources.FontSize;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.SadaKaret;


class MenuControlsTable
extends Table
{
	private final ResourcesManager resources;
	private final float width23;
	private final float heightStart;
	final StartButton startButton;
	final OptionsButton cards;
	final OptionsButton opponent;
	final OptionsButton boardSize;
	final HintOptionsButton boardHints;


	MenuControlsTable(ResourcesManager resources, float width23, float heightStart)
	{
		super();
		this.resources = resources;
		this.width23 = width23;
		this.heightStart = heightStart;

		int cardsCounter = SadaKaret.findOrdinal(resources.getPreferencies().getSadaKaret());
		JmenaHracu npc = JmenaHracu.fromId(resources.getPreferencies().getNPC().id());
		HraciPlocha board = HraciPlocha.fromId(resources.getPreferencies().getHraciPlocha().id);
		Napoveda hints = Napoveda.values()[(resources.getPreferencies().hasNapoveda() ? 1 : 0)];

		startButton = new StartButton("pexeso", FontSize.MenuPexeso.size(), Color.LIGHT_GRAY, resources);
		cards = new OptionsButton(
			cardsCounter,
			SadaKaret.VSECHNO.length,
			(cardsCounter + 1) + "",
			SadaKaret.VSECHNO[cardsCounter].nazev,
			resources
		);
		opponent = new OptionsButton(
			npc.ordinal(),
			HracPocitac.JmenaHracu.values().length,
			npc.id(),
			npc.nazev(),
			resources
		);
		boardSize = new BoardSizeOptionsButton(
			board.ordinal(),
			HraciPlocha.values().length,
			formatBoardSizeIndicator(board, hints),
			board.nazev(),
			resources
		);
		boardHints = new HintOptionsButton(hints, resources);

		createLayout(this);
	}

	String formatBoardSizeIndicator(HraciPlocha boardSize, Napoveda hints)
	{
		return boardSize.id + hints.id();
	}

	private Table createLayout(Table wrapper)
	{
		// player-info
		wrapper
			.add(createPlayerInfo(Color.LIGHT_GRAY, GraphicUtils.setAlpha(Color.LIGHT_GRAY, 0.4f))) // TODO: XHONZA: playerInfo && stats
			.colspan(3)
			.height(heightStart / 2f)
			.align(Align.right)
		;
		wrapper.row();

		// grow-placeholder
		wrapper
			.add(new Actor())
			.colspan(3)
			.grow()
		;
		wrapper.row();

		// sada-karet
		wrapper
			.add(cards.option)
			.height(heightStart / 2f)
			.align(Align.right)
			.expandX()
		;
		wrapper
			.add(cards.arrow)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper
			.add(cards.indicator)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper.row();

		// obtiznost (~pocitac)
		wrapper
			.add(opponent.option)
			.height(heightStart / 2f)
			.align(Align.right)
			.expandX()
		;
		wrapper
			.add(opponent.arrow)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper
			.add(opponent.indicator)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper.row();

		// hraci-plocha
		wrapper
			.add(createHraciPlochaWithHints())
			.height(heightStart / 2f)
			.align(Align.right)
			.expandX()
		;
		wrapper
			.add(boardSize.arrow)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper
			.add(boardSize.indicator)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper.row();

		// grow-placeholder
		wrapper
			.add(new Actor())
			.colspan(3)
			.grow()
		;
		wrapper.row();

		// start-button
		wrapper
			.add(new Actor())
			.expandX()
		;
		wrapper
			.add(startButton)
			.colspan(2)
			.width(width23)
			.height(heightStart)
		;
		wrapper.row();
		wrapper.align(Align.bottomRight);

		return wrapper;
	}

	private Table createHraciPlochaWithHints()
	{
		Table wrapper = new Table();

		wrapper
			.add(boardHints.optionWrapper)
			.height(heightStart / 2f)
		;
		wrapper
			.add(boardHints.arrow)
			.align(Align.center)
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper
			.add(boardSize.option)
			.height(heightStart / 2f)
			.width(boardSize.option.getWidth() * 1.05f)
		;
		wrapper.row();
		if (PexesoGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Actor createPlayerInfo(Color fgColor, Color bgColor)
	{
		Table wrapper = new Table();
		wrapper.setBackground(new TintableRegionDrawable(bgColor));

		int score = resources.getPreferencies().getCelkoveSkore();
		float iconSize = Gdx.graphics.getHeight() / 8;
		Image icon = new Image(new Texture(Gdx.files.internal("player.png")));
		icon.setAlign(Align.center);
		icon.setSize(iconSize, iconSize);
		icon.setColor(fgColor);

		Table wrapperIcon = new Table();
		wrapperIcon
			.add(icon)
			.width(iconSize)
			.height(iconSize)
		;
		wrapperIcon.row();

		wrapper
			.add(new LabelWithAlign(
				" " + (score < 0 ? "noob" : (score == 0 ? "score" : score) ) + " ",
				Align.right,
				FontSize.MenuPlayer,
				fgColor,
				resources
			))
			.height(heightStart / 2f)
			.expandX()
		;
		wrapper
			.add(new LabelWithAlign("P1", Align.center, FontSize.MenuPlayer, fgColor, resources))
			.width(width23 / 2f)
			.height(heightStart / 2f)
		;
		wrapper
			.add(wrapperIcon)
			.width(width23 / 2f)
			.height(heightStart / 2f)
			.align(Align.center)
		;
		wrapper.row();

		if (PexesoGame.isDebug())
		{
			wrapper.debug();
			wrapperIcon.debug();
		}
		return wrapper;
	}

	private class LabelWithAlign
	extends Label
	{
		public LabelWithAlign(CharSequence text, int align, FontSize size, Color color, ResourcesManager resources)
		{
			super(
				text,
				new LabelStyle(
					resources.getFont(FontEnum.DEFAULT, size.size()),
					color
				)
			);
			setAlignment(align);
		}
	}
}
