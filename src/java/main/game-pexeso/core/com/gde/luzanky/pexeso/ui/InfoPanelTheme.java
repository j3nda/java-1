package com.gde.luzanky.pexeso.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.gde.common.resources.CommonResources;

/** barevne schema */
class InfoPanelTheme
{
	// happy colors
	// -- https://colorhunt.co/palettes/happy
	static class Summary
	{
		static final Color buttonBgColor = new Color(0xFFCA03ff); // yellow
		static final String buttonBg = CommonResources.Tint.square16x16;
		static final float timeClockDuration = 0.05f;
		static final Color timeFontColor = Color.BLACK;
		static final Color roundFontColor = Color.RED;
		static final int height = Gdx.graphics.getHeight() / 8;
	}
	static class Player
	{
		/** pozadi aktivniho hrace (~zvyrazneni) */
		static final Color activeBgColor = new Color(0xFF4C03ff); // red
		static final Color activeFgColor = Color.BLACK;

		/** pozadi neaktivniho hrace (~ztmaveni) */
		static final Color inactiveBgColor = new Color(0xCFA300ff); // yellow(darker)
		static final Color inactiveFgColor = new Color(0x00000033);

		/** vyska uhadnutych karet */
		static final float cardHeight = Gdx.graphics.getHeight() / 7f;
	}

	String formatTime(float sec)
	{
		return formatTime(sec, FormatTimeType.Human);
	}

	enum  FormatTimeType { Digital, Human, HumanNoob };
	static String formatTime(float sec, FormatTimeType type)
	{
		switch(type)
		{
			case Digital:
			{
				int hours = (int) (sec / 3600);
				int minutes = (int) ((sec % 3600) / 60);
				int seconds = (int) (sec % 60);

				return String.format("%02d:%02d:%02d", hours, minutes, seconds);
			}
			case Human:
			case HumanNoob:
			{
				int hours = (int) (sec / 3600);
				int minutes = (int) ((sec % 3600) / 60);
				int seconds = (int) (sec % 60);

				if (hours > 0)
				{
					return (type == FormatTimeType.HumanNoob
						? "noob"
						: hours + "h" + minutes + "m" + seconds + "s"
					);
				}
				else
				if (minutes > 0)
				{
					return minutes + "m" + seconds + "s";
				}
				else
				if (seconds > 0)
				{
					return seconds + "s";
				}
				return "";
			}
			default:
				throw new RuntimeException("Invalid type!");
		}
	}
}
