package com.gde.luzanky.pexeso.hra;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.hraci.pocitac.ChovaniHrace;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;

/**
 * {@link HerniLogika} realizuje celou hru;
 * a informuje grafiku {@link IHerniGrafika} o tom, co se prave deje
 */
public class HerniLogika
extends Actor
{
	private volatile int pocetOtoceni;
	/** 2d pole karet pexesa */
	private Karta[][] hraciKarty;
	private final IHerniGrafika grafika;
	/** pole hracu, kteri hraji hru */
	private final Hrac[] hraci;
	/** index 'aktualne' hrajici hrac (~ukazuje do pole {@link HerniLogika#hraci}) */
	private volatile int playerIndex;
	private volatile boolean konecHry;
	/** odehrany cas od zacatku hry v [s] */
	public float odehranyCas;
	/** odehrane kolo od zacatku hry */
	public int odehraneKolo;
	/** odehrane karty v tahu */
	private final FifoQueue<Karta> odehraneKarty = new FifoQueue<>(2);

	// TODO: XHONZA: nastaveni: celkovyCas na tah; celkovyCas na hru
	protected HerniLogika(Karta[][] karty, IHerniGrafika grafika, Hrac[] hraci)
	{
		this.hraciKarty = karty;
		this.grafika = grafika;
		this.hraci = hraci;
	}

	/** vraci true, pokud je Hrac na tahu; jinak false */
	synchronized boolean jsemNaTahu(Hrac hrac)
	{
		if (konecHry)
		{
			return false;
		}
		return (getHrajiciHrac().equals(hrac) && pocetOtoceni < 2);
	}

	/** obecne vykonani herni logiky (~v kazdem render snimku) */
	@Override
	public void act(float delta)
	{
		// TODO: HRA: pricitani casu: celkovy cas hry
		odehranyCas += delta;

		// TODO: HRA: pricitani casu: prave hrajicimu hraci
		getHrajiciHrac().odehranyCasCelkem += delta;
		getHrajiciHrac().odehranyCasTah += delta;
	}

	/** uskutecni tah hrace */
	void tahClovek(Karta karta)
	{
		tah(karta, 0f);
	}

	/** uskutecni tah hrace s moznosti cekani pred 'zmenHrace', kdyz je tah neuspesny */
	private void tah(Karta karta, float pauzaPredKoncemTahu)
	{
		pocetOtoceni++;
		if (pocetOtoceni > 2)
		{
			return;
		}

		// zapamatuju si odehranou kartu -> vlozim ji do fronty
		// - a informuju grafiku, ze hraju kartu...
		// - a zahraju kartu -> otocim
		odehraneKarty.add(karta);
		grafika.onTah(getHrajiciHrac(), karta);

		karta.otoc(); // TODO: XHONZA: LOGIKA: toto by mela delat logika grafika jen reflektuje stav!
		printKarty("odehraneKarty");

		// TODO: HRA: cekani po otoceni karty, @see: ChovaniHrace.pauzaOtoceniKarta[1,2] /pouze pocitac!/
		if (pocetOtoceni < 2)
		{
			// informuju: grafiku, ze tah je neuplny a hrac ma pokracovat..
			grafika.onTah_NeuplnyPokracuj(getHrajiciHrac(), karta);
			return;
		}
		odehraneKolo++;

		// najdu predchozi otocenou kartu
		Karta predchoziKarta = odehraneKarty.prev();

		// hledam dvojici 'otocenych' karet v 2d-pole
		int sirka = hraciKarty.length;
		int vyska = hraciKarty[0].length;
		for (int x = 0; x < sirka; x++)
		{
			for (int y = 0; y < vyska; y++)
			{
				// mam stejnou dvojici karet?
				if (karta.id == hraciKarty[x][y].id && hraciKarty[x][y].jeVidet && karta != hraciKarty[x][y])
				{
					// hura! nasel jsem obe karty!
					naselJsemDvojiciKaret(karta, hraciKarty[x][y]);

					// nastal konec hry?
					konecHry = nastalKonecHry();
					if (konecHry)
					{
						grafika.onKonecHry(getViteznyHrac());
					}
					else
					{
						// pokud jsem pocitac, tak zahraju 'dalsi' tah...
						tahPocitac(getHrajiciHrac());
					}
					// a koncim...
					return;
				}
			}
		}

		// TODO: HRA: cekani pred koncem tahu, @see: ChovaniHrace.pauzaPredKoncemTahu /pouze pocitac!/

		// informuju:
		// - ze tah nebyl uspesny (grafika, logika)
		// - uberu body hrace a informuju grafiku
		// - zmenim hrace a informuju grafiku
		onTah_FAILED(predchoziKarta, karta);

		int body = -1 * (int)(karta.body * 0.1f);
		getHrajiciHrac().combo = 0;
		getHrajiciHrac().celkoveBody += body; // TODO: XHONZA: body x hra -> winConditions, resp. chovaniHry!
		if (getHrajiciHrac().celkoveBody < 0)
		{
			getHrajiciHrac().celkoveBody = 0;
		}
		else
		{
			grafika.onZmenaSkore(
				getHrajiciHrac(),
				-1 * (int)(karta.body * 0.1f)
			);
		}
		// TODO: XHONZA: toto pryc! hrac ma definovane cekani a toto se bude delat jinak!
		// (zmenit hrace muzu az v okamziku, kdy mi dobehne grafika na komplet otoceni karet zpet!)
		// na prave zahranou kartu pridam akci:
		// - pockej
		// - otoc vsechny karty zpet (~protoze tah byl neuspesny)
		// - zmen hrace
		karta.addAction(Actions.delay(
			1.5f, // [sec]
			Actions.run(new Runnable()
			{
				@Override
				public void run()
				{
					// nenasel jsem kartu...
					// vse otocim zpet... (~tj. otocim otocene :)
					for (int x = 0; x < hraciKarty.length; x++)
					{
						for (int y = 0; y < hraciKarty[0].length; y++)
						{
							if (hraciKarty[x][y].jeVidet && hraciKarty[x][y].isVisible())
							{
								hraciKarty[x][y].otoc();
							}
						}
					}
					pocetOtoceni = 0;
					zmenHrace();
				}
			})
		));
	}

	private void onTah_FAILED(Karta karta1, Karta karta2)
	{
		odehraneKarty.clear();

		// informuju: grafiku, ze tah nebyl uspesny
		grafika.onTah_FAILED(getHrajiciHrac(), karta1, karta2);

		// informuju: logiku, ze tah nebyl uspesny (~abych mohl ovladat chovani hracu)
		for(Hrac hrac : hraci)
		{
			if (hrac instanceof IHerniLogika)
			{
				((IHerniLogika)hrac).onTah_FAILED(getHrajiciHrac(), karta1, karta2);
			}
		}
	}

	/** vraci true, pokud nastal konec hry; jinak vraci false */
	private boolean nastalKonecHry()
	{
		int dostupneKarty = 0;
		int sirka = hraciKarty.length;
		int vyska = hraciKarty[0].length;
		for (int x = 0; x < sirka; x++)
		{
			for (int y = 0; y < vyska; y++)
			{
				if (hraciKarty[x][y].isVisible())
				{
					dostupneKarty++;
				}
			}
		}
		if (dostupneKarty == 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * nasel jsem dvojici karet...
	 * - priradim uhadnute karty hraci na tahu
	 * - prictu skore
	 */
	private void naselJsemDvojiciKaret(Karta karta1, Karta karta2)
	{
		odehraneKarty.clear();

		Hrac hrac = getHrajiciHrac();
		hrac.uhadnuteKarty.add(karta1);
		hrac.uhadnuteKarty.add(karta2);
		hrac.combo++; // TODO: XHONZA: hrac.{combo + comboCooldown} + onComboChange -> score! (~ruzne mooody pexesa)
		hrac.celkoveBody += karta1.body;
		hrac.celkoveBody += karta2.body;

		grafika.onZmenaSkore(
			hrac,
			karta1.body + karta2.body
		);

		// odeberu karty...
		// TODO: XHONZA: HRA/logika: HerniLogika by mela mit separatni stav -> ze karta je/neni dostupna
		// (ted to ma chybu; ze nemuzu animovat grafiku; kdyz jsem (ne)nasel karty)
		karta1.hide();
		karta2.hide();
//		karta1.addAction(Actions.sequence(
//			Actions.fadeOut(0.6f),
//			Actions.run(new Runnable()
//			{
//				@Override
//				public void run()
//				{
//					synchronized (this)
//					{
//						karta1.hide();
//					}
//				}
//			})
//		));
//		karta2.addAction(Actions.sequence(
//			Actions.fadeOut(0.6f),
//			Actions.run(new Runnable()
//			{
//				@Override
//				public void run()
//				{
//					synchronized (this)
//					{
//						karta2.hide();
//					}
//				}
//			})
//		));// TODO: XHONZA: problem! jak je zmineno vyse! logika -> ovlada grafiku! takze NPC nevi, kdy skocit!

		pocetOtoceni = 0;

		// informuju: grafiku, ze tah byl uspesny
		grafika.onTah_OK(getHrajiciHrac(), karta1, karta2);

		// informuju: logiku, ze tah byl uspesny (~abych mohl ovladat chovani hracu)
		for(Hrac player : hraci)
		{
			if (player instanceof IHerniLogika)
			{
				((IHerniLogika) player).onTah_OK(hrac, karta1, karta2);
			}
		}
	}

	/** zmeni hrace a informuje grafiku */
	private synchronized void zmenHrace()
	{
		if (playerIndex < 0)
		{
			playerIndex = hraci.length - 1;
		}
		Hrac prechozi = hraci[playerIndex];
		playerIndex++;
		if (playerIndex >= hraci.length)
		{
			playerIndex = 0;
		}
		Hrac aktualni = hraci[playerIndex];

		aktualni.odehranyCasTah = 0;
		grafika.onZmenaHrace(aktualni, prechozi);
		// TODO: LOGIKA: kdyz zmenim hrace na -> NPC (~pocitac) -> mel bych provolat jeho chovani
		tahPocitac(aktualni);
	}

	private void tahPocitac(Hrac hrac)
	{
		if (hrac.jsiPocitac && hrac instanceof HracPocitac)
		{
			ChovaniHrace chovani = ((HracPocitac)hrac).tah(hraciKarty);
			debug(
				getClass().getSimpleName() + ".tahPocitac(#" + odehraneKolo + ", " + hrac.jmeno + "): "
				+ "chovani: " + chovani
			);
			if (chovani.karta1.equals(chovani.karta2))
			{
				debug(
					getClass().getSimpleName() + ".tahPocitac(#" + odehraneKolo + ", " + hrac.jmeno + "): "
					+ " WARNING: karta1 == karta2 !!!"
				);
			}
// TODO: XHONZA: pauzaOtoceniKarta*, pauzaPredKoncemTahu
//			public final float pauzaOtoceniKarta1;
//			public final float pauzaOtoceniKarta2;
//			public final float pauzaPredKoncemTahu;
			tah(chovani.karta1, 0f);
			tah(chovani.karta2, 0f);
		}
	}

	void priprav()
	{
		konecHry = false;
		playerIndex = RandomUtils.nextInt(0, hraci.length - 1);
		pocetOtoceni = 0;
		odehranyCas = 0;
		odehraneKolo = 0;
		for(Hrac player : hraci)
		{
			player.odehranyCasCelkem = 0;
			player.uhadnuteKarty.clear();
		}
		printKarty("startHry/init");
		zamichejKarty(hraciKarty);

		printKarty("startHry/zamichano");
		nastavLogickePoziceKaret(hraciKarty);
	}

	void start()
	{
		// zacatek hry: pomoci DRY zmeny hrace...
		printOddelovac("startHry/hra!");
		printOddelovac("halooo,_tady_meduuza!_priprav se,_hrajem...");
		// -- https://www.youtube.com/watch?v=OENdNCYjDb8
		playerIndex--;
		zmenHrace();
	}

	/** zamicha nahodne karty pexesa */
	private void zamichejKarty(Karta[][] karty)
	{
		// TODO: HRA: zamichej 'nahodne' karty
		int pocetMichani = (int)((karty.length * karty[0].length) * 1.5f);
		int pocetSloupcu = karty.length - 1;
		int pocetRadku = karty[0].length - 1;
		for (int i = 0; i < pocetMichani / 2; i++)
		{
			int x1 = RandomUtils.nextInt(0, pocetSloupcu);
			int y1 = RandomUtils.nextInt(0, pocetRadku);

			int x2 = RandomUtils.nextInt(0, pocetSloupcu);
			int y2 = RandomUtils.nextInt(0, pocetRadku);

			Karta tmp = karty[x1][y1];
			karty[x1][y1] = karty[x2][y2];
			karty[x2][y2] = tmp;

			debug(
				getClass().getSimpleName()
				+ ".micham(" + i + "/" + pocetMichani + "): "
				+ x1 + "," + y1
				+ " <=> "
				+ x2 + "," + y2
				+ " [" + pocetSloupcu + "x" + pocetRadku + "]"
			);
		}
	}

	/** nastavim logicke pozice karet, kdybych chtel v logice hrace hledat v okoli karty */
	private void nastavLogickePoziceKaret(Karta[][] karty)
	{
		int sirka = karty.length;
		int vyska = karty[0].length;
		for (int x = 0; x < sirka; x++)
		{
			for (int y = 0; y < vyska; y++)
			{
				karty[x][y].setLogickaPozice(x, y);
			}
		}
	}

	private void printOddelovac(String debugTag)
	{
		if (!PexesoGame.isDebug())
		{
			return;
		}
		System.out.println(
			String.format(
				"%1$" + 78 + "s",
				"[" + debugTag
			).replace(' ', '=')
		);
	}

	private void printKarty(String debugTag)
	{
		if (!PexesoGame.isDebug())
		{
			return;
		}
		int sirkaKarty = 3;
		printOddelovac(debugTag);
		for (int y = 0; y < hraciKarty[0].length; y++)
		{
			for (int x = 0; x < hraciKarty.length; x++)
			{
				System.out.print(
					String.format(
						"%1$" + sirkaKarty + "s",
						hraciKarty[x][y].id
					)
					+ (!hraciKarty[x][y].isVisible()
						? "x"
						: (hraciKarty[x][y].jeVidet ? "*" : "_")
					)
					+ (x == hraciKarty[x][y].getLogickaPozice().x ? "_" : "!")
					+ (y == hraciKarty[x][y].getLogickaPozice().y ? "_" : "!")
					+ ","
				);
			}
			System.out.println();
		}
	}

	private void debug(String message)
	{
		if (!PexesoGame.isDebug())
		{
			return;
		}
		System.out.println(message);
	}

	public Hrac[] getHraci()
	{
		return hraci;
	}

	public synchronized Hrac getHrajiciHrac()
	{
		return hraci[playerIndex];
	}

	private Hrac getViteznyHrac()
	{
		// TODO: XHONZA: winConditions
		int uhadnuteKarty = -1;
		Hrac vitez = hraci[0];
		for(Hrac hrac : hraci)
		{
			// TODO: XHONZA: winConditions: kdyz je plychta tak co???!
			if (hrac.uhadnuteKarty.size() > uhadnuteKarty)
			{
				uhadnuteKarty = hrac.uhadnuteKarty.size();
				vitez = hrac;
			}
		}
		return vitez;
	}

	/** FIFO fronta s definovanou velikosti */
	public static class FifoQueue<T>
	extends ArrayList<T>
	{
		private static final long serialVersionUID = 8414200751634789992L;
		private final int maxSize;

		public FifoQueue(int size)
		{
			maxSize = size;
		}

		@Override
		public boolean add(T item)
		{
			boolean r = super.add(item);
			if (size() > maxSize)
			{
				removeRange(0, size() - maxSize);
			}
			return r;
		}

		T prev()
		{
			return get(size() - maxSize);
		}
	}
}
