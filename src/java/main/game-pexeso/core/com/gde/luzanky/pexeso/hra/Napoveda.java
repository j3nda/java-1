package com.gde.luzanky.pexeso.hra;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.ui.PrstNapoveda;

/**
 * usnadnuje orientaci ve hre, napr:
 * - kdyz hrac dlouho vaha nad tahem a nevi -> napoveda se mu snazi pomoct
 * - kdyz se otaci karty -> napoveda, ukazuje, ktere
 */
class Napoveda
{
	private final ResourcesManager resources;
	private final Vector2 parentSize;
	/** pole ukazatelu na kartu 'rucicka' */
	private final KartaNapoveda[] ukazatelNaKartu;
	/** mame 2x karty, tj. ukazatel na 1. anebo 2. kartu v poli */
	private int ukazatelNaKartuIndex = 0;
	/** je napoveda aktivni? */
	private final boolean aktivni;

	Napoveda(boolean aktivni, ResourcesManager resources, Vector2 parentSize)
	{
		this.aktivni = aktivni;
		this.resources = resources;
		this.parentSize = parentSize;
		ukazatelNaKartu = vytvorUkazatelNaKarty(parentSize);
	}

	/** vytvori ukazatele na kartu jako napovedu (~abych vizualne videl, kam pocitac klikl) */
	private KartaNapoveda[] vytvorUkazatelNaKarty(Vector2 parentSize)
	{
		if (!aktivni)
		{
			return new KartaNapoveda[0];
		}
		KartaNapoveda[] napoveda = new KartaNapoveda[2];

		napoveda[0] = new KartaNapoveda(resources, false);
		napoveda[1] = new KartaNapoveda(resources, true);

		return napoveda;
	}

	void inicializuj(Stage stage)
	{
		// pridam 'ukazovacek' do 'stage' jako posledni (~aby byl vzdy nahore!)
		for(KartaNapoveda ukazovacek : ukazatelNaKartu)
		{
			if (ukazovacek == null)
			{
				continue;
			}
			ukazovacek.setPosition(
				(parentSize.x / 2f) - ((ukazovacek.getWidth()  * ukazovacek.getScaleX()) / 2f),
				(parentSize.y / 2f) - ((ukazovacek.getHeight() * ukazovacek.getScaleY()) / 2f)
			);
			stage.addActor(ukazovacek);
		}
	}

	void onTah(Karta karta)
	{
		if (!aktivni)
		{
			return;
		}
		KartaNapoveda ukazovacek = ukazatelNaKartu[ukazatelNaKartuIndex];
		ukazatelNaKartuIndex++;
		if (ukazatelNaKartuIndex >= ukazatelNaKartu.length)
		{
			ukazatelNaKartuIndex = 0;
		}
		ukazovacek.click(karta);
	}

	/**
	 * prst 'ukazovacek' na prave otacenou kartu
	 * (~aby vizualne bylo jasne videt, co se na herni plose deje!)
	 */
	private class KartaNapoveda
	extends PrstNapoveda
	{
		private Karta lastKarta = null;
		private final Vector2 moveToCurr = new Vector2();

		public KartaNapoveda(ResourcesManager resources, boolean flip)
		{
			super(resources, flip);
		}

		public void click(Karta karta)
		{
	        moveToCurr.set(
				(karta.getX() + ((karta.getWidth()  * karta.getScaleX()) / 2f)),
				(karta.getY() + ((karta.getHeight() * karta.getScaleY()) / 2f))
			);
//TODO: XHONZA: napoveda; klik na stejnou kartu...
	        if (lastKarta != null && lastKarta.equals(karta))
	        {
//		        addAction(
//		        	Actions.sequence(
//			        	Actions.scaleBy(+0.2f, +0.2f, 0.25f),
//			        	Actions.scaleBy(-0.2f, -0.2f, 0.25f)
//		        	)
//		    	);
	        }
	        lastKarta = karta;
	        click(moveToCurr.x, moveToCurr.y);
		}
	}
}
