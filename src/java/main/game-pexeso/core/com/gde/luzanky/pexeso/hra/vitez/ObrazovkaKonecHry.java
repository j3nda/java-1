package com.gde.luzanky.pexeso.hra.vitez;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.display.rainbow.RainbowDisplay;
import com.gde.common.graphics.shaders.PixelateShader;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.common.resources.CommonResources;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.PexesoGameConfiguration;
import com.gde.luzanky.pexeso.SdilenaObrazovka;
import com.gde.luzanky.pexeso.TypObrazovky;
import com.gde.luzanky.pexeso.hra.HerniLogika;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.menu.ObrazovkaMenu;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.SadaKaret;
import com.gde.luzanky.pexeso.ui.FlyingPexeso;
import com.gde.luzanky.pexeso.ui.InfoPanel;
import com.gde.luzanky.pexeso.ui.InfoPanel.InfoPanelConfig;
import com.gde.luzanky.pexeso.ui.KartaHrace;

public class ObrazovkaKonecHry
extends SdilenaObrazovka
{
	private final PexesoGameConfiguration nastaveniHry;
	private FlyingPexeso flyingPexeso;
	private final Hrac vitez;
	private RainbowDisplay winnerDisplay;
	private final InfoPanelConfig infoPanelConfig;
	private InfoPanel infoPanel;
	private final int odehraneKolo;
	private final float odehranyCas;
	private final Karta prvniUhadnutaKarta;

	public ObrazovkaKonecHry(
		PexesoGame parentGame,
		PexesoGameConfiguration nastaveniHry,
		Hrac vitez,
		InfoPanelConfig infoPanelConfig,
		int odehraneKolo,
		float odehranyCas,
		Karta prvniUhadnutaKarta
	)
	{
		super(parentGame, TypObrazovky.MENU);
		this.nastaveniHry = nastaveniHry;
		this.vitez = vitez;
		this.infoPanelConfig = infoPanelConfig;
		this.odehraneKolo = odehraneKolo;
		this.odehranyCas = odehranyCas;
		this.prvniUhadnutaKarta = prvniUhadnutaKarta;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.KONEC_HRY;
	}

	@Override
	public void show()
	{
		super.show();

		// - 1. uhadnuta karta! (aby bylo pozadi stejne)
		// TODO: XHONZA: winScreen/plichta ~~ if (vitez != null && vitez.uhadnuteKarty.size() > 0)
		// TODO: XHONZA: winScreen/loser!
		KartaHrace kartaHrace = (infoPanel = vytvorInfoPanel(odehraneKolo, odehranyCas))
			.findKartaHrace(vitez)
		;
		float winnerDisplayHeight = getHeight() / 2f;
		float[] winnerDisplayY = new float[] {
			// startY
			(kartaHrace.isTop
				? getHeight()           // TOP -> bottom
				: - winnerDisplayHeight // BOTTOM -> top

			),
			// finishY
			(kartaHrace.isTop
				? 0                                 // top -> BOTTOM
				: getHeight() - winnerDisplayHeight // bottom -> TOP
			),
			// likeY
			(kartaHrace.isTop
				? getHeight() * (3/4f)
				: getHeight() * (1/4f)
			)
		};

		// flying-pexeso
		getStage().addActor(
			flyingPexeso = new MyFlyingPexeso(
				resources.getResourcesManager(),
				new Rectangle(
					0,
					0,
					getWidth() - infoPanel.getWidth(),
					getHeight()
				),
				nastaveniHry.sadaKaret,
				vitez.uhadnuteKarty,
				prvniUhadnutaKarta
			)
		);

		// thumb-up [moving>>right]
		// Free Pack Smoke Cartoon Green Screen
		// -- https://www.youtube.com/watch?v=RDwIZdQQnTU&ab_channel=YoungmanYeshua2.0
		// o) srdce -> thumb-up
		// o) smoke -> thumb-down
		for(Actor thumb : vytvorThumbUp(winnerDisplayY[2], 1))
		{
			getStage().addActor(thumb);
		}

		// info-panel [fromGame/onRight]
		infoPanel.onZmenaHrace(vitez, findPredchoziHrac(vitez));
		getStage().addActor(infoPanel);

		// fireworks
		if (vitez != null && !vitez.jsiPocitac)
		{
			showFireworks();
		}

		// rainbow-jmeno [jumping]
		getStage().addActor(
			winnerDisplay = new RainbowDisplay(
				resources.getResourcesManager().getFontConfiguration(),
				new String[] { vitez.jmeno },
				new Color[] { Color.WHITE },
				Palette.Gfx.Rainbow.Default()
			)
		);
		winnerDisplay.setViewport(
			new Rectangle(
				0,
				winnerDisplayY[0],
				getWidth(),
				winnerDisplayHeight
			)
		);
		winnerDisplay.addAction(
			Actions.delay(
				0.5f,
				Actions.moveTo(0, winnerDisplayY[1], 2f, Interpolation.bounceOut)
			)
		);

		// click-listener [click>>menu]
		Actor click = new Actor();
		click.setSize(getWidth(), getHeight());
		click.setPosition(0, 0);
		click.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(
					new ObrazovkaMenu(parentGame, null)
				);
			}
		});
		getStage().addActor(click);
	}

	private Hrac findPredchoziHrac(Hrac vitez)
	{
		if (vitez == null)
		{
			return null;
		}
		for(int i = 0; i < nastaveniHry.hraci.length; i++)
		{
			if (vitez.equals(nastaveniHry.hraci[i]))
			{
				int prev = i - 1;
				if (prev < 0)
				{
					prev = nastaveniHry.hraci.length - 1;
				}
				return nastaveniHry.hraci[prev];
			}
		}
		return null;
	}

	private void showFireworks()
	{
		Color[] fireworkPalette = Palette.Hardware.ZxSpectrum.Light();
		for(int i = 0; i < RandomUtils.nextInt(2, 4); i++) // TODO: XHONZA: 1-5, based on 'something'...
		{
			Firework firework = new Firework(
				resources.getResourcesManager(),
				fireworkPalette,
				new Rectangle(
					0,
					0,
					getWidth() - infoPanel.getWidth(),
					getHeight()
				)
			);
			getStage().addActor(firework);
		}
	}

	private Actor[] vytvorThumbUp(float y, int max)
	{
		Actor[] images = new ThumbUp[max];
		for(int i = 0; i < max; i++)
		{
			float duration = 1f;
			ThumbUp thumb = new ThumbUp(
				resources.getResourcesManager(),
				duration
			);
			float initX = ((thumb.getWidth() * thumb.getScaleX()) / -2f) - (i * (thumb.getWidth() * thumb.getScaleX()) + 10);
			float targetX = infoPanel.getX() + (infoPanel.getWidth() / 2f);
			thumb.setOrigin(Align.center);
			thumb.setPosition(
				initX,
				y,
				Align.center
			);
			thumb.addAction(Actions.forever(
				Actions.sequence(
					Actions.moveTo(
						targetX,
						y - ((thumb.getHeight() *thumb.getScaleY()) / 2f),
						duration// + (i * 0.5f)
					),
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							thumb.resetPixel();
						}
					}),
					Actions.moveTo(
						initX - ((thumb.getWidth() * thumb.getScaleX()) / 2f),
						y - ((thumb.getHeight() *thumb.getScaleY()) / 2f)
					)
				)
			));
			images[i] = thumb;
		}
		return images;
	}

	private InfoPanel vytvorInfoPanel(int odehraneKolo, float odehranyCas)
	{
		InfoPanel panel = new MyInfoPanel(
			new MyHerniLogika(nastaveniHry.hraci, odehraneKolo, odehranyCas),
			resources.getResourcesManager()
		);
		panel.setSize(
			infoPanelConfig.gameRightControlsWidth,
			getHeight()
		);
		panel.setPosition(
			getWidth() - (panel.getWidth() * panel.getScaleX()),
			0
		);
		if (PexesoGame.isDebug())
		{
			panel.debug();
		}

		// onTah_OK [simulation]
		for(Hrac hrac : nastaveniHry.hraci)
		{
			for(int i = 0; i < hrac.uhadnuteKarty.size(); i = i + 2)
			{
				panel.onTah_OK(
					hrac,
					hrac.uhadnuteKarty.get(i),
					hrac.uhadnuteKarty.get(i + 1)
				);
			}
		}
		return panel;
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}

	private static class Firework
	extends AnimationActor
	{
		private final Color[] palette;
		private final Rectangle viewport;

		Firework(ResourcesManager resources, Color[] palette, Rectangle viewport)
		{
			super(0.02f, createAnimation(resources));
			this.palette = palette;
			this.viewport = viewport;

			setOrigin(Align.center);
			randomNext();
		}

		@Override
		public void act(float delta)
		{
			super.act(delta);
			if (isAnimationFinished())
			{
				//delayAction
				randomNext();
			}
		}

		void randomNext()
		{
			reset();
			setFrameDuration(
				RandomUtils.nextFloat(1f, 3f) / 100f // OK: 0.01f, 0.02f, 0.03f (0.04f uz je moc!)
			);
			float duration = (getFrameDuration() * getKeyFrames().length) * 0.37f;

			setPosition(
				RandomUtils.nextInt(viewport.x, viewport.width),
				RandomUtils.nextInt(viewport.y, viewport.height),
				Align.center
			);
			setColor(palette[RandomUtils.nextInt(0, palette.length - 1)]);
			setScale(RandomUtils.nextFloat(0.25f, 1f));

			addAction(Actions.sequence(
				Actions.alpha(1f),
				Actions.fadeOut(duration)
			));
		}

		private static TextureRegion[] createAnimation(ResourcesManager resources)
		{
			int start = 395;
			int maxFrames = 35;
			TextureRegion[] frames = new TextureRegion[maxFrames];
			for(int i = start; i < start + maxFrames; i++)
			{
				frames[i - start] = new TextureRegion(
					resources.get(
						CommonResources.Animation.frame(CommonResources.Animation.click679x903, i),
						Texture.class
					)
				);
			}
			return frames;
		}
	}

	private class ThumbUp
	extends Image
	{
		private static final int FROM_PIXEL = 8;
		private final boolean isUp;
		private final PixelateShader pixelShader;
		ThumbUp(ResourcesManager resources, float duration)
		{
			this(true, resources, duration);
		}

		ThumbUp(boolean up, ResourcesManager resources, float duration)
		{
			super(
				resources.get(
					(up ? "like-up.png" : "like-down.png"),
					Texture.class
				)
			);
			setOrigin(Align.center);
			isUp = up;
			pixelShader = new PixelateShader(FROM_PIXEL, (int)getWidth(), (int)getHeight());
//			pixelateLimit = duration / FROM_PIXEL;
		}

		void resetPixel()
		{
//			pixel = FROM_PIXEL;
//			pixelBy = -1;
//			currentTime = 0;
		}

		@Override
		public void draw(Batch batch, float parentAlpha)
		{
			ShaderProgram prevShader = batch.getShader();
			batch.setShader(pixelShader);
			super.draw(batch, parentAlpha);

			batch.flush();
			batch.setShader(prevShader);
		}

//		private float currentTime;
//		private int pixelBy = -1;
//		private int pixel = FROM_PIXEL;
//		private float pixelateLimit = 0.07f;
//		@Override
//		public void act(float delta)
//		{
//			super.act(delta);
//			currentTime += delta;
//			if (currentTime > pixelateLimit)
//			{
//				pixel += pixelBy;
//				if (Math.abs(pixel) > FROM_PIXEL || pixel < 1)
//				{
//					pixelBy *= -1;
//					pixel += pixelBy;
//				}
//				pixelShader.updatePixelate(pixel);
//				currentTime = 0;
//			}
//		}
	}

	private class MyHerniLogika
	extends HerniLogika
	{
		MyHerniLogika(Hrac[] hraci, int odehraneKolo, float odehranyCas)
		{
			super(null, null, hraci);
			this.odehraneKolo = odehraneKolo;
			this.odehranyCas = odehranyCas;
		}
	}

	private class MyInfoPanel
	extends InfoPanel
	{
		private boolean isUpdated = false;
		public MyInfoPanel(HerniLogika herniLogika, ResourcesManager resources)
		{
			super(herniLogika, resources);
			hodiny.act(herniLogika.odehranyCas);
		}

		@Override
		public void act(float delta)
		{
			if (isUpdated)
			{
				return;
			}
			super.act(delta);
			isUpdated = true;
		}
	}

	private class MyFlyingPexeso
	extends FlyingPexeso
	{
		public MyFlyingPexeso(
			ResourcesManager resources, Rectangle viewport,
			SadaKaret sadaKaret, List<Karta> uhadnuteKarty, Karta uhadnutaKarta
		)
		{
			super(resources, viewport, false);
			List<Integer> ids = new ArrayList<>();
			{
				for(Karta karta : uhadnuteKarty)
				{
					ids.add(karta.id);
				}
			}
			int size = (uhadnuteKarty.size() / 2) - 1;
			update(sadaKaret, size, size, ids);

			for(Actor actor : getChildren())
			{
				FlyingCard karta = (FlyingCard) actor;
				karta.otoc();
			}
			updateBackground(uhadnutaKarta.getCardTexture());
		}

		@Override
		protected void updateBackground()
		{
			// do nothing...
			// (i want to update 1st card in different way here)
		}
	}
}
