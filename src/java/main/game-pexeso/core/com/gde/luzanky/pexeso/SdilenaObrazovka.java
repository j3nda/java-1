package com.gde.luzanky.pexeso;

import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky} a {@link IPexesoScreenResources})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, PexesoScreenResources, PexesoGame>
{
	public SdilenaObrazovka(PexesoGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		resources.getResourcesManager().dispose();
	}
}
