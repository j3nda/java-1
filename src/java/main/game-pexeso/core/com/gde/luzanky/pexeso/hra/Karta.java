package com.gde.luzanky.pexeso.hra;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.resources.FontEnum;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

/** hraci karta pexesa */
public class Karta
extends Image
{
	/** unikatni identifikator karty, abych mohl hledat dvojice */
	public final int id;
	/** body karty, kdyz uhadnu -> prictu */
	final int body;
	/** vrchni cast karty, tj. karta neni videt -> pozadi */
	final Drawable vrchniDrawable;
	/** spodni (~skryta) cast karty, tj. karta je videt -> obrazek */
	final Drawable spodniDrawable;
	/** indikator, zda zobrazuju 'obrazek' (~spodni cast) karty */
	boolean jeVidet = false;
	private ResourcesManager resources;
	private static LabelStyle debugLabelStyle;
	private Label debugLabel;
	private final Texture cardTexture;
	private final Texture backTexture;
	/** logicka pozice karty ve hre */
	private final Vector2 logickaPozice = new Vector2();


	protected Karta(int id, int body, Texture spodniTexture, Texture vrchniTexture)
	{
		super(vrchniTexture);
		this.id = id;
		this.body = body;
		this.cardTexture = spodniTexture;
		this.backTexture = vrchniTexture;
		this.spodniDrawable = new TextureRegionDrawable(new TextureRegion(spodniTexture));
		this.vrchniDrawable = new TextureRegionDrawable(new TextureRegion(vrchniTexture));
		setSize(
			Math.max(
				spodniTexture.getWidth() ,
				vrchniTexture.getWidth()
			),
			Math.max(
				spodniTexture.getHeight(),
				vrchniTexture.getHeight()
			)
		);
//		setOrigin(Align.center); // TODO: XHONZA: abych mel [x,y] karty uprostred a delal pekne animace!
	}

	/** otoci kartu */
	protected void otoc()
	{
		jeVidet = !jeVidet;
		if (jeVidet)
		{
			setDrawable(spodniDrawable);
		}
		else
		{
			setDrawable(vrchniDrawable);
		}

		// a toto nefunugje!!!
		// -- https://stackoverflow.com/questions/21907742/card-flip-animation-in-libgdx
		float duration = 1f;
//		float x = getX();//(getWidth() * getScaleX()) / -2f;
//		float width = getWidth();// * getScaleX();
//		addAction(Actions.sequence(
//			flipOut(x, width, duration),
//			flipIn(x, width, duration)
//		));

		// TODO: vypada blbe
//		addAction(Actions.sequence(
//			Actions.parallel(
//				Actions.scaleTo(0, 1, duration), Actions.moveBy(width / 2, 0, duration)
//			),
//			Actions.parallel(
//				Actions.scaleTo(1, 1, duration), Actions.moveBy(-width / 2, 0, duration)
//			)
//		));

		// -- https://github.com/Typhon0/AnimateFX/blob/master/animatefx/src/main/java/animatefx/animation/Flip.java
		// TODO: zkusit...

		// -- https://3dtransforms.desandro.com/card-flip
		// vysvetlene jak na to...

		// 2.5D
		// -- https://xoppa.github.io/blog/a-simple-card-game/

		// -- http://www.hkprogram.com/index1/section49.html
		// scaleTo()
//		addAction(Actions.sequence(
//			Actions.scaleTo(1, 1),
//			Actions.scaleTo(0, 1, duration),
//			Actions.scaleTo(1, 1, duration)
//		));

		// -- https://www.youtube.com/watch?v=IH-Fc1Zj3dY
		// in unity...

		// -- https://stackoverflow.com/questions/28988823/is-it-possible-to-skew-actor-in-libgdx
		// skew
//		class SkewActor extends Actor {
//		    private TextureRegion tex;
//		    private Affine2 affine = new Affine2();
//
//		    public SkewActor(TextureRegion textureRegion) {
//		        this.tex = textureRegion;
//		    }
//
//		    @Override
//		    public void draw(Batch batch, float parentAlpha) {
//		        Color color = getColor();
//		        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
//
//		        affine.setToTranslation(getX(), getY());
//		        affine.shear(0.5f, 0);  // <- modify skew here
//		        batch.draw(tex,getWidth(), getHeight(), affine);
//
//		    }
//		}

		// -- https://www.aurelienribon.com/post/2012-06-tutorial-animated-grass-using-libgdx
	}

	/** skryje kartu, tj. neni vubec videt */
	void hide()
	{
		setVisible(false);
	}

	/** zobrazi kartu, tj. je videt */
	void show()
	{
		setVisible(true);
	}

	@Override
	public void setX(float x)
	{
		setPosition(x, getY());
	}

	@Override
	public void setY(float y)
	{
		setPosition(getX(), y);
	}

	@Override
	public void setPosition(float x, float y)
	{
		super.setPosition(x, y);
		if (debugLabel != null)
		{
			debugLabel.setPosition(x, y);
		}
	}

	void setResourcesManager(ResourcesManager resources)
	{
		this.resources = resources;
		if (PexesoGame.isDebug())
		{
			if (debugLabelStyle == null)
			{
				debugLabelStyle = new LabelStyle(
					resources.getFont(FontEnum.VT323, 44),
					Color.YELLOW
				);
			}
			debugLabel = new Label(formatDebug(), debugLabelStyle);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);
		if (debugLabel != null)
		{
			debugLabel.draw(batch, parentAlpha);
		}
	}

	public void setLogickaPozice(int x, int y)
	{
		logickaPozice.set(x, y);
		if (PexesoGame.isDebug())
		{
			debugLabel.setText(formatDebug());
		}
	}

	public Vector2 getLogickaPozice()
	{
		return logickaPozice;
	}

	private String formatDebug()
	{
		return id + "/" + body
			+ " [" + (int)logickaPozice.x + "," + (int)logickaPozice.y + "]"
		;
	}

	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "(" + formatDebug() + ")";
	}

	public Texture getCardTexture()
	{
		return cardTexture;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + (jeVidet ? 1231 : 1237);
		result = prime * result + ((logickaPozice == null) ? 0 : logickaPozice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Karta other = (Karta) obj;
		if (id != other.id)
		{
			return false;
		}
		if (jeVidet != other.jeVidet)
		{
			return false;
		}
		if (logickaPozice == null)
		{
			if (other.logickaPozice != null)
			{
				return false;
			}
		}
		else
		if (!logickaPozice.equals(other.logickaPozice))
		{
			return false;
		}
		return true;
	}

	private static Action flipOut(final float x, final float width, final float duration)
	{
		return new Action()
		{
			float left = duration;

			@Override
			public boolean act(float delta)
			{
				left -= delta;
				if (left <= 0)
				{
					actor.setX(x + (width / 2));
					actor.setWidth(0);
					return true;
				}
				float tmpWidth = width * (left / duration);
				actor.setX(x + ((width / 2) - (tmpWidth / 2)));
				actor.setWidth(tmpWidth);
				return false;
			}
		};
	}

	private static Action flipIn(final float x, final float width, final float duration)
	{
		return new Action()
		{
			float done = 0;

			@Override
			public boolean act(float delta)
			{
				done += delta;
				if (done >= duration)
				{
					actor.setX(x);
					actor.setWidth(width);
					return true;
				}
				float tmpWidth = width * (done / duration);
				actor.setX(x + ((width / 2) - (tmpWidth / 2)));
				actor.setWidth(tmpWidth);
				return false;
			}
		};
	}
}
