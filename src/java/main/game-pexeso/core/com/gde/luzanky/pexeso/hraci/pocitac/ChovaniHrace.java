package com.gde.luzanky.pexeso.hraci.pocitac;

import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.hra.Karta;

/** datova obalka pro ruzne 'chovani hracu */
public class ChovaniHrace
{
	public enum Typ
	{
		/** nahodne chovani: vybira karty ciste nahodne */
		Nahodne,
	}
	/** pauza, nez hrac otoci 1. kartu */
	public final float pauzaOtoceniKarta1;
	/** pauza, nez hrac otoci 2. kartu */
	public final float pauzaOtoceniKarta2;
	/** 1. karta, kterou budu otacet */
	public final Karta karta1;
	/** 2. karta, kterou budu otacet */
	public final Karta karta2;
	/** pauza pred koncem tahu (~abych mel moznost videt odkryte karty) */
	public final float pauzaPredKoncemTahu;

	ChovaniHrace(
		float pauzaOtoceniKarta1, Karta karta1,
		float pauzaOtoceniKarta2, Karta karta2
	)
	{
		this(pauzaOtoceniKarta1, karta1, pauzaOtoceniKarta2, karta2, RandomUtils.nextFloat(1f, 2f));
	}

	ChovaniHrace(
		float pauzaOtoceniKarta1, Karta karta1,
		float pauzaOtoceniKarta2, Karta karta2,
		float pauzaPredKoncemTahu
	)
	{
		this.pauzaOtoceniKarta1 = pauzaOtoceniKarta1;
		this.karta1 = karta1;
		this.pauzaOtoceniKarta2 = pauzaOtoceniKarta2;
		this.karta2 = karta2;
		this.pauzaPredKoncemTahu = pauzaPredKoncemTahu;
	}

	@Override
	public String toString()
	{
		return "["
			+ "karta1[" + (int)karta1.getLogickaPozice().x + "," + (int)karta1.getLogickaPozice().y + "]"
				+ "(" + karta1.id + ", " + pauzaOtoceniKarta1 + "s)"
			+ ", karta2[" + (int)karta2.getLogickaPozice().x + "," + (int)karta2.getLogickaPozice().y + "]"
				+ "(" + karta2.id + ", " + pauzaOtoceniKarta2 + "s)"
			+ ", konecTahu(" + pauzaPredKoncemTahu + "s)"
			+ "]"
		;
	}
}
