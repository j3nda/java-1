package com.gde.luzanky.pexeso.resources;

import com.badlogic.gdx.Gdx;

public enum FontSize
{
	MenuPlayer(Gdx.graphics.getHeight() / 6),
	MenuItems(Gdx.graphics.getHeight() / 6),
	MenuItemsHints(Gdx.graphics.getHeight() / 10),
	MenuPexeso(Gdx.graphics.getHeight() / 10),

	GameTotalTime(Gdx.graphics.getHeight() / 15),
	GameTotalRound(Gdx.graphics.getHeight() / 20),
	GamePlayerName(Gdx.graphics.getHeight() / 10),
	GamePlayerScore(Gdx.graphics.getHeight() / 8),
	GamePlayerTime(Gdx.graphics.getHeight() / 15),
	GamePlayerCards(Gdx.graphics.getHeight() / 20),
	;
	private final int size;
	private FontSize(int size)
	{
		this.size = size;
	}
	public int size()
	{
		return size;
	}
}
