package com.gde.luzanky.pexeso.resources;

import com.gde.common.resources.CommonResources;

public enum FontEnum
{
	VT323(CommonResources.Font.vt323),
	;
	public static final FontEnum DEFAULT = FontEnum.VT323;
	private final String resource;
	private FontEnum(String resource)
	{
		this.resource = resource;
	}
	String getResourceId()
	{
		return resource;
	}
	static String UUID(FontEnum font, int size)
	{
		return font.ordinal() + ":" + size;
	}
}
