package com.gde.luzanky.pexeso.hra;

import com.gde.luzanky.pexeso.hraci.Hrac;

public interface IHerniLogika
{
	/** zavola se, kdyz je tah uspesny, tj. dvojice karet je uhadnuta spravne */
	void onTah_OK(Hrac hrac, Karta karta1, Karta karta2);

	/** zavola se, kdyz je tah neuspesny, tj. hrac neuhadl dvojici karet spravne */
	void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2);
}
