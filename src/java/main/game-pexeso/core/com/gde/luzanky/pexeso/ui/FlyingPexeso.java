package com.gde.luzanky.pexeso.ui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.SadaKaret;

public class FlyingPexeso
extends Group
{
	private final ResourcesManager resources;
	private SadaKaret sadaKaret;
	private final Rectangle viewport;
	private static final Vector2 randomFlip = new Vector2(2f, 10f);
	private static final Vector2 randomPosition = new Vector2(1f, 8f);
	private static final Vector2 randomRotate = new Vector2(1f, 3f);
	private static final Rectangle randomScale = new Rectangle(
		0.2f, 1.5f, // scale
		1f, 4f // duration
	);
	private final Background background;
	private Map<String, Texture> cardsTexture;
	protected final boolean flippable;


	public FlyingPexeso(ResourcesManager resources, Rectangle viewport)
	{
		this(resources, viewport, true);
	}

	protected FlyingPexeso(ResourcesManager resources, Rectangle viewport, boolean flippable)
	{
		this.resources = resources;
		this.viewport = viewport;
		this.flippable = flippable;
		setPosition(viewport.x, viewport.y);
		setSize(viewport.width, viewport.height);
//		createCards(RandomUtils.nextInt(16, 23), viewport);
		// TODO: XHONZA: mohlo by to reflektovat pocet her/?vyher? (~a pozadi by mohlo cyklit image)
//		createCards(1, viewport);
		background = new Background(resources);
		background.setSize(viewport.width, viewport.height);
	}

	public void update(SadaKaret sadaKaret, int minCards, int maxCards)
	{
		update(sadaKaret, minCards, maxCards, null);
	}

	public void update(SadaKaret sadaKaret, int minCards, int maxCards, List<Integer> cardIds)
	{
		List<Integer> flyingCards = new ArrayList<>();
		if (cardIds != null)
		{
			flyingCards.addAll(
				new HashSet<Integer>(cardIds)
			);
		}
		this.sadaKaret = sadaKaret;
		cardsTexture = resources.get(sadaKaret);
		int max = (flyingCards.size() == 0 ? RandomUtils.nextInt(minCards, maxCards) : flyingCards.size());

		updateBackground();
		if (getChildren().size == 0)
		{
			createCards(max, viewport, cardsTexture, flyingCards);
			addAction(Actions.fadeIn(0.5f));
			return;
		}
		addAction(Actions.sequence(
			Actions.fadeOut(0.5f),
			Actions.run(new Runnable()
			{
				@Override
				public void run()
				{
					synchronized (this)
					{
						clear();
						createCards(max, viewport, cardsTexture, flyingCards);
						addAction(Actions.fadeIn(0.5f));
					}
				}
			})
		));
	}

	protected void updateBackground()
	{
		updateBackground(
			cardsTexture.get(
				sadaKaret.kartyIds[RandomUtils.nextInt(0, sadaKaret.kartyIds.length - 1)] + ""
			)
		);
	}

	protected void updateBackground(Texture kartaTexture)
	{
		background.setVisible(true);
		background.aktualizuj(kartaTexture);
	}

	private void createCards(int max, Rectangle viewport, Map<String, Texture> cards, List<Integer> cardIds)
	{
		Texture cardBack = cards.get(ResourcesManager.BACK_ID);
		for(int i = 0; i < max; i++)
		{
			int cardId;
			if (cardIds.size() > 0)
			{
				cardId = cardIds.remove(0);
			}
			else
			{
				cardId = sadaKaret.kartyIds[RandomUtils.nextInt(0, sadaKaret.kartyIds.length - 1)];
			}
			Texture cardTexture = cards.get(cardId + "");
			if (cardTexture == null)
			{
				if (PexesoGame.isDebug())
				{
					System.out.println("WARNING: "
						+ getClass().getSimpleName()
						+ ".createCards(" + cardId + "): texture is missing!"
					);
				}
				continue;
			}
			FlyingCard card = new FlyingCard(
				cardId,
				cardTexture,
				cardBack,
				this,
				flippable
			);
			addActor(card);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		background.draw(batch, parentAlpha);
		super.draw(batch, parentAlpha);
		background.drawTvOverlayAtLast(batch, parentAlpha);
	}

	protected class FlyingCard
	extends Karta
	{
		FlyingCard(int id, Texture spodniTexture, Texture vrchniTexture, FlyingPexeso parent, boolean flippable)
		{
			super(id, 0, spodniTexture, vrchniTexture);
			setOrigin(Align.center);
			setPosition(
				RandomUtils.nextInt((int)parent.viewport.x, (int)(parent.viewport.x + parent.viewport.width)),
				RandomUtils.nextInt((int)parent.viewport.y, (int)(parent.viewport.y + parent.viewport.height))
			);
			addAction(Actions.parallel(
				Actions.forever(new CardMoveTo(parent)),
				Actions.forever(new CardScaleTo(parent)),
				Actions.forever(new CardRotateBy(parent)),
				(flippable && RandomUtils.nextInt(0, 100) < 50
					? Actions.forever(new CardFlip(this, parent))
					: new EmptyAction()
				)
			));
		}

		@Override
		public void otoc()
		{
			super.otoc();
		}

		private class CardRotateBy
		extends RotateByAction
		{
			private final FlyingPexeso parent;

			CardRotateBy(FlyingPexeso parent)
			{
				this.parent = parent;
				setScale(
					RandomUtils.nextFloat(parent.randomScale.x, parent.randomScale.y)
				);
				toRandomRotate();
			}

			private void toRandomRotate()
			{
				setAmount(
					RandomUtils.nextFloat(0f, +180f) * (RandomUtils.nextInt(0, 100) % 2 == 0 ? +1 : -1)
				);
				setDuration(
					RandomUtils.nextFloat(parent.randomRotate.x, parent.randomRotate.y)
				);
			}

			@Override
			public boolean act(float delta)
			{
				boolean complete = super.act(delta);
				if (complete)
				{
					toRandomRotate();
				}
				return complete;
			}
		}

		private class CardScaleTo
		extends ScaleToAction
		{
			private final FlyingPexeso parent;
			private float lastScale = -1;
			private boolean scaleUp = (RandomUtils.nextInt(0, 100) % 2 == 0);

			CardScaleTo(FlyingPexeso parent)
			{
				setScale(
					RandomUtils.nextFloat(parent.randomScale.x, parent.randomScale.y)
				);
				this.parent = parent;
				this.scaleUp = (getScaleX() >= 1);
				this.lastScale = getScaleX();
			}

			private void toRandomScale()
			{
				float scale;
				scaleUp = !scaleUp;
				if (scaleUp)
				{
					do
					{
						scale = RandomUtils.nextFloat(parent.randomScale.x, parent.randomScale.y);
					}
					while(scale > lastScale);
				}
				else
				{
					do
					{
						scale = RandomUtils.nextFloat(parent.randomScale.x, parent.randomScale.y);
					}
					while(scale < lastScale);
				}
				lastScale = scale;
				setScale(scale);
				setDuration(
					RandomUtils.nextFloat(parent.randomScale.width, parent.randomScale.height)
				);
			}

			@Override
			public boolean act(float delta)
			{
				boolean complete = super.act(delta);
				if (complete)
				{
					toRandomScale();
				}
				return complete;
			}
		}

		private class CardMoveTo
		extends MoveToAction
		{
			private final FlyingPexeso parent;

			CardMoveTo(FlyingPexeso parent)
			{
				this.parent = parent;
				toRandomPosition();
			}

			private void toRandomPosition()
			{
				setPosition(
					RandomUtils.nextFloat(parent.viewport.x, parent.viewport.x + parent.viewport.width),
					RandomUtils.nextFloat(parent.viewport.x, parent.viewport.y + parent.viewport.height)
				);
				setDuration(
					RandomUtils.nextFloat(parent.randomPosition.x, parent.randomPosition.y)
				);
			}

			@Override
			public boolean act(float delta)
			{
				boolean complete = super.act(delta);
				if (complete)
				{
					toRandomPosition();
				}
				return complete;
			}
		}

		private class CardFlip
		extends TemporalAction
		{
			private final FlyingCard card;
			private final FlyingPexeso parent;

			CardFlip(FlyingCard card, FlyingPexeso parent)
			{
				this.card = card;
				this.parent = parent;
				toRandomDuration();
			}

			private void toRandomDuration()
			{
				setDuration(
					RandomUtils.nextFloat(parent.randomFlip.x, parent.randomFlip.y)
				);
			}

			@Override
			protected void update(float percent)
			{
				if (percent == 1f)
				{
					card.otoc();
				}
			}
		}

		private class EmptyAction
		extends Action
		{
			@Override
			public boolean act(float delta)
			{
				return true;
			}
		}
	}

	private class Background
	extends PozadiHraciPlochy
	{
		public Background(ResourcesManager resources)
		{
			super(resources);
		}

		@Override
		protected void drawTvOverlay(Batch batch, float parentAlpha)
		{
			// keep it empty! i want to draw it as last one!
		}

		protected void drawTvOverlayAtLast(Batch batch, float parentAlpha)
		{
			super.drawTvOverlay(batch, parentAlpha);
		}

		public void aktualizuj(Texture cardTexture)
		{
			updateTimestamp();
			setDrawable(
				new TextureRegionDrawable(
					new TextureRegion(
						cardTexture
					)
				)
			);
			setVisible(true);
		}
	}
}
