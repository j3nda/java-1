package com.gde.luzanky.pexeso.hraci.pocitac;

import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hra.IHerniLogika;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.hraci.Hrac;

/**
 * {@link HracPocitac}, chovani: nahodne vybira karty
 * (neni chytry... sazi na nahodu)
 */
public class HracPocitac
extends Hrac
implements IHerniLogika
{
	private static final String[] jmenaPocitacu = new String[] {
		"IBM 360", "Atari", "C64", "ZX81", "Amiga"
	};
	public enum JmenaHracu {
		/** debug: hra pocitac x pocitac (~pro ladici ucely) */
		Hrnec(true),
		Tonda,
		Roman,
		;
		public final boolean random;
		private JmenaHracu()
		{
			this(false);
		}
		private JmenaHracu(boolean random)
		{
			this.random = random;
		}
		public String nazev()
		{
			return name();
		}
		public String id()
		{
			return nazev().charAt(0) + "";
		}
		public static JmenaHracu fromNazev(String nazev)
		{
			String name = nazev.toLowerCase();
			for(JmenaHracu value : values())
			{
				if (value.nazev().toLowerCase().equals(name))
				{
					return value;
				}
			}
			return null;
		}
		public static JmenaHracu fromId(String id)
		{
			for(JmenaHracu value : values())
			{
				if (value.id().equals(id))
				{
					return value;
				}
			}
			return values()[0];
		}
	}
	/** pocet odehranych kol (~tahu ve hre) */
	protected int odehraneKolo = 1;


	public HracPocitac()
	{
		this(jmenaPocitacu[RandomUtils.nextInt(0, jmenaPocitacu.length - 1)]);
	}

	public HracPocitac(String jmeno)
	{
		super(jmeno, true);
	}

	/** nahodne vybira karty a nijak nekontroluje co uz vybral. nepamatuje si nic. */
	public ChovaniHrace tah(Karta[][] hraciKarty)
	{
		int pocetIteraci = 0;
		int pocetSloupcu = hraciKarty.length - 1;
		int pocetRadku = hraciKarty[0].length - 1;
		int x1, y1, x2, y2;
		Karta karta1, karta2;
		do
		{
			x1 = RandomUtils.nextInt(0, pocetSloupcu);
			y1 = RandomUtils.nextInt(0, pocetRadku);
			x2 = RandomUtils.nextInt(0, pocetSloupcu);
			y2 = RandomUtils.nextInt(0, pocetRadku);

			karta1 = hraciKarty[x1][y1];
			karta2 = hraciKarty[x2][y2];
			debug(
				getClass().getSimpleName() + ".tah(#" + odehraneKolo + ", i: " + pocetIteraci + "): "
				+ "karta1[" + x1 + "][" + y1 + "](" + karta1.id + ", " + karta1.isVisible() + ")"
				+ ", karta2[" + x2 + "][" + y2 + "](" + karta2.id + ", " + karta2.isVisible() + ")"
			);
			pocetIteraci++;
		}
		while( (x1 == x2 && y1 == y2) || !karta1.isVisible() || !karta2.isVisible() );

		return new ChovaniHrace(
			cekej(1),
			karta1,
			cekej(2),
			karta2
		);
	}

	/** vrati pocet [s] jako cekani... 'poradiKarty' muze ovlivnovat toto cekani */
	protected float cekej(int poradiKarty)
	{
		return RandomUtils.nextFloat(0.5f, 1f);
	}

	@Override
	protected void debug(String message)
	{
		if (PexesoGame.isDebug())
		{
			System.out.println(message);
		}
	}

	@Override
	public void onTah_OK(Hrac hrac, Karta karta1, Karta karta2)
	{
		debug("logika.onTah_OK(" + karta1.id + ", " + karta2.id + "): " + hrac.jmeno + " [" + getClass().getSimpleName() + "]");
		odehraneKolo++;

		zapomenKarty(karta1, karta2);
	}

	@Override
	public void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2)
	{
		debug("logika.onTah_FAILED(" + karta1.id + ", " + karta2.id + "): " + hrac.jmeno + " [" + getClass().getSimpleName() + "]");
		odehraneKolo++;

		zapamatujSiKartu(karta1);
		zapamatujSiKartu(karta2);
	}

	protected void zapamatujSiKartu(Karta karta)
	{
		// pocitac je hloupy a nic si nepamatuje...
	}

	protected void zapomenKarty(Karta karta1, Karta karta2)
	{
		// pocitac je hloupy a neumi zapominat...
	}
}
