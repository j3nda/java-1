package com.gde.luzanky.pexeso.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.gde.common.graphics.shaders.BlackAndWhiteShader;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

public class PozadiHraciPlochy
extends Image
implements Disposable
{
	private Long updateTimestamp = null;
	private static final BlackAndWhiteShader bwShader = new BlackAndWhiteShader(0.5f);
	protected final TextureRegion tvOverlay;
	protected final Rectangle tvOverlaySize = new Rectangle();
	private Karta karta;


	public PozadiHraciPlochy(ResourcesManager resources)
	{
		tvOverlay = createTvOverlay(resources, CommonResources.Gde.tv1280x960);
	}

	private TextureRegion createTvOverlay(ResourcesManager resources, String resourceName)
	{
		Texture textureToRepeat = resources.get(resourceName, Texture.class);
		textureToRepeat.setWrap(
			TextureWrap.Repeat,
			TextureWrap.Repeat
		);
		TextureRegion repeatedTexture = new TextureRegion(textureToRepeat);
		repeatedTexture.setRegion(
			0,
			0,
			getWidth(), // width  after repeat
			getHeight() // height after repeat
		);

		return repeatedTexture;
	}

	protected void updateTimestamp()
	{
		updateTimestamp = System.currentTimeMillis();
	}

	public void aktualizuj(Karta karta)
	{
		if (updateTimestamp != null)
		{
			return;
		}
		updateTimestamp();
		setDrawable(
			new TextureRegionDrawable(
				new TextureRegion(
					karta.getCardTexture()
				)
			)
		);
		this.karta = karta;
	}

	public Karta getKarta()
	{
		return karta;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (!isVisible())
		{
			return;
		}
		drawImage(batch, parentAlpha);
		drawTvOverlay(batch, parentAlpha);
	}

	protected void drawImage(Batch batch, float parentAlpha)
	{
		ShaderProgram prevShader = batch.getShader();
		batch.setShader(bwShader);
		super.draw(batch, parentAlpha);

		batch.flush();
		batch.setShader(prevShader);
	}

	protected void drawTvOverlay(Batch batch, float parentAlpha)
	{
		batch.draw(
			tvOverlay,
			tvOverlaySize.x,
			tvOverlaySize.y
		);
		batch.flush();
	}

	@Override
	public void dispose()
	{
		bwShader.dispose();
	}

	@Override
	public void setSize(float width, float height)
	{
		super.setSize(width, height);
		if (tvOverlay != null)
		{
			tvOverlaySize.width = width;
			tvOverlaySize.height = height;
			tvOverlay.setRegion(
				0,
				0,
				tvOverlaySize.width, // width  after repeat
				tvOverlaySize.height // height after repeat
			);
		}
	}

	public void setTvOverlay(Rectangle rectangle)
	{
		if (tvOverlay != null)
		{
			tvOverlaySize.set(rectangle);
			if (tvOverlaySize.x > 0)
			{
				tvOverlaySize.x *= -1;
			}
			tvOverlay.setRegion(
				0,
				0,
				tvOverlaySize.width, // width  after repeat
				tvOverlaySize.height // height after repeat
			);
		}
	}
}
