package com.gde.luzanky.pexeso.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.ui.AnimationActor;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hra.HerniLogika;
import com.gde.luzanky.pexeso.hra.IHerniLogika;
import com.gde.luzanky.pexeso.hra.Karta;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.resources.FontEnum;
import com.gde.luzanky.pexeso.resources.FontSize;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.ui.InfoPanelTheme.FormatTimeType;

/** informacni panel: pro hru 2x hracu! */
public class InfoPanel
extends Table
implements IHerniLogika
{
	private ResourcesManager resources;
	private HracUiConfig aktivniHrac;
	private HracUiConfig neaktivniHrac;
	private TextureRegionDrawable celkoveInfoBg;
	static LabelStyles labelStyles;
	KartaHrace[] kartaHrace;
	private Label celkovyCas;
	private Label celkoveKolo;
	private Table celkoveInfo;
	protected AnimationActor hodiny;
	private final HerniLogika herniLogika;
	private float currentTime = updateTimeLimit + 1;
	/** casova prodleva pro aktualizaci celkoveho casu */
	private static final float updateTimeLimit = 0.5f;


	public InfoPanel(HerniLogika herniLogika, ResourcesManager resources)
	{
		super();
		this.resources = resources;
		this.herniLogika = herniLogika;
		this.kartaHrace = new KartaHrace[herniLogika.getHraci().length];
		this.labelStyles = new LabelStyles(
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GameTotalTime.size()), Color.GRAY),
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GameTotalRound.size()), Color.GRAY),
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GamePlayerName.size()), Color.GRAY),
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GamePlayerScore.size()), Color.GRAY),
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GamePlayerTime.size()), Color.GRAY),
			new LabelStyle(resources.getFont(FontEnum.DEFAULT, FontSize.GamePlayerCards.size()), Color.GRAY)
		);
		createLayout(this);
	}

	private void createLayout(Table wrapper)
	{
		createBackgrounds();
		for(int i = 0; i < herniLogika.getHraci().length; i++)
		{
			int align = (i == 0 ? Align.top : Align.bottom);
			kartaHrace[i] = new KartaHrace(
				herniLogika.getHraci()[i],
				resources,
				align,
				InfoPanelTheme.Player.cardHeight
			);
			kartaHrace[i].setBackground(neaktivniHrac.background);
			wrapper
				.add(kartaHrace[i])
				.grow()
				.align(align)
			;
			wrapper.row();
			if (i == 0)
			{
				wrapper
					.add(celkoveInfo = createCelkoveInfo())
					.fillX()
					.height(InfoPanelTheme.Summary.height)
				;
				wrapper.row();
			}
		}
		if (PexesoGame.isDebug())
		{
			wrapper.debug();
		}
	}

	private Table createCelkoveInfo()
	{
		Table wrapper = new Table();
		wrapper.setBackground(celkoveInfoBg);

		Table hodinyWrapper = new Table();
		hodinyWrapper
			.add(hodiny = createAnimovaneHodiny(InfoPanelTheme.Summary.timeClockDuration, 0.8f))
		;
		hodinyWrapper.row();

		Table infoWrapper = new Table();
		infoWrapper
			.add(celkoveKolo = new Label(formatKolo(1), labelStyles.celkoveKolo))
			.expandX()
			.align(Align.left)
		;
		celkoveKolo.setColor(InfoPanelTheme.Summary.roundFontColor);
		infoWrapper.row();
		infoWrapper
			.add(celkovyCas = new Label(formatTimeTotal(1), labelStyles.celkovyCas))
			.expandX()
			.align(Align.right)
		;
		celkovyCas.setColor(InfoPanelTheme.Summary.timeFontColor);
		infoWrapper.row();

		// clock#1 (cerne na hnedem, zajimave)
		// -- https://cdn.dribbble.com/users/241526/screenshots/954930/media/9c199720788595fed8c77ac7035ebb9d.gif
		// -- https://dribbble.com/shots/954930-Hourglass-loader/attachments/8592813?mode=media
		wrapper
			.add(hodinyWrapper)
			.align(Align.center)
		;
		wrapper
			.add(infoWrapper)
			.growX()
		;
		wrapper.row();

		if (PexesoGame.isDebug())
		{
			wrapper.debug();
			infoWrapper.debug();
			hodinyWrapper.debug();
		}
		return wrapper;
	}

	private AnimationActor createAnimovaneHodiny(float duration, float scale)
	{
		int maxFrames = 29;
		TextureRegion[] frames = new TextureRegion[maxFrames + 1];
		for(int i = 0; i <= maxFrames; i++)
		{
			frames[i] = new TextureRegion(
				resources.get(String.format("animace/hodiny/%02d.png", i), Texture.class)
			);
		}
		AnimationActor clock = new AnimationActor(duration, frames, PlayMode.LOOP);

		clock.setScale(scale);
		clock.setOrigin(Align.center);

		return clock;
	}

	private void createBackgrounds()
	{
		Pixmap pixmap = new Pixmap(1,1, Pixmap.Format.RGB565);
		pixmap.setColor(InfoPanelTheme.Player.activeBgColor);
		pixmap.fill();
		aktivniHrac = new HracUiConfig(
			InfoPanelTheme.Player.activeFgColor,
			InfoPanelTheme.Player.activeBgColor,
			new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)))
		);

		pixmap.setColor(InfoPanelTheme.Player.inactiveBgColor);
		pixmap.fill();
		neaktivniHrac = new HracUiConfig(
			InfoPanelTheme.Player.inactiveFgColor,
			InfoPanelTheme.Player.inactiveBgColor,
			new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)))
		);

		pixmap.setColor(InfoPanelTheme.Summary.buttonBgColor);
		pixmap.fill();
		celkoveInfoBg = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));

		pixmap.dispose();
	}

	public KartaHrace findKartaHrace(Hrac hrac)
	{
		if (hrac != null)
		{
			for(KartaHrace karta : kartaHrace)
			{
				if (karta.hrac.jmeno.equals(hrac.jmeno))
				{
					return karta;
				}
			}
		}
		return null;
	}

	public void onZmenaHrace(Hrac aktualniHrac, Hrac predchoziHrac)
	{
		KartaHrace aktualni = findKartaHrace(aktualniHrac);
		KartaHrace predchozi = findKartaHrace(predchoziHrac);
		if (predchozi != null)
		{
			predchozi.setBackground(neaktivniHrac.background);
			predchozi.setColor(neaktivniHrac.color);
		}
		if (aktualni != null)
		{
			aktualni.setBackground(aktivniHrac.background);
			aktualni.setColor(aktivniHrac.color);
		}
//		hodiny.setPlayMode(PlayMode.NORMAL); // TODO: XHONZA: dava to vizualne smysl?
//		hodiny.reset();
	}

	public void onZmenaSkore(Hrac hrac, int bodyPrirustek)
	{
		KartaHrace karta = findKartaHrace(hrac);
		karta.bodyLabel.setText(hrac.celkoveBody + "");
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		currentTime += delta;
		if (currentTime >= updateTimeLimit)
		{
			currentTime = 0;
			celkoveKolo.setText(formatKolo(herniLogika.odehraneKolo));
			celkovyCas.setText(formatTimeTotal(herniLogika.odehranyCas));

			Hrac hrajiciHrac = herniLogika.getHrajiciHrac();
			KartaHrace kartaHrace = findKartaHrace(hrajiciHrac);
			kartaHrace.odehranyCasLabel.setText(formatTimePlayer(hrajiciHrac.odehranyCasCelkem));
		}
	}

	private String formatKolo(int round)
	{
		return round + "x";
	}

	private String formatTimeTotal(float sec)
	{
		return InfoPanelTheme.formatTime(sec, FormatTimeType.HumanNoob);
	}

	private String formatTimePlayer(float sec)
	{
		return InfoPanelTheme.formatTime(sec, FormatTimeType.Human);
	}

	@Override
	public void onTah_OK(Hrac hrac, Karta karta1, Karta karta2)
	{
		findKartaHrace(hrac).onTah_OK(hrac, karta1, karta2);
	}

	@Override
	public void onTah_FAILED(Hrac hrac, Karta karta1, Karta karta2)
	{
		findKartaHrace(hrac).onTah_FAILED(hrac, karta1, karta2);
	}

	public static class InfoPanelConfig
	{
		public final float menuRightControlsWidth;
		public final float menuLeftControlsWidth;
		public final float gameRightControlsWidth;
		public final float gameLeftControlsWidth;
		public final Rectangle gameViewport;

		public InfoPanelConfig(int screenWidth, int screenHeight, float startButtonWidth, float exitButtonWidth)
		{
			this.gameViewport = new Rectangle();
			gameViewport.width = screenHeight;
			gameViewport.height = screenHeight;

			// menu controls
			this.menuRightControlsWidth = startButtonWidth;
			this.menuLeftControlsWidth = exitButtonWidth;

			// game controls
			int gameFreeSpace = screenWidth - screenHeight;
			if (gameFreeSpace >= startButtonWidth)
			{
				float rightLimit = startButtonWidth * 1.3f;
				if (gameFreeSpace >= rightLimit)
				{
					gameRightControlsWidth = rightLimit;
					gameLeftControlsWidth = 0;

					gameFreeSpace -= gameRightControlsWidth + gameLeftControlsWidth;
					gameViewport.x = gameFreeSpace / 2f;
					return;
				}
			}
			gameRightControlsWidth = gameFreeSpace;
			gameLeftControlsWidth = 0;
			gameViewport.x = 0;
		}
	}

	/** datova obalka, ktera dri nastaveni 'barev' hrace */
	public class HracUiConfig
	{
		public final Color color;
		public final Color bgColor;
		public final TextureRegionDrawable background;
		private HracUiConfig(Color fgColor, Color bgColor, TextureRegionDrawable background)
		{
			this.color = fgColor;
			this.bgColor = bgColor;
			this.background = background;
		}
	}

	public static class LabelStyles
	{
		public final LabelStyle celkovyCas;
		public final LabelStyle celkoveKolo;
		public final LabelStyle hrac;
		public final LabelStyle hracSkore;
		public final LabelStyle hracCas;
		public final LabelStyle hracKarty;
		LabelStyles(
			LabelStyle celkovyCas,
			LabelStyle celkoveKolo,
			LabelStyle hrac,
			LabelStyle hracSkore,
			LabelStyle hracCas,
			LabelStyle hracKarty
		)
		{
			this.celkovyCas = celkovyCas;
			this.celkoveKolo = celkoveKolo;
			this.hrac = hrac;
			this.hracSkore = hracSkore;
			this.hracCas = hracCas;
			this.hracKarty = hracKarty;
		}
	}
}
