package com.gde.luzanky.pexeso.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.TintableRegionDrawable;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.pexeso.resources.FontEnum;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

class LabelWithBackground
extends Label
{
	private Color bgColor;
	private final boolean padLeft;


	LabelWithBackground(String text, int size, Color textColor, Color bgColor, ResourcesManager resources)
	{
		this(text, size, textColor, bgColor, resources, false);
	}

	LabelWithBackground(String text, int size, Color textColor, Color bgColor, ResourcesManager resources, boolean padLeft)
	{
		super(
			text,
			new LabelStyle(
				resources.getFont(FontEnum.DEFAULT, size),
				textColor
			)
		);
		this.bgColor = bgColor;
		this.padLeft = padLeft;
		getStyle().background = new TintableRegionDrawable(
			new TextureRegion(resources.get(CommonResources.Tint.square16x16, Texture.class)),
			bgColor
		);
		setAlignment(Align.center);
		if (padLeft)
		{
			setText(text);
		}
	}

	Color getBackgroundColor()
	{
		return bgColor;
	}

	void setBackgroundColor(Color color)
	{
		bgColor = color;
		((TintableRegionDrawable)getStyle().background).setColor(color);
	}

	@Override
	public void setText(CharSequence text)
	{
		super.setText(
			(padLeft ? " " : "") + text
		);
	}
}
