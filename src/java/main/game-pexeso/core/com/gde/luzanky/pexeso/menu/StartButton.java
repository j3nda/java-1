package com.gde.luzanky.pexeso.menu;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.resources.CommonResources;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.resources.FontEnum;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

class StartButton
extends Table
{
	private final int maxStripes;
	private final ResourcesManager resources;
	private final Color[] colors;
	private volatile int colorIndex;
	private final Texture tintTexture;
	private final String text;
	private final int textSize;
	private final Color textColor;
	private Table stripesTable;
	private Group labels;


	StartButton(String text, int textSize, Color textColor, ResourcesManager resources)
	{
		this(
			text,
			textSize,
			textColor,
			0.7f,
			0.2f,
			Palette.Gfx.Rainbow.Repeated().length,
			Palette.Gfx.Rainbow.Repeated(),
			0.1f,
			resources
		);
	}

	private StartButton(
		String text, int textSize, Color textColor,
		float textDuration, float textDelay,
		int maxStripes, Color[] colors, float stripeDuration,
		ResourcesManager resources
	)
	{
		super();
		this.text = text;
		this.textSize = textSize;
		this.textColor = textColor;
		this.maxStripes = maxStripes;
		this.colors = Arrays.copyOf(colors, colors.length);
		this.resources = resources;
		this.tintTexture = resources.get(CommonResources.Tint.square16x16, Texture.class);
		createLayout(this);
		createStripesAnimation(stripeDuration);
		animateText(textSize, textDuration, textDelay);
	}

	private void createStripesAnimation(float duration)
	{
		addAction(
			Actions.forever(
				Actions.delay(
					duration,
					Actions.run(new Runnable()
					{
						@Override
						public void run()
						{
							animateStripesToRight();
						}
					})
				)
			)
		);
	}

	private Table createLayout(Table wrapper)
	{
		Stack stack = new Stack();

		stack.add(stripesTable = createStripesTable(maxStripes, colors));
		stack.add(labels = createLabel(text, textSize, textColor));

		int border = 5;
		Pixmap pixmap = new Pixmap(1, 1, Format.RGB565);
		pixmap.setColor(Color.BLACK);
		pixmap.fill();
		setBackground(
			new TextureRegionDrawable(
				new TextureRegion(
					new Texture(pixmap)
				)
			)
		);
		pixmap.dispose();

		wrapper.add(new Actor()).height(border);
		wrapper.row();

		wrapper.add(new Actor()).width(border);
		wrapper
			.add(stack)
			.grow()
		;
		wrapper.add(new Actor()).width(border);
		wrapper.row();

		wrapper.add(new Actor()).height(border);
		wrapper.row();

		return wrapper;
	}

	private Group createLabel(String text, int size, Color color)
	{
		Table wrapper = new Table();
		LabelStyle style = new LabelStyle(
			resources.getFont(FontEnum.DEFAULT, size),
			color
		);
		for(int i = 0; i < text.length(); i++)
		{
			Label label = new Label(
				text.charAt(i) + "",
				style
			);
			label.setAlignment(Align.center);
			wrapper.add(label);
		}
		wrapper.row();
		wrapper.align(Align.center);
		if (PexesoGame.isDebug())
		{
			wrapper.debug();
		}
		return wrapper;
	}

	private Table createStripesTable(int stripes, Color[] colors)
	{
		Table wraper = new Table();
		colorIndex = 0;
		for(int i = 0; i < stripes; i++)
		{
			Color color = colors[colorIndex];
			Image image = new Image(tintTexture);
			image.setColor(color);
			wraper
				.add(image)
				.grow()
			;
			colorIndex++;
			if (colorIndex >= colors.length)
			{
				colorIndex = 0;
			}
		}
		wraper.row();

		return wraper;
	}

	private synchronized void animateStripesToRight()
	{
		int max = stripesTable.getCells().size;
		for(int i = max - 2; i >= 0 ; i--)
		{
			Image next = (Image)stripesTable.getCells().get(i + 1).getActor();
			Image prev = (Image)stripesTable.getCells().get(i + 0).getActor();
			next.setColor(prev.getColor());
		}
		Image first = (Image)stripesTable.getCells().get(0).getActor();
		colorIndex++;
		if (colorIndex >= colors.length)
		{
			colorIndex = 0;
		}
		first.setColor(colors[colorIndex]);
	}

	private void animateText(int size, float duration, float delay)
	{
		for(int i = 0; i < labels.getChildren().size; i++)
		{
			Actor actor = labels.getChildren().get(i);
			actor.addAction(
				Actions.sequence(
					Actions.delay(i * delay),
					Actions.forever(
						Actions.sequence(
							Actions.moveBy(0, size / 2f, duration / 2f),
							Actions.moveBy(0, size * -1, duration),
							Actions.moveBy(0, size / 2f, duration / 2f)
						)
					)
				)
			);
		}
	}
}
