package com.gde.luzanky.pexeso;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.screens.ScreenResources;
import com.gde.luzanky.pexeso.resources.ResourcesManager;
import com.gde.luzanky.pexeso.resources.preferences.PexesoPreferencies;


public class PexesoScreenResources
extends ScreenResources
{
	private final ResourcesManager resources;


	public PexesoScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		this.resources = new ResourcesManager();
	}

	public ResourcesManager getResourcesManager()
	{
		return resources;
	}

	public PexesoPreferencies getPreferencies()
	{
		return (PexesoPreferencies)resources.getPreferencies();
	}

	@Override
	public void dispose()
	{
		super.dispose();
		resources.dispose();
	}
}
