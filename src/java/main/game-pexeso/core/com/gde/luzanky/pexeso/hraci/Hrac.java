package com.gde.luzanky.pexeso.hraci;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.hra.Karta;

public abstract class Hrac
{
	public final String jmeno;
	/** datova struktura {@link Collection}.{@link List}, budeme probirat v java-2/oop/kolekce */
	public List<Karta> uhadnuteKarty;
	public int celkoveBody;
	/** pocet po sobe uspesnych tahu (~ma vliv na nove udelene body) */
	public float combo;
	public float odehranyCasCelkem;
	public float odehranyCasTah;
	public final boolean jsiPocitac;

	protected Hrac(String jmeno, boolean jsiPocitac)
	{
		this.jmeno = jmeno;
		this.jsiPocitac = jsiPocitac;
		uhadnuteKarty = new ArrayList<>();
		celkoveBody = 0;
		combo = 0;
		odehranyCasCelkem = 0;
		odehranyCasTah = 0;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmeno == null) ? 0 : jmeno.hashCode());
		result = prime * result + (jsiPocitac ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		Hrac other = (Hrac) obj;
		if (jmeno == null)
		{
			if (other.jmeno != null)
			{
				return false;
			}
		}
		else
		if (!jmeno.equals(other.jmeno))
		{
			return false;
		}
		if (jsiPocitac != other.jsiPocitac)
		{
			return false;
		}
		return true;
	}

	protected void debug(String message)
	{
		if (PexesoGame.isDebug())
		{
			System.out.println(message);
		}
	}
}
