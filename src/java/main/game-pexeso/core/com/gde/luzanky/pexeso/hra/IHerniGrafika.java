package com.gde.luzanky.pexeso.hra;

import com.gde.luzanky.pexeso.hraci.Hrac;

interface IHerniGrafika
extends IHerniLogika
{
	/** zavola se, kdyz se zahraje karta */
	void onTah(Hrac hrac, Karta karta);

	/** zavola se, kdyz je tah neuplny, tj. hrac pokracuje v hledani spravne karty */
	void onTah_NeuplnyPokracuj(Hrac hrac, Karta karta);

	/** zavola se, kdyz nastane konec hry */
	void onKonecHry(Hrac vitez);

	/** zavola se, kdyz je tah neuspesny a je potreba zmenit hrace */
	void onZmenaHrace(Hrac aktualniHrac, Hrac predchoziHrac);

	/** zavola se, kdyz se hraci zmenim body */
	void onZmenaSkore(Hrac hrac, int bodyPrirustek);
}
