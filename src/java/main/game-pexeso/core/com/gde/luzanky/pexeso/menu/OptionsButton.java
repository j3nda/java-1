package com.gde.luzanky.pexeso.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gde.common.graphics.colors.TintableRegionDrawable;
import com.gde.luzanky.pexeso.PexesoGame;
import com.gde.luzanky.pexeso.PexesoGameConfiguration.Napoveda;
import com.gde.luzanky.pexeso.resources.FontSize;
import com.gde.luzanky.pexeso.resources.ResourcesManager;

class OptionsButton
{
	final Label option;
	final Label arrow;
	final Label indicator;
	private static final int size = FontSize.MenuItems.size();
	private static final Color fgColor = Color.RED;
	private static final Color bgColor = Color.YELLOW;
	private int counter;
	private final int max;
	protected final ResourcesManager resources;


	OptionsButton(int counter, int max, String indicatorText, String optionText, ResourcesManager resources)
	{
		this(counter, max, indicatorText, optionText, resources, size, size, size);
	}

	protected OptionsButton(
		int counter, int max,
		String indicatorText, String optionText,
		ResourcesManager resources,
		int sizeOption, int sizeArrow, int sizeIndicator
	)
	{
		this.counter = (counter >= 0 ? counter : 0);
		this.max = max;
		this.resources = resources;
		this.option = createOption(optionText, sizeOption, fgColor, bgColor, true);
		this.arrow = createArrow("<", sizeArrow, fgColor, bgColor);
		this.indicator = createIndicator(indicatorText, sizeIndicator, fgColor, bgColor);
	}

	protected Label createOption(String value, int size, Color fgColor, Color bgColor, boolean padLeft)
	{
		if (size == 0)
		{
			return null;
		}
		Label label = new LabelWithBackground(value, size, fgColor, bgColor, resources, padLeft);
		label.setName("option");
		label.setAlignment(Align.right);
		return label;
	}

	protected Label createArrow(String value, int size, Color fgColor, Color bgColor)
	{
		if (size == 0)
		{
			return null;
		}
		Label label = new LabelWithBackground(value, size, fgColor, bgColor, resources);
		label.setName("arrow");
		return label;
	}

	protected Label createIndicator(String value, int size, Color fgColor, Color bgColor)
	{
		if (size == 0)
		{
			return null;
		}
		Label label = new LabelWithBackground(value, size, fgColor, bgColor, resources);
		label.setName("indicator");
		return label;
	}

	int onClick(InputEvent event)
	{
		Actor actor = event.getTarget();
		if (actor != null && "indicator".equals(actor.getName()))
		{
			return onClickPrev();
		}
		else
		{
			return onClickNext();
		}
	}

	private int onClickPrev()
	{
		counter--;
		if (counter < 0)
		{
			counter = max - 1;
		}
		return counter;
	}

	private int onClickNext()
	{
		counter++;
		if (counter >= max)
		{
			counter = 0;
		}
		return counter;
	}

	int getCounter()
	{
		return counter;
	}

	void addListener(ClickListener listener)
	{
		if (option instanceof Actor)
		{
			option.addListener(listener);
		}
		if (arrow instanceof Actor)
		{
			arrow.addListener(listener);
		}
		if (indicator instanceof Actor)
		{
			indicator.addListener(listener);
		}
	}

	static class BoardSizeOptionsButton
	extends OptionsButton
	{		protected BoardSizeOptionsButton(int counter, int max, String indicatorText, String optionText, ResourcesManager resources)
		{
			super(counter, max, indicatorText, optionText, resources);
		}

		@Override
		protected Label createOption(String value, int size, Color fgColor, Color bgColor, boolean padLeft)
		{
			return super.createOption(value, size, fgColor, bgColor, false);
		}
	}

	static class HintOptionsButton
	extends OptionsButton
	{
		final Table optionWrapper;

		HintOptionsButton(Napoveda hint, ResourcesManager resources)
		{
			super(
				hint.ordinal(),
				Napoveda.values().length,
				null,
				hint.nazev(),
				resources,
				FontSize.MenuItemsHints.size(), // optionSize
				FontSize.MenuItems.size(),      // arrowSize
				0                               // optionSize
			);
			optionWrapper = createLayout(hint);
			update(hint);
		}

		private Table createLayout(Napoveda hint)
		{
			Table wrapper = new Table();
			wrapper.setBackground(
				new TintableRegionDrawable(((LabelWithBackground) option).getBackgroundColor())
			);
			wrapper
				.add(option)
			;
			wrapper.row();
			if (PexesoGame.isDebug())
			{
				wrapper.debug();
			}
			return wrapper;
		}

		void update(Napoveda hint)
		{
			option.setText("hinty: " + hint.nazev());
		}

		@Override
		void addListener(ClickListener listener)
		{
			optionWrapper.addListener(listener);
			arrow.addListener(listener);
		}
	}
}
