package com.gde.luzanky.pexeso;

import java.util.HashSet;
import java.util.Set;

import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pexeso.hraci.Hrac;
import com.gde.luzanky.pexeso.hraci.HracClovek;
import com.gde.luzanky.pexeso.hraci.pocitac.HracPocitac;
import com.gde.luzanky.pexeso.hraci.pocitac.HracRoman;
import com.gde.luzanky.pexeso.hraci.pocitac.HracTonda;
import com.gde.luzanky.pexeso.resources.SadaKaret;

/** datova obalka pro nastaveni hry (~gameplay) */
public class PexesoGameConfiguration
{
	public final SadaKaret sadaKaret;
	public final Hrac[] hraci;
	public final int maxSloupcu;
	public final int maxRadek;
	public final HraciPlocha hraciPlocha;
	public final boolean napoveda;
	public enum HraciPlocha {
		Mala("S", 4, 4),
		Stredni("N", 6, 6),
		Velka("L", 8, 8),
		;
		public final String id;
		public final int sloupcu;
		public final int radek;

		private HraciPlocha(String id, int sloupcu, int radek)
		{
			this.id = id;
			this.sloupcu = sloupcu;
			this.radek = radek;
		}
		public String nazev()
		{
			return sloupcu + "x" + radek;
		}
		public static HraciPlocha fromId(String id)
		{
			for(HraciPlocha value : values())
			{
				if (value.id.equals(id))
				{
					return value;
				}
			}
			return values()[0];
		}
	}
	public enum Napoveda
	{
		Off("Off"),
		On("On "),
		;
		private final String nazev;
		private Napoveda(String nazev)
		{
			this.nazev = nazev;
		}
		public String id()
		{
			return ordinal() + "";
		}
		public String nazev()
		{
			return nazev;
		}
		public static Napoveda fromId(String id)
		{
			return Napoveda.values()[Integer.valueOf(id)];
		}
	}


	public PexesoGameConfiguration(SadaKaret sadaKaret, int playerIndex, HraciPlocha hraciPlocha, boolean napoveda)
	{
		this(sadaKaret, createPlayer(playerIndex), hraciPlocha, napoveda);
	}

	/** debug purposes: PC x PC */
	public PexesoGameConfiguration(SadaKaret sadaKaret, HraciPlocha hraciPlocha, boolean napoveda)
	{
		this.sadaKaret = sadaKaret;
		this.hraciPlocha = hraciPlocha;
		this.napoveda = napoveda;
		this.maxRadek = hraciPlocha.radek;
		this.maxSloupcu = hraciPlocha.sloupcu;

		// nahodni 'pocitacovi hraci'
		// vyuzivam 'Set' jako strukturu (~mnozina), abych predesel nezadoucimu opakovani stejnych prvku!
		Set<HracPocitac> players = new HashSet<>();
		do
		{
			players.add(
				createPlayer(
					RandomUtils.nextInt(0, HracPocitac.JmenaHracu.values().length - 1)
				)
			);
		}
		while(players.size() < 2);

		// konverze 'Set' struktury do pole[]
		hraci = players.toArray(new Hrac[players.size()]);
	}

	private PexesoGameConfiguration(
		SadaKaret sadaKaret,
		HracPocitac pocitac,
		HraciPlocha obtiznost,
		boolean napoveda
	)
	{
		this.sadaKaret = sadaKaret;
		this.hraciPlocha = obtiznost;
		this.napoveda = napoveda;
		this.maxRadek = obtiznost.radek;
		this.maxSloupcu = obtiznost.sloupcu;
		this.hraci = new Hrac[] {
			new HracClovek("P1"),
			pocitac,
		};
	}

	private static HracPocitac createPlayer(int index)
	{
		switch(HracPocitac.JmenaHracu.values()[index])
		{
			case Tonda:
			{
				return new HracTonda();
			}
			case Roman:
			{
				return new HracRoman();
			}
			case Hrnec:
			default:
			{
				return new HracPocitac();
			}
		}
	}
}
