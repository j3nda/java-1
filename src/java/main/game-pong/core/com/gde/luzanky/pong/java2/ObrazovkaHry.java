package com.gde.luzanky.pong.java2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gde.common.graphics.display.CelkoveSkore;
import com.gde.luzanky.pong.PongGame;
import com.gde.luzanky.pong.PongInputProcessor;
import com.gde.luzanky.pong.PongInputProcessor.IPongMovement;
import com.gde.luzanky.pong.SdilenaObrazovka;
import com.gde.luzanky.pong.TypObrazovky;

/** herni obrazovka, zde se odehrava nase pong hra! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements IPongMovement
{
	/** maximalni pohyb palky v [%] vuci velikosti obrazovky (vyuzito v {@link PongInputProcessor}) */
	private static final float MAX_POHYB_PALKY = 0.25f;
	/** predchozi {@InputProcessor}, abych si ho uchoval a nastavil zpatky po skonceni hry */
	private InputProcessor prevInputProcessor;
	private CelkoveSkore celkoveSkore;
	private LeticiObjekt levaPalka;
	private LeticiObjekt pravaPalka;
	private LeticiObjekt balonek;

	public ObrazovkaHry(PongGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA_OOP;
	}

	/** 'renderScreen' se vola neustale, tj. 30fps */
	@Override
	protected void renderScreen(float delta)
	{
		// renderuje vsechny objekty, ktere jsou ve 'stage'
		// (pro ukazky: java-2/oop, uvazujeme, ze se vola .act() metoda!)
		renderScreenStage(delta);
	}

	@Override
	public void show()
	{
		// zapamatuju si predchozi 'input-processor'
		prevInputProcessor = Gdx.input.getInputProcessor();

		// zavolam 'show() rodice'
		super.show();

		// a spustim hru
		startHry();

		// vytvorim a nastavim novy 'input-processor', ktery umi hybat palkou...
		setInputProcessor(
			new PongInputProcessor(
				this,
				new Vector2(
					Gdx.graphics.getWidth()  * MAX_POHYB_PALKY,
					Gdx.graphics.getHeight() * MAX_POHYB_PALKY
				)
			)
		);
	}

	@Override
	public void hide()
	{
		Gdx.input.setInputProcessor(prevInputProcessor);
		super.hide();
	}

	/** 'startHry' se zavola na zacatku; ovlada vytvoreni a nastaveni vseho co hra potrebuje */
	private void startHry()
	{
		// vytvorim objekt 'CelkoveSkore' a nastavim 'viewport', kde se ma zobrazit
		celkoveSkore = new CelkoveSkore(
			resources.getFontsManager().getBitFontConfiguration(),
			new Color(0x23252Eaa)
		);
		celkoveSkore.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() / 4f,
				(Gdx.graphics.getHeight() /2f) - (Gdx.graphics.getHeight() / 4f),
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight() / 2f
			)
		);
		// vycistim celou 'stage' a pridam do ni 'celkoveSkore'
		getStage().clear();
		getStage().addActor(celkoveSkore);

		// TODO: HRA: vytvor: levou a pravou palku a balonek (toto proved objektove!)
		// (muzes upravit obrazky/textury. najdes je: "(vlevo)/assets")
		levaPalka = new Palka(new Texture("paddle.png"));
		pravaPalka = new Palka(new Texture("paddle.png"));
		balonek = new Balonek(new Texture("ball.png"));

		// TODO: HRA: pridej 'objekty': levaPalka, pravaPalka a balonek do 'stage'
		// (stage je kontejner, ktery ovlada vsechny objekty ve scene, tj. klikani, vykreslovani aj
		//  pridani docilis obdobne jako 'celkoveSkore')
		getStage().addActor(levaPalka);
//		getStage().addActor(pravaPalka);
		getStage().addActor(balonek);


		// kolize...
//		levaPalka.addKolizniObjekt(balonek);
//		pravaPalka.addKolizniObjekt(balonek);
//		balonek.addKolizniObjekt(levaPalka);
//		balonek.addKolizniObjekt(pravaPalka);
	}

	@Override
	public void pohniPalkou(float rychlost)
	{
		// TODO: LOGIKA/pohyb-palkou: uprav rychlost pohybu palky pro: levaPalka (~aby se hybala; (y-osa))
		levaPalka.setY(levaPalka.getY() + rychlost);
	}
}
// TODO: xhonza: pripravit oop/skeleton, aby se to na tom dalo vysvetlovat
// TODO: xhonza: kolize/nejak elegantne vymyslet ~ v ramci java-2 (co znaji)
