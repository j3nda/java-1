package com.gde.luzanky.pong.java2;

import com.badlogic.gdx.math.Rectangle;

public class KolizeRectangle
extends Rectangle
{
	private final Rectangle prunik = new Rectangle();

	/** vraci true, pokud {@link Rectangle} ma kolizi, tj. prunik; jinak vraci false */
	public boolean intersect(Rectangle rectangle)
	{
		if (this.overlaps(rectangle))
		{
			prunik.x = Math.max(x, rectangle.x);
			prunik.width = Math.min(x + width, rectangle.x + rectangle.width) - prunik.x;
			prunik.y = Math.max(y, rectangle.y);
			prunik.height = Math.min(y + height, rectangle.y + rectangle.height) - prunik.y;
			return true;
		}
		return false;
	}
}
