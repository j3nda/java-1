package com.gde.luzanky.pong.java1.roman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.gde.common.graphics.display.CelkoveSkore;
import com.gde.common.utils.RandomUtils;
import com.gde.luzanky.pong.ObrazovkaKonecHry;
import com.gde.luzanky.pong.PongGame;
import com.gde.luzanky.pong.PongInputProcessor;
import com.gde.luzanky.pong.PongInputProcessor.IPongMovement;
import com.gde.luzanky.pong.SdilenaObrazovka;
import com.gde.luzanky.pong.TypObrazovky;

/** herni obrazovka, zde se odehrava nase pong hra! */
public class ObrazovkaHry
extends SdilenaObrazovka
implements IPongMovement
{
	/** 1. hrac (vlevo) */
	private static final int PLAYER_1 = 0;
	/** 2. hrac (vpravo) */
	private static final int PLAYER_2 = 1;
	private static final int SMER_DOPRAVA = +1;
	private static final int SMER_DOLEVA = -1;
	private static final int SMER_NAHORU = +1;
	private static final int SMER_DOLU = -1;
	/** maximalni pohyb palky v [%] vuci velikosti obrazovky (vyuzito v {@link PongInputProcessor}) */
	private static final float MAX_POHYB_PALKY = 0.25f;
	private CelkoveSkore celkoveSkore;
	private Image levaPalka;
	private Image pravaPalka;
	private Image balonek;
	private float levaPalkaSmerY = 0;
	private float pravaPalkaSmerY = +15;
	private float balonekSmerX = +20;
	private float balonekSmerY = +20;
	/** rychlost balonku (~pro pocitani smeru letu balonku) */
	private final float balonekRychlost = 20;
	/** predchozi {@InputProcessor}, abych si ho uchoval a nastavil zpatky po skonceni hry */
	private InputProcessor prevInputProcessor;
	private boolean upravChovaniPlayer2;

	public ObrazovkaHry(PongGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.HRA_FUNKCE;
	}

	/** 'renderScreen' se vola neustale, tj. 30fps */
	@Override
	protected void renderScreen(float delta)
	{
		// vyhodnoti logiku, tj.
		// - pohyb: {leva, prava}Palka, balonek
		// - kolize:
		//   - balonek x hrany-obrazovky
		//   - balonek x palky{leva, prava}
		// - score -> pricteni bodu, popr. zmeny smeru balonku v kolizi s palkou
		vyhodnotPohyb();
		vyhodnotKolize();

		vyhodnotChovaniPlayer2();

		vyhodnotScore();
		vyhodnotKonecHry(9);

		// renderuje vsechny objekty, ktere jsou ve 'stage'
		// (pro ukazky: java-1/neuvazujeme oop, tj. nepocitame s volanim .act() metod!)
		renderScreenStage(delta);
	}

	private void vyhodnotChovaniPlayer2()
	{
		if (!upravChovaniPlayer2)
		{
			// TODO: HRA/roman: muzu a nemusim se hybat
			pravaPalkaSmerY = 0;
			return;
		}

		float pravaPalkaStredY = pravaPalka.getY() + pravaPalka.getHeight() / 2f;
		float balonekStredY = balonek.getY() + balonek.getHeight() / 2f;
		float rozdilY = Math.abs(pravaPalkaStredY - balonekStredY);

		if (rozdilY > Math.abs(balonekSmerY))
		{ // chovani: dojizdi balonek
			// TODO: LOGIKA/P2/BUG: zrychlit dojizdeni, abych to vzdy pravaPalka stihla dojet
			pravaPalkaSmerY = balonekSmerY;
			if (pravaPalkaStredY > balonekStredY)
			{
				// palka(nahore) x ball(dole) --> dojezd(dolu)
				pravaPalkaSmerY = SMER_DOLU * Math.abs(pravaPalkaSmerY);
			}
			else
			{
				// palka(dole) x ball(nahore) --> dojezd(nahoru)
				pravaPalkaSmerY = SMER_NAHORU * Math.abs(pravaPalkaSmerY);
			}
		}
		else
		{ // chovani: stinuje balonek
			pravaPalka.setY(balonekStredY - pravaPalka.getHeight() / 2f);
		}
	}

	@Override
	public void show()
	{
		// zapamatuju si predchozi 'input-processor'
		prevInputProcessor = Gdx.input.getInputProcessor();

		// zavolam 'show() rodice'
		super.show();

		// a spustim hru
		startHry();

		// vytvorim a nastavim novy 'input-processor', ktery umi hybat palkou...
		setInputProcessor(
			new PongInputProcessor(
				this,
				new Vector2(
					Gdx.graphics.getWidth()  * MAX_POHYB_PALKY,
					Gdx.graphics.getHeight() * MAX_POHYB_PALKY
				)
			)
		);
	}

	@Override
	public void hide()
	{
		Gdx.input.setInputProcessor(prevInputProcessor);
		super.hide();
	}

	/** 'startHry' se zavola na zacatku; ovlada vytvoreni a nastaveni vseho co hra potrebuje */
	private void startHry()
	{
		// vytvorim objekt 'CelkoveSkore' a nastavim 'viewport', kde se ma zobrazit
		celkoveSkore = new CelkoveSkore(
			resources.getFontsManager().getBitFontConfiguration(),
			new Color(0x23252Eaa)
		);
		celkoveSkore.setViewport(
			new Rectangle(
				Gdx.graphics.getWidth() / 4f,
				(Gdx.graphics.getHeight() /2f) - (Gdx.graphics.getHeight() / 4f),
				Gdx.graphics.getWidth() / 2f,
				Gdx.graphics.getHeight() / 2f
			)
		);
		// vycistim celou 'stage' a pridam do ni 'celkoveSkore'
		getStage().clear();
		getStage().addActor(celkoveSkore);

		// TODO: HRA/start: vytvor: levou a pravou palku a balonek
		// (muzes upravit obrazky/textury. najdes je: "(vlevo)/assets")
		levaPalka = new Image(new Texture("paddle.png"));
		pravaPalka = new Image(new Texture("paddle.png"));
		balonek = new Image(new Texture("ball.png"));

		// TODO: HRA/start: pridej 'objekty': levaPalka, pravaPalka a balonek do 'stage'
		// (stage je kontejner, ktery ovlada vsechny objekty ve scene, tj. klikani, vykreslovani aj
		//  pridani docilis obdobne jako 'celkoveSkore')
		getStage().addActor(levaPalka);
		getStage().addActor(pravaPalka);
		getStage().addActor(balonek);

		// TODO: HRA/start: nastav 'pravou palku' doprava na obrazovce
		pravaPalka.setX(Gdx.graphics.getWidth() - pravaPalka.getWidth());

		// TODO: HRA/start: nastav balonek doprostred obrazovky
		balonek.setX((Gdx.graphics.getWidth()  / 2f) - (balonek.getWidth()  / 2));
		balonek.setY((Gdx.graphics.getHeight() / 2f) - (balonek.getHeight() / 2));

		// TODO: HRA/start: nastav obe palky doprostred (vysky okna)
		levaPalka.setY((Gdx.graphics.getHeight() / 2f) - (levaPalka.getHeight() / 2f));
		pravaPalka.setY((Gdx.graphics.getHeight() / 2f) - (levaPalka.getHeight() / 2f));

		// TODO: HRA/start: zarid, aby balonek po startu letel na palku PLAYER_2 (vpravo)
		balonekSmerX = balonekRychlost * +1;
		balonekSmerY = 0;
	}

	private void vyhodnotPohyb()
	{
		// TODO: LOGIKA/pohyb: zpracuj pohyb: {leva,prava}Palka
		levaPalka.setY(levaPalka.getY() + levaPalkaSmerY);
		pravaPalka.setY(pravaPalka.getY() + pravaPalkaSmerY);

		// TODO: LOGIKA/pohyb: zpracuj pohyb: balonek
		balonek.setX(balonek.getX() + balonekSmerX);
		balonek.setY(balonek.getY() + balonekSmerY);
	}

	private void vyhodnotScore()
	{
		// TODO: LOGIKA/skore: na zaklade kolizi +pricti skore, napr:
		// - balonek narazi do prave strany-obrazovky: player1(vlevo) ziskava +1 bod
		if (balonek.getX() + balonek.getWidth() > Gdx.graphics.getWidth())
		{
			celkoveSkore.addScore(+1, PLAYER_1);
		}

		// - balonek narazi do leve  strany-obrazovky: player2(vpravo) ziskava +1 bod
		if (balonek.getX() < 0)
		{
			celkoveSkore.addScore(+1, PLAYER_2);
		}
	}

	private void vyhodnotKonecHry(int maxBodu)
	{
		// TODO: LOGIKA/hra: vyhodnot konec hry, kdy
		// - vitez je ten, kdo ziska nejdrive (napr: 9 bodu)
		if (celkoveSkore.getScore(PLAYER_1) >= maxBodu)
		{
			parentGame.setScreen(
				new ObrazovkaKonecHry(
					parentGame,
					getPreviousScreen(),
					true,
					celkoveSkore
				)
			);
		}
		if (celkoveSkore.getScore(PLAYER_2) >= maxBodu)
		{
			parentGame.setScreen(
				new ObrazovkaKonecHry(
					parentGame,
					getPreviousScreen(),
					false,
					celkoveSkore
				)
			);
		}
	}

	private void vyhodnotKolize()
	{
		vyhodnotKolize_levaPalka();
		vyhodnotKolize_pravaPalka();
		vyhodnotKolize_balonekObrazovka();

		// TODO: LOGIKA/kolize: palka x balonek
		// (mel by se odrazit (uhel dopadu == uhlu odrazu))
		vyhodnotKolize_balonekLevaPalka();
		vyhodnotKolize_balonekPravaPalka();
	}

	private void vyhodnotKolize_balonekLevaPalka()
	{
		// leva-palka: prava-hrana --> odrazi se vpravo (~smer-x)
		if (
			   balonek.getX() <= levaPalka.getX() + levaPalka.getWidth() // ....................// prava-hrana
			&& balonek.getY() >= levaPalka.getY() // ...........................................// spodni-hrana
			&& balonek.getY() + balonek.getHeight() <= levaPalka.getY() + levaPalka.getHeight() // horni-hrana
		   )
		{
			// ktera pulka palky? (y-ova osa)
			if (balonek.getY() + (balonek.getHeight() / 2f) > levaPalka.getY() + (levaPalka.getHeight() / 2f))
			{ // horni
				balonekSmerY = SMER_NAHORU * Math.abs(balonekRychlost);
			}
			else
			{ // dolni
				balonekSmerY = SMER_DOLU * Math.abs(balonekRychlost);
			}
			balonekSmerX = SMER_DOPRAVA * Math.abs(balonekRychlost);
			nastavChovaniPlayer2(true);
		}
	}

	private void vyhodnotKolize_balonekPravaPalka()
	{
		// prava-palka: leva-hrana --> odrazi se vlevo (~smer-x)
		if (
			   balonek.getX() + balonek.getWidth() >= pravaPalka.getX() // .......................// leva-hrana
			&& balonek.getY() >= pravaPalka.getY() // ............................................// spodni-hrana
			&& balonek.getY() + balonek.getHeight() <= pravaPalka.getY() + pravaPalka.getHeight() // horni-hrana
		   )
		{
			// ktera pulka palky? (y-ova osa)
			if (balonek.getY() + (balonek.getHeight() / 2f) > pravaPalka.getY() + (pravaPalka.getHeight() / 2f))
			{ // horni
				balonekSmerY = SMER_NAHORU * Math.abs(balonekRychlost);
			}
			else
			{ // dolni
				balonekSmerY = SMER_DOLU * Math.abs(balonekRychlost);
			}
			balonekSmerX = SMER_DOLEVA * Math.abs(balonekRychlost);
			nastavChovaniPlayer2(false);
		}
	}

	private void vyhodnotKolize_balonekObrazovka()
	{
		// TODO: LOGIKA/kolize: balonek x 'hrany-obrazovky' (nahore, dole, vlevo, vpravo)
		// (balonek by se mel odrazit (uhel dopadu == uhlu odrazu))
		//
		// nahore
		if (balonek.getY() + balonek.getHeight() > Gdx.graphics.getHeight())
		{
			balonekSmerY = SMER_DOLU * Math.abs(balonekRychlost);
		}

		// dolu
		if (balonek.getY() < 0)
		{
			balonekSmerY = SMER_NAHORU * Math.abs(balonekRychlost);
		}

		// vpravo
		if (balonek.getX() + balonek.getWidth() > Gdx.graphics.getWidth())
		{
			balonekSmerX = SMER_DOLEVA * Math.abs(balonekRychlost);
			nastavChovaniPlayer2(false);
		}

		// vlevo
		if (balonek.getX() < 0)
		{
			balonekSmerX = SMER_DOPRAVA * Math.abs(balonekRychlost);
			nastavChovaniPlayer2(true);
		}
	}

	private void nastavChovaniPlayer2(boolean stav)
	{
		if (stav)
		{
			// nahoda: hod kostkou...
			if (RandomUtils.nextInt(1, 10) / 10f < 0.5f)
			{
				stav = false;
			}
		}
		upravChovaniPlayer2 = stav;
	}

	private void vyhodnotKolize_pravaPalka()
	{
		// TODO: LOGIKA/kolize: prava-palka x {'horni-hrana-okna' anebo 'dolni-hrana-okna'}
		if (
			   pravaPalka.getY() + pravaPalka.getHeight() > Gdx.graphics.getHeight()
			|| pravaPalka.getY() < 0 // OR ~ ANEBO
		   )
		{
			pravaPalkaSmerY = pravaPalkaSmerY * -1;
		}
		// BONUS: zarid, aby palka nikdy nevyjela z obrazovky
		if (pravaPalka.getY() + pravaPalka.getHeight() > Gdx.graphics.getHeight())
		{
			pravaPalka.setY(Gdx.graphics.getHeight() - pravaPalka.getHeight());
		}
		if (pravaPalka.getY() < 0)
		{
			pravaPalka.setY(0);
		}
	}

	private void vyhodnotKolize_levaPalka()
	{
		// NAPOVEDA:
		// 1) kdyz palka narazi do 'hrany-okna' --> zmeni smer pohybu
		// 2) kdyz 'hrac' hybe palkou --> zarazi se o 'hranu-okna'
		//
		// TODO: LOGIKA/kolize: leva-palka x 'horni-hrana-okna'
		if (levaPalka.getY() + levaPalka.getHeight() > Gdx.graphics.getHeight())
		{
			levaPalka.setY(Gdx.graphics.getHeight() - levaPalka.getHeight());
		}

		// TODO: LOGIKA/kolize: leva-palka x 'dolni-hrana-okna'
		if (levaPalka.getY() < 0)
		{
			levaPalka.setY(0);
		}
	}

	@Override
	public void pohniPalkou(float rychlost)
	{
		// TODO: LOGIKA/pohyb-palkou: uprav rychlost pohybu palky pro: levaPalka (~aby se hybala; (y-osa))
		levaPalkaSmerY = rychlost;
	}
}
// TODO: xhonza: balonek lita pod urcitym uhlem
// TODO: xhonza: balonek meni svou rychlost
// TODO: xhonza: pravaPalka ~ je AI; -- https://www.quora.com/How-does-the-AI-of-the-ping-pong-game-work-What-is-a-detailed-explanation-or-some-references-that-explain-it
