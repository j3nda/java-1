package com.gde.luzanky.pong.java2;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class LeticiObjekt
extends Image
implements IKolizeLeticihoObjektu
{
	/** rychlost pohybu (~uvedeny jako vektor), tj. drzi informaci o: "x" a "y" */
	final Vector2 rychlost = new Vector2(10, 10);
	/** kolekce {@link LeticiObjekt} pro pocitani kolizi */
	protected final Set<IKolizeLeticihoObjektu> objektyKolize = new HashSet<>();
	/** vytvorim si statickou (tj. 1x v pameti) instanci {@link IKolizeLeticihoObjektu} pro kolize s obrazovkou! */
	private static final IKolizeLeticihoObjektu kolizeObrazovky = new KolizeObrazovky();

	public LeticiObjekt(Texture texture)
	{
		super(texture);

		// pridam kolizi s obrazovou
		addKolizniObjekt(kolizeObrazovky);
	}

	/** metoda 'act()' slouzi pro ovladani 'logiky' (a je nezavisla na vykreslovani!) */
	@Override
	public void act(float delta)
	{
		super.act(delta);
		pohybLeticihoObjektu();
		kolizeLeticihoObjektu();
	}

	private void pohybLeticihoObjektu()
	{
		// TODO: LOGIKA: vyres pohyb leticiho objektu 'po obrazovce'
		if (rychlost.x != 0)
		{
			setX(getX() + rychlost.x);
		}
		if (rychlost.y != 0)
		{
			setY(getY() + rychlost.y);
		}
	}

	/** obsluhuje reseni kolizi */
	private void kolizeLeticihoObjektu()
	{
		// TODO: LOGIKA: vyres kolize (podle typu objektu), tj. uprav chovani tak, aby:
		// - 'objekt' nevyjel z obrazovky
		// - 'objekt' nasel kolizi s jinym objektem
		for(IKolizeLeticihoObjektu kolize : objektyKolize)
		{
			// ?ptam se, zda nastala kolize...
			HranaKolize hrana = this.kolizeLeticihoObjektu(kolize);
			if (hrana != null)
			{
				// informuji, ze nastala kolize...
				this.nastalaKolize(kolize, hrana);
			}
			if (kolize instanceof LeticiObjekt)
			{
				// ?ptam se, zda nastala kolize...
				hrana = ((LeticiObjekt) kolize).kolizeLeticihoObjektu(this);
				if (hrana != null)
				{
					// informuji, ze nastala kolize...
					kolize.nastalaKolize(this, hrana);
				}
			}
		}
	}

	/** vraci hranu kolize */
	private final Rectangle optimalizaceKolize1 = new Rectangle();
	private final Rectangle optimalizaceKolize2 = new Rectangle();
	HranaKolize kolizeLeticihoObjektu(IKolizeLeticihoObjektu kolizniObjekt)
	{
		// TODO: vyres kolizi: this vs kolizniObjekt (a vrat hranu kolize)
		optimalizaceKolize1.set( // toto jsem ja! (tato instance objektu)
			this.getX(),
			this.getY(),
			this.getWidth(),
			this.getHeight()
		);
		optimalizaceKolize2.set( // toto je kolizniObjekt
			kolizniObjekt.getX(),
			kolizniObjekt.getY(),
			kolizniObjekt.getWidth(),
			kolizniObjekt.getHeight()
		);
		if (optimalizaceKolize1.fitInside(optimalizaceKolize2) != null) // kolizniObjekt se vejde do me; cely!
		{
//			return HranaKolize.Uplna;
		}
		if (optimalizaceKolize1.overlaps(optimalizaceKolize2))
		{
			if (kolizniObjekt.getX() + kolizniObjekt.getWidth() > getX())
			{
				return HranaKolize.Leva;
			}
			if (kolizniObjekt.getY() + kolizniObjekt.getHeight() > getY())
			{
				return HranaKolize.Dolni;
			}
			if (kolizniObjekt.getX() < getX() + getWidth())
			{
				return HranaKolize.Prava;
			}
			if (kolizniObjekt.getY() < getY() + getHeight())
			{
				return HranaKolize.Horni;
			}
		}
		return null;
	}

	public void nastavRychlost(float x, float y)
	{
		rychlost.x = x;
		rychlost.y = y;
	}

	public void nastavRychlostX(float x)
	{
		nastavRychlost(x, rychlost.y);
	}

	public void nastavRychlostY(float y)
	{
		nastavRychlost(rychlost.x, y);
	}

	public void addKolizniObjekt(IKolizeLeticihoObjektu objektKolize)
	{
		objektyKolize.add(objektKolize);
	}

	@Override
	public void nastalaKolize(IKolizeLeticihoObjektu kolizniObjekt, HranaKolize hranaKolize)
	{
		// TODO: LOGIKA: uprav chovani, podle toho, se kterym objektem kolidujes
		System.out.println(""
			+ "kolize(" + hranaKolize + "): "
			+ getClass().getName()
			+ " x "
			+ kolizniObjekt.getClass().getName())
		;
	}
}
