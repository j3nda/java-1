package com.gde.luzanky.pong.java2;

/** komunikacni rozhranni, ktere udava hranice kolize */
public interface IKolizeLeticihoObjektu
{
	float getX();
	float getY();
	float getWidth();
	float getHeight();
	void nastalaKolize(IKolizeLeticihoObjektu kolizniObjekt, HranaKolize hranaKolize);

	/** {@link HranaKolize} uvadi, ze ktere strany kolize nastala */
	public enum HranaKolize
	{
		Uplna,
		Leva,
		Prava,
		Horni,
		Dolni,
	}
}
