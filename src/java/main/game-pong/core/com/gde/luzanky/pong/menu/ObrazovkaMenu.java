package com.gde.luzanky.pong.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.display.MenuItemDisplay;
import com.gde.common.graphics.display.rainbow.RainbowDisplay;
import com.gde.luzanky.pong.PongGame;
import com.gde.luzanky.pong.SdilenaObrazovka;
import com.gde.luzanky.pong.TypObrazovky;
import com.gde.luzanky.pong.menu.Player2MenuDisplay.Player2Type;

public class ObrazovkaMenu
extends SdilenaObrazovka
implements ITapOnPlayer2Display
{
	private enum PlayerType {
		Honza,
		Roman,
		Tonda,
		;
		@Override
		public String toString()
		{
			return name().toLowerCase();
		}
	}
	private PlayerType currentPlayer = PlayerType.values()[0];


	public ObrazovkaMenu(PongGame parentGame, TypObrazovky previousScreenType)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.MENU;
	}

	@Override
	public void onTapPlayer2Display(String id)
	{
		for(PlayerType player : PlayerType.values())
		{
			if (id.contains(player.toString()))
			{
				currentPlayer = player;
			}
		}
	}

	private SdilenaObrazovka createObrazovkaHry()
	{
		switch(currentPlayer)
		{
			case Roman:
			{
				return new com.gde.luzanky.pong.java1.roman.ObrazovkaHry(parentGame, getScreenType());
			}
			case Tonda:
			{
				return new com.gde.luzanky.pong.java1.tonda.ObrazovkaHry(parentGame, getScreenType());
			}
			case Honza:
			default:
			{
				return new com.gde.luzanky.pong.java1.ObrazovkaHry(parentGame, getScreenType());
			}
		}
	}

	@Override
	public void show()
	{
		super.show();
		float vyskaRadku = Gdx.graphics.getHeight() / 2f; // vyska textu(horni x dolni radek)
		float playerWidth = Gdx.graphics.getWidth() / 4f; // sirka textu: "1" a "2" (vlevo, vpravo) pro volbu 'poctu hracu'

		// text: pong
		RainbowDisplay pong = new RainbowDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			new String[] { "PONG" },
			new Color[] { Color.WHITE },
			Palette.Gfx.Rainbow.Repeated()
		);
		pong.setViewport(
			new Rectangle(
				0,
				Gdx.graphics.getHeight() / 2f,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight() / 2f
			)
		);
		pong.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(
					createObrazovkaHry()
				);
			}
		});

		// text: 1 ~ player1
		MenuItemDisplay player1 = new MenuItemDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			1,
			Color.LIGHT_GRAY
		);
		player1.setText("1");
		player1.setViewport(new Rectangle(
			0,
			0,
			playerWidth,
			vyskaRadku
		));
		player1.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(
					createObrazovkaHry()
				);
			}
		});

		// text: 2 ~ player2
		MenuItemDisplay player2 = new MenuItemDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			1,
			Color.LIGHT_GRAY
		);
		player2.setText(">");
		player2.setViewport(new Rectangle(
			Gdx.graphics.getWidth() - playerWidth,
			0,
			playerWidth,
			vyskaRadku
		));
		player2.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(
					createObrazovkaHry()
				);
// TODO: xhonza: java-2/oop/solution!
//				parentGame.setScreen(
//					new com.gde.luzanky.pong.java2.ObrazovkaHry(
//						parentGame,
//						getScreenType(),
//						resources
//					)
//				);
			}
		});

		// text: (~player2: typ obtiznosti hrace)
		Player2MenuDisplay player2obtiznost = new Player2MenuDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			new Player2Type[] {
				new Player2Type(PlayerType.Honza.toString()),
				new Player2Type(PlayerType.Roman.toString()),
				new Player2Type(PlayerType.Tonda.toString()),
			},
			this
		);
		player2obtiznost.setViewport(new Rectangle(
			playerWidth,
			0,
			Gdx.graphics.getWidth() - (playerWidth * 2),
			vyskaRadku
		));

		getStage().addActor(pong);
		getStage().addActor(player1);
		getStage().addActor(player2);
		getStage().addActor(player2obtiznost);
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}
}
