package com.gde.luzanky.pong.java2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public class KolizeObrazovky
extends LeticiObjekt
implements IKolizeLeticihoObjektu
{
	public KolizeObrazovky()
	{
		super(new Texture(new Pixmap(1, 1, Pixmap.Format.RGB565)));
	}

	@Override
	public float getX()
	{
		return 0;
	}

	@Override
	public float getY()
	{
		return 0;
	}

	@Override
	public float getWidth()
	{
		return Gdx.graphics.getWidth();
	}

	@Override
	public float getHeight()
	{
		return Gdx.graphics.getHeight();
	}

	@Override
	public void nastalaKolize(IKolizeLeticihoObjektu kolizniObjekt, HranaKolize hranaKolize)
	{
		// prazdna implementace!
		// obrazovka neupravuje sve chovani, kdyz s ni neco koliduje...
	}

	@Override
	HranaKolize kolizeLeticihoObjektu(IKolizeLeticihoObjektu kolizniObjekt)
	{
		if (kolizniObjekt instanceof Balonek)
		{
			if (kolizniObjekt.getX() <= getX()) { return HranaKolize.Leva; }
			if (kolizniObjekt.getY() <= getY()) { return HranaKolize.Dolni; }
			if (kolizniObjekt.getX() + kolizniObjekt.getWidth() >= getWidth()) { return HranaKolize.Prava; }
			if (kolizniObjekt.getY() + kolizniObjekt.getHeight() >= getHeight()) { return HranaKolize.Leva; }
		}
		return null;
	}
}
