package com.gde.luzanky.pong;

import com.gde.common.graphics.fonts.FontsManager;
import com.gde.common.graphics.screens.PongScreenResources;
import com.gde.common.graphics.screens.ScreenBase;

/**
 * tuto obrazovku dedim, abych oddelil spolecny zaklad od konkretniho reseni
 * (zejmena vyuzivam doplneni konkretniho typu {@link TypObrazovky})
 */
public abstract class SdilenaObrazovka
extends ScreenBase<TypObrazovky, PongScreenResources, PongGame>
{
	protected FontsManager fontsFactory;

	public SdilenaObrazovka(PongGame parentGame, TypObrazovky previousScreenType)
	{
		super(parentGame, previousScreenType);
	}
}
