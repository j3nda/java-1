package com.gde.luzanky.pong;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gde.common.graphics.display.CelkoveSkore;
import com.gde.common.graphics.display.MultiLineDisplay;
import com.gde.luzanky.pong.menu.ObrazovkaMenu;

public class ObrazovkaKonecHry
extends SdilenaObrazovka
{
	private final boolean jsemVitez;
	private final CelkoveSkore celkoveSkore;

	public ObrazovkaKonecHry(
		PongGame parentGame,
		TypObrazovky previousScreenType,
		boolean jsemVitez,
		CelkoveSkore celkoveSkore
	)
	{
		// zavolani konstruktora predka, tj. "SdilenaObrazovka"
		super(parentGame, previousScreenType);
		this.jsemVitez = jsemVitez;
		this.celkoveSkore = celkoveSkore;
	}

	@Override
	public TypObrazovky getScreenType()
	{
		return TypObrazovky.KONEC_HRY;
	}

	@Override
	public void show()
	{
		super.show();
		float sirka = Gdx.graphics.getWidth()  * 0.8f;
		float vyska = Gdx.graphics.getHeight() / 2f;

		// [vitez]/porazeny<<<<<
		// [0]:0<<<<<<<<<<<<<<<<celkove skore

		MultiLineDisplay horniDisplay = new MultiLineDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			new String[] { jsemVitez ? "vitez" : "porazeny" },
			new Color[]  { jsemVitez ? Color.GOLD : Color.FIREBRICK }
		);
		horniDisplay.setViewport(
			new Rectangle(
				(Gdx.graphics.getWidth() / 2f) - (sirka / 2f),
				(Gdx.graphics.getHeight() / 2f),
				sirka,
				vyska
			)
		);
		horniDisplay.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(new ObrazovkaMenu(parentGame, getPreviousScreen()));
			}
		});
		MultiLineDisplay dolniDisplay = new MultiLineDisplay(
			resources.getFontsManager().getBitFontConfiguration(),
			new String[] { celkoveSkore.getScore(0) + ":" + celkoveSkore.getScore(1) },
			new Color[] { Color.GRAY }
		);
		dolniDisplay.setViewport(
			new Rectangle(
				(Gdx.graphics.getWidth() / 2f) - (sirka / 2f),
				0,
				sirka,
				vyska
			)
		);
		dolniDisplay.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				parentGame.setScreen(new ObrazovkaMenu(parentGame, getPreviousScreen()));
			}
		});
		getStage().addActor(horniDisplay);
		getStage().addActor(dolniDisplay);
	}

	@Override
	protected void renderScreen(float delta)
	{
		renderScreenStage(delta);
	}
}
