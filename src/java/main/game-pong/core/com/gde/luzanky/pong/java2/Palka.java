package com.gde.luzanky.pong.java2;

import com.badlogic.gdx.graphics.Texture;

public class Palka
extends LeticiObjekt
{
	public Palka(Texture texture)
	{
		super(texture);
		rychlost.x = 0;
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		// TODO: vyres chovani 'palky' tak, aby: se pohybovala pouze v y-ove ose
		// TODO: LOGIKA: vyres kolize (podle typu objektu), tj. uprav chovani tak, aby:
		// - 'objekt' nevyjel z obrazovky
		// - 'objekt' nasel kolizi s jinym objektem
	}
}
