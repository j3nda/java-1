package com.gde.luzanky.pong.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.gde.common.graphics.colors.Palette;
import com.gde.common.graphics.display.MenuItemDisplay;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class Player2MenuDisplay
extends MenuItemDisplay
{
	private final Player2Type[] players;
	private int currentPlayer;
	private final ITapOnPlayer2Display listener;
	private static final Color[] rainbowColorPalette1 = Palette.Gfx.Rainbow.Default();

	public Player2MenuDisplay(
		BitFontMaker2Configuration fontConfiguration,
		Player2Type[] players,
		ITapOnPlayer2Display listener
	)
	{
		super(
			fontConfiguration,
			zjistiNejdelsiText(players),
			players[0].fgColor,
			players[0].bgColor
		);
		this.players = players;
		this.currentPlayer = 0;
		this.listener = listener;
		setText(players[0].jmeno);
		addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				onTapDisplay();
			}
		});
		colorIndex = 0;
	}

	private static int zjistiNejdelsiText(Player2Type[] players)
	{
		int delkaJmena = 0;
		for(Player2Type player : players)
		{
			if (delkaJmena < player.jmeno.length())
			{
				delkaJmena = player.jmeno.length();
			}
		}
		return delkaJmena;
	}

	private void onTapDisplay()
	{
		currentPlayer++;
		if (currentPlayer >= players.length)
		{
			currentPlayer = 0;
		}
		setPlayer(players[currentPlayer]);
		listener.onTapPlayer2Display(players[currentPlayer].jmeno);
	}

	public void setPlayer(Player2Type player)
	{
		for(int i = 0; i < player.jmeno.length(); i++)
		{
			updateLetter(
				i,
				player.jmeno.charAt(i),
				player.fgColor,
				player.bgColor
			);
		}
	}

	private static int colorIndex = 0;
	public static class Player2Type
	{
		public final String jmeno;
		public final Color fgColor;
		public final Color bgColor;

		public Player2Type(String jmeno)
		{
			this.jmeno = jmeno;
			this.fgColor = rainbowColorPalette1[colorIndex];
			this.bgColor = Color.BLACK;

			colorIndex++;
			if (colorIndex >= rainbowColorPalette1.length)
			{
				colorIndex = 0;
			}
		}
	}
}
