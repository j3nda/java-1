package com.gde.luzanky.pong;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

/**
 * {@link InputProcessor} pro ovladani palky, formou:
 * - touchDragged(pocitam vektor tahnuti) --> pohybPalkou(vektor-tahnuti)
 */
public class PongInputProcessor
extends InputAdapter
{
	private final IPongMovement movement;
	private final Vector2 maxMovement;
	private final Vector2 last = new Vector2();
	private final Vector2 tmp = new Vector2();

	public PongInputProcessor(IPongMovement movement, Vector2 maxMovement)
	{
		this.movement = movement;
		this.maxMovement = maxMovement;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		movement.pohniPalkou(0);
		return true;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		last.set(screenX, screenY);
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		tmp.set(screenX, screenY).sub(last);
		last.set(screenX, screenY);

		float absX = Math.abs(tmp.x);
		if (absX > maxMovement.x)
		{
			tmp.x = (tmp.x > 0 ? +1 : -1) * maxMovement.x;
		}

		float absY = Math.abs(tmp.y);
		if (absY > maxMovement.y)
		{
			tmp.y = (tmp.y > 0 ? +1 : -1) * maxMovement.y;
		}

		movement.pohniPalkou(absX > absY
			? tmp.x
			: tmp.y * -1
		);
		return true;
	}

	public interface IPongMovement
	{
		void pohniPalkou(float rychlost);
	}
}
