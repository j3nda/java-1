package com.gde.luzanky.pong.java2;

import com.badlogic.gdx.graphics.Texture;

public class Balonek
extends LeticiObjekt
{
	public Balonek(Texture texture)
	{
		super(texture);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		// TODO: vyres chovani 'palky' tak, aby: se pohybovala pouze v y-ove ose
		// TODO: LOGIKA: vyres kolize (podle typu objektu), tj. uprav chovani tak, aby:
		// - 'objekt' nevyjel z obrazovky
		// - 'objekt' nasel kolizi s jinym objektem
	}

	@Override
	public void nastalaKolize(IKolizeLeticihoObjektu kolizniObjekt, HranaKolize hranaKolize)
	{
		super.nastalaKolize(kolizniObjekt, hranaKolize);
		if (kolizniObjekt instanceof KolizeObrazovky)
		{
			kolizeVsObrazovka(hranaKolize);
		}
	}

	private void kolizeVsObrazovka(HranaKolize hranaKolize)
	{
		switch(hranaKolize)
		{
			case Leva:
			case Prava:
			{
				rychlost.x *= -1;
				break;
			}
			case Horni:
			case Dolni:
			{
				rychlost.y *= -1;
				break;
			}
		}
	}
}
