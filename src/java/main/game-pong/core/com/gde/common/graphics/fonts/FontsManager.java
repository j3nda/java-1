package com.gde.common.graphics.fonts;

import com.gde.common.resources.CommonResources;


/** tovarnicka na fonty */
public class FontsManager
implements IFontsManager
{
	private final BitFontMaker2Configuration bitfontConfiguration;

	public FontsManager()
	{
		// vytvori font pro zobrazeni pismen. jako font se pouzije nize znimeny.
		// jedna se o: .ttf prevedene do .json formatu pomoci:
		// -- https://www.pentacom.jp/pentacom/bitfontmaker2/
		bitfontConfiguration = new BitFontMaker2Configuration(CommonResources.Font.born2bSporty16x16);
	}

	@Override
	public BitFontMaker2Configuration getBitFontConfiguration()
	{
		return bitfontConfiguration;
	}
}
