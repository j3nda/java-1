package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class MenuItemDisplay
extends DisplayRowLetters
{
	public MenuItemDisplay(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor
	)
	{
		this(fontConfiguration, length, fgColor, Color.BLACK);
	}

	public MenuItemDisplay(
		BitFontMaker2Configuration fontConfiguration,
		int length,
		Color fgColor,
		Color bgColor
	)
	{
		super(fontConfiguration, length, fgColor, bgColor);
	}

	public void setText(String text)
	{
		if (getLength() < text.length())
		{
			return;
		}
		for(int i = 0; i < text.length(); i++)
		{
			updateLetter(i, text.charAt(i));
		}
	}
}
