package com.gde.common.graphics.display;

import com.badlogic.gdx.graphics.Color;
import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public class CelkoveSkore
extends DisplayRowLetters
{
	private static final int maximalniScore = 9;
	private static final String formatScore = ""
		+ "%0" + (maximalniScore + "").length() + "d"
		+ ":"
		+ "%0" + (maximalniScore + "").length() + "d"
	;
	private final int score[] = new int[] {0, 0};

	public CelkoveSkore(BitFontMaker2Configuration font, Color color)
	{
		super(
			font,
			String.format(formatScore, maximalniScore, maximalniScore).length(),
			color,
			Color.BLACK
		);
		setScore(score[0], 0);
		setScore(score[1], 1);
	}

	public int getScore(int player)
	{
		return score[player];
	}

	public void addScore(int score, int player)
	{
		setScore(getScore(player) + score, player);
	}

	public void setScore(int score, int player)
	{
		if (score < 0)
		{
			score = 0;
		}
		else
		if (score > maximalniScore)
		{
			score = maximalniScore;
		}
		this.score[player] = score;
		nastavScoreJakoText(
			this.score[0],
			this.score[1]
		);
	}

	protected void nastavScoreJakoText(int player1, int player2)
	{
		String text = String.format(formatScore, player1, player2);
		for(int i = 0; i < text.length(); i++)
		{
			updateLetter(i, text.charAt(i));
		}
	}
}
