package com.gde.common.graphics.fonts;

import com.gde.common.graphics.fonts.BitFontMaker2Configuration;

public interface IFontsManager
{
	BitFontMaker2Configuration getBitFontConfiguration();
}
