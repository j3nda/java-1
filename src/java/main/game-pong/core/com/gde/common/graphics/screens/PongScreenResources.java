package com.gde.common.graphics.screens;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gde.common.graphics.fonts.FontsManager;


public class PongScreenResources
extends ScreenResources
{
	private final FontsManager fontsManager;


	public PongScreenResources(Camera camera, Viewport viewport, Batch batch)
	{
		super(camera, viewport, batch);
		fontsManager = new FontsManager();
	}

	public FontsManager getFontsManager()
	{
		return fontsManager;
	}
}
