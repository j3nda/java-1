#!/bin/bash -e

projectName=$1

currentDir=$(pushd ./ > /dev/null; pwd; popd > /dev/null;)

${currentDir}/check-copyright.sh "${projectName}" yes \
	| grep -Ei 'games' \
	| sort \
	| uniq
