#!/bin/bash -ex

currentDir=$(pushd $(dirname "$0") > /dev/null; pwd; popd > /dev/null;)

templateName="example-gdx"
templateNameRE="$(echo "${templateName}"|sed -r 's/\-/\\\-/g')"
gameName="$(echo "$1"|tr '[A-Z]' '[a-z]')"
gameNameRE="$(echo "${gameName}"|sed -r 's/\-/\\\-/g')"
	[ "${gameName}" == "" ] && { echo "ERROR: gameName is missing!" 2>&1; exit 1; }

[ ! -r "${currentDir}/../IDE/eclipse" ] && {
	echo "ERROR: currentDir must be tools!" 2>&1
	exit 1
}


# IDE/eclipse/{game-name}
rsync -avh \
	--exclude bin \
	"${currentDir}/../IDE/eclipse/${templateName}/" \
	"${currentDir}/../IDE/eclipse/${gameName}"

sed -i \
	"s/${templateNameRE}/${gameName}/g" \
	"${currentDir}/../IDE/eclipse/${gameName}/.project"

# resources/assets
dn="${currentDir}/../src/resources/${gameName}"; mkdir -p "${dn}"; echo "" > ${dn}/.gitempty
dn="${currentDir}/../src/assets/${gameName}"; mkdir -p "${dn}"; echo "" > ${dn}/.gitempty

# src/java
rsync -avh \
	--exclude android \
	"${currentDir}/../src/java/main/${templateName}/" \
	"${currentDir}/../src/java/main/${gameName}"

#mkdir -p "${currentDir}/../src/java/main/${gameName}/core";
#mkdir -p "${currentDir}/../src/java/main/${gameName}/desktop";

#src
#assets
#resources
