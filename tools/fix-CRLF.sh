#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pushd "${SCRIPT_DIR}/../src" > /dev/null

	# java:
	pushd "./java" > /dev/null
		find -name '*.java' -type f -print \
		| while read fn
		do
			dos2unix "${fn}"
		done
	popd > /dev/null

	# assets:
	pushd "./assets" > /dev/null
		find -name '*.json' -type f -print \
		| while read fn
		do
			dos2unix "${fn}"
		done
	popd > /dev/null


popd > /dev/null
