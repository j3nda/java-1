#!/bin/bash -ex

currentDir=$(pushd ./ > /dev/null; pwd; popd > /dev/null;)

find ${currentDir}/../IDE/eclipse -maxdepth 2 -type d -print \
	| grep -E '\/bin$' \
	| while read dn;
	do
		rm -fr "${dn}"
	done
