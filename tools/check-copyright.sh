#!/bin/bash -e

projectName=$1
withGameList=$2

currentDir=$(pushd ./ > /dev/null; pwd; popd > /dev/null;)
srcDir="${currentDir}/../src/java/main/${projectName}"
srcDirRE=$(echo "${srcDir}/" | sed 's/\//\\\//g')

getCopyrightHeader()
{
	cat "$1" | head -n 40 | grep -B 99 -Ei '^[[:blank:]]*\*{42,}+\/[[:blank:]]*'
}

hasCopyright()
{
	local fn="$1"
	local isCopyright=$((getCopyrightHeader "${fn}") | sed 1d | grep -iE 'copyright')
	if [ "${isCopyright}" != "" ];
	then
		echo "yes"
	else
		echo "no"
	fi
}

getGamesList()
{
	local header=$(getCopyrightHeader "$1")
	echo "${header}" | sed '$d' | grep -Ei -A 99 'developed as part of' | sed 1d
}

getGamesCount()
{
	getGamesList "$1" | wc -l
}

[ ! -d "${srcDir}" ] && {
	echo "ERROR: invalid project(${projectName})!" 2>&1
	ls -1 $(dirname "${srcDir}")
	exit 1
}

find "${srcDir}" -name '*.java' -print \
	| while read fn;
	do
#fn="/mnt/c/xhonza/r/gitlab/java-1/tools/../src/java/main/common/com/gde/common/graphics/fonts/BitFontMaker2Configuration.java"
		fnShort=$(echo "${fn}" | sed -r "s/${srcDirRE}//g")
		has=$(hasCopyright "${fn}")
		if [ "${has}" == "yes" ];
		then
			# TODO: diff ~ zda mi sedi format...
			gamesCount=$(getGamesCount "${fn}")
			if [ "${gamesCount}" == "0" ];
			then
				echo -e "YES-0-|$(printf '%2sx' "${gamesCount}")\t${fnShort}"
			else
				echo -e "yes---|$(printf '%2sx' "${gamesCount}")\t${fnShort}"
			fi
			[ "${withGameList}" != "" ] && {
				getGamesList "${fn}"
			}

		elif [ "${has}" == "no" ];
		then
			echo -e "----NO|\t${fn}"

		else
			echo "ERROR: ${fn}"
		fi
#		break
	done
