#!/bin/bash -e

scriptDir=$(pushd ./ > /dev/null; pwd; popd > /dev/null;)
dt=$(date '+%y%m%d-%H%M%S')
currentBranch=$(git branch --show-current)

git checkout -b tmp/luzanky/${dt}
git add ${scriptDir}/../src
git commit -m "luzanky-update.sh: ${dt} from: ${currentBranch}"
git checkout master
git pull origin master --rebase
