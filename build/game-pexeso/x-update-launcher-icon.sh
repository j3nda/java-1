#!/bin/bash -e

scriptDir="$(pushd ./ > /dev/null; pwd; popd > /dev/null)"
sourceIconImage="${scriptDir}/../../src/resources/$(basename ${scriptDir})/icon-back-128x128.png"

. ${scriptDir}/../gradle-scripts/update-launcher-icon.sh
