#!/bin/bash -e

scriptDir="$(pushd ./ > /dev/null; pwd; popd > /dev/null)"
sourceIconImage="${scriptDir}/../../src/resources/$(basename ${scriptDir})/icons/hangman512x512.png"

. ${scriptDir}/../gradle-scripts/update-launcher-icon.sh
