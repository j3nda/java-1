#!/bin/bash -e

scriptDir="$(pushd ./ > /dev/null; pwd; popd > /dev/null)"
sourceIconImage="${scriptDir}/../../src/resources/$(basename ${scriptDir})/221007-183421_bombarder-1024x1024.png"

. ${scriptDir}/../gradle-scripts/update-launcher-icon.sh
