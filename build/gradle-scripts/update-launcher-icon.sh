#!/bin/bash -e

[ "${scriptDir}" == "" ] || [ ! -d "${scriptDir}" ] && { echo "ERROR: scriptDir is missing!" 2>&1; exit 1; }
[ "${sourceIconImage}" == "" ] || [ ! -r "${sourceIconImage}" ] && { echo "ERROR: sourceIconImage is missing!" 2>&1; exit 1; }

echo "${sourceIconImage}"
echo "android:"
echo "
192,xxxhdpi
144,xxhdpi
96,xhdpi
72,hdpi
48,mdpi
36,ldpi
"	| grep -Ev '^[[:blank:]]*$' \
	| grep -Ev '^[[:blank:]]*$' \
	| while read line;
	do
		size=$(echo "${line}"|cut -d',' -f1);
		sizeDirName=$(echo "${line}"|cut -d',' -f2);
		convert \
			-resize ${size}x${size} \
			${sourceIconImage} \
			${scriptDir}/android/res/drawable-${sizeDirName}/ic_launcher.png \
			&& { echo -e "\t${line}:\tOK"; }
	done
echo
scriptDirRE=$(echo "${scriptDir}" | sed -r 's/\//\\\//g' | sed -r 's/\-/\\\-/g')
find ${scriptDir}/android/res -name '*.png' -print \
	| while read fn;
	do
		identify ${fn} | sed -r "s/^${scriptDirRE}//g"
	done

# android/shop/{icons, etc}
gameName=$(basename "${scriptDir}")
gplayIconsDir="${scriptDir}/../../src/resources/${gameName}/icons"
[ ! -d "${gplayIconsDir}" ] && { mkdir -p "${gplayIconsDir}"; }

# icon, 512x512
echo -n "icon-512x512" \
	&& convert \
		-resize 512x512 \
		${sourceIconImage} \
		${gplayIconsDir}/icon-512x512.png \
	&& echo ": OK"

# main-gfx, 1024x500
