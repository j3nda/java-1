#!/bin/bash -e

scriptDir="$(pushd ./ > /dev/null; pwd; popd > /dev/null)"
sourceIconImage="${scriptDir}/../../src/resources/$(basename ${scriptDir})/dino-original/assets/sprites/_raw/player/start/.dino-start.png" #88x94

. ${scriptDir}/../gradle-scripts/update-launcher-icon.sh
