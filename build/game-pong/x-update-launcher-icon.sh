#!/bin/bash -e

scriptDir="$(pushd ./ > /dev/null; pwd; popd > /dev/null)"
sourceIconImage="${scriptDir}/../../src/resources/$(basename ${scriptDir})/pong-icon-768x768.png"

. ${scriptDir}/../gradle-scripts/update-launcher-icon.sh
